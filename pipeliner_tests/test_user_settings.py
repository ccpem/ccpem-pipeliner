#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import io
import json
import os
import shutil
import tempfile
import unittest
from pathlib import Path
from unittest.mock import patch

from pipeliner import user_settings


class PipelinerUserSettingsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="ccpem-pipeliner_")
        self.settings_file = Path(self.test_dir) / "settings.json"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("platform.system", return_value="Linux")
    @patch.dict("os.environ")
    def test_get_settings_path_linux(self, _):
        os.environ["XDG_CONFIG_HOME"] = self.test_dir
        path = user_settings._get_settings_path()
        assert (
            path
            == Path(self.test_dir)
            / "ccpem"
            / "pipeliner"
            / "ccpem-pipeliner-settings.json"
        )

    @patch("platform.system", return_value="Darwin")
    @patch("pathlib.Path.expanduser")
    def test_get_settings_path_mac(self, mock_path_expanduser, _):
        mock_path_expanduser.return_value = Path(self.test_dir)
        path = user_settings._get_settings_path()
        assert (
            path
            == Path(self.test_dir)
            / "ccpem"
            / "pipeliner"
            / "ccpem-pipeliner-settings.json"
        )

    @patch("platform.system", return_value="Windows")
    def test_get_settings_path_windows_raises_error(self, _):
        with self.assertRaises(RuntimeError):
            _ = user_settings._get_settings_path()

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_load_settings_from_good_json_file(self, mock_stdout):
        self.settings_file.write_text('{"scratch_dir": "Example setting value"}')
        file_settings = user_settings.Settings(self.settings_file).settings_from_file
        assert file_settings is not None
        assert type(file_settings) is dict
        assert len(file_settings) == 1
        assert file_settings["scratch_dir"] == "Example setting value"
        # This should now show any warnings on stdout
        assert mock_stdout.getvalue() == ""

    def test_load_settings_from_json_file_with_unrecognised_keys(self):
        with self.assertLogs("pipeliner.user_settings", level="WARNING") as cm:
            self.settings_file.write_text(
                '{"example_setting": "Example setting value"}'
            )
            _ = user_settings.Settings(self.settings_file)
            assert "CCP-EM Pipeliner found unrecognised settings" in cm.output[0]

            assert (
                "The following settings were not recognised and will be ignored:"
                " example_setting." in cm.output[0]
            )

    def test_load_settings_from_empty_json_file(self):
        with self.assertLogs("pipeliner.user_settings", level="WARNING") as cm:
            settings = user_settings._load_settings_from_file(self.settings_file)
            assert settings == {}
            assert "CCP-EM Pipeliner could not load a settings file" in cm.output[0]

    def test_create_settings(self):
        example_settings_file = Path(self.test_dir) / "settings_dir" / "settings.json"
        assert not example_settings_file.parent.is_dir()
        assert not example_settings_file.is_file()
        user_settings._create_settings_json(example_settings_file, {})
        assert example_settings_file.parent.is_dir()
        assert example_settings_file.is_file()
        assert example_settings_file.read_text() == "{}"

    def test_create_settings_with_problem(self):
        bad_settings_file = Path(self.test_dir) / "settings_dir"
        bad_settings_file.mkdir()
        with self.assertLogs("pipeliner.user_settings", level="WARNING") as cm:
            user_settings._create_settings_json(bad_settings_file, {})
            assert "CCP-EM Pipeliner could not create a settings file" in cm.output[0]

    def test_get_path_list_setting_invalid_name(self):
        settings = user_settings.Settings(self.settings_file)
        with self.assertRaises(KeyError):
            _ = settings.get_list("not_path_to_source_files")

    def test_get_path_list_setting_wrong_type(self):
        settings = user_settings.Settings(self.settings_file)
        with self.assertRaises(AssertionError):
            _ = settings.get_list("qsub_template")

    def test_get_path_list_setting_from_defaults(self):
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_list("path_to_source_files")
        assert value == []

    def test_get_path_list_setting_from_file(self):
        settings_for_file = {"path_to_source_files": ["path1", "path/2"]}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_list("path_to_source_files")
        assert value == ["path1", "path/2"]

    @patch.dict("os.environ", {"PIPELINER_PATH_TO_SOURCE_FILES": "path3:path4"})
    def test_get_path_list_setting_from_env_var(self):
        settings_for_file = {"path_to_source_files": ["path1", "path/2"]}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_list("path_to_source_files")
        assert value == ["path3", "path4"]

    def test_get_qsub_template_setting_from_defaults(self):
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_string("qsub_template")
        assert value == ""

    def test_get_qsub_template_setting_from_file(self):
        settings_for_file = {"qsub_template": "/path/to/qsub/template.sh"}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_string("qsub_template")
        assert value == "/path/to/qsub/template.sh"

    @patch.dict("os.environ", {"PIPELINER_QSUB_TEMPLATE": "/path/to/qsub/template2.sh"})
    def test_get_qsub_template_setting_from_pipeliner_env_var(self):
        settings_for_file = {"qsub_template": "/this/path/should/be/overridden"}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_string("qsub_template")
        assert value == "/path/to/qsub/template2.sh"

    @patch.dict("os.environ", {"RELION_QSUB_TEMPLATE": "/path/to/qsub/template3.sh"})
    def test_get_qsub_template_setting_from_relion_env_var(self):
        settings_for_file = {"qsub_template": "/this/path/should/be/overridden"}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_string("qsub_template")
        assert value == "/path/to/qsub/template3.sh"

    @patch.dict(
        "os.environ",
        {
            "PIPELINER_QSUB_TEMPLATE": "/path/to/qsub/template4.sh",
            "RELION_QSUB_TEMPLATE": "/this/path/should/be/overridden",
        },
    )
    def test_get_qsub_template_setting_from_both_env_vars(self):
        settings_for_file = {"qsub_template": "/this/path/should/also/be/overridden"}
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_string("qsub_template")
        assert value == "/path/to/qsub/template4.sh"

    @patch.dict("os.environ", {"PIPELINER_QUEUE_USE": "yes"})
    def test_get_queue_use_yes_from_pipeliner_env_var(self):
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_bool("queue_use")
        assert value is True

    @patch.dict("os.environ", {"PIPELINER_QUEUE_USE": "no"})
    def test_get_queue_use_no_from_pipeliner_env_var(self):
        settings = user_settings.Settings(self.settings_file)
        value = settings.get_bool("queue_use")
        assert value is False

    def test_get_qsub_extra1_fails_before_calling_get_qsub_extras(self):
        settings = user_settings.Settings(self.settings_file)
        with self.assertRaises(KeyError):
            _ = settings.get_string("qsub_extra1")

    def test_get_qsub_extra3_defaults(self):
        settings = user_settings.Settings(self.settings_file)
        extras = settings.get_qsub_extras(3)
        assert extras["name"] == ""
        assert extras["default"] == ""
        assert extras["help"] == ""
        assert settings.get_string("qsub_extra3") == ""
        assert settings.get_string("qsub_extra3_default") == ""
        assert settings.get_string("qsub_extra3_help") == ""
        with self.assertRaises(KeyError):
            _ = settings.get_string("qsub_extra1")

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_get_qsub_extra1_from_file(self, mock_stdout):
        settings_for_file = {
            "qsub_extra_count": 1,
            "qsub_extra1": "First extra qsub variable",
            "qsub_extra1_default": "extra1 default value",
            "qsub_extra1_help": "extra1 help text",
        }
        with open(self.settings_file, "w") as settings_json:
            json.dump(settings_for_file, settings_json)
        settings = user_settings.Settings(self.settings_file)
        extras = settings.get_qsub_extras(1)
        assert extras["name"] == "First extra qsub variable"
        assert extras["default"] == "extra1 default value"
        assert extras["help"] == "extra1 help text"
        assert settings.get_string("qsub_extra1") == "First extra qsub variable"
        assert settings.get_string("qsub_extra1_default") == "extra1 default value"
        assert settings.get_string("qsub_extra1_help") == "extra1 help text"
        # Ensure there are no warnings about unknown keys on stdout
        assert mock_stdout.getvalue() == ""

    @patch.dict(
        "os.environ",
        {
            "PIPELINER_QSUB_EXTRA2": "Second extra",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "2",
            "PIPELINER_QSUB_EXTRA2_HELP": "Help for second extra",
        },
    )
    def test_get_qsub_extra2_from_env_vars(self):
        settings = user_settings.Settings(self.settings_file)
        extras = settings.get_qsub_extras(2)
        assert extras["name"] == "Second extra"
        assert extras["default"] == "2"
        assert extras["help"] == "Help for second extra"

    @patch.dict(
        "os.environ",
        {
            "PIPELINER_QSUB_EXTRA12": "Twelve extras!",
            "PIPELINER_QSUB_EXTRA12_DEFAULT": "12!",
            "RELION_QSUB_EXTRA12_HELP": "With mixed Relion and Pipeliner env vars.",
        },
    )
    def test_get_qsub_extra12_from_env_vars(self):
        settings = user_settings.Settings(self.settings_file)
        extras = settings.get_qsub_extras(12)
        assert extras["name"] == "Twelve extras!"
        assert extras["default"] == "12!"
        assert extras["help"] == "With mixed Relion and Pipeliner env vars."

    def test_all_top_level_getter_functions(self):
        """Check that all of the ``get_<setting name>()`` functions work without errors
        and access the correct underlying setting from the dictionary."""
        for name in dir(user_settings):
            if name.startswith("get_"):
                # Skip the qsub extras and get_ccpem_share_dir functions
                # since they behave differently
                if name == "get_qsub_extras" or name == "get_ccpem_share_dir":
                    continue
                # Mock the settings object so we can easily check how it was called
                # (The mock is made here rather than as a decorator so we get a fresh
                #  mock to test each getter function.)
                with patch.object(user_settings, "_settings") as mock_settings:
                    func = getattr(user_settings, name)
                    print(f"Calling user_settings.{name}")
                    _ = func()

                    # Inspect the mock to check the name of the setting that was fetched
                    assert len(mock_settings.mock_calls) == 1
                    call_args = mock_settings.mock_calls[0][1]
                    assert len(call_args) == 1
                    assert name[4:] in call_args[0]

    def test_missing_extra_names_raises_warning(self):
        self.settings_file.write_text('{"qsub_extra_count": 3}')
        with self.assertLogs("pipeliner.user_settings", level="WARNING") as cm:
            file_settings = user_settings.Settings(
                self.settings_file
            ).settings_from_file
            assert cm.output == [
                "WARNING:pipeliner.user_settings:CCP-EM Pipeliner found 3 qsub_extra"
                " variables have been specified in the 'qsub_extra_count' setting, but"
                " the expected settings for the following names are missing:"
                " qsub_extra1, qsub_extra2, qsub_extra3"
            ]
        assert file_settings["qsub_extra_count"] == 3
        # This should show a warning on stdout

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_no_warning_if_no_qsub_extra_name_needed(self, mock_stdout):
        self.settings_file.write_text('{"qsub_extra_count": 0}')
        file_settings = user_settings.Settings(self.settings_file).settings_from_file
        assert file_settings["qsub_extra_count"] == 0
        # This should show no warning on stdout
        assert mock_stdout.getvalue() == ""

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_no_warning_if_all_qsub_extra_names_accounted_for(self, mock_stdout):
        self.settings_file.write_text('{"qsub_extra_count": 1, "qsub_extra1": "qs1"}')
        file_settings = user_settings.Settings(self.settings_file).settings_from_file
        assert file_settings["qsub_extra_count"] == 1
        assert file_settings["qsub_extra1"] == "qs1"
        # This should show no warning on stdout
        assert mock_stdout.getvalue() == ""


if __name__ == "__main__":
    unittest.main()
