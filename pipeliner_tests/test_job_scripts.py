#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import contextlib
import io
import math
import os
import shutil
import tempfile
import unittest
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner.scripts.job_scripts import (
    get_map_parameters,
    check_star_file,
    cleanup_tmp_files,
    check_for_output_files,
)
from pipeliner.data_structure import RELION_SUCCESS_FILE


class PipelinerJobScriptsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_jobscripts")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_run_subprocess_get_map_parameters(self):
        map_input = os.path.join(self.test_data, "emd_3488.mrc")

        run_subprocess(
            get_python_command()
            + [
                get_map_parameters.__file__,
                "-m",
                map_input,
                "-odir",
                self.test_dir,
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile(
            os.path.join(self.test_dir, "emd_3488_map_parameters.json")
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "emd_3488_map_parameters.json")
            ).st_size,
            326,
            rel_tol=0.05,
        )

    def test_check_starfile_all_good(self):
        datafile = Path(self.test_data) / "class2d_data.star"
        args = ["--fn_in", str(datafile), "--block", "particles"]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 0
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            f'Checking contents of block "particles" in file "{datafile}"...\n'
            "Found 2377 records\n"
        )

    def test_check_starfile_all_good_sole_block(self):
        datafile = Path(self.test_data) / "sole_block.star"
        args = ["--fn_in", str(datafile)]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 0
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            f'Checking contents of the sole block in file "{datafile}"...\n'
            "Found 4 records\n"
        )

    def test_check_starfile_bad_blockname(self):
        datafile = Path(self.test_data) / "class2d_data.star"
        args = ["--fn_in", str(datafile), "--block", "bad"]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 1
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            f'Checking contents of block "bad" in file "{datafile}"...\n'
            "An error occurred trying to read the file: Block bad not found\n"
        )

    def test_check_starfile_missing(self):
        datafile = Path(self.test_data) / "nonexistent.star"
        args = ["--fn_in", str(datafile), "--block", "irrelevant"]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 1
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            f'Checking contents of block "irrelevant" in file "{datafile}"...\n'
            f"An error occurred trying to read the file:"
            f" [Errno 2] No such file or directory: '{datafile}'\n"
        )

    def test_check_starfile_empty(self):
        datafile = Path(self.test_data) / "empty.star"
        args = ["--fn_in", str(datafile), "--block", "coordinate_files"]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 1
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            f'Checking contents of block "coordinate_files" in file "{datafile}"...\n'
            "No data found\n"
        )

    def test_check_starfile_empty_removes_status_file(self):
        datafile = Path(self.test_data) / "empty.star"
        shutil.copy(datafile, self.test_dir)
        sf = Path(self.test_dir) / RELION_SUCCESS_FILE
        sf.touch()
        args = [
            "--fn_in",
            "empty.star",
            "--block",
            "coordinate_files",
            "--clear_relion_success_file",
        ]
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            assert check_star_file.main(args) == 1
        assert not sf.is_file()
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == (
            'Checking contents of block "coordinate_files" in file "empty.star"...\n'
            "No data found\n"
            f'Removing file "{RELION_SUCCESS_FILE}"\n'
        )

    def test_check_starfile_empty_soleblock(self):
        datafile = Path(self.test_data) / "empty_sole_block.star"
        args = ["--fn_in", str(datafile)]
        assert check_star_file.main(args) == 1

    def test_cleanup_tmp_files(self):
        t1 = Path("tmp1.txt")
        t2 = Path("tmp2.txt")
        t3 = Path("tmp1/tmp2/test.tmp")
        d1 = Path("mydir/")
        d2 = Path("dir1/dir2/")
        xd1 = Path("nonexistent_dir/")
        xf1 = Path("nonexistent.txt")
        for d in [t3.parent, d1, d2]:
            d.mkdir(parents=True)
            assert d.is_dir()

        for f in [t1, t2, t3]:
            f.touch()
            assert f.is_file()

        cleanup_tmp_files.main(
            [
                "--files",
                str(t1),
                str(t2),
                str(t3),
                str(xf1),
                "--dirs",
                str(d1),
                str(d2),
                str(xd1),
            ]
        )
        for f in [t1, t2, t3]:
            assert not f.is_file()
        for d in [d1, d2]:
            assert not d.is_dir()

    def test_check_for_output_files(self):
        files = [f"f{n}.txt" for n in range(3)]
        for f in files:
            Path(f).touch()
        check_for_output_files.main(["--files"] + files)

    def test_check_for_output_files_missing(self):
        files = [f"f{n}.txt" for n in range(3)]
        for f in files:
            Path(f).touch()
        files.append("file4.txt")
        with self.assertRaises(ValueError):
            check_for_output_files.main(["--files"] + files)


if __name__ == "__main__":
    unittest.main()
