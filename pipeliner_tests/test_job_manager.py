#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import contextlib
import io
import os
import shutil
import subprocess
import sys
import tempfile
import unittest
from pathlib import Path
from unittest.mock import patch, Mock

from pipeliner import job_manager, job_factory
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import live_test, job_running_test
from pipeliner.data_structure import (
    RELION_SUCCESS_FILE,
    SUCCESS_FILE,
    JOBSTATUS_FAIL,
    ABORT_FILE,
    FAIL_FILE,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
)
from pipeliner_tests.testing_tools import DummyJob, clean_starfile
from pipeliner.utils import get_pipeliner_root, touch
from pipeliner.project_graph import ProjectGraph
from pipeliner.data_structure import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_MASK3D,
)


class JobManagerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles"
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @live_test(job="relion.import")
    def test_running_fails_file_validation(self):
        # Prepare a new pipeline and run the job
        with ProjectGraph(create_new=True) as pipeline:
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
            )
            with self.assertRaises(ValueError):
                job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)

    @live_test(job="relion.import")
    def test_running_import_mask_job(self):
        # Copy the mask file into place
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        with ProjectGraph(create_new=True) as pipeline:
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
            )
            job_dir = "Import/job001/"
            assert not os.path.isdir(job_dir)

            job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)

            # Test for output files
            assert os.path.isdir(job_dir)
            assert os.path.isfile(os.path.join(job_dir, "run.out"))
            assert os.path.isfile(os.path.join(job_dir, "run.err"))
            assert os.path.isfile(os.path.join(job_dir, "emd_3488_mask.mrc"))

            # Test for correct project graph state
            assert len(pipeline.process_list) == 1
            assert pipeline.job_counter == 2
            assert len(pipeline.node_list) == 1
            assert pipeline.node_list[0].name == "Import/job001/emd_3488_mask.mrc"
            assert pipeline.node_list[0].type == f"{NODE_MASK3D}.mrc"
            assert os.path.isfile(".gui_relion_importjob.star")

            # look for the metadata file
            # md_files = glob("Import/job001/*job_metadata.json")
            # assert len(md_files) == 1

    @live_test(job="relion.import")
    def test_running_import_map_job(self):
        # Copy the map file into place
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        with ProjectGraph(create_new=True) as pipeline:
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Import/import_map.job")
            )
            job_dir = "Import/job001/"
            assert not os.path.isdir(job_dir)

            job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)

            # Test for output files
            assert os.path.isdir(job_dir)
            assert os.path.isfile(os.path.join(job_dir, "run.out"))
            assert os.path.isfile(os.path.join(job_dir, "run.err"))
            assert os.path.isfile(os.path.join(job_dir, "emd_3488.mrc"))

            # Test for correct project graph state
            assert len(pipeline.process_list) == 1
            assert pipeline.job_counter == 2
            assert len(pipeline.node_list) == 1
            assert pipeline.node_list[0].name == "Import/job001/emd_3488.mrc"
            assert pipeline.node_list[0].type == f"{NODE_DENSITYMAP}.mrc"

            # look for the metadata file
            # md_files = glob("Import/job001/*job_metadata.json")
            # assert len(md_files) == 1

    @live_test(job="relion.postprocess")
    def test_running_postprocess_job(self):
        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        with ProjectGraph(create_new=True) as pipeline:
            pipeline.job_counter = 3
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
            )

            job_dir = "PostProcess/job003/"

            assert not os.path.isdir(job_dir)

            job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)

            # Test for output files
            assert os.path.isdir(job_dir)
            assert os.path.isfile(os.path.join(job_dir, "run.out"))
            assert os.path.isfile(os.path.join(job_dir, "run.err"))
            assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
            assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
            assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))
            assert os.path.isfile(os.path.join(job_dir, RELION_SUCCESS_FILE))
            assert os.path.isfile(os.path.join(job_dir, SUCCESS_FILE))
            assert os.path.isfile(".gui_relion_postprocessjob.star")

            # look for the metadata file
            # md_files = glob("PostProcess/job003/*job_metadata.json")
            # assert len(md_files) == 1

    @live_test(job="relion.postprocess")
    def test_running_postprocess_job_with_alias(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.job_counter = 3
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
            )

            job_dir = "PostProcess/job003/"
            assert not os.path.isdir(job_dir)

            job_manager.run_job(
                pipeline=pipeline, job=job, alias="test_alias", run_in_foreground=True
            )

            # Test for output files
            assert os.path.isdir(job_dir)
            assert os.path.isfile(os.path.join(job_dir, "run.out"))
            assert os.path.isfile(os.path.join(job_dir, "run.err"))
            assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
            assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
            assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))
            assert os.path.isfile(os.path.join(job_dir, RELION_SUCCESS_FILE))
            assert os.path.isfile(os.path.join(job_dir, SUCCESS_FILE))
            assert os.path.isfile(".gui_relion_postprocessjob.star")

            # look for the metadata file
            # md_files = glob("PostProcess/job003/*job_metadata.json")
            # assert len(md_files) == 1

            the_job = pipeline.find_process("PostProcess/job003/")
            assert the_job.alias == "PostProcess/test_alias/"
            assert os.path.islink("PostProcess/test_alias")

    @live_test(job="relion.postprocess")
    def test_schedule_postprocess_job(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.job_counter = 3
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
            )

            job_dir = "PostProcess/job003/"
            assert not os.path.isdir(job_dir)

            job_manager.schedule_job(pipeline=pipeline, job=job)

            the_job = pipeline.find_process("PostProcess/job003/")
            assert os.path.isdir("PostProcess/job003")
            assert the_job.status == "Scheduled"
            assert not the_job.alias

    @live_test(job="relion.postprocess")
    def test_schedule_postprocess_job_with_alias(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.job_counter = 3
            job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
            )

            job_dir = "PostProcess/job003/"
            assert not os.path.isdir(job_dir)

            job_manager.schedule_job(pipeline=pipeline, job=job, alias="test_alias")

            the_job = pipeline.find_process("PostProcess/job003/")
            assert the_job.alias == "PostProcess/test_alias/"
            assert os.path.isdir("PostProcess/job003")
            assert os.path.islink("PostProcess/test_alias")
            assert the_job.status == "Scheduled"
            assert the_job.alias == "PostProcess/test_alias/"

    def test_generic_with_PP(self):
        job_running_test(
            test_jobfile=str(Path(self.jobfiles) / "PostProcess/postprocess.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    def test_running_maskcreate_job(self):
        job_running_test(
            test_jobfile=str(self.jobfiles / "MaskCreate/maskcreate.job"),
            input_files=[("Import/job001", str(Path(self.test_data) / "emd_3488.mrc"))],
            expected_outfiles=("run.out", "run.err", "mask.mrc"),
        )

    def test_postprocess_adhocbf(self):
        job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_adhocbf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    def test_postprocess_autobf(self):
        job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_autobf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    def test_split_job(self):
        """Select jobs that split star files in defined size groups generate
        an unknown number of output files so they are a special case where
        output nodes are generated after the job runs"""

        # first run a job to create the dirs
        job_running_test(
            test_jobfile=str(self.jobfiles / "Select/select_parts_split_runtest.job"),
            input_files=[
                ("Extract/job001", str(Path(self.test_data) / "particles.star"))
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "particles_split1.star",
                "particles_split2.star",
                "particles_split3.star",
                "particles_split4.star",
                "particles_split5.star",
                "particles_split6.star",
                "particles_split7.star",
                "particles_split8.star",
                "particles_split9.star",
                "particles_split10.star",
                "particles_split11.star",
                "particles_split12.star",
            ),
        )

        partnode = f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
        exp_lines = [
            ["Select/job998/", "Select/job998/particles_split1.star"],
            ["Select/job998/", "Select/job998/particles_split10.star"],
            ["Select/job998/", "Select/job998/particles_split11.star"],
            ["Select/job998/", "Select/job998/particles_split12.star"],
            ["Select/job998/", "Select/job998/particles_split2.star"],
            ["Select/job998/", "Select/job998/particles_split3.star"],
            ["Select/job998/", "Select/job998/particles_split4.star"],
            ["Select/job998/", "Select/job998/particles_split5.star"],
            ["Select/job998/", "Select/job998/particles_split6.star"],
            ["Select/job998/", "Select/job998/particles_split7.star"],
            ["Select/job998/", "Select/job998/particles_split8.star"],
            ["Select/job998/", "Select/job998/particles_split9.star"],
            ["Select/job998/particles_split1.star", partnode],
            ["Select/job998/particles_split10.star", partnode],
            ["Select/job998/particles_split11.star", partnode],
            ["Select/job998/particles_split12.star", partnode],
            ["Select/job998/particles_split2.star", partnode],
            ["Select/job998/particles_split3.star", partnode],
            ["Select/job998/particles_split4.star", partnode],
            ["Select/job998/particles_split5.star", partnode],
            ["Select/job998/particles_split6.star", partnode],
            ["Select/job998/particles_split7.star", partnode],
            ["Select/job998/particles_split8.star", partnode],
            ["Select/job998/particles_split9.star", partnode],
        ]
        lines = clean_starfile("default_pipeline.star")
        for i in lines:
            print(i)
        for line in exp_lines:
            assert line in lines, line

    def test_overwriting_job_with_different_type_raises_error(self):
        """Can't overwrite a job with a different type of job"""
        # first run a job to create the dirs
        job_run = job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_autobf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

        # then overwrite it with a different job
        with self.assertRaises(ValueError):
            job_running_test(
                test_jobfile=str(self.jobfiles / "MaskCreate/maskcreate.job"),
                input_files=[
                    ("Import/job001", str(Path(self.test_data) / "emd_3488.mrc"))
                ],
                expected_outfiles=("run.out", "run.err", "mask.mrc"),
                overwrite_jobname=job_run.name,
            )

    def test_overwriting_job(self):
        """Overwrite a job with a new run of the same type"""
        # first run a job to create the dirs
        job_run = job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_autobf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

        # then overwrite it with another job
        job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_skipfsc.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            overwrite_jobname=job_run.name,
        )

    def test_overwriting_job_with_children(self):
        """Overwrite a job with children, make sure it is archived and
        The change is noted on the children"""
        # first run a job to create the dirs
        job_run = job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_autobf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

        # copy in a pipeline with fake child jobs
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/children_pipeline.star"),
            "default_pipeline.star",
        )

        # make the childjob dirs
        os.makedirs("ChildProcess/job004/")
        os.makedirs("ChildProcess/job005/")

        # then overwrite it with another job
        with self.assertRaisesRegex(
            ValueError,
            "Cannot overwrite job PostProcess/job998/ because other jobs have used it"
            " as an input",
        ):
            job_running_test(
                test_jobfile=str(self.jobfiles / "PostProcess/postprocess_adhocbf.job"),
                input_files=[
                    (
                        "Import/job001",
                        str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                    ),
                    (
                        "Import/job001",
                        str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                    ),
                    ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
                    ("Import/mtffile", str(Path(self.test_data) / "mtf_k2_200kV.star")),
                ],
                expected_outfiles=(
                    "run.out",
                    "run.err",
                    "postprocess.mrc",
                    "postprocess_masked.mrc",
                    "logfile.pdf",
                ),
                overwrite_jobname=job_run.name,
            )

    def test_postprocess_skipfsc(self):
        job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_skipfsc.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    def test_postprocess_withmtf(self):
        job_running_test(
            test_jobfile=str(self.jobfiles / "PostProcess/postprocess_adhocbf.job"),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
                ("Import/mtffile", str(Path(self.test_data) / "mtf_k2_200kV.star")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    @patch("pipeliner.utils.subprocess_popen")
    @patch.object(sys, "executable", "/fake/python/exe/for/testing")
    def test_mock_run_dummy_job(self, mock_subprocess_popen):
        job = DummyJob()
        job_dir = Path("Test/job001")

        mock_process = Mock()
        mock_process.wait.return_value = 0  # Process has finished successfully
        mock_subprocess_popen.return_value = mock_process

        with ProjectGraph(create_new=True) as pipeline:
            assert not job_dir.is_dir()
            with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
                job_manager.run_job(pipeline, job)
            captured_stdout = redirected_stdout.getvalue()

        assert job_dir.is_dir()
        mock_subprocess_popen.assert_called_with(
            [
                "/fake/python/exe/for/testing",
                str(get_pipeliner_root() / "scripts" / "job_runner.py"),
                "Test/job001/job.star",
            ],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            start_new_session=True,
        )
        assert captured_stdout == ""

    @patch("pipeliner.utils.subprocess_popen")
    @patch.object(sys, "executable", "/fake/exe/for/doppio-web")
    def test_mock_run_dummy_job_in_doppio(self, mock_subprocess_popen):
        job = DummyJob()
        job_dir = Path("Test/job001")

        mock_process = Mock()
        mock_process.wait.return_value = 0  # Process has finished successfully
        mock_subprocess_popen.return_value = mock_process

        with ProjectGraph(create_new=True) as pipeline:
            assert not job_dir.is_dir()
            with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
                job_manager.run_job(pipeline, job)
            captured_stdout = redirected_stdout.getvalue()

        assert job_dir.is_dir()
        mock_subprocess_popen.assert_called_with(
            [
                "/fake/exe/for/doppio-web",
                "run-script",
                str(get_pipeliner_root() / "scripts" / "job_runner.py"),
                "Test/job001/job.star",
            ],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            start_new_session=True,
        )
        assert captured_stdout == ""

    @patch("pipeliner.utils.subprocess_popen")
    @patch.object(sys, "executable", "/fake/python/exe/for/testing")
    @patch.dict(
        os.environ,
        {
            "PIPELINER_QUEUE_USE": "Yes",
            "PIPELINER_QSUB_COMMAND": "qsub",
            "PIPELINER_QSUB_TEMPLATE": "qsub_template.sh",
        },
    )
    def test_mock_run_dummy_job_with_queue(self, mock_subprocess_popen):
        # Make a simple qsub template for testing
        Path("qsub_template.sh").write_text("XXXcommandXXX")

        job = DummyJob()
        job.make_queue_options()
        job_dir = Path("Test/job001")

        mock_process = Mock()
        mock_process.wait.return_value = 0  # Process has finished successfully
        mock_subprocess_popen.return_value = mock_process

        with ProjectGraph(create_new=True) as pipeline:
            assert not job_dir.is_dir()
            with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
                job_manager.run_job(pipeline, job)
            captured_stdout = redirected_stdout.getvalue()

        assert job_dir.is_dir()
        mock_subprocess_popen.assert_called_with(
            ["qsub", "Test/job001/run_submit.script"],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            start_new_session=True,
        )
        assert captured_stdout == ""
        qsub_script = job_dir / "run_submit.script"
        job_runner = str(get_pipeliner_root() / "scripts" / "job_runner.py")
        assert (
            qsub_script.read_text()
            == f"/fake/python/exe/for/testing {job_runner} Test/job001/job.star"
        )

    @patch("pipeliner.utils.subprocess_popen")
    @patch.dict(
        os.environ,
        {
            "PIPELINER_QUEUE_USE": "Yes",
            "PIPELINER_QSUB_COMMAND": "qsub",
            "PIPELINER_QSUB_TEMPLATE": "qsub_template.sh",
        },
    )
    def test_mock_run_dummy_job_qsub_command_not_found(self, mock_subprocess_popen):
        # Make a simple qsub template for testing
        Path("qsub_template.sh").write_text("XXXcommandXXX")

        mock_process = Mock()
        mock_process.wait.return_value = None  # Process is running
        mock_subprocess_popen.return_value = mock_process

        # Simulate a missing qsub command
        underlying_error = FileNotFoundError
        mock_subprocess_popen.side_effect = underlying_error

        job = DummyJob()
        job.make_queue_options()
        job_dir = Path("Test/job001")

        with ProjectGraph(create_new=True) as pipeline:
            assert not job_dir.is_dir()
            with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
                with self.assertRaises(RuntimeError) as cm:
                    job_manager.run_job(pipeline, job)
                    assert cm.exception.__cause__ is underlying_error
            captured_stdout = redirected_stdout.getvalue()
            assert pipeline.find_process("Test/job001/").status == JOBSTATUS_FAIL

        assert job_dir.is_dir()
        assert captured_stdout == ""

    @patch("pipeliner.utils.subprocess_popen")
    @patch.dict(
        os.environ,
        {
            "PIPELINER_QUEUE_USE": "Yes",
            "PIPELINER_QSUB_COMMAND": "qsub",
            "PIPELINER_QSUB_TEMPLATE": "qsub_template.sh",
        },
    )
    def test_mock_run_dummy_job_qsub_immediate_fail(self, mock_subprocess_popen):
        # Make a simple qsub template for testing
        Path("qsub_template.sh").write_text("XXXcommandXXX")

        job = DummyJob()
        job.make_queue_options()
        job_dir = Path("Test/job001")

        mock_process = Mock()
        mock_process.wait.return_value = -1  # Process has failed
        mock_subprocess_popen.return_value = mock_process

        with ProjectGraph(create_new=True) as pipeline:
            assert not job_dir.is_dir()
            with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
                with self.assertRaises(RuntimeError):
                    job_manager.run_job(pipeline, job)
            captured_stdout = redirected_stdout.getvalue()

        assert job_dir.is_dir()
        assert captured_stdout == ""

    def test_abort_nonexistent_job(self):
        with ProjectGraph(create_new=True) as pipeline:
            with self.assertRaises(ValueError):
                job_manager.abort_job(pipeline, "Test/job001")

    def test_abort_finished_job(self):
        job = DummyJob()
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)
        job.output_dir = os.fspath(job_dir) + "/"

        with ProjectGraph(create_new=True) as pipeline:
            process = pipeline.add_job(job, as_status="Succeeded", do_overwrite=False)
            with self.assertRaises(RuntimeError):
                job_manager.abort_job(pipeline, process.name)

    def test_abort_scheduled_job(self):
        job = DummyJob()
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)
        job.output_dir = os.fspath(job_dir) + "/"

        with ProjectGraph(create_new=True) as pipeline:
            process = pipeline.add_job(job, as_status="Scheduled", do_overwrite=False)
            with self.assertRaises(RuntimeError):
                job_manager.abort_job(pipeline, process.name)

    def test_abort_aborted_job(self):
        job = DummyJob()
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)
        job.output_dir = os.fspath(job_dir) + "/"

        with ProjectGraph(create_new=True) as pipeline:
            process = pipeline.add_job(job, as_status="Aborted", do_overwrite=False)
            with self.assertRaises(RuntimeError):
                job_manager.abort_job(pipeline, process.name)

    @staticmethod
    def test_abort_running_job():
        job = DummyJob()
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)
        job.output_dir = os.fspath(job_dir) + "/"

        # Check that the abort signal is made properly
        with ProjectGraph(create_new=True) as pipeline:
            process = pipeline.add_job(job, as_status="Running", do_overwrite=False)
            job_manager.abort_job(pipeline, process.name)

        assert (job_dir / "PIPELINER_JOB_ABORT_NOW").is_file()

        # Now simulate the job aborting
        (job_dir / "PIPELINER_JOB_EXIT_ABORTED").touch()

        with ProjectGraph(read_only=False) as pipeline:
            process = pipeline.find_process(job.output_dir)
            assert process.status == "Running"
            pipeline.check_process_completion()
            assert process.status == "Aborted"

    @staticmethod
    def make_new_job():
        job = DummyJob()
        job.output_dir = "Test/job001/"
        os.makedirs("Test/job001")
        return job

    def test_checking_wait_for_job_to_finish_succeeded(self):
        job = self.make_new_job()
        touch(os.path.join(job.output_dir, SUCCESS_FILE))
        assert job_manager.wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS

    def test_checking_wait_for_job_to_finish_failed(self):
        job = self.make_new_job()
        touch(os.path.join(job.output_dir, FAIL_FILE))
        assert job_manager.wait_for_job_to_finish(job) == JOBSTATUS_FAIL

    def test_checking_wait_for_job_to_finish_failed_with_error(self):
        job = self.make_new_job()
        touch(os.path.join(job.output_dir, FAIL_FILE))
        with self.assertRaises(RuntimeError):
            job_manager.wait_for_job_to_finish(job, error_on_fail=True)

    def test_checking_wait_for_job_to_finish_aborted(self):
        job = self.make_new_job()
        touch(os.path.join(job.output_dir, ABORT_FILE))
        assert job_manager.wait_for_job_to_finish(job) == JOBSTATUS_ABORT

    def test_checking_wait_for_job_to_finish_aborted_with_error(self):
        job = self.make_new_job()
        touch(os.path.join(job.output_dir, ABORT_FILE))
        with self.assertRaises(RuntimeError):
            job_manager.wait_for_job_to_finish(job, error_on_abort=True)

    def test_checking_wait_for_job_to_finish_timeout(self):
        job = self.make_new_job()
        with self.assertRaises(RuntimeError):
            job_manager.wait_for_job_to_finish(job, timeout=0.1)


if __name__ == "__main__":
    unittest.main()
