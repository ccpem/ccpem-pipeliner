#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import shlex
import shutil
import os
import unittest
import subprocess
import json
import warnings
from glob import glob
from pathlib import Path
from typing import Mapping, Sequence, Tuple, List, Dict, Optional, Union, Type, Literal
from contextlib import contextmanager
from functools import wraps
from gemmi import cif

from pipeliner import job_factory, job_manager
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE, RELION_GENERAL_PROCS
from pipeliner.project_graph import ProjectGraph
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.utils import touch, compare_nested_lists
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.starfile_handler import JobStar, StarFile
from pipeliner.runjob_reader import RunJobFile
from pipeliner_tests import test_data

test_data_dir = os.path.dirname(test_data.__file__)


def enforce_decorator_with_args(decorator_func):
    """Forces the decorator to be called with arguments"""

    @wraps(decorator_func)
    def wrapper(*args, **kwargs):
        if len(args) == 1 and callable(args[0]) and not kwargs:
            raise ValueError("Decorator must be called with arguments")
        return decorator_func(*args, **kwargs)

    return wrapper


@enforce_decorator_with_args
def live_test(
    job: Optional[str] = None,
    jobs: Optional[List[str]] = None,
    programs: Optional[List[str]] = None,
    condition: Optional[bool] = None,
):
    """Decorator for tests that require jobs to actually run

    Only run if env PIPELINER_TEST_LIVE is set and all the specified criteria are met

    Args:
        job (str): This job must be available for the test to be run
        jobs (List[str]): A list of jobs if multiple job types are required
        programs (List[str]): Only run the test if these specific programs are available
            generally jobs should be used instead of this whenever possible
        condition: (bool): Only run the test if this is True

    Raises:
        unittest.SkipTest: If the criteria to run the live test are not met
    """

    if all([x is None for x in (job, jobs, programs, condition)]):
        raise ValueError(
            "@live_test decorator must be called with at least one arg: jobs,"
            " programs, or condition"
        )

    def decorator(test_func):
        @wraps(test_func)
        def wrapper(*args, **kwargs):
            if not os.environ.get("PIPELINER_TEST_LIVE"):
                raise unittest.SkipTest("Live tests disabled")
            test_jobs = jobs if jobs is not None else []
            if job is not None:
                test_jobs.append(job)
            if test_jobs:
                job_errors = []
                for check_job in test_jobs:
                    can_run = job_factory.job_can_run(check_job)
                    if not can_run:
                        job_errors.append(check_job)
                if job_errors:
                    raise unittest.SkipTest(
                        f"Job(s) unavailable: {', '.join(job_errors)}"
                    )
            if programs:
                bad_progs = []
                for prog in programs:
                    if not shutil.which(prog):
                        bad_progs.append(prog)
                if bad_progs:
                    raise unittest.SkipTest(
                        f"Program(s) not available: {', '.join(bad_progs)}"
                    )
            if condition is not None and not condition:
                raise unittest.SkipTest("Condition for test not met")

            return test_func(*args, **kwargs)

        return wrapper

    return decorator


def slow_test(func):
    """Decorator for tests that take a long time to run

    Only run the test if env var PIPELINER_TEST_SLOW is set

    Raises:
        unittest.SkipTest: If slow tests are disabled

    """

    def wrapper(*args, **kwargs):
        if not os.environ.get("PIPELINER_TEST_SLOW"):
            raise unittest.SkipTest("Slow tests disabled")

        return func(*args, **kwargs)

    return wrapper


class DummyJob(PipelinerJob):
    """A dummy pipeliner job for testing purposes.

    The default commands can be overridden if required.
    """

    PROCESS_NAME = "dummyjob"
    OUT_DIR = "Test"

    def __init__(
        self,
        queue_enabled=True,
        do_mpi=True,
        do_threads=True,
        commands: Optional[List[PipelinerCommand]] = None,
    ):
        super().__init__()
        self.commands = commands

        self.jobinfo.programs = [ExternalProgram("touch")]

        if queue_enabled:
            self.make_queue_options()
        self.get_runtab_options(do_mpi, do_threads)

    def create_output_nodes(self):
        self.add_output_node("test_file.txt", "TestFile", ["txt", "dummyjob"])

    def get_commands(self) -> List[PipelinerCommand]:
        if self.commands is not None:
            return self.commands
        com1 = ["touch", os.path.join(self.output_dir, "test_file.txt")]
        com2 = ["touch", os.path.join(self.output_dir, "post_run.txt")]
        com3 = ["echo", "echo command wrote this line"]
        return [PipelinerCommand(com1), PipelinerCommand(com2), PipelinerCommand(com3)]

    def create_post_run_output_nodes(self):
        self.add_output_node("post_run.txt", "TestFile", ["post_run", "dummyjob"])


def get_pipeliner_tests_root() -> Path:
    return Path(__file__).parent


def make_command_comparison(actual_commands, expected_commands) -> str:
    comdif = len(actual_commands) - len(expected_commands)
    if comdif < 0:
        actual_commands.extend(["X"] * (-1 * comdif))
    if comdif > 0:
        expected_commands.extend(["X"] * comdif)

    comp_str = ""
    for line in zip(actual_commands, expected_commands):
        comp_str += f"\n{line[0]}\n{line[1]}\n"
        comp_com = ""
        for i in range(len(line[0])):
            try:
                if line[0][i] == line[1][i]:
                    comp_com += " "
                else:
                    comp_com += "*"
            except IndexError:
                pass
        comp_str += comp_com + "\n"
    return comp_str


def print_coms(commands, expected_commands):
    print(make_command_comparison(commands, expected_commands))


def get_file_structure() -> List[List[Sequence[str]]]:
    tree = []
    for root, dirs, files in os.walk(os.getcwd()):
        tree.append([root, dirs, files])
    return tree


def check_for_file_structure_changes(pre_tree, post_tree):
    created, deleted, create_errors, delete_errors = [], [], [], []
    for n, fd in enumerate(post_tree):
        if fd not in pre_tree:
            try:
                created.append([fd, pre_tree[n]])
            except IndexError:
                created.append([fd, [[], [], []]])

    for i in created:
        for cdir in i[0][1]:
            if cdir not in i[1][1]:
                create_errors.append(os.path.join(i[0][0], cdir))
        for cfile in i[0][2]:
            if cfile not in i[1][2]:
                create_errors.append(os.path.join(i[0][0], cfile))

    for n, fd in enumerate(pre_tree):
        if fd not in post_tree:
            deleted.append([fd, post_tree[n]])

    for i in deleted:
        for cdir in i[0][1]:
            if cdir not in i[1][1]:
                delete_errors.append(os.path.join(i[0][0], cdir))
        for cfile in i[0][2]:
            if cfile not in i[1][2]:
                delete_errors.append(os.path.join(i[0][0], cfile))
    assert not create_errors, f"get_commands created files/dirs: {create_errors}"
    assert not delete_errors, f"get_commands deleted files/dirs: {delete_errors}"


class ShortpipeFileStructure(object):
    def __init__(self, procs_to_make):
        """Builds the entire file structure for
        test_data/Pipelines/short_full_pipeline.star

        Input a list of processes or 'all' to make the entire pipeline.
        """

        # only making the first few .Nodes dirs for testing deletion later
        self.nodes_files = [
            ".Nodes/0/Import/job001/movies.star",
            ".Nodes/13/MotionCorr/job002/logfile.pdf",
            ".Nodes/1/MotionCorr/job002/corrected_micrographs.star",
            ".Nodes/13/CtfFind/job003/logfile.pdf",
            ".Nodes/1/CtfFind/job003/micrographs_ctf.star",
        ]

        self.nodes_dirs = [
            ".Nodes/0/Import/job001",
            ".Nodes/13/MotionCorr/job002",
            ".Nodes/1/MotionCorr/job002",
            ".Nodes/13/CtfFind/job003",
            ".Nodes/1/CtfFind/job003",
        ]

        self.common_files = [
            "default_pipeline.star",
            "run.job",
            "note.txt",
            SUCCESS_FILE,
            "run.err",
            "run.out",
        ]

        self.loop_procs = {
            "MotionCorr": [
                "_Fractions0-Patch-FitCoeff.log",
                "_Fractions0-Patch-Frame.log",
                "_Fractions0-Patch-Full.log",
                "_Fractions0-Patch-Patch.log",
                "_Fractions.com",
                "_Fractions.err",
                "_Fractions.mrc",
                "_Fractions.out",
                "_Fractions_shifts.eps",
                "_Fractions.star",
            ],
            "CtfFind": [
                "_Fractions.mrc",
                "_Fractions.pow",
                "_Fractions.ctf",
                "_Fractions.gctf.log",
            ],
            "AutoPick": ["_Fractions_autopick.star", "_Fractions_autopick.spi"],
            "Extract": ["_Fractions.mrcs", "_Fractions_extract.star"],
            "Class2D": [
                "_data.star",
                "_model.star",
                "_classes.mrcs",
                "_optimiser.star",
                "_sampling.star",
            ],
            "Class3D": [
                "_class001.mrc",
                "_class001_angdist.bild",
                "_class002.mrc",
                "_class002_angdist.bild",
                "_class003.mrc",
                "_class003_angdist.bild",
                "_sampling.star",
                "_data.star",
                "_model.star",
                "_optimiser.star",
            ],
            "Refine3D": [
                "_half1_class001.mrc",
                "_half1_class001_angdist.bild",
                "_half2_class001.mrc",
                "_half2_class001_angdist.bild",
                "_half2_model.star",
                "_half1_model.star",
                "_sampling.star",
                "_data.star",
                "_optimiser.star",
            ],
            "InitialModel": [
                "_data.star",
                "_class001.mrc",
                "_grad001.mrc",
                "_class002.mrc",
                "_grad002.mrc",
                "_sampling.star",
                "_class001_data.star",
                "_class002_data.star",
                "_model.star",
                "_optimiser.star",
            ],
            # need to add optimiser files here
            "MultiBody": [
                "_data.star",
                "_half1_body001_angdist.bild",
                "_half1_body001.mrc",
                "_half1_body002_angdist.bild",
                "_half1_body002.mrc",
                "_half1_model.star",
                "_half2_body001_angdist.bild",
                "_half2_body001.mrc",
                "_half2_body002_angdist.bild",
                "_half2_body002.mrc",
                "_half2_model.star",
            ],
            "CtfRefine": [
                "_fit.star",
                "_fit.eps",
                "_wAcc.mrc",
                "_xyAcc_real.mrc",
                "_xyAcc_imag.mrc",
            ],
            "Polish": [
                "_FCC_cc.mrc",
                "_FCC_w0.mrc",
                "_FCC_w1.mrc",
                "_shiny.mrcs",
                "_shiny.star",
                "_tracks.eps",
                "_tracks.star",
            ],
            "Subtract": ["star", "_opticsgroup1.mrcs", "_opticsgroup2.mrcs"],
        }

        self.single_files = {
            "MotionCorr": [
                "corrected_micrographs_all_rlnAccumMotionEarly.eps",
                "corrected_micrographs_all_rlnAccumMotionLate.eps",
                "corrected_micrographs_all_rlnAccumMotionTotal.eps",
                "corrected_micrographs_hist_rlnAccumMotionEarly.eps",
                "corrected_micrographs_hist_rlnAccumMotionLate.eps",
                "corrected_micrographs_hist_rlnAccumMotionTotal.eps",
                "corrected_micrographs.star",
                "logfile.pdf",
                "logfile.pdf.lst",
            ],
            "CtfFind": [
                "gctf0.err",
                "gctf0.out",
                "gctf1.err",
                "gctf1.out",
                "micrographs_ctf_all_rlnCtfAstigmatism.eps",
                "micrographs_ctf_all_rlnCtfFigureOfMerit.eps",
                "micrographs_ctf_all_rlnDefocusAngle.eps",
                "micrographs_ctf_all_rlnDefocusU.eps",
                "micrographs_ctf_hist_rlnCtfAstigmatism.eps",
                "micrographs_ctf_hist_rlnCtfFigureOfMerit.eps",
                "micrographs_ctf_hist_rlnDefocusAngle.eps",
                "micrographs_ctf_hist_rlnDefocusU.eps",
                "micrographs_ctf.star",
                "logfile.pdf",
            ],
            "Select": [
                "backup_selection.star",
                "particles.star",
                "class_averages.star",
            ],
            "Refine3D": [
                "run_class001.mrc",
                "run_class001_angdist.bild",
                "run_model.star",
                "run_sampling.star",
                "run_class001_half1_unfil.mrc",
                "run_class001_half2_unfil.mrc",
                "run_optimiser.star",
            ],
            "MultiBody": [
                "run_bodies.bild",
                "run_body001_angdist.bild",
                "run_body001_mask.mrc",
                "run_body001.mrc",
                "run_body002_angdist.bild",
                "run_body002_mask.mrc",
                "run_body002.mrc",
                "run_data.star",
                "run_half1_body001_unfil.mrc",
                "run_half1_body002_unfil.mrc",
                "run_half2_body001_unfil.mrc",
                "run_half2_body002_unfil.mrc",
                "run_optimiser.star",
            ],
            "CtfRefine": [
                "aberr_delta-phase_iter-fit_optics-group_1_N-4.mrc",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4.mrc",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4_residual.mrc",
                "aberr_delta-phase_per-pixel_optics-group_1.mrc",
                "asymmetric_aberrations_optics-group_1.eps",
                "beamtilt_delta-phase_iter-fit_optics-group_1_N-3.mrc",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3.mrc",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3_residual.mrc",
                "beamtilt_delta-phase_per-pixel_optics-group_1.mrc]",
                "particles_ctf_refine.star",
            ],
            "MaskCreate": ["mask.mrc.old", "mask.mrc"],
            "Polish": [
                "bfactors.eps",
                "bfactors.star",
                "scalefactors.eps",
                "shiny.star",
            ],
            "PostProcess": [
                "postprocess_fsc.eps",
                "postprocess_fsc.xml",
                "postprocess_guinier.eps",
                "postprocess_masked.mrc",
                "postprocess.mrc",
                "postprocess.star",
                "logfile.pdf",
            ],
            "LocalRes": [
                "relion_locres.mrc",
                "relion_locres_filtered.mrc",
                "relion_locres_fscs.star]",
            ],
        }

        self.jobstar_files = {
            "Import/job001/": "import_job.star",
            "MotionCorr/job002/": "motioncorr_own_job.star",
            "CtfFind/job003/": "ctffind_job.star",
            "AutoPick/job004/": "autopick_job.star",
            "Extract/job005/": "extract_job.star",
            "Class2D/job006/": "class2D_job.star",
            "Select/job007/": "select_interactive_averages_job.star",
            "InitialModel/job008/": "initialmodel_job.star",
            "Class3D/job009/": "class3D_job.star",
            "Refine3D/job010/": "refine3D_job.star",
            "MultiBody/job011/": "multibody_job.star",
            "CtfRefine/job012/": "ctfrefine_everything_job.star",
            "MaskCreate/job013/": "maskcreate_job.star",
            "Polish/job014/": "bayesianpolish_job.star",
            "JoinStar/job015/": "joinstar_mics_relionstyle_job.star",
            "Subtract/job016/": "subtract_addarg_job.star",
            "PostProcess/job017/": "postprocess_job.star",
            "External/job018/": "external_job.star",
            "LocalRes/job019/": "localres_relion_job.star",
        }
        self.outfiles = {}
        self.creation_functions = {
            "LocalRes": self.fs_localres,
            "Import": self.fs_import,
            "MotionCorr": self.fs_motioncorr,
            "CtfFind": self.fs_ctffind,
            "AutoPick": self.fs_autopick,
            "Extract": self.fs_extract,
            "Class2D": self.fs_class2d,
            "Select": self.fs_select,
            "InitialModel": self.fs_inimodel,
            "Class3D": self.fs_class3d,
            "Refine3D": self.fs_refine3d,
            "MultiBody": self.fs_multibody,
            "CtfRefine": self.fs_ctfrefine,
            "MaskCreate": self.fs_maskcreate,
            "Polish": self.fs_polish,
            "JoinStar": self.fs_joinstar,
            "Subtract": self.fs_subtract,
            "PostProcess": self.fs_postprocess,
            "External": self.fs_external,
        }
        for proc in RELION_GENERAL_PROCS:  # PROCS will be deprecated
            if proc in procs_to_make or procs_to_make == ["all"]:
                funct = self.creation_functions.get(proc)
                if funct:
                    funct(proc)

    # specific functions for creating specific jobs
    def fs_import(self, proc):
        jobdir = proc + "/job001/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["movies.star"], self.outfiles[proc])

    def fs_motioncorr(self, proc):
        jobdir = proc + "/job002/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_ctffind(self, proc):
        jobdir = proc + "/job003/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_data = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_data,
            self.loop_procs["CtfFind"],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_autopick(self, proc):
        jobdir = proc + "/job004/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(
            jobdir,
            ["coords_suffix_autopick.star", "logfile.pdf"],
            self.outfiles[proc],
        )
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_extract(self, proc):
        jobdir = proc + "/job005/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["particles.star"], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_class2d(self, proc):
        jobdir = proc + "/job006/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            25,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_select(self, proc):
        jobdir = proc + "/job007/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_inimodel(self, proc):
        jobdir = proc + "/job008/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            150,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_class3d(self, proc):
        jobdir = proc + "/job009/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            25,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_refine3d(self, proc):
        jobdir = proc + "/job010/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            16,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_multibody(self, proc):
        jobdir = proc + "/job011/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            12,
            "run_it*",
            self.outfiles[proc],
        )
        for i in range(0, 6):
            h = "analyse_component{:03d}_histogram.eps".format(i)
            hh = os.path.join(jobdir, h)
            touch(hh)
            self.outfiles[proc].append(hh)

            for j in range(1, 11):
                f = "analyse_component{:03d}_bin{:03d}.mrc".format(i, j)
                ff = os.path.join(jobdir, f)
                touch(ff)
                self.outfiles[proc].append(ff)
        shutil.copy(
            Path(test_data_dir) / "JobFiles/MultiBody/bodyfile.star", "bodyfile.star"
        )

    def fs_ctfrefine(self, proc):
        jobdir = proc + "/job012/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files["CtfRefine"], self.outfiles[proc])
        raw_data = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_data,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_maskcreate(self, proc):
        jobdir = proc + "/job013/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_polish(self, proc):
        jobdir = proc + "/job014/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_joinstar(self, proc):
        jobdir = proc + "/job015/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["join_particles.star"], self.outfiles[proc])

    def fs_subtract(self, proc):
        jobdir = proc + "/job016/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["particles_subtract.star"], self.outfiles[proc])
        part_dir = jobdir + "Particles"
        self.make_loopfiles(
            part_dir,
            self.loop_procs[proc],
            10,
            "subtracted_rank*",
            self.outfiles[proc],
        )

    def fs_postprocess(self, proc):
        jobdir = proc + "/job017/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_external(self, proc):
        jobdir = proc + "/job018/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1], [".file"], 10, "External_file_*", self.outfiles[proc]
        )

    def fs_localres(self, proc):
        jobdir = proc + "/job019/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def make_files(self, jobdir, filelist, out_list):
        os.makedirs(jobdir)

        for node_dir in self.nodes_dirs:
            os.makedirs(node_dir, exist_ok=True)

        for f in self.nodes_files:
            touch(f)
        for f in self.common_files:
            touch(os.path.join(jobdir, f))

        for f in filelist:
            ff = os.path.join(jobdir, f)
            touch(ff)
            out_list.append(ff)
        # copy in an actual job.star file needed for cleanup
        jobstar_dir = Path(test_data_dir) / "JobFiles"
        jobstar_file = f"{jobdir.split('/')[0]}/{self.jobstar_files[jobdir]}"
        jobstar = jobstar_dir / jobstar_file
        shutil.copy(jobstar, os.path.join(jobdir, "job.star"))
        out_list.append(os.path.join(jobdir, "job.star"))

    @staticmethod
    def make_loopfiles(writedir, file_list, n, fn, out=None):
        os.makedirs(writedir, exist_ok=True)

        for i in range(1, n + 1):
            for f in file_list:
                ff = writedir + "/" + fn.replace("*", "{:03d}".format(i)) + f
                touch(ff)
                if out:
                    out.append(ff)


def make_conversion_file_structure():
    """Make all the jobfiles and dirs for testing pipeline conversion
    Each process needs a directory with a job.star file in it"""
    test_data_loc = get_pipeliner_tests_root()
    files_path = os.path.join(test_data_loc, "test_data/JobFiles/Tutorial_Pipeline/")
    jobfiles = glob(files_path + "*_job.star")
    for f in jobfiles:
        fsplit = os.path.basename(f).split("_")
        target_dir = fsplit[0] + "/" + fsplit[1]
        os.makedirs(target_dir)
        shutil.copy(f, os.path.join(target_dir, "job.star"))


@contextmanager
def expected_warning(
    type_: Optional[Type[Warning]] = None, msg: str = "", nwarn: int = 1
):
    """Simple context manager to catch a single warning (or multiple copies of the same
    warning) and check that it contains a given message.

    It can also be used to check that no warnings are issued by setting ``nwarn = 0``.

    ``type_`` must be given if ``nwarn`` is greater than zero.

    Args:
        type_ (type): the type of the warning, for example RuntimeWarning
        msg (str): optional string to check for in the warning message
        nwarn (bool): number of warnings expected
    """
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("default")
        yield
        print([x.__dict__ for x in w])
        assert len(w) == nwarn, len(w)
        if nwarn > 0:
            assert type_ is not None
            assert issubclass(w[-1].category, type_), (
                w[-1].category,
                type_,
                str(w[-1]),
            )
            assert msg in str(w[-1].message), (str(w[-1].message), msg)


def get_relion_tutorial_data_path(relion_version: int) -> str:
    """Check if the relion tutorial data is available

    Args:
        relion_version (int): Which version of relion, only 4 or 5 is allowed

    Returns:
        str: the dir for containing the tutorial data

    Raises:
        unittest.SkipTest: if the tutorial data is not available
        ValueError: If relion_version is not 4 or 5
    """
    if relion_version == 4:
        tutorial_data = os.environ.get("PIPELINER_RELION4_TUTORIAL_DATA")
        if tutorial_data is None:
            raise unittest.SkipTest("Relion4 tutorial data env var not set")
        if not Path(tutorial_data).is_dir():
            raise ValueError("Tutorial data specified is not a dir")
        return tutorial_data
    elif relion_version == 5:
        tutorial_data = os.environ.get("PIPELINER_RELION5_TUTORIAL_DATA")
        if tutorial_data is None:
            raise unittest.SkipTest("Relion5 tutorial data env var not set")
        if not Path(tutorial_data).is_dir():
            raise ValueError("Tutorial data specified is not a dir")
        return tutorial_data
    else:
        raise ValueError("Invalid relion tutorial dataset name")


def get_relion_tutorial_data(
    relion_version: int,
    dirs: Optional[Union[list, str]] = None,
    do_pipeline: bool = True,
):
    """Copy in parts of the relion tutorial so it doesn't need
    to be included with the pipeliner

    Args:
        relion_version (int): Which version of relion, only 4 or 5 is allowed
        dirs (list): Which dirs to copy in, if `None` all will be done
        do_pipeline (bool): Don't copy over the default_pipeline.star file

    Raises:
        unittest.SkipTest: if the tutorial data is not available
    """

    tutorialdir = get_relion_tutorial_data_path(relion_version)
    dirs = [] if dirs is None else dirs
    if tutorialdir[-1] != "/":
        tutorialdir += "/"
    for d in os.walk(tutorialdir):
        dname = d[0].replace(tutorialdir, "")
        docopy = dname.split("/")[0] in dirs or dirs == []
        if not os.path.isdir(dname) and dname != "" and docopy:
            os.makedirs(dname)
        if len(d[2]) > 0 and docopy:
            for f in d[2]:
                fn = os.path.join(d[0], f)
                if os.path.isfile(fn) and f[0] != ".":
                    ln = fn.replace(tutorialdir, "")
                    os.symlink(fn, ln)
    # copy in the default pipeline
    if do_pipeline:
        if os.path.islink("default_pipeline.star"):
            os.unlink("default_pipeline.star")
        shutil.copyfile(
            os.path.join(tutorialdir, "default_pipeline.star"), "default_pipeline.star"
        )
    else:
        os.unlink("default_pipeline.star")


def job_running_test(
    *,
    test_jobfile: str,
    input_files: Sequence[Tuple[str, str]],
    expected_outfiles: Sequence[str],
    expected_search_files: Optional[Dict[str, int]] = None,
    success: bool = True,
    overwrite_jobname: Optional[str] = None,
    print_run: bool = False,
    print_err: bool = False,
    show_contents: bool = False,
):
    """Runs a job and checks that the expected outputs are produced

    Args:
        test_jobfile (str): The name of the file containing the running parameters
            for the job. A run.job or job.star file
        input_files (list):  A list of tuples for input files to the job (Directory
            the files should be in for the test, path to the file to copy)
        expected_outfiles (list): List of expected output files.  Only needs the
            path past the output dir of the job IE: `Import/job001/movies/test.mrc`
            should be `movies/test.mrc`
        expected_search_files (dict): A dict of file search strings and how many files
            are expected to be found for that search string
        success (bool): Do you expect the job to be successful
        overwrite_jobname (str): Overwrite this existing job when running the job. If
            `None` creates the job as job998
        print_run (bool): Display the contents of the run.job file, for debugging
        print_err (bool): Display the contents of the run.err file, for debugging
        show_contents (bool): Show the contents of the output directory, for debugging

    Raises:
        unittest.SkipTest: If the ``PIPELINER_TEST_LIVE`` environment variable is not
            set, or if the job type specified in the ``test_jobfile`` file is not
            available.
    """
    # check if the test should be run
    if not os.environ.get("PIPELINER_TEST_LIVE"):
        raise unittest.SkipTest("Live tests disabled")

    if Path(test_jobfile).suffix == ".star":
        jobtype = JobStar(test_jobfile).jobtype
    else:
        jobtype = RunJobFile(test_jobfile).get_all_options()["_rlnJobTypeLabel"]
    can_run = job_factory.job_can_run(jobtype)
    if not can_run:
        raise unittest.SkipTest(f"{jobtype} job unavailable")

    # Prepare the directory structure as if previous jobs have been run:
    if expected_search_files is None:
        expected_search_files = {}

    for infile in input_files:
        import_dir = infile[0]
        if import_dir:
            os.makedirs(import_dir, exist_ok=True)
            shutil.copy(infile[1], import_dir)
        else:
            shutil.copy(infile[1], Path(infile[1]).name)

    with ProjectGraph(read_only=False, create_new=not overwrite_jobname) as pipeline:
        pipeline.job_counter = 998
        job = job_factory.read_job(test_jobfile)
        job_dir = os.path.join(job.OUT_DIR, "job998/")

        if overwrite_jobname is not None:
            overwrite_proc = pipeline.find_process(overwrite_jobname)
        else:
            overwrite_proc = None

        extra_files = [
            "job.star",
            "note.txt",
            "job_pipeline.star",
            "run.out",
            "run.err",
        ]

        if success:
            extra_files.append(SUCCESS_FILE)
        else:
            extra_files.append(FAIL_FILE)

        # Validate job options first to avoid job running failures due to timeouts in
        # job options that make web requests for validation.
        validation_results = job.validate_joboptions()
        problems = [
            str(result)
            for result in validation_results
            if result.type in ("error", "warning")
        ]
        if problems:
            probs_str = "\n".join(problems)
            print(
                "Some job options are invalid. This should normally make the job fail,"
                " unless the validation failure is a simple web request timeout."
                "\nIf the job still succeeds, the validation code should be checked to"
                " make sure it corresponds to the true requirements of the job."
                f"\nThe problems were:\n{probs_str}"
            )

        job_run = job_manager.run_job(
            pipeline=pipeline,
            job=job,
            overwrite=overwrite_proc,
            run_in_foreground=True,
            ignore_invalid_joboptions=True,
        )

    if print_run:
        print((Path(job_dir) / "run.out").read_text())
    if print_err:
        print((Path(job_dir) / "run.err").read_text())

    if show_contents:
        subprocess.run(["ls", "-al"])
        subprocess.run(["ls", "-al", job_dir])
        print(Path("default_pipeline.star").read_text())
        print((Path(job_dir) / "job.star").read_text())
        print((Path(job_dir) / "run.job").read_text())
        print((Path(job_dir) / "job_pipeline.star").read_text())

        # test for output files
    assert os.path.isdir(job_dir)
    for outfile in expected_outfiles:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for outfile in extra_files:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for search_string in expected_search_files:
        ff = glob(os.path.join(job_dir, search_string))
        assert len(ff) == expected_search_files[search_string]

    assert os.path.isfile(".gui_" + job.PROCESS_NAME.replace(".", "_") + "job.star")

    return job_run


def job_generate_commands_test(
    *,
    jobfile: str,
    input_nodes: Mapping[str, str],
    output_nodes: Mapping[str, str],
    expected_commands: Sequence[str],
    show_coms: bool = False,
    show_inputnodes: bool = False,
    show_outputnodes: bool = False,
) -> PipelinerJob:
    """Test that executing the get command function returns the expected
    commands and input and output nodes.

    If the commands don't match, a comparison of the expected and actual
    commands will be displayed

    The pipeliner's check completion command will automatically be added,
    as well as some of the wrapping that the pipeliner performs

    Note that this test will not work correctly for jobs that are set to
    be submitted to a queue. That functionality is tested elsewhere.

    Args:
        jobfile (str): The file containing the running parameters for the job.
            job.star or run.job format
        input_nodes (dict): The input nodes expected to be created by the job
            `{node file path: nodetype}`
        output_nodes (dict): The output nodes expected to be created by the job
            Only needs the path past the output dir of the job IE:
            `Import/job001/movies/test.mrc` should be `movies/test.mrc`.
            {node file, nodetype}
        expected_commands (list): The commands the job is expected to generate
            each command string should be one item in the list
        show_coms (bool): Show the commands that were generated, for debugging
        show_inputnodes (bool): Show the input nodes that would be created, for
            debugging
        show_outputnodes (bool): Show the output nodes that would be created,
            for debugging

    Returns:
        PipelinerJob: The job that was created for the command generation
    """
    job = job_factory.read_job(jobfile)

    output_dir = os.path.join(job.OUT_DIR, "job999/")
    job.output_dir = output_dir

    # get the current state of the directory for comparison
    pre_tree = get_file_structure()

    # Change job option values as if Doppio user-uploaded files have been moved
    job.handle_doppio_uploads(dry_run=True)
    job.create_input_nodes()

    commands = job.get_final_commands()
    command_strings = [shlex.join(x) for x in commands]

    # Check that get_commands() has not made any output nodes
    assert len(job.output_nodes) == 0, (
        "Jobs should not create output nodes in their get_commands() function."
        " Use create_output_nodes() instead."
    )

    # Now actually make the output nodes for comparison with the expected ones
    job.create_output_nodes()
    all_expected_commands = []
    for command in expected_commands:
        cmd_parts = shlex.split(command)
        exe_path = shutil.which(cmd_parts[0])
        if exe_path is not None:
            cmd_parts[0] = exe_path
        all_expected_commands.append(shlex.join(cmd_parts))

    if show_coms:
        print_coms(command_strings, all_expected_commands)
    if show_inputnodes:
        print("\nINPUT NODES: ({}/{})".format(len(job.input_nodes), len(input_nodes)))
        for i in job.input_nodes:
            print(i.name)
    if show_outputnodes:
        print(
            "\nOUTPUT NODES: ({}/{})".format(len(job.output_nodes), len(output_nodes))
        )
        for i in job.output_nodes:
            print(i.name)

    post_tree = get_file_structure()
    check_for_file_structure_changes(pre_tree, post_tree)

    # make sure the expected nodes have been created
    expected_out_nodes = [
        os.path.join(job.OUT_DIR, f"job999/{x}") for x in output_nodes
    ]
    actual_in_nodes = [x.name for x in job.input_nodes]
    actual_out_nodes = [x.name for x in job.output_nodes]

    assert command_strings == all_expected_commands, make_command_comparison(
        command_strings, all_expected_commands
    )

    for input_node in input_nodes:
        assert input_node in actual_in_nodes, input_node
    for out_node in expected_out_nodes:
        assert out_node in actual_out_nodes, out_node

    # make sure no extra nodes have been produced
    for actual_in_node in actual_in_nodes:
        assert actual_in_node in input_nodes, "EXTRA NODE: " + actual_in_node
    for actual_out_node in actual_out_nodes:
        assert actual_out_node in expected_out_nodes, "EXTRA NODE: " + actual_out_node

    # make sure all of the nodes are of the correct type
    for node in job.input_nodes:
        assert node.type == input_nodes[node.name], (
            node.name,
            "node error expect/act",
            input_nodes[node.name],
            node.type,
        )
    # using the expected list here, but it has already been checked
    for node in job.output_nodes:
        node_name = node.name.replace(job.output_dir, "")
        assert node.type == output_nodes[node_name], (
            node_name,
            "node error expect/act",
            output_nodes[node_name],
            node.type,
        )
    return job


def job_display_object_generation_test(
    *,
    jobfile: str,
    expected_display_objects: List[ResultsDisplayObject],
    files_to_create: Dict[str, str],
    print_dispobj: bool = False,
):
    """Test that a job is creating the expected display objects for the GUI

    Args:
        jobfile (str): The file containing the running parameters for the job.
            job.star or run.job format
        expected_display_objects (List[ResultsDisplayObject]): The expected
            DisplayObjects, in the expected order
        files_to_create (Dict[str, str]): Files that needed for correct
            creation of the results display object.  File name is the key. It along with
            any additional directories will be created in 'JobType/job999' If an actual
            file (with real data) needs to be copied in provide a path in the value. If
            the value is an empty string an empty dummy file will be created.
        print_dispobj (bool): Print out the ResultsDisplyObject that was generated (for
            debugging)
    """
    # create the job
    job = job_factory.read_job(jobfile)
    output_dir = os.path.join(job.OUT_DIR, "job999/")
    job.output_dir = output_dir
    job.create_output_nodes()
    os.makedirs(output_dir)

    # create needed files
    for in_f in files_to_create:
        fdir = os.path.join(output_dir, os.path.dirname(in_f))
        f = os.path.join(job.output_dir, in_f)
        os.makedirs(fdir, exist_ok=True)
        if not files_to_create[in_f]:
            touch(f)
        else:
            shutil.copy(files_to_create[in_f], f)

    # create the display objects
    job.create_post_run_output_nodes()
    dispobjs = job.create_results_display()
    if print_dispobj:
        for dobj in dispobjs:
            print(dobj.__dict__)
    # check the display objects
    assert len(dispobjs) == len(expected_display_objects)
    for n, dispo in enumerate(expected_display_objects):
        for key in dispo.__dict__:
            assert dispo.__dict__[key] == dispobjs[n].__dict__[key], (
                key,
                dispo.__dict__[key],
                dispobjs[n].__dict__[key],
            )


def calculate_differences(actuals, expecteds):
    # Calculates the difference between values from a nested list
    vars_indices = []
    for i, current_value in enumerate(actuals):
        try:
            difference = abs(abs(current_value - expecteds[i]) / current_value)
            print(current_value, expecteds[i], difference)
            vars_indices.append(difference)
        except (ValueError, ZeroDivisionError):
            print(current_value, expecteds[i])
    return vars_indices


def compare_lists(mismatch):
    for i in mismatch:
        for j in i:
            actuals, expecteds = i[j][0], i[j][1]
            if isinstance(actuals, list):
                for n, act in enumerate(actuals):
                    try:
                        if act != expecteds[n]:
                            if isinstance(act, list):
                                indicies = calculate_differences(act, expecteds[n])
                                if indicies:
                                    print(f"Max variation: {max(indicies)}")
                            else:
                                print(act, expecteds[n])
                    except IndexError:
                        print(act, None)
            else:
                print(f"Actual:   {actuals}")
                print(f"Expected: {expecteds}")


def results_files_match(
    actual: Union[str, dict, ResultsDisplayObject],
    expected: Union[str, dict, ResultsDisplayObject],
    fields_only: bool = False,
    tolerance: float = 0,
) -> bool:
    if isinstance(actual, dict):
        a_data = actual
    elif issubclass(type(actual), ResultsDisplayObject):
        a_data = actual.__dict__
    elif isinstance(actual, str):
        with open(actual) as act:
            a_data = json.load(act)
    else:
        raise ValueError(
            "'Actual' must be a dict, path to json file, or ResultsDisplayObject: "
            f"not {type(actual)}"
        )

    if isinstance(expected, dict):
        e_data = expected
    elif issubclass(type(expected), ResultsDisplayObject):
        e_data = expected.__dict__
    elif isinstance(expected, str):
        with open(expected) as exp:
            e_data = json.load(exp)
    else:
        raise ValueError("'Expected' must be a dict or path to json file")

    # check the types match:
    if a_data["dobj_type"] != e_data["dobj_type"]:
        print("Types do not match")
        print(f"expected: {e_data['dobj_type']}")
        print(f"actual: {a_data['dobj_type']}")
        return False

    # check fields match
    a_not_e = set(a_data) - set(e_data)
    e_not_a = set(e_data) - set(a_data)
    if e_not_a or a_not_e:
        print("Fields in actual but not expected")
        print(a_not_e)
        print("Fields in expected but not actual")
        print(e_not_a)
        return False
    if fields_only:
        return True

    # check the values
    mismatch = []
    for field in a_data:
        a_val = a_data[field]
        e_val = e_data[field]
        if type(a_val) in [list, tuple] and a_val:
            match = compare_nested_lists(a_val, e_val, tolerance)
            if not match:
                mismatch.append({field: (a_data[field], e_data[field])})
        else:
            if a_val != e_val:
                mismatch.append({field: (a_data[field], e_data[field])})
    if mismatch:
        print("The following fields in the DisplayObjects do not have matching values:")
        print(mismatch)
        compare_lists(mismatch)

        return False
    return True


def clean_lines(lines, ignore_comments):
    cleaned = []
    for line in lines:
        if not line.isspace():
            cleanline = [cif.as_string(x) for x in shlex.split(line)]
            if not (cleanline[0].startswith("#") and ignore_comments):
                cleaned.append(cleanline)
    return cleaned


def compare_starfiles(
    file1: Union[str, "os.PathLike[str]"],
    file2: Union[str, "os.PathLike[str]"],
    ignore_comments: bool = True,
    ignore_order: bool = True,
):
    """Check if two star files are equivalent

    Args:
        ignore_comments (bool): do not check comment lines
        ignore_order (bool): Allow the keys for each block to be in different orders
    """
    if not ignore_order:
        cl1 = clean_starfile(file1, ignore_comments)
        cl2 = clean_starfile(file2, ignore_comments)
        assert cl1 == cl2

    else:
        f1_blocks = list_block_types(file1)
        f2_blocks = list_block_types(file2)
        f1 = StarFile(file1)
        f2 = StarFile(file2)
        assert f1_blocks == f2_blocks
        for block in f1_blocks:
            if f1_blocks[block][1] == "pair":
                assert f1.pairs_as_dict(block) == f2.pairs_as_dict(block)
            elif f1_blocks[block][1] == "loop":
                f1_vals = f1.loop_as_list(block=block, headers=True)
                f2_vals = f2.loop_as_list(block=block, headers=True)
                # remove the numbers from the headers
                f1_heads = [x.split()[0] for x in f1_vals[0]]
                f2_heads = [x.split()[0] for x in f2_vals[0]]
                f1_heads.sort()
                f2_heads.sort()
                assert f1_heads == f2_heads
                f1_data = [x.sort() for x in f1_vals]
                f2_data = [x.sort() for x in f2_vals]
                assert f1_data == f2_data


def clean_starfile(filename, ignore_comments=True):
    """Reads pipeline into a list where each line is a list of the entries on that line
    used for comparing pipeline outputs without having to worry about spaces or
    carriage returns, removes quotes on the entry names so all lines can be compared
    equally"""

    with open(filename) as f:
        lines = f.readlines()
    return clean_lines(lines, ignore_comments)


def list_block_types(
    starfile: Union[str, "os.PathLike[str]"]
) -> Dict[str, Literal["pair", "loop"]]:
    """List the blocks in the starfile and if they are key-value pairs or loops

    Assumes blocks contain either a set of key-value pairs or a single loop

    Args:
        starfile (str or Path): The file

    Returns:
        Dict[str, Literal["pair", "loop"]]: The block name and its type either
            'pair' or 'loop'

    Raises:
        ValueError: If any block contains multiple loops
        ValueError: If any block contains both key-value pairs and loop(s)

    """
    block_data: Dict[str, Literal["pair", "loop"]] = {}
    data = cif.read_file(str(starfile))
    for block in data:
        if len(list(block)) == 0:
            raise ValueError(f"The block {block.name} is empty")
        elif all([x.pair for x in block]):
            block_data[block.name] = "pair"
        elif all([x.loop for x in block]):
            block_data[block.name] = "loop"
            if len(list(block)) > 1:
                raise ValueError(f"Block {block.name} contains multiple loops")
        else:
            raise ValueError(
                f"Error parsing starfile block {block.name}, it may contain both loops"
                " and key-value pairs."
            )

    return block_data
