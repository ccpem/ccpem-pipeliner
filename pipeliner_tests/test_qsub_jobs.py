#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch, Mock

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.job_tests import other


@patch.dict(
    os.environ,
    {
        "PIPELINER_QSUB_COMMAND": "qsub",
        "PIPELINER_QUEUE_NAME": "openmpi",
    },
)
class QsubJobsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.plugin_test_data = os.path.dirname(other.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_env_vars_defaults(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_defaults.job")
        )

        assert job.joboptions["qsub_extra_1"].value == "101"
        assert job.joboptions["qsub_extra_2"].value == "202"
        assert job.joboptions["qsub_extra_3"].value == "303"
        assert job.joboptions["qsub_extra_4"].value == "404"

    def test_get_env_vars_2extras(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "2",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_2extras.job")
        )

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"

    def test_get_env_vars_4extras(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_4extras.job")
        )

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

    # Patch shutil.which to make sure the command path matches the expected one
    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: cmd))
    def test_qsub_write_script_refine3D(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        shutil.copy(scriptfile, ".")

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/refine3D_qsub_4extras.job")
        )

        output_dir = "Refine3D/job001/"
        job.output_dir = output_dir
        os.makedirs(output_dir)
        job.save_job_submission_script(job.get_final_commands())

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_ref3D.sh"
        )
        out_script = "Refine3D/job001/run_submit.script"
        assert os.path.isfile(out_script)

        with open(expected_script) as expected:
            expected = expected.readlines()

        with open(out_script) as wrote:
            wrote = wrote.readlines()
        for line in zip(wrote, expected):
            assert line[0] == line[1]

    # Patch shutil.which to make sure the command path matches the expected one
    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: cmd))
    def test_qsub_write_script_autopick(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        shutil.copy(scriptfile, ".")

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
        )

        output_dir = "AutoPick/job001/"
        job.output_dir = output_dir
        os.makedirs(output_dir)
        job.save_job_submission_script(job.get_final_commands())

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_autopick_2dref.sh"
        )
        out_script = "AutoPick/job001/run_submit.script"
        assert os.path.isfile(out_script)

        with open(expected_script) as expected:
            expected = expected.readlines()

        with open(out_script) as wrote:
            wrote = wrote.readlines()

        for line in zip(expected, wrote):
            assert line[0] == line[1]

    # Patch shutil.which to make sure the command path matches the expected one
    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: cmd))
    def test_qsub_write_script_multibody(self):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        shutil.copy(scriptfile, ".")
        bodyfile = os.path.join(self.test_data + "/JobFiles/MultiBody/bodyfile.star")
        shutil.copy(bodyfile, ".")

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/multibody_sep_eigen_qsub.job")
        )
        output_dir = "MultiBody/job001/"
        job.output_dir = output_dir
        os.makedirs(output_dir)
        job.save_job_submission_script(job.get_final_commands())

        assert job.joboptions["qsub_extra_1"].value.strip() == "Here is number 1"
        assert job.joboptions["qsub_extra_2"].value.strip() == "Here is number 2"
        assert job.joboptions["qsub_extra_3"].value.strip() == "Here is number 3"
        assert job.joboptions["qsub_extra_4"].value.strip() == "Here is number 4"

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_multibody.sh"
        )
        out_script = "MultiBody/job001/run_submit.script"
        assert os.path.isfile(out_script)

        with open(expected_script) as expected:
            expected = expected.readlines()
        with open(out_script) as wrote:
            wrote = wrote.readlines()
        for line in zip(wrote, expected):
            assert line[0] == line[1]

    def test_qsub_no_template_specified(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/qsub_notemplate.job")
        )
        output_dir = "AutoPick/job003/"
        job.output_dir = output_dir
        with self.assertRaises(ValueError):
            job.save_job_submission_script([])

    def test_qsub_template_missing(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
        )
        output_dir = "AutoPick/job003/"
        job.output_dir = output_dir
        with self.assertRaises(ValueError):
            job.save_job_submission_script([])

    @patch("pipeliner.user_settings.get_qsub_extra_count", return_value=4)
    def test_qsub_cant_write_output_script(self, *_):
        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        shutil.copy(scriptfile, ".")
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Qsub/autopick_2dref_qsub.job")
        )
        output_dir = "AutoPick/job003/"
        job.output_dir = output_dir
        with self.assertRaises(RuntimeError):
            job.save_job_submission_script([])

    # Patch shutil.which to make sure the command path matches the expected one
    @patch.object(
        shutil,
        "which",
        Mock(
            side_effect=(
                lambda cmd: (
                    "path/to/ccp4/bin/ccp4-python" if cmd == "ccp4-python" else cmd
                )
            )
        ),
    )
    @patch("pipeliner.pipeliner_job.get_python_command", return_value=["python3"])
    @patch(
        "pipeliner.jobs.ccpem.em_placement_job.get_job_script",
        return_value="job_script.py",
    )
    def test_qsub_write_script_for_job_with_workingdir(self, *_):
        envvars = {
            "PIPELINER_QSUB_EXTRA_COUNT": "4",
            "PIPELINER_QSUB_EXTRA1": "Extra variable 1:",
            "PIPELINER_QSUB_EXTRA1_DEFAULT": "101",
            "PIPELINER_QSUB_EXTRA1_HELP": "HELP! 1",
            "PIPELINER_QSUB_EXTRA2": "Extra variable 2:",
            "PIPELINER_QSUB_EXTRA2_DEFAULT": "202",
            "PIPELINER_QSUB_EXTRA2_HELP": "HELP! 2",
            "PIPELINER_QSUB_EXTRA3": "Extra variable 3:",
            "PIPELINER_QSUB_EXTRA3_DEFAULT": "303",
            "PIPELINER_QSUB_EXTRA3_HELP": "HELP! 3",
            "PIPELINER_QSUB_EXTRA4": "Extra variable 4:",
            "PIPELINER_QSUB_EXTRA4_DEFAULT": "404",
            "PIPELINER_QSUB_EXTRA4_HELP": "HELP! 4",
        }
        for envvar in envvars:
            os.environ[envvar] = envvars[envvar]

        scriptfile = os.path.join(self.test_data + "/submission_script.sh")
        shutil.copy(scriptfile, ".")

        job = job_factory.read_job(
            os.path.join(
                self.test_data,
                "JobFiles/EMPlacement/em_placement_fit_model_qsub_job.star",
            )
        )

        output_dir = "EMPlacement/job001/"
        job.output_dir = output_dir
        os.makedirs(output_dir)
        job.save_job_submission_script(job.get_final_commands())

        expected_script = os.path.join(
            self.test_data + "/JobFiles/Qsub/submission_script_emplace.sh"
        )
        actual_script = os.path.join(output_dir, "run_submit.script")
        assert os.path.isfile(actual_script)

        with open(expected_script) as expected_file:
            expected = expected_file.readlines()

        with open(actual_script) as actual_file:
            actual_lines = actual_file.readlines()

        for line_pair in zip(expected, actual_lines):
            print(line_pair[0].strip())
            print(line_pair[1].strip())
            assert line_pair[0].strip() == line_pair[1].strip()


if __name__ == "__main__":
    unittest.main()
