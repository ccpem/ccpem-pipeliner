#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import re
import shutil
import stat
import tempfile
import unittest
from glob import glob
from pathlib import Path
from unittest.mock import patch, Mock
from gemmi import cif

from pipeliner_tests import test_data
from pipeliner.utils import touch
from pipeliner.api.manage_project import (
    PipelinerProject,
    get_commands_and_nodes,
    convert_pipeline,
    get_archives_list_from_summary_file,
    get_metadata_reports_from_summary_file,
    get_ref_reports_from_summary_file,
    delete_summary_data_archive,
    delete_summary_data_metadata_report,
    delete_summary_data_reference_report,
    get_deleted_jobs_and_sizes,
    get_cleaned_up_jobs_and_sizes,
    delete_cleaned_job_files,
    delete_trashed_job_files,
    delete_all_cleaned_job_files,
    delete_all_trashed_job_files,
)

from pipeliner_tests.testing_tools import (
    slow_test,
    live_test,
    ShortpipeFileStructure,
    make_conversion_file_structure,
    clean_starfile,
    compare_starfiles,
    get_relion_tutorial_data,
)
from pipeliner.data_structure import (
    FAIL_FILE,
    SUCCESS_FILE,
    ABORT_FILE,
    ABORT_TRIGGER,
    JOBSTATUS_ABORT,
    JOBSTATUS_FAIL,
    JOBSTATUS_SCHED,
    JOBSTATUS_SUCCESS,
    PROJECT_FILE,
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_PROCESSDATA,
    NODE_MASK3D,
    EXTRACT_PARTICLE_NAME,
    CLASS2D_PARTICLE_NAME,
    POSTPROCESS_JOB_NAME,
    TRASH_DIR,
    CLEANUP_DIR,
    JOBINFO_FILE,
)

from pipeliner.pipeliner_job import JobInfo, Ref, ExternalProgram
from pipeliner.api.api_utils import get_job_info
from pipeliner.starfile_handler import JobStar
from pipeliner.job_options import (
    StringJobOption,
    IntJobOption,
    FloatJobOption,
    BooleanJobOption,
    InputNodeJobOption,
    SearchStringJobOption,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.metadata_tools import METADATA_REPORT_DIR
from pipeliner.summary_data_tools import SUMMARY_DATA_FILE, SUMMARY_DATA_HEADERS
from pipeliner.job_manager import wait_for_job_to_finish


class ManageProjectTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # Use addCleanup instead of tearDown to ensure other cleanups run first
        def cleanup():
            os.chdir(self._orig_dir)
            if os.path.exists(self.test_dir):
                shutil.rmtree(self.test_dir)

        self.addCleanup(cleanup)

    @staticmethod
    def check_date(test_date):
        if re.match(r"\d+-\d+-\d+ \d+:\d+:\d+", test_date) is not None:
            return True
        return False

    def test_check_date(self):
        assert self.check_date("12-34-56 12:23:43")
        assert not self.check_date("Failure string")

    def test_initialize_nonexistent_project_raises_exception(self):
        assert not os.path.isfile("default_pipeline.star")
        with self.assertRaises(FileNotFoundError):
            PipelinerProject()

    def test_initialize_new_empty_project(self):
        PipelinerProject(make_new_project=True)
        assert os.path.isfile("default_pipeline.star")
        assert os.path.isfile(PROJECT_FILE)
        with open(PROJECT_FILE, "r") as pf:
            proj_info = json.load(pf)

        assert proj_info["project name"] == "New project"
        assert proj_info["pipeline file"] == "default_pipeline.star"
        assert proj_info["description"] == "A new CCP-EM pipeliner project"
        assert self.check_date(proj_info["date created"])
        assert self.check_date(proj_info["last opened"])

    @patch("os.mkdir", side_effect=PermissionError)
    def test_initialize_new_project_without_dir_write_permission(self, _):
        with self.assertRaises(PermissionError):
            PipelinerProject(make_new_project=True)

    def test_initialize_project_vals(self):
        PipelinerProject(
            project_name="My project",
            description="This is my project, I love it!",
            make_new_project=True,
        )
        assert os.path.isfile("default_pipeline.star")
        assert os.path.isfile(PROJECT_FILE)
        with open(PROJECT_FILE, "r") as pf:
            proj_info = json.load(pf)

        assert proj_info["project name"] == "My project"
        assert proj_info["pipeline file"] == "default_pipeline.star"
        assert proj_info["description"] == "This is my project, I love it!"
        assert self.check_date(proj_info["date created"])
        assert self.check_date(proj_info["last opened"])

    def test_can_read_existing_pipeliner_project_with_non_writable_directory(self):
        # Copy pipeline and project files into test dir
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )
        # Make the test dir non-writable
        test_dir = Path(self.test_dir)
        curr_mode = test_dir.stat().st_mode
        test_dir.chmod(curr_mode & ~stat.S_IWUSR)
        # Add cleanup to ensure the test directory can be removed after the test
        self.addCleanup(test_dir.chmod, curr_mode)

        # Open the project. This should have no errors and it should read the correct
        # pipeline file name from the project file.
        proj = PipelinerProject()
        assert proj.pipeline_name == "short_full"

    def test_can_read_existing_relion_project_with_non_writable_directory(self):
        # Copy pipeline file into test dir
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        # Make the test dir non-writable
        test_dir = Path(self.test_dir)
        curr_mode = test_dir.stat().st_mode
        test_dir.chmod(curr_mode & ~stat.S_IWUSR)
        # Add cleanup to ensure the test directory can be removed after the test
        self.addCleanup(test_dir.chmod, curr_mode)

        # Need to set the correct pipeline name because there is no project file
        proj = PipelinerProject(pipeline_name="short_full")
        assert proj.pipeline_name == "short_full"

    def test_initialize_existing_project(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )
        proj = PipelinerProject()
        with ProjectGraph(name=proj.pipeline_name) as pipeline:
            proc_names = [x.name for x in pipeline.process_list]
            node_names = [x.name for x in pipeline.node_list]

        expected_procs = [
            "Import/job001/",
            "MotionCorr/job002/",
            "CtfFind/job003/",
            "AutoPick/job004/",
            "Extract/job005/",
            "Class2D/job006/",
            "Select/job007/",
            "InitialModel/job008/",
            "Class3D/job009/",
            "Refine3D/job010/",
            "MultiBody/job011/",
            "CtfRefine/job012/",
            "MaskCreate/job013/",
            "Polish/job014/",
            "JoinStar/job015/",
            "Subtract/job016/",
            "PostProcess/job017/",
            "External/job018/",
            "LocalRes/job019/",
        ]
        expected_nodes = [
            "Import/job001/movies.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/logfile.pdf",
            "AutoPick/job004/coords_suffix_autopick.star",
            "AutoPick/job004/logfile.pdf",
            "Extract/job005/particles.star",
            "Class2D/job006/run_it025_data.star",
            "Class2D/job006/run_it025_optimiser.star",
            "Select/job007/selected_particles.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_optimiser.star",
            "Refine3D/job010/run_data.star",
            "Refine3D/job010/run_optimiser.star",
            "Refine3D/job010/run_class001.mrc",
            "Refine3D/job010/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class002_half1_unfil.mrc",
            "CtfRefine/job012/logfile.pdf",
            "CtfRefine/job012/particles_ctf_refine.star",
            "MaskCreate/job013/mask.mrc",
            "Polish/job014/opt_params_all_groups.txt",
            "Polish/job014/logfile.pdf",
            "Polish/job014/shiny.star",
            "JoinStar/job015/join_particles.star",
            "Subtract/job016/particles_subtract.star",
            "PostProcess/job017/logfile.pdf",
            "PostProcess/job017/postprocess.star",
            "LocalRes/job019/relion_locres_filtered.mrc",
            "LocalRes/job019/relion_locres.mrc",
        ]
        for node in expected_nodes:
            assert node in node_names, node
        for proc in expected_procs:
            assert proc in proc_names, proc

        with open(PROJECT_FILE, "r") as pf:
            proj_info = json.load(pf)

        desc = (
            "This pipeline represents a full SPA processing run but is "
            "still short enough to handle easily"
        )
        assert proj_info["project name"] == "Short full pipeline"
        assert proj_info["description"] == desc
        assert proj_info["pipeline file"] == "short_full_pipeline.star"
        assert self.check_date(proj_info["date created"])
        assert self.check_date(proj_info["last opened"])

    def test_initialize_existing_project_no_projfile(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        proj = PipelinerProject(pipeline_name="short_full")
        with ProjectGraph(name=proj.pipeline_name) as pipeline:
            proc_names = [x.name for x in pipeline.process_list]
            node_names = [x.name for x in pipeline.node_list]

        expected_procs = [
            "Import/job001/",
            "MotionCorr/job002/",
            "CtfFind/job003/",
            "AutoPick/job004/",
            "Extract/job005/",
            "Class2D/job006/",
            "Select/job007/",
            "InitialModel/job008/",
            "Class3D/job009/",
            "Refine3D/job010/",
            "MultiBody/job011/",
            "CtfRefine/job012/",
            "MaskCreate/job013/",
            "Polish/job014/",
            "JoinStar/job015/",
            "Subtract/job016/",
            "PostProcess/job017/",
            "External/job018/",
            "LocalRes/job019/",
        ]
        expected_nodes = [
            "Import/job001/movies.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/logfile.pdf",
            "AutoPick/job004/coords_suffix_autopick.star",
            "AutoPick/job004/logfile.pdf",
            "Extract/job005/particles.star",
            "Class2D/job006/run_it025_data.star",
            "Class2D/job006/run_it025_optimiser.star",
            "Select/job007/selected_particles.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_optimiser.star",
            "Refine3D/job010/run_data.star",
            "Refine3D/job010/run_optimiser.star",
            "Refine3D/job010/run_class001.mrc",
            "Refine3D/job010/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class002_half1_unfil.mrc",
            "CtfRefine/job012/logfile.pdf",
            "CtfRefine/job012/particles_ctf_refine.star",
            "MaskCreate/job013/mask.mrc",
            "Polish/job014/opt_params_all_groups.txt",
            "Polish/job014/logfile.pdf",
            "Polish/job014/shiny.star",
            "JoinStar/job015/join_particles.star",
            "Subtract/job016/particles_subtract.star",
            "PostProcess/job017/logfile.pdf",
            "PostProcess/job017/postprocess.star",
            "LocalRes/job019/relion_locres_filtered.mrc",
            "LocalRes/job019/relion_locres.mrc",
        ]
        for node in expected_nodes:
            assert node in node_names, node
        for proc in expected_procs:
            assert proc in proc_names, proc

        with open(PROJECT_FILE, "r") as pf:
            proj_info = json.load(pf)

        desc = (
            'This project was imported from RELION. "Date created" is the date it was'
            " imported into the CCP-EM Pipeliner."
        )

        assert proj_info["project name"] == "Project imported from RELION"
        assert proj_info["description"] == desc
        assert proj_info["pipeline file"] == "short_full_pipeline.star"
        assert self.check_date(proj_info["date created"])
        assert self.check_date(proj_info["last opened"])

    def test_initialize_existing_project_change_info(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "ProjectFiles/short_full_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )
        PipelinerProject(project_name="Changed the name", description="new desc")
        with open(PROJECT_FILE, "r") as pf:
            proj_info = json.load(pf)

        assert proj_info["project name"] == "Changed the name"
        assert proj_info["description"] == "new desc"
        assert proj_info["pipeline file"] == "short_full_pipeline.star"
        assert self.check_date(proj_info["date created"])
        assert self.check_date(proj_info["last opened"])

    def test_cleanup_1job(self):
        outfiles = ShortpipeFileStructure(["MotionCorr"]).outfiles
        assert len(outfiles["MotionCorr"]) == 1016

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        projfile = os.path.join(
            self.test_data,
            "ProjectFiles/short_full_pipeline_projectfile.json",
        )
        shutil.copy(projfile, PROJECT_FILE)

        proj = PipelinerProject(pipeline_name="short_full")
        assert proj

        files = glob("MotionCorr/job002/**/*", recursive=True)
        procname = "MotionCorr"
        assert len(files) - 1 == len(outfiles[procname])

        proj.run_cleanup(["MotionCorr/job002/"], False)

        del_exts = ["com", "out", "err", "log"]
        removed, kept = [], []
        excluded = [
            "MotionCorr/job002/run.out",
            "MotionCorr/job002/run.err",
            "MotionCorr/job002/" + SUCCESS_FILE,
        ]
        for f in outfiles[procname]:
            if f not in excluded:
                if f.split(".")[1] in del_exts:
                    removed.append(f)
                else:
                    kept.append(f)

        files = glob("MotionCorr/job002/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f

    def test_cleanup_1job_harsh(self):
        outfiles = ShortpipeFileStructure(["MotionCorr"]).outfiles
        assert len(outfiles["MotionCorr"]) == 1016

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        projfile = os.path.join(
            self.test_data,
            "ProjectFiles/short_full_pipeline_projectfile.json",
        )
        shutil.copy(projfile, PROJECT_FILE)

        proj = PipelinerProject(pipeline_name="short_full")
        assert proj

        files = glob("MotionCorr/job002/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles["MotionCorr"])
        assert os.path.isdir("MotionCorr/job002/Raw_data")

        proj.run_cleanup(["MotionCorr/job002/"], True)

        assert not os.path.isdir("MotionCorr/job002/Raw_data")

    def test_clean_up_multiple_jobs(self):
        outfiles = ShortpipeFileStructure(["MotionCorr", "AutoPick"]).outfiles
        assert len(outfiles["AutoPick"]) == 209
        assert len(outfiles["MotionCorr"]) == 1016

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        projfile = os.path.join(
            self.test_data,
            "ProjectFiles/short_full_pipeline_projectfile.json",
        )
        shutil.copy(projfile, PROJECT_FILE)

        proj = PipelinerProject(pipeline_name="short_full")
        assert proj

        mc_files = glob("MotionCorr/job002/**/*", recursive=True)
        assert len(mc_files) - 1 == len(outfiles["MotionCorr"])

        ap_files = glob("AutoPick/job004/**/*", recursive=True)
        assert len(ap_files) - 1 == len(outfiles["AutoPick"])

        ap_globlist = ["AutoPick/job004/Raw_data/*.spi"]
        ap_del_files = []
        for f in ap_globlist:
            ap_del_files += glob(f)

        proj.run_cleanup(["MotionCorr/job002/", "AutoPick/job004/"], False)

        del_exts = ["com", "out", "err", "log"]
        removed, kept = [], []
        excluded = [
            "MotionCorr/job002/run.out",
            "MotionCorr/job002/run.err",
            "MotionCorr/job002/" + SUCCESS_FILE,
        ]
        for f in outfiles["MotionCorr"]:
            if f not in excluded:
                if f.split(".")[1] in del_exts:
                    removed.append(f)
                else:
                    kept.append(f)

        ap_files = glob("MotionCorr/job002/**/*", recursive=True)
        for f in kept:
            assert f in ap_files, f
        for f in removed:
            assert f not in ap_files, f

        ap_removed, ap_kept = [], []
        for f in outfiles["AutoPick"]:
            if f in ap_del_files:
                ap_removed.append(f)
            else:
                ap_kept.append(f)
        ap_files = glob("AutoPick/job004/**/*", recursive=True)

        for f in ap_kept:
            assert f in ap_files, f
        for f in ap_removed:
            assert f not in ap_files, f

    @slow_test
    def test_cleanup_alljobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        proj = PipelinerProject(pipeline_name="short_full")

        ShortpipeFileStructure(["all"])

        del_files = {
            "Import": [],
            "MotionCorr": [
                "/job002/Raw_data/*.com",
                "/job002/Raw_data/*.err",
                "/job002/Raw_data/*.out",
                "/job002/Raw_data/*.log",
            ],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*_extract.star"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
            ],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
                "Class2D/job006/run_it025_model.star",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
                "InitialModel/job008/run_it150_model.star",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_optimiser.star",
                "Class3D/job009/run_it025_model.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_model.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        proj.cleanup_all(False)

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert f"{CLEANUP_DIR}/{f}" in trash, f"{CLEANUP_DIR}/{f}"

    @slow_test
    def test_cleanup_alljobs_harsh(self):
        ShortpipeFileStructure(["all"])

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        proj = PipelinerProject(pipeline_name="short_full")
        assert proj

        del_files = {
            "Import": [],
            "MotionCorr": ["/job002/Raw_data/*"],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*", "/job011/analyse_component*_bin*.mrc"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
                "/job014/Raw_data/*shiny.star",
                "/job014/Raw_data/*shiny.mrcs",
            ],
            "JoinStar": [],
            "Subtract": ["/job016/subtracted_*"],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
                "Class2D/job006/run_it025_model.star",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
                "InitialModel/job008/run_it150_model.star",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_optimiser.star",
                "Class3D/job009/run_it025_model.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        proj.cleanup_all(True)

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert f"{CLEANUP_DIR}/{f}" in trash, f"{CLEANUP_DIR}/{f}"

    def test_delete_job(self):
        # copy in the pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_del_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_del_pipeline.star")

        # initialize the project
        proj = PipelinerProject(pipeline_name="short_cl2d_del")

        # make the files like the project has been run
        fdirs = {
            "Class2D/job008": "Class2D/LoG_based",
            "Extract/job007": "Extract/LoG_based",
        }
        for fdir in fdirs:
            os.makedirs(fdir)
            os.symlink(os.path.abspath(fdir), os.path.join(self.test_dir, fdirs[fdir]))
            assert os.path.isdir(fdir)
            assert os.path.islink(fdirs[fdir])

        cl2d_files = [
            "_data.star",
            "_optimiser.star",
        ]
        file_list = list()
        for i in range(0, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff), ff
                file_list.append(ff)

        cl2d_other_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]

        for f in cl2d_other_files:
            ff = "Class2D/job008/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)

        extract_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "particles.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]

        for f in extract_files:
            ff = "Extract/job007/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)

        moviedir = "Extract/job007/Movies"
        os.makedirs(moviedir)
        assert os.path.isdir("Extract/job007/Movies")

        for i in range(1, 11):
            f = moviedir + "/movie_parts{:03d}.mrcs".format(i)
            touch(f)
            assert os.path.isfile(f)
            file_list.append(f)

        # make the .Nodes files
        nodesfiles = [
            ".Nodes/MicrographsCoords/Extract/job007/particles.star",
            ".Nodes/ParticleGroupMetadata/Class2D/job008/run_it025_data.star",
        ]
        for f in nodesfiles:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            touch(f)
            assert os.path.isfile(f)

        # delete the extract job
        proj.delete_job("Extract/job007/")

        written = clean_starfile("short_cl2d_del_pipeline.star")
        removed_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Extract/job007/particles.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            ],
            [
                "Class2D/job008/run_it025_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it025_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        for removed_line in removed_lines:
            assert removed_line not in written, removed_line

        for f in file_list:
            assert not os.path.isfile(f), f
            trashname = f"{TRASH_DIR}/{f}"
            assert os.path.isfile(trashname), trashname

    def make_undelete_file_structure(self):
        dirs = [
            "Class2D/job008",
            "Extract/job007",
            "Trash",
        ]

        for d in dirs:
            os.makedirs(d)
            assert os.path.isdir(d), d

        common_files = [
            "run.out",
            "run.err",
            "note.txt",
            "run.job",
            "default_pipeline.star",
            SUCCESS_FILE,
        ]

        outfiles = list()
        for d in dirs[:-1]:
            for f in common_files:
                fn = d + "/" + f
                touch(fn)
                assert os.path.isfile(fn), fn
                outfiles.append(fn)

        shutil.copy(
            os.path.join(
                self.test_data,
                "Pipelines/for_undelete_cl2d_job_pipeline.star",
            ),
            os.path.join(self.test_dir, "Class2D/job008/job_pipeline.star"),
        )
        outfiles.append("Class2D/job008/job_pipeline.star")

        shutil.copy(
            os.path.join(
                self.test_data,
                "Pipelines/for_undelete_extract_job_pipeline.star",
            ),
            os.path.join(self.test_dir, "Extract/job007/job_pipeline.star"),
        )
        outfiles.append("Extract/job007/job_pipeline.star")

        touch("Extract/job007/particles.star")
        assert os.path.isfile("Extract/job007/particles.star")
        outfiles.append("Extract/job007/particles.star")

        os.makedirs("Extract/job007/Movies")
        assert os.path.isdir("Extract/job007/Movies")

        # make particles files
        for i in range(1, 11):
            f = "Extract/job007/Movies/movie_{:03d}.mrcs".format(i)
            touch(f)
            assert os.path.isfile(f), f
            outfiles.append(f)

        # make class2d files
        cl2d_files = ["_data.star", "_optimiser.star"]
        for i in range(1, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{:03d}{}".format(i, f)
                touch(ff)
                assert os.path.isfile(ff)
                outfiles.append(ff)

        shutil.move("Class2D", f"{TRASH_DIR}/Class2D")
        shutil.move("Extract", f"{TRASH_DIR}/Extract")

        assert not os.path.isdir("Class2D")
        assert not os.path.isdir("Extract")
        assert os.path.isdir(f"{TRASH_DIR}/Class2D")
        assert os.path.isdir(f"{TRASH_DIR}/Extract")

        return outfiles

    def test_undelete_process_with_parents(self):
        outfiles = self.make_undelete_file_structure()

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        proj = PipelinerProject(pipeline_name="for_undelete_deleted")

        restored_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            [
                "Class2D/job008/run_it025_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it025_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        original = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        proj.undelete_job("Class2D/job008/")

        wrote = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        for f in outfiles:
            assert os.path.isfile(f), f

        assert os.path.islink("Extract/LoG_based")
        assert os.path.islink("Class2D/LoG_based")

    def test_set_alias(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        os.makedirs("Import/job001")
        proj = PipelinerProject(pipeline_name="short_full")
        with ProjectGraph(name="short_full") as pipeline:
            assert pipeline.process_list[0].alias is None

        proj.set_alias("Import/job001/", "NEW_ALIAS")
        with ProjectGraph(name="short_full") as pipeline:
            assert pipeline.process_list[0].alias == "Import/NEW_ALIAS/"

    def test_overwrite_alias(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        os.makedirs("Import/job001")
        proj = PipelinerProject(pipeline_name="short_full")
        with ProjectGraph(name="short_full") as pipeline:
            assert pipeline.process_list[0].alias is None

        proj.set_alias("Import/job001/", "NEW_ALIAS")
        with ProjectGraph(name="short_full") as pipeline:
            assert pipeline.process_list[0].alias == "Import/NEW_ALIAS/"

        proj.set_alias("Import/job001/", "NEW_ALIAS_PART_DEUX")
        with ProjectGraph(name="short_full") as pipeline:
            assert pipeline.process_list[0].alias == "Import/NEW_ALIAS_PART_DEUX/"

    def test_set_alias_for_nonexistent_process(self):
        proj = PipelinerProject(make_new_project=True)
        with self.assertRaises(ValueError):
            proj.set_alias("NotAJobType/job999/", "NEW_ALIAS")

    def test_update_status(self):
        """Go through the entire short pipeline,
        mark every job failed, then aborted"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["all"])

        proj = PipelinerProject(pipeline_name="short_full")

        with ProjectGraph(name="short_full") as pipeline:
            for proc in pipeline.process_list:
                assert proc.status == JOBSTATUS_SUCCESS
            proc_list = pipeline.process_list

        for proc in proc_list:
            proj.update_job_status(proc.name, JOBSTATUS_FAIL)
            with ProjectGraph(name="short_full") as pipeline:
                newproc = pipeline.find_process(proc.name)
                assert newproc.status == JOBSTATUS_FAIL
                assert os.path.isfile(os.path.join(newproc.name, FAIL_FILE))
            with ProjectGraph(name="job", pipeline_dir=newproc.name) as jobpipe:
                jobpipe_proc = jobpipe.find_process(newproc.name)
                assert jobpipe_proc.status == JOBSTATUS_FAIL

        for proc in proc_list:
            proj.update_job_status(proc.name, JOBSTATUS_SUCCESS)
            with ProjectGraph(name="short_full") as pipeline:
                newproc = pipeline.find_process(proc.name)
                assert newproc.status == JOBSTATUS_SUCCESS
            with ProjectGraph(name="job", pipeline_dir=newproc.name) as jobpipe:
                jobpipe_proc = jobpipe.find_process(newproc.name)
                assert jobpipe_proc.status == JOBSTATUS_SUCCESS

        for proc in proc_list:
            proj.update_job_status(proc.name, JOBSTATUS_ABORT)
            with ProjectGraph(name="short_full") as pipeline:
                newproc = pipeline.find_process(proc.name)
                assert newproc.status == JOBSTATUS_ABORT
                assert os.path.isfile(os.path.join(newproc.name, ABORT_FILE))
            with ProjectGraph(name="job", pipeline_dir=newproc.name) as jobpipe:
                jobpipe_proc = jobpipe.find_process(newproc.name)
                assert jobpipe_proc.status == JOBSTATUS_ABORT

    @live_test(job="relion.postprocess")
    def test_run_job_from_runjob(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_test.job",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        proj.run_job("postprocess_manage_test.job", run_in_foreground=True)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]

        pipeline_data = clean_starfile("short_full_pipeline.star")

        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

    @live_test(job="relion.postprocess")
    def test_run_job_from_dict(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        proj.run_job(jobinput=jobdict, run_in_foreground=True)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]

        pipeline_data = clean_starfile("short_full_pipeline.star")

        for line in added_lines:
            assert line in pipeline_data

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

    @live_test(job="relion.postprocess")
    def test_run_job_from_dict_with_alias(self):
        # Prepare the directory structure as if Import jobs have been run
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        proj.run_job(jobdict, alias="Habari", run_in_foreground=True)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            [
                "PostProcess/job020/",
                "PostProcess/Habari/",
                POSTPROCESS_JOB_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]

        pipeline_data = clean_starfile("short_full_pipeline.star")

        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

        with ProjectGraph(name="short_full") as pipeline:
            the_job = pipeline.find_process("PostProcess/job020/")
            assert the_job.alias == "PostProcess/Habari/"
            assert os.path.isdir("PostProcess/job020")
            assert os.path.islink("PostProcess/Habari")
            assert the_job.status == JOBSTATUS_SUCCESS

    def test_run_job_from_dict_error_missing_jobtype_param(self):
        """To run a job from a dict the dict must contain the
        _rlnJobTypeLabel key"""

        proj = PipelinerProject(pipeline_name="short_full", make_new_project=True)
        jobdict = {
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        with self.assertRaises(ValueError):
            proj.run_job(jobdict)

    def test_run_job_from_dict_error_continue(self):
        """Can't continue a job from api.run_job()"""
        proj = PipelinerProject(pipeline_name="short_full", make_new_project=True)
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "_rlnJobIsContinue": "True",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        with self.assertRaises(ValueError):
            proj.run_job(jobdict)

    def test_run_job_from_dict_error_unknown_jobtype(self):
        """Error is jobtype is unknown"""
        proj = PipelinerProject(pipeline_name="short_full", make_new_project=True)
        jobdict = {
            "_rlnJobTypeLabel": "Badbadbad",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        with self.assertRaises(ValueError):
            proj.run_job(jobdict)

    @live_test(job="relion.postprocess")
    def test_schedule_job_from_dict(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        proj.schedule_job(jobdict)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

    @live_test(job="relion.postprocess")
    def test_schedule_job_from_dict_with_alias(self):
        # Prepare the directory structure as if Import jobs have been run
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        proj.schedule_job(jobdict, alias="Habari")

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            [
                "PostProcess/job020/",
                "PostProcess/Habari/",
                POSTPROCESS_JOB_NAME,
                JOBSTATUS_SCHED,
            ],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

        with ProjectGraph(name="short_full") as pipeline:
            the_job = pipeline.find_process("PostProcess/job020/")
            assert the_job.alias == "PostProcess/Habari/"
            assert os.path.isdir("PostProcess/job020")
            assert os.path.islink("PostProcess/Habari")
            assert the_job.status == JOBSTATUS_SCHED

    @live_test(job="relion.postprocess")
    def test_schedule_continuation(self):
        # First run the job
        # Prepare the directory structure as if Import jobs have been run
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        jobdict = {
            "_rlnJobTypeLabel": "relion.postprocess",
            "_rlnJobIsTomo": "0",
            "fn_in": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "fn_mask": "MaskCreate/job013/emd_3488_mask.mrc",
            "angpix": 1.244,
        }
        proj.run_job(jobdict, run_in_foreground=True)

        # Then schedule its continuation
        proj.schedule_continue_job("PostProcess/job020/")

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

    @live_test(job="relion.postprocess")
    def test_schedule_job_from_runjob(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_test.job",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        proj.schedule_job("postprocess_manage_test.job")

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

    @live_test(job="relion.postprocess")
    @patch("pipeliner.utils.date_time_tag")
    def test_run_job_from_jobstar(self, mock_dtt):
        # Prepare the directory structure as if Import jobs have been run
        #
        mock_dtt.return_value = "date_time_tag"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_job.star",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        proj.run_job("postprocess_manage_job.star", run_in_foreground=True)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]

        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

        # check the jobinfo file
        with open(f"PostProcess/job020/{JOBINFO_FILE}", "r") as ji:
            actual_ji = json.load(ji)
        assert actual_ji["job_directory"] == "PostProcess/job020/"
        assert actual_ji["comments"] == []
        assert actual_ji["history"][0] == "date_time_tag: Scheduled"
        assert actual_ji["history"][1] == "date_time_tag: Run"
        assert actual_ji["history"][2] == "date_time_tag: Job ended; Succeeded"
        com = (
            f"date_time_tag: {shutil.which('relion_postprocess')} --mask "
            "MaskCreate/job013/emd_3488_mask.mrc"
            " --i Import/job001/3488_run_half1_class001_unfil.mrc"
            " --o PostProcess/job020/postprocess --angpix 1.244 --adhoc_bfac -1000"
            " --pipeline_control PostProcess/job020/"
        )
        assert actual_ji["command_history"][0] == com

    @live_test(job="relion.postprocess")
    def test_abort_running_job(self):
        # Prepare the directory structure as if Import jobs have been run
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_job.star",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        job = proj.run_job("postprocess_manage_job.star", run_in_foreground=False)
        proj.abort_job(job.output_dir)
        with self.assertRaisesRegex(RuntimeError, f"{job.output_dir} was aborted"):
            wait_for_job_to_finish(job, error_on_abort=True)

    @live_test(job="relion.postprocess")
    @patch("pipeliner.utils.date_time_tag")
    def test_run_job_then_overwrite(self, mock_dtt):
        mock_dtt.return_value = "date_time_tag"
        # Prepare the directory structure as if Import jobs have been run
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_job.star",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        # initialize a project and run a job
        proj = PipelinerProject(pipeline_name="short_full")
        proj.run_job(jobinput="postprocess_manage_job.star", run_in_foreground=True)

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")

        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

        # rerun the job overwriting it
        proj.run_job(
            jobinput="postprocess_manage_job.star",
            overwrite="PostProcess/job020/",
            run_in_foreground=True,
        )

        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

        # check the jobinfo file
        with open(f"PostProcess/job020/{JOBINFO_FILE}", "r") as ji:
            actual_ji = json.load(ji)

        assert actual_ji["job_directory"] == "PostProcess/job020/"
        assert actual_ji["comments"] == []

        assert actual_ji["history"][0] == "date_time_tag: Scheduled"
        assert actual_ji["history"][1] == "date_time_tag: Run"
        assert actual_ji["history"][2] == "date_time_tag: Job ended; Succeeded"
        assert actual_ji["history"][3] == "date_time_tag: Scheduled overwrite last run"
        assert actual_ji["history"][4] == "date_time_tag: Run"
        assert actual_ji["history"][5] == "date_time_tag: Job ended; Succeeded"

        com = (
            f"{shutil.which('relion_postprocess')} --mask "
            "MaskCreate/job013/emd_3488_mask.mrc"
            " --i Import/job001/3488_run_half1_class001_unfil.mrc"
            " --o PostProcess/job020/postprocess --angpix 1.244 --adhoc_bfac -1000"
            " --pipeline_control PostProcess/job020/"
        )
        assert actual_ji["command_history"][0] == f"date_time_tag: {com}"
        assert actual_ji["command_history"][1] == f"date_time_tag: {com}"

    @live_test(job="relion.postprocess")
    def test_schedule_job_from_jobstar(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_job.star",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        proj.schedule_job("postprocess_manage_job.star")

        added_lines = [
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]
        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

    @live_test(job="relion.postprocess")
    def test_schedule_job_then_run(self):
        # Prepare the directory structure as if Import jobs have been run
        #
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "JobFiles/PostProcess/postprocess_manage_test.job",
            ),
            self.test_dir,
        )
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "MaskCreate/job013/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        proj = PipelinerProject(pipeline_name="short_full")
        proj.schedule_job("postprocess_manage_test.job")

        added_lines = [
            [
                "PostProcess/job020/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job020/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job020/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job020/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["PostProcess/job020/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED],
            ["MaskCreate/job013/emd_3488_mask.mrc", "PostProcess/job020/"],
            [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job020/",
            ],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess_masked.mrc"],
            ["PostProcess/job020/", "PostProcess/job020/postprocess.star"],
            ["PostProcess/job020/", "PostProcess/job020/logfile.pdf"],
        ]

        pipeline_data = clean_starfile("short_full_pipeline.star")
        for line in added_lines:
            assert line in pipeline_data, line

        job_dir = "PostProcess/job020/"
        assert os.path.isdir(job_dir)

        proj.run_schedule(
            fn_sched="schedule1",
            job_ids=["PostProcess/job020/"],
            nr_repeat=3,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=1,
        )

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))

    def test_empty_trash(self):
        my_project = PipelinerProject(make_new_project=True)
        self.make_undelete_file_structure()
        trash_files = glob(f"{TRASH_DIR}/*/*/*")
        assert len(trash_files) == 66
        my_project.empty_trash()
        trash_files = glob(f"{TRASH_DIR}/*/*/*")
        assert len(trash_files) == 0

    @staticmethod
    def test_empty_trash_error_no_files():
        my_project = PipelinerProject(make_new_project=True)
        trash_files = glob(f"{TRASH_DIR}/*/*/*")
        assert len(trash_files) == 0
        assert not my_project.empty_trash()

    @staticmethod
    def test_stop_schedule_GUI_style():
        """if the schedule is made by the GUI the RUNNING_ file is empty
        and should be deleted"""
        # make the RUNNING_ file
        touch("RUNNING_PIPELINER_default_empty")
        assert os.path.isfile("RUNNING_PIPELINER_default_empty")

        # stop the schedule
        my_project = PipelinerProject(pipeline_name="default", make_new_project=True)
        my_project.stop_schedule("empty")

        # file should be gone
        assert not os.path.isfile("RUNNING_PIPELINER_default_empty")

    def test_stop_schedule_from_API_style(self):
        """if the schedule is made by the API the RUNNING_ file
        just has a list of jobs"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data[0] == "PostProcess/job001/\n"

        # copy in the pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        my_project = PipelinerProject(pipeline_name="default")
        my_project.stop_schedule("api")

        # RUNNING file should be gone and abort trigger file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_TRIGGER)

    def test_stop_schedule_from_CL_style(self):
        """if the schedule is made by CL_relion the RUNNING_ file
        has a PID and a list of jobs, the schedule process needs to
        be killed as well"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("CL_RELION_SCHEDULE\n")
            f.write("9999999999\n")
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data == [
            "CL_RELION_SCHEDULE\n",
            "9999999999\n",
            "PostProcess/job001/\n",
        ]

        # copy in the pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        my_project = PipelinerProject(pipeline_name="default")
        my_project.stop_schedule("api")

        # RUNNING file should be gone and abort trigger file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_TRIGGER)

    def test_get_metadata_trace_two_jobs(self):
        """Get a full metadata trace with writing a file"""
        # get pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            "default_pipeline.star",
        )
        # make import directory
        os.makedirs("Import/job001")
        movie_file = os.path.join(self.test_data, "movies.star")
        shutil.copy(movie_file, "Import/job001/movies.star")
        jobstar = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        shutil.copy(jobstar, "Import/job001/job.star")

        # make motioncorr directory
        os.makedirs("MotionCorr/job002")
        movie_file = os.path.join(self.test_data, "corrected_micrographs.star")
        shutil.copy(movie_file, "MotionCorr/job002/corrected_micrographs.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/MotionCorr/motioncorr_own_job.star"
        )
        shutil.copy(jobstar, "MotionCorr/job002/job.star")

        # make ctffind directory
        ctfdir = Path("CtfFind/job003")
        ctfdir.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/CtfFind/ctffind_job.star",
            ctfdir / "job.star",
        )

        my_project = PipelinerProject(pipeline_name="default")
        with patch.object(ExternalProgram, "get_version") as mock:
            mock.return_value = "FAKE VERSION FOR TESTING"
            my_project.prepare_metadata_report("MotionCorr/job002/")
        mdfile = glob(f"{METADATA_REPORT_DIR}/*.json")[0]
        with open(mdfile, "r") as out_file:
            written = json.loads(out_file.read())
        exp_json = os.path.join(self.test_data, "Metadata/two_jobs.json")
        with open(exp_json, "r") as expfile:
            expected_md = json.loads(expfile.read())
        assert written == expected_md

    def test_get_metadata_trace_two_jobs_nowrite(self):
        """Get a full metadata trace with writing a file"""
        # get pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            "default_pipeline.star",
        )
        # make import directory
        os.makedirs("Import/job001")
        movie_file = os.path.join(self.test_data, "movies.star")
        shutil.copy(movie_file, "Import/job001/movies.star")
        jobstar = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        shutil.copy(jobstar, "Import/job001/job.star")

        # make motioncorr directory
        os.makedirs("MotionCorr/job002")
        movie_file = os.path.join(self.test_data, "corrected_micrographs.star")
        shutil.copy(movie_file, "MotionCorr/job002/corrected_micrographs.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/MotionCorr/motioncorr_own_job.star"
        )
        shutil.copy(jobstar, "MotionCorr/job002/job.star")

        # make ctffind directory
        ctfdir = Path("CtfFind/job003")
        ctfdir.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/CtfFind/ctffind_job.star",
            ctfdir / "job.star",
        )

        jobstar = os.path.join(
            self.test_data, "JobFiles/MotionCorr/motioncorr_own_job.star"
        )
        shutil.copy(jobstar, "MotionCorr/job002/job.star")

        my_project = PipelinerProject(pipeline_name="default")
        with patch.object(ExternalProgram, "get_version") as mock:
            mock.return_value = "FAKE VERSION FOR TESTING"
            my_project.prepare_metadata_report("MotionCorr/job002/")
        report_file = glob(os.path.join(self.test_dir, METADATA_REPORT_DIR, "*.json"))[
            0
        ]
        with open(report_file) as rf:
            metadata_dict = json.load(rf)

        exp_json = os.path.join(self.test_data, "Metadata/two_jobs.json")
        with open(exp_json, "r") as expfile:
            expected_md = json.loads(expfile.read())
        assert metadata_dict == expected_md

    @staticmethod
    def test_print_jobinfo_relion_job():
        out = get_job_info("relion.autopick.log")
        expected_result = JobInfo()
        expected_result.version = "0.1"
        expected_result.job_author = "Matt Iadanza"
        expected_result.programs = ["relion_autopick"]
        expected_result.short_desc = "Reference-free Laplacian of Gaussian autopicking"
        expected_result.references = [
            Ref(
                authors=[
                    "Zivanov J",
                    "Nakane T",
                    "Forsberg BO",
                    "Kimanius D",
                    "Hagen WJ",
                    "Lindahl E",
                    "Scheres SHW",
                ],
                title="New tools for automated high-resolution cryo-EM "
                "structure determination in RELION-3.",
                journal="eLife",
                year="2018",
                volume="7",
                pages="e42166",
                doi="10.7554/eLife.42166",
            ),
            Ref(
                authors="Scheres SHW",
                title="RELION: implementation of a Bayesian approach "
                "to cryo-EM structure determination.",
                journal="J Struct Biol.",
                year="2012",
                volume="180",
                issue="3",
                pages="519-30",
                doi="10.1016/j.jsb.2012.09.006",
            ),
        ]

        assert expected_result.version == out.version, "version"
        assert expected_result.job_author == out.job_author, "job_author"
        assert expected_result.programs == [x.command for x in out.programs], "programs"
        assert expected_result.short_desc == out.short_desc, "short desc"
        assert expected_result.long_desc == out.long_desc, "long desc"
        for pair in zip(expected_result.references, out.references):
            assert pair[0].__dict__ == pair[1].__dict__

    @staticmethod
    def test_print_jobinfo_relion_multibody():
        """Tested separately because it has lots of additional
        references and a long description"""
        out = get_job_info("relion.multibody.refine")
        expected_result = JobInfo()
        expected_result.version = "0.1"
        expected_result.job_author = "Matt Iadanza"
        expected_result.programs = ["relion_refine"]
        expected_result.short_desc = (
            "3D refinement with multiple independent rigid bodies"
        )
        expected_result.long_desc = (
            "Traditional image processing approaches often lead to blurred "
            "reconstructions when molecules adopt many different conformations."
            " By considering complexes to be comprised of multiple, independently"
            " moving rigid bodies, multi-body refinement in RELION enables structure"
            " determination of highly flexible complexes, while at the same time"
            " providing a characterization of the motions in the complex."
            " This method can be applied to any cryo-EM data set of flexible complexes"
            " that can be divided into two or more bodies, each with a minimum"
            " molecular weight of 100–150 kDa."
        )
        expected_result.references = [
            Ref(
                authors=[
                    "Zivanov J",
                    "Nakane T",
                    "Forsberg BO",
                    "Kimanius D",
                    "Hagen WJ",
                    "Lindahl E",
                    "Scheres SHW",
                ],
                title="New tools for automated high-resolution cryo-EM "
                "structure determination in RELION-3.",
                journal="eLife",
                year="2018",
                volume="7",
                pages="e42166",
                doi="10.7554/eLife.42166",
            ),
            Ref(
                authors=["Scheres SHW"],
                title="RELION: implementation of a Bayesian approach "
                "to cryo-EM structure determination.",
                journal="J Struct Biol.",
                year="2012",
                volume="180",
                issue="3",
                pages="519-30",
                doi="10.1016/j.jsb.2012.09.006",
            ),
            Ref(
                authors=["Nakane T", "Kimanius D", "Lindahl E", "Scheres SH"],
                title="Characterisation of molecular motions in cryo-EM "
                "single-particle data by multi-body refinement in RELION",
                journal="eLife",
                year="2018",
                volume="7",
                pages="e36861",
                doi="10.7554/eLife.36861",
            ),
            Ref(
                authors=["Scheres SH"],
                title="Processing of Structurally Heterogeneous Cryo-EM Data in RELION",
                journal="Methods Enzymol.",
                year="2016",
                volume="579",
                pages="125-57",
                doi="10.1016/bs.mie.2016.04.012",
                editor="Crowther RA",
            ),
            Ref(
                authors=["Nakene T", "Scheres SH"],
                title="Multi-body Refinement of Cryo-EM Images in RELION",
                journal="Methods in Molecular Biology",
                year="2021",
                volume="2215",
                pages="145-160",
                doi="doi.org/10.1007/978-1-0716-0966-8_7",
            ),
        ]

        assert expected_result.version == out.version, "version"
        assert expected_result.job_author == out.job_author, "job_author"
        assert expected_result.programs == [x.command for x in out.programs], "programs"
        assert expected_result.short_desc == out.short_desc, "short desc"
        assert expected_result.long_desc == out.long_desc, "long desc"
        for pair in zip(expected_result.references, out.references):
            assert pair[0].__dict__ == pair[1].__dict__

    def test_print_jobinfo_badjob(self):
        with self.assertRaises(ValueError):
            get_job_info("job.that.doesn't.exist")

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_jobstar_noproject(self):
        # get the files
        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )
        shutil.copy(jobfile, os.path.join(self.test_dir, "job.star"))

        # run print command
        commands, innodes, outnodes = get_commands_and_nodes("job.star")[:3]

        # verify command is as expected
        expected_commands = [
            "/path/to/relion_postprocess --mask Mask/emd_3488_mask.mrc"
            " --i HalfMaps/3488_run_half1_class001_unfil.mrc"
            " --o PostProcess/job000/postprocess --angpix 1.244 --adhoc_bfac -1000"
            " --pipeline_control PostProcess/job000/",
        ]

        coms = [" ".join(x) for x in commands]
        for i, com in enumerate(coms):
            assert expected_commands[i] == com

        for i, com in enumerate(expected_commands):
            assert com == coms[i]

        # check nodes
        expected_innodes = [
            ("Mask/emd_3488_mask.mrc", f"{NODE_MASK3D}.mrc"),
            (
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                f"{NODE_DENSITYMAP}.mrc.halfmap",
            ),
        ]
        expected_outnodes = [
            (
                "PostProcess/job000/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ),
            (
                "PostProcess/job000/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ),
            (
                "PostProcess/job000/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ),
            (
                "PostProcess/job000/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ),
        ]
        for node in expected_innodes:
            assert node in innodes, node
        for node in innodes:
            assert node in expected_innodes, node
        for node in expected_outnodes:
            assert node in outnodes, node
        for node in outnodes:
            assert node in expected_outnodes, node

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_runjob_noproject(self):
        # get the files
        jobfile = os.path.join(
            self.test_data, "JobFiles/InitialModel/initialmodel_3classes.job"
        )
        shutil.copy(jobfile, os.path.join(self.test_dir, "run.job"))

        # run print command
        commands, innodes, outnodes = get_commands_and_nodes("run.job")[:3]

        # verify command is as expected
        expected_commands = [
            "/path/to/relion_refine --grad --denovo_3dref"
            " --i Select/job014/particles.star"
            " --o InitialModel/job000/run --iter 200 --ctf --K 3 --sym C1"
            " --flatten_solvent --zero_mask --dont_combine_weights_via_disc"
            " --scratch_dir None --pool 3 --pad 2"
            " --particle_diameter 200 --oversampling 1 --healpix_order 1"
            " --offset_range 6 --offset_step 4.0 --j 1"
            " --pipeline_control InitialModel/job000/",
            "/path/to/rm -f InitialModel/job000/RELION_JOB_EXIT_SUCCESS",
            "/path/to/relion_align_symmetry"
            " --i InitialModel/job000/run_it200_model.star"
            " --o InitialModel/job000/initial_model.mrc --sym C1 --apply_sym"
            " --select_largest_class",
        ]

        coms = [" ".join(x) for x in commands]
        for i, com in enumerate(coms):
            assert expected_commands[i] == com

        for i, com in enumerate(expected_commands):
            assert com == coms[i]

        # check nodes
        expected_innodes = [
            (
                "Select/job014/particles.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            ),
        ]
        expected_outnodes = [
            (
                "InitialModel/job000/run_it200_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.initialmodel",
            ),
            (
                "InitialModel/job000/run_it200_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.initialmodel",
            ),
            (
                "InitialModel/job000/run_it200_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job000/run_it200_class002.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job000/run_it200_class003.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job000/initial_model.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
        ]

        for node in expected_innodes:
            assert node in innodes, node
        for node in innodes:
            assert node in expected_innodes, node
        for node in expected_outnodes:
            assert node in outnodes, node
        for node in outnodes:
            assert node in expected_outnodes, node

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_jobstar(self):
        # get the files
        pipefile = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        shutil.copy(pipefile, "default_pipeline.star")

        projfile = os.path.join(
            self.test_data, "ProjectFiles/tutorial_pipeline_projectfile.json"
        )
        shutil.copy(projfile, PROJECT_FILE)

        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )
        shutil.copy(jobfile, os.path.join(self.test_dir, "job.star"))
        # initialize project and run print command
        commands, innodes, outnodes = get_commands_and_nodes("job.star")[:3]

        # verify command is as expected
        expected_commands = [
            "/path/to/relion_postprocess --mask Mask/emd_3488_mask.mrc"
            " --i HalfMaps/3488_run_half1_class001_unfil.mrc"
            " --o PostProcess/job032/postprocess --angpix 1.244 --adhoc_bfac -1000"
            " --pipeline_control PostProcess/job032/",
        ]

        # check commands
        coms = [" ".join(x) for x in commands]
        for i, com in enumerate(coms):
            assert expected_commands[i] == com

        for i, com in enumerate(expected_commands):
            assert com == coms[i]

        # check nodes
        expected_innodes = [
            ("Mask/emd_3488_mask.mrc", f"{NODE_MASK3D}.mrc"),
            (
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                f"{NODE_DENSITYMAP}.mrc.halfmap",
            ),
        ]
        expected_outnodes = [
            (
                "PostProcess/job032/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ),
            (
                "PostProcess/job032/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ),
            (
                "PostProcess/job032/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ),
            (
                "PostProcess/job032/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ),
        ]
        for node in expected_innodes:
            assert node in innodes, node
        for node in innodes:
            assert node in expected_innodes, node
        for node in expected_outnodes:
            assert node in outnodes, node
        for node in outnodes:
            assert node in expected_outnodes, node

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_runjob(self):
        # get the files
        jobfile = os.path.join(
            self.test_data, "JobFiles/InitialModel/initialmodel_3classes.job"
        )
        shutil.copy(jobfile, os.path.join(self.test_dir, "run.job"))

        pipefile = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        shutil.copy(pipefile, "default_pipeline.star")

        shutil.copy(
            os.path.join(
                self.test_data,
                "ProjectFiles/tutorial_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        # run print command
        commands, innodes, outnodes = get_commands_and_nodes("run.job")[:3]

        # verify command is as expected
        expected_commands = [
            "/path/to/relion_refine --grad --denovo_3dref"
            " --i Select/job014/particles.star"
            " --o InitialModel/job032/run --iter 200 --ctf --K 3 --sym C1"
            " --flatten_solvent --zero_mask --dont_combine_weights_via_disc"
            " --scratch_dir None --pool 3 --pad 2"
            " --particle_diameter 200 --oversampling 1 --healpix_order 1"
            " --offset_range 6 --offset_step 4.0 --j 1"
            " --pipeline_control InitialModel/job032/",
            "/path/to/rm -f InitialModel/job032/RELION_JOB_EXIT_SUCCESS",
            "/path/to/relion_align_symmetry"
            " --i InitialModel/job032/run_it200_model.star"
            " --o InitialModel/job032/initial_model.mrc --sym C1 --apply_sym"
            " --select_largest_class",
        ]

        coms = [" ".join(x) for x in commands]
        for i, com in enumerate(coms):
            assert expected_commands[i] == com

        for i, com in enumerate(expected_commands):
            assert com == coms[i]

        # check nodes
        expected_innodes = [
            (
                "Select/job014/particles.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            ),
        ]
        expected_outnodes = [
            (
                "InitialModel/job032/run_it200_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.initialmodel",
            ),
            (
                "InitialModel/job032/run_it200_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.initialmodel",
            ),
            (
                "InitialModel/job032/run_it200_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job032/run_it200_class002.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job032/run_it200_class003.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
            (
                "InitialModel/job032/initial_model.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ),
        ]
        for node in expected_innodes:
            assert node in innodes, node
        for node in innodes:
            assert node in expected_innodes, node
        for node in expected_outnodes:
            assert node in outnodes, node
        for node in outnodes:
            assert node in expected_outnodes, node

    @patch("pipeliner.api.manage_project.prepare_script_archive")
    @patch(
        "pipeliner.api.manage_project.PipelinerProject.add_missing_nodes_from_all_jobs"
    )
    def test_archiving_script(self, update_mock, archive_mock):
        archive_mock.return_value = "archive"
        shutil.copy(
            Path(self.test_data) / "Pipelines/short_full_pipeline.star",
            "default_pipeline.star",
        )
        proj = PipelinerProject()
        proj.create_archive("PostProcess/job017/", full=False)
        update_mock.assert_called_once()
        archive_mock.assert_called_once()
        assert archive_mock.call_args[0][0].name == "PostProcess/job017/"

    @patch("pipeliner.api.manage_project.prepare_full_archive")
    @patch(
        "pipeliner.api.manage_project.PipelinerProject.add_missing_nodes_from_all_jobs"
    )
    def test_archiving_full(self, update_mock, archive_mock):
        archive_mock.return_value = "archive"
        shutil.copy(
            Path(self.test_data) / "Pipelines/short_full_pipeline.star",
            "default_pipeline.star",
        )
        proj = PipelinerProject()
        proj.create_archive("PostProcess/job017/", full=True)
        update_mock.assert_called_once()
        archive_mock.assert_called_once()
        assert archive_mock.call_args[0][0].name == "PostProcess/job017/"

    def test_proc_name_parse_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        proj = PipelinerProject()
        actual_name = "Class3D/job018/"

        variations = {
            "full name": "Class3D/job018/",
            "missing slash": "Class3D/job018",
            "no type": "job018/",
            "no type no slash": "job018",
            "alias": "Class3D/first_exhaustive/",
            "alias no slash": "Class3D/first_exhaustive",
            "extra space": "Class3D/job018/ ",
            "just a number": "18",
        }

        for var in variations:
            fixed = proj.parse_procname(variations[var])
            assert fixed == actual_name, (fixed, var)

    def test_proc_name_parse_from_trash(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        proj = PipelinerProject()
        os.makedirs(f"{TRASH_DIR}/Class3D/job180/")

        actual_name = "Class3D/job180/"

        trash_vars = {
            "full name": "Class3D/job180/",
            "missing slash": "Class3D/job180",
            "no type": "job180/",
            "no type no slash": "job180",
            "no type no slash space": "job180 ",
            "just a number": "180",
        }
        for trash_var in trash_vars:
            from_trash = proj.parse_procname(trash_vars[trash_var], search_trash=True)
            assert from_trash == actual_name, (from_trash, trash_var)
            from_trash = proj.parse_procname(
                f"{TRASH_DIR}/" + trash_vars[trash_var],
                search_trash=True,
            )
            assert from_trash == actual_name, (from_trash, trash_var)

    def test_proc_name_trashed_jobs_raise_errors(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        proj = PipelinerProject()
        os.makedirs(f"{TRASH_DIR}/Class3D/job180/")

        actual_name = "Class3D/job180/"

        trash_vars = {
            "full name": "Class3D/job180/",
            "missing slash": "Class3D/job180",
            "no type": "job180/",
            "no type no slash": "job180",
            "no type no slash space": "job180 ",
            "just a number": "180",
        }
        for trash_var in trash_vars:
            with self.assertRaises(ValueError):
                from_trash = proj.parse_procname(trash_vars[trash_var])
                assert from_trash == actual_name, (from_trash, trash_var)
                from_trash = proj.parse_procname(
                    f"{TRASH_DIR}/" + trash_vars[trash_var]
                )
                assert from_trash == actual_name, (from_trash, trash_var)

    def test_parse_procname_list(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        jobs_list = [
            "Select/job014/",
            "Class2D/job015/",
            "Select/job016/",
            "InitialModel/job017/",
            "Class3D/job018/",
            "Select/job019/",
            "Extract/job020/",
            "Refine3D/job021/",
            "MaskCreate/job022/",
        ]
        proj = PipelinerProject()

        found_list = proj.parse_proclist(jobs_list)
        assert found_list == jobs_list

        altered_jobs_list = [
            "Select/job014",
            "job015/",
            "job016",
            "job017/",
            "/job018/",
            "Select/job019/ ",
            "job020 ",
            "job021/",
            "MaskCreate/job022/",
        ]
        found_list = proj.parse_proclist(altered_jobs_list)
        assert found_list == jobs_list

    # This test needs to be updated when we have a ccpem-pipeliner version pipeline
    # for testing, the current test uses a relion 4.0 pipeline which DOES need
    # conversion
    # def test_convert_pipeline_convert_error(self):
    #     """Convert error raised when pipeline is already new style"""
    #     # copy in the pipeline - new version
    #     shutil.copy(
    #         os.path.join(
    #           self.test_data, "Pipelines/converted_relion31_pipeline.star"
    #         ),
    #         os.path.join(self.test_dir, "default_pipeline.star"),
    #     )
    #     assert not convert_pipeline("default")

    def test_convert_pipeline(self):
        """Convert an old style pipeline to a new style one"""
        # copy in the pipeline - old version
        make_conversion_file_structure()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003/micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/relion31_tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        # copy in the pipeline - new version
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/converted_relion31_pipeline.star"),
            self.test_dir,
        )

        result = convert_pipeline()
        assert result

        # get the pipeline data
        compare_starfiles("default_pipeline.star", "converted_relion31_pipeline.star")

    def setup_comment_test(self, with_comment=False):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        shutil.copy(
            os.path.join(
                self.test_data,
                "ProjectFiles/tutorial_pipeline_projectfile.json",
            ),
            os.path.join(self.test_dir, PROJECT_FILE),
        )

        os.makedirs("Class2D/job008")

        if with_comment:
            jifo_file = os.path.join(
                self.test_data, "ProjectFiles/class2d_jobinfo_com.json"
            )
        else:
            jifo_file = os.path.join(
                self.test_data, "ProjectFiles/class2d_jobinfo.json"
            )

        shutil.copy(jifo_file, f"Class2D/job008/{JOBINFO_FILE}")

    def test_edit_comment_new(self):
        self.setup_comment_test()
        proj = PipelinerProject()
        proj.edit_comment(
            "Class2D/job008/", comment="Here is the new comment", overwrite=False
        )
        with open(f"Class2D/job008/{JOBINFO_FILE}", "r") as wrote:
            wrote_dict = json.load(wrote)
        assert wrote_dict["comments"] == ["Here is the new comment"]

    def test_edit_comment_append(self):
        self.setup_comment_test(with_comment=True)
        proj = PipelinerProject()
        proj.edit_comment(
            "Class2D/job008/", comment="Here is the new comment", overwrite=False
        )
        with open(f"Class2D/job008/{JOBINFO_FILE}", "r") as wrote:
            wrote_dict = json.load(wrote)
        assert wrote_dict["comments"] == [
            "Hapa kuna maoni ya asili",
            "Here is the new comment",
        ]

    def test_edit_comment_overwrite(self):
        self.setup_comment_test(with_comment=True)
        proj = PipelinerProject()
        proj.edit_comment(
            "Class2D/job008/", comment="Here is the new comment", overwrite=True
        )
        with open(f"Class2D/job008/{JOBINFO_FILE}", "r") as wrote:
            wrote_dict = json.load(wrote)
        assert wrote_dict["comments"] == ["Here is the new comment"]

    def test_edit_comment_nothing_to_do(self):
        self.setup_comment_test()
        proj = PipelinerProject()
        with self.assertRaises(ValueError):
            proj.edit_comment("Class2D/job008/")

    def setup_comment_search_test(self):
        self.setup_comment_test()

        with ProjectGraph() as pipeline:
            jobinfo = os.path.join(self.test_data, "ProjectFiles/jobinfo_comment0.json")
            for job in pipeline.process_list:
                os.makedirs(job.name, exist_ok=True)
                shutil.copy(jobinfo, os.path.join(job.name, JOBINFO_FILE))

        comment_jobs = {
            "Import/job001/": 1,
            "AutoPick/job010/": 2,
            "AutoPick/job011/": 3,
            "Extract/job012/": 3,
        }

        for job in comment_jobs:
            n = comment_jobs[job]
            infofile = os.path.join(
                self.test_data, f"ProjectFiles/jobinfo_comment{n}.json"
            )
            shutil.copy(infofile, os.path.join(job, JOBINFO_FILE))

    def test_find_with_comment_contains(self):
        self.setup_comment_search_test()
        proj = PipelinerProject()
        found = proj.find_job_by_comment(contains=["Find this"])
        assert found == ["Import/job001/", "AutoPick/job010/"]

    def test_find_with_comment_contains_nonefound(self):
        self.setup_comment_search_test()
        proj = PipelinerProject()
        found = proj.find_job_by_comment(contains=["NOT HERE!!"])
        assert found == []

    def test_find_with_comment_contains_with_type(self):
        self.setup_comment_search_test()
        proj = PipelinerProject()
        found = proj.find_job_by_comment(contains=["this one"], job_type="autopick")
        assert found == ["AutoPick/job010/", "AutoPick/job011/"], found

    def test_find_with_comment_not_contains(self):
        self.setup_comment_search_test()
        proj = PipelinerProject()
        found = proj.find_job_by_comment(not_contains=["Don't", "Original"])
        assert len(found) == 26, len(found)

    def test_find_wth_comment_contains_and_not_contains(self):
        self.setup_comment_search_test()
        proj = PipelinerProject()
        found = proj.find_job_by_comment(contains=["Find this"], not_contains=["mbili"])
        assert found == ["Import/job001/"]

    def setup_compare_parameters_test(self):
        pipeline = os.path.join(self.test_data, "Pipelines/compare_jobs_pipeline.star")
        shutil.copy(pipeline, "default_pipeline.star")

        projfile = os.path.join(
            self.test_data, "ProjectFiles/tutorial_pipeline_projectfile.json"
        )
        shutil.copy(projfile, PROJECT_FILE)

        for n in [3, 4, 5]:
            ctfdir = f"CtfFind/job{n:03d}"
            os.makedirs(ctfdir)
            ctf_job_file = os.path.join(
                self.test_data, f"JobFiles/CtfFind/ctffind_compare{n}_job.star"
            )
            shutil.copy(ctf_job_file, f"{ctfdir}/job.star")

    def test_compare_params(self):
        self.setup_compare_parameters_test()
        proj = PipelinerProject()
        comp_jobs = [f"CtfFind/job{x:03d}/" for x in (3, 4, 5)]
        compare = proj.compare_job_parameters(comp_jobs)
        # skip these parameters because they"re set by ENVVARs
        skipcoms = ["qsubscript", "fn_ctffind_exe", "qsub", "queuename"]
        exp_dict = {
            "ctf_win": ["50", "-1", "-1"],
            "do_phaseshift": ["No", "No", "No"],
            "do_queue": ["No", "No", "Yes"],
            "input_star_mics": ["", "", ""],
            "min_dedicated": ["1", "1", "1"],
            "nr_mpi": ["1", "1", "1"],
            "other_args": ["", "", ""],
            "phase_max": ["180", "180", "90"],
            "phase_min": ["0", "0", "0"],
            "phase_step": ["10", "10", "11"],
            "slow_search": ["No", "No", "No"],
            "use_given_ps": ["Yes", "No", "No"],
            "use_noDW": ["No", "No", "No"],
            "box": ["512", "512", "512"],
            "dast": ["100", "100", "100"],
            "dfmax": ["50000", "50000", "50000"],
            "dfmin": ["5000", "5000", "5000"],
            "dfstep": ["500", "500", "500"],
            "qsub": ["qsub", "qsub", "qsub"],
            "resmax": ["5", "5", "5"],
            "resmin": ["30", "30", "30"],
            "_rlnJobTypeLabel": [
                "relion.ctffind.ctffind4",
                "relion.ctffind.ctffind4",
                "relion.ctffind.ctffind4",
            ],
            "_rlnJobIsContinue": ["0", "0", "0"],
            "_rlnJobIsTomo": ["0", "0", "0"],
        }
        for param in compare:
            if param not in skipcoms:
                assert compare[param] == exp_dict[param], param
        for param in exp_dict:
            if param not in skipcoms:
                assert compare[param] == exp_dict[param], param

    def test_compare_params_error_job_not_found(self):
        self.setup_compare_parameters_test()
        proj = PipelinerProject()
        comp_jobs = [f"CtfFind/job{x:03d}/" for x in (3, 4, 5, 6)]
        with self.assertRaises(ValueError):
            proj.compare_job_parameters(comp_jobs)

    def test_compare_params_error_jobtypes_dont_match(self):
        self.setup_compare_parameters_test()
        proj = PipelinerProject()
        comp_jobs = [f"CtfFind/job{x:03d}/" for x in (3, 4, 5)]
        js = JobStar("CtfFind/job003/job.star")
        js.modify({"_rlnJobTypeLabel": "bad"})
        js.write("CtfFind/job003/job.star")
        with self.assertRaises(ValueError):
            proj.compare_job_parameters(comp_jobs)

    def test_get_job(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )

        proj = PipelinerProject(pipeline_name="short_full")
        os.makedirs("Import/job001")
        f = os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        shutil.copy(f, "Import/job001/job.star")
        job = proj.get_job("Import/job001/")
        assert job.PROCESS_NAME == "relion.import.movies"
        exp_jobops = {
            "fn_in_raw": SearchStringJobOption,
            "is_multiframe": BooleanJobOption,
            "optics_group_name": StringJobOption,
            "fn_mtf": InputNodeJobOption,
            "angpix": FloatJobOption,
            "kV": IntJobOption,
            "Cs": FloatJobOption,
            "Q0": FloatJobOption,
            "beamtilt_x": FloatJobOption,
            "beamtilt_y": FloatJobOption,
            "is_synthetic": BooleanJobOption,
            "microscope_data_file": InputNodeJobOption,
            "validate_output": BooleanJobOption,
        }
        exp_vals = {
            "fn_in_raw": "Movies/*.tiff",
            "JobType": "relion.import.movies",
            "Continued": False,
            "is_multiframe": "Yes",
            "optics_group_name": "opticsGroup1",
            "fn_mtf": "",
            "angpix": 0.885,
            "kV": 200,
            "Cs": 1.4,
            "Q0": 0.1,
            "beamtilt_x": 0.0,
            "beamtilt_y": 0.0,
            "is_synthetic": "No",
            "microscope_data_file": "",
            "validate_output": "Yes",
        }
        for name, jobop in job.joboptions.items():
            assert type(jobop) is exp_jobops[name]
            assert jobop.value == exp_vals[name]

    def setup_summary_file_test(self, extra_file=None):
        sumfile = os.path.join(self.test_data, "Metadata/summary_file_4.star")
        shutil.copy(sumfile, SUMMARY_DATA_FILE)
        if extra_file is not None:
            efdir = os.path.dirname(extra_file)
            os.makedirs(efdir, exist_ok=True)
            touch(extra_file)

    def test_get_summary_data_ref(self):
        self.setup_summary_file_test()
        headers, data = get_ref_reports_from_summary_file()
        assert headers == SUMMARY_DATA_HEADERS["reference_reports"]
        assert data == [
            ["ReferenceReports/20230322122525_job002.json", "MotionCorr/job002/", "2"],
            ["ReferenceReports/20230322123534_job003.json", "CtfFind/job003/", "3"],
            ["ReferenceReports/20230322124555_job004.json", "AutoPick/job004/", "4"],
            ["ReferenceReports/20230322125784_job005.json", "Extract/job005/", "5"],
        ]

    def test_get_summary_data_md(self):
        self.setup_summary_file_test()
        headers, data = get_metadata_reports_from_summary_file()
        assert headers == SUMMARY_DATA_HEADERS["metadata_reports"]
        assert data == [
            ["MetadataReports/20230322122525_job002.json", "MotionCorr/job002/", "1"],
            ["MetadataReports/20230322123534_job003.json", "CtfFind/job003/", "2"],
            ["MetadataReports/20230322124555_job004.json", "AutoPick/job004/", "1"],
            ["MetadataReports/20230322125784_job005.json", "Extract/job005/", "4"],
        ]

    def test_get_summary_data_archive(self):
        self.setup_summary_file_test()
        headers, data = get_archives_list_from_summary_file()
        assert headers == SUMMARY_DATA_HEADERS["project_archives"]
        assert data == [
            [
                "ProjectArchives/20230322122525_job002.tar.gz",
                "MotionCorr/job002/",
                "full",
            ],
            ["ProjectArchives/20230322123534_job003.json", "CtfFind/job003/", "full"],
            [
                "ProjectArchives/20230322124555_job004.json",
                "AutoPick/job004/",
                "simple",
            ],
            ["ProjectArchives/20230322125784_job005", "Extract/job005/", "full"],
        ]

    def test_remove_md_report(self):
        testfile = "MetadataReports/20230322122525_job002.json"
        self.setup_summary_file_test(testfile)
        delete_summary_data_metadata_report(testfile)
        data = get_metadata_reports_from_summary_file()[1]
        assert data == [
            ["MetadataReports/20230322123534_job003.json", "CtfFind/job003/", "2"],
            ["MetadataReports/20230322124555_job004.json", "AutoPick/job004/", "1"],
            ["MetadataReports/20230322125784_job005.json", "Extract/job005/", "4"],
        ]
        assert not os.path.isfile(testfile)

    def test_remove_ref_report(self):
        testfile = "ReferenceReports/20230322124555_job004.json"
        self.setup_summary_file_test(testfile)
        delete_summary_data_reference_report(testfile)
        data = get_ref_reports_from_summary_file()[1]
        assert data == [
            ["ReferenceReports/20230322122525_job002.json", "MotionCorr/job002/", "2"],
            ["ReferenceReports/20230322123534_job003.json", "CtfFind/job003/", "3"],
            ["ReferenceReports/20230322125784_job005.json", "Extract/job005/", "5"],
        ]
        assert not os.path.isfile(testfile)

    def test_remove_archive_file(self):
        testfile = "ProjectArchives/20230322122525_job002.tar.gz"
        self.setup_summary_file_test(testfile)
        delete_summary_data_archive(testfile)
        data = get_archives_list_from_summary_file()[1]
        assert data == [
            ["ProjectArchives/20230322123534_job003.json", "CtfFind/job003/", "full"],
            [
                "ProjectArchives/20230322124555_job004.json",
                "AutoPick/job004/",
                "simple",
            ],
            ["ProjectArchives/20230322125784_job005", "Extract/job005/", "full"],
        ]
        assert not os.path.isfile(testfile)

    def test_remove_archive_dir(self):
        testdir = "ProjectArchives/20230322125784_job005"
        os.makedirs(testdir)
        self.setup_summary_file_test()
        delete_summary_data_archive(testdir)
        data = get_archives_list_from_summary_file()[1]
        assert data == [
            [
                "ProjectArchives/20230322122525_job002.tar.gz",
                "MotionCorr/job002/",
                "full",
            ],
            ["ProjectArchives/20230322123534_job003.json", "CtfFind/job003/", "full"],
            [
                "ProjectArchives/20230322124555_job004.json",
                "AutoPick/job004/",
                "simple",
            ],
        ]
        assert not os.path.isdir(testdir)

    @staticmethod
    def test_empiar_deposition():
        with patch("pipeliner.api.manage_project.prepare_empiar_deposition") as mock:
            mock.return_value = "deposition_file.json"
            proj = PipelinerProject(make_new_project=True)
            dep = proj.prepare_deposition(
                depo_type="empiar", terminal_job="PostProcess/job030/"
            )
            assert dep == "deposition_file.json"

    @staticmethod
    def test_onedep_deposition():
        with patch("pipeliner.api.manage_project.OneDepDeposition") as mock:
            proj = PipelinerProject(make_new_project=True)
            proj.prepare_deposition(
                depo_type="onedep", terminal_job="PostProcess/job030/", depo_id="1ABC"
            )
            mock.assert_called_once_with(terminal_job="PostProcess/job030/")
            mock.return_value.prepare_deposition.assert_called_once()
            mock.return_value.write_deposition_cif_file.assert_called_once_with(
                depo_id="1ABC"
            )

    @staticmethod
    def make_deletion_files():
        rmdir = Path("Refine3D/job001")
        keepdir = Path("Refine3D/job002")
        for dir in (rmdir, keepdir):
            trash = Path(TRASH_DIR) / dir
            clean = Path(CLEANUP_DIR) / dir
            trash.mkdir(parents=True)
            clean.mkdir(parents=True)

        trash_files = [Path(TRASH_DIR) / rmdir / f"{x}.txt" for x in range(10)]
        keep_files = [Path(TRASH_DIR) / keepdir / f"{x}.txt" for x in range(10)]
        clean_files = [Path(CLEANUP_DIR) / rmdir / f"{x}.txt" for x in range(10)]
        keep_clean_files = [Path(CLEANUP_DIR) / keepdir / f"{x}.txt" for x in range(10)]
        for f in trash_files + keep_files + clean_files + keep_clean_files:
            f.touch()
        return trash_files, keep_files, clean_files, keep_clean_files

    @staticmethod
    def test_list_cleaned_and_get_deleted_with_no_trashdir():
        assert get_deleted_jobs_and_sizes() == {}
        assert get_cleaned_up_jobs_and_sizes() == {}

    def test_list_trashed_files(self):
        self.make_deletion_files()
        assert get_deleted_jobs_and_sizes() == {
            "Refine3D/job001/": "0 B",
            "Refine3D/job002/": "0 B",
        }

    def test_list_cleaned_files_without_deleted(self):
        self.make_deletion_files()
        assert get_cleaned_up_jobs_and_sizes() == {}

    def test_list_cleaned_files_with_deleted(self):
        self.make_deletion_files()
        assert get_cleaned_up_jobs_and_sizes(show_deleted=True) == {
            "Refine3D/job001/": "0 B",
            "Refine3D/job002/": "0 B",
        }

    def test_delete_trashed_file(self):
        trashed, keep, cleaned, keep_cleaned = self.make_deletion_files()
        delete_trashed_job_files("Refine3D/job001/")
        for f in trashed + cleaned:
            assert not f.is_file()
        for f in keep + keep_cleaned:
            assert f.is_file()

    def test_delete_cleaned_files(self):
        trashed, keep, cleaned, keep_cleaned = self.make_deletion_files()
        delete_cleaned_job_files("Refine3D/job001")
        for f in trashed + keep + keep_cleaned:
            assert f.is_file()
        for f in cleaned:
            assert not f.is_file()

    def test_delete_all_trashed(self):
        trashed, keep, cleaned, keep_cleaned = self.make_deletion_files()
        delete_all_trashed_job_files()
        for f in trashed + keep + cleaned + keep_cleaned:
            assert not f.is_file()

    def test_delete_all_cleaned(self):
        trashed, keep, cleaned, keep_cleaned = self.make_deletion_files()
        delete_all_cleaned_job_files()
        for f in cleaned + keep_cleaned:
            assert not f.is_file()

    def test_adding_missing_nodes_to_converted_relion_pipeline(self):
        get_relion_tutorial_data(relion_version=5)
        old_pipe = cif.read_file("default_pipeline.star")

        proj = PipelinerProject()
        updates = proj.add_missing_nodes_from_all_jobs()

        new_pipe = cif.read_file("default_pipeline.star")
        old_nodes = old_pipe.find_block("pipeline_nodes").find(
            "_rln", ["PipeLineNodeName", "PipeLineNodeTypeLabel"]
        )
        new_nodes = new_pipe.find_block("pipeline_nodes").find(
            "_rln", ["PipeLineNodeName"]
        )

        new_nodes = [x[0] for x in new_nodes]
        old_nodes = [x[0] for x in old_nodes]

        added_nodes = [
            "mtf_k2_200kV.star",
            "Movies/gain.mrc",
            "AutoPick/job010/logfile.pdf",
            "AutoPick/job010/model_epoch10.sav",
            "InitialModel/job015/run_it100_optimiser.star",
            "InitialModel/job015/run_it100_data.star",
            "InitialModel/job015/run_it100_class001.mrc",
            "Refine3D/job019/run_half2_class001_unfil.mrc",
            "Refine3D/job025/run_half2_class001_unfil.mrc",
            "Refine3D/job029/run_half2_class001_unfil.mrc",
            "Ecoli.fasta",
            "DynaMight/job034//forward_deformations/checkpoints/checkpoint_final.pth",
            "DynaMight/job034/forward_deformations/checkpoints/checkpoint_final.pth",
        ]
        # doubling of the last one is from a typo in the relion job.star file
        for i in added_nodes:
            assert i in new_nodes
            assert i not in old_nodes

        assert set(updates) == set(added_nodes)

        new_out_edges = new_pipe.find_block("pipeline_output_edges").find(
            "_rln", ["PipeLineEdgeToNode", "PipeLineEdgeProcess"]
        )
        new_outs = [(x[0], x[1]) for x in new_out_edges]

        old_out_edges = old_pipe.find_block("pipeline_output_edges").find(
            "_rln", ["PipeLineEdgeToNode", "PipeLineEdgeProcess"]
        )
        old_outs = [(x[0], x[1]) for x in old_out_edges]
        added_out_edges = [
            ("AutoPick/job010/logfile.pdf", "AutoPick/job010/"),
            ("AutoPick/job010/model_epoch10.sav", "AutoPick/job010/"),
            ("InitialModel/job015/run_it100_optimiser.star", "InitialModel/job015/"),
            ("InitialModel/job015/run_it100_data.star", "InitialModel/job015/"),
            ("InitialModel/job015/run_it100_class001.mrc", "InitialModel/job015/"),
            ("Refine3D/job019/run_half2_class001_unfil.mrc", "Refine3D/job019/"),
            ("Refine3D/job025/run_half2_class001_unfil.mrc", "Refine3D/job025/"),
            ("Refine3D/job029/run_half2_class001_unfil.mrc", "Refine3D/job029/"),
            (
                "DynaMight/job034/forward_deformations/checkpoints/"  # // is a typo in
                "checkpoint_final.pth",  # the job.star file
                "DynaMight/job034/",
            ),
        ]
        for i in added_out_edges:
            assert i in new_outs
            assert i not in old_outs

        new_in_edges = new_pipe.find_block("pipeline_input_edges").find(
            "_rln", ["PipeLineEdgeFromNode", "PipeLineEdgeProcess"]
        )
        new_ins = [(x[0], x[1]) for x in new_in_edges]

        old_in_edges = old_pipe.find_block("pipeline_input_edges").find(
            "_rln", ["PipeLineEdgeFromNode", "PipeLineEdgeProcess"]
        )
        old_ins = [(x[0], x[1]) for x in old_in_edges]

        added_ins = [
            ("mtf_k2_200kV.star", "Import/job001/"),
            ("Movies/gain.mrc", "MotionCorr/job002/"),
            ("AutoPick/job010/model_epoch10.sav", "AutoPick/job011/"),
            ("mtf_k2_200kV.star", "PostProcess/job021/"),
            ("mtf_k2_200kV.star", "PostProcess/job026/"),
            ("mtf_k2_200kV.star", "PostProcess/job030/"),
            ("mtf_k2_200kV.star", "LocalRes/job031/"),
            ("Ecoli.fasta", "ModelAngelo/job033/"),
            (
                "DynaMight/job034//forward_deformations/checkpoints/"  # // is a typo
                "checkpoint_final.pth",
                "DynaMight/job034/",  # in the relion
            ),  # job.star file
            ("mtf_k2_200kV.star", "PostProcess/job035/"),
        ]
        for i in added_ins:
            assert i in new_ins
            assert i not in old_ins


if __name__ == "__main__":
    unittest.main()
