#
#    Copyright (C) 2024 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
import tempfile
import unittest
from unittest.mock import patch
from pathlib import Path

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.data_structure import NODE_DISPLAY_FILE
from pipeliner.results_display_objects import ResultsDisplayPending
from pipeliner.scripts import regenerate_results
from pipeliner.starfile_handler import DataStarFile
from pipeliner_tests import test_data


class RegenerateResultsTest(unittest.TestCase):
    def setUp(self):
        """
        Set up test data and output directories.
        """
        self.test_data = Path(test_data.__file__).parent
        test_dir = Path(tempfile.mkdtemp(prefix="pipeliner_regenerate_results_"))
        # Change to test directory
        orig_dir = Path.cwd()
        os.chdir(test_dir)
        self.addCleanup(os.chdir, orig_dir)
        self.addCleanup(shutil.rmtree, path=test_dir, ignore_errors=True)

        # Copy an example pipeline into the test dir
        shutil.copy(
            self.test_data / "Pipelines" / "short_3jobs_pipeline.star",
            "default_pipeline.star",
        )

    def test_generating_results_nonexistent_job(self):
        with self.assertRaisesRegex(ValueError, "Job BadJobName not found"):
            regenerate_results.main(["--jobs", "BadJobName"])

    def test_generating_results_nonexistent_job_with_ignore_errors(self):
        with self.assertLogs(
            "pipeliner.scripts.regenerate_results", level="ERROR"
        ) as cm:
            regenerate_results.main(["--jobs", "BadJobName", "--ignore_errors"])
            assert ("Error generating results for job BadJobName") in cm.output[0]

    @patch("pipeliner.project_graph.ProjectGraph.find_node")
    def test_generating_results_bad_node(self, mock_find_node):
        mock_find_node.return_value.write_default_result_file.side_effect = ValueError(
            "Bad node"
        )
        with self.assertRaisesRegex(ValueError, "Bad node"):
            regenerate_results.main(["--nodes", "node/name"])

    @patch("pipeliner.project_graph.ProjectGraph.find_node")
    def test_generating_results_bad_node_with_ignore_errors(self, mock_find_node):
        mock_find_node.return_value.write_default_result_file.side_effect = ValueError(
            "Bad node"
        )
        with self.assertLogs(
            "pipeliner.scripts.regenerate_results", level="ERROR"
        ) as cm:
            regenerate_results.main(["--nodes", "node/name", "--ignore_errors"])
            print(cm.output)
            assert (
                "ERROR:pipeliner.scripts.regenerate_results:Error generating results "
                "for node node/name"
            ) in str(cm.output[0])

    def test_generating_results_single_job_missing_all_files(self):
        Path("Import/job001").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_job.star",
            "Import/job001/job.star",
        )
        regenerate_results.main(["--jobs", "Import/job001"])

        job = PipelinerProject().get_job("Import/job001")
        dispobjs = job.load_results_display_files()
        assert len(dispobjs) == 1
        assert type(dispobjs[0]) is ResultsDisplayPending
        assert dispobjs[0].message == "Error creating montage results display"
        assert "No such file" in dispobjs[0].reason

    def test_generating_results_single_job_no_movies(self):
        job_dir = Path("Import/job001")
        job_dir.mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_job.star",
            job_dir / "job.star",
        )
        # Make an empty movies.star file
        (job_dir / "movies.star").write_text(
            "data_movies\n\nloop_\n_rlnMicrographMovieName #1\n_rlnOpticsGroup #2\n"
        )
        regenerate_results.main(["--jobs", "Import/job001"])

        job = PipelinerProject().get_job("Import/job001")
        dispobjs = job.load_results_display_files()
        assert len(dispobjs) == 1
        assert type(dispobjs[0]) is ResultsDisplayPending
        assert dispobjs[0].message == "Error creating montage results display"
        assert "No particles were found in file:" in dispobjs[0].reason

    def test_generating_results_multiple_jobs_missing_all_files(self):
        Path("Import/job001").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_job.star",
            "Import/job001/job.star",
        )
        Path("MotionCorr/job002").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "MotionCorr" / "motioncorr_own_job.star",
            "MotionCorr/job002/job.star",
        )
        regenerate_results.main(["--jobs", "Import/job001", "MotionCorr/job002"])

        import_job = PipelinerProject().get_job("Import/job001")
        import_dispobjs = import_job.load_results_display_files()
        assert len(import_dispobjs) == 1
        assert type(import_dispobjs[0]) is ResultsDisplayPending
        assert import_dispobjs[0].message == "Error creating montage results display"
        assert "No such file" in import_dispobjs[0].reason

        mocorr_job = PipelinerProject().get_job("MotionCorr/job002")
        mocorr_dispobjs = mocorr_job.load_results_display_files()
        assert len(mocorr_dispobjs) == 1
        assert type(mocorr_dispobjs[0]) is ResultsDisplayPending
        assert mocorr_dispobjs[0].message == "Error creating montage results display"
        assert "No such file" in mocorr_dispobjs[0].reason

    def test_generating_results_single_node_missing_all_files(self):
        regenerate_results.main(["--nodes", "CtfFind/job003/micrographs_ctf.star"])

        node_display_file = DataStarFile(NODE_DISPLAY_FILE)
        assert node_display_file.count_block() == 1
        assert node_display_file.column_as_list(
            blockname=None, colname="_pipelinerDisplayFile"
        ) == ["CtfFind/job003/NodeDisplay/results_display000_pending.json"]
        assert (
            "No such file"
            in Path(
                "CtfFind/job003/NodeDisplay/results_display000_pending.json"
            ).read_text()
        )

    def test_generating_results_multiple_nodes_missing_all_files(self):
        regenerate_results.main(
            [
                "--nodes",
                "Import/job001/movies.star",
                "CtfFind/job003/micrographs_ctf.star",
                "CtfFind/job003/logfile.pdf",
                "MotionCorr/job002/logfile.pdf",
            ]
        )
        node_display_file = DataStarFile(NODE_DISPLAY_FILE)
        assert node_display_file.count_block() == 4
        assert node_display_file.column_as_list(
            blockname=None, colname="_pipelinerDisplayFile"
        ) == [
            "Import/job001/NodeDisplay/results_display000_pending.json",
            "CtfFind/job003/NodeDisplay/results_display000_pending.json",
            "CtfFind/job003/NodeDisplay/results_display001_pdffile.json",
            "MotionCorr/job002/NodeDisplay/results_display000_pdffile.json",
        ]

    def test_generating_results_multiple_jobs_and_nodes(self):
        Path("Import/job001").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_job.star",
            "Import/job001/job.star",
        )
        Path("MotionCorr/job002").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "MotionCorr" / "motioncorr_own_job.star",
            "MotionCorr/job002/job.star",
        )

        regenerate_results.main(
            [
                "--jobs",
                "Import/job001",
                "MotionCorr/job002",
                "--nodes",
                "Import/job001/movies.star",
                "CtfFind/job003/micrographs_ctf.star",
                "CtfFind/job003/logfile.pdf",
            ]
        )

        job_results_files = sorted(list(Path().glob("**/.results_display*.json")))
        assert len(job_results_files) == 2
        assert job_results_files == [
            Path("Import/job001/.results_display000_pending.json"),
            Path("MotionCorr/job002/.results_display000_pending.json"),
        ]

        node_results_files = sorted(list(Path().glob("**/results_display*.json")))
        assert len(node_results_files) == 3

        node_display_file = DataStarFile(NODE_DISPLAY_FILE)
        assert node_display_file.count_block() == 3
        assert node_display_file.column_as_list(
            blockname=None, colname="_pipelinerDisplayFile"
        ) == [
            "Import/job001/NodeDisplay/results_display000_pending.json",
            "CtfFind/job003/NodeDisplay/results_display000_pending.json",
            "CtfFind/job003/NodeDisplay/results_display001_pdffile.json",
        ]

    def test_generating_results_whole_project(self):
        Path("Import/job001").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_job.star",
            "Import/job001/job.star",
        )
        Path("MotionCorr/job002").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "MotionCorr" / "motioncorr_own_job.star",
            "MotionCorr/job002/job.star",
        )
        Path("CtfFind/job003").mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "CtfFind" / "ctffind_job.star",
            "CtfFind/job003/job.star",
        )

        regenerate_results.main([])

        job_results_files = sorted(list(Path().glob("**/.results_display*.json")))
        assert len(job_results_files) == 3
        assert job_results_files == [
            Path("CtfFind/job003/.results_display000_pending.json"),
            Path("Import/job001/.results_display000_pending.json"),
            Path("MotionCorr/job002/.results_display000_pending.json"),
        ]

        node_results_files = sorted(list(Path().glob("**/results_display*.json")))
        assert len(node_results_files) == 5

        node_display_file = DataStarFile(NODE_DISPLAY_FILE)
        assert node_display_file.count_block() == 5
        assert node_display_file.column_as_list(
            blockname=None, colname="_pipelinerDisplayFile"
        ) == [
            "Import/job001/NodeDisplay/results_display000_pending.json",
            "MotionCorr/job002/NodeDisplay/results_display000_pending.json",
            "MotionCorr/job002/NodeDisplay/results_display001_pdffile.json",
            "CtfFind/job003/NodeDisplay/results_display000_pending.json",
            "CtfFind/job003/NodeDisplay/results_display001_pdffile.json",
        ]
