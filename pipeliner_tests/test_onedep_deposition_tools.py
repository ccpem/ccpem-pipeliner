#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil
import re
import shlex
from typing import Optional, Union, List
from dataclasses import dataclass
from unittest.mock import patch
from pipeliner import __version__ as pipe_vers
from pipeliner_tests import test_data
from pipeliner.deposition_tools.onedep_deposition import (
    DepositionData,
    make_deposition_data_object,
    merge_depdata_items,
    gather_onedep_jobs_and_depobjects,
    OneDepDeposition,
)
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmImaging,
    EmSampleSupport,
    EmCtfCorrection,
    EmSoftware,
    EmImageProcessing,
    EmSpecimen,
    EmImageRecording,
    Em2dProjectionSelection,
    Em3dReconstruction,
)

uuid_re = re.compile(
    "^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"
)
UID = "12345678-1234-4321-abcd-123456789abc"
id_placeholder = Optional[Union[int, List[Optional[str]], str]]


@dataclass
class DefaultCombine:
    """An example class for a combine merge strategy"""

    id: id_placeholder = None
    field1: Optional[str] = None
    field2: Optional[str] = None


# example dataclasses for testing
@dataclass
class DefaultOverwrite:
    """An example class for an overwrite merge strategy"""

    id = id_placeholder = None
    field1: Optional[str] = None
    field2: Optional[str] = None


@dataclass
class DefaultMulti:
    """An example class for a multiple merge strategy"""

    id = id_placeholder = None
    field1: Optional[str] = None
    field2: Optional[str] = None


testing_lookup = {
    DefaultCombine: ("default_combine", "combine", False),
    DefaultMulti: ("default_multi", "multi", False),
    DefaultOverwrite: ("default_overwrite", "overwrite", False),
    EmCtfCorrection: ("em_ctf_correction", "multi", False),
    EmImaging: ("em_imaging", "multi", False),
    EmImageProcessing: ("em_image_processing", "multi", False),
    EmImageRecording: ("em_image_recording", "multi", False),
    EmSoftware: ("em_software", "multi", False),
    EmSampleSupport: ("em_sample_support", "multi", False),
    EmSpecimen: ("em_specimen", "multi", False),
    Em2dProjectionSelection: ("em_2d_projection_selection", "multi", False),
    Em3dReconstruction: ("em_3d_reconstruction", "multi", False),
}


class NewDepositionSystem(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories and get example file.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_instantiate_DepositionData_obj_bad_merge_strat(self):
        with self.assertRaises(ValueError):
            DepositionData(
                field_name="test", data_items=DefaultCombine(), merge_strat="bad"
            )

    def test_instantiate_DepositionData_obj(self):
        do = DepositionData(
            field_name="test", data_items=DefaultCombine(), merge_strat="combine"
        )
        assert do.field_name == "test"
        assert do.data_items.field1 is None
        assert do.data_items.field2 is None
        assert do.cif_type == "pairs"
        assert do.merge_strategy == "combine"
        assert do.data_items.id is None

    def test_make_DepositionData_obj_with_existing_uid(self):
        do = DepositionData(
            field_name="test", data_items=DefaultCombine(id=UID), merge_strat="combine"
        )
        assert do.field_name == "test"
        assert do.data_items.field1 is None
        assert do.data_items.field2 is None
        assert do.cif_type == "pairs"
        assert do.merge_strategy == "combine"
        assert do.data_items.id == UID

    def test_make_DepositionData_object(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            do = make_deposition_data_object(
                DefaultCombine(field1="moja", field2="mbili")
            )
            assert uuid_re.match(do.data_items.id[0])
            assert do.field_name == "default_combine"
            assert type(do.data_items) is DefaultCombine
            assert do.data_items.field1 == "moja"
            assert do.data_items.field2 == "mbili"

    def test_make_DepositionData_object_with_existing_id(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            do = make_deposition_data_object(
                DefaultCombine(field1="moja", field2="mbili"), uid=UID
            )
            assert uuid_re.match(do.data_items.id[0])
            assert do.field_name == "default_combine"
            assert type(do.data_items) is DefaultCombine
            assert do.data_items.field1 == "moja"
            assert do.data_items.field2 == "mbili"
            assert type(do.data_items.id) is list
            assert len(do.data_items.id) == 1
            assert do.data_items.id[0] == UID

    def test_merge_combine_depobjs(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            do1 = make_deposition_data_object(DefaultCombine(field1="moja"))
            do2 = make_deposition_data_object(DefaultCombine(field2="mbili"))
            merge = merge_depdata_items(do2, [do1])
            assert len(merge) == 1
            do3 = merge[0]
            assert do3.data_items.id == [do1.data_items.id[0], do2.data_items.id[0]]
            assert do3.field_name == "default_combine"
            assert type(do3.data_items) is DefaultCombine
            assert do3.data_items.field1 == "moja"
            assert do3.data_items.field2 == "mbili"

    def test_updating_job_ref(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            sub = make_deposition_data_object(DefaultMulti())
            sub.field_name = "field1"
            dobj = make_deposition_data_object(
                DefaultCombine(field1="JOBREF: Import/job001/")
            )
            dobj.update_job_references(jobs_dict={"Import/job001/": [sub]})
            assert dobj.data_items.field1 == [sub.data_items.id[0]]

    def test_merge_overwrite_dep_objs(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            do1 = make_deposition_data_object(
                DefaultOverwrite(field1="11", field2="12")
            )
            do2 = make_deposition_data_object(
                DefaultOverwrite(field1="21", field2="22")
            )
            merged = merge_depdata_items(do2, [do1])
            assert len(merged) == 1
            do3 = merged[0]
            assert len(do3.data_items.id) == 2
            assert do3.data_items.id[0] == do2.data_items.id[0]
            assert do3.data_items.id[1] == do1.data_items.id[0]
            assert do3.data_items.field1 == "21"
            assert do3.data_items.field2 == "22"

    def test_merge_multi_dep_objects(self):
        with patch.dict(
            "pipeliner.deposition_tools.onedep_deposition.ONEDEP_OBJECTS",
            testing_lookup,
        ):
            do1 = make_deposition_data_object(DefaultMulti())
            do2 = make_deposition_data_object(DefaultMulti())
            merged = merge_depdata_items(do2, [do1])
            assert merged == [do1, do2]

    def prepare_testing_data(self):
        for dir in ["Import/job001", "MotionCorr/job002", "CtfFind/job003"]:
            os.makedirs(dir)
        for f in [
            ("Pipelines/relion40_tutorial.star", "default_pipeline.star"),
            ("Metadata/ebic_optics.csv", "scope_data.csv"),
            ("JobFiles/Import/import_default_job.star", "Import/job001/job.star"),
            (
                "JobFiles/MotionCorr/motioncorr_own_nofloat16_job.star",
                "MotionCorr/job002/job.star",
            ),
            ("JobFiles/CtfFind/tutorial_job003_job.star", "CtfFind/job003/job.star"),
        ]:
            shutil.copy(os.path.join(self.test_data, f[0]), f[1])

    def test_gather_onedep_jobs_and_depobjects(self):
        self.prepare_testing_data()
        with patch("pipeliner.pipeliner_job.ExternalProgram.get_version") as mock_vers:
            mock_vers.return_value = "**VERSION**"
            depobjs = gather_onedep_jobs_and_depobjects("CtfFind/job003/")

        ids = []
        for group in depobjs[0]:
            for depobj in group:
                ids.append(depobj.data_items.id)

        expected_dos = [
            [
                {
                    "field_name": "em_ctf_correction",
                    "data_items": EmCtfCorrection(
                        id=ids[0],
                        image_processing_id="JOBREF: MotionCorr/job002/",
                        type="PHASE FLIPPING AND AMPLITUDE CORRECTION",
                        details=None,
                    ),
                    "dc_type": EmCtfCorrection,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[1],
                        category="CTF CORRECTION",
                        details=None,
                        name="ctffind4",
                        version="**VERSION**",
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[2],
                        category="OTHER",
                        details=None,
                        name="relion_run_motioncorr",
                        version="**VERSION**",
                        image_processing_id="JOBREF: MotionCorr/job002/",
                        fitting_id=None,
                        imaging_id="JOBREF: Import/job001/",
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[3],
                        category="IMAGE ACQUISITION",
                        details=None,
                        name="EPU",
                        version="764-0 3.0.0",
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[4],
                        category="OTHER",
                        details="Project management and data analysis",
                        name="CCPEM-pipeliner/Doppio",
                        version=pipe_vers,
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_image_processing",
                    "data_items": EmImageProcessing(
                        id=ids[5], image_recording_id=None, details="Motion correction"
                    ),
                    "dc_type": EmImageProcessing,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_imaging",
                    "data_items": EmImaging(
                        id=ids[6],
                        entry_id="ENTRY_ID",
                        accelerating_voltage=None,
                        illumination_mode=None,
                        electron_source=None,
                        microscope_model=None,
                        imaging_mode=None,
                        sample_support_id="JOBREF: Import/job001/",
                        specimen_holder_type=None,
                        specimen_holder_model=None,
                        details=None,
                        date=None,
                        mode=None,
                        nominal_cs=None,
                        nominal_defocus_min=-0.5,
                        nominal_defocus_max=-2.0,
                        tilt_angle_min=0.0,
                        tilt_angle_max=0.0,
                        nominal_magnification=None,
                        calibrated_magnification=None,
                        energy_filter=None,
                        energy_window=None,
                        temperature=None,
                        detector_distance=None,
                        recording_temperature_minimum=None,
                        recording_temperature_maximum=None,
                    ),
                    "dc_type": EmImaging,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                }
            ],
            [
                {
                    "field_name": "em_sample_support",
                    "data_items": EmSampleSupport(
                        id=ids[7],
                        entry_id="ENTRY_ID",
                        film_material=None,
                        grid_type="HoleyCarbon",
                        grid_material=None,
                        grid_mesh_size=None,
                        details="Hole size: 1.4; Hole spacing 1.3",
                    ),
                    "dc_type": EmSampleSupport,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                }
            ],
            [
                {
                    "field_name": "em_specimen",
                    "data_items": EmSpecimen(
                        id=ids[8],
                        experiment_id=1,
                        concentration=None,
                        details=None,
                        embedding_applied=False,
                        shadowing_applied=False,
                        staining_applied=False,
                        vitrification_applied=True,
                    ),
                    "dc_type": EmSpecimen,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
        ]
        for n, group in enumerate(depobjs[0]):
            for m, dobj in enumerate(group):
                assert dobj.__dict__ == expected_dos[n][m]

        assert len(depobjs[1]["CtfFind/job003/"]) == 2
        assert len(depobjs[1]["MotionCorr/job002/"]) == 2
        assert len(depobjs[1]["Import/job001/"]) == 5
        for i in depobjs[1]:
            assert all([type(x) is DepositionData for x in depobjs[1][i]])

    def test_instantiate_OneDepDeposition(self):
        self.prepare_testing_data()
        dep = OneDepDeposition("CtfFind/job003/")
        assert len(dep.raw_depobjs) == 6
        for group in dep.raw_depobjs:
            all([type(x) is DepositionData for x in group])
        assert len(dep.jobs["CtfFind/job003/"]) == 2
        assert len(dep.jobs["MotionCorr/job002/"]) == 2
        assert len(dep.jobs["Import/job001/"]) == 5
        for i in dep.jobs:
            assert all([type(x) is DepositionData for x in dep.jobs[i]])
        assert dep.merged_depobjs == []
        assert dep.final_depobjs == []
        assert dep.int_ids == {}

    def test_OneDepDeposition_update_jobrefs(self):
        self.prepare_testing_data()
        dep = OneDepDeposition("CtfFind/job003/")
        dep.update_jobrefs()
        ids = []
        for group in dep.raw_depobjs:
            for dobj in group:
                ids.append(dobj.data_items.id)
        assert dep.raw_depobjs[0][0].data_items.image_processing_id == ids[5]
        assert dep.raw_depobjs[1][1].data_items.image_processing_id == ids[5]
        assert dep.raw_depobjs[1][1].data_items.imaging_id == ids[6]
        assert dep.raw_depobjs[3][0].data_items.sample_support_id == ids[7]

    def test_OneDepDeposition_merging_deposition_objs(self):
        self.prepare_testing_data()
        with patch("pipeliner.pipeliner_job.ExternalProgram.get_version") as mock_vers:
            mock_vers.return_value = "**VERSION**"
            dep = OneDepDeposition("CtfFind/job003/")
            dep.update_jobrefs()
            dep.merge_depobjs()

        ids = []
        for group in dep.merged_depobjs:
            for depobj in group:
                ids.append(depobj.data_items.id)
        expected_merged = [
            [
                {
                    "field_name": "em_ctf_correction",
                    "data_items": EmCtfCorrection(
                        id=ids[0],
                        image_processing_id=ids[5],
                        type="PHASE FLIPPING AND AMPLITUDE CORRECTION",
                        details=None,
                    ),
                    "dc_type": EmCtfCorrection,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[1],
                        category="CTF CORRECTION",
                        details=None,
                        name="ctffind4",
                        version="**VERSION**",
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[2],
                        category="OTHER",
                        details=None,
                        name="relion_run_motioncorr",
                        version="**VERSION**",
                        image_processing_id=ids[5],
                        fitting_id=None,
                        imaging_id=ids[6],
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[3],
                        category="IMAGE ACQUISITION",
                        details=None,
                        name="EPU",
                        version="764-0 3.0.0",
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
                {
                    "field_name": "em_software",
                    "data_items": EmSoftware(
                        id=ids[4],
                        category="OTHER",
                        details="Project management and data analysis",
                        name="CCPEM-pipeliner/Doppio",
                        version=pipe_vers,
                        image_processing_id=None,
                        fitting_id=None,
                        imaging_id=None,
                    ),
                    "dc_type": EmSoftware,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_image_processing",
                    "data_items": EmImageProcessing(
                        id=ids[5], image_recording_id=None, details="Motion correction"
                    ),
                    "dc_type": EmImageProcessing,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_imaging",
                    "data_items": EmImaging(
                        id=ids[6],
                        entry_id="ENTRY_ID",
                        accelerating_voltage=None,
                        illumination_mode=None,
                        electron_source=None,
                        microscope_model=None,
                        imaging_mode=None,
                        sample_support_id=ids[7],
                        specimen_holder_type=None,
                        specimen_holder_model=None,
                        details=None,
                        date=None,
                        mode=None,
                        nominal_cs=None,
                        nominal_defocus_min=-0.5,
                        nominal_defocus_max=-2.0,
                        tilt_angle_min=0.0,
                        tilt_angle_max=0.0,
                        nominal_magnification=None,
                        calibrated_magnification=None,
                        energy_filter=None,
                        energy_window=None,
                        temperature=None,
                        detector_distance=None,
                        recording_temperature_minimum=None,
                        recording_temperature_maximum=None,
                    ),
                    "dc_type": EmImaging,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_sample_support",
                    "data_items": EmSampleSupport(
                        id=ids[7],
                        entry_id="ENTRY_ID",
                        film_material=None,
                        grid_type="HoleyCarbon",
                        grid_material=None,
                        grid_mesh_size=None,
                        details="Hole size: 1.4; Hole spacing 1.3",
                    ),
                    "dc_type": EmSampleSupport,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
            [
                {
                    "field_name": "em_specimen",
                    "data_items": EmSpecimen(
                        id=ids[8],
                        experiment_id=1,
                        concentration=None,
                        details=None,
                        embedding_applied=False,
                        shadowing_applied=False,
                        staining_applied=False,
                        vitrification_applied=True,
                    ),
                    "dc_type": EmSpecimen,
                    "cif_type": "loop",
                    "merge_strategy": "multi",
                    "reverse": False,
                },
            ],
        ]
        for n, group in enumerate(dep.merged_depobjs):
            for m, dobj in enumerate(group):
                assert expected_merged[n][m] == dobj.__dict__

    def test_OneDepDeposition_make_int_ids(self):
        self.prepare_testing_data()
        dep = OneDepDeposition("CtfFind/job003/")
        dep.update_jobrefs()
        dep.merge_depobjs()
        dep.make_int_ids()
        ids = list(dep.int_ids)
        assert dep.int_ids[ids[0]] == 1
        assert dep.int_ids[ids[1]] == 1
        assert dep.int_ids[ids[2]] == 2
        assert dep.int_ids[ids[3]] == 3
        assert dep.int_ids[ids[4]] == 4
        assert dep.int_ids[ids[5]] == 1
        assert dep.int_ids[ids[6]] == 1
        assert dep.int_ids[ids[7]] == 1
        assert dep.int_ids[ids[8]] == 1

    def test_OneDepDeposition_update_uids(self):
        self.prepare_testing_data()
        dep = OneDepDeposition("CtfFind/job003/")
        dep.update_jobrefs()
        dep.merge_depobjs()
        dep.make_int_ids()
        dep.update_uids_to_int_ids()
        assert dep.merged_depobjs[0][0].data_items.id == 1
        assert dep.merged_depobjs[1][0].data_items.id == 1
        assert dep.merged_depobjs[1][1].data_items.id == 2
        assert dep.merged_depobjs[1][1].data_items.image_processing_id == 1
        assert dep.merged_depobjs[1][1].data_items.imaging_id == 1
        assert dep.merged_depobjs[1][2].data_items.id == 3
        assert dep.merged_depobjs[1][3].data_items.id == 4
        assert dep.merged_depobjs[2][0].data_items.id == 1
        assert dep.merged_depobjs[3][0].data_items.id == 1
        assert dep.merged_depobjs[3][0].data_items.sample_support_id == 1
        assert dep.merged_depobjs[4][0].data_items.id == 1
        assert dep.merged_depobjs[5][0].data_items.id == 1

    def test_OneDepDeposition_prepare_final(self):
        self.prepare_testing_data()
        dep = OneDepDeposition("CtfFind/job003/")
        dep.prepare_deposition()
        assert dep.final_depobjs[0].field_name == "em_ctf_correction"
        assert dep.final_depobjs[1].field_name == "em_software"
        assert dep.final_depobjs[2].field_name == "em_software"
        assert dep.final_depobjs[3].field_name == "em_software"
        assert dep.final_depobjs[4].field_name == "em_software"
        assert dep.final_depobjs[5].field_name == "em_image_processing"
        assert dep.final_depobjs[6].field_name == "em_imaging"
        assert dep.final_depobjs[7].field_name == "em_sample_support"
        assert dep.final_depobjs[8].field_name == "em_specimen"

    @patch("pipeliner.jobs.relion.import_job.pipe_vers", new="**PIPEVERS**")
    def test_OneDepDeposition_write_file(self):
        self.prepare_testing_data()
        with patch("pipeliner.pipeliner_job.ExternalProgram.get_version") as mock_vers:
            mock_vers.return_value = "**VERSION**"
            dep = OneDepDeposition("CtfFind/job003/")
            dep.prepare_deposition()
            dep.write_deposition_cif_file("1ABC")
        with open("1ABC_deposition_data.cif") as w:
            wrote = w.readlines()
        with open(os.path.join(self.test_data, "Metadata/onedep_3jobs.cif")) as a:
            expected = a.readlines()
        for n, line in enumerate(wrote):
            w = list(shlex.split(line))
            e = list(shlex.split(expected[n]))
            assert w == e
        for n, line in enumerate(expected):
            assert shlex.split(wrote[n]) == shlex.split(line)


if __name__ == "__main__":
    unittest.main()
