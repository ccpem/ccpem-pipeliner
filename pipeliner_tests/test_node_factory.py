#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil


from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    GenNodeFormatConverter,
    NODE_TOMOGRAM,
    NODE_IMAGE2DSTACK,
)
from pipeliner_tests import test_data
from pipeliner.node_factory import create_node, all_node_toplevel_types, NODE_CLASSES

all_node_types = [
    "AtomCoords",
    "AtomCoordsGroupMetadata",
    "DensityMap",
    "DensityMapGroupMetadata",
    "DensityMapMetadata",
    "EulerAngles",
    "EvaluationMetric",
    "Image2D",
    "Image2DGroupMetadata",
    "Image2DMetadata",
    "Image2DStack",
    "Image3D",
    "Image3DGroupMetadata",
    "Image3DMetadata",
    "LigandDescription",
    "LogFile",
    "Mask2D",
    "Mask3D",
    "Micrograph",
    "MicrographCoords",
    "MicrographCoordsGroup",
    "MicrographGroupMetadata",
    "MicrographMetadata",
    "MicrographMovie",
    "MicrographMovieGroupMetadata",
    "MicrographMovieMetadata",
    "MicroscopeData",
    "MlModel",
    "OptimiserData",
    "ParamsData",
    "ParticleGroupMetadata",
    "ProcessData",
    "Restraints",
    "RigidBodies",
    "Sequence",
    "SequenceAlignment",
    "SequenceGroup",
    "StructureFactors",
    "TiltSeries",
    "TiltSeriesGroupMetadata",
    "TiltSeriesMetadata",
    "TiltSeriesMovie",
    "TiltSeriesMovieGroupMetadata",
    "TiltSeriesMovieMetadata",
    "TomoManifoldData",
    "TomoOptimisationSet",
    "TomoTrajectoryData",
    "Tomogram",
    "TomogramGroupMetadata",
    "TomogramMetadata",
]


class NodeFactoryTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_all_node_types(self):
        all_nodes = all_node_toplevel_types()
        print(all_nodes)
        assert all_nodes == all_node_types

    def test_get_all_node_types_add_new_at_end_default(self):
        all_nodes = all_node_toplevel_types(add_new=True)
        assert all_nodes == all_node_types + ["NEW NODE TYPE (not recommended)"]

    def test_get_all_node_types_add_new_at_beginning(self):
        all_nodes = all_node_toplevel_types(add_new=True, new_pos=0)
        assert all_nodes == ["NEW NODE TYPE (not recommended)"] + all_node_types

    def test_create_node_instantiates_right_class(self):
        for nt in NODE_CLASSES:
            created = create_node("myfile.txt", nt, ["test"])
            assert type(created) is NODE_CLASSES[nt]

    def test_create_node(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, "file.mrc")
        node = create_node("file.mrc", NODE_TOMOGRAM, ["test"])
        assert node.format == "mrc"
        assert type(node.format_converter) is GenNodeFormatConverter
        assert node.input_for_processes_list == []
        assert node.kwds == ["test"]
        assert node.name == "file.mrc"
        assert not node.output_from_process
        assert node.toplevel_description == "A single tomogram"
        assert node.toplevel_type == "Tomogram"

    def test_create_node_format_generalised(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, "file.mrcs")
        node = create_node("file.mrcs", NODE_TOMOGRAM, ["test"])
        assert node.format == "mrc"
        assert type(node.format_converter) is GenNodeFormatConverter
        assert node.input_for_processes_list == []
        assert node.kwds == ["test"]
        assert node.name == "file.mrcs"
        assert not node.output_from_process
        assert node.toplevel_description == "A single tomogram"
        assert node.toplevel_type == "Tomogram"

    def test_create_node_filetype_wrong(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.star"))
        node = create_node(
            "file.star", NODE_PARTICLEGROUPMETADATA, ["file", "type", "wrong"]
        )
        assert node.format == "XstarX"
        assert type(node.format_converter) is GenNodeFormatConverter
        assert node.input_for_processes_list == []
        assert node.kwds == ["file", "type", "wrong"]
        assert node.name == "file.star"
        assert not node.output_from_process
        assert node.toplevel_description == (
            "Metadata for a set of particles, e.g particles.star from RELION"
        )
        assert node.toplevel_type == NODE_PARTICLEGROUPMETADATA

    def test_create_node_filetype_wrong_validation_turned_off(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.star"))
        node = create_node(
            "file.star",
            NODE_PARTICLEGROUPMETADATA,
            ["file", "type", "wrong"],
            do_validation=False,
        )
        assert node.format == "star"
        assert type(node.format_converter) is GenNodeFormatConverter
        assert node.input_for_processes_list == []
        assert node.kwds == ["file", "type", "wrong"]
        assert node.name == "file.star"
        assert not node.output_from_process
        assert node.toplevel_description == (
            "Metadata for a set of particles, e.g particles.star from RELION"
        )
        assert node.toplevel_type == NODE_PARTICLEGROUPMETADATA

    def test_create_node_override_format_no_ext_generalised(self):
        node = create_node(
            filename="file",
            nodetype=NODE_IMAGE2DSTACK,
            kwds=["override"],
            override_format="mrcs",
        )
        assert node.format == "mrc"
        assert node.kwds == ["override"]
        assert node.name == "file"
        assert node.toplevel_type == NODE_IMAGE2DSTACK

    def test_create_node_override_format_generalised(self):
        node = create_node(
            filename="file.XXX",
            nodetype=NODE_IMAGE2DSTACK,
            kwds=["override"],
            override_format="mrcs",
        )
        assert node.format == "mrc"
        assert node.kwds == ["override"]
        assert node.name == "file.XXX"
        assert node.toplevel_type == NODE_IMAGE2DSTACK


if __name__ == "__main__":
    unittest.main()
