#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import unittest
import tempfile
import shutil
import json
import math
import pandas as pd
from unittest.mock import patch
from pathlib import Path
from pipeliner.utils import touch
from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import get_relion_tutorial_data
from pipeliner.results_display_objects import (
    ResultsDisplayMontage,
    ResultsDisplayImage,
    ResultsDisplayHistogram,
    ResultsDisplayTable,
    ResultsDisplayMapModel,
    ResultsDisplayPlotlyHistogram,
    ResultsDisplayPlotlyObj,
    ResultsDisplayPlotlyScatter,
    ResultsDisplayTextFile,
    ResultsDisplayPending,
    ResultsDisplayJson,
    get_next_resultsfile_name,
)
from pipeliner.display_tools import (
    get_ordered_classes_arrays,
    graph_from_starfile_cols,
    histogram_from_starfile_col,
    mrc_thumbnail,
    tiff_thumbnail,
    mini_montage_from_many_files,
    mini_montage_from_starfile,
    make_map_model_thumb_and_display,
    make_particle_coords_thumb,
    mini_montage_from_stack,
    create_results_display_object,
    make_mrcs_central_slices_montage,
    make_maps_slice_montage_and_3d_display,
)


class DisplayToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_image_resultsdisplay_obj(self):
        rdo = ResultsDisplayImage(
            title="myimage",
            image_desc="Test description",
            image_path="test.png",
            associated_data=["data_file"],
        )
        assert rdo.title == "myimage"
        assert rdo.image_desc == "Test description"
        assert rdo.image_path == "test.png"
        assert rdo.associated_data == ["data_file"]

    def test_map_model_maps_models_opacities(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            maps_opacity=[0.175, 0.2],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.175, 0.2],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert mmo.__dict__ == expected

    def test_map_model_maps_models_colours_hexwith_hash(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            maps=["map1.mrc", "map2.mrc"],
            maps_colours=["#ffffff", "#000000"],
            models=["model1.pdb", "model2.pdb"],
            models_colours=["#ffffff", "#000000"],
            maps_opacity=[0.175, 0.2],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.175, 0.2],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": ["0xffffff", "0x000000"],
            "models_colours": ["0xffffff", "0x000000"],
        }

        assert mmo.__dict__ == expected

    def test_map_model_maps_models(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5, 0.5],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert mmo.__dict__ == expected

    def test_map_model_maps_only(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            maps=["map1.mrc", "map2.mrc"],
            maps_opacity=[0.175, 0.2],
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.175, 0.2],
            "models": [],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert mmo.__dict__ == expected

    def test_map_model_one_map_opacity_is_1(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            maps=["map1.mrc"],
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        assert mmo.__dict__ == {
            "maps": ["map1.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    def test_map_model_models_only(self):
        mmo = ResultsDisplayMapModel(
            title="3D viewer: test mm display",
            models=["model1.pdb", "model2.pdb"],
            models_data="here are the models",
            maps_data="here are the maps",
            associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
        )

        expected = {
            "maps": [],
            "maps_opacity": [],
            "dobj_type": "mapmodel",
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: test mm display",
            "maps_data": "here are the maps",
            "models_data": "here are the models",
            "associated_data": ["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert mmo.__dict__ == expected

    def test_map_model_no_maps_or_models(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMapModel(
                title="3D viewer: test mm display",
                models_data="here are the models",
                maps_data="here are the maps",
                associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            )

    def test_map_model_no_maps_opacities_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMapModel(
                title="3D viewer: test mm display",
                maps=["map1.mrc", "map2.mrc"],
                maps_opacity=[1],
                models_data="here are the models",
                maps_data="here are the maps",
                associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            )

    def test_map_model_no_maps_colours_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMapModel(
                title="3D viewer: test mm display",
                maps=["map1.mrc", "map2.mrc"],
                maps_opacity=[],
                maps_colours=["#ffffff"],
                models_data="here are the models",
                maps_data="here are the maps",
                associated_data=["map1.mrc", "map2.mac", "model1.pdb", "model2.pdb"],
            )

    @patch("pipeliner.display_tools.set_doppio_map_bin", return_value=(1.0, 0))
    def test_create_map_mode_thumbs_nobinning(self, _):
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        touch("model1.pdb")
        touch("model2.pdb")
        dispobj = make_map_model_thumb_and_display(
            title="Test map model display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            assoc_data=["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
            outputdir="",
        )
        assert dispobj.__dict__ == {
            "maps": ["map1.mrc", "map2.mrc"],
            "maps_opacity": [0.5, 0.5],
            "dobj_type": "mapmodel",
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: Test map model display",
            "maps_data": "map1.mrc, map2.mrc",
            "models_data": "model1.pdb, model2.pdb",
            "associated_data": ["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @patch("pipeliner.display_tools.set_doppio_map_bin", return_value=(0.5, 1.5))
    def test_create_map_mode_thumbs_with_bin(self, _):
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        touch("model1.pdb")
        touch("model2.pdb")
        dispobj = make_map_model_thumb_and_display(
            title="3D viewer: Test map model display",
            maps=["map1.mrc", "map2.mrc"],
            models=["model1.pdb", "model2.pdb"],
            assoc_data=["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
            outputdir="",
        )

        assert dispobj.__dict__ == {
            "maps": ["Thumbnails/map1.mrc", "Thumbnails/map2.mrc"],
            "maps_opacity": [0.5, 0.5],
            "dobj_type": "mapmodel",
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: Test map model display",
            "maps_data": "map1.mrc, map2.mrc",
            "models_data": "model1.pdb, model2.pdb",
            "associated_data": ["map1.mrc", "map2.mrc", "model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": (
                " Maps binned to 50% size for display. Maximum resolution of "
                "display is 3.0 Å."
            ),
            "maps_colours": [],
            "models_colours": [],
        }
        assert os.path.isfile("Thumbnails/map1.mrc")
        assert os.path.isfile("Thumbnails/map2.mrc")

    def test_histo(self):
        rdo = ResultsDisplayHistogram(
            title="mytitle",
            data_to_bin=[1] * 10 + [2] * 15 + [3] * 20 + [4] * 2 + [5] * 100,
            xlabel="number",
            ylabel="count",
            associated_data=["mydata.star"],
        )

        assert rdo.title == "mytitle"
        assert list(rdo.bins) == [10, 15, 0, 20, 2, 100]
        expround_bins = ["1.0", "1.7", "2.3", "3.0", "3.7", "4.3", "5.0"]
        assert [f"{x:.01f}" for x in list(rdo.bin_edges)] == expround_bins
        assert rdo.xlabel == "number"
        assert rdo.ylabel == "count"
        assert rdo.associated_data == ["mydata.star"]

    def test_histo_with_binedges(self):
        rdo = ResultsDisplayHistogram(
            title="mytitle",
            data_to_bin=[1] * 10 + [2] * 15 + [3] * 20 + [4] * 2 + [5] * 100,
            bin_edges=[0, 1, 2, 10],
            xlabel="number",
            ylabel="count",
            associated_data=["mydata.star"],
        )

        assert rdo.title == "mytitle"
        assert list(rdo.bins) == [0, 10, 137]
        expround_bins = ["0.0", "1.0", "2.0", "10.0"]
        assert [f"{x:.01f}" for x in list(rdo.bin_edges)] == expround_bins
        assert rdo.xlabel == "number"
        assert rdo.ylabel == "count"
        assert rdo.associated_data == ["mydata.star"]

    def test_histo_write_and_read(self):
        rdo = ResultsDisplayHistogram(
            title="mytitle",
            data_to_bin=[1] * 10 + [2] * 15 + [3] * 20 + [4] * 2 + [5] * 100,
            xlabel="number",
            ylabel="count",
            associated_data=["mydata.star"],
        )

        rdo.write_displayobj_file("")
        with open(".results_display000_histogram.json", "r") as of:
            wrote = json.load(of)
            del wrote["dobj_type"]
        rdo2 = ResultsDisplayHistogram(**wrote)
        assert rdo.__dict__ == rdo2.__dict__

    def test_montage_cant_instantiate_if_counts_dont_match_no_labels(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMontage(
                associated_data=["fake_file.txt"],
                title="Hereistitle",
                xvalues=[1, 2, 3, 4, 5, 6],
                yvalues=[1, 2, 3, 4],
                img="myimg.png",
            )

    def test_montage_cant_instantiate_if_counts_dont_match_with_labels(self):
        with self.assertRaises(ValueError):
            ResultsDisplayMontage(
                associated_data=["fake_file.txt"],
                title="Hereistitle",
                xvalues=[1, 2, 3, 4, 5, 6],
                yvalues=[1, 2, 3, 4, 5],
                img="my_image.png",
            )

    def test_montage_write_dispobj_json(self):
        ddata = ResultsDisplayMontage(
            associated_data=["fake_file.txt"],
            title="Hereistitle",
            xvalues=[1, 2, 3, 4, 5, 6],
            yvalues=[1, 2, 3, 4, 5, 6],
            labels=["1", "2", "3", "4", "5", "6"],
            img="test.png",
        )
        ddata.write_displayobj_file("")
        with open(".results_display000_montage.json", "r") as f:
            ddict = json.load(f)
        exp_dict = {
            "title": "Hereistitle",
            "dobj_type": "montage",
            "xvalues": [1, 2, 3, 4, 5, 6],
            "yvalues": [1, 2, 3, 4, 5, 6],
            "labels": ["1", "2", "3", "4", "5", "6"],
            "associated_data": ["fake_file.txt"],
            "img": "test.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert ddict == exp_dict

    def test_generic_plotlyobjs(self):
        # scatter subplots
        list_x = [2, 2, 5, 5, 5, 1, 10]
        list_y = [2, 2, 5, 5, 5, 1, 10]
        dict_data = {"x": list_x, "y": list_y}
        disp_obj = ResultsDisplayPlotlyObj(
            data=[dict_data] * 6,
            plot_type="Scatter",
            subplot=True,
            subplot_size=(3, 2),
            subplot_order="column",
            subplot_args=[
                {"name": "a"},
                {"name": "b"},
                {"name": "c"},
                {"name": "d"},
                {"name": "e"},
                {"name": "f"},
            ],
            trace_args={"marker_color": "Green"},
            layout_args={
                "title_text": "scatter subplot",
                "title_font_size": 20,
                "height": 600,
            },
            xaxes_args={
                "title_text": "xaxis title",
                "range": [1, 10],
                "showgrid": False,
            },
            yaxes_args={"title_text": "yaxis title", "showgrid": False},
            make_subplot_args={
                "vertical_spacing": 0.25,
                "subplot_titles": ["subplot"] * 6,
                "column_widths": [0.7, 0.3],
            },
            title="test scatter",
            associated_data=[],
        )
        assert hasattr(disp_obj, "plotlyfig")
        disp_obj.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display000_plotlyobj.json").st_size,
            12782,
            rel_tol=0.05,
        )
        # po.write_image(po.from_json(disp_obj.plotlyfig), "test1.png", format="png")
        # mix type multi_series plot
        disp_obj = ResultsDisplayPlotlyObj(
            data=[dict_data] * 3,
            plot_type=["Scatter", "Bar", "Scatter"],
            multi_series=True,
            series_args=[
                {
                    "mode": "lines",
                    "name": "series1",
                    "line": {"color": "orange", "dash": "dash"},
                },
                {},
                {
                    "mode": "lines+markers",
                    "name": "series3",
                    "marker": {"color": "black"},
                },
            ],
            # only update bar traces using selector
            trace_args={"marker_color": "Green", "selector": {"type": "bar"}},
            layout_args={
                "title_text": "multi_series plot",
                "title_font_size": 20,
            },
            xaxes_args={
                "title_text": "xaxis title",
                "range": [1, 10],
                "showgrid": False,
            },
            yaxes_args={"title_text": "yaxis title", "showgrid": False},
            title="test multi_series",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display001_plotlyobj.json").st_size,
            9383,
            rel_tol=0.05,
        )
        # po.write_image(po.from_json(disp_obj.plotlyfig), "test2.png", format="png")
        # subplots with multi_series
        disp_obj = ResultsDisplayPlotlyObj(
            data=[dict_data] * 6,
            plot_type=["Scatter", "Bar", "Scatter", "Bar", "Scatter", "Bar"],
            subplot=True,
            multi_series=True,
            subplot_size=(3, 1),
            subplot_order=[(1, 1), (1, 1), (2, 1), (2, 1), (3, 1), (3, 1)],
            subplot_args=[
                {"name": "a"},
                {"name": "b"},
                {"name": "c"},
                {"name": "d"},
                {"name": "e"},
                {"name": "f"},
            ],
            trace_args={"marker_color": "Green"},
            layout_args={
                "title_text": "scatter subplot",
                "title_font_size": 20,
                "height": 300 * 3,
            },
            xaxes_args={
                "title_text": "xaxis title",
                "range": [1, 10],
                "showgrid": False,
            },
            yaxes_args={"title_text": "yaxis title", "showgrid": False},
            make_subplot_args={
                "vertical_spacing": 0.15,
                "subplot_titles": ["subplot1", "subplot2", "subplot3"],
            },
            title="test scatter",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display002_plotlyobj.json").st_size,
            11443,
            rel_tol=0.05,
        )
        # po.write_image(po.from_json(disp_obj.plotlyfig), "test3.png", format="png")

    def test_plotly_histogram(self):
        list_data = [2, 2, 5, 5, 5, 1, 10]
        dict_data = {"test_dist": list_data}
        disp_obj = ResultsDisplayPlotlyHistogram(
            data=list_data, title="test histogram", associated_data=[]
        )
        assert hasattr(disp_obj, "plotlyfig")
        disp_obj.write_displayobj_file(self.test_dir)
        assert os.path.isfile(".results_display000_plotlyhistogram.json")
        assert math.isclose(
            os.stat(".results_display000_plotlyhistogram.json").st_size,
            8750,
            rel_tol=0.05,
        )
        disp_obj = ResultsDisplayPlotlyHistogram(
            data=dict_data, x="test_dist", title="test histogram", associated_data=[]
        )
        disp_obj.write_displayobj_file(self.test_dir)

        disp_obj3 = ResultsDisplayPlotlyHistogram(
            data=pd.DataFrame(dict_data),
            x="test_dist",
            title="test histogram",
            associated_data=[],
        )
        disp_obj3.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display001_plotlyhistogram.json").st_size,
            os.stat(".results_display002_plotlyhistogram.json").st_size,
            rel_tol=0.05,
        )
        bin_counts = [3, 5, 1]
        bin_centres = [1, 3, 5]
        disp_obj = ResultsDisplayPlotlyHistogram(
            x="test_dist",
            bin_counts=bin_counts,
            bin_centres=bin_centres,
            title="test histogram",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        disp_obj = ResultsDisplayPlotlyObj(
            data=dict_data,
            plot_type="histogram",
            x="test_dist",
            title="test histogram",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display004_plotlyobj.json").st_size,
            os.stat(".results_display003_plotlyhistogram.json").st_size,
            rel_tol=0.05,
        )

    def test_plotly_scatter(self):
        list_x = [2, 2, 5, 5, 5, 1, 10]
        list_y = [2, 2, 5, 5, 5, 1, 10]
        dict_data = {"x": list_x, "y": list_y}
        disp_obj = ResultsDisplayPlotlyScatter(
            data=[list_x, list_y], title="test scatter", associated_data=[]
        )
        assert hasattr(disp_obj, "plotlyfig")
        disp_obj.write_displayobj_file(self.test_dir)
        assert os.path.isfile(".results_display000_plotlyscatter.json")
        assert math.isclose(
            os.stat(".results_display000_plotlyscatter.json").st_size,
            10565,
            rel_tol=0.05,
        )
        disp_obj = ResultsDisplayPlotlyScatter(
            data=dict_data,
            x="x",
            y="y",
            title="test scatter",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)

        disp_obj3 = ResultsDisplayPlotlyScatter(
            data=pd.DataFrame(dict_data),
            x="x",
            y="y",
            title="test scatter",
            associated_data=[],
        )
        disp_obj3.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display001_plotlyscatter.json").st_size,
            os.stat(".results_display002_plotlyscatter.json").st_size,
            rel_tol=0.05,
        )
        disp_obj = ResultsDisplayPlotlyScatter(
            x=list_x,
            y=list_y,
            title="test scatter",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        disp_obj = ResultsDisplayPlotlyObj(
            data=dict_data,
            plot_type="scatter",
            x="x",
            y="y",
            title="test scatter",
            associated_data=[],
        )
        disp_obj.write_displayobj_file(self.test_dir)
        assert math.isclose(
            os.stat(".results_display004_plotlyobj.json").st_size,
            os.stat(".results_display002_plotlyscatter.json").st_size,
            rel_tol=0.05,
        )

    def test_prepare_class2d_montage_no_parts_counting(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(mf, 10, 50, "", "mfile.png")
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 10
        expy = sum([[n] * 10 for n in reversed(range(10))], [])
        explabels = ["Class 62; 2.61%", "Class 60; 0.00%"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-1] == explabels[1]
        assert len(ddata.labels) == 100
        assert ddata.img == "Thumbnails/mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"
        assert ddata.dobj_type == "montage"

    def test_prepare_class2d_montage_no_parts_counting_base64(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(
            mf, 10, 50, "", "mfile.png", base64_output=True
        )
        explabels = ["Class 62; 2.61%", "Class 60; 0.00%"]
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-1] == explabels[1]
        assert len(ddata.labels) == 100
        assert ddata.images == "imagelist.json"
        assert os.path.isfile(ddata.images)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"

    def test_prepare_class2d_montage_no_parts_counting_uneven_rows(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(mf, 11, 50, "", "mfile.png")
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 10
        expy = sum([[n] * 11 for n in reversed(range(10))], [])
        explabels = ["Class 62; 2.61%", "Class 60; 0.00%"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [""] * 10
        assert ddata.img == "Thumbnails/mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"

    def test_prepare_class2d_montage_no_parts_counting_uneven_rows_base64(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        ddata = get_ordered_classes_arrays(
            mf, 11, 50, "", "mfile.png", base64_output=True
        )
        explabels = ["Class 62; 2.61%", "Class 60; 0.00%"]
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [""] * 10
        assert ddata.images == "imagelist.json"
        assert os.path.isfile(ddata.images)
        assert ddata.associated_data == [mf]
        assert ddata.title == "2D class averages"

    def test_prepare_class2d_montage_parts_counting_uneven_rows(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        dat = "Class2D/job013/run_it100_data.star"
        ddata = get_ordered_classes_arrays(mf, 11, 50, "", "mfile.png", dat)
        expx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 10
        expy = sum([[n] * 11 for n in reversed(range(10))], [])
        explabels = ["Class 62; 2.61%; 239 particles", "Class 60; 0.00%; 0 particles"]
        assert ddata.xvalues == expx
        assert ddata.yvalues == expy
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [""] * 10
        assert ddata.img == "Thumbnails/mfile.png"
        assert os.path.isfile(ddata.img)
        assert ddata.associated_data == [mf, dat]
        assert ddata.title == "2D class averages"

    def test_prepare_class2d_montage_parts_counting_uneven_rows_base64(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])
        mf = "Class2D/job013/run_it100_model.star"
        dat = "Class2D/job013/run_it100_data.star"
        ddata = get_ordered_classes_arrays(
            mf, 11, 50, "", "mfile.png", dat, base64_output=True
        )
        explabels = ["Class 62; 2.61%; 239 particles", "Class 60; 0.00%; 0 particles"]
        assert ddata.labels[0] == explabels[0]
        assert ddata.labels[-11] == explabels[1]
        assert ddata.labels[-10:] == [""] * 10
        assert ddata.images == "imagelist.json"
        assert os.path.isfile(ddata.images)
        assert ddata.associated_data == [mf, dat]
        assert ddata.title == "2D class averages"

    def test_error_returns_ResultsDisplayPending_with_traceback(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D"])

        with ProjectGraph() as pipeline:
            test_proc = pipeline.find_process("Class2D/job013/")

        # remove a file to cause an error
        os.unlink("Class2D/job013/run_it100_data.star")

        job = active_job_from_proc(test_proc)
        results = job.create_results_display()
        expected = {
            "title": "Results pending...",
            "message": "Error creating results display",
            "start_collapsed": True,
        }
        reason = "Error creating montage display object:"
        for i in ["title", "message"]:
            assert expected[i] == results[0].__dict__[i]
        assert results[0].__dict__["reason"].startswith(reason)

    def test_graph_from_starfile_single_data_series_no_x_or_labels(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = graph_from_starfile_cols(
            title="test graph",
            starfile="data.star",
            block="particles",
            ycols=["_rlnDefocusU"],
            data_series_labels=["Defocus"],
        )
        assert dataobj.title == "test graph"
        assert len(dataobj.xvalues) == 1
        assert len(dataobj.xvalues[0]) == 2377
        assert dataobj.xaxis_label == "Count"
        assert len(dataobj.yvalues) == 1
        assert len(dataobj.yvalues[0]) == 2377
        assert dataobj.yaxis_label == "_rlnDefocusU"
        assert dataobj.data_series_labels == ["Defocus"]
        assert dataobj.associated_data == ["data.star"]

    def test_graph_from_starfile_multiple_data_sets(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = graph_from_starfile_cols(
            title="test graph",
            starfile="data.star",
            block="particles",
            ycols=["_rlnDefocusU", "_rlnDefocusV"],
            xcols=["_rlnDefocusV", "_rlnDefocusU"],
            data_series_labels=["Series1", "Series2"],
        )

        assert dataobj.title == "test graph"
        assert len(dataobj.xvalues) == 2
        assert len(dataobj.xvalues[0]) == 2377
        assert len(dataobj.xvalues[1]) == 2377
        assert dataobj.xaxis_label == "_rlnDefocusV"
        assert len(dataobj.yvalues) == 2
        assert len(dataobj.yvalues[0]) == 2377
        assert len(dataobj.yvalues[1]) == 2377
        assert dataobj.yaxis_label == "_rlnDefocusU"
        assert dataobj.data_series_labels == ["Series1", "Series2"]
        assert dataobj.associated_data == ["data.star"]

    def test_histp_from_starfile_minimal(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = histogram_from_starfile_col(
            title="my histo",
            starfile="data.star",
            block="particles",
            data_col="_rlnClassNumber",
        )
        hfile = os.path.join(self.test_data, "ResultsFiles/histo_from_star1.json")
        with open(hfile, "r") as hf:
            expected = json.load(hf)
        assert dataobj.__dict__ == expected

    def test_histp_from_starfile_with_labels(self):
        starfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(starfile, "data.star")
        dataobj = histogram_from_starfile_col(
            title="my histo",
            starfile="data.star",
            block="particles",
            data_col="_rlnClassNumber",
            xlabel="Classy class",
            ylabel="How many is there be?",
            assoc_data=["completely_different.file", "file2.txt"],
        )
        hfile = os.path.join(self.test_data, "ResultsFiles/histo_from_star2.json")
        with open(hfile, "r") as hf:
            expected = json.load(hf)
        assert dataobj.__dict__ == expected

    def test_ResultsDisplayTable(self):
        rdtab = ResultsDisplayTable(
            title="my table",
            headers=["col1", "col2", "col3"],
            header_tooltips=["col1", "col2", "col3"],
            table_data=[[1, 1, 1], [2, 2, 2], [3, 3, 3]],
            associated_data=["file1.txt"],
        )
        assert rdtab.__dict__ == {
            "title": "my table",
            "dobj_type": "table",
            "headers": ["col1", "col2", "col3"],
            "header_tooltips": ["col1", "col2", "col3"],
            "table_data": [[1, 1, 1], [2, 2, 2], [3, 3, 3]],
            "associated_data": ["file1.txt"],
            "start_collapsed": False,
            "flag": "",
        }

    def test_ResultsDisplayTable_error_header_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayTable(
                title="my table",
                headers=["col1", "col2"],
                table_data=[[1, 1, 1], [2, 2, 2], [3, 3, 3]],
                associated_data=["file1.txt"],
            )

    def test_ResultsDisplayTable_error_data_mismatch(self):
        with self.assertRaises(ValueError):
            ResultsDisplayTable(
                title="my table",
                headers=["col1", "col2", "col3"],
                table_data=[[1, 1, 1], [2, 2, 2], [3, 3]],
                associated_data=["file1.txt"],
            )

    def test_merge_bin_imagestack_mrcs(self):
        instack = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        merged_n_binned = mrc_thumbnail(instack, 10, "test.png")

        assert merged_n_binned.shape == (9, 9)
        assert os.path.isfile("test.png")

    def test_merge_bin_imagestack_tiff(self):
        instack = os.path.join(self.test_data, "tiff_stack.tiff")
        merged_n_binned = tiff_thumbnail(instack, 40, "test.png")
        assert merged_n_binned.shape == (40, 40)
        assert os.path.isfile("test.png")

    def test_mini_montage_tifs(self):
        os.makedirs("Thumbs")
        tifffile = os.path.join(self.test_data, "tiff_stack.tiff")
        for n in range(10):
            shutil.copy(tifffile, f"image{n:03d}.tiff")
        imgs = ["image000.tiff", "image001.tiff", "image002.tiff", "image003.tiff"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f000.png")
        expected = {
            "title": "test",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_tifs_not_enough_imgs(self):
        os.makedirs("Thumbs")
        tifffile = os.path.join(self.test_data, "tiff_stack.tiff")
        for n in range(5):
            shutil.copy(tifffile, f"image{n:03d}.tiff")
        imgs = ["image000.tiff", "image001.tiff", "image002.tiff", "image003.tiff"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f000.png")
        expected = {
            "title": "test",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_mrcs(self):
        os.makedirs("Thumbs")
        mrcfile = os.path.join(self.test_data, "single.mrc")
        for n in range(10):
            shutil.copy(mrcfile, f"image{n:03d}.mrc")
        imgs = ["image000.mrc", "image001.mrc", "image002.mrc", "image003.mrc"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f000.png")
        expected = {
            "title": "test",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected

    def test_mini_montage_mrcs_not_enough_imgs(self):
        os.makedirs("Thumbs")
        mrcfile = os.path.join(self.test_data, "single.mrc")
        for n in range(4):
            shutil.copy(mrcfile, f"image{n:03d}.mrc")
        imgs = ["image000.mrc", "image001.mrc", "image002.mrc", "image003.mrc"]
        dispobj = mini_montage_from_many_files(
            imgs,
            outputdir="",
            nimg=5,
            montagesize=200,
            title="test",
        )
        assert os.path.isfile("Thumbnails/montage_f000.png")
        expected = {
            "title": "test",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3],
            "yvalues": [0, 0, 0, 0],
            "labels": imgs,
            "associated_data": imgs,
            "img": "Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_single_map(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "emd_3488.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["emd_3488.mrc"],
            outputdir="",
            maps_data="test",
        )
        expected = {
            "maps": ["emd_3488.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Map: emd_3488.mrc",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["emd_3488.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_maps_noopacity(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc", "map2.mrc"],
            outputdir="",
            maps_data="test",
        )
        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5, 0.5],
            "models": [],
            "title": "3D viewer: Overlaid maps",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_models_opacity(self):
        touch("model1.pdb")
        touch("model2.pdb")
        dispobj = make_map_model_thumb_and_display(
            models=["model1.pdb", "model2.pdb"],
            outputdir="",
        )
        expected = {
            "maps": [],
            "dobj_type": "mapmodel",
            "maps_opacity": [],
            "models": ["model1.pdb", "model2.pdb"],
            "title": "3D viewer: Overlaid models",
            "maps_data": "",
            "models_data": "model1.pdb, model2.pdb",
            "associated_data": ["model1.pdb", "model2.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_multiple_maps_opacity(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        shutil.copy(mapfile, "map2.mrc")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc", "map2.mrc"],
            maps_opacity=[1.0, 0.5],
            outputdir="",
            maps_data="test",
        )
        expected = {
            "maps": ["map1.mrc", "map2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0, 0.5],
            "models": [],
            "title": "3D viewer: Overlaid maps",
            "maps_data": "test",
            "models_data": "",
            "associated_data": ["map1.mrc", "map2.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobj.__dict__ == expected

    def test_make_mapmodel_thumbnail_1map_1model(self):
        mapfile = os.path.join(os.path.dirname(test_data.__file__), "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")
        touch("model1.pdb")
        dispobj = make_map_model_thumb_and_display(
            maps=["map1.mrc"],
            models=["model1.pdb"],
            outputdir="",
            maps_data="test",
        )
        expected = {
            "maps": ["map1.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["model1.pdb"],
            "title": "3D viewer: Overlaid map: map1.mrc model: model1.pdb",
            "maps_data": "test",
            "models_data": "model1.pdb",
            "associated_data": ["map1.mrc", "model1.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert dispobj.__dict__ == expected

    def test_mini_montage_from_stafile_movies_file(self):
        os.makedirs("Movies")
        movfile = os.path.join(self.test_data, "movies_mrcs.star")
        shutil.copy(movfile, "movies.star")
        mrcfile = os.path.join(self.test_data, "single_100.mrc")
        for n in range(1, 25):
            shutil.copy(mrcfile, f"Movies/20170629_{n:05d}_frameImage.mrcs")

        # with expected_warning(RuntimeWarning, "divide", nwarn=1):
        dispobj = mini_montage_from_starfile(
            starfile="movies.star",
            block="movies",
            column="_rlnMicrographMovieName",
            outputdir="",
            nimg=7,
            montagesize=640,
        )
        files = [f"Movies/20170629_{n:05d}_frameImage.mrcs" for n in range(1, 8)]
        expected = {
            "title": "movies.star; 7/24 images",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3, 4, 5, 6],
            "yvalues": [0, 0, 0, 0, 0, 0, 0],
            "labels": files,
            "associated_data": ["movies.star"],
            "img": "Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected, (dispobj.__dict__, expected)

    def test_mini_montage_from_starfile_particles_file(self):
        partsdir = "Extract/job007/Movies/"
        os.makedirs(partsdir)
        partsfile = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(partsfile, "particles.star")
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        partsdest = "Extract/job007/Movies/20170629_00021_frameImage.mrcs"
        shutil.copy(mrcfile, partsdest)
        dispobj = mini_montage_from_starfile(
            starfile="particles.star",
            block="particles",
            column="_rlnImageName",
            outputdir="",
            nimg=50,
            montagesize=640,
        )

        ns = range(1, 51).__reversed__()
        labs = [f"{n:06d}@{partsdir}20170629_00021_frameImage.mrcs" for n in ns]
        labs.reverse()
        expected = {
            "title": "particles.star; 50/2377 images",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] * 5,
            "yvalues": [4] * 10 + [3] * 10 + [2] * 10 + [1] * 10 + [0] * 10,
            "labels": labs,
            "associated_data": ["particles.star"],
            "img": "Thumbnails/montage_s000.png",
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobj.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_s000.png")

    def test_mini_montage_from_empty_file(self):
        partsdir = "Extract/job007/Movies/"
        os.makedirs(partsdir)
        partsfile = os.path.join(self.test_data, "class2d_empty.star")
        shutil.copy(partsfile, "particles.star")
        dispobj = mini_montage_from_starfile(
            starfile="particles.star",
            block="particles",
            column="_rlnImageName",
            outputdir="",
            nimg=50,
            montagesize=640,
        )

        expected = {
            "title": "Results pending...",
            "dobj_type": "pending",
            "message": "Error creating montage results display",
            "reason": (
                "No particles were found in file: particles.star,"
                " block: particles, column: _rlnImageName"
            ),
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobj.__dict__ == expected
        assert not os.path.isfile("Thumbnails/montage_s000.png")

    def test_mini_montage_with_missing_images(self):
        partsdir = "Extract/job007/Movies/"
        os.makedirs(partsdir)
        partsfile = os.path.join(self.test_data, "class2d_mixed_data.star")
        shutil.copy(partsfile, "particles.star")
        dispobj = mini_montage_from_starfile(
            starfile="particles.star",
            block="particles",
            column="_rlnImageName",
            outputdir="",
            nimg=50,
            montagesize=640,
        )

        expected = {
            "title": "Results pending...",
            "dobj_type": "pending",
            "message": "Error creating montage results display",
            "reason": (
                "Unable to parse images in star file. Possibly a "
                "mix of stack slices and single images, or files are missing"
            ),
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobj.__dict__ == expected
        assert not os.path.isfile("Thumbnails/montage_s000.png")

    def test_make_particle_coords_thumb(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(relion_version=4, dirs=["MotionCorr", "AutoPick"])
        mc_file = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        coords = "AutoPick/job006/Movies/20170629_00021_frameImage_autopick.star"
        dispobj = make_particle_coords_thumb(mc_file, coords, "")
        expected = {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc: "
            "242 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "AutoPick/job006/Movies/20170629_00021_frameImage_autopick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected
        assert os.path.isfile(dispobj.image_path)

    def test_make_mini_motage_from_mrcs(self):
        mrcs = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcs, os.path.join(self.test_dir, "img.mrcs"))
        dispobj = mini_montage_from_stack(
            outputdir="",
            stack_file="img.mrcs",
            nimg=11,
        )
        expected = {
            "title": "",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "yvalues": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            "labels": [
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ],
            "associated_data": ["img.mrcs"],
            "img": "Thumbnails/montage_m000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobj.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_m000.png")

    def test_creating_ResultsDisplayTextFile(self):
        f = os.path.join(self.test_data, "particles.star")
        shutil.copy(f, "particles.star")
        do = create_results_display_object("textfile", file_path="particles.star")
        assert type(do) is ResultsDisplayTextFile
        assert do.__dict__ == {
            "file_path": "particles.star",
            "dobj_type": "textfile",
            "start_collapsed": False,
            "title": "particles.star",
            "flag": "",
        }

    def test_creating_ResultsDisplayTextFile_invalid_format(self):
        f = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(f, "emd_3488.mrc")
        do = create_results_display_object("textfile", file_path="emd_3488.mrc")
        assert type(do) is ResultsDisplayPending
        assert do.__dict__["message"] == "Error creating results display"
        assert do.__dict__["reason"].startswith(
            "Error creating ResultsDisplayTextFile: File 'emd_3488.mrc' is not "
            "a valid text file \n"
        )

    def test_autoincrement_dispobj_number_with_existing_files(self):
        jobdir = "Import/job001/Thumbnails"
        os.makedirs(jobdir)
        for n in range(3):
            touch(os.path.join(jobdir, f"dispfile{n:03d}.png"))
        of = get_next_resultsfile_name(jobdir, "dispfile*.png")
        assert of == "Import/job001/Thumbnails/dispfile003.png"

    def test_autoincrement_dispobj_number_with_no_existing(self):
        jobdir = "Import/job001/Thumbnails"
        of = get_next_resultsfile_name(jobdir, "dispfile*.png")
        assert of == "Import/job001/Thumbnails/dispfile000.png"

    def test_creating_ResultsDisplayJson(self):
        f = os.path.join(self.test_data, "5me2_a_residue_coordinates.json")
        shutil.copy(f, "5me2_a_residue_coordinates.json")
        do = create_results_display_object(
            "json", file_path="5me2_a_residue_coordinates.json"
        )
        assert type(do) is ResultsDisplayJson
        assert do.__dict__ == {
            "file_path": "5me2_a_residue_coordinates.json",
            "dobj_type": "json",
            "start_collapsed": False,
            "title": "5me2_a_residue_coordinates.json",
            "flag": "",
        }

    def test_creating_mrc_slices_montage_multiple(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc", "emd_3488_mask.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["", ""]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test")
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                f"{self.test_data}/emd_3488_mask.mrc: xy",
                f"{self.test_data}/emd_3488_mask.mrc: xz",
                f"{self.test_data}/emd_3488_mask.mrc: yz",
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "xvalues": [0, 1, 2, 0, 1, 2],
            "yvalues": [0, 0, 0, 1, 1, 1],
        }

    def test_creating_mrc_slices_montage_multiple_base64(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc", "emd_3488_mask.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["", ""]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test", base64_output=True)
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/imagelist.json").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
                f"{self.test_data}/emd_3488_mask.mrc: xy",
                f"{self.test_data}/emd_3488_mask.mrc: xz",
                f"{self.test_data}/emd_3488_mask.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "associated_nodes": [],
        }

    def test_creating_mrc_slices_montage_single(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, [""]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test")
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": f"Map slices: {self.test_data}/emd_3488.mrc",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }

    def test_creating_mrc_slices_montage_single_base64(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, [""]))
        optimiser = {
            "name": f"{self.test_data}/emd_3488_optimiser.star",
            "type": "OptimiserData.star.relion.class3d",
        }
        rdo = make_mrcs_central_slices_montage(
            in_dict, "test", base64_output=True, optimiser_info=optimiser
        )
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/imagelist.json").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": f"Map slices: {self.test_data}/emd_3488.mrc",
            "associated_nodes": [optimiser],
        }

    def test_creating_mrc_slices_montage_multiple_with_labels(self):
        Path("test").mkdir()
        files = {"emd_3488.mrc", "emd_3488_mask.mrc"}
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["class1", "class2"]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test")
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                "class2: xy",
                "class2: xz",
                "class2: yz",
                "class1: xy",
                "class1: xz",
                "class1: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "xvalues": [0, 1, 2, 0, 1, 2],
            "yvalues": [0, 0, 0, 1, 1, 1],
        }

    def test_creating_mrc_slices_montage_multiple_with_labels_base64(self):
        Path("test").mkdir()
        files = {"emd_3488.mrc", "emd_3488_mask.mrc"}
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["class1", "class2"]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test", base64_output=True)
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/imagelist.json").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                "class1: xy",
                "class1: xz",
                "class1: yz",
                "class2: xy",
                "class2: xz",
                "class2: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "associated_nodes": [],
        }

    def test_creating_mrc_slices_montage_single_with_label(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["class 1"]))
        rdo = make_mrcs_central_slices_montage(in_dict, "test")
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: class 1",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }

    def test_creating_mrc_slices_montage_single_with_label_base64(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["class 1"]))
        optimiser = {
            "name": f"{self.test_data}/emd_3488_optimiser.star",
            "type": "OptimiserData.star.relion.class3d",
        }
        rdo = make_mrcs_central_slices_montage(
            in_dict, "test", base64_output=True, optimiser_info=optimiser
        )
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/imagelist.json").is_file()
        assert rdo.__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: class 1",
            "associated_nodes": [optimiser],
        }

    def test_creating_mrc_map_slice_3d_montage_base64(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        optimiser = {
            "name": f"{self.test_data}/emd_3488_optimiser.star",
            "type": "OptimiserData.star.relion.class3d",
        }
        in_dict = dict(zip(test_files, ["class 1"]))
        rdo = make_maps_slice_montage_and_3d_display(
            in_dict, "test", base64_output=True, optimiser_info=optimiser
        )
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/imagelist.json").is_file()
        print(rdo)
        assert rdo[0].__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: class 1",
            "associated_nodes": [optimiser],
        }

    def test_creating_mrc_montage_and_3d_multiple_comb_montage(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc", "emd_3488_mask.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["", "newname"]))
        rdos = make_maps_slice_montage_and_3d_display(in_dict, "test")
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert rdos[0].__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                "newname: xy",
                "newname: xz",
                "newname: yz",
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "xvalues": [0, 1, 2, 0, 1, 2],
            "yvalues": [0, 0, 0, 1, 1, 1],
        }
        assert rdos[1].__dict__ == {
            "title": f"3D viewer: {self.test_data}/emd_3488.mrc",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488.mrc"],
        }
        assert rdos[2].__dict__ == {
            "title": "3D viewer: newname",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488_mask.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488_mask.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488_mask.mrc"],
        }

    def test_creating_mrc_montage_and_3d_multiple_comb_montage_base64(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc", "emd_3488_mask.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["", "newname"]))
        optimiser = {
            "name": f"{self.test_data}/emd_3488_optimiser.star",
            "type": "OptimiserData.star.relion.class3d",
        }
        rdos = make_maps_slice_montage_and_3d_display(
            in_dict, "test", base64_output=True, optimiser_info=optimiser
        )
        assert Path("test/Thumbnails/imagelist.json").is_file()
        assert rdos[0].__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
                f"{self.test_data}/emd_3488_mask.mrc",
            ],
            "dobj_type": "gallery",
            "flag": "",
            "images": "test/Thumbnails/imagelist.json",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
                "newname: xy",
                "newname: xz",
                "newname: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices",
            "associated_nodes": [optimiser],
        }
        assert rdos[1].__dict__ == {
            "title": f"3D viewer: {self.test_data}/emd_3488.mrc",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488.mrc"],
        }
        assert rdos[2].__dict__ == {
            "title": "3D viewer: newname",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488_mask.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488_mask.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488_mask.mrc"],
        }

    def test_creating_mrc_montage_and_3d_multiple_separate_montages(self):
        Path("test").mkdir()
        files = ["emd_3488.mrc", "emd_3488_mask.mrc"]
        test_files = [str(Path(self.test_data) / f) for f in files]
        in_dict = dict(zip(test_files, ["", "newname"]))
        rdos = make_maps_slice_montage_and_3d_display(
            in_dict, "test", combine_montages=False
        )
        assert Path("test/Thumbnails/slices_montage_000.png").is_file()
        assert Path("test/Thumbnails/slices_montage_001.png").is_file()

        assert rdos[0].__dict__ == {
            "associated_data": [
                f"{self.test_data}/emd_3488.mrc",
            ],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_000.png",
            "labels": [
                f"{self.test_data}/emd_3488.mrc: xy",
                f"{self.test_data}/emd_3488.mrc: xz",
                f"{self.test_data}/emd_3488.mrc: yz",
            ],
            "start_collapsed": False,
            "title": f"Map slices: {self.test_data}/emd_3488.mrc",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert rdos[1].__dict__ == {
            "title": f"3D viewer: {self.test_data}/emd_3488.mrc",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488.mrc"],
        }
        assert rdos[2].__dict__ == {
            "associated_data": [f"{self.test_data}/emd_3488_mask.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "test/Thumbnails/slices_montage_001.png",
            "labels": [
                f"{self.test_data}/emd_3488_mask.mrc: xy",
                f"{self.test_data}/emd_3488_mask.mrc: xz",
                f"{self.test_data}/emd_3488_mask.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: newname",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert rdos[3].__dict__ == {
            "title": "3D viewer: newname",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [f"{self.test_data}/emd_3488_mask.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": f"{self.test_data}/emd_3488_mask.mrc",
            "models_data": "",
            "associated_data": [f"{self.test_data}/emd_3488_mask.mrc"],
        }


if __name__ == "__main__":
    unittest.main()
