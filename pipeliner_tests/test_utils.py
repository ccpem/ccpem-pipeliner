#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import shutil
import os
import sys
import tempfile
import uuid
import re
import math
from pathlib import Path
from unittest.mock import patch, Mock
from datetime import datetime
from collections import OrderedDict
from pipeliner.utils import (
    truncate_number,
    clean_jobname,
    run_subprocess,
    check_for_illegal_symbols,
    make_pretty_header,
    get_job_number,
    str_is_hex_colour,
    get_pipeliner_root,
    get_job_script,
    format_string_to_type_objs,
    get_job_runner_command,
    get_regenerate_results_command,
    is_uuid4,
    clean_job_dirname,
    file_in_project,
    convert_relative_filename,
    compare_nested_lists,
    get_file_size_mb,
    count_file_lines,
    date_time_tag,
    increment_file_basenames,
    get_mrc_map_summary,
    make_atomic_model_summary,
    make_pretty_size,
    get_directory_info,
    atomic_write_json,
    launch_detached_process,
)
from pipeliner.mrc_image_tools import get_mrc_dims
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import live_test


class PipelinerUtilsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_truncate_number_seven_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 7)
        assert trunc == "1.6666667", trunc

    def test_truncate_number_two_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 2)
        assert trunc == "1.67", trunc

    def test_truncate_number_seven_places_trailing_zeros(self):
        x = 2.0
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_two_places_with_two_places_input(self):
        x = 0.75
        trunc = truncate_number(x, 2)
        assert trunc == "0.75", trunc

    def test_truncate_number_with_int(self):
        x = 2
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_with_large_int(self):
        x = 1234567890
        trunc = truncate_number(x, 7)
        assert trunc == "1234567890", trunc

    def test_truncate_number_with_near_multiple_of_ten(self):
        x = 1234567890.1
        trunc = truncate_number(x, 0)
        assert trunc == "1234567890", trunc

    def test_truncate_number_zero_places_rounding_up(self):
        x = 1.89
        trunc = truncate_number(x, 0)
        assert trunc == "2", trunc

    def test_truncate_number_zero_places_rounding_down(self):
        x = 1.324
        trunc = truncate_number(x, 0)
        assert trunc == "1", trunc

    def test_truncate_negative_number_zero_places_rounding_down(self):
        x = -1.324
        trunc = truncate_number(x, 0)
        assert trunc == "-1", trunc

    def test_truncate_negative_number_zero_places_rounding_up(self):
        x = -509.87
        trunc = truncate_number(x, 0)
        assert trunc == "-510", trunc

    def test_clean_jobname_with_trailing_slash(self):
        jobname = "Refine/job012/"
        cleaned = clean_jobname(jobname)
        assert cleaned == jobname

    def test_clean_jobname_without_trailing_slash(self):
        jobname = "Refine/job012"
        cleaned = clean_jobname(jobname)
        assert cleaned == "Refine/job012/"

    def test_clean_jobname_leading_slash(self):
        jobname = clean_jobname("/testjob/job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_double_slash(self):
        jobname = clean_jobname("testjob//job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_multiple_problems(self):
        jobname = clean_jobname("/testjob////////////job003")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_illegal_character(self):
        with self.assertRaises(ValueError):
            clean_jobname("testjob/job@@3/")

    def test_get_mrc_dims_3d(self):
        dims = get_mrc_dims(os.path.join(self.test_data, "emd_3488.mrc"))
        assert dims == (100, 100, 100)

    def test_check_for_illegal_symbols_with_good_input(self):
        result = check_for_illegal_symbols("abc123")
        assert result is None

    def test_check_for_illegal_symbols_with_bad_input(self):
        result = check_for_illegal_symbols("bad string *")
        assert "'*' in input" in result

    def test_check_for_illegal_symbols_with_bad_alias(self):
        result = check_for_illegal_symbols("bad alias /", string_name="alias")
        assert "'/' in alias" in result

    def test_check_for_illegal_symbols_with_excluded_symbol(self):
        result = check_for_illegal_symbols("good pipeline /", exclude="*/!")
        assert result is None

    def test_make_pretty_header_main(self):
        header = make_pretty_header("the test text")
        assert header.split("\n") == [
            "-=-=-=-=-=-=-",
            "the test text",
            "-=-=-=-=-=-=-",
        ]

    def test_make_pretty_header_main_with_breaks(self):
        header = make_pretty_header("the test text\nis of varied lengths\nyes!")
        assert header.split("\n") == [
            "-=-=-=-=-=-=-=-=-=-=",
            "the test text",
            "is of varied lengths",
            "yes!",
            "-=-=-=-=-=-=-=-=-=-=",
        ]

    def test_make_pretty_header_single_char(self):
        header = make_pretty_header("the test text", char="-")
        assert header.split("\n") == [
            "-------------",
            "the test text",
            "-------------",
        ]

    def test_make_pretty_header_sub_with_breaks(self):
        header = make_pretty_header(
            "the test text\nis of varied lengths\nyes!", char="-", top=False
        )
        assert header.split("\n") == [
            "the test text",
            "is of varied lengths",
            "yes!",
            "--------------------",
        ]

    def test_make_pretty_header_long_char(self):
        header = make_pretty_header("the test text", char="12345678")
        assert header.split("\n") == [
            "1234567812345",
            "the test text",
            "1234567812345",
        ]

    def test_get_job_number_no_slash(self):
        jn = get_job_number("Import/job001")
        assert jn == 1

    def test_get_job_number_with_slash(self):
        jn = get_job_number("Import/job001/")
        assert jn == 1

    def test_str_is_hexcolour(self):
        assert str_is_hex_colour("#122334")
        assert not str_is_hex_colour("122334")
        assert str_is_hex_colour("#ffffff")
        assert not str_is_hex_colour("#abcdeh")
        assert not str_is_hex_colour("#ffff")
        assert str_is_hex_colour("0x122334", allow_0x=True)
        assert str_is_hex_colour("0xffffff", allow_0x=True)
        assert not str_is_hex_colour("0x122334", allow_0x=False)
        assert not str_is_hex_colour("0xffffff", allow_0x=False)
        assert not str_is_hex_colour("0xabcdeh", allow_0x=True)
        assert not str_is_hex_colour("0xffff", allow_0x=True)

    def test_get_pipeliner_root(self):
        pipeliner_root = get_pipeliner_root()
        assert pipeliner_root.is_dir()
        assert pipeliner_root.name == "pipeliner"
        assert (pipeliner_root / "utils.py").is_file()

    def test_get_job_script(self):
        script = get_job_script("molrep/molrep_tools.py")
        assert script == str(
            Path(__file__).parent.parent
            / "pipeliner"
            / "scripts"
            / "job_scripts"
            / "molrep"
            / "molrep_tools.py"
        )
        assert os.path.isfile(script)

    def test_get_job_script_bad_name(self):
        with self.assertRaisesRegex(FileNotFoundError, "bad_filename.py"):
            get_job_script("bad_filename.py")

    def test_get_job_runner_command(self):
        command = get_job_runner_command()
        assert command[-1].endswith("job_runner.py")
        assert os.path.isfile(command[-1])

    def test_get_regenerate_results_command(self):
        command = get_regenerate_results_command()
        assert command[-1].endswith("regenerate_results.py")
        assert os.path.isfile(command[-1])

    def test_format_str_to_num_int(self):
        assert format_string_to_type_objs("12") == 12

    def test_format_str_to_num_float(self):
        assert format_string_to_type_objs("12.34") == 12.34

    def test_format_str_to_num_float_e(self):
        assert format_string_to_type_objs("12e100") == 12e100

    def test_format_str_to_num_NaN(self):
        x = format_string_to_type_objs("NaN")
        assert type(x) is float

    def test_format_str_to_num_true(self):
        assert format_string_to_type_objs("True") is True

    def test_format_str_to_num_false(self):
        assert format_string_to_type_objs("False") is False

    def test_format_str_to_num_None(self):
        assert format_string_to_type_objs("None") is None

    def test_is_uuid(self):
        assert not is_uuid4("Not a uid")
        assert is_uuid4(str(uuid.uuid4()))
        assert not is_uuid4(str(uuid.uuid1))

    def test_clean_job_dirname_no_slash(self):
        clean = clean_job_dirname("Import/job001")
        assert clean == "Import/job001/"

    def test_clean_job_dirname_no_change(self):
        clean = clean_job_dirname("Import/job001/")
        assert clean == "Import/job001/"

    def test_clean_job_dirname_invalid_missing_job(self):
        with self.assertRaises(ValueError):
            clean_job_dirname("Import/")

    def test_clean_job_dirname_invalid_not_relative(self):
        os.makedirs("Import/job001")
        test_dir = os.path.abspath("Import/job001")
        with self.assertRaises(ValueError):
            clean_job_dirname(test_dir)

    def test_check_if_file_in_project(self):
        assert file_in_project("file.txt")
        assert not file_in_project("~/file.txt")
        assert not file_in_project("../../file.txt")
        assert not file_in_project("/file.txt")

    def test_convert_relative_filepath(self):
        for i in [
            "~/file.txt",
            "../../file.txt",
            "../file.txt",
            "file.txt",
            "/file.txt",
        ]:
            assert convert_relative_filename(i) == "file.txt"

    def test_convert_relative_filepath_with_dirs(self):
        for i in [
            "~/mydir/file.txt",
            "../../mydir/file.txt",
            "../mydir/file.txt",
            "mydir/file.txt",
            "/mydir/file.txt",
        ]:
            assert convert_relative_filename(i) == "mydir/file.txt"

    def test_convert_relative_filepath_with_dirs_internal_dotdotslasd(self):
        # very weird edge case but will test for it...
        assert convert_relative_filename("~/../file.txt") == "file.txt"
        assert (
            convert_relative_filename("../../mydir/../file.txt") == "mydir/../file.txt"
        )

    def test_compare_nested_lists(self):
        l1 = [1.0, 2.0, 3.0, "a"]
        l1b = [1.0, 2.0, 3.0, "b"]
        l2 = [1.0, 2.0, 3.1, "a"]
        l3 = [[1.0, 2.0, 3.0], [1.0, 2.0, 3.0]]
        l4 = [[1.0, 2.0, 3.0], [1.0, 2.0, 3.1]]
        l4b = [[1.0, 2.0, 3.0], [1.0, 2.0, 3.1], 1]
        l5 = [[1.0, 2.0, 3.0], [1.0, 2.0]]
        l6 = [[1, 2, 3], [1, 2, 3]]
        l7 = [["1", "2", "3"], ["1", "2", "3"]]

        assert compare_nested_lists(l1, l1)
        assert not compare_nested_lists(l1, l1b)
        assert compare_nested_lists(l1, l2, tolerance=0.1)
        assert not compare_nested_lists(l1, l2)
        assert not compare_nested_lists(l1, l2, tolerance=0.0001)
        assert compare_nested_lists(l3, l4, tolerance=0.1)
        assert not compare_nested_lists(l3, l4)
        assert not compare_nested_lists(l3, l4b)
        assert not compare_nested_lists(l3, l4, tolerance=0.0001)
        assert not compare_nested_lists(l4, l5)
        assert compare_nested_lists(l6, l3, tolerance=0)
        assert not compare_nested_lists(l6, l4, tolerance=0.02)
        assert not compare_nested_lists(l6, l7)
        assert compare_nested_lists(l7, l4, tolerance=0.5)

    def test_compare_nested_lists_of_dicts(self):
        l1 = [{"test": 123}, {"test": 456}]
        l1b = [{"test": 123}, {"test": 457}]
        l1c = [{"test2": 123}, {"test": 456}]
        l2 = [{"test": 123}, {"test": 457}, "a"]
        l3 = [
            [{"test": 123}, {"test": 457}],
            [
                {"test": 321},
                {"test": 765},
            ],
        ]
        l4 = [[1.0, 2.0, 3.0], [1.0, 2.0, 3.1]]

        assert compare_nested_lists(l1, l1)
        assert compare_nested_lists(l1, l1.copy())
        assert not compare_nested_lists(l1, l1b)
        assert not compare_nested_lists(l1, l1c)
        assert compare_nested_lists(l1, l1, tolerance=0.1)
        assert not compare_nested_lists(l1, l2)
        assert compare_nested_lists(l3, l3)
        assert not compare_nested_lists(l1, l3)
        assert not compare_nested_lists(l3, l4, tolerance=0.001)

    def test_get_file_size_mb(self):
        file = Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"
        assert get_file_size_mb(file) == 3.82

    def test_count_file_lines(self):
        testfile = os.path.join(self.test_data, "1AKE_cha_molrep_to_move.cif")
        assert count_file_lines(testfile) == 1739

    def test_date_time_tag(self):
        dtt = date_time_tag()
        assert re.match(r"^\d+-\d+-\d+ \d+:\d+:\d+\.\d+$", dtt)

    def test_date_time_tag_standard_compact(self):
        dtt = date_time_tag(compact=True)
        assert len(dtt) == 14
        assert dtt.isdigit()

    @patch("pipeliner.utils.datetime")
    def test_date_time_tag_mock(self, mock_datetime):
        mock_datetime.now.return_value = datetime(2024, 1, 26, 13, 27, 37, 653861)
        assert date_time_tag() == "2024-01-26 13:27:37.653"
        assert date_time_tag(compact=True) == "20240126132737"

    def test_increment_filenames_catch_duplicates_error(self):
        files = [
            "Import/d1/job002_myfile.txt",
            "Import/job002/myfile.txt",
            "Import/job003/myfile.txt",
            "Import/d3/different.txt",
        ]
        with self.assertRaises(ValueError):
            increment_file_basenames(files)

    def test_get_map_header_summary(self):
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        assert get_mrc_map_summary(mapfile) == OrderedDict(
            [
                ("Box size (pixels)", (100, 100, 100)),
                ("Mode", 2),
                ("Start offsets (pixels)", (0, 0, 0)),
                ("Box size (angstroms)", (105.0, 105.0, 105.0)),
                ("Cell angles", (90.0, 90.0, 90.0)),
                ("Axis order", (1, 2, 3)),
                ("Origin", (0.0, 0.0, 0.0)),
                ("Voxel size (angstroms)", (1.05, 1.05, 1.05)),
                ("Min", -0.54143),
                ("Max", 0.70044),
                ("Mean", 0.00152),
                ("Std", 0.03715),
            ]
        )

    @live_test(programs=["gemmi"])
    def test_get_model_summary(self):
        modelfile = os.path.join(self.test_data, "5ni1_updated.cif")
        summary_file = "5ni1_summary.txt"
        make_atomic_model_summary(modelfile, summary_file)
        assert os.path.isfile(summary_file)
        assert math.isclose(os.stat(summary_file).st_size, 866, rel_tol=0.01)

    @staticmethod
    def test_get_pretty_file_sizes():
        exp = ["10 B", "9 KB", "9.54 MB", "9.31 GB", "9.09 TB", "8.88 PB"]
        vals = [10, 10000, 10**7, 10**10, 10**13, 10**16]
        for expected, val in zip(exp, vals):
            actual = make_pretty_size(val)
            assert actual == expected

    @staticmethod
    def make_test_dirs():
        dirs = ["test", "test/t1", "test/t2", "test/t3", "test/t3/t4"]
        for d in dirs:
            Path(d).mkdir(parents=True, exist_ok=True)
        files = []
        for n in range(3):
            for d in dirs:
                f = Path(d) / f"test{n}.txt"
                files.append(f)
                f.touch()
        return files

    def test_get_dir_info(self):
        files = self.make_test_dirs()
        exp = sorted(
            [(str(x.relative_to("test")), "0 B") for x in files],
            key=lambda x: (x[0].count("/"), x[0]),
        )
        assert get_directory_info("test") == exp

    def test_atomic_write_json(self):
        """
        Test atomic_write_json() writes JSON correctly and does not leave stray
        temporary files behind.
        """
        # Prepare test objects
        test_json = (
            '{"bool_field": true, "float_field": 1.23e-45, "int_array": [1, 2, 3], '
            '"int_field": 123, "string_field": "example"}'
        )
        test_obj = json.loads(test_json)
        test_file = Path(self.test_dir) / "test.json"

        # Write the JSON file
        atomic_write_json(test_obj, str(test_file), sort_keys=True)

        # Check the only file in the directory is the intended test file
        file_list = os.listdir(self.test_dir)
        assert len(file_list) == 1
        assert file_list[0] == "test.json"

        # Check the JSON is correct
        with open(test_file) as tf:
            written_json = tf.read()
            assert written_json == test_json


def test_run_subprocess_uses_normal_ld_path_outside_pyinstaller_mode(monkeypatch):
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == "test_ld_path"


def test_run_subprocess_does_not_set_ld_path_outside_pyinstaller_mode(monkeypatch):
    monkeypatch.delenv("LD_LIBRARY_PATH", raising=False)
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == ""


def test_run_subprocess_unsets_ld_path_in_pyinstaller_bundle_mode(monkeypatch):
    # Pretend we're running in a Pyinstaller bundle
    # See https://pyinstaller.org/en/stable/runtime-information.html
    monkeypatch.setattr(sys, "frozen", True, raising=False)
    monkeypatch.setattr(sys, "_MEIPASS", "", raising=False)
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == ""


def test_run_subprocess_uses_orig_ld_path_in_pyinstaller_bundle_mode(monkeypatch):
    # Pretend we're running in a Pyinstaller bundle
    # See https://pyinstaller.org/en/stable/runtime-information.html
    monkeypatch.setattr(sys, "frozen", True, raising=False)
    monkeypatch.setattr(sys, "_MEIPASS", "", raising=False)
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    monkeypatch.setenv("LD_LIBRARY_PATH_ORIG", "orig_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == "orig_ld_path"


@patch("pipeliner.utils.subprocess_popen")
def test_launch_detached_process_still_running(mock_subprocess_popen):
    # Mock the Popen object and its methods
    mock_process = Mock()
    mock_process.wait.return_value = None  # Process is still running
    mock_subprocess_popen.return_value = mock_process

    result = launch_detached_process(["some", "command"], 0.1)
    assert result is None
    mock_process.wait.assert_called_with(timeout=0.1)


@patch("pipeliner.utils.subprocess_popen")
def test_launch_detached_process_returns_nonzero(mock_subprocess_popen):
    # Mock the Popen object and its methods
    mock_process = Mock()
    mock_process.wait.return_value = -1  # Process finsihed/failed
    mock_subprocess_popen.return_value = mock_process

    result = launch_detached_process(["some", "command"], 0.1)
    assert result == -1


def test_increment_filenames_all_jobs():
    files = [
        "Import/job001/myfile.txt",
        "Import/job002/myfile.txt",
        "Import/job001/different.txt",
    ]
    assert increment_file_basenames(files) == {
        "Import/job001/myfile.txt": "job001_myfile.txt",
        "Import/job002/myfile.txt": "job002_myfile.txt",
        "Import/job001/different.txt": "different.txt",
    }


def test_increment_filenames_not_jobs():
    files = [
        "Import/d1/myfile.txt",
        "Import/d2/myfile.txt",
        "Import/d3/different.txt",
    ]
    assert increment_file_basenames(files) == {
        "Import/d1/myfile.txt": "001_myfile.txt",
        "Import/d2/myfile.txt": "002_myfile.txt",
        "Import/d3/different.txt": "different.txt",
    }


def test_increment_filenames_mix_jobs_and_not():
    files = [
        "Import/d1/myfile.txt",
        "Import/job002/myfile.txt",
        "Import/d3/different.txt",
    ]
    assert increment_file_basenames(files) == {
        "Import/d1/myfile.txt": "001_myfile.txt",
        "Import/job002/myfile.txt": "job002_myfile.txt",
        "Import/d3/different.txt": "different.txt",
    }


def test_increment_filenames_noext():
    files = [
        "Import/job001/myfile.txt",
        "Import/job002/myfile.txt1",
        "myfile.txt",
    ]
    assert increment_file_basenames(files, include_ext=False) == {
        "Import/job001/myfile.txt": "job001_myfile",
        "Import/job002/myfile.txt1": "job002_myfile",
        "myfile.txt": "001_myfile",
    }


def test_increment_filenames_noext_nojobs():
    files = [
        "Import/dir1/myfile.txt1",
        "Import/job002/myfile.txt2",
        "myfile.txt",
        "different.pdb",
        "dir2/different.txt",
    ]
    assert increment_file_basenames(files, include_ext=False) == {
        "Import/dir1/myfile.txt1": "001_myfile",
        "Import/job002/myfile.txt2": "job002_myfile",
        "myfile.txt": "002_myfile",
        "different.pdb": "003_different",
        "dir2/different.txt": "004_different",
    }


def test_increment_filenames_noext_nojobs_rel_paths_from_jobdir():
    """This test shows how the function behaves if the file paths are given relative to
    the job directory, though this is not recommended and it should typically be called
    with paths relative to the project."""
    files = [
        "../../Import/dir1/myfile.txt1",
        "../../Import/job002/myfile.txt2",
        "../../myfile.txt",
        "../../different.pdb",
        "../../dir2/different.txt",
        "myfile.txt",
    ]
    assert increment_file_basenames(files, include_ext=False) == {
        "../../Import/dir1/myfile.txt1": "001_myfile",
        "../../Import/job002/myfile.txt2": "job002_myfile",
        "../../myfile.txt": "002_myfile",
        "../../different.pdb": "003_different",
        "../../dir2/different.txt": "004_different",
        "myfile.txt": "005_myfile",
    }


if __name__ == "__main__":
    unittest.main()
