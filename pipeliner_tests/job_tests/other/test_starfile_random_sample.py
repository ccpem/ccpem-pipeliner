#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path

from pipeliner.utils import get_job_script, get_python_command, touch
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHCOORDS,
)
from pipeliner.scripts.job_scripts import starfile_random_sample
from pipeliner.starfile_handler import DataStarFile
from pipeliner.results_display_objects import (
    ResultsDisplayMontage,
    ResultsDisplayTextFile,
)


class SampleStarFileTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/StarFileSample"
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_parts(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "sample_parts_job.star"),
            input_nodes={"particles.star": NODE_PARTICLEGROUPMETADATA + ".star"},
            output_nodes={
                "sampled_particles.star": NODE_PARTICLEGROUPMETADATA + ".star"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f'{get_job_script("starfile_random_sample.py")} --input_file '
                "particles.star --n_samples 10 --block particles --output "
                "Select/job999/sampled_particles.star",
            ],
        )

    def test_results_display_parts(self):
        mp = "Extract/job007/Movies"
        Path(mp).mkdir(parents=True)
        rdo = ResultsDisplayMontage(
            title="Select/job999/sampled_particles.star; 4/4 images",
            xvalues=[0, 1, 2, 3],
            yvalues=[0, 0, 0, 0],
            labels=[
                "000001@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000002@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000003@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000004@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
            ],
            associated_data=["Select/job999/sampled_particles.star"],
            img="Select/job999/Thumbnails/montage_s000.png",
        )

        shutil.copy(
            Path(self.test_data) / "image_stack.mrcs",
            Path(self.test_dir) / mp / "20170629_00021_frameImage.mrcs",
        )
        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "sample_parts_job.star"),
            expected_display_objects=[rdo],
            files_to_create={
                "sampled_particles.star": os.path.join(
                    self.test_data, "particles_short.star"
                )
            },
        )

    def test_get_commands_mics(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "sample_mics_job.star"),
            input_nodes={"mics.star": NODE_MICROGRAPHGROUPMETADATA + ".star"},
            output_nodes={
                "sampled_micrographs.star": NODE_MICROGRAPHGROUPMETADATA + ".star"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f'{get_job_script("starfile_random_sample.py")} --input_file '
                "mics.star --n_samples 10 --block micrographs --output "
                "Select/job999/sampled_micrographs.star",
            ],
        )

    def test_results_display_mics(self):
        mp = "MotionCorr/job002/Movies"
        Path(mp).mkdir(parents=True)
        rdo = ResultsDisplayMontage(
            title="Select/job999/sampled_micrographs.star; 2/2 images",
            xvalues=[0, 1],
            yvalues=[0, 0],
            labels=[
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "MotionCorr/job002/Movies/20170629_00022_frameImage.mrc",
            ],
            associated_data=["Select/job999/sampled_micrographs.star"],
            img="Select/job999/Thumbnails/montage_f000.png",
        )

        shutil.copy(
            Path(self.test_data) / "single.mrc",
            Path(self.test_dir) / mp / "20170629_00021_frameImage.mrc",
        )
        shutil.copy(
            Path(self.test_data) / "single.mrc",
            Path(self.test_dir) / mp / "20170629_00022_frameImage.mrc",
        )
        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "sample_mics_job.star"),
            expected_display_objects=[rdo],
            files_to_create={
                "sampled_micrographs.star": os.path.join(
                    self.test_data, "micrographs_short.star"
                )
            },
        )

    def test_get_commands_movies(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "sample_movies_job.star"),
            input_nodes={"movies.star": NODE_MICROGRAPHMOVIEGROUPMETADATA + ".star"},
            output_nodes={
                "sampled_movies.star": NODE_MICROGRAPHMOVIEGROUPMETADATA + ".star"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f'{get_job_script("starfile_random_sample.py")} --input_file '
                "movies.star --n_samples 10 --block movies --output "
                "Select/job999/sampled_movies.star",
            ],
        )

    def test_results_display_movies(self):
        mp = "Movies"
        Path(mp).mkdir(parents=True)
        rdo = ResultsDisplayMontage(
            title="Select/job999/sampled_movies.star; 2/2 images",
            xvalues=[0, 1],
            yvalues=[0, 0],
            labels=[
                "Movies/20170629_00001_frameImage.mrcs",
                "Movies/20170629_00002_frameImage.mrcs",
            ],
            associated_data=["Select/job999/sampled_movies.star"],
            img="Select/job999/Thumbnails/montage_f000.png",
        )

        shutil.copy(
            Path(self.test_data) / "image_stack.mrcs",
            Path(self.test_dir) / mp / "20170629_00001_frameImage.mrcs",
        )
        shutil.copy(
            Path(self.test_data) / "image_stack.mrcs",
            Path(self.test_dir) / mp / "20170629_00002_frameImage.mrcs",
        )
        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "sample_movies_job.star"),
            expected_display_objects=[rdo],
            files_to_create={
                "sampled_movies.star": os.path.join(
                    self.test_data, "movies_mrcs_short.star"
                )
            },
        )

    def test_get_commands_coords_files(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "sample_coord_files_job.star"),
            input_nodes={"autopick.star": NODE_MICROGRAPHCOORDSGROUP + ".star"},
            output_nodes={
                "sampled_coordinate_files.star": NODE_MICROGRAPHCOORDSGROUP + ".star"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f'{get_job_script("starfile_random_sample.py")} --input_file '
                "autopick.star --n_samples 10 --block coordinate_files --output "
                "Select/job999/sampled_coordinate_files.star",
            ],
        )

    def test_results_display_coords_files(self):
        # ToDo: This is done because of issues with how the general test is structured,
        #  it needs to be refactored
        Path("Select/job999/").mkdir(parents=True)
        touch("Select/job999/sampled_coordinate_files.star")
        rdo = ResultsDisplayTextFile(
            title="Select/job999/sampled_coordinate_files.star",
            file_path="Select/job999/sampled_coordinate_files.star",
        )
        shutil.rmtree("Select/job999")
        # ---- end code to remove once test is refactored ---

        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "sample_coord_files_job.star"),
            expected_display_objects=[rdo],
            files_to_create={"sampled_coordinate_files.star": ""},
            print_dispobj=True,
        )

    def test_get_commands_coords(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "sample_coords_job.star"),
            input_nodes={"mic1_coords.star": NODE_MICROGRAPHCOORDS + ".star"},
            output_nodes={"sampled_coordinates.star": NODE_MICROGRAPHCOORDS + ".star"},
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f'{get_job_script("starfile_random_sample.py")} --input_file '
                "mic1_coords.star --n_samples 10 --block coordinates --output "
                "Select/job999/sampled_coordinates.star",
            ],
        )

    def test_results_display_coords(self):
        # ToDo: This is done because of issues with how the general test is structured,
        #  it needs to be refactored
        Path("Select/job999/").mkdir(parents=True)
        touch("Select/job999/sampled_coordinates.star")
        rdo = ResultsDisplayTextFile(
            title="Select/job999/sampled_coordinates.star",
            file_path="Select/job999/sampled_coordinates.star",
        )
        shutil.rmtree("Select/job999")
        # ---- end code to remove once test is refactored ---

        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "sample_coords_job.star"),
            expected_display_objects=[rdo],
            files_to_create={"sampled_coordinates.star": ""},
            print_dispobj=True,
        )

    def test_script_mics(self):
        parts = Path(self.test_data) / "corrected_micrographs.star"
        shutil.copy(parts, "mics.star")
        starfile_random_sample.main(
            [
                "--input_file",
                "mics.star",
                "--n_samples",
                "10",
                "--block",
                "micrographs",
                "--output",
                "Select/job999/sampled_micrographs.star",
            ]
        )
        assert os.path.isfile("Select/job999/sampled_micrographs.star")
        df = DataStarFile("Select/job999/sampled_micrographs.star").loop_as_list(
            "micrographs"
        )
        assert len(df) == 10

    def test_script_movies(self):
        parts = Path(self.test_data) / "movies.star"
        shutil.copy(parts, "movies.star")
        starfile_random_sample.main(
            [
                "--input_file",
                "movies.star",
                "--n_samples",
                "10",
                "--block",
                "movies",
                "--output",
                "Select/job999/sampled_movies.star",
            ]
        )
        assert os.path.isfile("Select/job999/sampled_movies.star")
        df = DataStarFile("Select/job999/sampled_movies.star").loop_as_list("movies")
        assert len(df) == 10

    def test_script_coords(self):
        parts = Path(self.test_data) / "coords.star"
        shutil.copy(parts, "coords.star")
        starfile_random_sample.main(
            [
                "--input_file",
                "coords.star",
                "--n_samples",
                "10",
                "--block",
                "coordinates",
                "--output",
                "Select/job999/sampled_coordinates.star",
            ]
        )
        assert os.path.isfile("Select/job999/sampled_coordinates.star")
        df = DataStarFile("Select/job999/sampled_coordinates.star").loop_as_list(
            "coordinates"
        )
        assert len(df) == 7

    def test_script_parts(self):
        parts = Path(self.test_data) / "class2d_data.star"
        shutil.copy(parts, "class2d_data.star")
        starfile_random_sample.main(
            [
                "--input_file",
                "class2d_data.star",
                "--n_samples",
                "10",
                "--block",
                "particles",
                "--output",
                "Select/job999/sampled_particles.star",
            ]
        )
        assert os.path.isfile("Select/job999/sampled_particles.star")
        df = DataStarFile("Select/job999/sampled_particles.star").loop_as_list(
            "particles"
        )
        assert len(df) == 10


if __name__ == "__main__":
    unittest.main()
