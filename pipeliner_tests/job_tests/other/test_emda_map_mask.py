#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.data_structure import NODE_MASK3D, NODE_DENSITYMAP


class EmdaMaskFromMap(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="emda_mask_from_map")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EmdaMapMask/emda_mapmask.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={"emda_halfmapmask_1.mrc": f"{NODE_MASK3D}.mrc.emda.mapmask"},
            expected_commands=[
                "emda2 halfmapmask "
                "--half1 ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "--half2 ../../Import/job002/3488_run_half2_class001_unfil.mrc "
            ],
            show_coms=True,
            show_outputnodes=True,
        )

    def test_mask_from_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/EmdaMapMask/emda_mapmask.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
            ],
            expected_outfiles=[
                "emda_halfmapmask_1.mrc",
                "run.out",
                "run.err",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        # check output size
        output_maskfile = os.path.join(
            self.test_dir,
            "EmdaMapMask",
            "job998",
            "emda_halfmapmask_1.mrc",
        )
        assert math.isclose(
            os.stat(output_maskfile).st_size,
            4001024,
            rel_tol=0.01,
        )
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: Mask",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc: xy",
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc: xz",
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc: yz",
            ],
            "associated_data": ["EmdaMapMask/job998/emda_halfmapmask_1.mrc"],
            "img": "EmdaMapMask/job998/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid maps",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc",
            ],
            "maps_opacity": [1.0, 0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": (
                "Import/job001/3488_run_half1_class001_unfil.mrc, "
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc"
            ),
            "models_data": "",
            "associated_data": [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "EmdaMapMask/job998/emda_halfmapmask_1.mrc",
            ],
        }


if __name__ == "__main__":
    unittest.main()
