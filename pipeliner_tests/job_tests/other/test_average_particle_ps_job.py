#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import mrcfile
from pathlib import Path
import shlex

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_IMAGE2DMETADATA,
    NODE_IMAGE2D,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.scripts.job_scripts.average_power_spectrum import (
    aps_find_res,
    aps_calc_avg_ps,
)
from pipeliner.job_factory import new_job_of_type


class APSTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="apstest_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_parts_files(self):
        datafile = Path(self.test_data) / "class2d_data.star"
        parts_file = Path(self.test_data) / "image_stack.mrcs"
        shutil.copy(datafile, "parts.star")
        os.makedirs("Extract/job007/Movies/")
        shutil.copy(parts_file, "Extract/job007/Movies/20170629_00021_frameImage.mrcs")

    def test_get_command_no_pad(self):
        self.setup_parts_files()
        jf = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/no_pad_job.star"
        job_generate_commands_test(
            jobfile=str(jf),
            input_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.aligned"
            },
            output_nodes={
                "average_ps.mrc": f"{NODE_IMAGE2D}.mrc.power_spectrum.averaged",
                "selected_features.star": f"{NODE_IMAGE2DMETADATA}.star",
            },
            expected_commands=[
                "relion_star_handler --i parts.star --o "
                "ImageAnalysis/job999/selected_class.star --select rlnClassNumber "
                "--minval 1 --maxval 1",
                "relion_stack_create --i ImageAnalysis/job999/selected_class.star --o "
                "ImageAnalysis/job999/aligned_particles --apply_transformation",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_calc_avg_ps.py')} --stack"
                " ImageAnalysis/job999/aligned_particles.mrcs --outdir "
                "ImageAnalysis/job999/",
                "relion_display --i ImageAnalysis/job999/average_ps.mrc --pick --o "
                "ImageAnalysis/job999/average_ps_coords.star --scale 4 "
                "--particle_radius 4",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_find_res.py')} "
                "--ps_size 64 --apix 3.54 "
                "--coords ImageAnalysis/job999/average_ps_coords.star --outdir "
                "ImageAnalysis/job999/",
            ],
        )

    def test_get_command_with_pad(self):
        self.setup_parts_files()
        jf = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/with_pad_job.star"
        job_generate_commands_test(
            jobfile=str(jf),
            input_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.aligned"
            },
            output_nodes={
                "average_ps.mrc": f"{NODE_IMAGE2D}.mrc.power_spectrum.averaged",
                "selected_features.star": f"{NODE_IMAGE2DMETADATA}.star",
            },
            expected_commands=[
                "relion_star_handler --i parts.star --o "
                "ImageAnalysis/job999/selected_class.star --select rlnClassNumber "
                "--minval 1 --maxval 1",
                "relion_stack_create --i ImageAnalysis/job999/selected_class.star --o "
                "ImageAnalysis/job999/aligned_particles --apply_transformation",
                "relion_image_handler --i ImageAnalysis/job999/aligned_particles.mrcs "
                "--o ImageAnalysis/job999/padded_particles.mrcs --new_box 128",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_calc_avg_ps.py')} --stack"
                " ImageAnalysis/job999/padded_particles.mrcs --outdir "
                "ImageAnalysis/job999/",
                "relion_display --i ImageAnalysis/job999/average_ps.mrc --pick --o "
                "ImageAnalysis/job999/average_ps_coords.star --scale 4 "
                "--particle_radius 4",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_find_res.py')} "
                "--ps_size 128 --apix 3.54 "
                "--coords ImageAnalysis/job999/average_ps_coords.star --outdir "
                "ImageAnalysis/job999/",
            ],
        )

    def test_get_command_no_picking(self):
        self.setup_parts_files()
        jf = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/no_pick_job.star"
        job_generate_commands_test(
            jobfile=str(jf),
            input_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.aligned"
            },
            output_nodes={
                "average_ps.mrc": f"{NODE_IMAGE2D}.mrc.power_spectrum.averaged"
            },
            expected_commands=[
                "relion_star_handler --i parts.star --o "
                "ImageAnalysis/job999/selected_class.star --select rlnClassNumber "
                "--minval 1 --maxval 1",
                "relion_stack_create --i ImageAnalysis/job999/selected_class.star --o "
                "ImageAnalysis/job999/aligned_particles --apply_transformation",
                "relion_image_handler --i ImageAnalysis/job999/aligned_particles.mrcs "
                "--o ImageAnalysis/job999/padded_particles.mrcs --new_box 128",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_calc_avg_ps.py')} --stack"
                " ImageAnalysis/job999/padded_particles.mrcs --outdir "
                "ImageAnalysis/job999/",
            ],
        )

    def test_get_command_with_pad_explicit_px_size(self):
        self.setup_parts_files()
        jf = (
            Path(self.test_data)
            / "JobFiles/AveragePowerSpectrum/with_explicit_px_job.star"
        )
        job_generate_commands_test(
            jobfile=str(jf),
            input_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.aligned"
            },
            output_nodes={
                "average_ps.mrc": f"{NODE_IMAGE2D}.mrc.power_spectrum.averaged",
                "selected_features.star": f"{NODE_IMAGE2DMETADATA}.star",
            },
            expected_commands=[
                "relion_star_handler --i parts.star --o "
                "ImageAnalysis/job999/selected_class.star --select rlnClassNumber "
                "--minval 1 --maxval 1",
                "relion_stack_create --i ImageAnalysis/job999/selected_class.star --o "
                "ImageAnalysis/job999/aligned_particles --apply_transformation",
                "relion_image_handler --i ImageAnalysis/job999/aligned_particles.mrcs "
                "--o ImageAnalysis/job999/padded_particles.mrcs --new_box 128",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_calc_avg_ps.py')} --stack"
                " ImageAnalysis/job999/padded_particles.mrcs --outdir "
                "ImageAnalysis/job999/",
                "relion_display --i ImageAnalysis/job999/average_ps.mrc --pick --o "
                "ImageAnalysis/job999/average_ps_coords.star --scale 4 "
                "--particle_radius 4",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_find_res.py')} "
                "--ps_size 128 --apix 1.25 "
                "--coords ImageAnalysis/job999/average_ps_coords.star --outdir "
                "ImageAnalysis/job999/",
            ],
        )

    def test_get_command_continue(self):
        self.setup_parts_files()
        jf = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/continue_job.star"
        job_generate_commands_test(
            jobfile=str(jf),
            input_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.aligned"
            },
            output_nodes={
                "average_ps.mrc": f"{NODE_IMAGE2D}.mrc.power_spectrum.averaged",
                "selected_features.star": f"{NODE_IMAGE2DMETADATA}.star",
            },
            expected_commands=[
                "relion_display --i ImageAnalysis/job999/average_ps.mrc --pick --o "
                "ImageAnalysis/job999/average_ps_coords.star --scale 4 "
                "--particle_radius 4",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('average_power_spectrum/aps_find_res.py')} "
                "--ps_size 128 --apix 3.54 "
                "--coords ImageAnalysis/job999/average_ps_coords.star --outdir "
                "ImageAnalysis/job999/",
            ],
        )

    def test_calc_res_script(self):
        odir = "ImageAnalysis/job999"
        coords = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/aps_parts.star"
        aps_find_res.main(
            [
                "--ps_size",
                "2000",
                "--apix",
                "1.2",
                "--coords",
                str(coords),
                "--outdir",
                odir,
            ]
        )

        outfile = str((Path(odir) / "selected_features.star"))

        wrote = DataStarFile(outfile).loop_as_list("coordinates")
        expected = [
            ["0", "0", "1.697"],
            ["1000", "2000", "2.400"],
            ["2000", "1000", "2.400"],
            ["2000", "2000", "1.697"],
            ["1200", "1000", "12.000"],
            ["1000", "1000", "inf"],
        ]
        assert wrote == expected

    def test_calc_res_script_no_input_file(self):
        odir = "ImageAnalysis/job999"
        aps_find_res.main(
            [
                "--ps_size",
                "2000",
                "--apix",
                "1.2",
                "--coords",
                "file_does_not_exist",
                "--outdir",
                odir,
            ]
        )

        outfile = str((Path(odir) / "selected_features.star"))

        wrote = DataStarFile(outfile).loop_as_list("coordinates")
        expected = []
        assert wrote == expected

    def test_make_avg_ps(self):
        parts_file = Path(self.test_data) / "image_stack.mrcs"
        aps_calc_avg_ps.main(
            ["--stack", str(parts_file), "--outdir", "ImageAnalysis/job999/"]
        )
        with mrcfile.open("ImageAnalysis/job999/average_ps.mrc") as aps:
            assert aps.data.shape == (64, 64)

    def test_prepare_cleanup(self):
        job = new_job_of_type("pipeliner.image_analysis.average_particle_ps")
        job.output_dir = "ImageAnalysis/job999/"
        wrote = job.prepare_clean_up_lists(do_harsh=False)
        assert wrote == ([], [])
        wrote = job.prepare_clean_up_lists(do_harsh=True)
        assert wrote == (
            [
                "ImageAnalysis/job999/selected_class.star",
                "ImageAnalysis/job999/aligned_particles.mrcs",
                "ImageAnalysis/job999/padded_particles.mrcs",
            ],
            [],
        )

    def test_results_display(self):
        os.makedirs("ImageAnalysis/job999")
        td = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/"
        shutil.copy(
            Path(td) / "sel_features.star",
            "ImageAnalysis/job999/selected_features.star",
        )
        shutil.copy(
            Path(self.test_data) / "single.mrc",
            "ImageAnalysis/job999/average_ps.mrc",
        )
        job = new_job_of_type("pipeliner.image_analysis.average_particle_ps")
        job.output_dir = "ImageAnalysis/job999/"
        rdo = job.create_results_display()
        exp_dobj = [
            {
                "title": "Marked features",
                "start_collapsed": False,
                "dobj_type": "table",
                "flag": "",
                "header_tooltips": ["CoordinateX", "CoordinateY", "Resolution"],
                "headers": ["CoordinateX", "CoordinateY", "Resolution"],
                "table_data": [
                    ["0", "0", "inf"],
                    ["10", "20", "45"],
                    ["20", "10", "42"],
                ],
                "associated_data": [
                    "ImageAnalysis/job999/average_ps.mrc",
                    "ImageAnalysis/job999/selected_features.star",
                ],
            },
            {
                "title": "Selected power spectrum features",
                "start_collapsed": False,
                "dobj_type": "image",
                "flag": "",
                "image_path": "ImageAnalysis/job999/Thumbnails/picked_coords000.png",
                "image_desc": "ImageAnalysis/job999/average_ps.mrc: 3 particles",
                "associated_data": [
                    "ImageAnalysis/job999/average_ps.mrc",
                    "ImageAnalysis/job999/selected_features.star",
                ],
            },
        ]
        assert rdo[0].__dict__ == exp_dobj[0]
        assert rdo[1].__dict__ == exp_dobj[1]

    def test_results_display_no_picking(self):
        os.makedirs("ImageAnalysis/job999")
        shutil.copy(
            Path(self.test_data) / "single.mrc",
            "ImageAnalysis/job999/average_ps.mrc",
        )
        job = new_job_of_type("pipeliner.image_analysis.average_particle_ps")
        job.output_dir = "ImageAnalysis/job999/"
        job.joboptions["do_picking"].value = False
        rdo = job.create_results_display()
        exp_dobj = {
            "title": "Averaged particle power spectra",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "No features were selected for analysis",
            "image_path": "ImageAnalysis/job999/Thumbnails/average_ps.png",
            "image_desc": "",
            "associated_data": ["ImageAnalysis/job999/average_ps.mrc"],
        }
        assert rdo[0].__dict__ == exp_dobj

    def test_results_display_picking_but_no_picks(self):
        os.makedirs("ImageAnalysis/job999")
        td = Path(self.test_data) / "JobFiles/AveragePowerSpectrum/"
        shutil.copy(
            Path(td) / "sel_features_empty.star",
            "ImageAnalysis/job999/selected_features.star",
        )
        shutil.copy(
            Path(self.test_data) / "single.mrc",
            "ImageAnalysis/job999/average_ps.mrc",
        )
        job = new_job_of_type("pipeliner.image_analysis.average_particle_ps")
        job.output_dir = "ImageAnalysis/job999/"
        rdo = job.create_results_display()
        exp_dobj = {
            "title": "Averaged particle power spectra",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "No features were selected for analysis",
            "image_path": "ImageAnalysis/job999/Thumbnails/picked_coords000.png",
            "image_desc": "ImageAnalysis/job999/average_ps.mrc: 0 particles",
            "associated_data": [
                "ImageAnalysis/job999/average_ps.mrc",
                "ImageAnalysis/job999/selected_features.star",
            ],
        }
        assert rdo[0].__dict__ == exp_dobj
