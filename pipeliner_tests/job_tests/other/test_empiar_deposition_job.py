#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import shlex
import unittest
import os
import shutil
import tempfile
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner.utils import get_job_script, touch, get_python_command
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.nodes import NODE_PROCESSDATA, NODE_IMAGE2D
from pipeliner.scripts.job_scripts.deposition import make_empiar_submission_script
from pipeliner import __version__ as pipevers


class EMPIARDepositionTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner-empiar-depo")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_authors_and_pi_entered(self):
        jf = os.path.join(
            self.test_data,
            "JobFiles/EmpiarDeposition/empiar_deposition_job.star",
        )
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={"mythumb.png": f"{NODE_IMAGE2D}.png"},
            output_nodes={
                "deposition_data_EMPIAR.json": f"{NODE_PROCESSDATA}."
                "json.deposition_data.empiar",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_deposition.py')} "
                "--terminal_job PostProcess/job030/ "
                "--jobstar_file Deposition/job999/job.star --movies "
                "--mics --parts --corr_parts",
                "cp mythumb.png Deposition/job999/mythumb.png",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_submission_script.py')} "
                "--jobstar_file Deposition/job999/job.star",
            ],
        )

    def test_get_commands_citation_file(self):
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/EmpiarDeposition/citation.json"),
            "citation.json",
        )
        jf = os.path.join(
            self.test_data,
            "JobFiles/EmpiarDeposition/citation_file_job.star",
        )
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={
                "mythumb.png": f"{NODE_IMAGE2D}.png",
                "citation.json": f"{NODE_PROCESSDATA}.json.citation",
            },
            output_nodes={
                "deposition_data_EMPIAR.json": f"{NODE_PROCESSDATA}."
                "json.deposition_data.empiar",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_deposition.py')} "
                "--terminal_job PostProcess/job030/ "
                "--jobstar_file Deposition/job999/job.star --movies "
                "--mics --parts --corr_parts",
                "cp mythumb.png Deposition/job999/mythumb.png",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_submission_script.py')} "
                "--jobstar_file Deposition/job999/job.star",
            ],
        )

    def test_get_commands_authors_file(self):
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/EmpiarDeposition/authors_file.csv"),
            "authors_file.csv",
        )
        jf = os.path.join(
            self.test_data,
            "JobFiles/EmpiarDeposition/authors_file_job.star",
        )
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={
                "mythumb.png": f"{NODE_IMAGE2D}.png",
                "authors_file.csv": f"{NODE_PROCESSDATA}.csv.authorlist",
            },
            output_nodes={
                "deposition_data_EMPIAR.json": f"{NODE_PROCESSDATA}."
                "json.deposition_data.empiar",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_deposition.py')} "
                "--terminal_job PostProcess/job030/ "
                "--jobstar_file Deposition/job999/job.star --movies "
                "--mics --parts --corr_parts",
                "cp mythumb.png Deposition/job999/mythumb.png",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_submission_script.py')} "
                "--jobstar_file Deposition/job999/job.star",
            ],
        )

    def test_get_commands_minimal_info(self):
        jf = os.path.join(
            self.test_data,
            "JobFiles/EmpiarDeposition/minimal_job.star",
        )
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={},
            output_nodes={
                "deposition_data_EMPIAR.json": f"{NODE_PROCESSDATA}."
                "json.deposition_data.empiar",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_deposition.py')} "
                "--terminal_job PostProcess/job030/ "
                "--jobstar_file Deposition/job999/job.star --movies "
                "--mics --parts --corr_parts",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('deposition/make_empiar_submission_script.py')} "
                "--jobstar_file Deposition/job999/job.star",
            ],
        )

    @patch("shutil.which")
    def test_make_submission_script(self, mock_which):
        mock_which.return_value = "this/is/a/path"
        touch(os.path.join(self.test_dir, "default_pipeline.star"))
        jobstar = os.path.join(
            self.test_data, "JobFiles/EmpiarDeposition/empiar_deposition_job.star"
        )
        os.makedirs("Deposition/job999")
        shutil.copy(jobstar, "Deposition/job999/job.star")
        make_empiar_submission_script.main(
            ["--jobstar_file", "Deposition/job999/job.star"]
        )
        assert os.path.isfile("Deposition/job999/.result_text.txt")
        with open("Deposition/job999/.result_text.txt") as res_text:
            wrote_res = res_text.readlines()
        assert wrote_res[:-1] == [
            "The deposition is ready.  Review the data below, and when satisfied begin "
            "the deposition by running:\n",
            "\tDeposition/job999/start_empiar_deposition.py\n",
            "From the project directory: \n",
        ]
        assert self.test_dir in wrote_res[-1]
        assert os.path.isfile("Deposition/job999/start_empiar_deposition.py")
        with open("Deposition/job999/start_empiar_deposition.py") as script:
            wrote_script = script.readlines()
        assert wrote_script[:4] == [
            "#/usr/bin/env python3\n",
            "# EMPIAR deposition script\n",
            f"# Written by CCPEM-pipeliner vers {pipevers}\n",
            "# This script should be run from the project dir:\n",
        ]
        assert self.test_dir in wrote_script[4]
        assert wrote_script[5:] == [
            "tmp_env = os.environ.copy()\n",
            "tmp_env['EMPIAR_TRANSFER_PASS'] = 'EXpassword'\n",
            "command = ['empiar-depositor', '-a', 'this/is/a/path', '-g', "
            "'this/is/a/path', '-e', 'Deposition/job999/mythumb.png', '123abc', "
            "'Deposition/job999/deposition_data_EMPIAR.json', "
            "'Deposition/job999/deposition_data']\n",
            "subprocess.Popen(command, env=tmp_env)",
        ]

    @patch("shutil.which")
    def test_make_submission_script_fail_missing_programs(self, mock_which):
        mock_which.return_value = None
        touch(os.path.join(self.test_dir, "default_pipeline.star"))
        jobstar = os.path.join(
            self.test_data, "JobFiles/EmpiarDeposition/empiar_deposition_job.star"
        )
        os.makedirs("Deposition/job999")
        shutil.copy(jobstar, "Deposition/job999/job.star")
        make_empiar_submission_script.main(
            ["--jobstar_file", "Deposition/job999/job.star"]
        )
        assert os.path.isfile("Deposition/job999/.result_text.txt")
        with open("Deposition/job999/.result_text.txt") as res_text:
            wrote_res = res_text.readlines()
        assert wrote_res == [
            "The deposition data and file have been prepared and can be reviewed below "
            "but an automated deposition cannot be made for the following reasons:\n",
            "----\n",
            "empiar-depositor not found:  This script is used to make the deposition. "
            "It must be available in the system $PATH and can be installed with 'pip "
            "install empiar-depositor'\n",
            "----\n",
            "Globus or ascp (Aspera) not found: One of these two programs is necessary "
            "for data transfer to the EMPIAR servers.  \n",
            "Download the ascp tool from: https://www.ibm.com/aspera/connect/\n",
            "or install globus with 'pip install globus-cli==1.7.0'",
        ]
        assert not os.path.isfile("Deposition/job999/start_empiar_deposition.py")

    @patch("shutil.which")
    def test_make_submission_script_fail_missing_kew_and_pw(self, mock_which):
        mock_which.return_value = "this/is/a/path"
        touch(os.path.join(self.test_dir, "default_pipeline.star"))
        jobstar = os.path.join(
            self.test_data, "JobFiles/EmpiarDeposition/empiar_deposition_nokey_job.star"
        )
        os.makedirs("Deposition/job999")
        shutil.copy(jobstar, "Deposition/job999/job.star")
        make_empiar_submission_script.main(
            ["--jobstar_file", "Deposition/job999/job.star"]
        )
        assert os.path.isfile("Deposition/job999/.result_text.txt")
        with open("Deposition/job999/.result_text.txt") as res_text:
            wrote_res = res_text.readlines()
        assert wrote_res == [
            "The deposition data and file have been prepared and can be reviewed below "
            "but an automated deposition cannot be made for the following reasons:\n",
            "----\n",
            "No EMPIAR transfer password provided: The password is provided by EMPIAR "
            "when the deposition is arranged and gives access to upload data to the "
            "EMPIAR servers\n",
            "----\n",
            "No EMPIAR API token was provided: This token is provided by EMPIAR when a "
            "deposition is arranged. Alternatively your EMPIAR username can be used, "
            "but either way the deposition must be arranged beforehand with EMPIAR",
        ]

        assert not os.path.isfile("Deposition/job999/start_empiar_deposition.py")


if __name__ == "__main__":
    unittest.main()
