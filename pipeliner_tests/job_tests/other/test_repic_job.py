#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path

from pipeliner.utils import get_job_script, touch, get_python_command
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.nodes import NODE_MICROGRAPHCOORDSGROUP
from pipeliner.scripts.job_scripts.repic.repic_setup import (
    main as repic_main,
    get_parts_files,
)


class RepicTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/REPIC"
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_parts(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "repic_job.star"),
            input_nodes={
                "AutoPick/job001/autopick.star": NODE_MICROGRAPHCOORDSGROUP + ".star",
                "AutoPick/job002/autopick.star": NODE_MICROGRAPHCOORDSGROUP + ".star",
                "AutoPick/job003/autopick.star": NODE_MICROGRAPHCOORDSGROUP + ".star",
            },
            output_nodes={
                "consensus_picks.star": NODE_MICROGRAPHCOORDSGROUP + ".star.consensus"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('repic/repic_setup.py')} --outdir REPIC/job999/ "
                "--autopick_files AutoPick/job001/autopick.star "
                "AutoPick/job002/autopick.star AutoPick/job003/autopick.star",
                "repic get_cliques REPIC/job999/ REPIC/job999/cliques 128",
                "repic run_ilp REPIC/job999/cliques 180",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('repic/repic_setup.py')} --write_summary --outdir "
                f"REPIC/job999/ --autopick_files AutoPick/job001/autopick.star",
            ],
            show_inputnodes=True,
        )

    def test_repic_script_making_files_subfunction(self):
        ap_file = Path(self.test_data) / "autopick.star"
        os.makedirs("AutoPick/job006")
        shutil.copy(ap_file, "AutoPick/job006/autopick.star")
        os.makedirs("AutoPick/job006/Movies/")
        for n in range(1, 5):
            shutil.copy(
                Path(self.test_data) / "coords.star",
                f"AutoPick/job006/Movies/micrograph00{n}_autopick.star",
            )
        get_parts_files(
            autopick_file="AutoPick/job006/autopick.star", outdir="REPIC/job999"
        )
        assert os.path.isdir("REPIC/job999/from_job006")
        with open(Path(self.test_data) / "coords.box") as ebox:
            expected_box = [x.strip() for x in ebox.readlines()]
        for n in range(1, 5):
            f = f"REPIC/job999/from_job006/micrograph00{n}.box"
            assert os.path.isfile(f)
            with open(f) as rf:
                assert [x.strip() for x in rf.readlines()] == expected_box

    def test_repic_script_making_files_main(self):
        ap1 = Path(self.test_data) / "autopick.star"
        ap2 = Path(self.test_data) / "autopick2.star"

        for n in (6, 7):
            os.makedirs(f"AutoPick/job00{n}")
            os.makedirs(f"AutoPick/job00{n}/Movies/")
            for m in range(1, 5):
                shutil.copy(
                    Path(self.test_data) / "coords.star",
                    f"AutoPick/job00{n}/Movies/micrograph00{m}_autopick.star",
                )
        shutil.copy(ap1, "AutoPick/job006/autopick.star")
        shutil.copy(ap2, "AutoPick/job007/autopick.star")
        repic_main(
            [
                "--outdir",
                "REPIC/job999",
                "--autopick_files",
                "AutoPick/job006/autopick.star",
                "AutoPick/job007/autopick.star",
            ]
        )
        with open(Path(self.test_data) / "coords.box") as ebox:
            expected_box = [x.strip() for x in ebox.readlines()]
        for job in (6, 7):
            assert os.path.isdir(f"REPIC/job999/from_job00{job}")
            for n in range(1, 5):
                f = f"REPIC/job999/from_job00{job}/micrograph00{n}.box"
                assert os.path.isfile(f)
                with open(f) as rf:
                    assert [x.strip() for x in rf.readlines()] == expected_box

    def test_repic_script_summary_file(self):
        ap_file = Path(self.test_data) / "autopick.star"
        os.makedirs("AutoPick/job006")
        shutil.copy(ap_file, "AutoPick/job006/autopick.star")
        os.makedirs("REPIC/job999/cliques")

        for n in range(1, 5):
            touch(f"REPIC/job999/cliques/micrograph00{n}.box")
        repic_main(
            [
                "--write_summary",
                "--outdir",
                "REPIC/job999",
                "--autopick_files",
                "AutoPick/job006/autopick.star",
            ]
        )
        assert os.path.isfile("REPIC/job999/consensus_picks.star")
        with open("REPIC/job999/consensus_picks.star") as cf:
            print(cf.readlines())
        expected = [
            "data_coordinate_files\n",
            "loop_\n",
            "_rlnMicrographName\n",
            "_rlnMicrographCoordinates\n",
            "MotionCorr/job002/micrograph001.mrc "
            "REPIC/job999/cliques/micrograph001.box\n",
            "MotionCorr/job002/micrograph002.mrc "
            "REPIC/job999/cliques/micrograph002.box\n",
            "MotionCorr/job002/micrograph003.mrc "
            "REPIC/job999/cliques/micrograph003.box\n",
            "MotionCorr/job002/micrograph004.mrc "
            "REPIC/job999/cliques/micrograph004.box\n",
        ]

        with open("REPIC/job999/consensus_picks.star") as actual:
            assert actual.readlines() == expected
