#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import contextlib
import io
import textwrap
import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path
import mrcfile

from pipeliner_tests import test_data
from pipeliner.utils import get_job_script, get_python_command
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
    job_display_object_generation_test,
)
from pipeliner.scripts.job_scripts.pipeliner_select import (
    clav_stack_star,
    make_particles_star,
    write_centered_clavs_starfile,
)
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
    Image2DGroupMetadataNode,
    DensityMapGroupMetadataNode,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMontage,
    ResultsDisplayMapModel,
)
from pipeliner.job_factory import new_job_of_type


class PipelinerClass2DSelectTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="select2d_")
        self.jobfiles = Path(self.test_data) / "JobFiles/PipelinerSelect"
        self.pycom = shlex.join(get_python_command())
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_2d_opt(self, dirno: int):
        cl2ddir = Path(f"Class2D/job{dirno:03d}")
        cl2ddir.mkdir(parents=True)
        opt_n = {6: 25, 13: 100}
        shutil.copy(
            Path(self.test_data) / "optimiser_duplicate_entries.star",
            cl2ddir / f"run_it{opt_n[dirno]:03d}_optimiser.star",
        )

    def test_2d_get_command_clavs_recenter(self):
        self.setup_2d_opt(dirno=13)
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select2d_classes_recenter_job.star"),
            input_nodes={
                "Class2D/job013/run_it100_optimiser.star": NODE_OPTIMISERDATA + ".star."
                "relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                f"{self.pycom} "
                f"{get_job_script('pipeliner_select/clav_stack_star.py')} "
                "--optimiser_file Class2D/job013/run_it100_optimiser.star --classes "
                "1 3 6 19 27 --outdir Select/job999/",
                "relion_image_handler --i Select/job999/class_averages.mrcs "
                "--shift_com true --o Select/job999/class_averages_centered.mrcs",
                f"{self.pycom} "
                f"{get_job_script('pipeliner_select/write_centered_clavs_starfile.py')}"
                " --optimiser_file Class2D/job013/run_it100_optimiser.star --classes 1 "
                "3 6 19 27 --outdir Select/job999/",
                f"{self.pycom} "
                f"{get_job_script('pipeliner_select/make_particles_star.py')} "
                "--data_file Class2D/job013/run_it100_data.star --classes 1 3 6 19 27 "
                "--outdir Select/job999/",
            ],
        )

    def test_3d_get_ignore_recenter(self):
        cl3ddir = Path("Class3D/job016")
        cl3ddir.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/PipelinerSelect/cl3d_optimiser.star",
            cl3ddir / "run_it025_optimiser.star",
        )
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select3d_all_job.star"),
            input_nodes={
                "Class3D/job016/run_it025_optimiser.star": NODE_OPTIMISERDATA + ".star."
                "relion",
            },
            output_nodes={
                "particles.star": NODE_PARTICLEGROUPMETADATA + ".star.relion",
            },
            expected_commands=[
                f"{self.pycom} "
                f"{get_job_script('pipeliner_select/clav_stack_star.py')} "
                "--optimiser_file Class3D/job016/run_it025_optimiser.star --classes "
                "1 2  --outdir Select/job999/",
                f"{self.pycom} "
                f"{get_job_script('pipeliner_select/make_particles_star.py')} "
                "--data_file Class3D/job016/run_it025_data.star --classes 1 2 "
                "--outdir Select/job999/",
            ],
        )

    def test_make_postrun_clavs_output_node_2d_no_center(self):
        self.setup_2d_opt(dirno=13)
        job = new_job_of_type("pipeliner.select.classes")
        job.joboptions["fn_optimiser"].value = "Class2D/job013/run_it100_optimiser.star"
        job.create_post_run_output_nodes()
        exp_node = Image2DGroupMetadataNode(
            name="class_averages.star",
            kwds=["relion", "class_averages", "masked"],
        )
        assert job.output_nodes[0].name == exp_node.name
        assert job.output_nodes[0].toplevel_type == exp_node.toplevel_type
        assert job.output_nodes[0].kwds == exp_node.kwds

    def test_make_postrun_clavs_output_node_2d_with_center(self):
        self.setup_2d_opt(dirno=13)
        job = new_job_of_type("pipeliner.select.classes")
        job.joboptions["fn_optimiser"].value = "Class2D/job013/run_it100_optimiser.star"
        job.joboptions["do_recenter"].value = "Yes"
        job.create_post_run_output_nodes()
        exp_node = Image2DGroupMetadataNode(
            name="class_averages.star",
            kwds=["relion", "class_averages", "masked"],
        )
        exp_node_cent = Image2DGroupMetadataNode(
            name="class_averages_centered.star",
            kwds=["relion", "class_averages", "masked", "centered"],
        )
        print(job.output_nodes)
        assert job.output_nodes[0].name == exp_node.name
        assert job.output_nodes[0].toplevel_type == exp_node.toplevel_type
        assert job.output_nodes[0].kwds == exp_node.kwds
        assert job.output_nodes[1].name == exp_node_cent.name
        assert job.output_nodes[1].toplevel_type == exp_node_cent.toplevel_type
        assert job.output_nodes[1].kwds == exp_node_cent.kwds

    def test_make_postrun_clavs_output_node_3d(self):
        cl3ddir = Path("Class3D/job016")
        cl3ddir.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/PipelinerSelect/cl3d_optimiser.star",
            cl3ddir / "run_it025_optimiser.star",
        )
        job = new_job_of_type("pipeliner.select.classes")
        job.joboptions["fn_optimiser"].value = "Class3D/job016/run_it025_optimiser.star"
        job.create_post_run_output_nodes()
        exp_node = DensityMapGroupMetadataNode(
            name="selected_classes.star",
            kwds=["relion", "3d_classes"],
        )
        assert job.output_nodes[0].name == exp_node.name
        assert job.output_nodes[0].toplevel_type == exp_node.toplevel_type
        assert job.output_nodes[0].kwds == exp_node.kwds

    def test_make_particles_script_2d(self):
        get_relion_tutorial_data(relion_version=5, dirs="Class2D")
        em = Path("Class2D/job008")
        output = Path("Select/job001")
        output.mkdir(parents=True)
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            make_particles_star.main(
                [
                    "--data_file",
                    str(em / "run_it025_data.star"),
                    "--outdir",
                    str(output),
                    "--classes",
                    "25",
                    "44",
                ]
            )
        with open(self.jobfiles / "wrote_particles_2d.star") as exp:
            expected = [x.strip() for x in exp.readlines()]
        with open(output / "particles.star") as act:
            wrote = [x.strip() for x in act.readlines()]
        assert wrote == expected
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == textwrap.dedent(
            """\
                Class 25: found 239 particles
                Class 44: found 67 particles
                Selected 306 particles from 2 classes
            """
        )

    def test_make_particles_script_3d(self):
        get_relion_tutorial_data(relion_version=5, dirs="Class3D")
        em = Path("Class3D/job016")
        output = Path("Select/job001")
        output.mkdir(parents=True)
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            make_particles_star.main(
                [
                    "--data_file",
                    str(em / "run_it025_data.star"),
                    "--outdir",
                    str(output),
                    "--classes",
                    "1",
                    "3",
                ]
            )
        with open(self.jobfiles / "wrote_particles_3d.star") as exp:
            expected = [x.strip() for x in exp.readlines()]
        with open(output / "particles.star") as act:
            wrote = [x.strip() for x in act.readlines()]
        assert wrote == expected
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == textwrap.dedent(
            """\
                Class 1: found 1 particles
                Class 3: found 145 particles
                Selected 146 particles from 2 classes
            """
        )

    def test_make_clav_stacks_script_2d(self):
        get_relion_tutorial_data(relion_version=5, dirs="Class2D")
        em = Path("Class2D/job008")
        output = Path("Select/job001")
        output.mkdir(parents=True)
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            clav_stack_star.main(
                [
                    "--optimiser_file",
                    str(em / "run_it025_optimiser.star"),
                    "--outdir",
                    str(output),
                    "--classes",
                    "1",
                    "2",
                    "5",
                    "10",
                ]
            )

        with open("Select/job001/class_averages.star") as written:
            wrote = [x.strip() for x in written.readlines()]
        with open(self.jobfiles / "class_averages_2d.star") as expected:
            exp = [x.strip() for x in expected.readlines()]
        assert wrote == exp
        made_files = list(output.glob("*"))
        for f in ["class_averages.star", "class_averages.mrcs"]:
            ff = output / f
            assert ff in made_files
        wrote_stack = mrcfile.mmap(output / "class_averages.mrcs")
        assert wrote_stack.data.shape[0] == 4
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == textwrap.dedent(
            """\
                Created new substack Select/job001/class_averages.mrcs with 4 frames from Class2D/job008/run_it025_classes.mrcs
                Selected 4 of 4 classes in Class2D/job008/run_it025_model.star
                Wrote Select/job001/class_averages.star with 4 total classes
            """  # noqa: E501
        )

    def test_make_clav_stacks_script_3d(self):
        get_relion_tutorial_data(relion_version=5, dirs="Class3D")
        em = Path("Class3D/job016")
        output = Path("Select/job001")
        output.mkdir(parents=True)
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            clav_stack_star.main(
                [
                    "--optimiser_file",
                    str(em / "run_it025_optimiser.star"),
                    "--outdir",
                    str(output),
                    "--classes",
                    "1",
                    "2",
                    "5",
                    "10",
                ]
            )

        with open(output / "selected_classes.star") as written:
            wrote = [x.strip() for x in written.readlines()]
        with open(self.jobfiles / "selected_classes.star") as expected:
            exp = [x.strip() for x in expected.readlines()]
        assert wrote == exp
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == textwrap.dedent(
            """\
                Selected 2 of 4 classes in Class3D/job016/run_it025_model.star
                Warning! The following classes were not found: 10, 5
                The output file will only contain these classes: 1, 2
                Wrote Select/job001/selected_classes.star with 2 total classes
            """
        )

    def test_results_display_2d(self):
        exp_parts = ResultsDisplayMontage(
            title="Select/job999/particles.star; 20/306 images",
            xvalues=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            yvalues=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            labels=[
                "000002@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000012@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000013@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000016@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000020@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000021@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000028@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000030@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000035@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000038@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000048@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000085@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000111@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000114@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000118@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000140@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000148@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000150@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000155@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                "000169@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
            ],
            associated_data=["Select/job999/particles.star"],
            img="Select/job999/Thumbnails/montage_s000.png",
        )
        expclavs = ResultsDisplayMontage(
            title="Select/job999/class_averages.star; 4/4 images",
            xvalues=[0, 1, 2, 3],
            yvalues=[0, 0, 0, 0],
            labels=[
                "000001@Class2D/job008/run_it025_classes.mrcs",
                "000002@Class2D/job008/run_it025_classes.mrcs",
                "000005@Class2D/job008/run_it025_classes.mrcs",
                "000010@Class2D/job008/run_it025_classes.mrcs",
            ],
            associated_data=["Select/job999/class_averages.star"],
            img="Select/job999/Thumbnails/montage_s001.png",
        )

        recentered = ResultsDisplayMontage(
            title="Select/job999/class_averages_centered.star; 5/5 images",
            xvalues=[0, 1, 2, 3, 4],
            yvalues=[0, 0, 0, 0, 0],
            labels=[
                "000001@Select/job001/class_averages_centered.mrcs",
                "000002@Select/job001/class_averages_centered.mrcs",
                "000003@Select/job001/class_averages_centered.mrcs",
                "000004@Select/job001/class_averages_centered.mrcs",
                "000005@Select/job001/class_averages_centered.mrcs",
            ],
            associated_data=["Select/job999/class_averages_centered.star"],
            img="Select/job999/Thumbnails/montage_s002.png",
        )

        get_relion_tutorial_data(relion_version=5, dirs=["Extract", "Class2D"])
        seldir = Path("Select/job001")
        seldir.mkdir(parents=True)
        shutil.copy(
            "Class2D/job008/run_it025_classes.mrcs",
            seldir / "class_averages_centered.mrcs",
        )
        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "select2d_classes_recenter_job.star"),
            expected_display_objects=[exp_parts, expclavs, recentered],
            files_to_create={
                "particles.star": str(self.jobfiles / "wrote_particles_2d.star"),
                "class_averages.star": str(self.jobfiles / "class_averages_2d.star"),
                "class_averages_centered.star": str(
                    self.jobfiles / "recentered_clavs.star"
                ),
            },
        )

    def test_results_display_3d(self):
        exp_parts = ResultsDisplayMontage(
            title="Select/job999/particles.star; 20/146 images",
            xvalues=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            yvalues=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            labels=[
                "000055@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "000092@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "000136@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "000183@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "000220@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "000095@Extract/job012/Movies/20170629_00022_frameImage.mrcs",
                "000157@Extract/job012/Movies/20170629_00022_frameImage.mrcs",
                "000170@Extract/job012/Movies/20170629_00022_frameImage.mrcs",
                "000182@Extract/job012/Movies/20170629_00022_frameImage.mrcs",
                "000241@Extract/job012/Movies/20170629_00022_frameImage.mrcs",
                "000145@Extract/job012/Movies/20170629_00023_frameImage.mrcs",
                "000177@Extract/job012/Movies/20170629_00023_frameImage.mrcs",
                "000180@Extract/job012/Movies/20170629_00023_frameImage.mrcs",
                "000217@Extract/job012/Movies/20170629_00023_frameImage.mrcs",
                "000235@Extract/job012/Movies/20170629_00023_frameImage.mrcs",
                "000095@Extract/job012/Movies/20170629_00024_frameImage.mrcs",
                "000130@Extract/job012/Movies/20170629_00024_frameImage.mrcs",
                "000141@Extract/job012/Movies/20170629_00024_frameImage.mrcs",
                "000151@Extract/job012/Movies/20170629_00024_frameImage.mrcs",
                "000206@Extract/job012/Movies/20170629_00024_frameImage.mrcs",
            ],
            associated_data=["Select/job999/particles.star"],
            img="Select/job999/Thumbnails/montage_s000.png",
        )
        mm1 = ResultsDisplayMapModel(
            title="3D viewer: Selected class: Class3D/job016/run_it025_class001.mrc",
            maps=["Class3D/job016/run_it025_class001.mrc"],
            associated_data=["Class3D/job016/run_it025_class001.mrc"],
        )
        mm2 = ResultsDisplayMapModel(
            title="3D viewer: Selected class: Class3D/job016/run_it025_class002.mrc",
            maps=["Class3D/job016/run_it025_class002.mrc"],
            associated_data=["Class3D/job016/run_it025_class002.mrc"],
        )
        get_relion_tutorial_data(relion_version=5, dirs=["Extract", "Class3D"])
        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "select3d_all_job.star"),
            expected_display_objects=[exp_parts, mm1, mm2],
            files_to_create={
                "particles.star": str(self.jobfiles / "wrote_particles_3d.star"),
                "selected_classes.star": str(self.jobfiles / "selected_classes.star"),
            },
        )

    def test_write_centered_clavs_starfile(self):
        seldir = Path("Select/job001")
        seldir.mkdir(parents=True)
        shutil.copy(self.jobfiles / "run.out", seldir / "run.out")
        get_relion_tutorial_data(relion_version=5, dirs="Class2D")
        write_centered_clavs_starfile.main(
            [
                "--optimiser_file",
                "Class2D/job008/run_it025_optimiser.star",
                "--classes",
                "1",
                "10",
                "15",
                "20",
                "25",
                "--outdir",
                "Select/job001",
            ]
        )
        os.system("cat Select/job001/class_averages_centered.star")
        with open("Select/job001/class_averages_centered.star") as wrote:
            actual = [x.strip() for x in wrote.readlines()]
        with open(self.jobfiles / "recentered_clavs.star") as exp:
            expected = [x.strip() for x in exp.readlines()]
        assert actual == expected


if __name__ == "__main__":
    unittest.main()
