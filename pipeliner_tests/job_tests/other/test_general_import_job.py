#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.utils import touch
from pipeliner.node_factory import create_node
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_IMAGE2DSTACK,
    NODE_IMAGE2D,
    NODE_ATOMCOORDS,
)


class GeneralImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Import"
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_standard_node(self):
        touch("test_image.mrc")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "pipeliner_import_single_file_job.star"),
            input_nodes={},
            output_nodes={"test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty"},
            expected_commands=["cp test_image.mrc Import/job999/test_image.mrc"],
        )

    def test_get_command_batch_standard_node(self):
        touch("test_image.mrc")
        touch("test_image2.mrc")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "pipeliner_import_batch_files_job.star"),
            input_nodes={},
            output_nodes={
                "test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
                "test_image2.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
            },
            expected_commands=[
                "cp test_image.mrc Import/job999/test_image.mrc",
                "cp test_image2.mrc Import/job999/test_image2.mrc",
            ],
        )

    def test_get_command_standard_node_file_in_DoppioUploads(self):
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/test_image.mrc")
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "pipeliner_import_single_file_usrfiles_job.star"
            ),
            input_nodes={},
            output_nodes={"test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty"},
            expected_commands=[
                "mv Import/job999/InputFiles/test_image.mrc"
                " Import/job999/test_image.mrc"
            ],
        )

    def test_get_command_standard_node_file_in_DoppioUploads_batch(self):
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/test_image.mrc")
        touch("DoppioUploads/test_image2.mrc")
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "pipeliner_import_batch_files_doppio_uploads_job.star"
            ),
            input_nodes={},
            output_nodes={
                "test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
                "test_image2.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
            },
            expected_commands=[
                "mv Import/job999/InputFiles/test_image.mrc "
                "Import/job999/test_image.mrc",
                "mv Import/job999/InputFiles/test_image2.mrc "
                "Import/job999/test_image2.mrc",
            ],
        )

    def test_get_command_standard_node_no_keywords(self):
        touch("test_image.mrc")
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "pipeliner_import_single_file_no_keywords_job.star"
            ),
            input_nodes={},
            output_nodes={"test_image.mrc": f"{NODE_DENSITYMAP}.mrc"},
            expected_commands=["cp test_image.mrc Import/job999/test_image.mrc"],
        )

    def test_get_command_standard_node_synthetic(self):
        touch("test_image.mrc")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "pipeliner_import_single_file_sim_job.star"),
            input_nodes={},
            output_nodes={
                "test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty.synthetic"
            },
            expected_commands=["cp test_image.mrc Import/job999/test_image.mrc"],
        )

    def test_get_command_standard_node_bad_characters(self):
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "pipeliner_import_single_file_man_node_job.star"
            ),
            input_nodes={},
            output_nodes={"test_image.mrc": "NewNode.mrc.test.fancy.pretty"},
            expected_commands=["cp test_image.mrc Import/job999/test_image.mrc"],
        )

    def test_run_job_standard_node(self):
        touch("test_image.mrc")
        job_running_test(
            test_jobfile=str(self.jobfiles / "pipeliner_import_single_file_job.star"),
            input_files=[],
            expected_outfiles=["test_image.mrc"],
        )

    def test_run_job_manual_node_name(self):
        touch("test_image.mrc")
        job_running_test(
            test_jobfile=str(
                self.jobfiles / "pipeliner_import_single_file_man_node_job.star"
            ),
            input_files=[],
            expected_outfiles=["test_image.mrc"],
        )

    def test_run_job_manual_node_name_bad_chars(self):
        touch("test_image.mrc")
        with self.assertRaises(ValueError):
            job_running_test(
                test_jobfile=str(
                    self.jobfiles / "pipeliner_import_single_file_bad_chars_job.star"
                ),
                input_files=[],
                expected_outfiles=["test_image.mrc"],
            )

    def setup_results_test(
        self, results_file, nodetype, nodekwds=None, is_relion=False
    ):
        nodekwds = [] if not nodekwds else nodekwds
        outdir = "Import/job999/"
        os.makedirs(outdir)
        shutil.copy(results_file, outdir)
        thejob = new_job_of_type("relion.import")
        thejob.joboptions["pipeliner_node_type"].value = nodetype
        thejob.joboptions["kwds"].value = nodekwds
        thejob.joboptions["is_relion"].value = "Yes" if is_relion else "No"
        thejob.output_dir = outdir
        thejob.output_nodes.append(
            create_node(
                os.path.join("Import/job999", os.path.basename(results_file)),
                nodetype,
                nodekwds,
            )
        )
        return thejob.create_results_display()

    def test_display_image2d_mrc(self):
        rfile = os.path.join(self.test_data, "single.mrc")
        results = self.setup_results_test(rfile, NODE_IMAGE2D)
        exp = {
            "title": "Import/job999/single.mrc",
            "dobj_type": "image",
            "image_path": "Import/job999/Thumbnails/single.png",
            "image_desc": "Import/job999/single.mrc",
            "associated_data": ["Import/job999/single.mrc"],
            "start_collapsed": False,
            "flag": "",
        }
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].image_path)

    def test_display_2stack_mrcs(self):
        rfile = os.path.join(self.test_data, "image_stack.mrcs")
        results = self.setup_results_test(rfile, NODE_IMAGE2DSTACK)
        with open(
            os.path.join(self.test_data, "ResultsFiles/genimport_2dstack_mrcs.json")
        ) as exps:
            exp = json.load(exps)
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].img)

    def test_display_2stack_tiff(self):
        rfile = os.path.join(self.test_data, "movie_tiff.tiff")
        results = self.setup_results_test(rfile, NODE_IMAGE2DSTACK)
        with open(
            os.path.join(self.test_data, "ResultsFiles/genimport_2dstack_tiff.json")
        ) as exps:
            exp = json.load(exps)
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].img)

    def test_display_2stack_other(self):
        rfile = os.path.join(self.test_data, "logfile.log")
        results = self.setup_results_test(rfile, NODE_IMAGE2DSTACK)
        exp = {
            "title": "Error creating results display",
            "dobj_type": "text",
            "display_data": "Illegal file type for mini_montage_from_stack(): .log",
            "associated_data": ["Import/job999/logfile.log"],
            "start_collapsed": False,
            "flag": "",
        }

        assert results[0].__dict__ == exp, results[0].__dict__

    def test_display_mapmodel_map(self):
        rfile = os.path.join(self.test_data, "emd_3488.mrc")
        results = self.setup_results_test(rfile, "DensityMap", ["masked", "emdb"])
        exp = [
            {
                "title": "Map slices: Import/job999/emd_3488.mrc",
                "start_collapsed": False,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0, 1, 2],
                "yvalues": [0, 0, 0],
                "labels": [
                    "Import/job999/emd_3488.mrc: xy",
                    "Import/job999/emd_3488.mrc: xz",
                    "Import/job999/emd_3488.mrc: yz",
                ],
                "associated_data": ["Import/job999/emd_3488.mrc"],
                "img": "Import/job999/Thumbnails/slices_montage_000.png",
            },
            {
                "title": "3D viewer: Import/job999/emd_3488.mrc",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": ["Import/job999/emd_3488.mrc"],
                "maps_opacity": [1.0],
                "maps_colours": [],
                "models_colours": [],
                "models": [],
                "maps_data": "Import/job999/emd_3488.mrc",
                "models_data": "",
                "associated_data": ["Import/job999/emd_3488.mrc"],
            },
        ]

        assert [x.__dict__ for x in results] == exp

    def test_display_mapmodel_model(self):
        rfile = os.path.join(self.test_data, "5me2_a.pdb")
        results = self.setup_results_test(rfile, NODE_ATOMCOORDS, ["refined"])
        exp = {
            "maps": [],
            "dobj_type": "mapmodel",
            "maps_opacity": [],
            "models": ["Import/job999/5me2_a.pdb"],
            "title": "3D viewer: Import/job999/5me2_a.pdb",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["Import/job999/5me2_a.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].models[0])

    def test_display_nodisplay_text(self):
        rfile = os.path.join(self.test_data, "micrographs_ctf.star")
        shutil.copy(rfile, "micrographs_ctf.xxx")
        results = self.setup_results_test(rfile, NODE_MICROGRAPHGROUPMETADATA, ["ctf"])
        exp = {
            "title": "Import/job999/micrographs_ctf.star",
            "start_collapsed": False,
            "dobj_type": "textfile",
            "flag": "",
            "file_path": "Import/job999/micrographs_ctf.star",
        }
        assert results[0].__dict__ == exp, results[0].__dict__


if __name__ == "__main__":
    unittest.main()
