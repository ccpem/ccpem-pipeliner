#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pipeliner_tests import test_data
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_DENSITYMAP
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
)
from pipeliner.utils import get_python_command, get_job_script


class TEMPyReFFTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="tempy_reff")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPy_REFF/flexfit_refine.job"
            ),
            input_nodes={
                "Import/job001/5me2_chain_a.cif": f"{NODE_ATOMCOORDS}.cif",
                "Import/job002/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                os.path.join(
                    "flex_fit_out", "final.pdb"
                ): f"{NODE_ATOMCOORDS}.pdb.tempy_reff.ribfind_fit",
                os.path.join(
                    "gmm_out", "final_gmm.cif"
                ): f"{NODE_ATOMCOORDS}.cif.tempy_reff.gmm_fit",
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())} "
                    f"{get_job_script('ribfind/process_input_model.py')} "
                    "-p ../../Import/job001/5me2_chain_a.cif "
                    "-mid 5me2_chain_a"
                ),
                "mkdssp 5me2_chain_a_no_wat_lig.pdb ribfind.dssp",
                (
                    "ribfind --model 5me2_chain_a_no_wat_lig.pdb --dssp ribfind.dssp "
                    "--output-dir ribfind_5me2_chain_a"
                ),
                (
                    "tempy-reff --model 5me2_chain_a_no_wat_lig.pdb --map "
                    "../../Import/job002/emd_3488.mrc --resolution 3.2 "
                    "--convergence-type variance --convergence-threshold 1e-06 "
                    "--convergence-timeout 100 "
                    "--platform-cuda --fitting-density --fitting-density-k 100.0 "
                    "--restrain-ribfind --restrain-ribfind-dir "
                    "ribfind_5me2_chain_a/protein --output-dir flex_fit_out"
                ),
                (
                    "tempy-reff --model flex_fit_out/final.pdb --map "
                    "../../Import/job002/emd_3488.mrc --resolution 3.2 "
                    "--convergence-type patience --convergence-patience 5 "
                    "--convergence-timeout 100 "
                    "--platform-cuda "
                    "--fitting-gmm --fitting-gmm-k 50000.0 --output-dir gmm_out "
                ),
                "gemmi convert --remove-h gmm_out/final.pdb gmm_out/final_gmm.cif",
                "TEMPy.smoc -m ../../Import/job002/emd_3488.mrc --models "
                "5me2_chain_a_no_wat_lig.pdb flex_fit_out/final.pdb "
                "gmm_out/final_gmm.cif -r 3.2 --smoc-window 5 --output-format json",
            ],
        )

    def test_use_ribfind_and_refine(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPy_REFF/flexfit_refine.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_chain_a.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(
                        self.test_data,
                        "emd_3488.mrc",
                    ),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "ribfind.dssp",
                "flex_fit_out/final.cif",
                "gmm_out/final_gmm.cif",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert len(dispobjs) == 2


if __name__ == "__main__":
    unittest.main()
