#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from unittest.mock import patch
from pipeliner.utils import get_python_command
from pipeliner.job_factory import active_job_from_proc, new_job_of_type
from pipeliner.utils import get_job_script
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.data_structure import (
    NODE_EVALUATIONMETRIC,
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
)


class EMDBValidateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="emdb-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_validation_task(self):
        self.proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
            ],
            expected_outfiles=[
                "emd_3488.mrc_all.json",
                "emd_3488.mrc_raps.json",
                "emd_3488.mrc_density_distribution.json",
                "emd_3488.mrc_mmfsc.json",
                "run.out",
                "run.err",
            ],
        )
        assert os.path.isfile("EMDB_validation/job998/emd_3488_map_parameters.json")
        assert os.path.isfile("EMDB_validation/job998/emd_3488.mrc_xprojection.tif")
        dispobjs = active_job_from_proc(self.proc).create_results_display()
        assert dispobjs[1].__dict__["title"] == "Orthogonal projections"

    @patch("os.getcwd")
    def test_get_command_emdbvalidation(self, mock_getcwd):
        mock_getcwd.return_value = "/project_dir"
        coms = [
            "cp ../../Import/job001/emd_3488.mrc .",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('get_map_parameters.py')} -m "
            "../../Import/job001/emd_3488.mrc -odir .",
            "cp ../../Import/job002/5ni1_updated.cif .",
            "va -m emd_3488.mrc -met singleparticle -d "
            "/project_dir/EMDB_validation/job999/ -f 5ni1_updated.cif -i True -s 3.2 "
            "-cl 0.081",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('emdb_validation/convert_emdb_validation_images.py')} "
            "--map emd_3488.mrc --outdir . --model 5ni1_updated.cif",
        ]

        out_nodes = {
            "emd_3488.mrc_all.json": f"{NODE_EVALUATIONMETRIC}.json.emdb_va."
            "all_results"
        }
        out_nodes["emd_3488.mrc_raps.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.raps"
        )
        out_nodes["emd_3488.mrc_density_distribution.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.mapdistribution"
        )
        out_nodes["emd_3488.mrc_mmfsc.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va." "mapmodelfsc"
        )
        out_nodes["emd_3488.mrc_atom_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.atominclusion"
        )
        out_nodes["emd_3488.mrc_residue_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.residueinclusion"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes=out_nodes,
            expected_commands=coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch("os.getcwd")
    def test_get_command_emdbvalidation_with_halfmaps(self, mock_getcwd):
        mock_getcwd.return_value = "/project_dir"
        coms = [
            "cp ../../Import/job001/emd_3488.mrc .",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('get_map_parameters.py')} -m "
            "../../Import/job001/emd_3488.mrc -odir .",
            "cp ../../Import/job002/5ni1_updated.cif .",
            "cp ../../Import/job001/hm1.mrc .",
            "cp ../../Import/job001/hm2.mrc .",
            "va -m emd_3488.mrc -met singleparticle -d "
            "/project_dir/EMDB_validation/job999/ -f "
            "5ni1_updated.cif -i True -hmeven hm1.mrc -hmodd hm2.mrc -s 3.2 -cl 0.081",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('emdb_validation/convert_emdb_validation_images.py')} "
            "--map emd_3488.mrc --outdir . --model 5ni1_updated.cif --hm1 hm1.mrc "
            "--hm2 hm2.mrc",
        ]

        out_nodes = {
            "emd_3488.mrc_all.json": f"{NODE_EVALUATIONMETRIC}.json.emdb_va.all_results"
        }
        out_nodes["emd_3488.mrc_raps.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.raps"
        )
        out_nodes["emd_3488.mrc_density_distribution.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.mapdistribution"
        )
        out_nodes["emd_3488.mrc_mmfsc.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va." "mapmodelfsc"
        )
        out_nodes["emd_3488.mrc_atom_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.atominclusion"
        )
        out_nodes["emd_3488.mrc_residue_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.residueinclusion"
        )
        out_nodes["emd_3488.mrc_fsc.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.halfmapfsc"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate_halfmap.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
                "Import/job001/hm1.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job001/hm2.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
            },
            output_nodes=out_nodes,
            expected_commands=coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch("os.getcwd")
    def test_get_command_emdbvalidation_with_halfmaps_pdb(self, mock_getcwd):
        mock_getcwd.return_value = "/project_dir"
        coms = [
            "cp ../../Import/job001/emd_3488.mrc .",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('get_map_parameters.py')} -m "
            "../../Import/job001/emd_3488.mrc -odir .",
            "gemmi convert --shorten ../../Import/job002/5me2_a.pdb 5me2_a.cif",
            "cp ../../Import/job001/hm1.mrc .",
            "cp ../../Import/job001/hm2.mrc .",
            "va -m emd_3488.mrc -met singleparticle -d "
            "/project_dir/EMDB_validation/job999/ -f "
            "5me2_a.cif -i True -hmeven hm1.mrc -hmodd hm2.mrc -s 3.2 -cl 0.081",
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('emdb_validation/convert_emdb_validation_images.py')} "
            "--map emd_3488.mrc --outdir . --model 5me2_a.cif --hm1 hm1.mrc "
            "--hm2 hm2.mrc",
        ]

        out_nodes = {
            "emd_3488.mrc_all.json": f"{NODE_EVALUATIONMETRIC}.json.emdb_va.all_results"
        }
        out_nodes["emd_3488.mrc_raps.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.raps"
        )
        out_nodes["emd_3488.mrc_density_distribution.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.mapdistribution"
        )
        out_nodes["emd_3488.mrc_mmfsc.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va." "mapmodelfsc"
        )
        out_nodes["emd_3488.mrc_atom_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.atominclusion"
        )
        out_nodes["emd_3488.mrc_residue_inclusion.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.residueinclusion"
        )
        out_nodes["emd_3488.mrc_fsc.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.emdb_va.halfmapfsc"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate_halfmap_pdb.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job001/hm1.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job001/hm2.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
            },
            output_nodes=out_nodes,
            expected_commands=coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch("pipeliner.pipeliner_job.ExternalProgram.get_version")
    def test_job_marked_unavailale_if_wrong_version_found(self, mockvers):
        mockvers.return_value = "va: 0.0.1.dev01"
        job = new_job_of_type("emdb_validation_analysis.map_model_validation")
        assert job.jobinfo.alternative_unavailable_reason == (
            "EMDB Validation version va: 0.0.1.dev01 was found.\n"
            "The version must be >= 0.0.1.dev43 and =< 0.0.1.dev55"
        )
        mockvers.return_value = "va: 0.0.1.dev56"
        job = new_job_of_type("emdb_validation_analysis.map_model_validation")
        assert job.jobinfo.alternative_unavailable_reason == (
            "EMDB Validation version va: 0.0.1.dev56 was found.\n"
            "The version must be >= 0.0.1.dev43 and =< 0.0.1.dev55"
        )
        mockvers.return_value = "lalalaladodododo"
        job = new_job_of_type("emdb_validation_analysis.map_model_validation")
        assert job.jobinfo.alternative_unavailable_reason == (
            "Could not find version of EMDB validation"
        )


if __name__ == "__main__":
    unittest.main()
