#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.scripts.job_scripts.model_validation import (
    get_checkmyseq_results,
)
from pipeliner.utils import get_python_command
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_SEQUENCE,
    NODE_LOGFILE,
)


class CheckMySeqTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="checkmysequence")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_checkmysequence_task(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/CheckMySequence/checkseq.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                ("Import/job003", os.path.join(self.test_data, "5ni1_mod.fasta")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        assert len(dispobjs) == 5

    def test_get_command_modelvalidation(self):
        exp_out_nodes = {}
        exp_coms = []
        # checkmysequence
        exp_coms += [
            "checkmysequence --mapin ../../Import/job002/emd_3488.mrc"
            " --modelin ../../Import/job001/5me2_a.pdb --seqin "
            "../../Import/job003/5ni1_mod.fasta --jsonout 5me2_a_emd_3488checkseq.json",
            f"{shlex.join(get_python_command())} {get_checkmyseq_results.__file__} "
            "-j 5me2_a_emd_3488checkseq.json -id 5me2_a_emd_3488",
        ]
        exp_out_nodes["5me2_a_emd_3488checkseq.json"] = (
            f"{NODE_LOGFILE}.json.checkmysequence.json_out"
        )
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/CheckMySequence/checkseq.job"
            ),
            input_nodes={
                "Import/job002/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )


if __name__ == "__main__":
    unittest.main()
