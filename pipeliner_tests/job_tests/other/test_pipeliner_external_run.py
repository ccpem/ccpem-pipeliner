#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test, job_running_test
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA
from pipeliner.job_factory import new_job_of_type


class PipelinerRunExternalProgram(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/PipelinerExternal"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_no_inputs_or_outputs(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "no_inputs_or_outputs_job.star"),
            input_nodes={},
            output_nodes={},
            expected_commands=[
                "run command1 --arg A --arg2",
                "run command2 --arg B --arg2",
            ],
        )

    def test_mics_inputs(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "mics_inputs_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "MotionCorr/job002/corrected_micrographs."
                "star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={},
            expected_commands=[
                "run command1 --arg ../../CtfFind/job003/micrographs_ctf.star --arg2",
                "run command2 --arg ../../MotionCorr/job002/corrected_micrographs.star "
                "--arg2",
            ],
        )

    def test_mics_inputs_single_output(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "mics_inputs_output_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "MotionCorr/job002/corrected_micrographs."
                "star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.kwd1.kwd2"
            },
            expected_commands=[
                "run command1 --arg ../../CtfFind/job003/micrographs_ctf.star --arg2",
                "run command2 --arg ../../MotionCorr/job002/corrected_micrographs.star "
                "--arg2",
            ],
        )

    def test_mics_inputs_multiple_outputs(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "mics_inputs_multi_output_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "MotionCorr/job002/corrected_micrographs."
                "star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.kwd1.kwd2",
                "micrographs2.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.kwd1.kwd2",
            },
            expected_commands=[
                "run command1 --arg ../../CtfFind/job003/micrographs_ctf.star --arg2",
                "run command2 --arg ../../MotionCorr/job002/corrected_micrographs.star "
                "--arg2",
            ],
        )

    def test_live_run(self):
        ctf = Path("CtfFind/job003")
        mocor = Path("MotionCorr/job002")
        for jd in [ctf, mocor]:
            jd.mkdir(parents=True)
        ctffile = ctf / "micrographs_ctf.star"
        ctffile.touch()
        mocorfile = mocor / "corrected_micrographs.star"
        mocorfile.touch()

        job_running_test(
            test_jobfile=str(self.jobfiles / "live_job.star"),
            input_files=[],
            expected_outfiles=[],
        )
        assert Path("External/job998/test.star").is_file()

    def test_index_error(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "index_error_job.star"),
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    def test_bad_key_error(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "bad_key_job.star"),
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    def test_redirect_output_in_coms(self):
        job = new_job_of_type("pipeliner.run_external_program")
        job.joboptions["commands"].value = "cat file > otherfile"
        val = job.additional_joboption_validation()
        assert len(val) == 1
        assert val[0].message == (
            "'<' and '>' cannot be used to redirect inputs and outputs for commands in"
            " this job"
        )
        assert val[0].type == "warning"
        assert len(val[0].raised_by) == 1
        assert val[0].raised_by[0].label == "Commands to be executed"

    def test_redirect_input_in_coms(self):
        job = new_job_of_type("pipeliner.run_external_program")
        job.joboptions["commands"].value = "echo < filedata"
        val = job.additional_joboption_validation()
        assert len(val) == 1
        assert val[0].message == (
            "'<' and '>' cannot be used to redirect inputs and outputs for commands in"
            " this job"
        )
        assert val[0].type == "warning"
        assert len(val[0].raised_by) == 1
        assert val[0].raised_by[0].label == "Commands to be executed"


if __name__ == "__main__":
    unittest.main()
