#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from gemmi import cif
import shlex
from glob import glob
from pathlib import Path

from pipeliner.utils import touch, get_job_script, get_python_command
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.job_factory import new_job_of_type
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_PARAMSDATA,
)
from pipeliner.scripts.job_scripts.cryolo import (
    make_cryolo_output_file,
    make_cryolo_links_dir,
)

make_files_script = get_job_script("cryolo/make_cryolo_links_dir.py")
output_script = get_job_script("cryolo/make_cryolo_output_file.py")


class CrYOLOTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/CrYOLO"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def set_up_micrographs_file(self, output_dir):
        """Make the micrographs file that the cryolo plugin needs to read for
        doing comparisons at command 3"""
        os.makedirs(os.path.join(output_dir, "Micrographs"))
        os.makedirs("CtfFind/job003")
        infile = os.path.join(self.test_data, "JobFiles/CrYOLO/micrographs_ctf.star")
        file_path = "CtfFind/job003/micrographs_ctf.star"
        shutil.copy(infile, file_path)
        return file_path

    def test_get_command_crYOLO_plugin_with_file_input(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "crYOLO_file_input_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.cryolo.autopick"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star "
                "--cryolo_job_dir AutoPick/job999/",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0 1 -o ."
                " -t 0.3",
                f"{shlex.join(get_python_command())} {output_script} -i "
                "../../CtfFind/job003/micrographs_ctf.star -o AutoPick/job999/",
            ],
        )

    def test_get_command_crYOLO_plugin_with_config_file(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "crYOLO_with_config_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "AutoPick/job999/InputFiles/my_cryolo_config.json": f"{NODE_PARAMSDATA}"
                ".json",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.cryolo.autopick",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star "
                "--cryolo_job_dir AutoPick/job999/",
                "cryolo_predict.py -c InputFiles/my_cryolo_config.json -w"
                " model.h5 -i Micrographs/ -o ."
                " -t 0.3 -nc 2",
                f"{shlex.join(get_python_command())} {output_script} -i "
                "../../CtfFind/job003/micrographs_ctf.star -o AutoPick/job999/",
            ],
        )

    def test_get_command_crYOLO_plugin_with_file_input_continue(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        os.makedirs("AutoPick/job999")
        apfile = os.path.join(self.test_data, "JobFiles/CrYOLO/autopick.star")
        shutil.copy(apfile, "AutoPick/job999/autopick.star")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "crYOLO_continue_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.cryolo.autopick"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star "
                "--cryolo_job_dir AutoPick/job999/ --is_continue",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0 1 -o ."
                " -t 0.3",
                f"{shlex.join(get_python_command())} {output_script} -i "
                "../../CtfFind/job003/micrographs_ctf.star -o AutoPick/job999/",
            ],
        )

    def test_get_command_CrYOLO_plugin_with_file_input_use_JANNI(self):
        """Test using a starfile as in input, using JANNI instead of LOWPASS"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "crYOLO_file_JANNI_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.cryolo.autopick"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star "
                "--cryolo_job_dir AutoPick/job999/",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter JANNI --janni_model path/to/JANNI.h5",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0 1 2 3 -o ."
                " -t 0.3",
                f"{shlex.join(get_python_command())} {output_script} -i "
                "../../CtfFind/job003/micrographs_ctf.star -o AutoPick/job999/",
            ],
        )

    def test_counting_and_comparing_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly"""
        self.set_up_micrographs_file("AutoPick/job003/")
        os.makedirs("STAR")
        for n in range(21, 50):
            f = os.path.join("STAR", f"20170629_{n:05d}_frameImage.star")
            touch(f)
        make_cryolo_output_file.main(
            ["-i", "CtfFind/job003/micrographs_ctf.star", "-o", "AutoPick/job003/"]
        )

        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 45):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 45):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with one missing"""

        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "STAR"
        os.makedirs(stardir)
        for n in range(21, 44):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        make_cryolo_output_file.main(
            ["-i", "CtfFind/job003/micrographs_ctf.star", "-o", "AutoPick/job003/"]
        )
        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 44):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 44):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning_multi(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with multiple
        missing"""
        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "STAR"
        os.makedirs(stardir)
        for n in range(21, 36):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        make_cryolo_output_file.main(
            ["-i", "CtfFind/job003/micrographs_ctf.star", "-o", "AutoPick/job003/"]
        )
        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 36):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 36):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_fail_if_no_part_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Fail if no Particle files were written"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_dir = "AutoPick/job003/"
        yolojob.working_dir = yolojob.output_dir
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        with self.assertRaisesRegex(
            ValueError, "No particle coordinate files were written"
        ):
            make_cryolo_output_file.main(
                ["-i", "CtfFind/job003/micrographs_ctf.star", "-o", "AutoPick/job003/"]
            )

    def test_link_making_script(self):
        os.makedirs("AutoPick/job003")
        os.makedirs("MotionCorr/job002/")
        mics_file = "MotionCorr/job002/corrected_micrographs.star"
        shutil.copy(
            Path(self.test_data) / "corrected_micrographs.star",
            "MotionCorr/job002",
        )
        os.chdir("AutoPick/job003")
        make_cryolo_links_dir.main(
            [
                "--input_mics_file",
                f"../../{mics_file}",
                "--cryolo_job_dir",
                "AutoPick/job003/",
            ]
        )

        assert Path("Micrographs").is_dir()
        mics_links = glob("Micrographs/*frameImage.mrc")
        assert len(mics_links) == 24
        for mlink in mics_links:
            assert Path(mlink).is_symlink()

    def test_link_making_script_continue(self):
        os.makedirs("AutoPick/job003")
        os.makedirs("Autopick/job003/Micrographs")
        existing_files = []
        for n in range(4):
            fname = f"Autopick/job003/Micrographs/existing{n}.mrc"
            existing_files.append(fname)
            Path(fname).touch()

        os.makedirs("MotionCorr/job002/")
        mics_file = "MotionCorr/job002/corrected_micrographs.star"
        shutil.copy(
            Path(self.test_data) / "corrected_micrographs.star",
            "MotionCorr/job002",
        )
        shutil.copy(
            Path(self.test_data) / "autopick_cryolo.star",
            "AutoPick/job003/autopick.star",
        )
        os.chdir("AutoPick/job003")
        make_cryolo_links_dir.main(
            [
                "--input_mics_file",
                f"../../{mics_file}",
                "--cryolo_job_dir",
                "AutoPick/job003/",
                "--is_continue",
            ]
        )

        assert Path("Micrographs").is_dir()
        mics_links = glob("Micrographs/*frameImage.mrc")
        assert len(mics_links) == 20
        for mlink in mics_links:
            assert Path(mlink).is_symlink()
        for ef in existing_files:
            assert not Path(ef).is_file()


if __name__ == "__main__":
    unittest.main()
