#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import shlex
import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_IMAGE2DSTACK, NODE_LOGFILE
from pipeliner.utils import get_job_script, get_python_command


class CryoVaeTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.jobfiles = Path(self.test_data) / "JobFiles/CryoDANN"
        self.test_dir = tempfile.mkdtemp(prefix="cryodann_test")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_star_input_no_vae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_star_no_vae_job.star"),
            input_nodes={"job008.star": NODE_PARTICLEGROUPMETADATA + ".star.relion"},
            output_nodes={
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
                "lightning_logs/good_particles_t0.006.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryodann.good_particles",
            },
            expected_commands=[
                "relion_stack_create --i job008.star --o "
                "CryoDANN/job999/aligned_stacks --apply_transformation",
                "cryodann rel_training_data CryoDANN/job999/aligned_stacks.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 3 --gamma 0.01 "
                "--lr 0.001 --batch_size 128 --particle_file job008.star",
            ],
        )

    def test_get_commands_star_input_vae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_star_vae_job.star"),
            input_nodes={"job008.star": NODE_PARTICLEGROUPMETADATA + ".star.relion"},
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
                "lightning_logs/good_particles_t0.006.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryodann.good_particles",
            },
            expected_commands=[
                "relion_stack_create --i job008.star --o "
                "CryoDANN/job999/aligned_stacks --apply_transformation",
                "relion_image_handler --i CryoDANN/job999/aligned_stacks.mrcs "
                "--lowpass 10 --o CryoDANN/job999/lp_filtered_stack.mrcs",
                "cryovae CryoDANN/job999/lp_filtered_stack.mrcs CryoDANN/job999/ "
                "--max_epochs 2 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} "
                "--stackfile CryoDANN/job999/recons.mrcs --outdir CryoDANN/job999/ "
                "--starfile job008.star",
                "cryodann rel_training_data CryoDANN/job999/lp_filtered_stack.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 2 --gamma 0.01 "
                "--lr 0.001 --batch_size 128 --particle_file job008.star",
            ],
        )

    def test_get_commands_stack_input_vae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_stack_vae_job.star"),
            input_nodes={"job008_lp10.mrcs": NODE_IMAGE2DSTACK + ".mrc"},
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
            },
            expected_commands=[
                "relion_image_handler --i job008_lp10.mrcs "
                "--lowpass 10 --o CryoDANN/job999/lp_filtered_stack.mrcs",
                "cryovae CryoDANN/job999/lp_filtered_stack.mrcs CryoDANN/job999/ "
                "--max_epochs 2 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} "
                "--stackfile CryoDANN/job999/recons.mrcs --outdir CryoDANN/job999/ ",
                "cryodann rel_training_data CryoDANN/job999/lp_filtered_stack.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 2 --gamma 0.01 "
                "--lr 0.001 --batch_size 128",
            ],
        )

    def test_get_commands_stack_input_novae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_stack_no_vae_job.star"),
            input_nodes={"job008_lp10.mrcs": NODE_IMAGE2DSTACK + ".mrc"},
            output_nodes={
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
            },
            expected_commands=[
                "cryodann rel_training_data job008_lp10.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 2 --gamma 0.01 "
                "--lr 0.001 --batch_size 128",
            ],
        )

    def test_get_commands_stack_star_input_novae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_stack_star_no_vae_job.star"),
            input_nodes={
                "job008_lp10.mrcs": NODE_IMAGE2DSTACK + ".mrc",
                "job008.star": NODE_PARTICLEGROUPMETADATA + ".star.relion",
            },
            output_nodes={
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
                "lightning_logs/good_particles_t0.006.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryodann.good_particles",
            },
            expected_commands=[
                "cryodann rel_training_data job008_lp10.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 2 --gamma 0.01 "
                "--lr 0.001 --batch_size 128 --particle_file job008.star",
            ],
        )

    def test_get_commands_stack_star_input_vae(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryodann_stack_star_vae_job.star"),
            input_nodes={
                "job008_lp10.mrcs": NODE_IMAGE2DSTACK + ".mrc",
                "job008.star": NODE_PARTICLEGROUPMETADATA + ".star.relion",
            },
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
                "lightning_logs/index_confidence.txt": NODE_LOGFILE
                + ".txt.cryodann.scores",
                "lightning_logs/good_particles_t0.006.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryodann.good_particles",
            },
            expected_commands=[
                "relion_image_handler --i job008_lp10.mrcs "
                "--lowpass 10 --o CryoDANN/job999/lp_filtered_stack.mrcs",
                "cryovae CryoDANN/job999/lp_filtered_stack.mrcs CryoDANN/job999/ "
                "--max_epochs 2 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} "
                "--stackfile CryoDANN/job999/recons.mrcs --outdir CryoDANN/job999/ "
                "--starfile job008.star",
                "cryodann rel_training_data CryoDANN/job999/lp_filtered_stack.mrcs "
                "CryoDANN/job999/ --keep_percent 0.006 --max_epochs 2 --gamma 0.01 "
                "--lr 0.001 --batch_size 128 --particle_file job008.star",
            ],
        )
