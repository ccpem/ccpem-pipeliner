#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.utils import get_job_script


class TEMPyMapFitTests(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="tempy-scores")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_smoc_results(self):
        smoc_out = os.path.join(self.test_data, "smoc_score.json")
        residue_coordinates = os.path.join(
            self.test_data, "5me2_a_residue_coordinates.json"
        )
        run_subprocess(
            [
                get_python_command()[0],
                get_job_script("model_validation/get_smoc_results.py"),
                "-smoc",
                smoc_out,
                "-coord",
                residue_coordinates,
                "-id",
                "5me2_a_emd_3488",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5me2_a_emd_3488_residue_smoc.json")
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5me2_a_emd_3488_residue_smoc.json")
            ).st_size,
            20120,
            rel_tol=0.05,
        )

    def test_get_command_mask(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        contour = 1e-6
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        maskfile = "../../Import/job003/emd_3488_mask.mrc"
        apply_mask_script = get_job_script("model_validation/apply_map_mask.py")
        residue_coord_script = get_job_script(
            "model_validation/get_residue_coordinates.py"
        )
        smoc_results_script = get_job_script("model_validation/get_smoc_results.py")
        # tempy glob scores
        exp_coms += [
            f"{shlex.join(get_python_command())} "
            f"{apply_mask_script} -m {input_map} -ma {maskfile} --ignore_edge"
        ]
        masked_map = os.path.splitext(os.path.basename(input_map))[0] + "_masked.mrc"
        output_csv_prefix = "globscores_{}_vs_{}".format(
            "5ni1_updated",
            "emd_3488",
        )
        exp_coms += [
            "TEMPy.scores -m "
            f"{masked_map} -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            f"{output_csv_prefix} --output-format csv"
        ]
        exp_out_nodes[f"{output_csv_prefix}.csv"] = (
            "EvaluationMetric.csv.tempy.global_scores"
        )
        # smoc
        output_smoc_prefix = "5ni1_updated_emd_3488_smoc_score"
        exp_coms += [
            f"TEMPy.smoc -m {input_map} "
            f"-p {input_model} -r 3.2 --smoc-window 1 "
            f"--output-format json --output-prefix {output_smoc_prefix}"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {residue_coord_script} "
            f"-p {input_model} -mid 5ni1_updated"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {smoc_results_script} "
            f"-smoc {output_smoc_prefix}.json -coord "
            "5ni1_updated_residue_coordinates.json -id 5ni1_updated_emd_3488",
        ]
        exp_out_nodes[f"{output_smoc_prefix}.json"] = (
            "EvaluationMetric.json.tempy.smoc_scores"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_mask.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job003/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_contour(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        contour = 0.08
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        residue_coord_script = get_job_script(
            "model_validation/get_residue_coordinates.py"
        )
        smoc_results_script = get_job_script("model_validation/get_smoc_results.py")
        output_csv_prefix = "globscores_{}_vs_{}".format(
            "5ni1_updated",
            "emd_3488",
        )
        # tempy glob scores
        exp_coms += [
            "TEMPy.scores -m "
            f"{input_map} -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            f"{output_csv_prefix} --output-format csv"
        ]
        exp_out_nodes[f"{output_csv_prefix}.csv"] = (
            "EvaluationMetric.csv.tempy.global_scores"
        )
        # smoc
        output_smoc_prefix = "5ni1_updated_emd_3488_smoc_score"
        exp_coms += [
            f"TEMPy.smoc -m {input_map} "
            f"-p {input_model} -r 3.2 --smoc-window 1 "
            f"--output-format json --output-prefix {output_smoc_prefix}"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {residue_coord_script} "
            f"-p {input_model} -mid 5ni1_updated"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {smoc_results_script} "
            f"-smoc {output_smoc_prefix}.json -coord "
            "5ni1_updated_residue_coordinates.json -id 5ni1_updated_emd_3488",
        ]
        exp_out_nodes[f"{output_smoc_prefix}.json"] = (
            "EvaluationMetric.json.tempy.smoc_scores"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_contour.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_mask_multi(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        contour = 1e-6
        input_model_list = [
            "../../Import/job001/5ni1_updated.cif",
            "../../Import/job002/5me2_a.pdb",
        ]
        input_map = "../../Import/job003/emd_3488.mrc"
        maskfile = "../../Import/job004/emd_3488_mask.mrc"
        apply_mask_script = get_job_script("model_validation/apply_map_mask.py")
        residue_coord_script = get_job_script(
            "model_validation/get_residue_coordinates.py"
        )
        smoc_results_script = get_job_script("model_validation/get_smoc_results.py")
        # tempy glob scores
        exp_coms += [
            f"{shlex.join(get_python_command())} "
            f"{apply_mask_script} -m {input_map} -ma {maskfile} --ignore_edge"
        ]
        masked_map = os.path.splitext(os.path.basename(input_map))[0] + "_masked.mrc"
        for input_model in input_model_list:
            modelid = os.path.splitext(os.path.basename(input_model))[0]
            output_csv_prefix = "globscores_{}_vs_{}".format(
                modelid,
                "emd_3488",
            )
            exp_coms += [
                "TEMPy.scores -m "
                f"{masked_map} -p {input_model} "
                f"-r 3.2 --contour-level {contour} --output-prefix "
                f"{output_csv_prefix} --output-format csv"
            ]
            exp_out_nodes[f"{output_csv_prefix}.csv"] = (
                "EvaluationMetric.csv.tempy.global_scores"
            )
            # smoc
            output_smoc_prefix = modelid + "_emd_3488_smoc_score"
            exp_coms += [
                f"TEMPy.smoc -m {input_map} "
                f"-p {input_model} -r 3.2 --smoc-window 1 "
                f"--output-format json --output-prefix {output_smoc_prefix}"
            ]
            exp_coms += [
                f"{shlex.join(get_python_command())} {residue_coord_script} "
                f"-p {input_model} -mid {modelid}"
            ]
            exp_coms += [
                f"{shlex.join(get_python_command())} {smoc_results_script} "
                f"-smoc {output_smoc_prefix}.json -coord "
                f"{modelid}_residue_coordinates.json -id {modelid}_emd_3488",
            ]
            exp_out_nodes[f"{output_smoc_prefix}.json"] = (
                "EvaluationMetric.json.tempy.smoc_scores"
            )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_mask_multi.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job002/5me2_a.pdb": "AtomCoords.pdb",
                "Import/job003/emd_3488.mrc": "DensityMap.mrc",
                "Import/job004/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_smoconly(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        residue_coord_script = get_job_script(
            "model_validation/get_residue_coordinates.py"
        )
        smoc_results_script = get_job_script("model_validation/get_smoc_results.py")
        # smoc
        output_smoc_prefix = "5ni1_updated_emd_3488_smoc_score"
        exp_coms += [
            f"TEMPy.smoc -m {input_map} "
            f"-p {input_model} -r 3.2 --smoc-window 1 "
            f"--output-format json --output-prefix {output_smoc_prefix}"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {residue_coord_script} "
            f"-p {input_model} -mid 5ni1_updated"
        ]
        exp_coms += [
            f"{shlex.join(get_python_command())} {smoc_results_script} "
            f"-smoc {output_smoc_prefix}.json -coord "
            "5ni1_updated_residue_coordinates.json -id 5ni1_updated_emd_3488",
        ]
        exp_out_nodes[f"{output_smoc_prefix}.json"] = (
            "EvaluationMetric.json.tempy.smoc_scores"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_mask_smoconly.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job003/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_mask_scoresonly(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []

        contour = 1e-6
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        maskfile = "../../Import/job003/emd_3488_mask.mrc"
        apply_mask_script = get_job_script("model_validation/apply_map_mask.py")
        # tempy glob scores
        exp_coms += [
            f"{shlex.join(get_python_command())} "
            f"{apply_mask_script} -m {input_map} -ma {maskfile} --ignore_edge"
        ]
        masked_map = os.path.splitext(os.path.basename(input_map))[0] + "_masked.mrc"
        output_csv_prefix = "globscores_{}_vs_{}".format(
            "5ni1_updated",
            "emd_3488",
        )
        exp_coms += [
            "TEMPy.scores -m "
            f"{masked_map} -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            f"{output_csv_prefix} --output-format csv"
        ]
        exp_out_nodes[f"{output_csv_prefix}.csv"] = (
            "EvaluationMetric.csv.tempy.global_scores"
        )
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_mask_scoresonly.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job003/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_mask_multi_run(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/TEMPyScores/all_scores_mask_multi.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "5ni1_updated_emd_3488_smoc_score.json",
                "5me2_a_emd_3488_smoc_score.json",
                "globscores_{}_vs_{}.csv".format("5ni1_updated", "emd_3488"),
                "globscores_{}_vs_{}.csv".format("5me2_a", "emd_3488"),
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # fit score table, smoc plot * 4 chains
        assert len(dispobjs) == 5
        # fit scores table
        scores_table = dispobjs[0].__dict__
        assert len(scores_table["table_data"]) == 4
        assert scores_table["table_data"][0][1] == "0.645"


if __name__ == "__main__":
    unittest.main()
