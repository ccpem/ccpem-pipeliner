#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import shutil
import tempfile
import shlex
import math
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    results_files_match,
    slow_test,
)
from pipeliner.scripts.job_scripts.model_validation import (
    get_molprobity_results,
    analyse_bfactors,
)
from pipeliner.scripts.job_scripts.model_validation import run_molprobity_reduce
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_STRUCTUREFACTORS,
    NODE_LOGFILE,
    NODE_LIGANDDESCRIPTION,
    NODE_RESTRAINTS,
    NODE_EVALUATIONMETRIC,
)


class RefmacServalcatTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="refmac_servalcat_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_half_map(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
                "sharpened_weighted.mrc": (
                    f"{NODE_DENSITYMAP}.mrc.refmac_servalcat.sharpened_weighted_map"
                ),
                "5me2_a_clashscore.json": f"{NODE_EVALUATIONMETRIC}.json.clashscore."
                "output",
                "refined_clashscore.json": f"{NODE_EVALUATIONMETRIC}.json.clashscore."
                "output",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --halfmaps ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job001/3488_run_half2_class001_unfil.mrc"
                " --model ../../Import/job002/5me2_a.pdb --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all",
                "gemmi sf2map diffmap.mtz sharpened_weighted.mrc",
                "molprobity.clashscore ../../Import/job002/5me2_a.pdb --json "
                "--json-filename 5me2_a_clashscore.json",
                "molprobity.clashscore refined.pdb --json --json-filename "
                "refined_clashscore.json",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id 5me2_a -clash 5me2_a_clashscore.json",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id refined -clash refined_clashscore.json",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    def test_get_command_half_map_modelname_duplicate(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_duplicate_name.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/refined.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
                "sharpened_weighted.mrc": (
                    f"{NODE_DENSITYMAP}.mrc.refmac_servalcat.sharpened_weighted_map"
                ),
                "job002_refined_clashscore.json": (
                    f"{NODE_EVALUATIONMETRIC}.json.clashscore.output"
                ),
                "job999_refined_clashscore.json": (
                    f"{NODE_EVALUATIONMETRIC}.json.clashscore.output"
                ),
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --halfmaps ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job001/3488_run_half2_class001_unfil.mrc"
                " --model ../../Import/job002/refined.pdb --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all",
                "gemmi sf2map diffmap.mtz sharpened_weighted.mrc",
                "molprobity.clashscore ../../Import/job002/refined.pdb --json "
                "--json-filename job002_refined_clashscore.json",
                "molprobity.clashscore refined.pdb --json --json-filename "
                "job999_refined_clashscore.json",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id job002_refined -clash job002_refined_clashscore.json",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id job999_refined -clash job999_refined_clashscore.json",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid job999_refined",
            ],
        )

    def test_get_command_whole_map(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_whole_map.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
                "5me2_a_molprobity.out": f"{NODE_LOGFILE}.txt.molprobity.output",
                "refined_molprobity.out": f"{NODE_LOGFILE}.txt.molprobity.output",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --map ../../Import/job001/emd_3488.mrc"
                " --model ../../Import/job002/5me2_a.pdb --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody "
                " --jellybody_params 0.02 4.2 --hydrogen all",
                "cp ../../Import/job002/5me2_a.pdb 5me2_a_reduce_out.pdb",
                f"{shlex.join(get_python_command())} {run_molprobity_reduce.__file__} "
                "-i ../../Import/job002/5me2_a.pdb -o 5me2_a_reduce_out.pdb",
                "molprobity.molprobity 5me2_a_reduce_out.pdb output.percentiles=True",
                "mv molprobity.out 5me2_a_molprobity.out",
                "mv molprobity_coot.py 5me2_a_molprobity_coot.py",
                "cp refined.pdb refined_reduce_out.pdb",
                f"{shlex.join(get_python_command())} {run_molprobity_reduce.__file__} "
                "-i refined.pdb -o refined_reduce_out.pdb",
                "molprobity.molprobity refined_reduce_out.pdb output.percentiles=True",
                "mv molprobity.out refined_molprobity.out",
                "mv molprobity_coot.py refined_molprobity_coot.py",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id 5me2_a -molp 5me2_a_molprobity.out"
                " -cootscript 5me2_a_molprobity_coot.py",
                f"{shlex.join(get_python_command())} {get_molprobity_results.__file__}"
                " -id refined -molp refined_molprobity.out"
                " -cootscript refined_molprobity_coot.py",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    def test_get_command_ligand(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_ligands.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/lig.cif": f"{NODE_LIGANDDESCRIPTION}.cif",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --map ../../Import/job001/emd_3488.mrc"
                " --model ../../Import/job002/5me2_a.pdb --ligand"
                " ../../Import/job003/lig.cif"
                " --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    def test_get_command_with_keywords(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_keywords.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --map ../../Import/job001/emd_3488.mrc"
                " --model ../../Import/job002/5me2_a.pdb"
                " --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all --keywords 'vdwr 2'"
                " 'plan 3'",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    def test_get_command_with_external_restraints(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Refmac_Servalcat/refine_external_restraints.job",
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/restraints.txt": f"{NODE_RESTRAINTS}.txt",
                "ProSMART/job004/restraints.txt": f"{NODE_RESTRAINTS}.txt",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --map ../../Import/job001/emd_3488.mrc"
                " --model ../../Import/job002/5me2_a.pdb"
                " --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all --keywords 'vdwr 2'"
                " --keyword_file ../../Import/job003/restraints.txt"
                " ../../ProSMART/job004/restraints.txt",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    def test_get_command_multi_ligand(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_multi_ligands.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/5me2_chain_a_mic_vln.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/MIC_acedrg.cif": f"{NODE_LIGANDDESCRIPTION}.cif",
                "Import/job004/VLN_acedrg.cif": f"{NODE_LIGANDDESCRIPTION}.cif",
            },
            output_nodes={
                "refined.pdb": f"{NODE_ATOMCOORDS}.pdb.refmac_servalcat.refined",
                "refined.mmcif": f"{NODE_ATOMCOORDS}.cif.refmac_servalcat.refined",
                "diffmap.mtz": f"{NODE_STRUCTUREFACTORS}.mtz.refmac_servalcat."
                "sharpened_weighted_map.difference_map",
            },
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix refined"
                " --map ../../Import/job001/emd_3488.mrc"
                " --model ../../Import/job002/5me2_chain_a_mic_vln.pdb "
                " --ligand ../../Import/job003/MIC_acedrg.cif"
                " ../../Import/job004/VLN_acedrg.cif"
                " --resolution 3.2 --ncycle 3"
                " --bfactor 40.0 --mask_radius 3.0 --ncsr local --pg C1 --jellybody"
                " --jellybody_params 0.02 4.2 --hydrogen all",
                f"{shlex.join(get_python_command())} {analyse_bfactors.__file__}"
                " -p refined.pdb -mid refined",
            ],
        )

    @slow_test
    def test_hb_refinement_half_maps(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
                "sharpened_weighted.mrc",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        results_file = os.path.join(
            self.test_data, "JobFiles/Refmac_Servalcat/half_map_results.json"
        )
        with open(results_file) as rf:
            exp_results = json.load(rf)["results"]
        for n, dob in enumerate(dispobjs):
            if dob.__dict__["title"] == "B-factor distribution":
                assert dob.__dict__.keys() == exp_results[n].keys()
                assert dob.__dict__["data"].keys() == exp_results[n]["data"].keys()
                assert len(dob.__dict__["data"]["Per-residue B-factors (Å²)"]) == len(
                    exp_results[n]["data"]["Per-residue B-factors (Å²)"]
                )
                assert all(
                    math.isclose(
                        dob.__dict__["data"]["Per-residue B-factors (Å²)"][i],
                        exp_results[n]["data"]["Per-residue B-factors (Å²)"][i],
                        rel_tol=1e-2,
                    )
                    for i in range(
                        len(exp_results[n]["data"]["Per-residue B-factors (Å²)"])
                    )
                )
            else:
                assert results_files_match(
                    dob.__dict__,
                    exp_results[n],
                    tolerance=3.0,
                )

    @slow_test
    def test_hb_refinement_whole_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_whole_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        results_file = os.path.join(
            self.test_data, "JobFiles/Refmac_Servalcat/whole_map_results.json"
        )
        with open(results_file) as rf:
            exp_results = json.load(rf)["results"]
        for n, dob in enumerate(dispobjs):
            if dob.__dict__["title"] == "B-factor distribution":
                assert dob.__dict__.keys() == exp_results[n].keys()
                assert dob.__dict__["data"].keys() == exp_results[n]["data"].keys()
                assert len(dob.__dict__["data"]["Per-residue B-factors (Å²)"]) == len(
                    exp_results[n]["data"]["Per-residue B-factors (Å²)"]
                )
                assert all(
                    math.isclose(
                        dob.__dict__["data"]["Per-residue B-factors (Å²)"][i],
                        exp_results[n]["data"]["Per-residue B-factors (Å²)"][i],
                        rel_tol=1e-2,
                    )
                    for i in range(
                        len(exp_results[n]["data"]["Per-residue B-factors (Å²)"])
                    )
                )
            else:
                assert results_files_match(
                    dob.__dict__,
                    exp_results[n],
                    tolerance=10.0,
                )

    @slow_test
    def test_multi_ligand_refine(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_multi_ligands.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_chain_a_mic_vln.pdb"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "MIC_acedrg.cif"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "VLN_acedrg.cif"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job001/emd_3488.mrc model: "
            "RefmacServalcat/job998/refined.pdb",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Import/job001/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["RefmacServalcat/job998/refined.pdb"],
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "RefmacServalcat/job998/refined.pdb",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "RefmacServalcat/job998/refined.pdb",
            ],
        }


if __name__ == "__main__":
    unittest.main()
