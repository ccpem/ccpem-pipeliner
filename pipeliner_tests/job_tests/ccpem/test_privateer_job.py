#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

import math
import shlex
from pipeliner_tests import test_data
from unittest.mock import patch
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.scripts.job_scripts import shift_model_origin_zero
import json


class PrivateerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="privateer")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch(
        "pipeliner.jobs.ccpem.privateer_job.privateer_database_path",
        new=None,
    )
    @patch("pipeliner.jobs.ccpem.privateer_job.check_origin_zero")
    def test_get_command_map_origin_nonzero_no_database(self, mock_origin_check):
        mock_origin_check.return_value = False
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/6vyb_Ndom.cif"
        input_map = "../../Import/job002/emd_21457_seg.mrc"
        exp_coms += [
            f"{shlex.join(get_python_command())} {shift_model_origin_zero.__file__} -m "
            f"{input_map} -p {input_model} --origin_only"
        ]
        exp_out_nodes["6vyb_Ndom_shifted_zero.cif"] = (
            "AtomCoords.cif.shifted.zero_origin"
        )
        privateer_command = (
            "privateer -pdbin 6vyb_Ndom_shifted_zero.cif -mapin "
            f"{input_map} -resolution 3.2 -radiusin 2.5 -expression undefined "
            "-glytoucan "
        )
        privateer_command += "-closest_match_disable -cores 1"
        exp_coms += [privateer_command]
        exp_coms += ["mv program.xml 6vyb_Ndom_program.xml"]
        exp_out_nodes["6vyb_Ndom_program.xml"] = "ProcessData.xml.privateer.output"
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Privateer/origin_shift.job",
            ),
            input_nodes={
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch(
        "pipeliner.jobs.ccpem.privateer_job.privateer_database_path",
        new="lib/data/privateer_database.json",
    )
    @patch("pipeliner.jobs.ccpem.privateer_job.check_origin_zero")
    def test_get_command_map_origin_nonzero(self, mock_origin_check):
        mock_origin_check.return_value = False
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/6vyb_Ndom.cif"
        input_map = "../../Import/job002/emd_21457_seg.mrc"
        exp_coms += [
            f"{shlex.join(get_python_command())} {shift_model_origin_zero.__file__} -m "
            f"{input_map} -p {input_model} --origin_only"
        ]
        exp_out_nodes["6vyb_Ndom_shifted_zero.cif"] = (
            "AtomCoords.cif.shifted.zero_origin"
        )
        privateer_command = (
            "privateer -pdbin 6vyb_Ndom_shifted_zero.cif -mapin "
            f"{input_map} -resolution 3.2 -radiusin 2.5 -expression undefined "
            "-glytoucan "
        )
        privateer_command += "-databasein lib/data/privateer_database.json "
        privateer_command += "-closest_match_disable -cores 1"
        exp_coms += [privateer_command]
        exp_coms += ["mv program.xml 6vyb_Ndom_program.xml"]
        exp_out_nodes["6vyb_Ndom_program.xml"] = "ProcessData.xml.privateer.output"
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Privateer/origin_shift.job",
            ),
            input_nodes={
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch(
        "pipeliner.jobs.ccpem.privateer_job.privateer_database_path",
        new=None,
    )
    @patch("pipeliner.jobs.ccpem.privateer_job.check_origin_zero")
    def test_get_command_map_undefined_no_database(self, mock_origin_check):
        mock_origin_check.return_value = False
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/6vyb_Ndom.cif"
        input_map = "../../Import/job002/emd_21457_seg.mrc"
        exp_coms += [
            f"{shlex.join(get_python_command())} {shift_model_origin_zero.__file__} -m "
            f"{input_map} -p {input_model} --origin_only"
        ]
        exp_out_nodes["6vyb_Ndom_shifted_zero.cif"] = (
            "AtomCoords.cif.shifted.zero_origin"
        )
        privateer_command = (
            "privateer -pdbin 6vyb_Ndom_shifted_zero.cif -mapin "
            f"{input_map} -resolution 3.2 -radiusin 2.5 -expression undefined "
            "-valstring ABC O5 C1 C2 C3 C4 -D- 1t0 -codein ABC "
            "-glytoucan "
        )
        privateer_command += "-closest_match_disable -cores 1"
        exp_coms += [privateer_command]
        exp_coms += ["mv program.xml 6vyb_Ndom_program.xml"]
        exp_out_nodes["6vyb_Ndom_program.xml"] = "ProcessData.xml.privateer.output"
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Privateer/origin_shift_undefined.job",
            ),
            input_nodes={
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch(
        "pipeliner.jobs.ccpem.privateer_job.privateer_database_path",
        new="lib/data/privateer_database.json",
    )
    @patch("pipeliner.jobs.ccpem.privateer_job.check_origin_zero")
    def test_get_command_map_undefined(self, mock_origin_check):
        mock_origin_check.return_value = False
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/6vyb_Ndom.cif"
        input_map = "../../Import/job002/emd_21457_seg.mrc"
        exp_coms += [
            f"{shlex.join(get_python_command())} {shift_model_origin_zero.__file__} -m "
            f"{input_map} -p {input_model} --origin_only"
        ]
        exp_out_nodes["6vyb_Ndom_shifted_zero.cif"] = (
            "AtomCoords.cif.shifted.zero_origin"
        )
        privateer_command = (
            "privateer -pdbin 6vyb_Ndom_shifted_zero.cif -mapin "
            f"{input_map} -resolution 3.2 -radiusin 2.5 -expression undefined "
            "-valstring ABC O5 C1 C2 C3 C4 -D- 1t0 -codein ABC "
            "-glytoucan "
        )
        privateer_command += "-databasein lib/data/privateer_database.json "
        privateer_command += "-closest_match_disable -cores 1"
        exp_coms += [privateer_command]
        exp_coms += ["mv program.xml 6vyb_Ndom_program.xml"]
        exp_out_nodes["6vyb_Ndom_program.xml"] = "ProcessData.xml.privateer.output"
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Privateer/origin_shift_undefined.job",
            ),
            input_nodes={
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_map_nonzero_origin(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Privateer/origin_shift.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "6vyb_Ndom.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_21457_seg.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # validation table, scores plot * 2, html display
        assert len(dispobjs) == 4
        validation_table = dispobjs[0].__dict__
        with open(
            os.path.join(
                self.test_data,
                "ResultsFiles",
                "6vyb_privateer_results_display000_table.json",
            ),
            "r",
        ) as t:
            table_results = json.load(t)
        assert validation_table == table_results
        # html
        html_plots = dispobjs[3].__dict__
        with open(
            os.path.join(
                self.test_data,
                "ResultsFiles",
                "6vyb_privateer_results_display003_html.json",
            ),
            "r",
        ) as t:
            html_results = json.load(t)
        assert html_plots == html_results
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "Privateer", "job998", "glycanview.html")
            ).st_size,
            23236,
            rel_tol=0.01,
        )
