#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
import math
from pipeliner_tests import test_data

from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.scripts.job_scripts import edit_map_origin_nstart
import mrcfile


class MapShiftTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="map-shift")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_edit_origin(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/MapShift/refmap_origin.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_21457_seg.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_21457_seg_orig0.mrc"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "emd_21457_seg_shifted.mrc"],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        output_mapfile = os.path.join(
            self.test_dir,
            "MapShift",
            "job998",
            "emd_21457_seg_shifted.mrc",
        )
        # check output size
        assert math.isclose(
            os.stat(output_mapfile).st_size,
            os.stat(os.path.join(self.test_data, "emd_21457_seg.mrc")).st_size,
            rel_tol=0.01,
        )
        # check output map origin
        with mrcfile.open(output_mapfile, mode="r") as mrc:
            assert mrc.header.origin.item() == (0.0, 0.0, 0.0)

        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Overlaid maps",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job002/emd_21457_seg_orig0.mrc",
                "MapShift/job998/emd_21457_seg_shifted.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": "Import/job002/emd_21457_seg_orig0.mrc, "
            "MapShift/job998/emd_21457_seg_shifted.mrc",
            "models_data": "",
            "associated_data": [
                "Import/job002/emd_21457_seg_orig0.mrc",
                "MapShift/job998/emd_21457_seg_shifted.mrc",
            ],
        }

    def test_edit_origin_nstart(self):
        input_map = os.path.join(self.test_data, "emd_21457_seg.mrc")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(edit_map_origin_nstart.__file__),
                "--map",
                input_map,
                "--use_record",
                "both",
                "--ox",
                "10.0",
                "--oy",
                "5.0",
                "--oz",
                "10.0",
                "--nsx",
                "5",
                "--nsy",
                "5",
                "--nsz",
                "5",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("emd_21457_seg_shifted.mrc")
        with mrcfile.open("emd_21457_seg_shifted.mrc", mode="r") as mrc:
            assert mrc.header.origin.item() == (10.0, 5.0, 10.0)
            assert mrc.header.nxstart == 5
            assert mrc.header.nystart == 5
            assert mrc.header.nzstart == 5

    def test_get_command_multi_shift(self):
        exp_out_nodes = {
            "emd_3488_shifted.mrc": "DensityMap.mrc.shifted_map.origin",
            "emd_3599_shifted.mrc": "DensityMap.mrc.shifted_map.origin",
            "emd_3488_mask_shifted.mrc": "Mask3D.mrc.shifted_mask.origin",
        }
        exp_coms = []
        input_maps = [
            "../../Import/job001/emd_3488.mrc",
            "../../Import/job002/emd_3599.mrc",
            "../../Import/job003/emd_3488_mask.mrc",
        ]
        for input_map in input_maps:
            outfile = os.path.splitext(os.path.basename(input_map))[0] + "_shifted.mrc"
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(edit_map_origin_nstart.__file__)} "
                f"--map {input_map} --use_record origin "
                "--ox 10.0 --oy 5.0 --oz 10.0 --ofile "
                f"{outfile}",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/MapShift/multi_shift.job"),
            input_nodes={
                "Import/job001/emd_3488.mrc": "DensityMap.mrc",
                "Import/job002/emd_3599.mrc": "DensityMap.mrc",
                "Import/job003/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_refmap_origin(self):
        exp_out_nodes = {
            "emd_21457_seg_shifted.mrc": "DensityMap.mrc.shifted_map.origin",
        }
        exp_coms = []
        input_maps = [
            "../../Import/job001/emd_21457_seg.mrc",
        ]
        refmap = "../../Import/job002/emd_21457_seg_orig0.mrc"
        for input_map in input_maps:
            outfile = os.path.splitext(os.path.basename(input_map))[0] + "_shifted.mrc"
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(edit_map_origin_nstart.__file__)} "
                f"--map {input_map} --use_record origin --refmap {refmap} --ofile "
                f"{outfile}",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/MapShift/refmap_origin.job"),
            input_nodes={
                "Import/job001/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job002/emd_21457_seg_orig0.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_origin_nstart(self):
        exp_out_nodes = {
            "emd_21457_seg_shifted.mrc": "DensityMap.mrc.shifted_map.origin_nstart",
        }
        exp_coms = []
        input_maps = [
            "../../Import/job001/emd_21457_seg.mrc",
        ]
        for input_map in input_maps:
            outfile = os.path.splitext(os.path.basename(input_map))[0] + "_shifted.mrc"
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(edit_map_origin_nstart.__file__)} "
                f"--map {input_map} --use_record both --ox 10.0 --oy 5.0 --oz 10.0 "
                f"--nsx 5 --nsy 5 --nsz 5 --ofile {outfile}",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/MapShift/origin_nstart.job"),
            input_nodes={
                "Import/job001/emd_21457_seg.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )


if __name__ == "__main__":
    unittest.main()
