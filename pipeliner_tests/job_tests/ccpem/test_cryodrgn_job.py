#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import unittest
import os
import shutil
import tempfile
from pathlib import Path
import subprocess

from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
    live_test,
)
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MLMODEL,
)


def prepare_cryodrgn_live_test_data(test_dir: str):
    """Untar and move project archive to test_dir ready for running
    slow live tests. The project archive data used to run these tests
    can be downloaded from https://zenodo.org/records/12703955. The
    CRYODRGN_SLOWTEST_DATA environment variable needs to be manually set as
    the full filepath for the project archive (tar) file

    Args:
        test_dir (str): test job directory
    """
    # copy the tar'ed project archive to local dir and untar
    cryodrgn_test_data = os.environ["CRYODRGN_SLOWTEST_DATA"]
    shutil.copy(cryodrgn_test_data, test_dir)
    tar_name = os.path.basename(cryodrgn_test_data)
    subprocess.run(
        ["tar", "-xzf", os.path.join(test_dir, tar_name), "-C", test_dir],
        check=True,
    )
    # <filename>.tar.gz so double splitext
    unpacked_dir = os.path.splitext(os.path.splitext(tar_name)[0])[0]
    for item in os.listdir(unpacked_dir):
        shutil.move(os.path.join(unpacked_dir, item), test_dir)


class CryoDRGNTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp()  # prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set the path to find cryodrgn
        # see if there is a functioning copy of cryodrgn
        self.oldpath = os.environ["PATH"]

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_commands(self):
        os.makedirs("CryoDRGN/job999")

        shutil.copy(
            os.path.join(self.test_data, "StarFiles/cryoDRGN_run_data.star"),
            self.test_dir,
        )

        parse_pose_cmd = (
            "cryodrgn parse_pose_star "
            "-o CryoDRGN/job999/pose.pkl "
            "cryoDRGN_run_data.star"
        )
        parse_ctf_cmd = (
            "cryodrgn parse_ctf_star "
            "-o CryoDRGN/job999/ctf.pkl "
            "cryoDRGN_run_data.star"
        )
        mkdir_cmd = "mkdir CryoDRGN/job999/mrcs"
        downsample_cmd = (
            "cryodrgn downsample "
            "-D 32 "
            "-o CryoDRGN/job999/mrcs/particles.32.mrcs "
            "--datadir Extract/job013/Movies "
            "--chunk 1000 "
            # pipeliner_tests/test_data/StarFiles/cryoDRGN_run_data.star
            "cryoDRGN_run_data.star"
        )
        train_vae_cmd = (
            "cryodrgn train_vae "
            "CryoDRGN/job999/mrcs/particles.32.txt "
            "--poses CryoDRGN/job999/pose.pkl "
            "--ctf CryoDRGN/job999/ctf.pkl "
            "--zdim 8 "
            "-n 1 "
            "-o CryoDRGN/job999/train_32 "
            "--checkpoint 1 "
            "--log-interval 1000 "
            "--max-threads 16 "
            "-b 8 "
            "--wd 0.0 "
            "--lr 0.0001 "
            "--enc-layers 1 "
            "--enc-dim 64 "
            "--dec-layers 1 "
            "--dec-dim 64 "
            "--seed 0"
        )
        analyze_cmd = (
            "cryodrgn analyze "
            "CryoDRGN/job999/train_32 "
            "0 "
            "-o CryoDRGN/job999/train_32/analyze.0"
        )

        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/CryoDRGN/cryodrgn_train_vae_job.star"
            ),
            input_nodes={
                "cryoDRGN_run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star",
            },
            output_nodes={
                "train_32/weights.pkl": f"{NODE_MLMODEL}.pkl.cryodrgn",
            },
            expected_commands=[
                parse_pose_cmd,
                parse_ctf_cmd,
                mkdir_cmd,
                downsample_cmd,
                train_vae_cmd,
                analyze_cmd,
                # cleanup_cmd,
            ],
        )

    @live_test(
        job="cryodrgn.train_vae", condition="CRYODRGN_SLOWTEST_DATA" in os.environ
    )
    @slow_test
    def test_run_cryodrgn(self):
        # Unpack and move files required for the test
        prepare_cryodrgn_live_test_data(self.test_dir)

        out_proc = job_running_test(
            overwrite_jobname="CryoDRGN/job045/",
            show_contents=True,
            test_jobfile=str(
                Path(self.test_data)
                / "JobFiles/CryoDRGN/cryodrgn_train_vae_slowtest_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "run.job",
                "job.star",
                "default_pipeline.star",
                "note.txt",
                "job_pipeline.star",
                "train_32/run.log",
                "train_32/config.yaml",
                "train_32/weights.0.pkl",
                "train_32/z.0.pkl",
                "train_32/weights.pkl",
                "train_32/z.pkl",
                "train_32/analyze.0/umap.pkl",
                "train_32/analyze.0/cryoDRGN_figures.ipynb",
                "train_32/analyze.0/cryoDRGN_viz.ipynb",
                "train_32/analyze.0/cryoDRGN_filtering.ipynb",
                "train_32/analyze.0/learning_curve_epoch0.png",
                "train_32/analyze.0/z_pca.png",
                "train_32/analyze.0/z_pca_marginals.png",
                "train_32/analyze.0/z_pca_hexbin.png",
                "train_32/analyze.0/umap.png",
                "train_32/analyze.0/umap_marginals.png",
                "train_32/analyze.0/umap_hexbin.png",
                "train_32/analyze.0/kmeans20/z_values.txt",
                "train_32/analyze.0/kmeans20/labels.pkl",
                "train_32/analyze.0/kmeans20/centers.txt",
                "train_32/analyze.0/kmeans20/centers_ind.txt",
                "train_32/analyze.0/kmeans20/vol_000.mrc",
                "train_32/analyze.0/kmeans20/vol_001.mrc",
                "train_32/analyze.0/kmeans20/vol_002.mrc",
                "train_32/analyze.0/kmeans20/vol_003.mrc",
                "train_32/analyze.0/kmeans20/vol_004.mrc",
                "train_32/analyze.0/kmeans20/vol_005.mrc",
                "train_32/analyze.0/kmeans20/vol_006.mrc",
                "train_32/analyze.0/kmeans20/vol_007.mrc",
                "train_32/analyze.0/kmeans20/vol_008.mrc",
                "train_32/analyze.0/kmeans20/vol_009.mrc",
                "train_32/analyze.0/kmeans20/vol_010.mrc",
                "train_32/analyze.0/kmeans20/vol_011.mrc",
                "train_32/analyze.0/kmeans20/vol_012.mrc",
                "train_32/analyze.0/kmeans20/vol_013.mrc",
                "train_32/analyze.0/kmeans20/vol_014.mrc",
                "train_32/analyze.0/kmeans20/vol_015.mrc",
                "train_32/analyze.0/kmeans20/vol_016.mrc",
                "train_32/analyze.0/kmeans20/vol_017.mrc",
                "train_32/analyze.0/kmeans20/vol_018.mrc",
                "train_32/analyze.0/kmeans20/vol_019.mrc",
                "train_32/analyze.0/kmeans20/z_pca.png",
                "train_32/analyze.0/kmeans20/z_pca_hex.png",
                "train_32/analyze.0/kmeans20/umap.png",
                "train_32/analyze.0/kmeans20/umap_hex.png",
                "train_32/analyze.0/pc1/z_values.txt",
                "train_32/analyze.0/pc1/vol_000.mrc",
                "train_32/analyze.0/pc1/vol_001.mrc",
                "train_32/analyze.0/pc1/vol_002.mrc",
                "train_32/analyze.0/pc1/vol_003.mrc",
                "train_32/analyze.0/pc1/vol_004.mrc",
                "train_32/analyze.0/pc1/vol_005.mrc",
                "train_32/analyze.0/pc1/vol_006.mrc",
                "train_32/analyze.0/pc1/vol_007.mrc",
                "train_32/analyze.0/pc1/vol_008.mrc",
                "train_32/analyze.0/pc1/vol_009.mrc",
                "train_32/analyze.0/pc1/umap.png",
                "train_32/analyze.0/pc1/umap_traversal.png",
                "train_32/analyze.0/pc1/umap_traversal_connected.png",
                "train_32/analyze.0/pc1/pca_traversal.png",
                "train_32/analyze.0/pc1/pca_traversal_hex.png",
                "train_32/analyze.0/pc2/z_values.txt",
                "train_32/analyze.0/pc2/vol_000.mrc",
                "train_32/analyze.0/pc2/vol_001.mrc",
                "train_32/analyze.0/pc2/vol_002.mrc",
                "train_32/analyze.0/pc2/vol_003.mrc",
                "train_32/analyze.0/pc2/vol_004.mrc",
                "train_32/analyze.0/pc2/vol_005.mrc",
                "train_32/analyze.0/pc2/vol_006.mrc",
                "train_32/analyze.0/pc2/vol_007.mrc",
                "train_32/analyze.0/pc2/vol_008.mrc",
                "train_32/analyze.0/pc2/vol_009.mrc",
                "train_32/analyze.0/pc2/umap.png",
                "train_32/analyze.0/pc2/umap_traversal.png",
                "train_32/analyze.0/pc2/umap_traversal_connected.png",
                "train_32/analyze.0/pc2/pca_traversal.png",
                "train_32/analyze.0/pc2/pca_traversal_hex.png",
                SUCCESS_FILE,
            ),
        )

        assert out_proc.output_nodes[-1].name == os.path.join(
            out_proc.name,
            "train_32/weights.pkl",
        )
        assert out_proc.output_nodes[-1].type == f"{NODE_MLMODEL}.pkl.cryodrgn"

        dispobjs = active_job_from_proc(out_proc).create_results_display()
        print("We have: {}".format(dispobjs[0].__dict__))
        assert dispobjs[0].__dict__ == {
            "title": "Training Loss",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": (
                "CryoDRGN/job998/train_32/analyze.0/learning_curve_epoch0.png"
            ),
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[1].__dict__))
        assert dispobjs[1].__dict__ == {
            "title": "PCA of latent space with density profiles",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/z_pca_marginals.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[2].__dict__))
        assert dispobjs[2].__dict__ == {
            "title": "PCA of latent space hex-binned",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/z_pca_hexbin.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[3].__dict__))
        assert dispobjs[3].__dict__ == {
            "title": "PCA of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/z_pca.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[4].__dict__))
        assert dispobjs[4].__dict__ == {
            "title": "UMAP of latent space with density profiles",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/umap_marginals.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[5].__dict__))
        assert dispobjs[5].__dict__ == {
            "title": "UMAP of latent space hex-binned",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/umap_hexbin.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[6].__dict__))
        assert dispobjs[6].__dict__ == {
            "title": "UMAP of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/umap.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[7].__dict__))
        assert dispobjs[7].__dict__ == {
            "title": "K-means centres on PCA of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/kmeans20/z_pca.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[8].__dict__))
        assert dispobjs[8].__dict__ == {
            "title": "K-means centres on hex-binned PCA of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/kmeans20/z_pca_hex.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[9].__dict__))
        assert dispobjs[9].__dict__ == {
            "title": "K-means centres on UMAP of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/kmeans20/umap.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[10].__dict__))
        assert dispobjs[10].__dict__ == {
            "title": "K-means centres on hex-binned UMAP of latent space",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "CryoDRGN/job998/train_32/analyze.0/kmeans20/umap_hex.png",
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[11].__dict__))
        assert dispobjs[11].__dict__ == {
            "title": "PCA component 1 traversal",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": (
                "CryoDRGN/job998/train_32/analyze.0/pc1/umap_traversal_connected.png"
            ),
            "image_desc": "",
            "associated_data": [],
        }

        print("We have: {}".format(dispobjs[12].__dict__))
        assert dispobjs[12].__dict__ == {
            "title": "PCA component 2 traversal",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": (
                "CryoDRGN/job998/train_32/analyze.0/pc2/umap_traversal_connected.png"
            ),
            "image_desc": "",
            "associated_data": [],
        }


if __name__ == "__main__":
    unittest.main()
