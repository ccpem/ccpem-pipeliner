#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import shlex
import unittest
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner.scripts.job_scripts.cryovae import make_cryovae_starfile
from pipeliner_tests.testing_tools import compare_starfiles
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_IMAGE2DSTACK
from pipeliner.utils import get_job_script, get_python_command


class CryoVaeTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.jobfiles = Path(self.test_data) / "JobFiles/CryoVAE"
        self.test_dir = tempfile.mkdtemp(prefix="cryovae_test")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_starfile_script_with_starfile_input(self):
        Path("CryoVAE/job001").mkdir(parents=True)
        starfile = Path(self.test_data) / "particles_short.star"
        make_cryovae_starfile.main(
            in_args=[
                "--starfile",
                str(starfile),
                "--stackfile",
                "CryoVAE/job001/stack.mrcs",
                "--outdir",
                "CryoVAE/job001/",
            ]
        )
        compare_starfiles(
            str(Path(self.test_data) / "ResultsFiles/cryovae_star.star"),
            "CryoVAE/job001/denoised_particles.star",
        )

    @patch("pipeliner.scripts.job_scripts.cryovae.make_cryovae_starfile.get_mrc_dims")
    def test_starfile_script_with_stack_input(self, dims_mock):
        dims_mock.return_value = (64, 64, 10)
        Path("CryoVAE/job001").mkdir(parents=True)
        make_cryovae_starfile.main(
            in_args=[
                "--stackfile",
                "CryoVAE/job001/stack.mrcs",
                "--outdir",
                "CryoVAE/job001/",
            ]
        )
        compare_starfiles(
            str(Path(self.test_data) / "ResultsFiles/cryovae_stack.star"),
            "CryoVAE/job001/denoised_particles.star",
        )

    def test_get_commands_star_input_with_filt(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryovae_star_filt_job.star"),
            input_nodes={
                "Extract/job002/particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.relion"
            },
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
            },
            expected_commands=[
                "relion_stack_create --i Extract/job002/particles.star --o "
                "CryoVAE/job999/aligned_stacks --apply_transformation",
                "relion_image_handler --i CryoVAE/job999/aligned_stacks.mrcs "
                "--lowpass 10 --o CryoVAE/job999/lp_filtered_stack.mrcs",
                "cryovae CryoVAE/job999/lp_filtered_stack.mrcs CryoVAE/job999/ "
                "--max_epochs 100 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} --stackfile "
                "CryoVAE/job999/recons.mrcs --outdir CryoVAE/job999 --starfile "
                "Extract/job002/particles.star",
            ],
        )

    def test_get_commands_star_input_no_filt(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryovae_star_filt_job.star"),
            input_nodes={
                "Extract/job002/particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.relion"
            },
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
            },
            expected_commands=[
                "relion_stack_create --i Extract/job002/particles.star --o "
                "CryoVAE/job999/aligned_stacks --apply_transformation",
                "relion_image_handler --i CryoVAE/job999/aligned_stacks.mrcs "
                "--lowpass 10 --o CryoVAE/job999/lp_filtered_stack.mrcs",
                "cryovae CryoVAE/job999/lp_filtered_stack.mrcs CryoVAE/job999/ "
                "--max_epochs 100 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} "
                "--stackfile CryoVAE/job999/recons.mrcs --outdir CryoVAE/job999 "
                "--starfile Extract/job002/particles.star",
            ],
        )

    def test_get_commands_stack_input_no_filt(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryovae_stack_no_filt_job.star"),
            input_nodes={"Refine3D/job002/data.mrcs": NODE_IMAGE2DSTACK + ".mrc"},
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
            },
            expected_commands=[
                "cryovae Refine3D/job002/data.mrcs CryoVAE/job999/ --max_epochs 100 "
                "--beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} --stackfile "
                "CryoVAE/job999/recons.mrcs --outdir CryoVAE/job999",
            ],
        )

    def test_get_commands_stack_input_with_filt(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "cryovae_stack_filt_job.star"),
            input_nodes={"Refine3D/job002/data.mrcs": NODE_IMAGE2DSTACK + ".mrc"},
            output_nodes={
                "recons.mrcs": NODE_IMAGE2DSTACK + ".mrc.cryovae.denoised",
                "denoised_particles.star": NODE_PARTICLEGROUPMETADATA
                + ".star.cryovae.denoised",
            },
            expected_commands=[
                "relion_image_handler --i Refine3D/job002/data.mrcs --lowpass 10 --o "
                "CryoVAE/job999/lp_filtered_stack.mrcs",
                "cryovae CryoVAE/job999/lp_filtered_stack.mrcs CryoVAE/job999/ "
                "--max_epochs 100 --beta 0.1",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cryovae/make_cryovae_starfile.py')} "
                "--stackfile CryoVAE/job999/recons.mrcs --outdir CryoVAE/job999",
            ],
        )
