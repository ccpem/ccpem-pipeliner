#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch
from unittest import mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc, new_job_of_type
from pipeliner.data_structure import NODE_ATOMCOORDS
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)


class ProcessPredictedModelTest(unittest.TestCase):
    @mock.patch.dict(os.environ, {"CCP4": "/Applications/ccp4-9"})
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="process_predicted_model")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_process_predicted_model(self):
        """
        Process AF2 model for downstream model building in MMTBX.
        Called via ccp4-python
        """

        def fake_get_env(*arg):
            args = {
                "PIPELINER_QSUB_EXTRA_COUNT": "0",
                "PIPELINER_MINIMUM_DEDICATED": "1",
                "CCP4": "Applications/ccp4",
            }
            return args.get(arg[0]) if args.get(arg[0]) else ""

        get_ccp4_env_var = "Applications/ccp4"
        command = os.path.join(get_ccp4_env_var, "bin/mmtbx.process_predicted_model")
        with patch("pipeliner.jobs.ccpem.process_predicted_model_job.os.getenv") as emk:
            emk.side_effect = fake_get_env
            job_generate_commands_test(
                jobfile=os.path.join(
                    self.test_data,
                    "JobFiles/ProcessPredictedModel/process_predicted_model_atomic_"
                    "run.job",
                ),
                input_nodes={
                    "Import/job001/AF-G9PMX7-F1-model_v4.pdb": f"{NODE_ATOMCOORDS}.pdb",
                },
                output_nodes={},
                expected_commands=[
                    command + " ../../Import/job001/AF-G9PMX7-F1-model_v4.pdb"
                    " remove_low_confidence_residues=True"
                    " split_model_by_compact_regions=True"
                    " maximum_domains=4 domain_size=80"
                ],
            )

    def test_get_command_process_predicted_model_cif(self):

        def fake_get_env(*arg):
            args = {
                "PIPELINER_QSUB_EXTRA_COUNT": "0",
                "PIPELINER_MINIMUM_DEDICATED": "1",
                "CCP4": "Applications/ccp4",
            }
            return args.get(arg[0]) if args.get(arg[0]) else ""

        get_ccp4_env_var = "Applications/ccp4"
        command = os.path.join(get_ccp4_env_var, "bin/mmtbx.process_predicted_model")
        with patch("pipeliner.jobs.ccpem.process_predicted_model_job.os.getenv") as emk:
            emk.side_effect = fake_get_env
            job_generate_commands_test(
                jobfile=os.path.join(
                    self.test_data,
                    "JobFiles/ProcessPredictedModel/process_predicted_model_atomic_"
                    "run_cif.job",
                ),
                input_nodes={
                    "Import/job001/AF-G9PMX7-F1-model_v4.cif": f"{NODE_ATOMCOORDS}.cif",
                },
                output_nodes={},
                expected_commands=[
                    "gemmi convert --shorten"
                    " ../../Import/job001/AF-G9PMX7-F1-model_v4.cif"
                    " AF-G9PMX7-F1-model_v4.pdb",
                    command + " AF-G9PMX7-F1-model_v4.pdb"
                    " remove_low_confidence_residues=True"
                    " split_model_by_compact_regions=True"
                    " maximum_domains=4 domain_size=80",
                ],
            )

    def test_prepare_predicted_model_full(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/ProcessPredictedModel/process_predicted_model_atomic_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "AF-G9PMX7-F1-model_v4.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "AF-G9PMX7-F1-model_v4_processed.pdb",
                "AF-G9PMX7-F1-model_v4_processed_A1.pdb",
                "AF-G9PMX7-F1-model_v4_processed_A2.pdb",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Process predicted models",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed.pdb",
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A1.pdb",
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A2.pdb",
            ],
            "maps_data": "",
            "models_data": (
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed.pdb,"
                " ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A1.pdb,"
                " ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A2.pdb"
            ),
            "associated_data": [
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed.pdb",
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A1.pdb",
                "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A2.pdb",
            ],
        }

    def test_create_postrun_output_nodes(self):
        Path("ProcessPredictedModel/job998").mkdir(parents=True)
        outs = [
            "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed.pdb",
            "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A1.pdb",
            "ProcessPredictedModel/job998/AF-G9PMX7-F1-model_v4_processed_A2.pdb",
        ]
        for f in outs:
            Path(f).touch()

        job = new_job_of_type("process_predicted_model.atomic_model_utilities")
        job.output_dir = "ProcessPredictedModel/job998/"
        job.joboptions["model"].value = "Import/job001/AF-G9PMX7-F1-model_v4.pdb"
        job.create_post_run_output_nodes()
        assert [x.name for x in job.output_nodes] == outs


if __name__ == "__main__":
    unittest.main()
