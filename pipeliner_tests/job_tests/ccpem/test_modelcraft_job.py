#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc, new_job_of_type
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.user_settings import get_modelcraft_executable
from pipeliner.data_structure import (
    NODE_SEQUENCE,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_ATOMCOORDS,
)


class ModelCraftJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="modelcraft")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_modelcraft_half_maps(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_half_maps.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "modelcraft.cif": f"{NODE_ATOMCOORDS}.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job002/3488_run_half2_class001_unfil.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    def test_get_commands_modelcraft_full_map(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelCraft/build_with_full_map.job",
            ),
            input_nodes={
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job005/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "modelcraft.cif": f"{NODE_ATOMCOORDS}.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job005/emd_3488.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    def test_get_commands_modelcraft_auto_stop_cycle(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelCraft/modelcraft_check_auto_stop_cycles.job",
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/3488_run_half2_class001_unfil."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "modelcraft.cif": "AtomCoords.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job002/3488_run_half2_class001_unfil.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --auto-stop-cycles 3"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    @slow_test
    def test_build_model_half_maps(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_half_maps.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "job_1_servalcat/map0_trimmed.mrc",
                "job_1_servalcat/map1_trimmed.mrc",
                "job_2_servalcat/nemap_normalized_fo.mrc",
                "job_3_cbuccaneer/xyzout.cif",
                "modelcraft.cif",
                "modelcraft.json",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Residues built and FSC average for modelcraft.cif",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": ["Residues built", "FSC average"],
            "headers": ["Residues built", "FSC average"],
            "table_data": [[578, 0.7868]],
            "associated_data": ["ModelCraft/job998/modelcraft.json"],
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job001/3488_run_half1_"
            "class001_unfil.mrc "
            "model: ModelCraft/job998/modelcraft.cif",
            "maps": ["Import/job001/3488_run_half1_class001_unfil.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelCraft/job998/modelcraft.cif"],
            "maps_data": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "models_data": "ModelCraft/job998/modelcraft.cif",
            "associated_data": [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "ModelCraft/job998/modelcraft.cif",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @slow_test
    def test_build_model_full_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_full_map.job"
            ),
            input_files=[
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
                (
                    "Import/job005",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "job_1_servalcat/map0_trimmed.mrc",
                "job_2_refmacat/mapin.ccp4",
                "job_3_cbuccaneer/xyzout.cif",
                "modelcraft.cif",
                "modelcraft.json",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Residues built and FSC average for modelcraft.cif",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": ["Residues built", "FSC average"],
            "headers": ["Residues built", "FSC average"],
            "table_data": [[671, 0.6091]],
            "associated_data": ["ModelCraft/job998/modelcraft.json"],
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job005/emd_3488.mrc model: "
            "ModelCraft/job998/modelcraft.cif",
            "maps": ["Import/job005/emd_3488.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelCraft/job998/modelcraft.cif"],
            "maps_data": "Import/job005/emd_3488.mrc",
            "models_data": "ModelCraft/job998/modelcraft.cif",
            "associated_data": [
                "Import/job005/emd_3488.mrc",
                "ModelCraft/job998/modelcraft.cif",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @patch("pipeliner.pipeliner_job.ExternalProgram.get_version")
    def test_job_marked_unavailale_if_wrong_version_found(self, mockvers):
        mockvers.return_value = "4.0.1"
        job = new_job_of_type("modelcraft.atomic_model_build")
        assert job.jobinfo.alternative_unavailable_reason == ""
        mockvers.return_value = "5"
        job = new_job_of_type("modelcraft.atomic_model_build")
        assert job.jobinfo.alternative_unavailable_reason == (
            "Modelcraft version 5 found.\n"
            "Modelcraft version < 4 and >= 5 are not currently supported"
        )
        mockvers.return_value = "no version"
        job = new_job_of_type("modelcraft.atomic_model_build")
        assert job.jobinfo.alternative_unavailable_reason == (
            "Could not find Modelcraft version"
        )


if __name__ == "__main__":
    unittest.main()
