#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
    live_test,
)
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MLMODEL,
    NODE_DENSITYMAP,
)
from pipeliner_tests.job_tests.ccpem.test_cryodrgn_job import (
    prepare_cryodrgn_live_test_data,
)


class CryoDRGNAbinitHomoTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp()  # prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set the path to find cryodrgn
        # see if there is a functioning copy of cryodrgn
        self.oldpath = os.environ["PATH"]

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_commands(self):
        os.makedirs("CryoDRGN/job999")

        shutil.copy(
            os.path.join(self.test_data, "StarFiles/cryoDRGN_run_data.star"),
            self.test_dir,
        )

        parse_ctf_cmd = (
            "cryodrgn parse_ctf_star "
            "-o CryoDRGN/job999/ctf.pkl "
            "cryoDRGN_run_data.star"
        )
        mkdir_cmd = "mkdir CryoDRGN/job999/mrcs"
        downsample_cmd = (
            "cryodrgn downsample "
            "-D 32 "
            "-o CryoDRGN/job999/mrcs/particles.32.mrcs "
            "--datadir Extract/job013/Movies "
            "--chunk 1000 "
            # pipeliner_tests/test_data/StarFiles/cryoDRGN_run_data.star
            "cryoDRGN_run_data.star"
        )
        abinit_homo_cmd = (
            "cryodrgn abinit_homo "
            "CryoDRGN/job999/mrcs/particles.32.txt "
            "--ctf CryoDRGN/job999/ctf.pkl "
            "-n 1 "
            "-o CryoDRGN/job999/train_32 "
            "--checkpoint 1 "
            "--log-interval 1000 "
            "-b 8 "
            "--wd 0.0 "
            "--lr 0.0001 "
            "--seed 0"
        )
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/CryoDRGN/cryodrgn_abinit_homo_job.star"
            ),
            input_nodes={
                "cryoDRGN_run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star",
            },
            output_nodes={
                "train_32/weights.pkl": f"{NODE_MLMODEL}.pkl.cryodrgn",
                "train_32/reconstruct.mrc": f"{NODE_DENSITYMAP}.mrc.cryodrgn",
            },
            expected_commands=[
                parse_ctf_cmd,
                mkdir_cmd,
                downsample_cmd,
                abinit_homo_cmd,
                # cleanup_cmd,
            ],
        )

    @live_test(
        job="cryodrgn.abinit_homo", condition="CRYODRGN_SLOWTEST_DATA" in os.environ
    )
    @slow_test
    def test_run_cryodrgn(self):
        # Unpack and move files required for the test
        prepare_cryodrgn_live_test_data(self.test_dir)

        out_proc = job_running_test(
            overwrite_jobname="CryoDRGN/job100/",
            show_contents=True,
            test_jobfile=str(
                Path(self.test_data)
                / "JobFiles/CryoDRGN/cryodrgn_abinit_homo_slowtest_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "run.job",
                "job.star",
                "default_pipeline.star",
                "note.txt",
                "job_pipeline.star",
                "train_32/run.log",
                "train_32/config.yaml",
                "train_32/weights.0.pkl",
                "train_32/pose.0.pkl",
                "train_32/weights.pkl",
                "train_32/pose.pkl",
                "train_32/pretrain.reconstruct.mrc",
                "train_32/reconstruct.mrc",
                SUCCESS_FILE,
            ),
        )

        assert out_proc.output_nodes[-2].name == os.path.join(
            out_proc.name,
            "train_32/weights.pkl",
        )
        assert out_proc.output_nodes[-2].type == f"{NODE_MLMODEL}.pkl.cryodrgn"

        assert out_proc.output_nodes[-1].name == os.path.join(
            out_proc.name,
            "train_32/reconstruct.mrc",
        )
        assert out_proc.output_nodes[-1].type == f"{NODE_DENSITYMAP}.mrc.cryodrgn"

        dispobjs = active_job_from_proc(out_proc).create_results_display()
        print("We have: {}".format(dispobjs[0].__dict__))
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: Ab initio Model",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "CryoDRGN/job998/train_32/reconstruct.mrc: xy",
                "CryoDRGN/job998/train_32/reconstruct.mrc: xz",
                "CryoDRGN/job998/train_32/reconstruct.mrc: yz",
            ],
            "associated_data": ["CryoDRGN/job998/train_32/reconstruct.mrc"],
            "img": "CryoDRGN/job998/Thumbnails/slices_montage_000.png",
        }

        print("We have: {}".format(dispobjs[1].__dict__))
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Ab initio Model",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["CryoDRGN/job998/train_32/reconstruct.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": "CryoDRGN/job998/train_32/reconstruct.mrc",
            "models_data": "",
            "associated_data": ["CryoDRGN/job998/train_32/reconstruct.mrc"],
        }


if __name__ == "__main__":
    unittest.main()
