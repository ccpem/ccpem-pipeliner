#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pipeliner_tests import test_data
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_PROCESSDATA
from pipeliner.utils import run_subprocess, get_python_command, get_job_script
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
)
from pipeliner_tests.testing_tools import live_test


class RibfindTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="ribfind")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @live_test(programs=["gemmi"])
    def test_run_process_model(self):
        input_pdb = os.path.join(self.test_data, "testpdb_noheader.pdb")
        run_subprocess(
            [
                get_python_command()[0],
                get_job_script("ribfind/process_input_model.py"),
                "-p",
                input_pdb,
                "-mid",
                "testpdb_noheader",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("testpdb_noheader_no_wat_lig.pdb")
        with open("testpdb_noheader_no_wat_lig.pdb", "r") as p:
            assert p.readline() == "HEADER\n"

        input_pdb = os.path.join(self.test_data, "testpdb_header.pdb")
        run_subprocess(
            [
                get_python_command()[0],
                get_job_script("ribfind/process_input_model.py"),
                "-p",
                input_pdb,
                "-mid",
                "testpdb_header",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("testpdb_header_no_wat_lig.pdb")
        with open("testpdb_noheader_no_wat_lig.pdb", "r") as p:
            count_h = 0
            for line in p:
                if line.startswith("HEADER"):
                    count_h += 1
        assert count_h == 1

    def test_get_command_protein(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/Ribfind/protein_only.job"),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={
                "ribfind.dssp": f"{NODE_PROCESSDATA}.dssp.mkdssp.secondary_structure"
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())} "
                    f"{get_job_script('ribfind/process_input_model.py')} "
                    "-p ../../Import/job001/5ni1_updated.cif "
                    "-mid 5ni1_updated"
                ),
                "mkdssp 5ni1_updated_no_wat_lig.pdb ribfind.dssp",
                (
                    "ribfind --model 5ni1_updated_no_wat_lig.pdb "
                    "--output-dir ribfind_5ni1_updated --dssp ribfind.dssp"
                ),
            ],
        )

    def test_get_command_rna_component(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/Ribfind/rna_component.job"),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={
                "ribfind.dssp": f"{NODE_PROCESSDATA}.dssp.mkdssp.secondary_structure",
                "5ni1_updated_no_wat_lig.pdb.xml": f"{NODE_PROCESSDATA}.xml.rnaview."
                "secondary_structure",
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())} "
                    f"{get_job_script('ribfind/process_input_model.py')} "
                    "-p ../../Import/job001/5ni1_updated.cif "
                    "-mid 5ni1_updated"
                ),
                "mkdssp 5ni1_updated_no_wat_lig.pdb ribfind.dssp",
                "rnaview -p 5ni1_updated_no_wat_lig.pdb",
                (
                    "ribfind --model 5ni1_updated_no_wat_lig.pdb "
                    "--output-dir ribfind_5ni1_updated --dssp ribfind.dssp "
                    "--rnaml 5ni1_updated_no_wat_lig.pdb.xml"
                ),
            ],
        )

    def test_get_command_rna_only(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/Ribfind/rna_only.job"),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={
                "5ni1_updated_no_wat_lig.pdb.xml": f"{NODE_PROCESSDATA}.xml.rnaview."
                "secondary_structure",
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())} "
                    f"{get_job_script('ribfind/process_input_model.py')} "
                    "-p ../../Import/job001/5ni1_updated.cif "
                    "-mid 5ni1_updated"
                ),
                "rnaview -p 5ni1_updated_no_wat_lig.pdb",
                (
                    "ribfind --model 5ni1_updated_no_wat_lig.pdb "
                    "--output-dir ribfind_5ni1_updated "
                    "--rnaml 5ni1_updated_no_wat_lig.pdb.xml"
                ),
            ],
        )

    def test_ribfind_protein(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Ribfind/protein_only.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "ribfind.dssp",
            ],
            expected_search_files={
                "ribfind_5ni1_updated/protein/rigid_body_0.00.txt": 1,
                "ribfind_5ni1_updated/protein/rigid_body_33.33.txt": 1,
                "ribfind_5ni1_updated/protein/rigid_body_38.46.txt": 1,
                "ribfind_5ni1_updated/protein/rigid_body_47.37.txt": 1,
                "ribfind_5ni1_updated/protein/rigid_body_50.00.txt": 1,
                "ribfind_5ni1_updated/protein/rigid_body_53.85.txt": 1,
            },
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert len(dispobjs) == 6


if __name__ == "__main__":
    unittest.main()
