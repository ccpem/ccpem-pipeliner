#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.data_structure import (
    NODE_ATOMCOORDS,
    NODE_LOGFILE,
    NODE_EVALUATIONMETRIC,
)
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)


class SliceTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="slice_test")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_slice(self):
        # Input model from https://alphafold.ebi.ac.uk/entry/G9PMX7
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/SliceNDice/slice.job"),
            input_nodes={
                "Import/job001/AF-G9PMX7-F1-model_v4.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "slicendice_0/slicendice.log": f"{NODE_LOGFILE}.txt.slice.report",
            },
            expected_commands=[
                "slicendice "
                "--bfactor_column plddt "
                "--xyzin ../../Import/job001/AF-G9PMX7-F1-model_v4.pdb "
                "--plddt_threshold 70 "
                "--clustering_method birch "
                "--min_splits 2 "
                "--max_splits 2"
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    def test_get_command_slice_pae(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/SliceNDice/slice_pae.job"),
            input_nodes={
                "Import/job001/AF-G9PMX7-F1-model_v4.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job001/AF-G9PMX7-F1-predicted"
                "_aligned_error_v4.json": f"{NODE_EVALUATIONMETRIC}.json",
            },
            output_nodes={
                "slicendice_0/slicendice.log": f"{NODE_LOGFILE}.txt.slice.report",
            },
            expected_commands=[
                "slicendice "
                "--bfactor_column plddt "
                "--xyzin ../../Import/job001/AF-G9PMX7-F1-model_v4.pdb "
                "--plddt_threshold 70 "
                "--clustering_method pae_networkx "
                "--pae_file "
                "../../Import/job001/AF-G9PMX7-F1-predicted_aligned_error_v4.json "
                "--pae_cutoff 5 "
                "--pae_power 1"
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    def test_slice_only(self):
        proc = job_running_test(
            test_jobfile=os.path.join(self.test_data, "JobFiles/SliceNDice/slice.job"),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "AF-G9PMX7-F1-model_v4.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "slicendice_0/input.pdb",
                "slicendice_0/slicendice.log",
                "slicendice_0/slicendice_results.json",
                "slicendice_0/split_2/" "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                "slicendice_0/split_2/" "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dics = [
            {
                "title": "3D viewer: Slice: Split 2",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": [],
                "maps_opacity": [],
                "maps_colours": [],
                "models_colours": ["0x1f77b4", "0xaec7e8"],
                "models": [
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                ],
                "maps_data": "",
                "models_data": "Slice/job998/slicendice_0/split_2/"
                "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb, Slice/job998/slicendice_0/"
                "split_2/pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                "associated_data": [
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                ],
            },
            {
                "title": "Slice Colour Key",
                "start_collapsed": True,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0],
                "yvalues": [0],
                "labels": ["Slice/job998/Thumbnails/color_table.tif"],
                "associated_data": ["Slice/job998/Thumbnails/color_table.tif"],
                "img": "Slice/job998/Thumbnails/Thumbnails/montage_f000.png",
            },
        ]
        for i, test_dic in enumerate(test_dics):
            for key in test_dic.keys():
                assert test_dic[key] == dispobjs[i].__dict__[key]

    def test_slice_pae(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/SliceNDice/slice_pae.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "AF-G9PMX7-F1-model_v4.pdb"),
                ),
                (
                    "Import/job001",
                    os.path.join(
                        self.test_data, "AF-G9PMX7-F1-predicted_aligned_error_v4.json"
                    ),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "slicendice_0/input.pdb",
                "slicendice_0/slicendice.log",
                "slicendice_0/slicendice_results.json",
                "slicendice_0/split_1/" "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                "slicendice_0/split_2/" "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                "slicendice_0/split_2/" "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                "slicendice_0/split_3/" "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                "slicendice_0/split_3/" "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                "slicendice_0/split_3/" "pdb_AF-G9PMX7-F1-model_v4_cluster_2.pdb",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()

        test_dics = [
            {
                "title": "3D viewer: Slice: Split 1",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": [],
                "maps_opacity": [],
                "maps_colours": [],
                "models_colours": ["0x1f77b4"],
                "models": [
                    "Slice/job998/slicendice_0/split_1/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb"
                ],
                "maps_data": "",
                "models_data": "Slice/job998/slicendice_0/split_1/"
                "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                "associated_data": [
                    "Slice/job998/slicendice_0/split_1/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb"
                ],
            },
            {
                "title": "3D viewer: Slice: Split 2",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": [],
                "maps_opacity": [],
                "maps_colours": [],
                "models_colours": ["0x1f77b4", "0xaec7e8"],
                "models": [
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                ],
                "maps_data": "",
                "models_data": "Slice/job998/slicendice_0/split_2/"
                "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb, Slice/job998/"
                "slicendice_0/split_2/pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                "associated_data": [
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_2/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                ],
            },
            {
                "title": "3D viewer: Slice: Split 3",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": [],
                "maps_opacity": [],
                "maps_colours": [],
                "models_colours": ["0x1f77b4", "0xaec7e8", "0xff7f0e"],
                "models": [
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_2.pdb",
                ],
                "maps_data": "",
                "models_data": "Slice/job998/slicendice_0/split_3/"
                "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb, Slice/job998/"
                "slicendice_0/split_3/pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb, "
                "Slice/job998/slicendice_0/split_3/"
                "pdb_AF-G9PMX7-F1-model_v4_cluster_2.pdb",
                "associated_data": [
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_0.pdb",
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_1.pdb",
                    "Slice/job998/slicendice_0/split_3/"
                    "pdb_AF-G9PMX7-F1-model_v4_cluster_2.pdb",
                ],
            },
            {
                "title": "Slice Colour Key",
                "start_collapsed": True,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0],
                "yvalues": [0],
                "labels": ["Slice/job998/Thumbnails/color_table.tif"],
                "associated_data": ["Slice/job998/Thumbnails/color_table.tif"],
                "img": "Slice/job998/Thumbnails/Thumbnails/montage_f000.png",
            },
        ]
        for i, test_dic in enumerate(test_dics):
            for key in test_dic.keys():
                assert test_dic[key] == dispobjs[i].__dict__[key]


if __name__ == "__main__":
    unittest.main()
