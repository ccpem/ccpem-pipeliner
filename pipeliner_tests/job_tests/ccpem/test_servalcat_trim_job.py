#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.job_factory import new_job_of_type, read_job
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_PROCESSDATA,
)

from pipeliner_tests.testing_tools import job_generate_commands_test


class ServalcatTrimTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="servalcat_trim_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_mask(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatTrim/mask_job.star"
            ),
            input_nodes={
                "Refine3D/job001/run_class001.mrc": NODE_DENSITYMAP + ".mrc",
                "Refine3D/job001/run_class002.mrc": NODE_DENSITYMAP + ".mrc",
                "MaskCreate/job002/mask.mrc": NODE_MASK3D + ".mrc",
            },
            output_nodes={
                "run_class001_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
                "run_class002_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
                "mask_trimmed.mrc": NODE_MASK3D + ".mrc.trimmed",
                "trim_shifts.json": NODE_PROCESSDATA + ".json.servalcat.shifts",
            },
            expected_commands=[
                "servalcat trim --maps ../../Refine3D/job001/run_class001.mrc"
                " ../../Refine3D/job001/run_class002.mrc"
                " --mask ../../MaskCreate/job002/mask.mrc"
                " --padding 15.0 --mask_cutoff 0.6 --noncubic --noncentered",
            ],
        )

    def test_get_commands_model(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatTrim/model_job.star"
            ),
            input_nodes={
                "Refine3D/job001/run_class001.mrc": NODE_DENSITYMAP + ".mrc",
                "Refine3D/job001/run_class002.mrc": NODE_DENSITYMAP + ".mrc",
                "Import/job002/model.pdb": NODE_ATOMCOORDS + ".pdb",
            },
            output_nodes={
                "run_class001_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
                "run_class002_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
                "model_trimmed.pdb": NODE_ATOMCOORDS + ".pdb.trimmed",
                "trim_shifts.json": NODE_PROCESSDATA + ".json.servalcat.shifts",
            },
            expected_commands=[
                "servalcat trim --maps ../../Refine3D/job001/run_class001.mrc"
                " ../../Refine3D/job001/run_class002.mrc"
                " --model ../../Import/job002/model.pdb"
                " --padding 15.0 --mask_cutoff 0.5",
            ],
        )

    def test_get_commands_maps_mask_and_models(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatTrim/maps_mask_models_job.star"
            ),
            input_nodes={
                "Refine3D/job029/run_half2_class001_unfil.mrc": NODE_DENSITYMAP
                + ".mrc",
                "Refine3D/job029/run_half1_class001_unfil.mrc": NODE_DENSITYMAP
                + ".mrc",
                "Refine3D/job029/run_class001.mrc": NODE_DENSITYMAP + ".mrc",
                "MaskCreate/job020/mask.mrc": NODE_MASK3D + ".mrc",
                "RefmacServalcat/job047/refined.mmcif": NODE_ATOMCOORDS + ".cif",
                "RefmacServalcat/job047/refined.pdb": NODE_ATOMCOORDS + ".pdb",
            },
            output_nodes={
                "run_half2_class001_unfil_trimmed.mrc": NODE_DENSITYMAP
                + ".mrc.trimmed",
                "run_half1_class001_unfil_trimmed.mrc": NODE_DENSITYMAP
                + ".mrc.trimmed",
                "run_class001_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
                "mask_trimmed.mrc": NODE_MASK3D + ".mrc.trimmed",
                "refined_trimmed.pdb": NODE_ATOMCOORDS + ".pdb.trimmed",
                "refined_trimmed.mmcif": NODE_ATOMCOORDS + ".cif.trimmed",
                "trim_shifts.json": NODE_PROCESSDATA + ".json.servalcat.shifts",
            },
            expected_commands=[
                "servalcat trim --maps"
                " ../../Refine3D/job029/run_half2_class001_unfil.mrc"
                " ../../Refine3D/job029/run_half1_class001_unfil.mrc"
                " ../../Refine3D/job029/run_class001.mrc --model"
                " ../../RefmacServalcat/job047/refined.mmcif"
                " ../../RefmacServalcat/job047/refined.pdb --mask"
                " ../../MaskCreate/job020/mask.mrc --padding 5.0 --mask_cutoff 0.5"
                " --noncubic --noncentered",
            ],
        )

    def test_get_commands_shifts(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatTrim/shifts_job.star"
            ),
            input_nodes={
                "Refine3D/job001/run_class001.mrc": NODE_DENSITYMAP + ".mrc",
                "ServalcatTrim/job002/trim_shifts.json": NODE_PROCESSDATA + ".json",
            },
            output_nodes={
                "run_class001_trimmed.mrc": NODE_DENSITYMAP + ".mrc.trimmed",
            },
            expected_commands=[
                "servalcat trim --maps ../../Refine3D/job001/run_class001.mrc"
                " --shifts ../job002/trim_shifts.json",
            ],
        )

    def test_creating_metadata_mask(self):
        job = new_job_of_type("servalcat.map_utilities.trim")
        job.output_dir = "Trim/job001/"
        os.makedirs(job.output_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/ServalcatTrim/logfile_mask.txt"),
            "Trim/job001/run.out",
        )
        md = job.gather_metadata()
        with open(
            os.path.join(self.test_data, "JobFiles/ServalcatTrim/metadata_mask.json")
        ) as exmd:
            expected_md = json.load(exmd)
        assert md == expected_md

    def test_creating_metadata_model(self):
        job = new_job_of_type("servalcat.map_utilities.trim")
        job.output_dir = "Trim/job001/"
        os.makedirs(job.output_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/ServalcatTrim/logfile_model.txt"),
            "Trim/job001/run.out",
        )
        md = job.gather_metadata()
        with open(
            os.path.join(self.test_data, "JobFiles/ServalcatTrim/metadata_model.json")
        ) as exmd:
            expected_md = json.load(exmd)
        assert md == expected_md

    def test_get_results_display(self):
        # Set up a job as if Servalcat Trim has been run with maps, mask and models
        # Copy in example files of the right types (though they are otherwise not
        # related to the files listed in the job.star file)
        job = read_job(
            os.path.join(
                self.test_data, "JobFiles/ServalcatTrim/maps_mask_models_job.star"
            )
        )
        job.output_dir = "ServalcatTrim/job056/"
        job.create_output_nodes()

        job_dir = Path(job.output_dir)
        job_dir.mkdir(parents=True)
        test_data = Path(self.test_data)
        shutil.copy(
            test_data / "JobFiles/ServalcatTrim/logfile_maps_mask_models.txt",
            job_dir / "run.out",
        )
        shutil.copy(
            test_data / "emd_3488.mrc",
            job_dir / "run_half1_class001_unfil_trimmed.mrc",
        )
        shutil.copy(
            test_data / "emd_3488.mrc",
            job_dir / "run_half2_class001_unfil_trimmed.mrc",
        )
        shutil.copy(
            test_data / "emd_3488.mrc",
            job_dir / "run_class001_trimmed.mrc",
        )
        shutil.copy(
            test_data / "emd_3488_mask.mrc",
            job_dir / "mask_trimmed.mrc",
        )
        # No need for model or shifts.json files

        # Now create and check the results display objects
        res = job.create_results_display()
        expres = [
            {
                "title": "Trimming results",
                "start_collapsed": False,
                "dobj_type": "table",
                "flag": "",
                "header_tooltips": ["Map", "size (voxel)", "size (Å)"],
                "headers": ["Map", "size (voxel)", "size (Å)"],
                "table_data": [
                    [
                        "../../Refine3D/job029/run_half2_class001_unfil.mrc",
                        "256 x 256 x 256",
                        "318.6778 x 318.6778 x 318.6778",
                    ],
                    [
                        "../../Refine3D/job029/run_half1_class001_unfil.mrc",
                        "256 x 256 x 256",
                        "318.6778 x 318.6778 x 318.6778",
                    ],
                    [
                        "../../Refine3D/job029/run_class001.mrc",
                        "256 x 256 x 256",
                        "318.6778 x 318.6778 x 318.6778",
                    ],
                    [
                        "../../MaskCreate/job020/mask.mrc",
                        "256 x 256 x 256",
                        "318.6778 x 318.6778 x 318.6778",
                    ],
                    ["", "", ""],
                    [
                        "Trimmed maps",
                        "132 x 166 x 94",
                        "164.3182 x 206.6426 x 117.0145",
                    ],
                ],
                "associated_data": [
                    "ServalcatTrim/job056/run_half2_class001_unfil_trimmed.mrc",
                    "ServalcatTrim/job056/run_half1_class001_unfil_trimmed.mrc",
                    "ServalcatTrim/job056/run_class001_trimmed.mrc",
                    "ServalcatTrim/job056/mask_trimmed.mrc",
                    "ServalcatTrim/job056/refined_trimmed.mmcif",
                    "ServalcatTrim/job056/refined_trimmed.pdb",
                    "ServalcatTrim/job056/trim_shifts.json",
                ],
            },
            {
                "title": "Map slices",
                "start_collapsed": False,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2],
                "yvalues": [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3],
                "labels": [
                    "ServalcatTrim/job056/mask_trimmed.mrc: xy",
                    "ServalcatTrim/job056/mask_trimmed.mrc: xz",
                    "ServalcatTrim/job056/mask_trimmed.mrc: yz",
                    "ServalcatTrim/job056/run_class001_trimmed.mrc: xy",
                    "ServalcatTrim/job056/run_class001_trimmed.mrc: xz",
                    "ServalcatTrim/job056/run_class001_trimmed.mrc: yz",
                    "ServalcatTrim/job056/run_half1_class001_unfil_trimmed.mrc: xy",
                    "ServalcatTrim/job056/run_half1_class001_unfil_trimmed.mrc: xz",
                    "ServalcatTrim/job056/run_half1_class001_unfil_trimmed.mrc: yz",
                    "ServalcatTrim/job056/run_half2_class001_unfil_trimmed.mrc: xy",
                    "ServalcatTrim/job056/run_half2_class001_unfil_trimmed.mrc: xz",
                    "ServalcatTrim/job056/run_half2_class001_unfil_trimmed.mrc: yz",
                ],
                "associated_data": [
                    "ServalcatTrim/job056/mask_trimmed.mrc",
                    "ServalcatTrim/job056/run_class001_trimmed.mrc",
                    "ServalcatTrim/job056/run_half1_class001_unfil_trimmed.mrc",
                    "ServalcatTrim/job056/run_half2_class001_unfil_trimmed.mrc",
                ],
                "img": "ServalcatTrim/job056/Thumbnails/slices_montage_000.png",
            },
        ]
        for n, x in enumerate(res):
            assert x.__dict__ == expres[n]


if __name__ == "__main__":
    unittest.main()
