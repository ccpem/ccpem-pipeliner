#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
import shlex
from pathlib import Path
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import live_test
from unittest.mock import patch
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.scripts.job_scripts.model_validation import (
    get_molprobity_results,
    run_molprobity_reduce,
    validation_results,
    get_tortoize_results,
    get_smoc_results,
    analyse_bfactors,
    fdr_val,
    get_fdrbackbone_results,
    apply_map_mask,
    get_checkmyseq_results,
)
from pipeliner.scripts.job_scripts import shift_map_origin_refmap
from pipeliner.data_structure import (
    NODE_EVALUATIONMETRIC,
    NODE_MASK3D,
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_PROCESSDATA,
    NODE_LOGFILE,
)
from pipeliner.jobs.ccpem.model_validation_job import ModelValidate

bfactor_path = os.path.realpath(analyse_bfactors.__file__)
results_path = os.path.realpath(validation_results.__file__)
get_smoc_results_path = os.path.realpath(get_smoc_results.__file__)
get_molp_results_path = os.path.realpath(get_molprobity_results.__file__)
fdr_path = None
ccpem_path = shutil.which("ccpem-python")
if ccpem_path:
    fdr_path = ccpem_path.split("/bin")[0] + "/lib/py2/FDRcontrol.py"

programs_available = all(
    [
        shutil.which("molprobity.molprobity"),
        shutil.which("TEMPy.smoc"),
        shutil.which("TEMPy.scores"),
        shutil.which("molprobity.reduce"),
        shutil.which("tortoize"),
        shutil.which("ccpem-python"),
        fdr_path,
    ]
)


class ModelValidateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="model-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_tortoize_results(self):
        tortoize_out = os.path.join(self.test_data, "5me2_a_tortoize.json")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(get_tortoize_results.__file__),
                "-tortoize",
                tortoize_out,
                "-id",
                "5me2",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5me2_tortoize_summary.json")
        assert math.isclose(
            os.stat(os.path.join(self.test_dir, "5me2_residue_tortoize.json")).st_size,
            16688,
            rel_tol=0.05,
        )

    @slow_test
    @live_test(condition=programs_available)
    def test_validation_task(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job005",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
            ],
            expected_outfiles=[
                "5me2_a_molprobity.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table, cluster table, outlier plot * 4 chains
        assert len(dispobjs) == 14
        # molp table
        mp_table = dispobjs[3].__dict__
        assert len(mp_table["table_data"]) == 14
        assert (
            mp_table["table_data"][0][0] == "Ramachandran_outliers"
            and mp_table["table_data"][0][1] == "0.00%"
        )
        # cluster table
        cluster_table = dispobjs[5].__dict__
        assert len(cluster_table["table_data"]) == 74
        assert (
            cluster_table["table_data"][0][0] == "1"
            and cluster_table["table_data"][0][1] == "B_31"
        )

    @slow_test
    @live_test(condition=programs_available)
    def test_validation_task_cif(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate_cif.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_modelcraft_10cyc.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job005",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
            ],
            expected_outfiles=[
                "5ni1_modelcraft_10cyc_molprobity.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table, cluster table, outlier plot * 4 chains
        assert len(dispobjs) == 18
        # molp table
        mp_table = dispobjs[3].__dict__
        assert len(mp_table["table_data"]) == 14
        assert (
            mp_table["table_data"][0][0] == "Ramachandran_outliers"
            and mp_table["table_data"][0][1] == "9.23%"
        )
        # cluster table
        cluster_table = dispobjs[5].__dict__
        assert len(cluster_table["table_data"]) == 415
        assert (
            cluster_table["table_data"][0][0] == "1"
            and cluster_table["table_data"][0][1] == "A_91"
        )

    @live_test(condition=programs_available)
    def test_validation_task_quick(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate_quick.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table, cluster table, outlier plot * 4 chains
        assert len(dispobjs) == 7
        # molp table
        mp_table = dispobjs[1].__dict__
        assert len(mp_table["table_data"]) == 1
        assert (
            mp_table["table_data"][0][0] == "Clashscore"
            and mp_table["table_data"][0][1] == "3.53"
        )
        # cluster table
        cluster_table = dispobjs[2].__dict__
        assert len(cluster_table["table_data"]) == 48
        assert (
            cluster_table["table_data"][0][0] == "1"
            and cluster_table["table_data"][0][1] == "D_50"
        )

    @live_test(condition=programs_available)
    def test_validation_map_nonzero_origin(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelValidation/validate_map_nonzero_origin_live.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "6vyb_Ndom.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_21457_seg.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table, cluster table, outlier plot * 4 chains
        assert len(dispobjs) == 5
        # map fit table
        mapfit_table = dispobjs[1].__dict__
        assert len(mapfit_table["table_data"]) == 4
        # with percentiles
        assert len(mapfit_table["table_data"][0]) == 3
        assert (
            mapfit_table["table_data"][0][0] == "CCC overlap mask"
            and math.isclose(
                float(mapfit_table["table_data"][0][1]), 0.313, rel_tol=0.01
            )
            and math.isclose(float(mapfit_table["table_data"][0][2]), 0.3, rel_tol=0.01)
        )
        # mp table
        mp_table = dispobjs[2].__dict__
        assert len(mp_table["table_data"]) == 14
        assert (
            mp_table["table_data"][0][0] == "Ramachandran_outliers"
            and mp_table["table_data"][0][1] == "0.00%"
        )

    @patch("pipeliner.jobs.ccpem.model_validation_job.check_origin_zero")
    def test_get_command_modelvalidation(self, mock_origin_check):
        mock_origin_check.return_value = True
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {
            "5me2_a_molprobity.out": f"{NODE_LOGFILE}.txt.molprobity.output"
        }
        exp_coms = []
        input_model = "../../Import/job001/5me2_a.pdb"
        input_map = "../../Import/job002/emd_3488.mrc"
        if bfactor_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {bfactor_path} -p {input_model} "
                "-mid 5me2_a"
            ]
        exp_coms += [
            f"cp {input_model} 5me2_a_reduce_out.pdb",
            f"{shlex.join(get_python_command())} {run_molprobity_reduce.__file__} "
            "-i ../../Import/job001/5me2_a.pdb -o 5me2_a_reduce_out.pdb",
            "molprobity.molprobity 5me2_a_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 5me2_a_molprobity.out",
            "mv molprobity_coot.py 5me2_a_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5me2_a -molp 5me2_a_molprobity.out"
            " -cootscript 5me2_a_molprobity_coot.py",
        ]
        # FDR score and confidence map
        if fdr_path:
            exp_coms += [
                "sh -c 'echo \"backend: Agg\" > matplotlibrc'",
                f"ccpem-python {fdr_path} --em_map {input_map} "
                "-method BY --testProc rightSided",
            ]

            exp_out_nodes["emd_3488_confidenceMap.mrc"] = (
                f"{NODE_MASK3D}.mrc.confidencemap.fdr_map"
            )

            exp_coms += [
                f"{shlex.join(get_python_command())} {fdr_val.__file__} "
                f"{input_model} "
                "emd_3488_confidenceMap.mrc --resolution 3.2 "
                "--coord_mode ST"
            ]
            exp_coms += [
                f"{shlex.join(get_python_command())} {get_fdrbackbone_results.__file__}"
                " -fdrscore 5me2_a_FDR_ranked_residues_ST.csv -coord "
                "5me2_a_residue_coordinates.json -id 5me2_a_emd_3488"
            ]
        # servalcat model-mask fsc
        exp_coms += [
            "servalcat fsc --model ../../Import/job001/5me2_a.pdb --mask_soft_edge 6 "
            "--halfmaps ../../Import/job003/3488_run_half1_class001_unfil.mrc "
            "../../Import/job004/3488_run_half2_class001_unfil.mrc "
            "-o modelmask_fsc.dat"
        ]

        exp_out_nodes["modelmask_fsc.dat"] = (
            f"{NODE_EVALUATIONMETRIC}.dat.servalcat.fsc"
        )
        # tempy glob scores
        globscore_input = "../../Import/job002/emd_3488.mrc"
        contour = "1e-06"
        if fdr_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {apply_map_mask.__file__} "
                f"-m {input_map} -ma emd_3488_confidenceMap.mrc --ignore_edge"
            ]
            globscore_input = "emd_3488_masked.mrc"
        elif os.path.isfile(input_map):
            contour = "0.051"
        exp_coms += [
            "TEMPy.scores -m "
            f"{globscore_input} -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            "globscores_5me2_a_vs_emd_3488 --output-format csv"
        ]
        exp_out_nodes["globscores_5me2_a_vs_emd_3488.csv"] = (
            f"{NODE_EVALUATIONMETRIC}.csv.tempy.global_scores"
        )
        # smoc
        exp_coms += [
            f"TEMPy.smoc -m {input_map} -p {input_model} -r 3.2 --smoc-window 1"
            " --output-format json --output-prefix 5me2_a_emd_3488_smoc_score",
            f"{shlex.join(get_python_command())} {get_smoc_results_path} "
            "-smoc 5me2_a_emd_3488_smoc_score.json -coord "
            "5me2_a_residue_coordinates.json -id 5me2_a_emd_3488",
        ]
        exp_out_nodes["5me2_a_emd_3488_smoc_score.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.tempy.smoc_scores"
        )
        # checkmysequence
        exp_coms += [
            "checkmysequence --mapin ../../Import/job002/emd_3488.mrc"
            " --modelin ../../Import/job001/5me2_a.pdb --seqin "
            "../../Import/job005/5ni1_mod.fasta --jsonout 5me2_a_emd_3488checkseq.json",
            f"{shlex.join(get_python_command())} {get_checkmyseq_results.__file__} "
            "-j 5me2_a_emd_3488checkseq.json -id 5me2_a_emd_3488",
        ]
        exp_out_nodes["5me2_a_emd_3488checkseq.json"] = (
            f"{NODE_PROCESSDATA}.json.checkmysequence.json_out"
        )
        # results
        exp_coms += [f"{shlex.join(get_python_command())} {results_path}"]
        exp_out_nodes["global_report.txt"] = (
            f"{NODE_EVALUATIONMETRIC}.txt.modelval.glob_output"
        )
        exp_out_nodes["outlier_clusters.csv"] = (
            f"{NODE_PROCESSDATA}.csv.modelval.cluster_output"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate.job"
            ),
            input_nodes={
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job001/5me2_a.pdb": "AtomCoords.pdb",
                "Import/job003/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job004/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job005/5ni1_mod.fasta": "Sequence.fasta",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_modelvalidation_quick(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        exp_coms += [f"gemmi convert --shorten  {input_model} 5ni1_updated.pdb"]
        exp_out_nodes["5ni1_updated.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        if bfactor_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {bfactor_path} -p "
                f"{input_model} -mid 5ni1_updated"
            ]
        exp_coms += [
            "molprobity.clashscore 5ni1_updated.pdb --json --json-filename "
            "5ni1_updated_clashscore.json",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5ni1_updated -clash 5ni1_updated_clashscore.json",
        ]
        exp_out_nodes["5ni1_updated_clashscore.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.clashscore.output"
        )
        # smoc
        exp_coms += [
            f"TEMPy.smoc -m {input_map} -p {input_model} -r 3.2 --smoc-window 1"
            " --output-format json --output-prefix 5ni1_updated_emd_3488_smoc_score",
            f"{shlex.join(get_python_command())} {get_smoc_results_path} "
            "-smoc 5ni1_updated_emd_3488_smoc_score.json -coord "
            "5ni1_updated_residue_coordinates.json -id 5ni1_updated_emd_3488",
        ]
        exp_out_nodes["5ni1_updated_emd_3488_smoc_score.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.tempy.smoc_scores"
        )
        # results
        exp_coms += [f"{shlex.join(get_python_command())} {results_path}"]
        exp_out_nodes["global_report.txt"] = (
            f"{NODE_EVALUATIONMETRIC}.txt.modelval.glob_output"
        )
        exp_out_nodes["outlier_clusters.csv"] = (
            f"{NODE_PROCESSDATA}.csv.modelval.cluster_output"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate_quick.job"
            ),
            input_nodes={
                "Import/job002/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_modelvalidation_mask(self):
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/5ni1_updated.cif"
        input_map = "../../Import/job002/emd_3488.mrc"
        input_mask = "../../Import/job005/emd_3488_mask.mrc"
        exp_coms += [f"gemmi convert --shorten  {input_model} 5ni1_updated.pdb"]
        exp_out_nodes["5ni1_updated.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        if bfactor_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {bfactor_path} -p "
                f"{input_model} -mid 5ni1_updated"
            ]
        exp_coms += [
            "cp 5ni1_updated.pdb 5ni1_updated_reduce_out.pdb",
            f"{shlex.join(get_python_command())} {run_molprobity_reduce.__file__} "
            "-i 5ni1_updated.pdb -o 5ni1_updated_reduce_out.pdb",
            "molprobity.molprobity 5ni1_updated_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 5ni1_updated_molprobity.out",
            "mv molprobity_coot.py 5ni1_updated_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5ni1_updated -molp 5ni1_updated_molprobity.out"
            " -cootscript 5ni1_updated_molprobity_coot.py",
        ]
        exp_out_nodes["5ni1_updated_molprobity.out"] = (
            f"{NODE_LOGFILE}.txt.molprobity.output"
        )
        # servalcat model-mask fsc
        exp_coms += [
            "servalcat fsc --model ../../Import/job001/5ni1_updated.cif "
            f"--mask {input_mask} "
            "--halfmaps ../../Import/job003/3488_run_half1_class001_unfil.mrc "
            "../../Import/job004/3488_run_half2_class001_unfil.mrc "
            "-o modelmask_fsc.dat"
        ]

        exp_out_nodes["modelmask_fsc.dat"] = (
            f"{NODE_EVALUATIONMETRIC}.dat.servalcat.fsc"
        )
        # TEMPy scores
        exp_coms += [
            f"{shlex.join(get_python_command())} {apply_map_mask.__file__} "
            f"-m {input_map} -ma {input_mask} "
            "--ignore_edge"
        ]
        contour = 1e-6
        exp_coms += [
            "TEMPy.scores -m "
            f"emd_3488_masked.mrc -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            "globscores_5ni1_updated_vs_emd_3488 --output-format csv"
        ]
        exp_out_nodes["globscores_5ni1_updated_vs_emd_3488.csv"] = (
            f"{NODE_EVALUATIONMETRIC}.csv.tempy.global_scores"
        )
        # smoc
        exp_coms += [
            f"TEMPy.smoc -m {input_map} -p {input_model} -r 3.2 --smoc-window 1"
            " --output-format json --output-prefix 5ni1_updated_emd_3488_smoc_score",
            f"{shlex.join(get_python_command())} {get_smoc_results_path} "
            "-smoc 5ni1_updated_emd_3488_smoc_score.json -coord "
            "5ni1_updated_residue_coordinates.json -id 5ni1_updated_emd_3488",
        ]
        exp_out_nodes["5ni1_updated_emd_3488_smoc_score.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.tempy.smoc_scores"
        )
        # results
        exp_coms += [f"{shlex.join(get_python_command())} {results_path}"]
        exp_out_nodes["global_report.txt"] = (
            f"{NODE_EVALUATIONMETRIC}.txt.modelval.glob_output"
        )
        exp_out_nodes["outlier_clusters.csv"] = (
            f"{NODE_PROCESSDATA}.csv.modelval.cluster_output"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate_cif_mask.job"
            ),
            input_nodes={
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job001/5ni1_updated.cif": "AtomCoords.cif",
                "Import/job003/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job004/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job005/emd_3488_mask.mrc": "Mask3D.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @patch("pipeliner.jobs.ccpem.model_validation_job.check_origin_zero")
    def test_get_command_map_origin_nonzero(self, mock_origin_check):
        Path("Import/job002/").mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "emd_21457_seg.mrc",
            "Import/job002/emd_21457_seg.mrc",
        )
        mock_origin_check.return_value = False
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {}
        exp_coms = []
        input_model = "../../Import/job001/6vyb_Ndom.cif"
        input_map = "../../Import/job002/emd_21457_seg.mrc"
        input_map_abspath = "Import/job002/emd_21457_seg.mrc"
        exp_coms += [f"gemmi convert --shorten {input_model} 6vyb_Ndom.pdb"]
        exp_out_nodes["6vyb_Ndom.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        if bfactor_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {bfactor_path} -p "
                f"{input_model} -mid 6vyb_Ndom"
            ]
        exp_coms += [
            "cp 6vyb_Ndom.pdb 6vyb_Ndom_reduce_out.pdb",
            f"{shlex.join(get_python_command())} {run_molprobity_reduce.__file__} "
            "-i 6vyb_Ndom.pdb -o 6vyb_Ndom_reduce_out.pdb",
            "molprobity.molprobity 6vyb_Ndom_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 6vyb_Ndom_molprobity.out",
            "mv molprobity_coot.py 6vyb_Ndom_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 6vyb_Ndom -molp 6vyb_Ndom_molprobity.out -cootscript "
            "6vyb_Ndom_molprobity_coot.py",
        ]
        exp_out_nodes["6vyb_Ndom_molprobity.out"] = (
            f"{NODE_LOGFILE}.txt.molprobity.output"
        )
        # FDR score and confidence map
        if fdr_path:
            exp_coms += [
                "sh -c 'echo \"backend: Agg\" > matplotlibrc'",
                f"ccpem-python {fdr_path} --em_map {input_map} "
                "-method BY --testProc rightSided",
            ]

            exp_out_nodes["emd_21457_seg_confidenceMap.mrc"] = (
                f"{NODE_MASK3D}.mrc.confidencemap.fdr_map"
            )
            # origin shift
            exp_coms += [
                f"{shlex.join(get_python_command())} {shift_map_origin_refmap.__file__}"
                f" -m emd_21457_seg_confidenceMap.mrc -refm {input_map}"
            ]
            exp_out_nodes["emd_21457_seg_confidenceMap_shifted.mrc"] = (
                f"{NODE_MASK3D}.mrc.confidencemap.nonzero_origin"
            )
            exp_coms += [
                f"{shlex.join(get_python_command())} {fdr_val.__file__} "
                f"{input_model} "
                "emd_21457_seg_confidenceMap_shifted.mrc --resolution 3.2 "
                "--coord_mode ST"
            ]
            exp_coms += [
                f"{shlex.join(get_python_command())} {get_fdrbackbone_results.__file__}"
                " -fdrscore 6vyb_Ndom_FDR_ranked_residues_ST.csv -coord "
                "6vyb_Ndom_residue_coordinates.json -id 6vyb_Ndom_emd_21457_seg"
            ]

        # tempy glob scores
        globscore_input = "../../Import/job002/emd_21457_seg.mrc"
        contour = "1e-06"
        if fdr_path:
            exp_coms += [
                f"{shlex.join(get_python_command())} {apply_map_mask.__file__} "
                f"-m {input_map} -ma emd_21457_seg_confidenceMap_shifted.mrc "
                "--ignore_edge"
            ]
            globscore_input = "emd_21457_seg_masked.mrc"
        elif os.path.isfile(input_map_abspath):
            contour = "0.933"
        exp_coms += [
            "TEMPy.scores -m "
            f"{globscore_input} -p {input_model} "
            f"-r 3.2 --contour-level {contour} --output-prefix "
            "globscores_6vyb_Ndom_vs_emd_21457_seg --output-format csv"
        ]
        exp_out_nodes["globscores_6vyb_Ndom_vs_emd_21457_seg.csv"] = (
            f"{NODE_EVALUATIONMETRIC}.csv.tempy.global_scores"
        )
        # smoc
        exp_coms += [
            f"TEMPy.smoc -m {input_map} -p {input_model} -r 3.2 --smoc-window 1"
            " --output-format json --output-prefix 6vyb_Ndom_emd_21457_seg_smoc_score",
            f"{shlex.join(get_python_command())} {get_smoc_results_path} "
            "-smoc 6vyb_Ndom_emd_21457_seg_smoc_score.json -coord "
            "6vyb_Ndom_residue_coordinates.json -id 6vyb_Ndom_emd_21457_seg",
        ]
        exp_out_nodes["6vyb_Ndom_emd_21457_seg_smoc_score.json"] = (
            f"{NODE_EVALUATIONMETRIC}.json.tempy.smoc_scores"
        )
        # results
        exp_coms += [f"{shlex.join(get_python_command())} {results_path}"]
        exp_out_nodes["global_report.txt"] = (
            f"{NODE_EVALUATIONMETRIC}.txt.modelval.glob_output"
        )
        exp_out_nodes["outlier_clusters.csv"] = (
            f"{NODE_PROCESSDATA}.csv.modelval.cluster_output"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelValidation/validate_map_nonzero_origin.job",
            ),
            input_nodes={
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_percentile_from_emdb_json_fscavg_with_reso(self):
        percentile_str = ModelValidate.get_percentile_from_emdb_json(
            metric="FSCavg_modelmask", score=0.7, resolution=2.5
        )
        assert percentile_str == "96.0 (0.0-2.5 Å)"

    def test_get_percentile_from_emdb_json_fscavg_with_reso_sgl_value(self):
        percentile_str = ModelValidate.get_percentile_from_emdb_json(
            metric="FSCavg_modelmask", score=0.7, resolution=2.9
        )
        assert percentile_str == "99.5 (2.5-3.5 Å)"

    def test_get_percentile_from_emdb_json_fscavg_without_reso(self):
        percentile_str = ModelValidate.get_percentile_from_emdb_json(
            metric="FSCavg_modelmask", score=0.7
        )
        assert percentile_str == "99.2 (all structures)"

    def test_get_percentile_from_emdb_json_non_existent_metric(self):
        percentile_str = ModelValidate.get_percentile_from_emdb_json(
            metric="not a metric", score=0.7
        )
        assert percentile_str == ""

    def test_get_percentile_from_emdb_json_metric_maxres(self):
        percentile_str = ModelValidate.get_percentile_from_emdb_json(
            metric="FSCavg_modelmask", score=0.7, resolution=6.7
        )
        assert percentile_str == "99.3 (>6.5 Å)"


if __name__ == "__main__":
    unittest.main()
