#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path
from pipeliner_tests import test_data

from pipeliner.job_factory import active_job_from_proc
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    job_display_object_generation_test,
    slow_test,
)
from pipeliner.data_structure import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_LOGFILE


class LocScaleTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="locscale")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_model_based(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/LocScale/locscale_localsharpen_job.star"
            ),
            input_nodes={
                "Import/job001/"
                "3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job002/"
                "3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job003/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={
                "locscale_output.mrc": f"{NODE_DENSITYMAP}.mrc.locscale.localsharpen",
                "processing_files/locscale.log": f"{NODE_LOGFILE}.txt.locscale.report",
            },
            expected_commands=[
                "cp ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job002/3488_run_half2_class001_unfil.mrc .",
                "locscale --halfmap_paths "
                "3488_run_half1_class001_unfil.mrc "
                "3488_run_half2_class001_unfil.mrc "
                "-mc ../../Import/job003/5ni1_updated.cif -res 3.2 --skip_refine "
                "-o locscale_output.mrc",
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    def test_get_command_model_partial(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/LocScale/locscale_localsharpen_hybrid_job.star",
            ),
            input_nodes={
                "Import/job001/"
                "3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job002/"
                "3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job003/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "locscale_output.mrc": f"{NODE_DENSITYMAP}.mrc.locscale.localsharpen",
                "processing_files/locscale.log": f"{NODE_LOGFILE}.txt.locscale.report",
            },
            expected_commands=[
                "cp ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job002/3488_run_half2_class001_unfil.mrc .",
                "locscale --halfmap_paths "
                "3488_run_half1_class001_unfil.mrc "
                "3488_run_half2_class001_unfil.mrc -sym C1 "
                "-mc ../../Import/job003/5me2_a.pdb --complete_model "
                "-res 3.2 --total_iterations 10 --refmac_iterations 10 "
                "-o locscale_output.mrc",
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    def test_get_command_model_free(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/LocScale/locscale_localsharpen_model_free_job.star",
            ),
            input_nodes={
                "Import/job001/"
                "3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job002/"
                "3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
            },
            output_nodes={
                "locscale_output.mrc": f"{NODE_DENSITYMAP}.mrc.locscale.localsharpen",
                "processing_files/locscale.log": f"{NODE_LOGFILE}.txt.locscale.report",
            },
            expected_commands=[
                "cp ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job002/3488_run_half2_class001_unfil.mrc .",
                "locscale --halfmap_paths "
                "3488_run_half1_class001_unfil.mrc "
                "3488_run_half2_class001_unfil.mrc -sym C1 "
                "-gpus 1 -res 3.2 --fdr_window_size 10 --use_low_context_model "
                "--batch_size 8 -o locscale_output.mrc",
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    def test_get_command_feature_enhance(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/LocScale/locscale_localsharpen_feature_enhance_job.star",
            ),
            input_nodes={
                "Import/job001/"
                "3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
                "Import/job002/"
                "3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap",
            },
            output_nodes={
                "locscale_output.mrc": f"{NODE_DENSITYMAP}.mrc.locscale.localsharpen",
                "processing_files/locscale.log": f"{NODE_LOGFILE}.txt.locscale.report",
            },
            expected_commands=[
                "cp ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job002/3488_run_half2_class001_unfil.mrc .",
                "locscale feature_enhance -np 1 --halfmap_paths "
                "3488_run_half1_class001_unfil.mrc "
                "3488_run_half2_class001_unfil.mrc -sym C1 -res 3.2 "
                "--use_low_context_model --batch_size 8 "
                "--stride 16 --monte_carlo_iterations 20 -o locscale_output.mrc",
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    @slow_test
    def test_model_based_locscale(self):
        """
        Fast implementation test with small, low resolution map (1ake).
        """
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/LocScale/locscale_localsharpen_job.star"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "1ake_10A_fitted.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "locscale_output.mrc",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Map slices",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 0, 1, 2],
            "yvalues": [0, 0, 0, 1, 1, 1],
            "labels": [
                "LocScale/job998/locscale_output.mrc: xy",
                "LocScale/job998/locscale_output.mrc: xz",
                "LocScale/job998/locscale_output.mrc: yz",
                "Import/job001/1ake_10A.mrc: xy",
                "Import/job001/1ake_10A.mrc: xz",
                "Import/job001/1ake_10A.mrc: yz",
            ],
            "associated_data": [
                "Import/job001/1ake_10A.mrc",
                "LocScale/job998/locscale_output.mrc",
            ],
            "img": "LocScale/job998/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: LocScale local sharpening",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job001/1ake_10A.mrc",
                "LocScale/job998/locscale_output.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": ["Import/job002/1ake_10A_fitted.pdb"],
            "maps_data": "Import/job001/1ake_10A.mrc, LocScale/job998/locscale_"
            "output.mrc",
            "models_data": "Import/job002/1ake_10A_fitted.pdb",
            "associated_data": [
                "Import/job001/1ake_10A.mrc",
                "LocScale/job998/locscale_output.mrc",
                "Import/job002/1ake_10A_fitted.pdb",
            ],
        }

    def test_get_results_display_model_free(self):
        Path("Import/job001").mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "3488_run_half1_class001_unfil.mrc",
            "Import/job001/3488_run_half1_class001_unfil.mrc",
        )
        Path("Import/job002").mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "3488_run_half2_class001_unfil.mrc",
            "Import/job002/3488_run_half2_class001_unfil.mrc",
        )
        exp_dobjs = [
            ResultsDisplayMontage(
                title="Map slices",
                xvalues=[0, 1, 2, 0, 1, 2, 0, 1, 2],
                yvalues=[0, 0, 0, 1, 1, 1, 2, 2, 2],
                labels=[
                    "LocScale/job999/locscale_output.mrc: xy",
                    "LocScale/job999/locscale_output.mrc: xz",
                    "LocScale/job999/locscale_output.mrc: yz",
                    "Import/job002/3488_run_half2_class001_unfil.mrc: xy",
                    "Import/job002/3488_run_half2_class001_unfil.mrc: xz",
                    "Import/job002/3488_run_half2_class001_unfil.mrc: yz",
                    "Import/job001/3488_run_half1_class001_unfil.mrc: xy",
                    "Import/job001/3488_run_half1_class001_unfil.mrc: xz",
                    "Import/job001/3488_run_half1_class001_unfil.mrc: yz",
                ],
                associated_data=[
                    "Import/job001/3488_run_half1_class001_unfil.mrc",
                    "Import/job002/3488_run_half2_class001_unfil.mrc",
                    "LocScale/job999/locscale_output.mrc",
                ],
                img="LocScale/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title="LocScale local sharpening",
                maps=[
                    "Import/job001/3488_run_half1_class001_unfil.mrc",
                    "Import/job002/3488_run_half2_class001_unfil.mrc",
                    "LocScale/job999/locscale_output.mrc",
                ],
                maps_opacity=[0.5, 0.7, 1.0],
                maps_data=(
                    "Import/job001/3488_run_half1_class001_unfil.mrc, "
                    "Import/job002/3488_run_half2_class001_unfil.mrc, "
                    "LocScale/job999/locscale_output.mrc"
                ),
                associated_data=[
                    "Import/job001/3488_run_half1_class001_unfil.mrc",
                    "Import/job002/3488_run_half2_class001_unfil.mrc",
                    "LocScale/job999/locscale_output.mrc",
                ],
            ),
        ]
        job_display_object_generation_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/LocScale/locscale_localsharpen_"
                "model_free_job.star"
            ),
            expected_display_objects=exp_dobjs,
            files_to_create={
                "locscale_output.mrc": str(Path(self.test_data) / "emd_3488.mrc")
            },
        )

    @slow_test
    def test_model_free_locscale(self):
        """
        Fast implementation test with small, low resolution map (1ake).
        """
        job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/LocScale/locscale_localsharpen_model_free_job.star",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "locscale_output.mrc",
            ],
        )


if __name__ == "__main__":
    unittest.main()
