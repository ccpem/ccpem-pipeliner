#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

import json
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMontage,
)
from pipeliner.nodes import (
    NODE_MLMODEL,
    NODE_PARTICLEGROUPMETADATA,
)

from pipeliner.job_factory import new_job_of_type


class CryoDRGNFilterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Select"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        # spoofing to point to file w/500 particles only
        sample_dir = "Refine3D/job027"
        input_dir = "CryoDRGN/job160/train_32"
        output_dir = "Select/job999"  # "Select/job148"
        os.makedirs(sample_dir)
        os.makedirs(input_dir)
        Path("CryoDRGN/job160/train_32/weights.pkl").touch()
        os.makedirs(output_dir)

        shutil.copy(
            Path(self.test_data) / "StarFiles/cryodrgn_filter_sampled_particles.star",
            "Refine3D/job027/run_data.star",
        )
        shutil.copy(
            Path(self.test_data)
            / "JobFiles/CryoDRGN/cryodrgn_filter_trainvae_job.star",
            "CryoDRGN/job160/job.star",
        )

        filtering_cmd = (
            "cryodrgn "
            "filter "
            "CryoDRGN/job160/train_32 "
            "--epoch "
            "-1 "
            "--force "
            "--sel-dir "
            "Select/job999/"
        )

        starfile_cmd = (
            "cryodrgn_utils "
            "write_star "
            "Refine3D/job027/run_data.star "
            "--ind "
            "Select/job999/indices.pkl "
            "--datadir "
            "Extract/job013/Movies "
            "-o "
            "Select/job999/selection.star"
        )

        starfile_inv_cmd = (
            "cryodrgn_utils "
            "write_star "
            "Refine3D/job027/run_data.star "
            "--ind "
            "Select/job999/indices_inverse.pkl "
            "--datadir "
            "Extract/job013/Movies "
            "-o "
            "Select/job999/inverse_selection.star"
        )

        weights_file = "CryoDRGN/job160/train_32/weights.pkl"
        sel = "selection.star"
        inv_sel = "inverse_selection.star"

        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/CryoDRGN/cryodrgn_filter_job.star"
            ),
            input_nodes={
                weights_file: f"{NODE_MLMODEL}.pkl.cryodrgn.weights",
            },
            output_nodes={
                sel: f"{NODE_PARTICLEGROUPMETADATA}.star.cryodrgn",
                inv_sel: f"{NODE_PARTICLEGROUPMETADATA}.star.cryodrgn",
            },
            expected_commands=[
                filtering_cmd,
                # mv_cmd,
                # mv_inv_cmd,
                starfile_cmd,
                starfile_inv_cmd,
            ],
        )

    # TODO add a live_test when there is known handling for interactive jobs

    def test_display_object_gen(self):
        sel_mon = ResultsDisplayMontage(
            title="Select/job999/selection.star; 30/137 images",
            xvalues=[
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            yvalues=[
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            labels=[
                "000001@Extract/job159/Movies/000000.mrcs",
                "000002@Extract/job159/Movies/000000.mrcs",
                "000003@Extract/job159/Movies/000000.mrcs",
                "000006@Extract/job159/Movies/000000.mrcs",
                "000008@Extract/job159/Movies/000000.mrcs",
                "000009@Extract/job159/Movies/000000.mrcs",
                "000010@Extract/job159/Movies/000000.mrcs",
                "000013@Extract/job159/Movies/000000.mrcs",
                "000016@Extract/job159/Movies/000000.mrcs",
                "000022@Extract/job159/Movies/000000.mrcs",
                "000024@Extract/job159/Movies/000000.mrcs",
                "000028@Extract/job159/Movies/000000.mrcs",
                "000029@Extract/job159/Movies/000000.mrcs",
                "000030@Extract/job159/Movies/000000.mrcs",
                "000033@Extract/job159/Movies/000000.mrcs",
                "000034@Extract/job159/Movies/000000.mrcs",
                "000035@Extract/job159/Movies/000000.mrcs",
                "000036@Extract/job159/Movies/000000.mrcs",
                "000040@Extract/job159/Movies/000000.mrcs",
                "000041@Extract/job159/Movies/000000.mrcs",
                "000042@Extract/job159/Movies/000000.mrcs",
                "000043@Extract/job159/Movies/000000.mrcs",
                "000045@Extract/job159/Movies/000000.mrcs",
                "000046@Extract/job159/Movies/000000.mrcs",
                "000049@Extract/job159/Movies/000000.mrcs",
                "000051@Extract/job159/Movies/000000.mrcs",
                "000052@Extract/job159/Movies/000000.mrcs",
                "000054@Extract/job159/Movies/000000.mrcs",
                "000057@Extract/job159/Movies/000000.mrcs",
                "000059@Extract/job159/Movies/000000.mrcs",
            ],
            associated_data=["Select/job999/selection.star"],
            img="Select/job999/Thumbnails/montage_s000.png",
        )

        inv_sel_mon = ResultsDisplayMontage(
            title="Select/job999/inverse_selection.star; 30/104 images",
            xvalues=[
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            yvalues=[
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            labels=[
                "000004@Extract/job159/Movies/000000.mrcs",
                "000005@Extract/job159/Movies/000000.mrcs",
                "000007@Extract/job159/Movies/000000.mrcs",
                "000011@Extract/job159/Movies/000000.mrcs",
                "000012@Extract/job159/Movies/000000.mrcs",
                "000014@Extract/job159/Movies/000000.mrcs",
                "000015@Extract/job159/Movies/000000.mrcs",
                "000017@Extract/job159/Movies/000000.mrcs",
                "000018@Extract/job159/Movies/000000.mrcs",
                "000019@Extract/job159/Movies/000000.mrcs",
                "000020@Extract/job159/Movies/000000.mrcs",
                "000021@Extract/job159/Movies/000000.mrcs",
                "000023@Extract/job159/Movies/000000.mrcs",
                "000025@Extract/job159/Movies/000000.mrcs",
                "000026@Extract/job159/Movies/000000.mrcs",
                "000027@Extract/job159/Movies/000000.mrcs",
                "000031@Extract/job159/Movies/000000.mrcs",
                "000032@Extract/job159/Movies/000000.mrcs",
                "000037@Extract/job159/Movies/000000.mrcs",
                "000038@Extract/job159/Movies/000000.mrcs",
                "000039@Extract/job159/Movies/000000.mrcs",
                "000044@Extract/job159/Movies/000000.mrcs",
                "000047@Extract/job159/Movies/000000.mrcs",
                "000048@Extract/job159/Movies/000000.mrcs",
                "000050@Extract/job159/Movies/000000.mrcs",
                "000053@Extract/job159/Movies/000000.mrcs",
                "000055@Extract/job159/Movies/000000.mrcs",
                "000056@Extract/job159/Movies/000000.mrcs",
                "000058@Extract/job159/Movies/000000.mrcs",
                "000060@Extract/job159/Movies/000000.mrcs",
            ],
            associated_data=["Select/job999/inverse_selection.star"],
            img="Select/job999/Thumbnails/montage_s001.png",
        )
        Path("Extract/job159/Movies").mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "cryodrgn_filter_imgstack.mrcs",
            Path("Extract/job159/Movies/000000.mrcs"),
        )
        job_display_object_generation_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/CryoDRGN/cryodrgn_filter_job.star"
            ),
            expected_display_objects=[
                sel_mon,
                inv_sel_mon,
            ],
            files_to_create={
                "selection.star": str(
                    Path(self.test_data) / "StarFiles/cryodrgn_filter_selection.star"
                ),
                "inverse_selection.star": str(
                    Path(self.test_data)
                    / "StarFiles/cryodrgn_filter_inverse_selection.star"
                ),
                "Extract/job159/Movies/000000.mrcs": str(
                    Path(self.test_data) / "cryodrgn_filter_imgstack.mrcs"
                ),
            },
            print_dispobj=True,
        )

    def test_results_display_interactive(self):
        # setup out/in node files
        os.makedirs("CryoDRGN/job160/train_32")
        os.makedirs("Select/job167")
        os.makedirs("Extract/job159/Movies")

        shutil.copy(
            Path(self.test_data) / "StarFiles/cryodrgn_filter_selection.star",
            "Select/job167/selection.star",
        )

        shutil.copy(
            Path(self.test_data) / "StarFiles/cryodrgn_filter_inverse_selection.star",
            "Select/job167/inverse_selection.star",
        )

        # get the mrcs file in the Movies dir
        shutil.copy(
            Path(self.test_data) / "cryodrgn_filter_imgstack.mrcs",
            "Extract/job159/Movies/000000.mrcs",
        )

        Path("CryoDRGN/job160/train_32/weights.pkl").touch()
        shutil.copy(
            Path(self.test_data) / "Pipelines/cryodrgn_filter_default_pipeline.star",
            "Select/job167/default_pipeline.star",
        )
        job = new_job_of_type("cryodrgn.filter")
        job.joboptions["weights"].value = "CryoDRGN/job160/train_32/weights.pkl"
        job.output_dir = "Select/job167"

        job.create_output_nodes()
        res = job.create_results_display()

        sel_f = os.path.join(
            self.test_data, "ResultsFiles/select_cryodrgn_filter_000.json"
        )
        inv_sel_f = os.path.join(
            self.test_data, "ResultsFiles/select_cryodrgn_filter_001.json"
        )
        with open(sel_f, "r") as sel_json:
            expected_sel = json.load(sel_json)
            assert res[0].__dict__ == expected_sel
        with open(inv_sel_f, "r") as inv_sel_json:
            expected_inv_sel = json.load(inv_sel_json)
            assert res[1].__dict__ == expected_inv_sel
