#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
import math
import mrcfile
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.utils import get_python_command, run_subprocess
from pipeliner.scripts.job_scripts import crop_pad_map
from pipeliner.data_structure import NODE_DENSITYMAP, NODE_MASK3D


class CropPadMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="map_CropPad_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_map_crop_mask(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/crop_apply_mask.job",
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "emd_3488_masked_cropped.mrc": f"{NODE_DENSITYMAP}.mrc.ccpem-utils."
                "crop_pad_map"
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())}"
                    f" {os.path.realpath(crop_pad_map.__file__)}"
                    " -m ../../Import/job001/emd_3488.mrc --mask --maskfile"
                    " ../../Import/job002/emd_3488_mask.mrc --crop --mask_threshold"
                    " 0.99 --extend 5 --out_cubic -ofile emd_3488_masked_cropped.mrc"
                ),
            ],
        )

    def test_get_command_map_crop_mask_multi(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/crop_apply_mask_multi.job",
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/emd_3599.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "emd_3488_masked_cropped.mrc": f"{NODE_DENSITYMAP}.mrc.ccpem-utils."
                "crop_pad_map",
                "emd_3599_masked_cropped.mrc": f"{NODE_DENSITYMAP}.mrc.ccpem-utils."
                "crop_pad_map",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())}"
                f" {os.path.realpath(crop_pad_map.__file__)}"
                " -m ../../Import/job001/emd_3488.mrc --mask --maskfile"
                " ../../Import/job002/emd_3488_mask.mrc --crop --mask_threshold"
                " 0.99 --extend 5 --out_cubic -ofile emd_3488_masked_cropped.mrc",
                f"{shlex.join(get_python_command())}"
                f" {os.path.realpath(crop_pad_map.__file__)}"
                " -m ../../Import/job002/emd_3599.mrc --mask --maskfile"
                " ../../Import/job002/emd_3488_mask.mrc --crop --mask_threshold"
                " 0.99 --extend 5 --out_cubic -ofile emd_3599_masked_cropped.mrc",
            ],
        )

    def test_crop_mask_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/crop_apply_mask.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "emd_3488_masked_cropped.mrc"],
        )

        job = active_job_from_proc(proc)
        output_mapfile = os.path.join(
            self.test_dir,
            "CropPadMaskMap",
            "job998",
            "emd_3488_masked_cropped.mrc",
        )
        # check output size
        assert math.isclose(
            os.stat(output_mapfile).st_size,
            2288172,
            rel_tol=0.01,
        )
        # check output map dimensions
        with mrcfile.open(output_mapfile, mode="r") as mrc:
            assert mrc.data.shape == (83, 83, 83)

        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid maps",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc",
            ],
            "maps_opacity": [0.5, 0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": (
                "Import/job001/emd_3488.mrc, "
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc"
            ),
            "models_data": "",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc",
            ],
        }
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: CropPadMaskMap/job998/emd_3488_masked_cropped.mrc",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc: xy",
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc: xz",
                "CropPadMaskMap/job998/emd_3488_masked_cropped.mrc: yz",
            ],
            "associated_data": ["CropPadMaskMap/job998/emd_3488_masked_cropped.mrc"],
            "img": "CropPadMaskMap/job998/Thumbnails/slices_montage_000.png",
        }

    def test_get_command_map_crop_contour(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/crop_threshold_map.job",
            ),
            input_nodes={"Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={
                "emd_3488_contoured_cropped.mrc": f"{NODE_DENSITYMAP}.mrc.ccpem-utils"
                + ".crop_pad_map"
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())}"
                    f" {os.path.realpath(crop_pad_map.__file__)}"
                    " -m ../../Import/job001/emd_3488.mrc --contour --threshold 0.08"
                    " --crop --extend 5 -ofile emd_3488_contoured_cropped.mrc"
                ),
            ],
        )

    def test_crop_contour_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/crop_threshold_map.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                )
            ],
            expected_outfiles=["run.out", "run.err", "emd_3488_contoured_cropped.mrc"],
        )

        job = active_job_from_proc(proc)
        output_mapfile = os.path.join(
            self.test_dir,
            "CropPadMaskMap",
            "job998",
            "emd_3488_contoured_cropped.mrc",
        )
        # check output size
        assert math.isclose(
            os.stat(output_mapfile).st_size,
            1502524,
            rel_tol=0.01,
        )
        # check output map dimensions
        with mrcfile.open(output_mapfile, mode="r") as mrc:
            assert mrc.data.shape == (65, 77, 75)

        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid maps",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc",
            ],
            "maps_opacity": [0.5, 0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": (
                "Import/job001/emd_3488.mrc, "
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc"
            ),
            "models_data": "",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc",
            ],
        }
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc: xy",
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc: xz",
                "CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc: yz",
            ],
            "associated_data": ["CropPadMaskMap/job998/emd_3488_contoured_cropped.mrc"],
            "img": "CropPadMaskMap/job998/Thumbnails/slices_montage_000.png",
        }

    def test_get_command_map_pad(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/pad_map.job",
            ),
            input_nodes={"Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={
                "emd_3488_padded.mrc": f"{NODE_DENSITYMAP}.mrc.ccpem-utils.crop_pad_map"
            },
            expected_commands=[
                (
                    f"{shlex.join(get_python_command())}"
                    f" {os.path.realpath(crop_pad_map.__file__)}"
                    " -m ../../Import/job001/emd_3488.mrc --pad --cpx 10 --cpy 10"
                    " --cpz 10 --fill_pad 0.01 -ofile emd_3488_padded.mrc"
                ),
            ],
        )

    def test_pad_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/CropPad_Map/pad_map.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                )
            ],
            expected_outfiles=["run.out", "run.err", "emd_3488_padded.mrc"],
        )

        job = active_job_from_proc(proc)
        output_mapfile = os.path.join(
            self.test_dir,
            "CropPadMaskMap",
            "job998",
            "emd_3488_padded.mrc",
        )
        # check output size
        assert math.isclose(
            os.stat(output_mapfile).st_size,
            6913024,
            rel_tol=0.01,
        )
        # check output map dimensions
        with mrcfile.open(output_mapfile, mode="r") as mrc:
            assert mrc.data.shape == (120, 120, 120)

        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid maps",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_padded.mrc",
            ],
            "maps_opacity": [0.5, 0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": (
                "Import/job001/emd_3488.mrc, "
                "CropPadMaskMap/job998/emd_3488_padded.mrc"
            ),
            "models_data": "",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "CropPadMaskMap/job998/emd_3488_padded.mrc",
            ],
        }

    # threshold crop script test
    def test_threshold_crop_map_script(self):
        input_map = os.path.join(self.test_data, "emd_3488.mrc")
        run_subprocess(
            [
                f"{shlex.join(get_python_command())}",
                f"{os.path.realpath(crop_pad_map.__file__)}",
                "-m",
                input_map,
                "--contour",
                "--threshold",
                "0.08",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("emd_3488_contoured.mrc")
        run_subprocess(
            [
                f"{shlex.join(get_python_command())}",
                f"{os.path.realpath(crop_pad_map.__file__)}",
                "-m",
                "emd_3488_contoured.mrc",
                "--crop",
                "--threshold",
                "0.01",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("emd_3488_contoured_cropped.mrc")
        assert math.isclose(
            os.stat("emd_3488_contoured_cropped.mrc").st_size,
            2219524,
            rel_tol=0.05,
        )


if __name__ == "__main__":
    unittest.main()
