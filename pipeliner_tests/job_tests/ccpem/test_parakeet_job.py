#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import unittest
import os
import shutil
import tempfile
import difflib
import filecmp
import shlex
from pathlib import Path

from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.utils import (
    get_pipeliner_root,
    get_python_command,
    get_job_script,
)
from pipeliner.scripts.job_scripts.parakeet import make_micrograph_starfile
from pipeliner.scripts.job_scripts.parakeet import make_configuration_yaml
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_PARAMSDATA,
)


class ParakeetTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp()  # prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set the path to find parakeet
        # see if there is a functioning copy of parakeet
        self.oldpath = os.environ["PATH"]

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.environ["PATH"] = self.oldpath

    def test_get_commands(self):
        os.makedirs("Parakeet/job999")

        shutil.copy(
            os.path.join(self.test_data, "parakeet_config.yaml"),
            self.test_dir,
        )
        mkdir_cmd = "mkdir Parakeet/job999/Micrographs"

        make_parakeet_config_yaml_cmd = (
            f"{shlex.join(get_python_command())} "
            f"{make_configuration_yaml.__file__} --device gpu"
            " --acceleration_voltage_spread 8e-07 --electrons_per_angstrom 45.0"
            " --energy 300.0 --energy_spread 2.66e-06 --illumination_semiangle 0.02"
            " --phi 0.0 --theta 0.0 --dqe False --nx 1000 --ny 1000 --pixel_size 1.0"
            " --Detector_origin_x 0 --Detector_origin_y 0 --phase_plate False"
            " --phase_shift 90.0 --spot_radius 0.005"
            " --c_10 -11179.73827016168 --c_12 0.0 --phi_12 0.0 --c_21 0.0 --phi_21 0.0"
            " --c_23 0.0 --phi_23 0.0 --c_30 2.7 --c_32 0.0 --phi_32 0.0 --c_34 0.0"
            " --phi_34 0.0 --c_41 0.0 --phi_41 0.0 --c_43 0.0 --phi_43 0.0 --c_45 0.0"
            " --phi_45 0.0 --c_50 0.0 --c_52 0.0 --phi_52 0.0 --c_54 0.0 --phi_54 0.0"
            " --c_56 0.0 --phi_56 0.0 --c_c 2.7 --current_spread 3.3e-07 --box_x 1000.0"
            " --box_y 1000.0 --box_z 500.0 --centre_x 500.0 --centre_y 500.0"
            " --centre_z 250.0 --cube_length 1000.0 --cuboid_length_x 1000.0"
            " --cuboid_length_y 1000.0 --cuboid_length_z 500.0 --cylinder_length 1000.0"
            " --cylinder_radius 500.0 --margin_x 0.0 --margin_y 0.0 --margin_z 0.0"
            " --type cuboid --fast_ice False --simulation_margin 100"
            " --simulation_padding 100 --radiation_damage_model False"
            " --sensitivity_coefficient 0.022 --slice_thickness 3.0"
            " --config_yaml_filename Parakeet/job999/Micrographs/image_000001.yaml"
            " --pdb_source pdb --pdb_filepaths 4v1w --pdb_instances 1"
        )

        sample_new_cmd = (
            "parakeet.sample.new -c"
            " Parakeet/job999/Micrographs/image_000001.yaml"
            " -s Parakeet/job999/sample.h5"
        )

        sample_add_mols_cmd = (
            "parakeet.sample.add_molecules -c"
            " Parakeet/job999/Micrographs/image_000001.yaml"
            " -s Parakeet/job999/sample.h5"
        )

        simulate_exitwave_cmd = (
            "parakeet.simulate.exit_wave"
            " -c Parakeet/job999/Micrographs/image_000001.yaml"
            " -s Parakeet/job999/sample.h5"
            " -e Parakeet/job999/exit_wave.h5 -d gpu"
        )

        simulate_optics_cmd = (
            "parakeet.simulate.optics"
            " -c Parakeet/job999/Micrographs/image_000001.yaml"
            " -e Parakeet/job999/exit_wave.h5"
            " -o Parakeet/job999/optics.h5 -d gpu"
        )

        simulate_image_cmd = (
            "parakeet.simulate.image"
            " -c Parakeet/job999/Micrographs/image_000001.yaml"
            " -o Parakeet/job999/optics.h5"
            " -i Parakeet/job999/image.h5"
        )

        export_cmd = (
            "parakeet.export Parakeet/job999/image.h5"
            " -o Parakeet/job999/Micrographs/image_000001.mrc"
        )

        parakeet_metadata_cmd = (
            "parakeet.metadata.export -c"
            " Parakeet/job999/Micrographs/image_000001.yaml"
            " -s Parakeet/job999/sample.h5"
            " --directory Parakeet/job999/"
        )

        starfile = "Parakeet/job999/synthetic_micrographs.star"
        starfile_cmd = (
            f"{shlex.join(get_python_command())} {make_micrograph_starfile.__file__}"
            " --mtf_star Parakeet/job999/relion/mtf_300kV.star"
            " --original_pixel_size 1.0"
            " --pixel_size 1.0 --voltage 300.0 --c_30 2.7"
            " --micrograph_name_dir Parakeet/job999/Micrographs"
            " --micrograph_search_string image_*.mrc"
            " --optics_group_name opticsGroup1 --amplitude_contrast 0.02"
            f" --micrograph_starfile_name {starfile}"
        )
        cleanup_cmd = (
            f"{shlex.join(get_python_command())} "
            f"{get_job_script('cleanup_tmp_files.py')}  --files "
            "Parakeet/job999/relion/relion_input.star "
            "Parakeet/job999/relion/corrected_micrographs.star "
            "Parakeet/job999/relion/particle.star Parakeet/job999/sample.h5 "
            "Parakeet/job999/exit_wave.h5 Parakeet/job999/optics.h5 "
            "Parakeet/job999/image.h5"
        )
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Parakeet/parakeet_simulate_job.star"
            ),
            input_nodes={
                "parakeet_config.yaml": f"{NODE_PARAMSDATA}.yaml.parakeet.config",
            },
            output_nodes={
                "relion/mtf_300kV.star": f"{NODE_MICROSCOPEDATA}.star.parakeet.mtf",
                "synthetic_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "parakeet.synthetic",
            },
            expected_commands=[
                mkdir_cmd,
                make_parakeet_config_yaml_cmd,
                sample_new_cmd,
                sample_add_mols_cmd,
                simulate_exitwave_cmd,
                simulate_optics_cmd,
                simulate_image_cmd,
                export_cmd,
                parakeet_metadata_cmd,
                starfile_cmd,
                cleanup_cmd,
            ],
        )

    def test_make_micrograph_starfile(self):
        # tmp dir created during tests means filepaths returned from glob.glob
        # are different as self.test_dir changes every time
        # So instead just passing a string to a non-existent dir
        # and changing list of micrographs in reference starfile
        # to be empty
        micrograph_dir = "Parakeet/job999/Micrographs"

        # make the reference mtf file easily available for local use
        shutil.copy(
            os.path.join(self.test_data, "mtf_300kV_all_1.0.star"),
            self.test_dir,
        )

        # set up the arguments for testing micrograph creation
        starfile = os.path.join(self.test_dir, "synthetic_micrographs.star")
        test_args = [
            "--mtf_star",
            "mtf_300kV_all_1.0.star",
            "--original_pixel_size",
            "1.0",
            "--pixel_size",
            "1.0",
            "--voltage",
            "300.0",
            "--c_30",
            "2.7",
            "--micrograph_name_dir",
            "{}".format(micrograph_dir),
            "--micrograph_search_string",
            "image_*.mrc",
            "--optics_group_name",
            "opticsGroup1",
            "--amplitude_contrast",
            "0.1",
            "--micrograph_starfile_name",
            "{}".format(starfile),
        ]

        # create the micrographs starfile
        # starfile validation occurs in the called function
        make_micrograph_starfile.main(test_args)

        # compare the created file to a reference file
        newfile = os.path.join(
            os.path.join(get_pipeliner_root(), "../"),
            "pipeliner_tests/test_data/synthetic_micrographs.star",
        )

        # Find and print the diff between new and reference starfiles
        with open(starfile) as ref:
            ref_text = ref.readlines()

        with open(newfile) as new:
            new_text = new.readlines()

        filecomp = difflib.unified_diff(
            ref_text, new_text, fromfile=starfile, tofile=newfile, lineterm="", n=0
        )
        line_errs = False
        for line in filecomp:
            if line[0].startswith("@@") and line[0].endswith("@@"):
                line_errs = True
        assert not line_errs, filecomp

    def test_make_configuration_yaml(self):
        reference_yaml_file = os.path.join(
            get_pipeliner_root(), "../pipeliner_tests/test_data/parakeet_config.yaml"
        )
        os.makedirs("Parakeet/job999/")
        new_yaml = "Parakeet/job999/config.yaml"
        test_args = [
            "--config_yaml_filename",
            new_yaml,
        ]
        make_configuration_yaml.main(test_args)
        # compare the created file to a reference file
        # Find and print the diff between new and reference yamls
        os.system("ls -lrth Parakeet/job999/")
        with open(new_yaml) as ref:
            ref_text = ref.readlines()

        with open(reference_yaml_file) as new:
            new_text = new.readlines()

        for line in difflib.unified_diff(
            ref_text,
            new_text,
            fromfile=new_yaml,
            tofile=reference_yaml_file,
            lineterm="",
        ):
            print(line)

        # ensure they are the same
        assert filecmp.cmp(new_yaml, reference_yaml_file)

    # TODO add slowtest decorator???
    def test_run_parakeet(self):
        shutil.copy(
            os.path.join(self.test_data, "parakeet_config.yaml"),
            self.test_dir,
        )

        out_proc = job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Parakeet/parakeet_simulate_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "relion/mtf_300kV.star",
                "synthetic_micrographs.star",
                "Micrographs/image_000001.mrc",
                SUCCESS_FILE,
            ),
        )

        # Check that the output nodes were created properly
        assert out_proc.output_nodes[-2].name == os.path.join(
            out_proc.name, "relion/mtf_300kV.star"
        )
        assert out_proc.output_nodes[-2].type == (
            f"{NODE_MICROSCOPEDATA}.star.parakeet.mtf"
        )

        assert out_proc.output_nodes[-1].name == os.path.join(
            out_proc.name, "synthetic_micrographs.star"
        )
        assert (
            out_proc.output_nodes[-1].type
            == f"{NODE_MICROGRAPHGROUPMETADATA}.star.parakeet.synthetic"
        )

        # make sure cleanup happened
        assert not os.path.isfile("Parakeet/job999/sample.h5")
        assert not os.path.isfile("Parakeet/job999/exit_wave.h5")
        assert not os.path.isfile("Parakeet/job999/optics.h5")
        assert not os.path.isfile("Parakeet/job999/image.h5")
        assert not os.path.isfile("Parakeet/job999/relion/relion_input.star")
        assert not os.path.isfile("Parakeet/job999/relion/corrected_micrographs.star")
        assert not os.path.isfile("Parakeet/job999/relion/particle.star")

        dispobjs = active_job_from_proc(out_proc).create_results_display()
        print("We have: {}".format(dispobjs[0].__dict__))
        assert dispobjs[0].__dict__ == {
            "title": "Parakeet/job999/synthetic_micrographs.star; 1/1 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0],
            "yvalues": [0],
            "labels": ["Parakeet/job999/Micrographs/image_000001.mrc"],
            "associated_data": ["Parakeet/job999/synthetic_micrographs.star"],
            "img": "Parakeet/job999/Thumbnails/montage_f000.png",
        }


if __name__ == "__main__":
    unittest.main()
