#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_python_command, get_job_script
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_LIGANDDESCRIPTION


class AceDRGTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="acedrg_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_smiles_string(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_run.job"
            ),
            input_nodes={},
            output_nodes={
                "TYL_acedrg.cif": f"{NODE_LIGANDDESCRIPTION}.cif.acedrg.restraints",
                "TYL_acedrg.mmcif": f"{NODE_ATOMCOORDS}.cif.acedrg.coordinates",
            },
            expected_commands=[
                "acedrg --smi=CC(=O)Nc1ccc(O)cc1 --res=TYL --out=TYL_acedrg",
                "gemmi convert --from=chemcomp --to=mmcif TYL_acedrg.cif "
                "TYL_acedrg.mmcif",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_for_output_files.py')} "
                "--files TYL_acedrg.cif TYL_acedrg.mmcif",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cleanup_tmp_files.py')} "
                "--dirs TYL_acedrg_TMP",
            ],
        )

    def test_get_command_cif_file(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_input_cif.job"
            ),
            input_nodes={
                "lig.cif": "AtomCoords.cif",
            },
            output_nodes={
                "LIG_acedrg.cif": f"{NODE_LIGANDDESCRIPTION}.cif.acedrg.restraints",
                "LIG_acedrg.mmcif": f"{NODE_ATOMCOORDS}.cif.acedrg.coordinates",
            },
            expected_commands=[
                "acedrg --mmcif=../../lig.cif --res=LIG --out=LIG_acedrg",
                "gemmi convert --from=chemcomp --to=mmcif LIG_acedrg.cif "
                "LIG_acedrg.mmcif",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_for_output_files.py')} "
                "--files LIG_acedrg.cif LIG_acedrg.mmcif",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('cleanup_tmp_files.py')} "
                "--dirs LIG_acedrg_TMP",
            ],
        )

    @slow_test
    def test_create_ligand(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_run.job"
            ),
            input_files=[],
            expected_outfiles=[
                "run.out",
                "run.err",
                "TYL_acedrg.cif",
                "TYL_acedrg.mmcif",
            ],
            show_contents=True,
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: TYL_acedrg.mmcif",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": ["AceDRG/job998/TYL_acedrg.mmcif"],
            "maps_data": "",
            "models_data": "AceDRG/job998/TYL_acedrg.mmcif",
            "associated_data": ["AceDRG/job998/TYL_acedrg.mmcif"],
        }


if __name__ == "__main__":
    unittest.main()
