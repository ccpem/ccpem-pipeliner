#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner.job_factory import new_job_of_type
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.utils import touch, get_python_command, get_job_script
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_SEQUENCE,
    NODE_ATOMCOORDS,
)
from ccpem_utils.model.gemmi_model_utils import GemmiModelUtils
import gemmi


class ModelAngeloTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.mangelo_node = NODE_ATOMCOORDS + ".cif.modelangelo"
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_commands_with_multi_fasta_gpu(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        infile = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_with_multi_fasta_gpu_job.star"
        )

        job_generate_commands_test(
            jobfile=infile,
            input_nodes={
                "my_map.mrc": NODE_DENSITYMAP + ".mrc",
                "my_protein_sequence.fasta": NODE_SEQUENCE + ".fasta",
                "my_dna_sequence.fasta": NODE_SEQUENCE + ".fasta",
                "my_rna_sequence.fasta": NODE_SEQUENCE + ".fasta",
            },
            output_nodes={"job999.cif": self.mangelo_node},
            expected_commands=[
                "model_angelo build -v my_map.mrc -pf my_protein_sequence.fasta"
                " -df my_dna_sequence.fasta -rf my_rna_sequence.fasta --device 0,1"
                " -o ModelAngelo/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_commands_with_fasta(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        infile = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_with_protein_fasta_job.star"
        )

        job_generate_commands_test(
            jobfile=infile,
            input_nodes={
                "my_map.mrc": NODE_DENSITYMAP + ".mrc",
                "my_protein_sequence.fasta": NODE_SEQUENCE + ".fasta",
            },
            output_nodes={"job999.cif": self.mangelo_node},
            expected_commands=[
                "model_angelo build -v my_map.mrc -pf my_protein_sequence.fasta"
                " -o ModelAngelo/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_commands_no_fasta_no_hmmer(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        infile = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_no_fasta_map_job.star"
        )

        job_generate_commands_test(
            jobfile=infile,
            # Check converts .map to .mrc
            input_nodes={
                "my_map.map": NODE_DENSITYMAP + ".mrc",
            },
            output_nodes={"job999.cif": self.mangelo_node},
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('modelangelo/modelangelo_tools.py')} "
                "--symlink_mrc_map my_map.map",
                "model_angelo build_no_seq -v my_map.mrc -o " "ModelAngelo/job999/",
            ],
        )

    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_commands_no_fasta_with_hmmer(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        jf = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_no_fasta_hmmer_job.star"
        )
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={
                "my_map.map": NODE_DENSITYMAP + ".mrc",
                "mylib.fasta": NODE_SEQUENCE + ".fasta",
            },
            output_nodes={"job999.cif": self.mangelo_node},
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('modelangelo/modelangelo_tools.py')} "
                "--symlink_mrc_map my_map.map",
                "model_angelo build_no_seq -v my_map.mrc -o " "ModelAngelo/job999/",
                "model_angelo hmm_search -i ModelAngelo/job999/ -f mylib.fasta -o "
                "ModelAngelo/job999/ -a DNA --F1 0.1 --F2 0.001 --F3 1 --E 100",
            ],
        )

    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_commands_no_build_just_hmmer(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        jf = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_only_hmmer_job.star"
        )
        os.makedirs("ModelAngelo/job001")
        touch("ModelAngelo/job001/protein_model.cif")
        job_generate_commands_test(
            jobfile=jf,
            input_nodes={
                "ModelAngelo/job001/protein_model.cif": NODE_ATOMCOORDS + ".cif",
                "mylib.fasta": NODE_SEQUENCE + ".fasta",
            },
            output_nodes={},
            expected_commands=[
                "model_angelo hmm_search -i ModelAngelo/job001 -f mylib.fasta -o "
                "ModelAngelo/job999/ -a RNA --F1 0.10 --F2 0.01 --F3 101 --E 102",
            ],
        )

    def gemmi_utils_init(self, x):
        gemmi_utils_input = os.path.join(
            os.path.dirname(test_data.__file__), "5ni1_updated.cif"
        )
        self.structure = gemmi.read_structure(gemmi_utils_input)

    # mock init of class with a new init function
    @patch.object(GemmiModelUtils, "__init__", gemmi_utils_init)
    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_results_display(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        ma_job = new_job_of_type("modelangelo")
        ma_job.joboptions["fn_map"].value = "my_map.mrc"
        ma_job.output_dir = "ModelAngelo/job001/"
        ma_job.joboptions["p_seq"].value = os.path.join(
            self.test_data, "5ni1_entry.fasta"
        )
        rdo = ma_job.create_results_display()
        expected_rdo = {
            "maps": ["my_map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelAngelo/job001/job001.cif"],
            "title": "3D viewer: Generated atomic model",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["ModelAngelo/job001/job001.cif"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert len(rdo) == 2
        assert rdo[0].__dict__ == expected_rdo
        assert rdo[1].__dict__ == {
            "title": "Sequence coverage",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": ["Input sequence ID", "Chain ID", "Coverage"],
            "headers": ["Input sequence ID", "Chain ID", "Coverage"],
            "table_data": [
                ["pdb|5ni1|A C", "A", 100.0],
                ["pdb|5ni1|A C", "C", 100.0],
                ["pdb|5ni1|B D", "B", 100.0],
                ["pdb|5ni1|B D", "D", 100.0],
            ],
            "associated_data": ["ModelAngelo/job001/job001.cif"],
        }

    @patch.object(GemmiModelUtils, "__init__", gemmi_utils_init)
    @patch("pipeliner.jobs.relion.modelangelo_job.get_modelangelo_executable")
    def test_get_results_display_with_hmmer(self, mock_mangelo):
        mock_mangelo.return_value = "model_angelo"
        ma_job = new_job_of_type("modelangelo")
        ma_job.joboptions["fn_map"].value = "my_map.mrc"
        ma_job.joboptions["do_hmmer"].value = "Yes"
        ma_job.output_dir = "ModelAngelo/job001/"
        Path("ModelAngelo/job001/").mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "best_hits.csv", "ModelAngelo/job001/best_hits.csv"
        )
        rdo = ma_job.create_results_display()
        expected_rdo_mapmodel = {
            "maps": ["my_map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelAngelo/job001/job001.cif"],
            "title": "3D viewer: Generated atomic model",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["ModelAngelo/job001/job001.cif"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        expected_rdo_table = {
            "associated_data": ["ModelAngelo/job001/best_hits.csv"],
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": [
                "target_name",
                "query_name",
                "accession",
                "E-value",
                "score",
                "bias",
            ],
            "headers": [
                "target_name",
                "query_name",
                "accession",
                "E-value",
                "score",
                "bias",
            ],
            "start_collapsed": False,
            "table_data": [
                [
                    "lcl|NC_000913.3_prot_NP_414878.1_338",
                    "S",
                    "",
                    "0.0",
                    "1166.7205810546875",
                    "5.973388671875",
                ],
                [
                    "lcl|NC_000913.3_prot_YP_026199.1_3025",
                    "c",
                    "",
                    "3.0957836276054846e-08",
                    "31.999740600585938",
                    "14.323028564453125",
                ],
            ],
            "title": "HMMer best hits",
        }

        assert len(rdo) == 2
        assert rdo[0].__dict__ == expected_rdo_mapmodel
        assert rdo[1].__dict__ == expected_rdo_table


if __name__ == "__main__":
    unittest.main()
