#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import shlex
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
)
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.job_factory import new_job_of_type
from pipeliner.runjob_reader import RunJobFile


class ExtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Extract"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_basic(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_basic.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_job.star"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}."
                "star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_FOM(self):
        """Extract using the selection based on FOM added in relion 4.0"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_onFOM_job.star"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --minimum_pick_fom 0.1 --pipeline_control"
                " Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_rextract_wrongfile(self):
        jobfile = os.path.join(
            self.test_data, "JobFiles/Extract/extract_rextract_wrongfile.job"
        )
        params = RunJobFile(jobfile).get_all_options()
        job = new_job_of_type("relion.extract")
        for jobop in job.joboptions:
            job.joboptions[jobop].value = params[jobop]
        check = job.additional_joboption_validation()
        assert len(check) == 1
        assert check[0].message == (
            "Files for both extraction and re-extract were specified, use only one "
            "type"
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_rextract(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_rextract.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star"
                ".relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_preprocess_mpi --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job999/particles.star --part_dir Extract/job999/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 25"
                " --white_dust -1 --black_dust -1 --invert_contrast "
                "--rextract_on_this_one --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_rextract_recenter(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_rextract_recenter.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--recenter --recenter_x 10 --recenter_y 20 --recenter_z 30 "
                "--part_star Extract/job999/particles.star --part_dir Extract/job999/"
                " --extract --extract_size 256 --norm --bg_radius 125"
                " --white_dust -1 --black_dust -1 --invert_contrast"
                " --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_rextract_setbgdia(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_rextract_set_bgdia.job"),
            input_nodes={
                "Select/job005/micrographs_selected."
                "star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job999/particles.star --part_dir Extract/job999/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 24"
                " --white_dust -1 --black_dust -1 --invert_contrast --pipeline_control"
                " Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_helical.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion."
                "helixstartend",
            },
            output_nodes={
                "particles."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.helicalsegments",
                "helix_segments.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "helicalsegments",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_helical_relionstyle_name(self):
        """Test automatic conversion of ambiguous relion style name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_helical_relionstyle.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion."
                "helixstartend",
            },
            output_nodes={
                "particles."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.helicalsegments",
                "helix_segments.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "helicalsegments",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_helical_nosegs(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "extract_helical_nosegs.job"),
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion.helixstartend",
            },
            output_nodes={
                "particles."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.helicalsegments"
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job999/particles.star --part_dir "
                "Extract/job999/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --pipeline_control "
                "Extract/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} "
                "--fn_in Extract/job999/particles.star --block particles "
                "--clear_relion_success_file",
            ],
        )

    def test_extract_generate_display_data(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Extract"])

        project = PipelinerProject()
        job = project.get_job("Extract/job007/")
        dispobjs = job.create_results_display()

        expres = os.path.join(self.test_data, "ResultsFiles/extract_results.json")
        with open(expres, "r") as er:
            expected = json.load(er)

        assert dispobjs[0].__dict__ == expected

    def test_reextract_generate_display_data(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Extract"])

        project = PipelinerProject()
        job = project.get_job("Extract/job018/")
        dispobjs = job.create_results_display()

        expres = os.path.join(self.test_data, "ResultsFiles/reextract_results.json")
        with open(expres, "r") as er:
            expected = json.load(er)
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
