#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)


class ApplyBFactorJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor"},
            expected_commands=[
                "relion_image_handler --i test.mrc --bfactor -100.0"
                " --o BFactor/job999/test_bfact.mrc"
            ],
        )

    def test_get_commands_multiple(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_multi_job.star"
            ),
            input_nodes={
                "test.mrc": NODE_DENSITYMAP + ".mrc",
                "test2.mrc": NODE_DENSITYMAP + ".mrc",
                "test3.map": NODE_DENSITYMAP + ".mrc",
            },
            output_nodes={
                "test_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor",
                "test2_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor",
                "test3_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor",
            },
            expected_commands=[
                "relion_image_handler --i test.mrc --bfactor -100.0"
                " --o BFactor/job999/test_bfact.mrc",
                "relion_image_handler --i test2.mrc --bfactor -100.0"
                " --o BFactor/job999/test2_bfact.mrc",
                "relion_image_handler --i test3.map:mrc --bfactor -100.0"
                " --o BFactor/job999/test3_bfact.mrc",
            ],
        )

    def test_get_commands_mapext(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_mapext_job.star"
            ),
            input_nodes={"emd_2660_small.map": NODE_DENSITYMAP + ".mrc"},
            output_nodes={
                "emd_2660_small_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor"
            },
            expected_commands=[
                "relion_image_handler --i emd_2660_small.map:mrc --bfactor -100.0"
                " --o BFactor/job999/emd_2660_small_bfact.mrc"
            ],
        )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title=("Map slices: test_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test_bfact.mrc"],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "BFactor/job999/test_bfact.mrc: xy",
                    "BFactor/job999/test_bfact.mrc: xz",
                    "BFactor/job999/test_bfact.mrc: yz",
                ],
                img="BFactor/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title=("3D viewer: test_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test_bfact.mrc"],
                maps=["BFactor/job999/test_bfact.mrc"],
                maps_data="BFactor/job999/test_bfact.mrc",
            ),
            ResultsDisplayMontage(
                title=("Map slices: test2_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test2_bfact.mrc"],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "BFactor/job999/test2_bfact.mrc: xy",
                    "BFactor/job999/test2_bfact.mrc: xz",
                    "BFactor/job999/test2_bfact.mrc: yz",
                ],
                img="BFactor/job999/Thumbnails/slices_montage_001.png",
            ),
            ResultsDisplayMapModel(
                title=("3D viewer: test2_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test2_bfact.mrc"],
                maps=["BFactor/job999/test2_bfact.mrc"],
                maps_data="BFactor/job999/test2_bfact.mrc",
            ),
            ResultsDisplayMontage(
                title=("Map slices: test3_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test3_bfact.mrc"],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "BFactor/job999/test3_bfact.mrc: xy",
                    "BFactor/job999/test3_bfact.mrc: xz",
                    "BFactor/job999/test3_bfact.mrc: yz",
                ],
                img="BFactor/job999/Thumbnails/slices_montage_002.png",
            ),
            ResultsDisplayMapModel(
                title=("3D viewer: test3_bfact.mrc with applied B-factor -100.0 Å²"),
                associated_data=["BFactor/job999/test3_bfact.mrc"],
                maps=["BFactor/job999/test3_bfact.mrc"],
                maps_data="BFactor/job999/test3_bfact.mrc",
            ),
        ]

        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_multi_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={
                "test_bfact.mrc": mrc_file,
                "test2_bfact.mrc": mrc_file,
                "test3_bfact.mrc": mrc_file,
            },
        )


if __name__ == "__main__":
    unittest.main()
