#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from pathlib import Path

from pipeliner_tests import test_data

from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.nodes import (
    NODE_PROCESSDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARAMSDATA,
    NODE_LOGFILE,
)


class BayesianPolishTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Polish"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_bayesian_train(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_train.job"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "opt_params_all_groups.txt": f"{NODE_PARAMSDATA}.txt.relion.polish"
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --min_p 4000 --eval_frac 0.5 "
                "--align_frac 0.5 --params3 --j 16 --pipeline_control Polish/job999/"
            ],
        )

    def test_get_command_bayesian_train_relionstyle_jobname(self):
        """Check conversion of ambiguous jobname"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_train_relionstyle.job"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}."
                "star.relion",
            },
            output_nodes={
                "opt_params_all_groups.txt": f"{NODE_PARAMSDATA}.txt.relion.polish"
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --min_p 4000 --eval_frac 0.5 "
                "--align_frac 0.5 --params3 --j 16 --pipeline_control Polish/job999/"
            ],
        )

    def test_get_command_bayesian_polish_own_params(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_polish_ownparam.job"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.polish",
                "shiny.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.polish",
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --s_vel 0.2 --s_div 5000 "
                "--s_acc 2 --combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16"
                " --pipeline_control Polish/job999/"
            ],
        )

    def test_get_command_bayesian_polish_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_job.star"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.polish",
                "shiny.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.polish",
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --s_vel 0.2 --s_div 5000 "
                "--s_acc 2 --combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16"
                " --pipeline_control Polish/job999/"
            ],
        )

    def test_get_command_bayesian_polish_param_file(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_polish_paramfile.job"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star.relion"
                ".postprocess",
                "paramfile.txt": f"{NODE_PARAMSDATA}.txt.relion.polish",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.polish",
                "shiny.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.polish",
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --params_file paramfile.txt "
                "--combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --j 16 "
                "--pipeline_control Polish/job999/"
            ],
        )

    def test_get_command_bayesian_polish_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "bayesianpolish_polish_continue.job"),
            input_nodes={
                "Refine3D/job025/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job026/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
                "paramfile.txt": f"{NODE_PARAMSDATA}.txt.relion.polish",
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.polish",
                "shiny.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.polish",
            },
            expected_commands=[
                "relion_motion_refine --i Refine3D/job025/run_data.star"
                " --f PostProcess/job026/postprocess.star --corr_mic "
                "MotionCorr/job002/corrected_micrographs.star --first_frame 1 "
                "--last_frame -1 --o Polish/job999/ --params_file paramfile.txt "
                "--combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 "
                "--only_do_unfinished --j 16 --pipeline_control Polish/job999/"
            ],
        )

    def test_create_display_training_nofiles(self):
        pipeline = os.path.join(self.test_data, "Pipelines/relion40_tutorial.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/Polish/bayesianpolish_job.star"
        )
        os.makedirs("Polish/job027/")
        shutil.copy(pipeline, "default_pipeline.star")
        shutil.copy(jobstar, "Polish/job027/job.star")

        project = PipelinerProject()
        job = project.get_job("Polish/job027/")
        dispobjs = job.create_results_display()

        reason = "Error creating graph display object: [Errno 2]"
        assert dispobjs[0].__dict__["title"] == "Results pending..."
        assert dispobjs[0].__dict__["message"] == "Error creating results display"
        assert dispobjs[0].__dict__["reason"].startswith(reason)

    @staticmethod
    def test_create_display_training():
        get_relion_tutorial_data(relion_version=4, dirs=["Polish", "Refine3D"])

        project = PipelinerProject()
        job = project.get_job("Polish/job027/")
        dispobjs = job.create_results_display()

        expected_dict = {
            "title": "Trained Polishing parameters",
            "dobj_type": "table",
            "headers": [
                "Optics Group",
                "Sigma velocity (Å/dose)",
                "Sigma divergence (Å)",
                "Sigma for acceleration",
            ],
            "header_tooltips": [
                "Optics Group",
                "Sigma velocity (Å/dose)",
                "Sigma divergence (Å)",
                "Sigma for acceleration",
            ],
            "table_data": [["opticsGroup1", "0.4035", "1155", "2.715"]],
            "associated_data": ["Polish/job027/opt_params_all_groups.txt"],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[0].__dict__ == expected_dict

    def test_create_display_polishing(self):
        get_relion_tutorial_data(relion_version=4, dirs="Polish")

        project = PipelinerProject()
        job = project.get_job("Polish/job028/")
        dispobjs = job.create_results_display()

        bffile = os.path.join(
            self.test_data, "ResultsFiles/polish_particles_bfactor.json"
        )
        with open(bffile, "r") as bf:
            exp_bf = json.load(bf)
        assert dispobjs[0].__dict__ == exp_bf
        gfile = os.path.join(
            self.test_data, "ResultsFiles/polish_particles_guinier.json"
        )
        with open(gfile, "r") as gf:
            exp_guinier = json.load(gf)
        assert dispobjs[1].__dict__ == exp_guinier


if __name__ == "__main__":
    unittest.main()
