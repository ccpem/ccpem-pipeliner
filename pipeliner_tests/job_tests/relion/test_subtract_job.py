#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch, Mock, MagicMock
import numpy as np

import pipeliner.jobs.relion.subtract_job
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.utils import touch
from pipeliner.data_structure import (
    NODE_OPTIMISERDATA,
    NODE_MASK3D,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.results_display_objects import ResultsDisplayMontage


class SubtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Subtract"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}.star"
                ".relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ --new_box 420"
                " --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_relionstyle_name(self):
        """Check correct interpretation of ambiguous jobname"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_relionstyle.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}.star"
                ".relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ --new_box 420"
                " --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_diff_parts(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_diff_parts.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --data myother_particles.star"
                " --o Subtract/job999/ --new_box 420 "
                "--pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_mpi_default_value_is_2(self):
        job = pipeliner.jobs.relion.subtract_job.RelionSubtract()
        assert job.joboptions["nr_mpi"].value == 2
        assert job.joboptions["nr_mpi"].default_value == 2
        job.joboptions["fn_mask"].value = "mask.mrc"
        job.output_dir = "Subtract/job999/"

        touch("test_opt.star")
        job.joboptions["fn_opt"].value = "test_opt.star"
        exp_com = [
            "mpirun",
            "-n",
            "2",
            "/path/to/relion_particle_subtract_mpi",
            "--i",
            "test_opt.star",
            "--mask",
            "mask.mrc",
            "--o",
            "Subtract/job999/",
            "--recenter_on_mask",
            "--new_box",
            "-1",
        ]
        coms = job.get_commands()
        assert len(coms) == 1
        assert coms[0].com == exp_com

    def test_error_running_with_less_than_2_mpi(self):
        job = pipeliner.jobs.relion.subtract_job.RelionSubtract()
        job.joboptions["nr_mpi"].value = 1
        with self.assertRaises(ValueError):
            job.get_commands()

    def test_get_subtract_commands_revert(self):
        os.mkdir("Subtract")
        os.mkdir("Subtract/job999")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_revert.job"),
            input_nodes={
                "these_were_original.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={"original.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"},
            expected_commands=[
                "relion_particle_subtract --revert these_were_original.star"
                " --o Subtract/job999/ --pipeline_control Subtract/job999/"
            ],
        )

    def test_get_subtract_commands_revert_relionstyle_name(self):
        """Test conversion of relion style ambiguous job name"""
        os.mkdir("Subtract")
        os.mkdir("Subtract/job999")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_revert_relionstyle_job.star"),
            input_nodes={
                "Refine3D/job010/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={"original.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"},
            expected_commands=[
                "relion_particle_subtract --revert Refine3D/job010/run_data.star"
                " --o Subtract/job999/ --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_centonmask(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_centonmask.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ "
                "--recenter_on_mask --new_box 420 --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_centoncoords(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_centoncoords.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ "
                "--center_x 101 --center_y 102 --center_z 103 --new_box 420"
                " --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_additional_arg(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_addarg.job"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
                "myother_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_subtract_commands_additional_arg_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "subtract_addarg_job.star"),
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion",
                "MaskCreate/job501/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "particles_subtracted.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job999/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job999/"
            ],
        )

    @patch("pipeliner.display_tools.mrcfile.mmap", autospec=True)
    @patch("pipeliner.display_tools.mrc_thumbnail")
    def test_subtract_create_results_display(self, mockthumb, mockmmap):
        mockthumb.return_value = np.empty(shape=[2, 2])
        mock_mrc = MagicMock()
        mock_mrc.__enter__.return_value.data = np.empty(shape=[100, 100, 100])
        mockmmap.return_value = mock_mrc

        expected_rdo = ResultsDisplayMontage(
            title="Subtract/job999/particles_subtracted.star; 30/1158 images",
            xvalues=[
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
            ],
            yvalues=[
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            labels=[
                f"{n:06d}@Extract/job007/Movies/20170629_00021_frameImage.mrcs"
                for n in range(1, 31)
            ],
            associated_data=["Subtract/job999/particles_subtracted.star"],
            img="Subtract/job999/Thumbnails/montage_s000.png",
        )

        job_display_object_generation_test(
            jobfile=str(self.jobfiles / "subtract.job"),
            expected_display_objects=[expected_rdo],
            files_to_create={
                "particles_subtracted.star": str(
                    Path(self.test_data) / "particles.star"
                ),
            },
            print_dispobj=True,
        )


if __name__ == "__main__":
    unittest.main()
