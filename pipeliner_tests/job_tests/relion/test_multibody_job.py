#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_OPTIMISERDATA,
    NODE_RIGIDBODIES,
    NODE_LOGFILE,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner_tests.testing_tools import job_generate_commands_test


class MultiBodyTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star"),
            self.test_dir,
        )
        self.jobfiles = Path(self.test_data) / "JobFiles/MultiBody"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_basic(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody.job"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_blush(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_blush_job.star"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --blush --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_basic_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_relionstyle_job.star"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_flex_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted
        A warning shoulf be raised because the flexibility analysis will not
        be performed"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_relionstyle_analysis_job.star"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_job.star"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_reconstruct_subtracted(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_rec_sub.job"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_no_flexanalysis(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_noflex.job"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    def test_get_command_multibody_separate_oneigen(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_sep_eigen.job"),
            input_nodes={
                "MultiBody/job012/run_model.star": f"{NODE_OPTIMISERDATA}.star.relion"
                ".multibody",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "analyse_eval1_select_min-15_max10."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
                ".flexanalysis.eigenselected",
                "analyse_logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.flexanalysis",
            },
            expected_commands=[
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data"
                ".star --bodies bodyfile.star --o MultiBody/job999/analyse --do_maps"
                " --k 3 --select_eigenvalue 1 --select_eigenvalue_min -15.0"
                " --select_eigenvalue_max 10.0 --write_pca_projections "
                "--pipeline_control MultiBody/job999/"
            ],
        )

    def test_get_command_multibody_separate_oneigen_small(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_sep_eigen_small.job"),
            input_nodes={
                "MultiBody/job012/run_model.star": f"{NODE_OPTIMISERDATA}.star.relion"
                ".multibody",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "analyse_eval1_select_min-0p25_max0p5."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.flexanalysis.eigenselected",
                "analyse_logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.flexanalysis",
            },
            expected_commands=[
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data.star"
                " --bodies bodyfile.star --o MultiBody/job999/analyse --do_maps --k 3"
                " --select_eigenvalue 1 --select_eigenvalue_min -0.25"
                " --select_eigenvalue_max 0.5 --write_pca_projections "
                "--pipeline_control MultiBody/job999/"
            ],
            show_inputnodes=True,
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_multibody_different_sampling(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_diff_samp.job"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 2 --auto_local_healpix_order 2 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control "
                "MultiBody/job999/"
            ],
        )

    def test_get_command_multibody_noMPI(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "multibody_noMPI.job"),
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}."
                "star.relion.refine3d",
                "bodyfile.star": f"{NODE_RIGIDBODIES}.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "relion_refine "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job999/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 16 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --pipeline_control"
                " MultiBody/job999/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
