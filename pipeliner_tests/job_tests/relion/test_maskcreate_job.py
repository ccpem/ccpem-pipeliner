#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner.api.manage_project import PipelinerProject
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.data_structure import NODE_DENSITYMAP, NODE_MASK3D


class MaskCreateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/MaskCreate"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_mask_create_commands(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "maskcreate.job"),
            input_nodes={"Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"mask.mrc": f"{NODE_MASK3D}.mrc.relion"},
            expected_commands=[
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job999/mask.mrc --lowpass 15 --angpix -1 "
                "--ini_threshold 0.005 --extend_inimask 0 --width_soft_edge 6 --j 12 "
                "--pipeline_control MaskCreate/job999/"
            ],
        )

    def test_get_mask_create_commands_with_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "maskcreate_job.star"),
            input_nodes={"Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"mask.mrc": f"{NODE_MASK3D}.mrc.relion"},
            expected_commands=[
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job999/mask.mrc --lowpass 15 --angpix -1 --ini_threshold "
                "0.005 --extend_inimask 0 --width_soft_edge 6 --j 12 "
                "--pipeline_control MaskCreate/job999/"
            ],
        )

    def test_get_mask_create_commands_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "maskcreate_helical.job"),
            input_nodes={"Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"mask.mrc": f"{NODE_MASK3D}.mrc.relion"},
            expected_commands=[
                "relion_mask_create --i Import/job001/emd_3488.mrc --o "
                "MaskCreate/job999/mask.mrc --lowpass 15 --angpix -1 --ini_threshold "
                "0.005 --extend_inimask 0 --width_soft_edge 6 --helix --z_percentage"
                " 0.3 --j 12 --pipeline_control MaskCreate/job999/"
            ],
        )

    def test_maskcreate_generate_results(self):
        get_relion_tutorial_data(relion_version=4, dirs=["MaskCreate", "Refine3D"])

        project = PipelinerProject()
        job = project.get_job("MaskCreate/job020/")
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: MaskCreate/job020/mask.mrc",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "MaskCreate/job020/mask.mrc: xy",
                "MaskCreate/job020/mask.mrc: xz",
                "MaskCreate/job020/mask.mrc: yz",
            ],
            "associated_data": ["MaskCreate/job020/mask.mrc"],
            "img": "MaskCreate/job020/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "maps": [
                "MaskCreate/job020/mask.mrc",
                "Refine3D/job019/run_class001.mrc",
            ],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5, 1.0],
            "models": [],
            "title": "3D viewer: Created mask overlaid on input map",
            "maps_data": "Threshold: 0.005; Extend: 0 px; Soft edge: 6 px",
            "models_data": "",
            "associated_data": [
                "MaskCreate/job020/mask.mrc",
                "Refine3D/job019/run_class001.mrc",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
