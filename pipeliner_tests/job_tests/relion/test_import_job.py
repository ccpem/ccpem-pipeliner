#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import shlex
from dataclasses import asdict
from pathlib import Path
from pipeliner import job_manager
from pipeliner.results_display_objects import ResultsDisplayMontage
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
    job_display_object_generation_test,
    live_test,
    expected_warning,
    get_relion_tutorial_data,
    clean_starfile,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner import __version__ as pipe_vers
from pipeliner.utils import touch, get_job_script, get_python_command
from pipeliner.job_factory import active_job_from_proc, read_job, new_job_of_type
from pipeliner.api.api_utils import write_default_jobstar, job_default_parameters_dict
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.scripts.job_scripts.microscope_data_parsers import (
    parse_ebic_csv,
    check_file_is_ebic_csv,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_DENSITYMAP,
    NODE_IMAGE2D,
    NODE_MASK2D,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EMPIAR_DEPOSITION_COMMENT,
)
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmImaging,
    EmSampleSupport,
    EmSpecimen,
    EmSoftware,
)

OUTPUT_NODE_MOVIES = f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
OUTPUT_NODE_COORDS_DEP = f"{NODE_MICROGRAPHCOORDSGROUP}.star.deprecated_format"


class ImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_import_test(self):
        dirs = [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
        ]
        for d in dirs:
            os.makedirs(d, exist_ok=True)

        files = {
            "Import/job001/": os.path.join(
                self.test_data, "Pipelines/import_job_pipeline.star"
            ),
            "MotionCorr/job002/": os.path.join(
                self.test_data, "Pipelines/motioncorr_job_pipeline.star"
            ),
            "CtfFind/job003/": os.path.join(
                self.test_data, "Pipelines/ctffind_job_pipeline.star"
            ),
        }
        for f in files:
            shutil.copy(files[f], f + "job_pipeline.star")

        import_text = (
            " ++++ Executing new job on Fri May 29 11:25:32 2020"
            "\n++++ with the following command(s):"
            "\necho importing..."
            "\nrelion_star_loopheader rlnMicrographMovieName > "
            "Import/job001/movies.star ls -rt Micrographs/*.mrc >> "
            "Import/job001/movies.star \n++++"
        )

        with open("Import/job001/note.txt", "w") as note:
            note.write(import_text)

        mocorr_text = (
            " ++++ Executing new job on Fri Jul 10 13:59:07 2020"
            "\n++++ with the following command(s):"
            "`which relion_run_motioncorr` --i Import/job001/movies.star --o "
            "MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum -1 --use_own"
            "  --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1 "
            "--preexposure 0 --patch_x 1 --patch_y 1 --group_frames 3 "
            "--gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0  "
            "--pipeline_control MotionCorr/job002/\n++++"
        )

        with open("MotionCorr/job002/note.txt", "w") as note:
            note.write(mocorr_text)

        ctffind_text = (
            " ++++ Executing new job on Fri Jul 10 14:00:58 2020"
            "\n++++ with the following command(s):"
            "\n`which relion_run_ctffind` --i "
            "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
            " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
            "--FStep 500 --dAst 100 --use_gctf --gctf_exe /public/EM"
            '/Gctf/bin/Gctf --ignore_ctffind_params --gpu "" '
            "--pipeline_control CtfFind/job003/\n ++++"
        )

        with open("CtfFind/job003/note.txt", "w") as note:
            note.write(ctffind_text)

    def test_import_movies_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Import/import_movies_relionstyle.job"
            ),
            input_nodes={"mtf_k2_200kV.star": f"{NODE_MICROSCOPEDATA}.star.mtf"},
            output_nodes={"movies.star": OUTPUT_NODE_MOVIES},
            expected_commands=[
                "relion_import --do_movies --optics_group_name opticsGroup1 "
                "--optics_group_mtf mtf_k2_200kV.star --angpix 0.885 --kV 200"
                " --Cs 1.4 --Q0 0.1 --beamtilt_x 0 --beamtilt_y 0 --i Movies/*.tiff"
                " --odir Import/job999/ --ofile movies.star "
                "--pipeline_control Import/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "Import/job999/movies.star --block movies "
                "--clear_relion_success_file",
            ],
        )

    def test_import_movies_relionstyle_jobname_noMTF(self):
        """Automatic conversion of the ambigious relion style jobname"""
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data)
                / "JobFiles/Import/import_movies_relionstyle_nomtf.job"
            ),
            input_nodes={},
            output_nodes={"movies.star": OUTPUT_NODE_MOVIES},
            expected_commands=[
                "relion_import --do_movies --optics_group_name opticsGroup1 "
                "--angpix 0.885 --kV 200 --Cs 1.4 --Q0 0.1 --beamtilt_x 0 "
                "--beamtilt_y 0 --i Movies/*.tiff --odir Import/job999/ "
                "--ofile movies.star --pipeline_control Import/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "Import/job999/movies.star --block movies "
                "--clear_relion_success_file",
            ],
        )

    def test_import_other_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        with self.assertRaises(RuntimeError):
            job_generate_commands_test(
                jobfile=str(
                    Path(self.test_data)
                    / "JobFiles/Import/import_coords_relionstyle.job"
                ),
                input_nodes={},
                output_nodes={"coords_suffix.box": OUTPUT_NODE_COORDS_DEP},
                expected_commands=[
                    "relion_import --do_coordinates --i mycoords/*.box "
                    "--odir Import/job999/ --ofile coords_suffix.box "
                    "--pipeline_control Import/job999/"
                ],
            )

    def test_import_movies_prepare_deposition_object(self):

        self.setup_import_test()
        # set up the import dirs
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        write_default_jobstar("relion.import.movies", "Import/job001/job.star")

        # Read pipeline STAR file
        with ProjectGraph() as pipeline:
            import_job = active_job_from_proc(pipeline.process_list[0])

        with expected_warning(RuntimeWarning, "overflow"):
            results = [
                asdict(x)
                for x in import_job.prepare_deposition_data(depo_type="EMPIAR")
            ]

        expected_result = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629_000*_"
                "frameImage.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629_000*_"
                "frameImage.mrcs",
            },
        ]
        for i in expected_result:
            assert i in results
        for i in results:
            assert i in expected_result

    @live_test(job="relion.import.movies")
    def test_import_movies_running(self):
        # copy in fake files for the movie stacks
        mn = list(range(21, 32)) + [35, 36, 37] + [39, 40] + list(range(42, 50))
        get_relion_tutorial_data(relion_version=4, dirs="Import")
        os.makedirs("Movies")
        for n in mn:
            fn = f"Movies/20170629_{n:05d}_frameImage.tiff"
            shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), fn)

        project = PipelinerProject()
        job = project.get_job("Import/job001/")
        dispobjs = job.create_results_display()

        efile = os.path.join(self.test_data, "ResultsFiles/import_movies.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected
        assert os.path.isfile("Import/job001/Thumbnails/montage_f000.png")

    @live_test(job="relion.import.movies")
    def test_import_movies_running_with_scopedata(self):
        # copy in fake files for the movie stacks
        touch("scope_file.json")
        mn = list(range(21, 32)) + [35, 36, 37] + [39, 40] + list(range(42, 50))
        os.makedirs("Movies")
        for n in mn:
            fn = f"Movies/20170629_{n:05d}_frameImage.tiff"
            shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), fn)
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.movies")
        params["fn_in_raw"] = "Movies/*.tiff"
        params["microscope_data_file"] = "scope_file.json"
        job = proj.run_job(params, run_in_foreground=True)
        assert os.path.isfile("Import/job001/scope_file.json")
        assert "Import/job001/scope_file.json" in [x.name for x in job.output_nodes]
        lines = clean_starfile(os.path.join(job.output_dir, "job.star"))
        assert ["microscope_data_file", "scope_file.json"] in lines

    @live_test(job="relion.import.movies")
    def test_import_movies_running_with_scopedata_in_DoppioUploads(self):
        # copy in fake files for the movie stacks
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/scope_file.json")
        mn = list(range(21, 32)) + [35, 36, 37] + [39, 40] + list(range(42, 50))
        os.makedirs("Movies")
        for n in mn:
            fn = f"Movies/20170629_{n:05d}_frameImage.tiff"
            shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), fn)
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.movies")
        params["fn_in_raw"] = "Movies/*.tiff"
        params["microscope_data_file"] = "DoppioUploads/scope_file.json"
        job = proj.run_job(params, run_in_foreground=True)
        assert os.path.isfile("Import/job001/scope_file.json")
        assert "Import/job001/scope_file.json" in [x.name for x in job.output_nodes]
        lines = clean_starfile(os.path.join(job.output_dir, "job.star"))
        assert [
            "microscope_data_file",
            "Import/job001/InputFiles/scope_file.json",
        ] in lines

    @live_test(job="relion.import")
    def test_import_micrographs_running(self):
        get_relion_tutorial_data(relion_version=4, dirs="MotionCorr")
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "MotionCorr/job002/corrected_micrographs.star"
        params["node_type"] = "Micrographs STAR file (.star)"
        params["is_relion"] = "True"
        proj.run_job(jobinput=params, run_in_foreground=True)
        project = PipelinerProject()
        job = project.get_job("Import/job032/")
        dispobjs = job.create_results_display()

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_mics.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @live_test(job="relion.import")
    def test_import_micrographs_batch_running(self):
        get_relion_tutorial_data(relion_version=4, dirs="MotionCorr")
        shutil.copy(
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/corrected_micrographs2.star",
        )
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = (
            "MotionCorr/job002/corrected_micrographs.star:::"
            "MotionCorr/job002/corrected_micrographs2.star"
        )
        params["node_type"] = "Micrographs STAR file (.star)"
        params["is_relion"] = "True"
        proj.run_job(jobinput=params, run_in_foreground=True)

        project = PipelinerProject()
        job = project.get_job("Import/job032/")
        dispobjs = job.create_results_display()

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_mics.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @live_test(job="relion.import")
    def test_create_display_import_particles(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class2D", "Extract"])
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "Class2D/job008/run_it025_data.star"
        params["node_type"] = "Particles STAR file (.star)"
        params["is_relion"] = "Yes"
        job = proj.run_job(jobinput=params, run_in_foreground=True)
        dispobjs = job.create_results_display()

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_parts.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @live_test(job="relion.import")
    def test_import_2drefs_running(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Select", "Class2D"])
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "Select/job009/class_averages.star"
        params["node_type"] = "2D references STAR file (.star)"
        params["is_relion"] = "Yes"
        proj.run_job(jobinput=params, run_in_foreground=True)

        project = PipelinerProject()
        job = project.get_job("Import/job032/")
        dispobjs = job.create_results_display()

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_2drefs.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @live_test(job="relion.import")
    def test_import_3dref_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = NODE_DENSITYMAP
        params["is_relion"] = "False"
        proj.run_job(jobinput=params, run_in_foreground=True)

        project = PipelinerProject()
        job = project.get_job("Import/job001/")
        dispobjs = job.create_results_display()

        expected = [
            {
                "title": "Map slices: Import/job001/map.mrc",
                "start_collapsed": False,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0, 1, 2],
                "yvalues": [0, 0, 0],
                "labels": [
                    "Import/job001/map.mrc: xy",
                    "Import/job001/map.mrc: xz",
                    "Import/job001/map.mrc: yz",
                ],
                "associated_data": ["Import/job001/map.mrc"],
                "img": "Import/job001/Thumbnails/slices_montage_000.png",
            },
            {
                "associated_data": ["Import/job001/map.mrc"],
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": ["Import/job001/map.mrc"],
                "maps_colours": [],
                "maps_data": "Import/job001/map.mrc",
                "maps_opacity": [1.0],
                "models": [],
                "models_colours": [],
                "models_data": "",
                "start_collapsed": True,
                "title": "3D viewer: Import/job001/map.mrc",
            },
        ]

        assert [x.__dict__ for x in dispobjs] == expected

    @live_test(job="relion.import")
    def test_create_display_import_mask(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = NODE_MASK3D
        params["is_relion"] = "No"
        proj.run_job(jobinput=params, run_in_foreground=True)

        project = PipelinerProject()
        job = project.get_job("Import/job001/")
        dispobjs = job.create_results_display()
        expected = {
            "title": "Map slices: Import/job001/map.mrc",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "Import/job001/map.mrc: xy",
                "Import/job001/map.mrc: xz",
                "Import/job001/map.mrc: yz",
            ],
            "associated_data": ["Import/job001/map.mrc"],
            "img": "Import/job001/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[0].__dict__ == expected

    @live_test(job="relion.import")
    def test_import_halfmap_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = NODE_DENSITYMAP
        params["is_relion"] = "False"
        params["kwds"] = "halfmap"
        proj.run_job(jobinput=params, run_in_foreground=True)

        project = PipelinerProject()
        job = project.get_job("Import/job001/")
        dispobjs = job.create_results_display()

        expected = [
            {
                "title": "Map slices: Import/job001/map.mrc",
                "start_collapsed": False,
                "dobj_type": "montage",
                "flag": "",
                "xvalues": [0, 1, 2],
                "yvalues": [0, 0, 0],
                "labels": [
                    "Import/job001/map.mrc: xy",
                    "Import/job001/map.mrc: xz",
                    "Import/job001/map.mrc: yz",
                ],
                "associated_data": ["Import/job001/map.mrc"],
                "img": "Import/job001/Thumbnails/slices_montage_000.png",
            },
            {
                "title": "3D viewer: Import/job001/map.mrc",
                "start_collapsed": True,
                "dobj_type": "mapmodel",
                "flag": "",
                "maps": ["Import/job001/map.mrc"],
                "maps_opacity": [1.0],
                "maps_colours": [],
                "models_colours": [],
                "models": [],
                "maps_data": "Import/job001/map.mrc",
                "models_data": "",
                "associated_data": ["Import/job001/map.mrc"],
            },
        ]
        assert [x.__dict__ for x in dispobjs] == expected

    @live_test(job="relion.import")
    def test_import_mic_running(self):
        get_relion_tutorial_data(relion_version=4, dirs="MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = mic
        params["pipeliner_node_type"] = NODE_IMAGE2D
        params["is_relion"] = "False"
        job = proj.run_job(jobinput=params, run_in_foreground=True)
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Import/job032/20170629_00021_frameImage.mrc",
            "dobj_type": "image",
            "image_path": "Import/job032/Thumbnails/20170629_00021_frameImage.png",
            "image_desc": "Import/job032/20170629_00021_frameImage.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
            "start_collapsed": False,
            "flag": "",
        }
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile(f"Import/job032/{os.path.basename(mic)}")

    @live_test(job="relion.import")
    def test_import_coordsstar_running(self):
        get_relion_tutorial_data(relion_version=4, dirs="MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        shutil.copy(mic, "MotionCorr/job002/micrograph001.mrc")
        ap_file = os.path.join(self.test_data, "autopick.star")
        movs = "AutoPick/job006/Movies/"
        os.makedirs(movs)
        pfile = os.path.join(self.test_data, "micrograph001_autopick.star")
        shutil.copy(pfile, os.path.join(movs, "micrograph001_autopick.star"))
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = ap_file
        params["node_type"] = "RELION coordinates STAR file (.star)"
        params["is_relion"] = "True"
        job = proj.run_job(jobinput=params, run_in_foreground=True)
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job032/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/micrograph001.mrc: 242 particles",
            "associated_data": [
                "MotionCorr/job002/micrograph001.mrc",
                "AutoPick/job006/Movies/micrograph001_autopick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile("Import/job032/autopick.star")

    @live_test(job="relion.import")
    def test_import_model_running(self):
        model = os.path.join(self.test_data, "5me2_a.pdb")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = model
        params["pipeliner_node_type"] = NODE_ATOMCOORDS
        params["is_relion"] = "No"
        job = proj.run_job(jobinput=params, run_in_foreground=True)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": [],
            "dobj_type": "mapmodel",
            "maps_opacity": [],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "3D viewer: Import/job001/5me2_a.pdb",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["Import/job001/5me2_a.pdb"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert os.path.isfile("Import/job001/5me2_a.pdb")

    @live_test(job="relion.import")
    def test_import_2Dmask_running(self):
        get_relion_tutorial_data(relion_version=4, dirs="MotionCorr")
        fake_mask = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import")
        params["fn_in_other"] = fake_mask
        params["pipeliner_node_type"] = NODE_MASK2D
        params["is_relion"] = "No"
        job = proj.run_job(jobinput=params, run_in_foreground=True)
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Import/job032/20170629_00021_frameImage.mrc",
            "start_collapsed": False,
            "dobj_type": "image",
            "flag": "",
            "image_path": "Import/job032/Thumbnails/20170629_00021_frameImage.png",
            "image_desc": "Import/job032/20170629_00021_frameImage.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
        }

        assert os.path.isfile(dispobjs[0].image_path)

    def test_import_raw_coords_getcommand(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )

        os.makedirs("coords")
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))

        cd = "Import/job999/CoordinateFiles"
        job_generate_commands_test(
            jobfile=str(Path(self.test_data) / "JobFiles/Import/coords_files_job.star"),
            input_nodes={micsfile: f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"},
            output_nodes={
                "coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion"
            },
            expected_commands=[
                "mkdir Import/job999/CoordinateFiles",
                f"cp coords/20170629_00021_frameImage_coords.box {cd}",
                f"cp coords/20170629_00022_frameImage_coords.box {cd}",
                f"cp coords/20170629_00023_frameImage_coords.box {cd}",
                f"cp coords/20170629_00024_frameImage_coords.box {cd}",
                f"cp coords/20170629_00025_frameImage_coords.box {cd}",
                f"cp coords/20170629_00026_frameImage_coords.box {cd}",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('import/match_coords_to_mics.py')} -m "
                "CtfFind/job003/micrographs_ctf.star -c 'coords/*.box' -o "
                "Import/job999/",
            ],
        )

    def test_running_import_coords_with_suffix(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        os.makedirs("coords")
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))
            cfs.append(os.path.join("CoordinateFiles", cf))
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Import/coords_files_job.star"
            ),
            input_files=[(ctfdir, str(Path(self.test_data) / "micrographs_ctf.star"))],
            expected_outfiles=["coordinates.star"] + cfs,
        )

    def test_running_import_coords_with_nosuffix(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        os.makedirs("coords")
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage.box"
            touch(os.path.join("coords", cf))
            cfs.append(os.path.join("CoordinateFiles", cf))
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Import/coords_files_job.star"
            ),
            input_files=[(ctfdir, str(Path(self.test_data) / "micrographs_ctf.star"))],
            expected_outfiles=["coordinates.star"] + cfs,
        )

    def test_import_coords_files_create_display_box_format(self):
        ctfdir = "CtfFind/job003"
        movdir = "MotionCorr/job002/Movies"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        for dir_ in (ctfdir, "coords", movdir):
            os.makedirs(dir_)

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            os.path.join(movdir, "20170629_00021_frameImage.mrc"),
        )
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))
            with open(os.path.join("coords", cf), "w") as f:
                f.write(f"{n}  {n}\n" * n)
            cfs.append(os.path.join("CoordinateFiles", cf))

        with ProjectGraph(create_new=True) as pipeline:
            pipeline.job_counter = 4
            job = read_job(
                os.path.join(self.test_data, "JobFiles/Import/coords_files_job.star")
            )
            job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)
            dispobjs = job.create_results_display()

        exp_histo = {
            "title": "141 picked particles",
            "dobj_type": "histogram",
            "bins": [3, 3],
            "bin_edges": [21.0, 23.5, 26.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["Import/job004/coordinates.star"],
            "start_collapsed": False,
            "flag": "",
        }
        assert exp_histo == dispobjs[0].__dict__

        exp_image = {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job004/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 21 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "Import/job004/CoordinateFiles/20170629_00021_frameImage_coords.box",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert exp_image == dispobjs[1].__dict__

    def test_import_coords_files_create_display_star_format(self):
        ctfdir = "CtfFind/job003"
        movdir = "MotionCorr/job002/Movies"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        for dir_ in (ctfdir, "coords", movdir):
            os.makedirs(dir_)

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            os.path.join(movdir, "20170629_00021_frameImage.mrc"),
        )
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.star"
            touch(os.path.join("coords", cf))
            with open(os.path.join("coords", cf), "w") as f:
                f.write("data_\nloop_\n_rlnCoordinateX\n _rlnCoordinateY\n")
                f.write(f"{n}  {n}\n" * n)
            cfs.append(os.path.join("CoordinateFiles", cf))

        with ProjectGraph(create_new=True) as pipeline:
            pipeline.job_counter = 4
            job = read_job(
                os.path.join(
                    self.test_data, "JobFiles/Import/coords_files_star_job.star"
                )
            )
            job_manager.run_job(pipeline=pipeline, job=job, run_in_foreground=True)
            dispobjs = job.create_results_display()

        exp_histo = {
            "title": "141 picked particles",
            "dobj_type": "histogram",
            "bins": [3, 3],
            "bin_edges": [21.0, 23.5, 26.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["Import/job004/coordinates.star"],
            "start_collapsed": False,
            "flag": "",
        }
        assert exp_histo == dispobjs[0].__dict__

        exp_image = {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job004/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 21 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "Import/job004/CoordinateFiles/20170629_00021_frameImage_coords.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert exp_image == dispobjs[1].__dict__

    def test_reading_ebic_optics_file(self):
        rf = parse_ebic_csv(
            os.path.join(self.test_data, "Metadata/ebic_optics.csv"),
            output_dir="Import/job001/",
        )
        assert rf[0].__dict__ == {
            "id": None,
            "entry_id": "ENTRY_ID",
            "accelerating_voltage": None,
            "illumination_mode": None,
            "electron_source": None,
            "microscope_model": None,
            "imaging_mode": None,
            "sample_support_id": "JOBREF: Import/job001/",
            "specimen_holder_type": None,
            "specimen_holder_model": None,
            "details": None,
            "date": None,
            "mode": None,
            "nominal_cs": None,
            "nominal_defocus_min": -0.5,
            "nominal_defocus_max": -2.0,
            "tilt_angle_min": 0.0,
            "tilt_angle_max": 0.0,
            "nominal_magnification": None,
            "calibrated_magnification": None,
            "energy_filter": None,
            "energy_window": None,
            "temperature": None,
            "detector_distance": None,
            "recording_temperature_minimum": None,
            "recording_temperature_maximum": None,
        }
        assert rf[1].__dict__ == {
            "id": None,
            "entry_id": "ENTRY_ID",
            "film_material": None,
            "grid_type": "HoleyCarbon",
            "grid_material": None,
            "grid_mesh_size": None,
            "details": "Hole size: 1.4; Hole spacing 1.3",
        }
        assert rf[2].__dict__ == {
            "id": None,
            "experiment_id": 1,
            "concentration": None,
            "details": None,
            "embedding_applied": False,
            "shadowing_applied": False,
            "staining_applied": False,
            "vitrification_applied": True,
        }

    def test_check_ebic_file_format(self):
        file = os.path.join(self.test_data, "Metadata/ebic_optics.csv")
        assert check_file_is_ebic_csv(file)
        bad_file = os.path.join(self.test_data, "run_model.star")
        assert not check_file_is_ebic_csv(bad_file)

    def test_movies_prepare_onedep_deposition(self):
        job = new_job_of_type("relion.import.movies")
        job.joboptions["microscope_data_file"].value = os.path.join(
            self.test_data, "Metadata/ebic_optics.csv"
        )
        job.output_dir = "Import/job001/"
        results = job.prepare_deposition_data("ONEDEP")
        assert results == [
            EmImaging(
                id=None,
                entry_id="ENTRY_ID",
                accelerating_voltage=None,
                illumination_mode=None,
                electron_source=None,
                microscope_model=None,
                imaging_mode=None,
                sample_support_id="JOBREF: Import/job001/",
                specimen_holder_type=None,
                specimen_holder_model=None,
                details=None,
                date=None,
                mode=None,
                nominal_cs=None,
                nominal_defocus_min=-0.5,
                nominal_defocus_max=-2.0,
                tilt_angle_min=0.0,
                tilt_angle_max=0.0,
                nominal_magnification=None,
                calibrated_magnification=None,
                energy_filter=None,
                energy_window=None,
                temperature=None,
                detector_distance=None,
                recording_temperature_minimum=None,
                recording_temperature_maximum=None,
            ),
            EmSampleSupport(
                id=None,
                entry_id="ENTRY_ID",
                film_material=None,
                grid_type="HoleyCarbon",
                grid_material=None,
                grid_mesh_size=None,
                details="Hole size: 1.4; Hole spacing 1.3",
            ),
            EmSpecimen(
                id=None,
                experiment_id=1,
                concentration=None,
                details=None,
                embedding_applied=False,
                shadowing_applied=False,
                staining_applied=False,
                vitrification_applied=True,
            ),
            EmSoftware(
                id=None,
                category="IMAGE ACQUISITION",
                name="EPU",
                version="764-0 3.0.0",
                details=None,
            ),
            EmSoftware(
                id=None,
                category="OTHER",
                name="CCPEM-pipeliner/Doppio",
                version=pipe_vers,
                details="Project management and data analysis",
            ),
        ]

    def test_results_display_eer_placeholder(self):
        jobdir = "Import/job001/"
        os.makedirs(jobdir)
        shutil.copy(os.path.join(self.test_data, "movies.star"), f"{jobdir}movies.star")
        job = new_job_of_type("relion.import.movies")
        job.output_dir = jobdir
        job.joboptions["fn_in_raw"].value = "files/*.eer"
        dispobj = job.create_results_display()
        assert dispobj[0].__dict__ == {
            "associated_data": ["Import/job001/movies.star"],
            "display_data": "Thumbnail images for eer format data are not currently "
            "supported",
            "dobj_type": "text",
            "flag": "",
            "start_collapsed": False,
            "title": "EER display not supported",
        }

    def test_get_commands_relion_import_movies(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Import/import_movies_job.star"
            ),
            input_nodes={},
            output_nodes={
                "movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_import --do_movies --optics_group_name "
                "opticsGroup1 --angpix 1.4 --kV 300 --Cs 2.7 --Q0 0.1 --beamtilt_x 0.0 "
                "--beamtilt_y 0.0 --i 'Movies/*.tif' --odir Import/job999/ "
                "--ofile movies.star --pipeline_control Import/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "Import/job999/movies.star --block movies "
                "--clear_relion_success_file",
            ],
        )

    def test_get_commands_relion_import_micrographs(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Import/import_micrographs_job.star"
            ),
            input_nodes={},
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_import --do_micrographs --optics_group_name opticsGroup1"
                " --angpix 1.4 --kV 300 --Cs 2.7 --Q0 0.1 --beamtilt_x 0.0 "
                "--beamtilt_y 0.0 --i 'Micrographs/*.mrc' --odir Import/job999/ "
                "--ofile micrographs.star --pipeline_control Import/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "Import/job999/micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_commands_relion_import_coords(self):
        os.makedirs("CtfFind/job003")
        os.makedirs("coords")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            "CtfFind/job003/micrographs_ctf.star",
        )
        mics = DataStarFile("CtfFind/job003/micrographs_ctf.star")
        files = mics.loop_as_list("micrographs", ["_rlnMicrographName"])
        mv_coms = ["mkdir Import/job999/CoordinateFiles"]
        for f in files:
            ff = os.path.basename(f[0]).replace(".mrc", ".box")
            touch(os.path.join("coords", ff))
            mv_coms.append(f"cp coords/{ff} Import/job999/CoordinateFiles")

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Import/coords_files_job.star"
            ),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion"
            },
            expected_commands=mv_coms
            + [
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('import/match_coords_to_mics.py')} -m "
                "CtfFind/job003/micrographs_ctf.star -c 'coords/*.box' -o "
                "Import/job999/"
            ],
        )

    @staticmethod
    def test_identify_duplicate_file_names():
        job = new_job_of_type("relion.import")
        job.joboptions["fn_in_other"].value = (
            "in1/file.mrc:::in2/file.mrc:::in3/file.mrc:::in3/diff.mrc"
        )
        names = job.identify_duplicate_file_names()
        assert names == {
            "in1/file.mrc": "file.mrc",
            "in2/file.mrc": "file_001.mrc",
            "in3/file.mrc": "file_002.mrc",
            "in3/diff.mrc": "diff.mrc",
        }

    @staticmethod
    def test_identify_duplicate_file_names_new_name_duplicates():
        job = new_job_of_type("relion.import")
        job.joboptions["fn_in_other"].value = (
            "in1/file.mrc:::in2/file.mrc:::in3/file_001.mrc:::in3/diff.mrc"
        )
        names = job.identify_duplicate_file_names()
        assert names == {
            "in1/file.mrc": "file.mrc",
            "in2/file.mrc": "file_001.mrc",
            "in3/file_001.mrc": "file_001_001.mrc",
            "in3/diff.mrc": "diff.mrc",
        }

    def test_get_commands_with_duplicate_filenames_nonrelion(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Import/pipeliner_import_batch_duplicates_job.star",
            ),
            input_nodes={},
            output_nodes={
                "test_image.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
                "test_image_001.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
                "different.mrc": f"{NODE_DENSITYMAP}.mrc.test.fancy.pretty",
            },
            expected_commands=[
                "cp d1/test_image.mrc Import/job999/test_image.mrc",
                "cp d2/test_image.mrc Import/job999/test_image_001.mrc",
                "cp d2/different.mrc Import/job999/different.mrc",
            ],
        )

    def test_duplicate_filenames_relion_import(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Import/relion_import_batch_particles_job.star"
            ),
            input_nodes={},
            output_nodes={
                "parts.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_001.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_002.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            expected_commands=[
                "relion_import --do_particles --i d1/parts.star --odir Import/job999/ "
                "--ofile parts.star --pipeline_control Import/job999/",
                "relion_import --do_particles --i d2/parts.star --odir Import/job999/ "
                "--ofile parts_001.star --pipeline_control Import/job999/",
                "relion_import --do_particles --i d3/parts.star --odir Import/job999/ "
                "--ofile parts_002.star --pipeline_control Import/job999/",
            ],
        )

    def test_results_display_movie_import(self):
        # setup
        td = Path(self.test_data)
        movs_file = td / "movies.star"
        movs = DataStarFile(str(movs_file)).column_as_list(
            "movies", "_rlnMicrographMovieName"
        )
        Path("Movies").mkdir()
        for f in movs:
            shutil.copy(td / "tiff_stack.tiff", f)

        # expected results
        rd = ResultsDisplayMontage(
            title="Import/job999/movies.star; 2/24 images",
            flag="",
            xvalues=[0, 1],
            yvalues=[0, 0],
            labels=[
                "Movies/20170629_00021_frameImage.tiff",
                "Movies/20170629_00022_frameImage.tiff",
            ],
            associated_data=["Import/job999/movies.star"],
            img="Import/job999/Thumbnails/montage_f000.png",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(str(td / "JobFiles/Import/import_movies_job.star")),
            files_to_create={"movies.star": str(movs_file)},
            expected_display_objects=[rd],
        )

    def test_results_display_mic_import(self):
        # setup
        td = Path(self.test_data)
        mics_file = td / "mics.star"
        mics = DataStarFile(str(mics_file)).column_as_list(
            "micrographs", "_rlnMicrographName"
        )
        Path("Movies").mkdir()
        for f in mics:
            shutil.copy(td / "single.mrc", f)

        # expected results
        rd = ResultsDisplayMontage(
            title="Import/job999/micrographs.star; 2/24 images",
            flag="",
            xvalues=[0, 1],
            yvalues=[0, 0],
            labels=[
                "Movies/20170629_00021_frameImage.mrc",
                "Movies/20170629_00022_frameImage.mrc",
            ],
            associated_data=["Import/job999/micrographs.star"],
            img="Import/job999/Thumbnails/montage_f000.png",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(str(td / "JobFiles/Import/import_mics_job.star")),
            files_to_create={"micrographs.star": str(mics_file)},
            expected_display_objects=[rd],
        )


if __name__ == "__main__":
    unittest.main()
