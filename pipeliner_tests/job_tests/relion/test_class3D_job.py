#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch
import shlex
from pathlib import Path

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_factory import read_job
from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
    clean_starfile,
)

from pipeliner.job_factory import active_job_from_proc
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticles,
    EMPIAR_DEPOSITION_COMMENT,
)
from pipeliner.scripts.job_scripts.relion import make_class3d_multiref_starfile
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_OPTIMISERDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_DENSITYMAPGROUPMETADATA,
    NODE_MASK3D,
)
from pipeliner.utils import get_python_command


class Class3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Class3D"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = read_job(os.path.join(self.test_data, "JobFiles/Class3D/class3D.job"))
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = read_job(os.path.join(self.test_data, "JobFiles/Class3D/class3D.job"))
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_class3D_basic(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_blush(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_blush_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --blush --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_fast_subsets(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_fss_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --fast_subsets --K 4 --flatten_solvent "
                "--zero_mask --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_with_symmetry(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_c4sym.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                f"c4sym",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.c4sym",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C4 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_multi(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_multi.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference4.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference5.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job999/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class005.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{make_class3d_multiref_starfile.__file__} "
                "--output_dir Class3D/job999/ --references"
                " InitialModel/job015/run_it150_class001_symD2.mrc reference2.mrc"
                " reference3.mrc reference4.mrc reference5.mrc",
                "relion_refine --i Select/job014/particles.star --o Class3D/job999/run"
                " --ref Class3D/job999/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 5 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/",
            ],
        )
        os.makedirs("Class3D/job999")
        make_class3d_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job999/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "reference2.mrc",
                "reference3.mrc",
                "reference4.mrc",
                "reference5.mrc",
            ]
        )
        wrote_refs = clean_starfile("Class3D/job999/references.star")
        for line in [
            ["data_references"],
            ["loop_"],
            ["_rlnReferenceImage", "#1"],
            ["InitialModel/job015/run_it150_class001_symD2.mrc"],
            ["reference2.mrc"],
            ["reference3.mrc"],
            ["reference4.mrc"],
            ["reference5.mrc"],
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_multi_with_DoppioUploads(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_multi_UF.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Class3D/job999/InputFiles/reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference4.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference5.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job999/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}"
                ".star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class005.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{make_class3d_multiref_starfile.__file__}"
                " --output_dir Class3D/job999/"
                " --references InitialModel/job015/run_it150_class001_symD2.mrc"
                " Class3D/job999/InputFiles/reference2.mrc reference3.mrc"
                " reference4.mrc reference5.mrc",
                "relion_refine --i Select/job014/particles.star --o Class3D/job999/run"
                " --ref Class3D/job999/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 5 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5"
                " --offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Class3D/job999/",
            ],
        )
        os.makedirs("Class3D/job999")
        make_class3d_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job999/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "Class3D/job999/InputFiles/reference2.mrc",
                "reference3.mrc",
                "reference4.mrc",
                "reference5.mrc",
            ]
        )
        wrote_refs = clean_starfile("Class3D/job999/references.star")
        for line in [
            ["data_references"],
            ["loop_"],
            ["_rlnReferenceImage", "#1"],
            ["InitialModel/job015/run_it150_class001_symD2.mrc"],
            ["Class3D/job999/InputFiles/reference2.mrc"],
            ["reference3.mrc"],
            ["reference4.mrc"],
            ["reference5.mrc"],
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_mask(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_mask.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_2masks(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_2masks.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": f"{NODE_MASK3D}.mrc",
                "MaskCreate/job013/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--solvent_mask2 MaskCreate/job013/mask.mrc "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_mask_absgrey(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_mask_noabsgrey.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --firstiter_cc"
                " --ini_high 50.0 --dont_combine_weights_via_disc --preread_images"
                " --pool 30 --pad 2 --ctf --iter "
                "25 --tau2_fudge 4 --particle_diameter 200 --K 4 --flatten_solvent "
                "--zero_mask --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_noctfref(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_noctfcorref.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_ctfintact(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_ctfintact.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --ctf_intact_first_peak"
                " --iter 25 --tau2_fudge 4 --particle_diameter 200 --K 4 "
                "--flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --offset_range 5 --offset_step 2.0"
                " --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_nozeromask(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_nozero.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_hireslim(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_hireslim.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --strict_highres_exp 12.0 --oversampling 1 --healpix_order 2 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j 6 "
                "--gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_dfferent_sampling(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_diffsamp.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 3 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_local(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_local.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --sigma_ang 1.66667 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_coarse(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_coarse.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --allow_coarser_sampling --sym C1 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                "helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_helical_multiref(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_multi.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job999/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                "helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{make_class3d_multiref_starfile.__file__}"
                " --output_dir Class3D/job999/ --references"
                " InitialModel/job015/run_it150_class001_symD2.mrc reference2.mrc"
                " reference3.mrc",
                "relion_refine --i"
                " Select/job014/particles.star --o Class3D/job999/run --ref"
                " Class3D/job999/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 3 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step"
                " 2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0"
                " --helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed"
                " --sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j"
                " 6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/",
            ],
        )
        os.makedirs("Class3D/job999")
        make_class3d_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job999/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "reference2.mrc",
                "reference3.mrc",
            ]
        )
        wrote_refs = clean_starfile("Class3D/job999/references.star")
        for line in [
            ["data_references"],
            ["loop_"],
            ["_rlnReferenceImage", "#1"],
            ["InitialModel/job015/run_it150_class001_symD2.mrc"],
            ["reference2.mrc"],
            ["reference3.mrc"],
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_helical_multiref_with_DoppioUploads(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_multi_UF.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Class3D/job999/InputFiles/reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job999/InputFiles/reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job999/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                "helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{make_class3d_multiref_starfile.__file__}"
                " --output_dir Class3D/job999/ --references"
                " InitialModel/job015/run_it150_class001_symD2.mrc"
                " Class3D/job999/InputFiles/reference2.mrc"
                " Class3D/job999/InputFiles/reference3.mrc",
                "relion_refine --i"
                " Select/job014/particles.star --o Class3D/job999/run"
                " --ref Class3D/job999/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 3 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step 2.0"
                " --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0"
                " --helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed"
                " --sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j 6"
                " --gpu 4:5:6:7 --pipeline_control Class3D/job999/",
            ],
        )
        os.makedirs("Class3D/job999")
        make_class3d_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job999/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "Class3D/job999/InputFiles/reference2.mrc",
                "Class3D/job999/InputFiles/reference3.mrc",
            ]
        )
        wrote_refs = clean_starfile("Class3D/job999/references.star")
        for line in [
            ["data_references"],
            ["loop_"],
            ["_rlnReferenceImage", "#1"],
            ["InitialModel/job015/run_it150_class001_symD2.mrc"],
            ["Class3D/job999/InputFiles/reference2.mrc"],
            ["Class3D/job999/InputFiles/reference3.mrc"],
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_helical_noTPF(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_noTPF.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                f"helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_helical_nosym(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_nosym.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                "helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --ignore_helical_symmetry "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_helical_search(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_search.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                f"helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_get_command_class3D_helical_search_relionstyle_name(self):
        """Test automatuic conversion of ambiguous relion style name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "class3D_helical_search_relionstyle.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}."
                "star.relion.helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class3d."
                "helical",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class3d.helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job999/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job999/"
            ],
        )

    def test_create_display_cl3d(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class3D"])

        project = PipelinerProject()
        job = project.get_job("Class3D/job016/")
        dispobjs = job.create_results_display()
        dist_results = os.path.join(
            self.test_data, "ResultsFiles/class3d_dist_results.json"
        )
        with open(dist_results, "r") as dr:
            expected = [json.load(dr)]
        expected.extend(
            [
                {
                    "title": "Map slices",
                    "start_collapsed": False,
                    "dobj_type": "gallery",
                    "flag": "",
                    "images": "Class3D/job016/Thumbnails/imagelist.json",
                    "labels": [
                        "class 4 - 9.85 Å - 4747 particles: xy",
                        "class 4 - 9.85 Å - 4747 particles: xz",
                        "class 4 - 9.85 Å - 4747 particles: yz",
                        "class 2 - 20.6 Å - 736 particles: xy",
                        "class 2 - 20.6 Å - 736 particles: xz",
                        "class 2 - 20.6 Å - 736 particles: yz",
                        "class 3 - 25.17 Å - 173 particles: xy",
                        "class 3 - 25.17 Å - 173 particles: xz",
                        "class 3 - 25.17 Å - 173 particles: yz",
                        "class 1 - 25.17 Å - 154 particles: xy",
                        "class 1 - 25.17 Å - 154 particles: xz",
                        "class 1 - 25.17 Å - 154 particles: yz",
                    ],
                    "associated_data": [
                        "Class3D/job016/run_it025_class001.mrc",
                        "Class3D/job016/run_it025_class002.mrc",
                        "Class3D/job016/run_it025_class003.mrc",
                        "Class3D/job016/run_it025_class004.mrc",
                    ],
                    "associated_nodes": [
                        {
                            "name": "Class3D/job016/run_it025_optimiser.star",
                            "type": "OptimiserData.star.relion.class3d",
                        }
                    ],
                },
                {
                    "maps": ["Class3D/job016/run_it025_class001.mrc"],
                    "dobj_type": "mapmodel",
                    "maps_opacity": [1.0],
                    "models": [],
                    "title": "3D viewer: class 1 - 25.17 Å - 154 particles",
                    "maps_data": "Class3D/job016/run_it025_class001.mrc",
                    "models_data": "",
                    "associated_data": ["Class3D/job016/run_it025_class001.mrc"],
                    "start_collapsed": True,
                    "flag": "",
                    "maps_colours": [],
                    "models_colours": [],
                },
                {
                    "maps": ["Class3D/job016/run_it025_class002.mrc"],
                    "dobj_type": "mapmodel",
                    "maps_opacity": [1.0],
                    "models": [],
                    "title": "3D viewer: class 2 - 20.6 Å - 736 particles",
                    "maps_data": "Class3D/job016/run_it025_class002.mrc",
                    "models_data": "",
                    "associated_data": ["Class3D/job016/run_it025_class002.mrc"],
                    "start_collapsed": True,
                    "flag": "",
                    "maps_colours": [],
                    "models_colours": [],
                },
                {
                    "maps": ["Class3D/job016/run_it025_class003.mrc"],
                    "dobj_type": "mapmodel",
                    "maps_opacity": [1.0],
                    "models": [],
                    "title": "3D viewer: class 3 - 25.17 Å - 173 particles",
                    "maps_data": "Class3D/job016/run_it025_class003.mrc",
                    "models_data": "",
                    "associated_data": ["Class3D/job016/run_it025_class003.mrc"],
                    "start_collapsed": True,
                    "flag": "",
                    "maps_colours": [],
                    "models_colours": [],
                },
                {
                    "maps": ["Class3D/job016/run_it025_class004.mrc"],
                    "dobj_type": "mapmodel",
                    "maps_opacity": [1.0],
                    "models": [],
                    "title": "3D viewer: class 4 - 9.85 Å - 4747 particles",
                    "maps_data": "Class3D/job016/run_it025_class004.mrc",
                    "models_data": "",
                    "associated_data": ["Class3D/job016/run_it025_class004.mrc"],
                    "start_collapsed": True,
                    "flag": "",
                    "maps_colours": [],
                    "models_colours": [],
                },
            ]
        )

        do_dicts = [x.__dict__ for x in dispobjs]
        for i in do_dicts:
            assert i in expected, i
        for i in expected:
            assert i in do_dicts, i

    def test_get_onedep_data(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Class3D", "Extract"])
        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Class3D/job016/")
        job = active_job_from_proc(proc)
        depobjs = job.prepare_deposition_data("EMPIAR")
        expected_depobjs = [
            EmpiarParticles(
                name="Particle images",
                directory="Extract/job012/Movies",
                category="('T5', '')",
                header_format="('T2', '')",
                data_format="('OT', '16-bit float')",
                num_images_or_tilt_series=24,
                frames_per_image=1,
                voxel_type="('OT', '16-bit float')",
                pixel_width=0.885,
                pixel_height=0.885,
                details="5812 total particles; Voltage 1.4; Spherical aberration "
                "1.4; Image data in file: Class3D/job016/run_it025_data."
                f"star; {EMPIAR_DEPOSITION_COMMENT}",
                image_width=64,
                image_height=64,
                micrographs_file_pattern="Extract/job012/Movies/20170629_000*_"
                "frameImage.mrcs",
                picked_particles_file_pattern="Class3D/job016/run_it025_data.star",
            ),
        ]

        assert depobjs[0] == expected_depobjs[0]

    @staticmethod
    def test_make_reference_file_script():
        os.makedirs("Class3D/job001")
        make_class3d_multiref_starfile.main(
            ["--output_dir", "Class3D/job001/", "--references", "ref1.mrc", "ref2.mrc"]
        )
        reflines = [
            "data_references",
            "loop_",
            "_rlnReferenceImage #1",
            "ref1.mrc",
            "ref2.mrc",
        ]
        with open("Class3D/job001/references.star") as rf:
            wrote = [x.strip() for x in rf.readlines()]
        for line in reflines:
            assert line in wrote


if __name__ == "__main__":
    unittest.main()
