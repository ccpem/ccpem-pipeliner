#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import shlex
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.api.manage_project import PipelinerProject
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.utils import touch, get_python_command
from pipeliner.scripts.job_scripts.relion import (
    make_localres_halfmap_symlinks as make_halfmap_symlinks,
)
from pipeliner.user_settings import get_resmap_executable
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_IMAGE3D,
    NODE_MASK3D,
    NODE_MICROSCOPEDATA,
)


class LocalResTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/LocalRes"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        # make some dirs for testing sym link making
        os.mkdir("Refine3D")
        os.mkdir("Refine3D/job029")
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            "Refine3D/job029/run_half1_class001_unfil.mrc",
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            "Refine3D/job029/run_half2_class001_unfil.mrc",
        )
        os.mkdir("LocalRes")
        os.mkdir("LocalRes/job999")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_relion_localres(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_relion.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc"
                ".halfmap",
                "mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "histogram.pdf": f"{NODE_LOGFILE}.pdf.relion.localres",
                "relion_locres_filtered.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "localresfiltered",
                "relion_locres.mrc": f"{NODE_IMAGE3D}.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 /path/to/relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job999/relion --angpix 1.244 --adhoc_bfac -30 "
                "--mask mask.mrc --pipeline_control LocalRes/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_relion_localres_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_relion_job.star"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}."
                "mrc.halfmap",
                "mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "histogram.pdf": f"{NODE_LOGFILE}.pdf.relion.localres",
                "relion_locres_filtered.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "localresfiltered",
                "relion_locres.mrc": f"{NODE_IMAGE3D}.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 /path/to/relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job999/relion --angpix 1.244 --adhoc_bfac -30 "
                "--mask mask.mrc --pipeline_control LocalRes/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_relion_localres_mtf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_relion_mtf.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "my_MTF_file.star": f"{NODE_MICROSCOPEDATA}.star.mtf",
                "mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "histogram.pdf": f"{NODE_LOGFILE}.pdf.relion.localres",
                "relion_locres_filtered.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "localresfiltered",
                "relion_locres.mrc": f"{NODE_IMAGE3D}.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 /path/to/relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job999/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --mask mask.mrc --pipeline_control "
                "LocalRes/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_relion_localres_mtf_relionstyle_name(self):
        """Test Automatic concersion of ambiguous relion style job name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_relion_mtf_relionstyle.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc"
                ".halfmap",
                "my_MTF_file.star": f"{NODE_MICROSCOPEDATA}.star.mtf",
                "mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "histogram.pdf": f"{NODE_LOGFILE}.pdf.relion.localres",
                "relion_locres_filtered.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "localresfiltered",
                "relion_locres.mrc": f"{NODE_IMAGE3D}.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 /path/to/relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job999/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --mask mask.mrc --pipeline_control "
                "LocalRes/job999/"
            ],
        )

    def test_relion_localres_nompi(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_relion_nompi.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}."
                "mrc.halfmap",
                "mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "histogram.pdf": f"{NODE_LOGFILE}.pdf.relion.localres",
                "relion_locres_filtered.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "localresfiltered",
                "relion_locres.mrc": f"{NODE_IMAGE3D}.mrc.relion.localresmap",
            },
            expected_commands=[
                "relion_postprocess --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job999/relion --angpix 1.244 --adhoc_bfac -30 "
                "--mask mask.mrc --pipeline_control LocalRes/job999/"
            ],
        )

    def test_resmap_localres(self):
        # have to copy in actual files because symlinks are made
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_resmap.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc"
                ".halfmap",
                "MaskCreate/job400/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={"half1_resmap.mrc": f"{NODE_IMAGE3D}.mrc.resmap.localresmap"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_halfmap_symlinks.__file__} "
                "--hm1 Refine3D/job029/run_half1_class001_unfil.mrc --hm2 "
                "Refine3D/job029/run_half2_class001_unfil.mrc --output_dir "
                "LocalRes/job999/",
                f"{get_resmap_executable()} --maskVol=MaskCreate/job400/mask.mrc "
                "--noguiSplit LocalRes/job999/half1.mrc LocalRes/job999/half2.mrc "
                "--vxSize=1.244 --pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1",
            ],
        )

    def test_resmap_localres_relionstyle_jobname(self):
        """test automatic conversion of ambiguous relion style jobname"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "localres_resmap_relionstyle.job"),
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}."
                "mrc.halfmap",
                "MaskCreate/job400/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={"half1_resmap.mrc": f"{NODE_IMAGE3D}.mrc.resmap.localresmap"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {make_halfmap_symlinks.__file__} "
                "--hm1 Refine3D/job029/run_half1_class001_unfil.mrc --hm2 "
                "Refine3D/job029/run_half2_class001_unfil.mrc --output_dir "
                "LocalRes/job999/",
                f"{get_resmap_executable()} --maskVol=MaskCreate/job400/mask.mrc "
                "--noguiSplit LocalRes/job999/half1.mrc LocalRes/job999/half2.mrc "
                "--vxSize=1.244 --pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1",
            ],
        )

    def test_create_display_locres_relion(self):
        get_relion_tutorial_data(relion_version=4, dirs="LocalRes")

        project = PipelinerProject()
        job = project.get_job("LocalRes/job031/")
        dispobjs = job.create_results_display()

        histfile = os.path.join(self.test_data, "ResultsFiles/local_res.json")
        with open(histfile, "r") as hf:
            exp_hist = json.load(hf)
        exp_montage = {
            "associated_data": ["LocalRes/job031/relion_locres_filtered.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "LocalRes/job031/Thumbnails/slices_montage_000.png",
            "labels": [
                "LocalRes/job031/relion_locres_filtered.mrc: xy",
                "LocalRes/job031/relion_locres_filtered.mrc: xz",
                "LocalRes/job031/relion_locres_filtered.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: Local resolution filtered map",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        exp_map = {
            "maps": ["LocalRes/job031/relion_locres_filtered.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Local resolution filtered map",
            "maps_data": "LocalRes/job031/relion_locres_filtered.mrc",
            "models_data": "",
            "associated_data": ["LocalRes/job031/relion_locres_filtered.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == exp_hist
        assert dispobjs[1].__dict__ == exp_montage
        assert dispobjs[2].__dict__ == exp_map

    @staticmethod
    def test_script_makes_symlinks_properly():
        lrdir = "LocalRes/job002"
        refdir = "Refine3D/job001"
        os.makedirs(lrdir)
        os.makedirs(refdir)
        hm_files = ["hm1.mrc", "hm2.mrc"]
        for f in hm_files:
            touch(os.path.join(refdir, f))
        make_halfmap_symlinks.main(
            [
                "--hm1",
                os.path.join(refdir, "hm1.mrc"),
                "--hm2",
                os.path.join(refdir, "hm2.mrc"),
                "--output_dir",
                lrdir,
            ]
        )
        for n in [1, 2]:
            link = os.path.join(lrdir, f"half{n}.mrc")
            assert os.path.islink(link)
            realpath = os.path.abspath(os.path.join(refdir, f"hm{n}.mrc"))
            assert os.path.realpath(os.path.join(lrdir, f"half{n}.mrc")) == realpath


if __name__ == "__main__":
    unittest.main()
