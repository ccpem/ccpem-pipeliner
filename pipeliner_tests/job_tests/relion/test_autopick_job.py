#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_factory import new_job_of_type
from pipeliner.utils import touch
from pipeliner.nodes import (
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_IMAGE2DSTACK,
    NODE_LOGFILE,
    NODE_MLMODEL,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)


class AutoPickTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/AutoPick"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_helical.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --max_stddev_noise -1 "
                "--min_distance 7.62 --helix --helical_tube_outer_diameter 200 "
                "--helical_tube_kappa_max 0.1 --helical_tube_length_min -1 "
                "--gpu 0:1:2:3 --this_one_is_helical --pipeline_control"
                " AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref_amyloid(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_amyloid.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --max_stddev_noise -1"
                " --min_distance 7.62 --helix --amyloid --helical_tube_outer_diameter"
                " 200 --helical_tube_kappa_max 0.1 --helical_tube_length_min -1 "
                "--gpu 0:1:2:3 --this_one_is_helical --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_continue.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --only_do_unfinished "
                "--pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_3Dref(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_3dref.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job999/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1 --gpu "
                "0:1 --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_3Dref_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_3dref_helical.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job999/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1"
                " --min_distance -1.0 --helix --helical_tube_outer_diameter 200 "
                "--helical_tube_kappa_max 0.1 --helical_tube_length_min -1 --gpu "
                "0:1 --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_3Dref_relionstylename(self):
        """Name automatically converted from relionstyle relion.autopick to pipeliner
        style relion.autopick.ref3d"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_3dref.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job999/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1 --gpu "
                "0:1 --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_LoG(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_LoG.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job999/ --pickname autopick --LoG --LoG_diam_min 150"
                " --LoG_diam_max 180 --shrink 0 --lowpass 20 --LoG_adjust_threshold 0"
                " --LoG_upper_threshold 5 --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_LoG_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_LoG_helical.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job999/ "
                "--pickname autopick --LoG --LoG_diam_min 150 --LoG_diam_max 180"
                " --shrink 0 --lowpass 20 --LoG_adjust_threshold 0 "
                "--LoG_upper_threshold 5 --min_distance -1.0 --helix --amyloid "
                "--helical_tube_outer_diameter 200 --helical_tube_kappa_max 0.1 "
                "--helical_tube_length_min -1 --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref_writeFOM(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_writeFOM.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --write_fom_maps "
                "--pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_2Dref_writeFOM_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_continue_writeFOM.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --write_fom_maps"
                " --pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_readFOM(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_readFOM.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --read_fom_maps "
                "--pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_readFOM_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_2dref_continue_readFOM.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --read_fom_maps "
                "--pipeline_control AutoPick/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 /path/to/relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0.05 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_additional_args(self, mock_topaz):
        """train topaz adding additional arguments"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_train_addargs_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={
                "model_epoch10.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --topaz_args "
                "additional_args_here --gpu '' --pipeline_control AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_command_relion5(self, mock_topaz):
        """train topaz adding additional arguments"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "relion_python_topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_train_addargs_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={
                "model_epoch10.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job999/ --pickname autopick"
                " --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --topaz_args "
                "additional_args_here --gpu '' --pipeline_control AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_diameter(self, mock_topaz):
        """Pick with topaz manually entered particle diameter"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_pick_diameter_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --particle_diameter 150 --topaz_extract"
                " --topaz_model TOPAZ_MODEL/is/here.sav --gpu ''"
                " --pipeline_control AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_diameter_helical(self, mock_topaz):
        """Pick with topaz manually entered particle diameter"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "autopick_topaz_pick_diameter_helical_job.star"
            ),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --particle_diameter 150 --topaz_extract "
                "--topaz_model TOPAZ_MODEL/is/here.sav  --gpu '' --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_no_model(self, mock_topaz):
        """Pick with topaz without user defined model"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_pick_nomodel_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --particle_diameter 150 --topaz_extract"
                " --gpu '' --pipeline_control"
                " AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_advanced_helical(self, mock_topaz):
        """Pick with topaz without user defined model"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "relion_python_topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_pick_advanced_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "relion_python_topaz --particle_diameter 150 --topaz_extract"
                " --helix --topaz_threshold -5.0 --helical_tube_length_min -1.0 --gpu "
                "'' --pipeline_control AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_no_model_relion_5(self, mock_topaz):
        """Pick with topaz without user defined model"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "relion_python_topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_pick_nomodel_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "relion_python_topaz --particle_diameter 150 --topaz_extract"
                " --gpu '' --pipeline_control"
                " AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_pick_with_nr_parts(self, mock_topaz):
        """Pick with topaz manually entered expected nr of parts"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_pick_nrparts_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --topaz_nr_particles 1003 --topaz_extract"
                " --topaz_model TOPAZ_MODEL/is/here.sav --gpu '' --pipeline_control"
                " AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_train_with_coords(self, mock_topaz):
        """train topaz using coordinates"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_train_coords_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={
                "model_epoch10.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --gpu '' --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.autopick_job.ExternalProgram")
    def test_get_command_topaz_train_with_parts(self, mock_topaz):
        """train topaz using particles"""
        mock_instance = mock_topaz.return_value
        mock_instance.exe_path = "topaz"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "autopick_topaz_train_parts_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job010/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
            },
            output_nodes={
                "input_training_coords.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
                "model_epoch10.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job999/ --pickname autopick --topaz_exe "
                "topaz --topaz_train --topaz_train_parts "
                "Select/job010/run_data.star --gpu '' --pipeline_control "
                "AutoPick/job999/"
            ],
        )

    @staticmethod
    def test_autopick_generate_display_data():
        get_relion_tutorial_data(
            relion_version=4, dirs=["AutoPick", "CtfFind", "MotionCorr"]
        )

        project = PipelinerProject()
        job = project.get_job("AutoPick/job011/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "AutoPick/job011/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
            ": 409 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "AutoPick/job011/Movies/20170629_00021_frameImage_autopick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[1].__dict__ == {
            "title": "9191 picked particles",
            "dobj_type": "histogram",
            "bins": [8, 8, 4, 4],
            "bin_edges": [348.0, 369.0, 390.0, 411.0, 432.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["AutoPick/job011/autopick.star"],
            "start_collapsed": False,
            "flag": "",
        }

    @staticmethod
    def test_results_display_topaz_train():
        get_relion_tutorial_data(
            relion_version=4, dirs=["AutoPick", "CtfFind", "MotionCorr", "Select"]
        )

        project = PipelinerProject()
        job = project.get_job("AutoPick/job010/")
        dispobjs = job.create_results_display()

        expected = {
            "title": "Topaz training complete",
            "dobj_type": "text",
            "display_data": "Topaz training jobs have no graphical output",
            "associated_data": ["AutoPick/job010/topaz_train.txt"],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[0].__dict__ == expected

    @staticmethod
    def test_topaz_train_create_post_run_output_nodes():
        ttjob = new_job_of_type("relion.autopick.topaz.train")
        odir = "AutoPick/job001/"
        ttjob.output_dir = odir
        os.makedirs(odir)
        for i in range(11):
            touch(f"{odir}model_epoch{i}.sav")
        assert ttjob.output_nodes == []
        ttjob.create_post_run_output_nodes()
        assert len(ttjob.output_nodes) == 1
        assert ttjob.output_nodes[0].name == f"{odir}model_epoch10.sav"

    @staticmethod
    def test_topaz_train_create_post_run_output_nodes_helical():
        ttjob = new_job_of_type("relion.autopick.topaz.train")
        odir = "AutoPick/job002/"
        ttjob.output_dir = odir
        os.makedirs(odir)
        for i in range(10):
            touch(f"{odir}model_epoch{i}.sav")
        assert ttjob.output_nodes == []
        ttjob.create_post_run_output_nodes()
        assert len(ttjob.output_nodes) == 1
        assert ttjob.output_nodes[0].name == f"{odir}model_epoch9.sav"

    def test_topaz_train_cleanup(self):
        ttjob = new_job_of_type("relion.autopick.topaz.train")
        odir = "AutoPick/job002/"
        ttjob.output_dir = odir
        os.makedirs(odir)
        files = []
        for i in range(10):
            f = f"{odir}model_epoch{i}.sav"
            touch(f)
            files.append(f)

        cleanup_lists = ttjob.prepare_clean_up_lists()
        assert cleanup_lists[0] == files[:-1]
        assert cleanup_lists[1] == []


if __name__ == "__main__":
    unittest.main()
