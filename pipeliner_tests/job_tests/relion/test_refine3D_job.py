#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
import tempfile
import unittest
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_factory import new_job_of_type
from pipeliner.data_structure import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
    NODE_DENSITYMAP,
    NODE_MASK3D,
)


class Refine3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Refine3D"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_basic(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256.mrc": f"{NODE_DENSITYMAP}."
                "mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i "
                "Extract/job018/particles.star --o Refine3D/job999/run --auto_refine"
                " --split_random_halves --ref Class3D/job016/run_it025_class001"
                "_box256.mrc --firstiter_cc --ini_high 50 --dont_combine_weights_via"
                "_disc --preread_images --pool 30 --pad 2 --ctf "
                "--particle_diameter 200 --flatten_solvent --zero_mask "
                "--oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 "
                "--norm --scale --j 6 --gpu 4:5:6:7 --pipeline_control "
                "Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_job.star"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_blush(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_blush_job.star"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256.mrc": f"{NODE_DENSITYMAP}."
                "mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--blush --ref Class3D/job016/run_it025_class001_box256.mrc "
                "--firstiter_cc --ini_high 50 --dont_combine_weights_via_disc "
                "--preread_images --pool 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_samplingcheck(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_sampling.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 4 --auto_local_healpix_order 7 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_mask(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_mask.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "MaskCreate/job200/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_mask_solvflat(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_mask_solvflat.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "MaskCreate/job200/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --solvent_mask MaskCreate/job2"
                "00/mask.mrc --solvent_correct_fsc --oversampling 1 --healpix_order 2 "
                "--auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D"
                "2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_absgrey(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_absgrey.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_ctf1stpeak(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_ctf1stpeak.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --ctf_intact_fi"
                "rst_peak --particle_diameter 200 --flatten_solvent --zero_mask --over"
                "sampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_ra"
                "nge 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_finerfaster(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_finerfaster.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d"
                ".d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --auto_ignore_angles --auto_resol_angles "
                "--ctf --particle_diameter"
                " 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym "
                "D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_helical(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helicalsegments",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_helical_relionstyle_jobname(self):
        """Make sure an ambiguous relion style job name is converted"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical_relionstyle.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_noprior(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical_noprior.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helicalsegments",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --j 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_helical_noHsym(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical_noHsym.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helicalsegments",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 "
                "--ignore_helical_symmetry --sigma_tilt 5 --sigma_psi 3.33333 --sigma_"
                "rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_helical_search(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical_search.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helicalsegments",
                "Class3D/job016/run_it025_class001_box256."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --helical_symmetry_search --helical_twist_"
                "min 20 --helical_twist_max 30 --helical_twist_inistep 1 --helical_ris"
                "e_min 3 --helical_rise_max 4.5 --helical_rise_inistep 0.25 --sigma_ti"
                "lt 5 --sigma_psi 3.33333 --sigma_rot"
                " 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_helical_sigmarot(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_helical_sigmarot.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": f"{NODE_DENSITYMAP}."
                "mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d."
                "helical",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d."
                "helical",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.helical.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.helical."
                "d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_sigma_distance 5 --helical_keep_tilt_prior_fixed --j"
                " 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job999/"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_2masks(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "refine3D_2masks.job"),
            input_nodes={
                "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "Class3D/job016/run_it025_class001_box256.mrc": f"{NODE_DENSITYMAP}."
                "mrc",
                "MaskCreate/job200/mask.mrc": f"{NODE_MASK3D}.mrc",
                "MaskCreate/job013/mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "refine3d.d2sym.halfmap",
                "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job999/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --solvent_mask2 MaskCreate/job013/mask.mrc --pipeline_control "
                "Refine3D/job999/"
            ],
        )

    def test_get_command_refine3D_continue_nofile(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "refine3D_continue_nofile.job"),
                input_nodes={},
                output_nodes={},
                expected_commands="",
            )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_refine3D_continue(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "refine3D_continue_nofile.job"),
                input_nodes={
                    "Extract/job018/particles.star": f"{NODE_PARTICLEGROUPMETADATA}."
                    "star.relion",
                    "Class3D/job016/run_it025_class001_"
                    "box256.mrc": f"{NODE_DENSITYMAP}.mrc",
                },
                output_nodes={
                    "run_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d.d2sym",
                    "run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                    "refine3d.d2sym.halfmap",
                    "run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                    "refine3d.d2sym.halfmap",
                    "run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                    "refine3d",
                    "run_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
                },
                expected_commands=[
                    "mpirun -n 5 /path/to/relion_refine_mpi --continue "
                    "run_ct01_it003_optimiser.star --o Refine3D/job011/run_ct3 "
                    "--dont_combine_weights_via_disc --preread_images  --pool 3 --pad "
                    "2  --particle_diameter 200 --j 6 --gpu 4:5:6:7"
                    "--pipeline_control Refine3D/job999/"
                ],
            )

    def test_refine3d_generate_results(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Refine3D"])

        project = PipelinerProject()
        job = project.get_job("Refine3D/job019/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Map slices: Final reconstruction - 3.89 Å",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "Refine3D/job019/run_class001.mrc: xy",
                "Refine3D/job019/run_class001.mrc: xz",
                "Refine3D/job019/run_class001.mrc: yz",
            ],
            "associated_data": ["Refine3D/job019/run_class001.mrc"],
            "img": "Refine3D/job019/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "maps": ["Refine3D/job019/run_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Final reconstruction - 3.89 Å",
            "maps_data": "Refine3D/job019/run_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_class001.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    def test_refine3d_generate_results_not_finished(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Refine3D"])
        os.remove("Refine3D/job019/run_class001.mrc")

        project = PipelinerProject()
        job = project.get_job("Refine3D/job019/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": (
                "Map slices: Intermediate reconstruction - iteration 11 - 7.59 Å"
            ),
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "Refine3D/job019/run_it011_half1_class001.mrc: xy",
                "Refine3D/job019/run_it011_half1_class001.mrc: xz",
                "Refine3D/job019/run_it011_half1_class001.mrc: yz",
            ],
            "associated_data": ["Refine3D/job019/run_it011_half1_class001.mrc"],
            "img": "Refine3D/job019/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "maps": ["Refine3D/job019/run_it011_half1_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Intermediate reconstruction - iteration 11 - 7.59 Å",
            "maps_data": "Refine3D/job019/run_it011_half1_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_it011_half1_class001.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @staticmethod
    def test_mpi_validation_at_least_3():
        job = new_job_of_type("relion.refine3d")
        job.joboptions["nr_mpi"].value = 2
        errs = job.additional_joboption_validation()
        assert len(errs) == 1
        assert errs[0].message == "Refine jobs need at least 2 mpi to run"

    @staticmethod
    def test_mpi_validation_must_be_odd():
        job = new_job_of_type("relion.refine3d")
        job.joboptions["nr_mpi"].value = 6
        errs = job.additional_joboption_validation()
        assert len(errs) == 1
        assert errs[0].message == "An odd number of MPIs must be used"

    @staticmethod
    def test_helical_rise_validation_minmax():
        job = new_job_of_type("relion.refine3d.helical")
        job.joboptions["nr_mpi"].value = 3
        job.joboptions["do_local_search_helical_symmetry"].value = "Yes"
        job.joboptions["helical_rise_min"].value = 2.0
        job.joboptions["helical_rise_max"].value = 1.0
        job.joboptions["helical_twist_min"].value = 3.9
        job.joboptions["helical_twist_max"].value = 5.2
        errs = job.additional_joboption_validation()
        assert len(errs) == 1
        assert errs[0].message == "Rise min > rise max"

    @staticmethod
    def test_helical_twist_validation_minmax():
        job = new_job_of_type("relion.refine3d.helical")
        job.joboptions["nr_mpi"].value = 3
        job.joboptions["do_local_search_helical_symmetry"].value = "Yes"
        job.joboptions["helical_rise_min"].value = 3.9
        job.joboptions["helical_rise_max"].value = 5.2
        job.joboptions["helical_twist_min"].value = 2.0
        job.joboptions["helical_twist_max"].value = 1.0
        errs = job.additional_joboption_validation()
        assert len(errs) == 1
        assert errs[0].message == "Twist min > twist max"

    @staticmethod
    def test_helical_step_validation():
        job = new_job_of_type("relion.refine3d.helical")
        job.joboptions["nr_mpi"].value = 3
        job.joboptions["do_local_search_helical_symmetry"].value = "Yes"
        job.joboptions["helical_rise_min"].value = 3.9
        job.joboptions["helical_rise_max"].value = 5.2
        job.joboptions["helical_rise_inistep"].value = 4.0
        job.joboptions["helical_twist_min"].value = 3.9
        job.joboptions["helical_twist_max"].value = 5.2
        job.joboptions["helical_twist_inistep"].value = 4.0
        errs = job.additional_joboption_validation()
        assert len(errs) == 2
        assert [x.message == "Step is bigger than total range" for x in errs]

    @staticmethod
    def test_helical_validation_turned_off():
        job = new_job_of_type("relion.refine3d.helical")
        job.joboptions["nr_mpi"].value = 3
        job.joboptions["do_local_search_helical_symmetry"].value = "No"
        job.joboptions["helical_rise_min"].value = 3.9
        job.joboptions["helical_rise_max"].value = 5.2
        job.joboptions["helical_rise_inistep"].value = 4.0
        job.joboptions["helical_twist_min"].value = 3.9
        job.joboptions["helical_twist_max"].value = 5.2
        job.joboptions["helical_twist_inistep"].value = 4.0
        errs = job.additional_joboption_validation()
        assert len(errs) == 0


if __name__ == "__main__":
    unittest.main()
