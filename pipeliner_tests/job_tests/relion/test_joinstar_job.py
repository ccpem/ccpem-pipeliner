#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    live_test,
    job_generate_commands_test,
    expected_warning,
)
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.api.api_utils import job_default_parameters_dict, write_default_jobstar
from pipeliner.job_factory import active_job_from_proc
from pipeliner.project_graph import ProjectGraph
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EMPIAR_DEPOSITION_COMMENT,
)


class JoinStarTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/JoinStar"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_join_parts(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_parts.job"),
            input_nodes={
                "parts_file1.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file2.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file3.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file4.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i parts_file1.star parts_file2.star"
                " parts_file3.star parts_file4.star --check_duplicates rlnImageName"
                " --o JoinStar/job999/join_particles.star"
                " --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_parts_relionstyle_name(self):
        """Automatic conversion of ambiguous relion style names"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_parts_relionstyle_job.star"),
            input_nodes={
                "Parts1.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "Parts2.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i Parts1.star Parts2.star"
                " --check_duplicates rlnImageName"
                " --o JoinStar/job999/join_particles.star"
                " --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_parts_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_parts_job.star"),
            input_nodes={
                "parts_file1.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file2.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file3.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "parts_file4.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i parts_file1.star parts_file2.star"
                " parts_file3.star parts_file4.star --check_duplicates rlnImageName"
                " --o JoinStar/job999/join_particles.star"
                " --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_mics(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_mics.job"),
            input_nodes={
                "micrographs_file1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "micrographs_file2.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "micrographs_file3.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "micrographs_file4.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_mics.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i micrographs_file1.star"
                " micrographs_file2.star micrographs_file3.star micrographs_file4.star"
                " --check_duplicates rlnMicrographName --o"
                " JoinStar/job999/join_mics.star --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_mics_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style jobname"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_mics_relionstyle_job.star"),
            input_nodes={
                "Mics1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Mics2.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_mics.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i Mics1.star Mics2.star"
                " --check_duplicates rlnMicrographName --o"
                " JoinStar/job999/join_mics.star --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_movies(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_movies.job"),
            input_nodes={
                "movies_file1.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file2.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file3.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file4.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i movies_file1.star movies_file2.star"
                " movies_file3.star movies_file4.star --check_duplicates"
                " rlnMicrographMovieName --o JoinStar/job999/join_movies.star"
                " --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_movies_relionstyle_jobname(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_movies_relionstyle_job.star"),
            input_nodes={
                "Movies1.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "Movies2.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i Movies1.star Movies2.star"
                " --check_duplicates rlnMicrographMovieName --o"
                " JoinStar/job999/join_movies.star --pipeline_control JoinStar/job999/"
            ],
        )

    def test_join_movies_additional_args(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "joinstar_movies_extra_args.job"),
            input_nodes={
                "movies_file1.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file2.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file3.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "movies_file4.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "join_movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --combine --i movies_file1.star movies_file2.star"
                " movies_file3.star movies_file4.star --check_duplicates"
                " rlnMicrographMovieName --o JoinStar/job999/join_movies.star"
                " --Here_are_the_extra_arguments Blah"
                " --pipeline_control JoinStar/job999/"
            ],
        )

    @live_test(job="relion.joinstar.movies")
    def test_join_movies_prepare_deposition_object(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")
        write_default_jobstar("relion.import.movies", "Import/job001/job.star")

        mf1 = os.path.join(self.test_dir, "Movies.star")
        mf2 = os.path.join(self.test_dir, "Movies2.star")
        header = [
            "# version 30001",
            "data_optics",
            "loop",
            "_rlnOpticsGroupName #1",
            "_rlnOpticsGroup #2",
            "_rlnMicrographOriginalPixelSize #3",
            "_rlnVoltage #4",
            "_rlnSphericalAberration #5",
            "_rlnAmplitudeContrast #6",
            "opticsGroup1            1     0.885000   200.000000"
            "   1.400000     0.100000",
            "# version 30001",
            "data_movies",
            "loop_",
            "_rlnMicrographMovieName #1",
            "_rlnOpticsGroup #2",
        ]
        for f in (mf1, mf2):
            with open(f, "w") as fout:
                for line in header:
                    fout.write(line + "/n")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
            with open(mf1, "a") as fout:
                fout.write(target + "\n")
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
            with open(mf2, "a") as fout:
                fout.write(target + "\n")

        # run the joinstar job
        proj = PipelinerProject()
        params = job_default_parameters_dict("relion.joinstar.movies")
        params["fn_movs"] = "mf1.star:::mf2.star"
        proj.run_job(params)

        with ProjectGraph() as pipeline:
            joinstar_job = active_job_from_proc(pipeline.process_list[0])

        with expected_warning(RuntimeWarning, "overflow"):
            print(joinstar_job.prepare_deposition_data("EMPIAR"))
            results = [
                x.__dict__ for x in joinstar_job.prepare_deposition_data("EMPIAR")
            ]

        expected_result = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629_000*_frame"
                "Image.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629_000*_"
                "frameImage.mrcs",
            },
        ]
        for i in expected_result:
            assert i in results
        for i in results:
            assert i in expected_result


if __name__ == "__main__":
    unittest.main()
