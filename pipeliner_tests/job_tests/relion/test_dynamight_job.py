#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path

from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
    NODE_PROCESSDATA,
)
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.utils import touch, get_python_command, get_job_script
from pipeliner.job_factory import new_job_of_type
from pipeliner.jobs.relion.dynamight_job import DynamightContinuousHeterogeneity
from pipeliner.job_options import JobOptionValidationResult
from pipeliner.results_display_objects import ResultsDisplayText, ResultsDisplayMapModel
from pipeliner.scripts.job_scripts.dynamight import dynamight_setup


class DynamightJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.dynamight_test = Path(test_data.__file__).parent / "JobFiles/DynaMight"
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_motion_estimation_only(self):
        job_generate_commands_test(
            jobfile=str(self.dynamight_test / "motion_only_job.star"),
            input_nodes={
                "Refine3D/job001/map.mrc": NODE_DENSITYMAP + ".mrc",
                "Refine3D/job001/data.star": NODE_PARTICLEGROUPMETADATA + ".star",
            },
            output_nodes={
                "forward_deformations/checkpoints/checkpoint_final."
                "pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint"
            },
            expected_commands=[
                "dynamight optimize-deformations --refinement-star-file "
                "Refine3D/job001/data.star --output-directory DynaMight/job999/ "
                "--initial-model Refine3D/job001/map.mrc --n-gaussians 10000 "
                "--regularization-factor 1 --n-threads 1 --gpu-id 0",
            ],
        )

    def test_get_commands_inverse_only(self):
        fd_dir = Path("DynaMight/job001/forward_deformations/checkpoints")
        fd_dir.mkdir(parents=True)
        touch(str(fd_dir / "checkpoint_final.pth"))
        job_generate_commands_test(
            jobfile=str(self.dynamight_test / "inverse_only_job.star"),
            input_nodes={
                "DynaMight/job001/forward_deformations/checkpoints/"
                "checkpoint_final.pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint"
            },
            output_nodes={
                "forward_deformations/checkpoints/checkpoint_final."
                "pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('dynamight/dynamight_setup.py')} "
                "--output_dir DynaMight/job999/ --checkpoint_file "
                "DynaMight/job001/forward_deformations/checkpoints/checkpoint_"
                "final.pth",
                "dynamight optimize-inverse-deformations DynaMight/job999/ --n-epochs "
                "200 --checkpoint-file DynaMight/job999/forward_deformations/"
                "checkpoints/checkpoint_final.pth --gpu-id 0",
            ],
        )

    def test_get_commands_reconstruction_only(self):
        fd_dir = Path("DynaMight/job001/forward_deformations/checkpoints")
        fd_dir.mkdir(parents=True)
        touch(str(fd_dir / "checkpoint_final.pth"))
        job_generate_commands_test(
            jobfile=str(self.dynamight_test / "rec_only_job.star"),
            input_nodes={
                "DynaMight/job001/forward_deformations/checkpoints/"
                "checkpoint_final.pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint"
            },
            output_nodes={
                "forward_deformations/checkpoints/checkpoint_final."
                "pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint",
                "backprojection/map_half1.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
                "backprojection/map_half2.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('dynamight/dynamight_setup.py')} "
                "--output_dir DynaMight/job999/ --checkpoint_file DynaMight/job001/"
                "forward_deformations/checkpoints/checkpoint_final.pth "
                "--do_backproject",
                "dynamight deformable-backprojection DynaMight/job999/ --batch-size 10 "
                "--checkpoint-file DynaMight/job999/forward_deformations/checkpoints/"
                "checkpoint_final.pth --gpu-id 0",
            ],
        )

    def test_get_commands_all_tasks(self):
        job_generate_commands_test(
            jobfile=str(self.dynamight_test / "all_tasks_job.star"),
            input_nodes={
                "Refine3D/job001/map.mrc": NODE_DENSITYMAP + ".mrc",
                "Refine3D/job001/data.star": NODE_PARTICLEGROUPMETADATA + ".star",
            },
            output_nodes={
                "forward_deformations/checkpoints/checkpoint_final."
                "pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint",
                "backprojection/map_half1.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
                "backprojection/map_half2.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
            },
            expected_commands=[
                "dynamight optimize-deformations --refinement-star-file "
                "Refine3D/job001/data.star --output-directory DynaMight/job999/ "
                "--initial-model Refine3D/job001/map.mrc --n-gaussians 10000 "
                "--regularization-factor 1 --n-threads 1 --gpu-id 0",
                "dynamight optimize-inverse-deformations DynaMight/job999/ --n-epochs "
                "200 --checkpoint-file DynaMight/job999/forward_deformations/"
                "checkpoints/checkpoint_final.pth --gpu-id 0",
                "dynamight deformable-backprojection DynaMight/job999/ --batch-size 10 "
                "--checkpoint-file DynaMight/job999/forward_deformations/checkpoints/"
                "checkpoint_final.pth --gpu-id 0",
            ],
        )

    def test_get_commands_inverse_and_rec(self):
        fd_dir = Path("DynaMight/job001/forward_deformations/checkpoints")
        fd_dir.mkdir(parents=True)
        touch(str(fd_dir / "checkpoint_final.pth"))
        job_generate_commands_test(
            jobfile=str(self.dynamight_test / "inverse_and_rec_job.star"),
            input_nodes={
                "DynaMight/job001/forward_deformations/checkpoints/checkpoint_final."
                "pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint",
            },
            output_nodes={
                "forward_deformations/checkpoints/checkpoint_final"
                ".pth": NODE_PROCESSDATA + ".pth.dynamight.checkpoint",
                "backprojection/map_half1.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
                "backprojection/map_half2.mrc": NODE_DENSITYMAP
                + ".mrc.relion.halfmap.dynamight",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                ""
                f"{get_job_script('dynamight/dynamight_setup.py')} "
                "--output_dir DynaMight/job999/ --checkpoint_file "
                "DynaMight/job001/forward_deformations/checkpoints/checkpoint_"
                "final.pth",
                "dynamight optimize-inverse-deformations DynaMight/job999/ --n-epochs "
                "200 --checkpoint-file DynaMight/job999/forward_deformations/"
                "checkpoints/checkpoint_final.pth --gpu-id 0",
                "dynamight deformable-backprojection DynaMight/job999/ --batch-size 10 "
                "--checkpoint-file DynaMight/job999/forward_deformations/checkpoints/"
                "checkpoint_final.pth --gpu-id 0",
            ],
            show_inputnodes=True,
        )

    def test_get_commands_no_inverse_error(self):
        job = new_job_of_type(DynamightContinuousHeterogeneity.PROCESS_NAME)
        job.joboptions["do_est_motion"].value = "Yes"
        job.joboptions["do_inverse"].value = "No"
        job.joboptions["do_reconstruct"].value = "Yes"
        valerr = job.additional_joboption_validation()

        assert len(valerr) == 1
        assert type(valerr[0]) is JobOptionValidationResult
        assert valerr[0].type == "error"
        assert valerr[0].message == (
            "Inverse deformations must be calculated if estimated motions are used "
            "for a consensus map"
        )

    @staticmethod
    def make_visdisp():
        pth = Path(
            "DynaMight/job999/forward_deformations/checkpoints/checkpoint_final.pth"
        ).resolve()
        jobdir = Path("DynaMight/job999").resolve()
        return ResultsDisplayText(
            title="Visualize continuous heterogeneity",
            display_data=(
                "Pipeliner currently cannot display the results of a DynaMight job "
                "in the browser.\nTo visualize the results of this job and save movies "
                "run the following command in the terminal:\n\ndynamight explore-"
                f"latent-space {jobdir} --half-set <choose a halfset "
                f"to view> --checkpoint-file {pth} --gpu-id 0\n\nChoose which halfset "
                "to display by altering the --half_set argument:\n1: view halfset "
                "1\n2: view halfset 2\n0: view error estimates on the 3D deformations "
                "for a validation set of 10% of your particles"
            ),
            associated_data=[
                "DynaMight/job999/forward_deformations/checkpoints/checkpoint_final.pth"
            ],
        )

    def test_display_objects_motion_only(self):
        fd_dir = Path("DynaMight/job999/forward_deformations/checkpoints")
        job_display_object_generation_test(
            jobfile=str(self.dynamight_test / "motion_only_job.star"),
            expected_display_objects=[self.make_visdisp()],
            files_to_create={str(fd_dir / "checkpoint_final.pth"): ""},
        )

    def test_display_objects_inverse(self):
        fd_dir = Path("DynaMight/job999/forward_deformations/checkpoints")
        job_display_object_generation_test(
            jobfile=str(self.dynamight_test / "inverse_only_job.star"),
            expected_display_objects=[self.make_visdisp()],
            files_to_create={str(fd_dir / "checkpoint_final.pth"): ""},
        )

    def test_display_rec_only(self):
        fd_dir = Path("DynaMight/job999/forward_deformations/checkpoints")
        job_display_object_generation_test(
            jobfile=str(self.dynamight_test / "rec_only_job.star"),
            expected_display_objects=[
                self.make_visdisp(),
                ResultsDisplayMapModel(
                    maps=["DynaMight/job999/backprojection/map_half1.mrc"],
                    maps_data="DynaMight/job999/backprojection/map_half1.mrc",
                    title=(
                        "3D viewer: Map: DynaMight/job999/backprojection/map_half1.mrc"
                    ),
                    associated_data=[
                        "DynaMight/job999/backprojection/map_half1.mrc",
                    ],
                ),
                ResultsDisplayMapModel(
                    maps=["DynaMight/job999/backprojection/map_half2.mrc"],
                    maps_data="DynaMight/job999/backprojection/map_half2.mrc",
                    title=(
                        "3D viewer: Map: DynaMight/job999/backprojection/map_half2.mrc"
                    ),
                    associated_data=[
                        "DynaMight/job999/backprojection/map_half2.mrc",
                    ],
                ),
            ],
            files_to_create={
                "backprojection/map_half1.mrc": str(
                    Path(self.test_data) / "emd_3488.mrc"
                ),
                "backprojection/map_half2.mrc": str(
                    Path(self.test_data) / "emd_3488.mrc"
                ),
                str(fd_dir / "checkpoint_final.pth"): "",
            },
        )

    def test_display_all_tasks(self):
        fd_dir = Path("DynaMight/job999/forward_deformations/checkpoints")
        job_display_object_generation_test(
            jobfile=str(self.dynamight_test / "all_tasks_job.star"),
            expected_display_objects=[
                self.make_visdisp(),
                ResultsDisplayMapModel(
                    maps=["DynaMight/job999/backprojection/map_half1.mrc"],
                    maps_data="DynaMight/job999/backprojection/map_half1.mrc",
                    title=(
                        "3D viewer: Map: DynaMight/job999/backprojection/map_half1.mrc"
                    ),
                    associated_data=[
                        "DynaMight/job999/backprojection/map_half1.mrc",
                    ],
                ),
                ResultsDisplayMapModel(
                    maps=["DynaMight/job999/backprojection/map_half2.mrc"],
                    maps_data="DynaMight/job999/backprojection/map_half2.mrc",
                    title=(
                        "3D viewer: Map: DynaMight/job999/backprojection/map_half2.mrc"
                    ),
                    associated_data=[
                        "DynaMight/job999/backprojection/map_half2.mrc",
                    ],
                ),
            ],
            print_dispobj=True,
            files_to_create={
                "backprojection/map_half1.mrc": str(
                    Path(self.test_data) / "emd_3488.mrc"
                ),
                "backprojection/map_half2.mrc": str(
                    Path(self.test_data) / "emd_3488.mrc"
                ),
                str(fd_dir / "checkpoint_final.pth"): "",
            },
        )

    @staticmethod
    def setup_script_checks():
        chdir = Path("DynaMight/job001/forward_deformations/checkpoints")
        chdir.mkdir(parents=True)
        touch(str(chdir / "checkpoint_final.pth"))
        voldir = Path("DynaMight/job001/forward_deformations/volumes")
        voldir.mkdir(parents=True)
        for n in range(0, 210, 10):
            for f in ["mask_half1_", "volume_half1_", "volume_half2_"]:
                touch(str(voldir / f"{f}{n:03d}.mrc"))

    def test_script_inverse_only(self):
        self.setup_script_checks()
        dynamight_setup.main(
            [
                "--output_dir",
                "DynaMight/job999",
                "--checkpoint_file",
                "DynaMight/job001/forward_deformations/checkpoints/"
                "checkpoint_final.pth",
            ]
        )
        ddir = "DynaMight/job999/forward_deformations/"
        expected_files = [
            Path(f"{ddir}/checkpoints/checkpoint_final.pth"),
            Path(f"{ddir}/volumes/volume_half1_200.mrc"),
            Path(f"{ddir}/volumes/volume_half2_200.mrc"),
            Path(f"{ddir}/volumes/mask_half1_200.mrc"),
        ]
        for f in expected_files:
            assert f.is_file()

    def test_script_inv_and_backproject(self):
        "inverse_deformations/inv_chkpt.pth"
        self.setup_script_checks()
        iddir = Path("DynaMight/job001/inverse_deformations")
        iddir.mkdir(parents=True)
        touch(str(iddir / "inv_chkpt.pth"))
        dynamight_setup.main(
            [
                "--output_dir",
                "DynaMight/job999",
                "--checkpoint_file",
                "DynaMight/job001/forward_deformations/checkpoints/"
                "checkpoint_final.pth",
                "--do_backproject",
            ]
        )
        fdir = "DynaMight/job999/forward_deformations/"
        idir = "DynaMight/job999/inverse_deformations/"

        expected_files = [
            Path(f"{fdir}/checkpoints/checkpoint_final.pth"),
            Path(f"{fdir}/volumes/volume_half1_200.mrc"),
            Path(f"{fdir}/volumes/volume_half2_200.mrc"),
            Path(f"{fdir}/volumes/mask_half1_200.mrc"),
            Path(f"{idir}/inv_chkpt.pth"),
        ]
        for f in expected_files:
            assert f.is_file()


if __name__ == "__main__":
    unittest.main()
