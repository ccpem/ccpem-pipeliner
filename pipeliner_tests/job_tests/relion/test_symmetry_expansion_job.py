#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.node_factory import create_node
from pipeliner.data_structure import NODE_PARTICLEGROUPMETADATA
from pipeliner.results_display_objects import ResultsDisplayMontage, ResultsDisplayTable


class SymmetryExpandJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.job_files = Path(self.test_data) / "JobFiles/SymmetryExpansion"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_job(self):
        """A valid input file is needed for command generation"""
        jobdir = Path("SymmetryExpansion/job001")
        jobdir.mkdir(parents=True)
        shutil.copy(Path(self.test_data) / "particles_short.star", jobdir)
        shutil.copy(Path(self.test_data) / "particles.star", jobdir / "expanded.star")
        job = new_job_of_type("relion.image_analysis.symmetry_expansion")
        job.output_dir = str(jobdir)
        job.joboptions["input_particles"].value = str(jobdir / "particles_short.star")
        job.output_nodes = [
            create_node(
                str(jobdir / "expanded.star"),
                nodetype=NODE_PARTICLEGROUPMETADATA,
                kwds=["relion", "symmetry_expanded"],
            )
        ]
        return job

    def test_get_counts(self):
        job = self.setup_job()
        assert job.get_counts() == (4, 1158)

    def test_get_command(self):
        self.setup_job()
        job_generate_commands_test(
            jobfile=str(self.job_files / "sym_expansion_job.star"),
            input_nodes={
                "SymmetryExpansion/job001/particles_short.star": (
                    NODE_PARTICLEGROUPMETADATA + ".star.relion"
                ),
            },
            output_nodes={
                "expanded.star": NODE_PARTICLEGROUPMETADATA + ".star.relion.symmetry"
                "_expanded"
            },
            expected_commands=[
                "relion_particle_symmetry_expand --i SymmetryExpansion/job001/particles"
                "_short.star --sym T --o SymmetryExpansion/job999/expanded.star "
                "--angpix 3.540000"
            ],
        )

    def test_get_command_angpix_provided(self):
        job_generate_commands_test(
            jobfile=str(self.job_files / "sym_expansion_apix_job.star"),
            input_nodes={
                "SymmetryExpansion/job001/particles_short.star": (
                    NODE_PARTICLEGROUPMETADATA + ".star.relion"
                ),
            },
            output_nodes={
                "expanded.star": NODE_PARTICLEGROUPMETADATA + ".star.relion.symmetry"
                "_expanded"
            },
            expected_commands=[
                "relion_particle_symmetry_expand --i SymmetryExpansion/job001/particles"
                "_short.star --sym T --o SymmetryExpansion/job999/expanded.star "
                "--angpix 1.04"
            ],
        )

    def test_get_command_no_angpix_raises_error(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.job_files / "sym_expansion_job.star"),
                input_nodes={
                    "SymmetryExpansion/job001/particles_short.star": (
                        NODE_PARTICLEGROUPMETADATA + ".star.relion"
                    ),
                },
                output_nodes={
                    "expanded.star": NODE_PARTICLEGROUPMETADATA
                    + ".star.relion.symmetry"
                    "_expanded"
                },
                expected_commands=[
                    "relion_particle_symmetry_expand --i "
                    "SymmetryExpansion/job001/particles_short.star --sym T --o "
                    "SymmetryExpansion/job999/expanded.star --angpix 3.540000"
                ],
            )

    def test_get_command_helical(self):
        self.setup_job()
        job_generate_commands_test(
            jobfile=str(self.job_files / "sym_expansion_helical_job.star"),
            input_nodes={
                "SymmetryExpansion/job001/particles_short.star": (
                    NODE_PARTICLEGROUPMETADATA + ".star.relion"
                ),
            },
            output_nodes={
                "expanded.star": NODE_PARTICLEGROUPMETADATA + ".star.relion.symmetry"
                "_expanded"
            },
            expected_commands=[
                "relion_particle_symmetry_expand --i SymmetryExpansion/job001/particles"
                "_short.star --sym T --o SymmetryExpansion/job999/expanded.star "
                "--angpix 3.540000 --helix --twist 30 --rise 4.8 --asu 1 "
                "--frac_sampling 1 --frac_range 0.5"
            ],
        )

    def test_joboption_validation(self):
        job = new_job_of_type("relion.image_analysis.symmetry_expansion")
        job.joboptions["symmetry"].value = "C1"
        val = job.additional_joboption_validation()
        assert len(val) == 1
        assert val[0].message == "A higher order symmetry must be used"
        assert val[0].type == "error"

    @patch("pipeliner.nodes.ParticleGroupMetadataNode.default_results_display")
    @patch(
        "pipeliner.jobs.relion.symmetry_expansion_job.RelionSymmetryExpansion."
        "get_counts"
    )
    def test_get_results_display(self, countpatch, nodepatch):
        montage = ResultsDisplayMontage(
            xvalues=[1, 2],
            yvalues=[1, 2],
            img="image.png",
            title="a montage",
            associated_data=["data"],
        )
        nodepatch.return_value = montage
        countpatch.return_value = 10, 20
        table = ResultsDisplayTable(
            title="Particle symmetry expansion results",
            headers=["", "Particle count"],
            table_data=[
                ["Original file", "10"],
                ["Symmetry expanded", "20"],
                ["Particles added", "10"],
            ],
            associated_data=["SymmetryExpansion/job999/expanded.star"],
        )

        job_display_object_generation_test(
            jobfile=str(self.job_files / "sym_expansion_job.star"),
            expected_display_objects=[table, montage],
            files_to_create={},
            print_dispobj=True,
        )

    def test_gather_metadata(self):
        job = self.setup_job()
        md = job.gather_metadata()
        assert md == {"expanded_particle_count": 1158, "original_particle_count": 4}


if __name__ == "__main__":
    unittest.main()
