#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner.job_factory import new_job_of_type
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner_tests import test_data

from pipeliner.data_structure import NODE_PARTICLEGROUPMETADATA, NODE_DENSITYMAP


class PlugInsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Reconstruct"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_reconstruct_plugin(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "reconstruct_plugin_job.star"),
            input_nodes={
                "test_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "reconstruction.mrc": f"{NODE_DENSITYMAP}.mrc.relion.reconstruct"
            },
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction.mrc --ctf --angpix 1.07"
                " --sym C1"
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_reconstruct_plugin_with_halfmaps(self):
        shutil.copy(Path(self.test_data) / "run_data.star", "test_particles.star")
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "reconstruct_halfmaps_job.star"),
            input_nodes={
                "test_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "reconstruction.mrc": f"{NODE_DENSITYMAP}.mrc.relion.reconstruct",
                "reconstruction_half1.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "reconstruct.halfmap",
                "reconstruction_half2.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "reconstruct.halfmap",
            },
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction.mrc --ctf --angpix 1.07"
                " --sym C1",
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction_half1.mrc --ctf --angpix 1.07"
                " --sym C1 --subset 1",
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction_half2.mrc --ctf --angpix 1.07"
                " --sym C1 --subset 2",
            ],
        )

    def test_get_command_reconstruct_plugin_with_halfmaps_error_if_no_subsets(self):
        shutil.copy(Path(self.test_data) / "particles.star", "test_particles.star")
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "reconstruct_halfmaps_job.star"),
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_reconstruct_mpi_plugin(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "reconstruct_plugin_mpi_job.star"),
            input_nodes={
                "test_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "reconstruction.mrc": f"{NODE_DENSITYMAP}.mrc.relion.reconstruct"
            },
            expected_commands=[
                "mpirun -n 8 /path/to/relion_reconstruct_mpi "
                "--i test_particles.star --o "
                "Reconstruct/job999/reconstruction.mrc --ctf --angpix 1.07"
                " --sym C1"
            ],
        )

    def test_get_command_reconstruct_plugin_with_no_angpix_and_maxres(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "reconstruct_plugin_angpx_maxres_job.star"),
            input_nodes={
                "test_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "reconstruction.mrc": f"{NODE_DENSITYMAP}.mrc.relion.reconstruct"
            },
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction.mrc --ctf --maxres 2.0"
                " --sym C1"
            ],
        )

    def test_get_command_reconstruct_plugin_with_no_ctf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "reconstruct_no_ctf_job.star"),
            input_nodes={
                "test_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "reconstruction.mrc": f"{NODE_DENSITYMAP}.mrc.relion.reconstruct"
            },
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job999/reconstruction.mrc --angpix 1.07 --sym C1"
            ],
        )

    def test_create_results_display(self):
        mrc = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs("Reconstruct/job001")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction.mrc")
        job = new_job_of_type("relion.reconstruct")
        job.output_dir = "Reconstruct/job001/"
        dispobj = job.create_results_display()

        assert dispobj[0].__dict__ == {
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "Reconstruct/job001/Thumbnails/slices_montage_000.png",
            "labels": [
                "Reconstruct/job001/reconstruction.mrc: xy",
                "Reconstruct/job001/reconstruction.mrc: xz",
                "Reconstruct/job001/reconstruction.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: Reconstructed map",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert dispobj[1].__dict__ == {
            "maps": ["Reconstruct/job001/reconstruction.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Reconstructed map",
            "maps_data": "Reconstruct/job001/reconstruction.mrc",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    def test_create_results_display_halfmaps(self):
        mrc = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs("Reconstruct/job001")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction.mrc")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction_half1.mrc")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction_half2.mrc")

        job = new_job_of_type("relion.reconstruct")
        job.joboptions["do_halfmaps"].value = "Yes"
        job.output_dir = "Reconstruct/job001/"
        dispobj = job.create_results_display()

        assert dispobj[0].__dict__ == {
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "Reconstruct/job001/Thumbnails/slices_montage_000.png",
            "labels": [
                "Reconstruct/job001/reconstruction.mrc: xy",
                "Reconstruct/job001/reconstruction.mrc: xz",
                "Reconstruct/job001/reconstruction.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: Reconstructed map",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert dispobj[1].__dict__ == {
            "maps": ["Reconstruct/job001/reconstruction.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Reconstructed map",
            "maps_data": "Reconstruct/job001/reconstruction.mrc",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobj[2].__dict__ == {
            "associated_data": ["Reconstruct/job001/reconstruction_half1.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "Reconstruct/job001/Thumbnails/slices_montage_001.png",
            "labels": [
                "Reconstruct/job001/reconstruction_half1.mrc: xy",
                "Reconstruct/job001/reconstruction_half1.mrc: xz",
                "Reconstruct/job001/reconstruction_half1.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: Halfmap 1",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert dispobj[3].__dict__ == {
            "maps": ["Reconstruct/job001/reconstruction_half1.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Halfmap 1",
            "maps_data": "Reconstruct/job001/reconstruction_half1.mrc",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction_half1.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobj[4].__dict__ == {
            "associated_data": ["Reconstruct/job001/reconstruction_half2.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "Reconstruct/job001/Thumbnails/slices_montage_002.png",
            "labels": [
                "Reconstruct/job001/reconstruction_half2.mrc: xy",
                "Reconstruct/job001/reconstruction_half2.mrc: xz",
                "Reconstruct/job001/reconstruction_half2.mrc: yz",
            ],
            "start_collapsed": False,
            "title": "Map slices: Halfmap 2",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert dispobj[5].__dict__ == {
            "maps": ["Reconstruct/job001/reconstruction_half2.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Halfmap 2",
            "maps_data": "Reconstruct/job001/reconstruction_half2.mrc",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction_half2.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
