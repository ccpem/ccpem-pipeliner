#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner import job_factory
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_PROCESSDATA,
    NODE_LOGFILE,
    NODE_MICROSCOPEDATA,
    POSTPROCESS_JOB_NAME,
)
from pipeliner.api.manage_project import PipelinerProject


class PostProcessTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/PostProcess"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_reading_postprocess_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess_autobf.job")
        )
        assert job.PROCESS_NAME == POSTPROCESS_JOB_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 17
        assert job.joboptions["fn_in"].label == "One of the 2 unfiltered half-maps:"
        assert (
            job.joboptions["fn_in"].value
            == "Import/job001/3488_run_half1_class001_unfil.mrc"
        )
        assert job.joboptions["fn_in"].node_type == NODE_DENSITYMAP
        assert job.joboptions["fn_mask"].label == "Solvent mask:"
        assert job.joboptions["fn_mask"].value == "Import/job002/emd_3488_mask.mrc"
        assert job.joboptions["fn_mask"].node_type == NODE_MASK3D
        assert job.joboptions["angpix"].label == "Calibrated pixel size (A)"
        assert job.joboptions["angpix"].value == "1.244"
        assert job.joboptions["angpix"].get_number() == 1.244
        assert (
            job.joboptions["do_auto_bfac"].label == "Estimate B-factor automatically?"
        )
        assert job.joboptions["do_auto_bfac"].value == "Yes"
        assert job.joboptions["do_auto_bfac"].get_boolean()
        assert (
            job.joboptions["autob_lowres"].label
            == "Lowest resolution for auto-B fit (A):"
        )
        assert job.joboptions["autob_lowres"].value == "10"
        assert job.joboptions["autob_lowres"].get_number() == 10
        assert job.joboptions["do_adhoc_bfac"].label == "Use your own B-factor?"
        assert job.joboptions["do_adhoc_bfac"].value == "No"
        assert not job.joboptions["do_adhoc_bfac"].get_boolean()
        assert job.joboptions["adhoc_bfac"].label == "User-provided B-factor:"
        assert job.joboptions["adhoc_bfac"].value == "-1000"
        assert job.joboptions["adhoc_bfac"].get_number() == -1000
        assert job.joboptions["fn_mtf"].label == "MTF of the detector (STAR file)"
        assert job.joboptions["fn_mtf"].value == ""
        assert job.joboptions["mtf_angpix"].label == "Original detector pixel size:"
        assert job.joboptions["mtf_angpix"].value == "1"
        assert job.joboptions["mtf_angpix"].get_number() == 1
        assert job.joboptions["do_skip_fsc_weighting"].label == "Skip FSC-weighting?"
        assert job.joboptions["do_skip_fsc_weighting"].value == "No"
        assert not job.joboptions["do_skip_fsc_weighting"].get_boolean()
        assert job.joboptions["low_pass"].label == "Ad-hoc low-pass filter (A):"
        assert job.joboptions["low_pass"].value == "5"
        assert job.joboptions["low_pass"].get_number() == 5.0
        assert job.joboptions["other_args"].label == "Additional arguments:"
        assert job.joboptions["other_args"].value == ""

    def test_get_postprocess_commands_autobf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_autobf.job"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "postprocess.masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --auto_bfac"
                " --autob_lowres 10 --pipeline_control PostProcess/job999/"
            ],
        )

    def test_get_postprocess_commands_adhocbf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_adhocbf.job"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess."
                "masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --adhoc_bfac -1000 "
                "--pipeline_control PostProcess/job999/"
            ],
        )

    def test_get_postprocess_commands_skipfsc(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_skipfsc.job"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess."
                "masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --auto_bfac"
                " --autob_lowres 10 --skip_fsc_weighting --low_pass 5"
                " --pipeline_control PostProcess/job999/"
            ],
        )

    def test_get_postprocess_commands_withmtf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_withmtf.job"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/mtffile/mtf_k2_200kV.star": f"{NODE_MICROSCOPEDATA}.star.mtf",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess."
                "masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --auto_bfac"
                " --autob_lowres 10 --mtf Import/mtffile/mtf_k2_200kV.star --mtf_angpix"
                " 1 --pipeline_control PostProcess/job999/"
            ],
        )

    def test_get_postprocess_commands_withmtf_read_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_job.star"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess."
                "masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --adhoc_bfac -1000 "
                "--pipeline_control PostProcess/job999/"
            ],
        )

    def test_general_getpp(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "postprocess_withmtf.job"),
            input_nodes={
                "Import/job002/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/mtffile/mtf_k2_200kV.star": f"{NODE_MICROSCOPEDATA}.star.mtf",
            },
            output_nodes={
                "postprocess.mrc": f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
                "postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc.relion."
                "postprocess.masked",
                "postprocess.star": f"{NODE_PROCESSDATA}.star.relion.postprocess",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.postprocess",
            },
            expected_commands=[
                "relion_postprocess --mask Import/job002/emd_3488_mask.mrc --i"
                " Import/job001/3488_run_half1_class001_unfil.mrc --o "
                "PostProcess/job999/postprocess --angpix 1.244 --auto_bfac"
                " --autob_lowres 10 --mtf Import/mtffile/mtf_k2_200kV.star"
                " --mtf_angpix 1 --pipeline_control PostProcess/job999/"
            ],
        )

    def test_postprocess_generate_results(self):
        get_relion_tutorial_data(relion_version=4, dirs=["PostProcess"])

        project = PipelinerProject()
        job = project.get_job("PostProcess/job021/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "PostProcessed map info",
            "dobj_type": "table",
            "headers": ["Resolution:", "3.03 Å"],
            "header_tooltips": ["Resolution:", "3.03 Å"],
            "table_data": [["Sharpening b-factor:", "-36.62221"]],
            "associated_data": ["PostProcess/job021/postprocess.star"],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[1].__dict__ == {
            "title": "Map slices: Postprocessed and masked map",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "PostProcess/job021/postprocess_masked.mrc: xy",
                "PostProcess/job021/postprocess_masked.mrc: xz",
                "PostProcess/job021/postprocess_masked.mrc: yz",
            ],
            "associated_data": ["PostProcess/job021/postprocess_masked.mrc"],
            "img": "PostProcess/job021/Thumbnails/slices_montage_000.png",
        }

        assert dispobjs[2].__dict__ == {
            "title": "3D viewer: Postprocessed and masked map",
            "dobj_type": "mapmodel",
            "maps": ["PostProcess/job021/postprocess_masked.mrc"],
            "associated_data": ["PostProcess/job021/postprocess_masked.mrc"],
            "maps_data": "PostProcess/job021/postprocess_masked.mrc",
            "maps_opacity": [1.0],
            "models": [],
            "models_data": "",
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        fsc_results = os.path.join(self.test_data, "ResultsFiles/postprocess_fsc.json")
        with open(fsc_results, "r") as fscr:
            exp_fsc = json.load(fscr)
        assert dispobjs[3].__dict__ == exp_fsc


if __name__ == "__main__":
    unittest.main()
