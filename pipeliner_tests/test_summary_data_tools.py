#
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner.starfile_handler import DataStarFile
from pipeliner.summary_data_tools import (
    SUMMARY_DATA_FILE,
    add_to_summary_file,
    remove_from_summary_file,
    get_summary_file_data,
    create_new_summary_file,
)


class SummaryDataToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_add_report_to_file_single_job_noexisting_file(self):
        add_to_summary_file("metadata_reports", ["added_report.json", "job010", "1"])
        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            ["added_report.json", "job010", "1"],
        ]

    def test_add_report_to_file_single_job_existing_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_1.star"),
            SUMMARY_DATA_FILE,
        )
        add_to_summary_file("metadata_reports", ["added_report.json", "job010", "1"])
        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            [
                "MetadataReports/20230322122525_job002.json",
                "MotionCorr/job002/",
                "1",
            ],
            ["added_report.json", "job010", "1"],
        ]

    def test_add_report_to_file_multi_job_noexisting_file(self):
        add_to_summary_file("metadata_reports", ["added_report.json", "job010", "2"])
        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            ["added_report.json", "job010", "2"],
        ]

    def test_add_report_to_file_multi_job_existing_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_1.star"),
            SUMMARY_DATA_FILE,
        )
        add_to_summary_file("metadata_reports", ["added_report.json", "job010", "2"])
        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            [
                "MetadataReports/20230322122525_job002.json",
                "MotionCorr/job002/",
                "1",
            ],
            ["added_report.json", "job010", "2"],
        ]

    def test_add_report_overwrite_existing(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_4.star"),
            SUMMARY_DATA_FILE,
        )
        add_to_summary_file(
            "metadata_reports",
            ["MetadataReports/20230322124555_job004.json", "new_job004", "2"],
        )

        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            [
                "MetadataReports/20230322122525_job002.json",
                "MotionCorr/job002/",
                "1",
            ],
            ["MetadataReports/20230322123534_job003.json", "CtfFind/job003/", "2"],
            ["MetadataReports/20230322124555_job004.json", "new_job004", "2"],
            ["MetadataReports/20230322125784_job005.json", "Extract/job005/", "4"],
        ]

    def test_remove_report_from_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_4.star"),
            SUMMARY_DATA_FILE,
        )
        remove_from_summary_file(
            "metadata_reports", "MetadataReports/20230322122525_job002.json"
        )
        report = DataStarFile(SUMMARY_DATA_FILE)
        assert report.loop_as_list("metadata_reports", headers=True) == [
            [
                "_pipelinerMetadataReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            ["MetadataReports/20230322123534_job003.json", "CtfFind/job003/", "2"],
            ["MetadataReports/20230322124555_job004.json", "AutoPick/job004/", "1"],
            ["MetadataReports/20230322125784_job005.json", "Extract/job005/", "4"],
        ]

    def test_remove_report_from_file_empties_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_1.star"),
            SUMMARY_DATA_FILE,
        )
        remove_from_summary_file(
            "metadata_reports", "MetadataReports/20230322122525_job002.json"
        )
        with open(SUMMARY_DATA_FILE) as md:
            wrote = md.readlines()

        assert wrote == [
            "data_reference_reports\n",
            "loop_\n",
            "_pipelinerReferenceReportName\n",
            "_pipelinerInitiatedFrom\n",
            "_pipelinerNumberOfJobs\n",
            "ReferenceReports/20230322122525_job002.json MotionCorr/job002/ 2\n",
            "\n",
            "data_project_archives\n",
            "loop_\n",
            "_pipelinerArchiveName\n",
            "_pipelinerInitiatedFrom\n",
            "_pipelinerArchiveType\n",
            "ProjectArchives/20230322122525_job002.tar.gz MotionCorr/job002/ full\n",
            "\n",
            "data_metadata_reports\n",
        ]

    def test_get_summary_file_data(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_4.star"),
            SUMMARY_DATA_FILE,
        )
        data = get_summary_file_data("metadata_reports")
        assert data == (
            ["MetadataReportName", "InitiatedFrom", "NumberOfJobs"],
            [
                [
                    "MetadataReports/20230322122525_job002.json",
                    "MotionCorr/job002/",
                    "1",
                ],
                ["MetadataReports/20230322123534_job003.json", "CtfFind/job003/", "2"],
                ["MetadataReports/20230322124555_job004.json", "AutoPick/job004/", "1"],
                ["MetadataReports/20230322125784_job005.json", "Extract/job005/", "4"],
            ],
        )

    def test_get_summary_file_data_file_doesnt_exist(self):
        assert get_summary_file_data("metadata_reports") == (
            ["MetadataReportName", "InitiatedFrom", "NumberOfJobs"],
            [[]],
        )

    def test_get_summary_file_data_file_exist_is_empty(self):
        create_new_summary_file()
        assert get_summary_file_data("metadata_reports") == (
            ["MetadataReportName", "InitiatedFrom", "NumberOfJobs"],
            [[]],
        )


if __name__ == "__main__":
    unittest.main()
