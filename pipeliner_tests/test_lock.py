#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import threading
import time
from pathlib import Path

from pipeliner.utils import DirectoryBasedLock


class LockingTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_acquiring_lock_creates_lock_directory(self):
        lock = DirectoryBasedLock()
        assert not Path(".relion_lock").exists()
        lock.acquire()
        assert Path(".relion_lock").is_dir()

    def test_acquiring_lock_successfully_returns_true(self):
        lock = DirectoryBasedLock()
        assert lock.acquire() is True

    def test_acquiring_and_releasing_lock(self):
        lock = DirectoryBasedLock()
        assert not Path(".relion_lock").exists()
        lock.acquire()
        assert Path(".relion_lock").is_dir()
        lock.release()
        assert not Path(".relion_lock").is_dir()

    def test_acquiring_and_releasing_lock_with_non_default_name(self):
        name = "lockdir"
        lock = DirectoryBasedLock(dirname=name)
        assert not Path(name).exists()
        assert not Path(".relion_lock").exists()
        lock.acquire()
        assert Path(name).is_dir()
        assert not Path(".relion_lock").exists()
        lock.release()
        assert not Path(name).exists()
        assert not Path(".relion_lock").is_dir()

    def test_releasing_unlocked_lock_raises_exception(self):
        lock = DirectoryBasedLock()
        with self.assertRaisesRegex(RuntimeError, ".relion_lock is not present"):
            lock.release()

    def test_acquire_without_block_when_locked_returns_false(self):
        lock = DirectoryBasedLock()
        os.mkdir(".relion_lock")
        assert lock.acquire(block=False) is False

    def test_acquire_with_block_and_timeout_when_locked_returns_false(self):
        lock = DirectoryBasedLock()
        os.mkdir(".relion_lock")
        result = lock.acquire(block=True, timeout=0.1)
        assert result is False

    def test_acquire_with_block_and_zero_timeout_when_locked_returns_false(self):
        lock = DirectoryBasedLock()
        os.mkdir(".relion_lock")
        result = lock.acquire(block=True, timeout=0.0)
        assert result is False

    def test_acquire_with_block_and_negative_timeout_when_locked_returns_false(self):
        lock = DirectoryBasedLock()
        os.mkdir(".relion_lock")
        result = lock.acquire(block=True, timeout=-1)
        assert result is False

    def test_releasing_lock_locked_from_elsewhere(self):
        os.mkdir(".relion_lock")
        lock = DirectoryBasedLock()
        lock.release()
        assert not Path(".relion_lock").is_dir()

    def test_acquiring_and_releasing_lock_using_context_manager(self):
        assert not Path(".relion_lock").exists()
        with DirectoryBasedLock() as lock:
            assert Path(lock.dirname).is_dir()
        assert not Path(lock.dirname).is_dir()

    def test_lock_uses_timeout_in_context_manager(self):
        lock = DirectoryBasedLock()
        # Check the default timeout is set as it should be
        assert lock.timeout_for_context_manager == 60.0
        # Set a much shorter timeout so the test runs quickly
        lock.timeout_for_context_manager = 0.1
        os.mkdir(".relion_lock")
        with self.assertRaisesRegex(
            TimeoutError, "waiting for lock directory .relion_lock to disappear"
        ):
            with lock:
                pass
        time.sleep(1)
        assert Path(".relion_lock").is_dir()

    def test_acquire_with_block_and_timeout(self):
        lock_for_thread = DirectoryBasedLock()
        lock_for_test = DirectoryBasedLock()
        thread_ready_signal = threading.Event()
        unlock_signal = threading.Event()

        def lock_with_signals(lock, ready_signal, unlock_signal):
            # Acquire lock and signal back to parent thread
            lock.acquire()
            ready_signal.set()

            # Wait for signal to unlock and then release after a short delay
            unlock_signal.wait()
            time.sleep(0.05)
            lock.release()

        locking_thread = threading.Thread(
            target=lock_with_signals,
            args=(lock_for_thread, thread_ready_signal, unlock_signal),
            daemon=True,
        )
        locking_thread.start()

        try:
            # Wait for the thread to indicate it has acquired the lock
            thread_ready_signal.wait(timeout=1.0)
            assert Path(".relion_lock").is_dir()
            unlock_signal.set()

            result = lock_for_test.acquire(block=True, timeout=5.0)
            assert result is True
        finally:
            locking_thread.join(timeout=1.0)
            assert not locking_thread.is_alive(), "join() timed out"


if __name__ == "__main__":
    unittest.main()
