#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    results_files_match,
    live_test,
    slow_test,
    list_block_types,
)
from pipeliner_tests import test_data
from pipeliner.display_tools import create_results_display_object
from pipeliner.data_structure import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
)


class TestingToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_running_test(self):
        job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_class2D_scratch_comb_em(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Class2D/class2D_scratch_comb.job"
            ),
            input_nodes={
                "Extract/job007/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion"
                ".class2d",
                "run_it025_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "class2d",
            },
            expected_commands=[
                "mpirun -n 5 /path/to/relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job999/run "
                "--scratch_dir Fake_scratch_dir --pool 30 --pad 2 --ctf "
                "--iter 25 --tau2_fudge 2 --particle_diameter 200 --K 50 "
                "--flatten_solvent --zero_mask --center_classes --oversampling 1"
                " --psi_step 12.0"
                " --offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job999/"
            ],
        )

    def test_match_results_types_dicts(self):
        m1 = {"dobj_type": "mapmodel"}
        m2 = {"dobj_type": "mapmodel"}
        other = {"dobj_type": "other"}

        assert results_files_match(m1, m2, fields_only=True)
        assert not results_files_match(m1, other, fields_only=True)

    def test_match_results_fields_dicts(self):
        m1 = {"dobj_type": "mapmodel", "field1": "test1"}
        m2 = {"dobj_type": "mapmodel", "field1": "test1"}
        other = {"dobj_type": "mapmodel", "field1": "test1", "field2": "BAD!!"}

        assert results_files_match(m1, m2, fields_only=True)
        assert not results_files_match(m1, other, fields_only=True)

    def test_match_results_values_dicts(self):
        m1 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [1.1, 2.2, 3.3]}
        m2 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [1.1, 2.2, 3.3]}
        other = {"dobj_type": "mapmodel", "field1": "test1", "field2": [1.1, 2.2, 3.0]}

        assert results_files_match(m1, m2)
        assert not results_files_match(m1, other)

    def test_match_results_nested_values_dicts(self):
        m1 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [[1.1, 2.2], [3.3]]}
        m2 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [[1.1, 2.2], [3.3]]}
        other = {
            "dobj_type": "mapmodel",
            "field1": "test1",
            "field2": [[1.1, 2.2], [3.0]],
        }

        assert results_files_match(m1, m2)
        assert not results_files_match(m1, other)

    def test_match_results_nested_values_with_tol_dicts(self):
        m1 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [[1.1, 2.2], [3.3]]}
        m2 = {"dobj_type": "mapmodel", "field1": "test1", "field2": [[1.1, 2.2], [3.3]]}
        other = {
            "dobj_type": "mapmodel",
            "field1": "test1",
            "field2": [[1.1, 2.2], [3.0]],
        }

        assert results_files_match(m1, m2, tolerance=0.1)
        assert results_files_match(m1, other, tolerance=0.1)
        assert not results_files_match(m1, other, tolerance=0)

    def test_match_results_files(self):
        td = os.path.join(self.test_data, "ResultsFiles/class2d_results.json")
        td2 = os.path.join(self.test_data, "ResultsFiles/extract_results.json")

        assert results_files_match(td, td)
        assert not results_files_match(td, td2)

    def test_match_results_files_with_nested_lists(self):
        td = os.path.join(self.test_data, "ResultsFiles/class3d_dist_results.json")
        td2 = os.path.join(self.test_data, "ResultsFiles/postprocess_fsc.json")

        assert results_files_match(td, td)
        assert not results_files_match(td, td2)

    def test_match_results_rdo(self):
        t1 = create_results_display_object(
            "text",
            title="Test",
            display_data="test text",
            associated_data=[],
            start_collapsed=False,
            flag="",
        )
        t2 = create_results_display_object(
            "text",
            title="Test2",
            display_data="test text",
            associated_data=[],
            start_collapsed=False,
            flag="",
        )

        assert results_files_match(t1, t1)
        assert not results_files_match(t1, t2)
        assert not results_files_match(t2, t1)

    def test_match_results_mixed_lists_dicts(self):
        t1 = create_results_display_object(
            "gallery",
            title="Test",
            labels=["test text"],
            images="Class2D/job008/imagelist.json",
            associated_nodes=[
                {"name": "test", "type": "test"},
                {"name": "test2", "type": "test2"},
            ],
            associated_data=[],
            start_collapsed=False,
            flag="",
        )
        t2 = create_results_display_object(
            "gallery",
            title="Test2",
            labels=["test text"],
            images="Class2D/job008/imagelist.json",
            associated_nodes=[{"name": "test", "type": "test"}],
            associated_data=[["test"]],
            start_collapsed=False,
            flag="",
        )

        assert results_files_match(t1, t1)
        assert not results_files_match(t1, t2)
        assert not results_files_match(t2, t1)

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_no_args(self):
        with self.assertRaises(ValueError):

            @live_test()
            def dummy_test():
                pass

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_no_args_entered(self):
        with self.assertRaises(ValueError):

            @live_test
            def dummy_test():
                pass

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_program(self):
        @live_test(programs=["program1"])
        def dummy_test():
            pass

        with patch("shutil.which") as mock_which:
            mock_which.return_value = "foundit"
            try:
                dummy_test()
            except unittest.SkipTest:
                self.fail()
            mock_which.return_value = None
            with self.assertRaises(unittest.SkipTest):
                dummy_test()

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_true_condition(self):
        @live_test(condition=True)
        def dummy_test():
            try:
                dummy_test()
            except unittest.SkipTest:
                self.fail()

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_false_condition(self):
        @live_test(condition=False)
        def dummy_test():
            with self.assertRaises(unittest.SkipTest):
                dummy_test()

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_multiple_jobs(self):
        @live_test(jobs=["job1", "job2"])
        def dummy_test():
            pass

        with patch("pipeliner_tests.testing_tools.job_factory.job_can_run") as mock_run:
            mock_run.return_value = False
            with self.assertRaises(unittest.SkipTest):
                dummy_test()
            mock_run.return_value = True
            try:
                dummy_test()
            except unittest.SkipTest:
                self.fail()

    @patch.dict(os.environ, {"PIPELINER_TEST_LIVE": "True"})
    def test_live_test_decorator_with_single_job(self):
        @live_test(job="job1")
        def dummy_test():
            pass

        with patch("pipeliner_tests.testing_tools.job_factory.job_can_run") as mock_run:
            mock_run.return_value = False
            with self.assertRaises(unittest.SkipTest):
                dummy_test()
            mock_run.return_value = True
            try:
                dummy_test()
            except unittest.SkipTest:
                self.fail()

    @patch.dict(os.environ, {}, clear=True)
    def test_live_test_decorator_turned_off(self):
        @live_test(condition=True)
        def dummy_test():
            pass

        with self.assertRaises(unittest.SkipTest):
            assert dummy_test()

    @patch.dict(os.environ, {}, clear=True)
    def test_slow_test_decorator_turned_off(self):
        @slow_test
        def dummy_test():
            pass

        with self.assertRaises(unittest.SkipTest):
            assert dummy_test()

    @patch.dict(os.environ, {"PIPELINER_TEST_SLOW": "True"})
    def test_slow_test_decorator_turned_on(self):
        @slow_test
        def dummy_test():
            pass

        try:
            dummy_test()
        except unittest.SkipTest:
            self.fail()

    def test_starfile_list_block_types_pipeline(self):
        sf = Path(self.test_data) / "StarFiles/short_pipeline.star"
        blocks = list_block_types(str(sf))
        assert blocks == {
            "pipeline_general": "pair",
            "pipeline_input_edges": "loop",
            "pipeline_nodes": "loop",
            "pipeline_output_edges": "loop",
            "pipeline_processes": "loop",
        }

    def test_starfile_list_block_types_soleblock(self):
        sf = str(Path(self.test_data) / "empty_sole_block.star")
        blocks = list_block_types(sf)
        assert blocks == {" ": "loop"}

    def test_starfile_list_block_types_bothtypes(self):
        sf = str(Path(self.test_data) / "StarFiles/pair_and_loop.star")
        with self.assertRaisesRegex(ValueError, "both"):
            list_block_types(sf)

    def test_starfile_list_block_types_multiple_loops(self):
        sf = str(Path(self.test_data) / "StarFiles/multi_loop.star")
        with self.assertRaisesRegex(ValueError, "multiple"):
            list_block_types(sf)

    def test_starfile_list_block_types_empty_block(self):
        sf = str(Path(self.test_data) / "StarFiles/no_blocks.star")
        with self.assertRaisesRegex(ValueError, "empty"):
            list_block_types(sf)


if __name__ == "__main__":
    unittest.main()
