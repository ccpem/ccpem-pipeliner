#!/usr/bin/env python3
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.job_manager import wait_for_job_to_finish

proj = PipelinerProject(make_new_project=True)
import_001 = proj.schedule_job("JobFiles/import_001_job.star", alias="THIS_IS_IMPORT")
motioncorr_002 = proj.schedule_job("JobFiles/motioncorr_002_job.star")
ctffind_003 = proj.schedule_job("JobFiles/ctffind_003_job.star", alias="CTFME_BABY")
autopick_004 = proj.schedule_job("JobFiles/autopick_004_job.star")
extract_005 = proj.schedule_job("JobFiles/extract_005_job.star")
class2d_006 = proj.schedule_job("JobFiles/class2d_006_job.star")
select_007 = proj.schedule_job("JobFiles/select_007_job.star")
initialmodel_008 = proj.schedule_job("JobFiles/initialmodel_008_job.star")
class3d_009 = proj.schedule_job("JobFiles/class3d_009_job.star")
refine3d_010 = proj.schedule_job("JobFiles/refine3d_010_job.star")
maskcreate_011 = proj.schedule_job("JobFiles/maskcreate_011_job.star")
postprocess_012 = proj.schedule_job("JobFiles/postprocess_012_job.star")

proj.run_scheduled_job(import_001)
wait_for_job_to_finish(import_001, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(motioncorr_002)
wait_for_job_to_finish(motioncorr_002, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(ctffind_003)
wait_for_job_to_finish(ctffind_003, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(autopick_004)
wait_for_job_to_finish(autopick_004, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(extract_005)
wait_for_job_to_finish(extract_005, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(class2d_006)
wait_for_job_to_finish(class2d_006, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(select_007)
wait_for_job_to_finish(select_007, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(initialmodel_008)
wait_for_job_to_finish(initialmodel_008, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(class3d_009)
wait_for_job_to_finish(class3d_009, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(refine3d_010)
wait_for_job_to_finish(refine3d_010, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(maskcreate_011)
wait_for_job_to_finish(maskcreate_011, error_on_fail=True, error_on_abort=True)

proj.run_scheduled_job(postprocess_012)
wait_for_job_to_finish(postprocess_012, error_on_fail=True, error_on_abort=True)
