
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnImagePixelSize #8 
_rlnImageSize #9 
_rlnImageDimensionality #10 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     3.540000           64            2 
 

# version 30001

data_particles

loop_ 
_rlnCoordinateX #1 
_rlnCoordinateY #2 
_rlnAutopickFigureOfMerit #3 
_rlnClassNumber #4 
_rlnAnglePsi #5 
_rlnImageName #6 
_rlnMicrographName #7 
_rlnOpticsGroup #8 
_rlnCtfMaxResolution #9 
_rlnCtfFigureOfMerit #10 
_rlnDefocusU #11 
_rlnDefocusV #12 
_rlnDefocusAngle #13 
_rlnCtfBfactor #14 
_rlnCtfScalefactor #15 
_rlnPhaseShift #16 
_rlnGroupNumber #17 
_rlnAngleRot #18 
_rlnAngleTilt #19 
_rlnOriginXAngst #20 
_rlnOriginYAngst #21 
_rlnNormCorrection #22 
_rlnLogLikeliContribution #23 
_rlnMaxValueProbDistribution #24 
_rlnNrOfSignificantSamples #25 
 1614.217657   112.882354     0.163048           40   -173.52365 000001@Extract/job007/Movies/20170629_00021_frameImage.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.809192     0.131159 10864.146484 10575.793945    77.995003     0.000000     1.000000     0.000000            1     0.000000     0.000000     -6.54422     -3.00422     0.804371  6031.635186     0.991878            2 
 3160.705901  2393.105896     0.154922           30   -173.52365 Extract/job007/Movies/20170629_00021_frameImage.mrcs MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1     4.809192     0.131159 10864.146484 10575.793945    77.995003     0.000000     1.000000     0.000000            1     0.000000     0.000000     0.535781    -10.08422     0.827265  6004.840974     0.392461            1 
