# RELION version 4.0 / CCP-EM Pipeliner version 0.2.0

data_job

_rlnJobTypeLabel    pipeliner.image_analysis.average_particle_ps

_rlnJobIsContinue    1

_rlnJobIsTomo    0


# RELION version 4.0 / CCP-EM Pipeliner version 0.2.0

data_joboptions_values

loop_
_rlnJobOptionVariable #1
_rlnJobOptionValue #2
    'do_pad'          Yes
  'do_queue'           No
'min_dedicated'            1
'particle_star'           'parts.star'
'pixel_size'           -1
'selected_class'            1
        qsub         qsub
  qsubscript           ''
   queuename      openmpi