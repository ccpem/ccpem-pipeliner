# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                        cryoef.map_analysis
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
input_file			Refine3D/job001/run_data.star
b_factor                   150      # optional put -1 to use default
particle_diameter_ang      80       # optional put -1 to use default
box_size_px                128      # optional put -1 to use default
resolution                 3.8      # optional put -1 to not use it
symmetry                   C1       # optional put "" to not use it
angular_acc				   -1		# optional put -1 to use default
max_tilt_angle			   -1		# optional put -1 to use default
do_queue         	No 
min_dedicated       1 
qsub       			qsub 
qsubscript 			"" 
queuename    		openmpi 
other_args         	"" 