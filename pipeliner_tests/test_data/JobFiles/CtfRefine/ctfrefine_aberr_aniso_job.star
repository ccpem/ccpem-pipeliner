
# version 50001
# CCP-EM Pipeliner version 1.0.1

data_job

_rlnJobTypeLabel    relion.ctfrefine
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_4thorder'           Yes
'do_aniso_mag'           Yes
  'do_astig'           No 
'do_bfactor'           No 
    'do_ctf'          No
'do_defocus'           No 
  'do_phase'           No 
  'do_queue'           No 
   'do_tilt'           Yes
'do_trefoil'           Yes
   'fn_data'           'Refine3D/job001/run_data.star'
   'fn_post'           'PostProcess/job002/postprocess.star'
'min_dedicated'            1 
'mpi_command' 'mpirun -n XXXmpinodesXXX' 
    'nr_mpi'            1 
'nr_threads'            1 
'other_args'           '' 
      minres           30 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
