
# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_job

_rlnJobTypeLabel    pipeliner.deposition.empiar
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'book_chapter_title'           '' 
'ca_address'           '' 
   'ca_city'           '' 
'ca_country' 'United States of America' 
  'ca_email' mattiadanza@email.com 
'ca_first_name'      Matthew 
'ca_middle_name'           '' 
  'ca_orcid'           '' 
    'ca_org'        CCPEM 
'ca_postcode'           '' 
  'ca_state'           '' 
'ca_surname'      Iadanza 
    'ca_tel'           '' 
'citation_authors'           '' 
'citation_file'           '' 
'citation_title'           '' 
'cite_country' 'United States Minor Outlying Islands' 
'dep_corr_mics'          Yes 
'dep_corr_parts'          Yes 
 'dep_parts'          Yes 
'dep_raw_mics'          Yes 
  'emdb_ref'           '' 
'empiar_ref'           '' 
'empiar_token'           '' 
'empiar_transfer_password'           '' 
'entry_authors' 'Iadanza, MI, 1234-1233-1234-1234' 
'entry_title' 'My empiar deposition' 
'first_page'           '' 
 'input_job' PostProcess/job030
'j_or_nj_citation'           No 
'journal_abbrev'           '' 
 'last_page'           '' 
'pi_address'           '' 
   'pi_city'           '' 
'pi_country' 'United States of America' 
  'pi_email' matt@ccpem.com 
'pi_first_name'      Matthew 
'pi_middle_name'           '' 
  'pi_orcid'           '' 
    'pi_org'        CCPEM 
'pi_postcode'           '' 
  'pi_state'           '' 
'pi_surname'      Iadanza 
    'pi_tel'           '' 
'pub_location'           '' 
'release_status' 'Directly after the submission has been processed' 
'thumbnail_file'           '' 
'upload_entry_author_file'           '' 
     details           '' 
         doi           '' 
     editors           '' 
       issue           '' 
     journal           '' 
    language           '' 
   published           No 
   publisher           '' 
    pubmedid           '' 
      volume           '' 
        year           '' 
 
