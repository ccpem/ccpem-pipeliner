
# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_job

_rlnJobTypeLabel    pipeliner.deposition.empiar
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'book_chapter_title'           'A fancy book'
'ca_address'           'my house'
   'ca_city'           'Portland'
'ca_country'           'US'
  'ca_email'           'pi@pi.com'
'ca_first_name'           'Some'
'ca_middle_name'           'Crazy'
  'ca_orcid'           '1234-5678-91011'
    'ca_org'           'An important one'
'ca_postcode'           '97223'
  'ca_state'           'Oregon'
'ca_surname'           'Guy'
    'ca_tel'           '+1 (503) 639-5777'
'citation_authors'         'Lebowski, J, 1234-5678-91011:::Sobchak, W'
'citation_file'           '' 
'citation_title'           'A reference'
'cite_country'           'US'
'dep_corr_mics'          Yes
'dep_corr_parts'          Yes 
 'dep_parts'          Yes 
'dep_raw_mics'          Yes 
  'emdb_ref'           '0010'
'empiar_ref'           '123'
'empiar_token'          '123abc'
'empiar_transfer_password'    'EXpassword'
'entry_authors'           'Lebowski, J, 1234-5678-91011:::Sobchak, W'
'entry_title'           'My first deposition'
'first_page'           '1'
 'input_job' 'PostProcess/job030/'
'j_or_nj_citation'           No 
'journal_abbrev'           'J. Imp. Stuff'
 'last_page'           '2'
'pi_address'           'my house'
   'pi_city'           'Portland'
'pi_country'           'US'
  'pi_email'           'pi@pi.com'
'pi_first_name'           'Some'
'pi_middle_name'           'Crazy'
  'pi_orcid'           '1234-5678-91011'
    'pi_org'           'An important one'
'pi_postcode'           '97223'
  'pi_state'           'Oregon'
'pi_surname'           'Guy'
    'pi_tel'           '+1 (503) 639-5777'
'pub_location'           'Earth'
'release_status' 'Directly after the submission has been processed' 
'thumbnail_file'           'mythumb.png'
'upload_entry_author_file'           '' 
     details           'a deposition'
         doi           '' 
     editors           'Lebowski, M:::Quintana, J'
       issue           '10'
     journal           'Journal of important stuff'
    language           'English'
   published           Yes
   publisher           'Corporate overlords'
    pubmedid           '12345'
      volume           '90'
        year           '2023'
 
