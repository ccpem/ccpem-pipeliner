
# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_job

_rlnJobTypeLabel    relion.map_utilities.filter_map
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_advanced'           No 
'do_highpass'           No
'do_lowpass'           Yes
'filter_direction' non-directional 
'filter_edge_width'            2 
 'input_map'           'emd_2660_small.map'
    highpass            1
     lowpass            12
 
