
# version 50001
# CCP-EM Pipeliner version 1.1.0

data_job

_rlnJobTypeLabel    cryodann.select.particles
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  batch_size          128 
 dann_epochs            2 
    do_queue           No 
       gamma         0.01 
 input_stack job008_lp10.mrcs 
input_starfile  job008.star 
          lr        0.001 
min_dedicated            1 
  other_args           "" 
        qsub         qsub 
  qsubscript           "" 
   queuename      openmpi 
 run_cryovae          Yes 
   threshold        0.006 
   train_set rel_training_data 
    vae_beta          0.1 
  vae_epochs            2 
 
