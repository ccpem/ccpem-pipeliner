# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                            relion.reproject
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
map_filename		emd_3488.mrc
angles_filename		reproj_angles.star
nr_uniform			10
apix				1.07
do_queue         	No 
min_dedicated       1 
qsub       			qsub 
qsubscript 			/public/EM/RELION/relion/bin/relion_qsub.csh 
queuename    		openmpi 
other_args         	"" 