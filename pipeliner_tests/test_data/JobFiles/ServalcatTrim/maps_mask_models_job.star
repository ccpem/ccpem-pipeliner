
# version 50001
# CCP-EM Pipeliner version 1.1.0

data_job

_rlnJobTypeLabel    servalcat.map_utilities.trim
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    do_queue           No 
    do_shift          Yes 
  input_maps Refine3D/job029/run_half2_class001_unfil.mrc:::Refine3D/job029/run_half1_class001_unfil.mrc:::Refine3D/job029/run_class001.mrc 
input_models RefmacServalcat/job047/refined.mmcif:::RefmacServalcat/job047/refined.pdb 
        mask MaskCreate/job020/mask.mrc 
mask_threshold          0.5 
min_dedicated            1 
 noncentered          Yes 
    noncubic          Yes 
  other_args           "" 
     padding          5.0 
        qsub         qsub 
  qsubscript           "" 
   queuename      openmpi 
 shifts_json           "" 
 
