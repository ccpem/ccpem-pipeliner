
# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_job

_rlnJobTypeLabel    servalcat.map_utilities.trim
_rlnJobIsContinue    0
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
'input_maps'           'Refine3D/job001/run_class001.mrc'
        mask           ''
'input_models'           ''
     padding            5
'mask_threshold'           0.5
    noncubic          No
 noncentered          No
    do_shift          Yes
'shifts_json'         'ServalcatTrim/job002/trim_shifts.json'
  'do_queue'           No
'min_dedicated'            1
'other_args'           ''
        qsub         qsub
  qsubscript           ''
   queuename      openmpi
 
