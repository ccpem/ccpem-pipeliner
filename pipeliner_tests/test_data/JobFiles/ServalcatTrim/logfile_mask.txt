# Servalcat ver. 0.2.95 (Python 3.9.13)
# Library vers. gemmi 0.5.4, scipy 1.9.3, numpy 1.23.3, pandas 1.5.0
# Started on 2023-09-21 11:29:11.864874
# Host: SCLT198MAC User: tjg99114
# Command-line args:
# trim --maps Refine3D/job001/run_class001.mrc Refine3D/job001/run_class002.mrc --mask mask.mrc
Input map files:
Reading CCP4/MRC map file Refine3D/job001/run_class001.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:44:51

Reading CCP4/MRC map file Refine3D/job001/run_class002.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:44:51

Using mask to decide border: mask.mrc
Reading CCP4/MRC map file mask.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:26:13

Original grid start:    0    0    0
         grid   end:  255  255  255
Limits: (89, 167) (70, 186) (53, 203)
Padding: 9 9 9
Centered trimming will be performed.
Rad0= 48.5
Rad1= 67.5
Rad2= 84.5
Slices: [slice(79, 177, None), slice(60, 196, None), slice(43, 213, None)]
Cubic Slices: [slice(43, 213, None), slice(43, 213, None), slice(43, 213, None)]
Shift for model: -53.52791171875 -53.52791171875 -53.52791171875
New Cell: 211.6220 211.6220 211.6220 90.000 90.000 90.000
New grid: 170 170 170
Slicing Refine3D/job001/run_class001.mrc
Reading CCP4/MRC map file Refine3D/job001/run_class001.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:44:51

Writing map file: run_class001_trimmed.mrc
Slicing Refine3D/job001/run_class002.mrc
Reading CCP4/MRC map file Refine3D/job001/run_class002.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:44:51

Writing map file: run_class002_trimmed.mrc
Slicing mask.mrc
Reading CCP4/MRC map file mask.mrc
   Cell Grid:  256  256  256
    Map mode: 2
       Start:    0    0    0
       Shape:  256  256  256
        Cell: 318.6778 318.6778 318.6778 90.0 90.0 90.0
  Axis order: X Y Z
 Space group: 0
     Spacing: 1.244835 1.244835 1.244835
  Voxel size: 1.244835 1.244835 1.244835
      Origin: 0.000000e+00 0.000000e+00 0.000000e+00
       Label: Relion    01-Jun-21  16:26:13

Writing map file: mask_trimmed.mrc

# Finished on 2023-09-21 11:29:12.940684