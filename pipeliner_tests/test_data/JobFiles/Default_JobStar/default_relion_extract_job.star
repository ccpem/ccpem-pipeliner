
# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_job

_rlnJobTypeLabel    relion.extract
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'bg_diameter'           -1 
'black_dust'           -1 
'coords_suffix'           '' 
'do_float16'          Yes 
'do_fom_threshold'           No 
 'do_invert'          Yes 
   'do_norm'          Yes 
  'do_queue'           No 
'do_recenter'           No 
'do_reextract'          No
'do_rescale'           No
'do_reset_offsets'           No 
'extract_size'          128 
'fndata_reextract'           '' 
'min_dedicated'            1 
'minimum_pick_fom'            0 
'mpi_command' 'mpirun -n XXXmpinodesXXX' 
    'nr_mpi'            1 
'other_args'           '' 
'recenter_x'            0 
'recenter_y'            0 
'recenter_z'            0 
 'star_mics'           '' 
'white_dust'           -1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
     rescale          128 
 
