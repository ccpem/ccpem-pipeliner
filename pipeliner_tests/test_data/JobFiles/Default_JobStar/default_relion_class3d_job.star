# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.class3d
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'allow_coarser'           No 
'ctf_intact_first_peak'           No 
'do_blush'                    No
'do_combine_thru_disc'           No
'do_ctf_correction'          Yes 
'do_local_ang_searches'           No 
   'do_fast_subsets'            No
   'do_pad1'           No
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
'do_zero_mask'          Yes 
'dont_skip_align'          Yes 
   'fn_cont' '' 
    'fn_img'           '' 
   'fn_mask'           '' 
    'fn_ref'           '' 
   'gpu_ids'           '' 
'highres_limit'           -1 
  'ini_high'           60 
'min_dedicated'            1 
'nr_classes'            1 
   'nr_iter'           25 
'nr_mpi'             1
'mpi_command'       'mpirun -n XXXmpinodesXXX'
   'nr_pool'            3
'nr_threads'            1 
'offset_range'            5 
'offset_step'            1 
'other_args'           '' 
'particle_diameter'          200 
'ref_correct_greyscale'           No 
'scratch_dir'       None 
'sigma_angles'          5.0 
'skip_gridding'          Yes 
  'sym_name'           C1 
 'tau_fudge'            4 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
    sampling '7.5 degrees' 