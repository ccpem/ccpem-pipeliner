# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.import.movies
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'beamtilt_x'          0.0 
'beamtilt_y'          0.0 
 'fn_in_raw' Movies/*.tif
    'fn_mtf'           '' 
'is_multiframe'          Yes 
'optics_group_name' opticsGroup1 
          Cs          2.7 
          Q0          0.1 
      angpix          1.4 
          kV          300
   do_raw        Yes
   do_other         No
   is_synthetic     No
   microscope_data_file ''
   validate_output    Yes
