# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel     relion.autopick.log
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'do_read_fom_maps'           No 
'do_write_fom_maps'           No 
'fn_input_autopick'           '' 
'log_adjust_thr'            0 
'log_diam_max'          250 
'log_diam_min'          200 
'log_invert'           No 
'log_maxres'           20 
'log_upper_thr'        999.99
'min_dedicated'            1 
    'nr_mpi'            1
    'mpi_command'       'mpirun -n XXXmpinodesXXX'
'other_args'           '' 
      angpix           -1 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      do_log         Yes
      do_refs        No
      do_ref3d       No
      do_topaz      No