# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.multibody.refine
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_blush'               No
'do_combine_thru_disc'           No
   'do_pad1'           No 
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
'do_subtracted_bodies'          Yes 
 'fn_bodies'           '' 
   'fn_cont'           '' 
     'fn_in'           '' 
   'gpu_ids'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'mpi_command'       'mpirun -n XXXmpinodesXXX'
   'nr_pool'            3
'nr_threads'            1 
'offset_range'            3 
'offset_step'         0.75 
'other_args'           '' 
'scratch_dir'       None 
'skip_gridding'          Yes 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
    sampling '1.8 degrees' 