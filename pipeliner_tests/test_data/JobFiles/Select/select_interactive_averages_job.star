# version 30001

data_job

_rlnJobTypeLabel                     relion.select.interactive
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
discard_label rlnImageName 
discard_sigma          4 
do_discard         No 
  do_queue         No 
 do_random         No 
do_recenter        Yes 
do_regroup        Yes 
do_remove_duplicates         No 
do_select_values         No 
  do_split         No 
duplicate_threshold         30 
   fn_data         "" 
    fn_mic         "" 
  fn_model         "Class2D/job006/run_it025_optimiser.star"
image_angpix         -1 
min_dedicated          1 
 nr_groups         60 
  nr_split         -1 
other_args         "" 
      qsub     sbatch 
qsubscript "" 
 queuename      scarf 
select_label rlnCtfMaxResolution 
select_maxval      9999. 
select_minval     -9999. 
split_size        100 
do_class_ranker         No
rank_threshold			0.5
do_rank_relative		No 