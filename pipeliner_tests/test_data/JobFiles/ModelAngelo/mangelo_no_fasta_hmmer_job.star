
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    modelangelo
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    'fn_map'           'my_map.map'
'p_seq'           ''
'do_hmmer'      Yes
'fn_lib'        mylib.fasta
'alphabet'      DNA
'F1'            0.1
'F2'            0.001
'F3'            1
'E'               100

 
