
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    modelangelo
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    'fn_map'           ''
'p_seq'           ''
'do_hmmer'      Yes
'fn_lib'        'mylib.fasta'
'hmmer_input'   'ModelAngelo/job001/protein_model.cif'
'alphabet'      RNA
'F1'            0.10
'F2'            0.01
'F3'            101
'E'               102
 
