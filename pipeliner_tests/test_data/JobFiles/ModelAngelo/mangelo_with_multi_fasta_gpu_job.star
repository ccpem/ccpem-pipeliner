
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    modelangelo
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    'fn_map'           'my_map.mrc'
'p_seq'           'my_protein_sequence.fasta'
'd_seq'           'my_dna_sequence.fasta'
'r_seq'           'my_rna_sequence.fasta'
'gpu_ids'                   '0,1'
 'do_hmmer'      No
