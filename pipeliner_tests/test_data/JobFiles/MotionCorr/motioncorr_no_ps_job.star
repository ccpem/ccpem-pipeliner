
# version 30001

data_job

_rlnJobTypeLabel                    relion.motioncorr.motioncor2
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
   bfactor        150 
bin_factor          1 
do_dose_weighting         Yes 
do_own_motioncor        No 
  do_queue         No 
dose_per_frame          1.277 
first_frame_sum          1 
 fn_defect         "" 
fn_gain_ref Movies/gain.mrc 
fn_motioncor2_exe /public/EM/MOTIONCOR2/MotionCor2 
 gain_flip "No flipping (0)" 
  gain_rot "No rotation (0)" 
   gpu_ids          0 
group_for_ps          4 
group_frames          3 
input_star_mics Import/job001/movies.star 
last_frame_sum         0 
min_dedicated          1 
    nr_mpi          1 
nr_threads         24 
other_args         "" 
other_motioncor2_args         "some other args" 
   patch_x          5 
   patch_y          5 
pre_exposure          0 
      qsub       qsub 
qsubscript "" 
do_save_noDW	No
do_save_ps	No
queuename    openmpi
eer_grouping         32 