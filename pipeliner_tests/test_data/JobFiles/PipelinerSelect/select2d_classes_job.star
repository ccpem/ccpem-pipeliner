
# version 50001
# CCP-EM Pipeliner version 1.0.1

data_job

_rlnJobTypeLabel    pipeliner.select.classes
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_recenter'          No
  'do_clavs'          Yes 
'do_particles'          No
  'do_queue'           No 
'fn_optimiser'    'Class2D/job013/run_it100_optimiser.star'
'min_dedicated'            1 
     classes           '1,3,6,19,27'
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
