
# version 50001
# CCP-EM Pipeliner version 1.0.1

data_job

_rlnJobTypeLabel    pipeliner.select.classes
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'do_recenter'          Yes
  'do_clavs'          Yes 
'do_particles'          Yes
  'do_queue'           No 
'fn_optimiser'    'Class3D/job016/run_it025_optimiser.star'
'min_dedicated'            1 
     classes           '1, 2'
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
