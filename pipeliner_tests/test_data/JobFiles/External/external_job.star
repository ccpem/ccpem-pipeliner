# version 30001

data_job

_rlnJobTypeLabel                      relion.External
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  do_queue         No 
    fn_exe   /path/to/external/here.exe 
  in_3dref         Import/job001/3dref.mrc 
 in_coords         "" 
   in_mask         "" 
    in_mic         "" 
    in_mov   	""
   in_part         "" 
min_dedicated          1 
nr_threads          16 
other_args         "" 
param10_label         "" 
param10_value         "" 
param1_label     	 test_param1 
param1_value     	 1001 
param2_label         test_param2
param2_value         2002
param3_label         test_param3
param3_value         3003 
param4_label         test_param4
param4_value         4004
param5_label         test_param5
param5_value         5005
param6_label         test_param6
param6_value         6006
param7_label         test_param7
param7_value         7007
param8_label         test_param8
param8_value         8008
param9_label         test_param9
param9_value         9009
param10_label         test_param10
param10_value         1010
qsub       qsub 
qsubscript "" 
queuename    openmpi 
fn_output ""
output_nodetype ""
alt_nodetype ""
output_kwds ""