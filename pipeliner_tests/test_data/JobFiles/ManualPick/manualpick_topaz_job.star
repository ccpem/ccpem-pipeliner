# version 30001

data_job

_rlnJobTypeLabel                      relion.manualpick
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    angpix         0.885 
 black_val          0 
blue_value          0 
color_label rlnParticleSelectZScore 
  ctfscale          1 
  diameter        200 
  do_color         No 
  do_queue         No 
do_startend         No 
  fn_color         "" 
     fn_in  CtfFind/job003/micrographs_ctf.star
  highpass         -1 
   lowpass         20 
  micscale        0.25 
min_dedicated          1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
 red_value          2 
sigma_contrast          3 
 white_val          0 
 do_topaz_denoise	Yes
 fn_topaz_exec		public/EM/TOPAZ/topaz
 do_fom_threshold 	No
minimum_pick_fom 	0