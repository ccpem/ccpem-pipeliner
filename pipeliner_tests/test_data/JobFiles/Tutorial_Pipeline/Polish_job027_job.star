
# version 30001

data_job

_rlnJobType                            20
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
do_own_params         No 
do_param_optim        Yes 
 do_polish         No 
  do_queue         No 
 eval_frac        0.5 
first_frame          1 
   fn_data Refine3D/job025/run_data.star 
    fn_mic MotionCorr/job002/corrected_micrographs.star 
   fn_post PostProcess/job026/postprocess.star 
last_frame         -1 
    maxres         -1 
min_dedicated         24 
    minres         20 
    nr_mpi          1 
nr_threads         16 
opt_params         "" 
optim_min_part       4000 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
 sigma_acc          2 
 sigma_div       5000 
 sigma_vel        0.2 