
# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_job

_rlnJobTypeLabel    dynamight
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'adarg_inverse'           '' 
 'adarg_rec'           '' 
'backproject_batchsize'           10 
'do_est_motion'        Yes
'do_inverse'           Yes
'do_preload'           No 
  'do_queue'           No 
'do_reconstruct'           Yes
'do_store_deform'           No 
'do_visualize'           No 
'fn_checkpoint'           ''
'fn_dynamight_exe'    dynamight 
    'fn_map'           'Refine3D/job001/map.mrc'
   'fn_star'           'Refine3D/job001/data.star'
    'gpu_id'            0 
'initial_threshold'           -1 
'min_dedicated'            1 
 'nr_epochs'          200 
'nr_gaussians'        10000 
'nr_threads'            1 
'reg_factor'            1 
     halfset            1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
