# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                       2


# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_
_rlnPipeLineProcessName #1
_rlnPipeLineProcessAlias #2
_rlnPipeLineProcessTypeLabel #3
_rlnPipeLineProcessStatusLabel #4
ProShade/job001/       None           proshade.map_utilities.rebox           Succeeded

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_
_rlnPipeLineNodeName #1
_rlnPipeLineNodeTypeLabel #2
ProShade/job001/reboxed_map.mrc       DensityMap.mrc.proshade.reboxed
test.mrc        DensityMap.mrc

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_
_rlnPipeLineEdgeFromNode #1
_rlnPipeLineEdgeProcess #2
test.mrc     ProShade/job001/

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_
_rlnPipeLineEdgeProcess #1
_rlnPipeLineEdgeToNode #2
ProShade/job001/    ProShade/job001/reboxed_map.mrc