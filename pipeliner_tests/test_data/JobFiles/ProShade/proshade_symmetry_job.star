
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    proshade.map_analysis.symmetry
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
 'input_map'           'test.mrc' 
'min_dedicated'            1 
'other_args'           '' 
 'point_sym'           '' 
        qsub         qsub 
  qsubscript           'sub.sh' 
   queuename      openmpi 
 
