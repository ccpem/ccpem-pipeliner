# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.initialmodel
 
_rlnJobIsContinue    0

_rlnJobIsTomo				0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'ctf_intact_first_peak'           No 
'do_combine_thru_disc'           No 
'do_ctf_correction'          Yes 
   'do_pad1'           No 
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
 'do_run_C1'          No 
'do_solvent'          Yes 
   'fn_cont' ''
    'fn_img'           'Select/job014/particles.star' 
   'gpu_ids'           '' 
'min_dedicated'            1 
'nr_classes'            1 
   'nr_iter'          200 
   'nr_pool'            3 
'nr_threads'            1 
'offset_range'            6 
'offset_step'            2 
'other_args'           '' 
'particle_diameter'          200 
'scratch_dir'       None 
'skip_gridding'          Yes 
  'sym_name'           C4 
 'tau_fudge'            4 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
    sampling '15 degrees' 