
# version 50001
# CCP-EM Pipeliner version 1.0.0

data_job

_rlnJobTypeLabel    cryodrgn.abinit_homo
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'advanced_options'           No 
'batch_size'            8 
'cryoDRGN_other_args'           '' 
  'do_queue'           No 
    'fn_img' 'Refine3D/job027/run_data.star' 
'learning_rate'       0.0001 
'log_interval'         1000 
'min_dedicated'            1 
'num_epochs'            1 
 'rand_seed'            0 
'uninvert_data'           No 
 'use_chunk'          Yes 
'weight_decay'          0.0 
  checkpoint            1 
       chunk         1000 
  downsample           32 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
