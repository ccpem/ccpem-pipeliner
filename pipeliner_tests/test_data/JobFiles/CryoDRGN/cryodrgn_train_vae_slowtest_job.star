
# version 50001
# CCP-EM Pipeliner version 1.0.0

data_job

_rlnJobTypeLabel    cryodrgn.train_vae
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'advanced_options'          Yes 
'batch_size'            8 
'continue_from_weights'           No 
'cryoDRGN_other_args'           '' 
   'dec_dim'           64 
'dec_layers'            1 
  'do_queue'           No 
   'enc_dim'           64 
'enc_layers'            1 
    'fn_img' 'Refine3D/job027/run_data.star' 
'learning_rate'       0.0001 
'log_interval'         1000 
'max_threads'           16 
'min_dedicated'            1 
'num_epochs'            1 
 'rand_seed'            0 
'uninvert_data'           No 
 'use_chunk'          Yes 
'weight_decay'          0.0 
  checkpoint            1 
       chunk         1000 
  downsample           32 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
     weights           '' 
        zdim            8 