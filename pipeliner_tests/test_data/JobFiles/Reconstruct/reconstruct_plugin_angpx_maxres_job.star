# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                            relion.reconstruct
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
input_particles		test_particles.star
angpix				-1
sym					C1
maxres				2
do_mpi				No
nr_mpi				1	
do_queue         	No 
min_dedicated       1 
qsub       			qsub 
qsubscript 			/public/EM/RELION/relion/bin/relion_qsub.csh 
queuename    		openmpi 
other_args         	""
do_ctf              Yes