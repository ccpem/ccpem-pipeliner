# version 30001

data_job

_rlnJobTypeLabel                     relion.import
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
        Cs        2.7 
        Q0        0.1 
    angpix        1.4 
beamtilt_x          0 
beamtilt_y          0 
  do_other        Yes 
  do_queue         No 
    do_raw         No 
fn_in_other emd_3488_mask.mrc 
 fn_in_raw Micrographs/*.tif 
    fn_mtf         "" 
is_multiframe        Yes 
        kV        300 
min_dedicated          1 
 pipeliner_node_type Mask3D
optics_group_name opticsGroup1
optics_group_particles         "" 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi
 is_relion           No