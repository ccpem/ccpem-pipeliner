
# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_job

_rlnJobTypeLabel    relion.import.movies
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'beamtilt_x'          0.0 
'beamtilt_y'          0.0 
  'do_other'           No 
    'do_raw'          Yes 
 'fn_in_raw' Movies/*.mrc
    'fn_mtf'           '' 
'is_multiframe'          No
'is_synthetic'           No 
'microscope_data_file'           '' 
'optics_group_name' opticsGroup1 
          Cs          2.7 
          Q0          0.1 
      angpix          1.4 
          kV          300 
 
