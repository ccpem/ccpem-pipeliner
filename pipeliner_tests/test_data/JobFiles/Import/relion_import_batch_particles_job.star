
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    relion.import
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'alt_nodetype'           '' 
     'fn_in_other'           'd1/parts.star:::d2/parts.star:::d3/parts.star'
'is_synthetic'           No
        kwds           ''
    node_type           'Particles STAR file (.star)'
 pipeliner_node_type    ''
 is_relion           Yes
