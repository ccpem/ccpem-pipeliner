
# version 30001

data_job
_rlnJobIsTomo				1
_rlnJobTypeLabel                             relion.import.movies
_rlnJobIsContinue                       0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionValue #1
_rlnJobOptionVariable #2 
        1.4        Cs 
	0.1 	Q0
	0.885 	angpix 
          0 	beamtilt_x
          0 	beamtilt_y
           No 	do_queue
            Yes 	do_raw
  Movies/*.tiff		fn_in_raw
             "" 	fn_mtf
        Yes 	is_multiframe
                200 	kV
          1 	min_dedicated
  "Particle coordinates (*.box, *_pick.star)" 	node_type
 opticsGroup1 	optics_group_name
         "" 	other_args
 
