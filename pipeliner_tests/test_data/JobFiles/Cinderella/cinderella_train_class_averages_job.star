
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    cinderella.select.class_averages.train
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'bad_path'           'path/to/bad'
'batch_size'           32 
    'do_gpu'           Yes
 'good_path'           'path/to/good'
'input_size'           64 
'mask_radius'           -1 
'nb_early_stop'           15 
  'nb_epoch'          100 
'pretrained_weightes'           '' 
         gpu           '1'
 
