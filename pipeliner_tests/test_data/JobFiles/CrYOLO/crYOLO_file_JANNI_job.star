# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                            cryolo.autopick
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
input_file				CtfFind/job003/micrographs_ctf.star 	
input_dir				""
model_path				cryolomodel.h5	
box_size				256
gpus					"0, 1, 2, 3"				# Separate with a comma 
confidence_threshold	0.3
use_JANNI_denoise		Yes
JANNI_model_path		path/to/JANNI.h5	# only needed if use_JANNI is Yes
do_queue         		No 
min_dedicated       	1	 
qsub       				qsub 
qsubscript 				/public/EM/RELION/relion/bin/relion_qsub.csh 
queuename    			openmpi 
other_args         		"" 