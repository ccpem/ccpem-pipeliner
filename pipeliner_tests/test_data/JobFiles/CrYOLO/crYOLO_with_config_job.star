
# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_job

_rlnJobTypeLabel    cryolo.autopick
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'JANNI_model_path'           'janni.h5'
  'box_size'          128 
'confidence_threshold'          0.3 
'config_file'           '/cryolofiles/my_cryolo_config.json'
  'do_queue'           No 
'input_file'           'CtfFind/job003/micrographs_ctf.star'
'min_dedicated'            1 
'model_path'           'model.h5'
'nr_threads'           2
'use_JANNI_denoise'           No 
        gpus           '' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
