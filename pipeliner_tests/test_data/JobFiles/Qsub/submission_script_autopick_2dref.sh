#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e AutoPick/job001/run.err
#$ -o AutoPick/job001/run.out
#$ -P openmpi
#$  -M Here is number 1    # put your email address here 
#$  -l coproc_v100=4   # GPUS in relion should be left blank
 
## load modules 
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
Here is number 2
Here is number 3

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info 
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
mpirun -n 6 relion_autopick_mpi --i CtfFind/job003/micrographs_ctf.star --odir AutoPick/job001/ --pickname autopick --ref Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 --max_stddev_noise -1 --gpu 0:1:2:3 --pipeline_control AutoPick/job001/

#One more extra command for good measure 
Here is number 4
dedicated nodes: 24
output name: AutoPick/job001/
