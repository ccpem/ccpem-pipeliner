#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e run.err
#$ -o run.out
#$ -P openmpi
#$  -M 101    # put your email address here
#$  -l coproc_v100=4   # GPUS in relion should be left blank

## load modules
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
202
303

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
python3 job_script.py ../../Import/job001/3488_run_half1_class001_unfil.mrc ../../Import/job001/3488_run_half2_class001_unfil.mrc 3.2 None ../../Import/job003/3488_5me2.fasta 5me2_a_to_move ../../Import/job002/5me2_a_to_move.pdb 1.5 em_placement.phil
path/to/ccp4/libexec/phaser_voyager.em_placement em_placement.phil

#One more extra command for good measure
404
dedicated nodes: 1
output name: EMPlacement/job001/