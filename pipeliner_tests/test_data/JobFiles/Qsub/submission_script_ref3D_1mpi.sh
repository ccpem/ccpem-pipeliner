#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e Refine3D/job001/run.err
#$ -o Refine3D/job001/run.out
#$ -P openmpi
#$  -M Here is number 1    # put your email address here 
#$  -l coproc_v100=4   # GPUS in relion should be left blank
 
## load modules 
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
Here is number 2
Here is number 3

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info 
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
relion_refine --i Extract/job018/particles.star --o Refine3D/job001/run --auto_refine --split_random_halves --ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc --ini_high 50 --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2 --ctf --particle_diameter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job001/

#One more extra command for good measure 
Here is number 4
dedicated nodes: 24
output name: Refine3D/job001/
