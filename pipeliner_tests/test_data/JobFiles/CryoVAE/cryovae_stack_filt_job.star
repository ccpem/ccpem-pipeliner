
# version 50001
# CCP-EM Pipeliner version 1.1.0

data_job

_rlnJobTypeLabel    cryovae.image_analysis
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
        beta          0.1 
    do_queue           No 
      epochs          100 
input_raw_stack           "Refine3D/job002/data.mrcs"
  input_star           ""
lowpass_filter          Yes
min_dedicated            1 
  other_args           "" 
        qsub         qsub 
  qsubscript           "" 
   queuename      openmpi 
 
