# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                            pipeliner.select.random_sample
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_
_rlnJobOptionVariable #1
_rlnJobOptionValue #2
fn_movs         movies.star
fn_mics         ""
fn_parts        ""
fn_coord_files  ""
fn_coords		""
n_samples       10