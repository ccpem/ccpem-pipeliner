
# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_job

_rlnJobTypeLabel    relion.map_utilities.align_symmetry
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.5.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'box_size'           64 
 'input_map'           'Refine3D/job001/mymap.mrc:::Refine3D/job002/mymap.mrc'
'do_recentre'          Yes
'nr_uniform'          400 
  'only_rot'           No 
        fold            1 
         sym 'Cyclic (C)' 
 
