
# version 50001
# CCP-EM Pipeliner version 1.1.0

data_job

_rlnJobTypeLabel    relion.image_analysis.symmetry_expansion
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
         asu            1 
    do_helix           Yes
    do_queue           No 
  frac_range          0.5 
frac_sampling            1
input_particles           "SymmetryExpansion/job001/particles_short.star"
min_dedicated            1 
  other_args           "" 
        qsub         qsub 
  qsubscript           "" 
   queuename      openmpi 
        rise            4.8
    symmetry           T
       twist            30
 
