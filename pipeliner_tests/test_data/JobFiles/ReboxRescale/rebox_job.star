
# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_job

_rlnJobTypeLabel    relion.map_utilities.rebox_rescale_map
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_rebox'            Yes
  'box_size'          100
 'input_map'           'test.mrc'
 'do_rescale'           No
 'rescale_angpix'       1.0
 
