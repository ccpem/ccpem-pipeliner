
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnMicrographPixelSize #8 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     0.885000 
 

# version 30001

data_micrographs

loop_ 
_rlnCtfPowerSpectrum #1 
_rlnMicrographName #2 
_rlnMicrographMetadata #3 
_rlnOpticsGroup #4 
_rlnAccumMotionTotal #5 
_rlnAccumMotionEarly #6 
_rlnAccumMotionLate #7 
MotionCorr/job002/Movies/20170629_00021_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00001_frameImage.mrc MotionCorr/job002/Movies/20170629_00001_frameImage.star            1    16.420495     2.506308    13.914187 
MotionCorr/job002/Movies/20170629_00022_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00002_frameImage.mrc MotionCorr/job002/Movies/20170629_00002_frameImage.star            1    19.551677     2.478968    17.072709 
MotionCorr/job002/Movies/20170629_00023_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00003_frameImage.mrc MotionCorr/job002/Movies/20170629_00003_frameImage.star            1    17.547827     1.941103    15.606724 
MotionCorr/job002/Movies/20170629_00024_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00004_frameImage.mrc MotionCorr/job002/Movies/20170629_00004_frameImage.star            1    18.100817     1.722567    16.378250 
MotionCorr/job002/Movies/20170629_00025_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00005_frameImage.mrc MotionCorr/job002/Movies/20170629_00005_frameImage.star            1    24.129034     3.569005    20.560029 
MotionCorr/job002/Movies/20170629_00026_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00006_frameImage.mrc MotionCorr/job002/Movies/20170629_00006_frameImage.star            1    13.157146     1.829366    11.327780 
MotionCorr/job002/Movies/20170629_00027_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00007_frameImage.mrc MotionCorr/job002/Movies/20170629_00007_frameImage.star            1    15.087258     1.446982    13.640275 
MotionCorr/job002/Movies/20170629_00028_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00008_frameImage.mrc MotionCorr/job002/Movies/20170629_00008_frameImage.star            1    13.800697     1.106689    12.694009 
MotionCorr/job002/Movies/20170629_00029_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00009_frameImage.mrc MotionCorr/job002/Movies/20170629_00009_frameImage.star            1    16.182733     1.525147    14.657585 
MotionCorr/job002/Movies/20170629_00030_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00010_frameImage.mrc MotionCorr/job002/Movies/20170629_00010_frameImage.star            1    16.742190     1.883503    14.858687 
MotionCorr/job002/Movies/20170629_00031_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00011_frameImage.mrc MotionCorr/job002/Movies/20170629_00011_frameImage.star            1    13.607894     1.499256    12.108638 
MotionCorr/job002/Movies/20170629_00035_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00012_frameImage.mrc MotionCorr/job002/Movies/20170629_00012_frameImage.star            1    15.636644     1.686942    13.949702 
MotionCorr/job002/Movies/20170629_00036_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00013_frameImage.mrc MotionCorr/job002/Movies/20170629_00013_frameImage.star            1    12.979715     0.765371    12.214344 
MotionCorr/job002/Movies/20170629_00037_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00014_frameImage.mrc MotionCorr/job002/Movies/20170629_00014_frameImage.star            1    13.647807     1.401435    12.246372 
MotionCorr/job002/Movies/20170629_00039_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00015_frameImage.mrc MotionCorr/job002/Movies/20170629_00015_frameImage.star            1    12.160827     1.452939    10.707888 
MotionCorr/job002/Movies/20170629_00040_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00016_frameImage.mrc MotionCorr/job002/Movies/20170629_00016_frameImage.star            1    13.632912     1.228787    12.404124 
MotionCorr/job002/Movies/20170629_00042_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00017_frameImage.mrc MotionCorr/job002/Movies/20170629_00017_frameImage.star            1    15.115522     1.151673    13.963849 
MotionCorr/job002/Movies/20170629_00043_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00018_frameImage.mrc MotionCorr/job002/Movies/20170629_00018_frameImage.star            1    13.547137     1.361030    12.186107 
MotionCorr/job002/Movies/20170629_00044_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00019_frameImage.mrc MotionCorr/job002/Movies/20170629_00019_frameImage.star            1    15.221134     1.761495    13.459639 
MotionCorr/job002/Movies/20170629_00045_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00020_frameImage.mrc MotionCorr/job002/Movies/20170629_00020_frameImage.star            1    10.418015     1.084238     9.333777 
MotionCorr/job002/Movies/20170629_00046_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00021_frameImage.mrc MotionCorr/job002/Movies/20170629_00021_frameImage.star            1    11.558493     0.921003    10.637490 
MotionCorr/job002/Movies/20170629_00047_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00022_frameImage.mrc MotionCorr/job002/Movies/20170629_00022_frameImage.star            1    13.504645     0.801910    12.702735 
MotionCorr/job002/Movies/20170629_00048_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00023_frameImage.mrc MotionCorr/job002/Movies/20170629_00023_frameImage.star            1    12.203512     0.881317    11.322195 
MotionCorr/job002/Movies/20170629_00049_frameImage_PS.mrc MotionCorr/job002/Movies/20170629_00024_frameImage.mrc MotionCorr/job002/Movies/20170629_00024_frameImage.star            1    13.507392     1.432063    12.075329 
 
