# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      20
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/       None            relion.import.movies            Succeeded 
MotionCorr/job002/       None            relion.motioncorr.own            Succeeded  
CtfFind/job003/       None            relion.ctffind.ctffind4            Succeeded  
AutoPick/job004/       None            relion.autopick.log           Succeeded 
Extract/job005/       None            relion.extract           Succeeded 
Class2D/job006/       None            relion.class2d            Succeeded 
Select/job007/       None            relion.select.interactive            Succeeded 
InitialModel/job008/       None           relion.initialmodel            Succeeded 
Class3D/job009/       None            relion.class3d            Succeeded 
Refine3D/job010/       None           relion.refine3d            Succeeded 
MultiBody/job011/       None           relion.multibody.refine            Succeeded 
CtfRefine/job012/       None           relion.ctfrefine            Succeeded 
MaskCreate/job013/       None           relion.maskcreate            Succeeded 
Polish/job014/       None           relion.polish            Succeeded 
JoinStar/job015/       None           relion.joinstar.particles            Succeeded 
Subtract/job016/       None           relion.subtract            Succeeded 
PostProcess/job017/       None           relion.postprocess            Succeeded 
External/job018/       None           relion.external            Succeeded 
LocalRes/job019/       None           relion.localres.resmap            Succeeded 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star            MicrographMoviesData.star.relion 
MotionCorr/job002/corrected_micrographs.star            MicrographsData.star.relion 
MotionCorr/job002/logfile.pdf           LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star            MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf           LogFile.pdf.relion.ctffind
AutoPick/job004/coords_suffix_autopick.star            MicrographsCoords.star.relion.autopick 
AutoPick/job004/logfile.pdf           LogFile.pdf.relion.autopick 
Extract/job005/particles.star            ParticleGroupMetadata.star.relion
Class2D/job006/run_it025_data.star            ParticleGroupMetadata.star.relion.class2d
Class2D/job006/run_it025_optimiser.star            OptimiserDataData.star.relion.class2d
Select/job007/selected_particles.star            ParticleGroupMetadata.star.relion
InitialModel/job008/run_it150_class001.mrc            DensityMap.mrc.relion.initialmodel
InitialModel/job008/run_it150_class002.mrc            DensityMap.mrc.relion.initialmodel 
InitialModel/job008/run_it150_class001_data.star            ParticleGroupMetadata.star.relion.initialmodel
InitialModel/job008/run_it150_class002_data.star            ParticleGroupMetadata.star.relion.initialmodel
InitialModel/job008/run_it150_optimiser.star            ProcessData.star.relion.optimiser.initialmodel 
Class3D/job009/run_it025_class001.mrc            DensityMap.mrc.relion.class3d 
Class3D/job009/run_it025_class002.mrc            DensityMap.mrc.relion.class3d
Class3D/job009/run_it025_class003.mrc            DensityMap.mrc.relion.class3d 
Class3D/job009/run_it025_data.star            ParticleGroupMetadata.star.relion.class3d
Class3D/job009/run_it025_optimiser.star            ProcessData.star.relion.optimister.class3d 
Refine3D/job010/run_data.star            ParticleGroupMetadata.star.relion.refine3d
Refine3D/job010/run_optimiser.star            ProcessData.star.relion.optimister.refine3d 
Refine3D/job010/run_class001.mrc            DensityMap.mrc.relion.refine3d 
Refine3D/job010/run_class001_half1_unfil.mrc           DensityMap.mrc.relion.halfmap.refine3d 
MultiBody/job011/run_class001_half1_unfil.mrc           DensityMap.mrc.relion.halfmap.multibody 
MultiBody/job011/run_class002_half1_unfil.mrc           DensityMap.mrc.relion.halfmap.multibody 
CtfRefine/job012/logfile.pdf           LogFile.pdf.relion.ctfrefine 
CtfRefine/job012/particles_ctf_refine.star            ParticleGroupMetadata.star.relion.ctfrefine
MaskCreate/job013/mask.mrc            Mask3D.mrc.relion
Polish/job014/opt_params_all_groups.txt           ProcessData.txt.relion.polishparams
Polish/job014/logfile.pdf           LogFile.pdf.relion.polish  
Polish/job014/shiny.star            ParticleGroupMetadata.star.relion.polished
JoinStar/job015/join_particles.star            ParticleGroupMetadata.star.relion
Subtract/job016/particles_subtract.star            ParticleGroupMetadata.star.relion.subtracted
PostProcess/job017/logfile.pdf          LogFile.pdf.relion.postprocess 
PostProcess/job017/postprocess.star           ProcessData.star.relion.postprocess 
LocalRes/job019/relion_locres_filtered.mrc           DensityMap.mrc.relion.localresfiltered 
LocalRes/job019/relion_locres.mrc           Image3D.mrc.relion.localresmap 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job004/ 
CtfFind/job003/micrographs_ctf.star Extract/job005/ 
AutoPick/job004/coords_suffix_autopick.star Extract/job005/ 
Extract/job005/particles.star Class2D/job006/ 
Class2D/job006/run_it025_optimiser.star  Select/job007/ 
Select/job007/selected_particles.star InitialModel/job008/ 
InitialModel/job008/run_it150_class001.mrc Class3D/job009/ 
Select/job007/selected_particles.star Class3D/job009/ 
Class3D/job009/run_it025_data.star Refine3D/job010/ 
Refine3D/job010/run_data.star MultiBody/job011/ 
MotionCorr/job002/corrected_micrographs.star CtfRefine/job012/ 
Refine3D/job010/run_class001.mrc MaskCreate/job013/ 
MaskCreate/job013/mask.mrc Subtract/job016/ 
Refine3D/job010/run_data.star Subtract/job016/ 
Refine3D/job010/run_class001_half1_unfil.mrc PostProcess/job017/ 
MaskCreate/job013/mask.mrc PostProcess/job017/ 
Refine3D/job010/run_class001.mrc External/job018/ 
Refine3D/job010/run_class001_half1_unfil.mrc LocalRes/job019/ 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
AutoPick/job004/ AutoPick/job004/coords_suffix_autopick.star 
AutoPick/job004/ AutoPick/job004/logfile.pdf 
Extract/job005/ Extract/job005/particles.star 
Class2D/job006/ Class2D/job006/run_it025_data.star 
Class2D/job006/ Class2D/job006/run_it025_optimiser.star  
Select/job007/ Select/job007/selected_particles.star 
InitialModel/job008/ InitialModel/job008/run_it150_class001.mrc 
InitialModel/job008/ InitialModel/job008/run_it150_class002.mrc 
InitialModel/job008/ InitialModel/job008/run_it150_class001_data.star 
InitialModel/job008/ InitialModel/job008/run_it150_class002_data.star 
InitialModel/job008/ InitialModel/job008/run_it150_optimiser.star 
Class3D/job009/ Class3D/job009/run_it025_class001.mrc 
Class3D/job009/ Class3D/job009/run_it025_class002.mrc 
Class3D/job009/ Class3D/job009/run_it025_class003.mrc 
Class3D/job009/ Class3D/job009/run_it025_data.star 
Class3D/job009/ Class3D/job009/run_it025_optimiser.star 
Refine3D/job010/ Refine3D/job010/run_data.star 
Refine3D/job010/ Refine3D/job010/run_optimiser.star 
Refine3D/job010/ Refine3D/job010/run_class001.mrc 
Refine3D/job010/ Refine3D/job010/run_class001_half1_unfil.mrc 
MultiBody/job011/ MultiBody/job011/run_class001_half1_unfil.mrc 
MultiBody/job011/ MultiBody/job011/run_class002_half1_unfil.mrc 
CtfRefine/job012/ CtfRefine/job012/logfile.pdf 
CtfRefine/job012/ CtfRefine/job012/particles_ctf_refine.star 
MaskCreate/job013/ MaskCreate/job013/mask.mrc 
Polish/job014/ Polish/job014/opt_params_all_groups.txt 
Polish/job014/ Polish/job014/logfile.pdf 
Polish/job014/ Polish/job014/shiny.star 
JoinStar/job015/ JoinStar/job015/join_particles.star 
Subtract/job016/ Subtract/job016/particles_subtract.star 
PostProcess/job017/ PostProcess/job017/logfile.pdf 
PostProcess/job017/ PostProcess/job017/postprocess.star 
LocalRes/job019/ LocalRes/job019/relion_locres_filtered.mrc 
LocalRes/job019/ LocalRes/job019/relion_locres.mrc 