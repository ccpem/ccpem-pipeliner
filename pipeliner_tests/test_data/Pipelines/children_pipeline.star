# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                       4
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
PostProcess/job998/       None           relion.postproces            Running
ChildProcess/job004/	  None			 relion.test.generic          Running
ChildProcess/job005/	  None			 relion.test.generic          Running

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job002/emd_3488_mask.mrc            Mask3D.mrc 
Import/job001/3488_run_half1_class001_unfil.mrc           DensityMap.mrc.halfmap 
PostProcess/job998/postprocess.mrc           DensityMap.mrc.relion.postprocess
PostProcess/job998/postprocess_masked.mrc           DensityMap.relion.postprocess.masked
PostProcess/job998/postprocess.star           ProcessData.star.relion.postprocess
PostProcess/job998/logfile.pdf           LogFile.pdf.relion.postprocess
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job002/emd_3488_mask.mrc PostProcess/job998/
Import/job001/3488_run_half1_class001_unfil.mrc PostProcess/job998/
PostProcess/job998/postprocess.mrc ChildProcess/job004/
PostProcess/job998/postprocess.mrc ChildProcess/job005/

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
PostProcess/job998/ PostProcess/job998/postprocess.mrc
PostProcess/job998/ PostProcess/job998/postprocess_masked.mrc
PostProcess/job998/ PostProcess/job998/postprocess.star
PostProcess/job998/ PostProcess/job998/logfile.pdf