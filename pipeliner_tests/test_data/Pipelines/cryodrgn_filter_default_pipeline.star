# This test pipeline file manually edited to reduce size, might not be fully valid
# version 50001
# CCP-EM Pipeliner version 1.0.1

data_pipeline_general

_rlnPipeLineJobCounter    168
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Extract/job159/       None relion.extract    Succeeded 
CryoDRGN/job160/       None cryodrgn.train_vae    Succeeded 
Select/job167/       None cryodrgn.filter    Scheduled 
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Extract/job159/particles.star ParticleGroupMetadata.star.relion 
CryoDRGN/job160/train_32/weights.0.pkl MlModel.pkl.cryodrgn 
CryoDRGN/job160/train_32/weights.1.pkl MlModel.pkl.cryodrgn 
CryoDRGN/job160/train_32/weights.2.pkl MlModel.pkl.cryodrgn 
CryoDRGN/job160/train_32/weights.3.pkl MlModel.pkl.cryodrgn 
CryoDRGN/job160/train_32/weights.4.pkl MlModel.pkl.cryodrgn 
CryoDRGN/job160/train_32/weights.pkl MlModel.pkl.cryodrgn 
Select/job167/selection.star ParticleGroupMetadata.star.cryodrgn 
Select/job167/inverse_selection.star ParticleGroupMetadata.star.cryodrgn 
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Extract/job159/particles.star CryoDRGN/job160/ 
CryoDRGN/job160/train_32/weights.pkl Select/job167/ 
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Extract/job159/ Extract/job159/particles.star 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.0.pkl 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.1.pkl 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.2.pkl 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.3.pkl 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.4.pkl 
CryoDRGN/job160/ CryoDRGN/job160/train_32/weights.pkl 
Select/job167/ Select/job167/selection.star 
Select/job167/ Select/job167/inverse_selection.star 
 
