
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      9
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/ Import/movies/            relion.import.movies            Succeeded 
MotionCorr/job002/ MotionCorr/own/            relion.motioncorr.own            Succeeded 
CtfFind/job003/ CtfFind/gctf/            relion.ctffind.ctffind4            Succeeded 
ManualPick/job004/ ManualPick/illustrate_only/            relion.manualpick            Succeeded 
Select/job005/ Select/5mics/            relion.select            Succeeded 
AutoPick/job006/ AutoPick/LoG_based/            relion.autopick.log            Succeeded 
Extract/job007/ Extract/LoG_based/            relion.extract            Succeeded 
Class2D/job008/ Class2D/LoG_based/            relion.class2d.em            Running 	 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star            MicrographMoviesData.star.relion
MotionCorr/job002/corrected_micrographs.star            MicrographsData.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf           LogFile.pdf.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star            MicrographsData.star.relion.ctf 
CtfFind/job003/logfile.pdf           LogFile.pdf.relion.ctffind 
ManualPick/job004/coords_suffix_manualpick.star            MicrographsCoords.star.relion.manualpick 
ManualPick/job004/micrographs_selected.star           MicrographsData.star.relion 
Select/job005/micrographs_selected.star            MicrographsData.star.relion 
AutoPick/job006/coords_suffix_autopick.star            MicrographsCoords.star.relion.autopick 
AutoPick/job006/logfile.pdf           LogFile.pdf.relion.autopick 
Extract/job007/particles.star            ParticleGroupMetadata.star.relion

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
ManualPick/job004/coords_suffix_manualpick.star Select/job005/ 
Select/job005/micrographs_selected.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/coords_suffix_autopick.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/coords_suffix_manualpick.star 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
Select/job005/ Select/job005/micrographs_selected.star 
AutoPick/job006/ AutoPick/job006/coords_suffix_autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
