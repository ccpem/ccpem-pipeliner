
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnImagePixelSize #8 
_rlnImageSize #9 
_rlnImageDimensionality #10 
opticsGroup1            1 Movies/relion/mtf_300kV.star     1.000000   300.000000     2.700000     0.100000     1.000000          320            2 
 

# version 30001

data_particles

loop_ 
_rlnCoordinateX #1 
_rlnCoordinateY #2 
_rlnAutopickFigureOfMerit #3 
_rlnClassNumber #4 
_rlnAnglePsi #5 
_rlnImageName #6 
_rlnMicrographName #7 
_rlnOpticsGroup #8 
_rlnCtfMaxResolution #9 
_rlnCtfFigureOfMerit #10 
_rlnDefocusU #11 
_rlnDefocusV #12 
_rlnDefocusAngle #13 
_rlnCtfBfactor #14 
_rlnCtfScalefactor #15 
_rlnPhaseShift #16 
_rlnGroupNumber #17 
_rlnAngleRot #18 
_rlnAngleTilt #19 
_rlnOriginXAngst #20 
_rlnOriginYAngst #21 
_rlnNormCorrection #22 
_rlnLogLikeliContribution #23 
_rlnMaxValueProbDistribution #24 
_rlnNrOfSignificantSamples #25 
_rlnRandomSubset #26 
 2604.000000   119.000000     -0.48785            1   171.755108 000001@Extract/job013/Movies/000000.mrcs MotionCorr/job007/Movies/000000.mrc            1     4.506824     0.093808 24842.826172 24707.996094    -67.02640     0.000000     1.000000     0.000000            1    -79.27184    69.403600    -13.33707     1.058430     0.534676 4.291629e+05     0.044452           45            1 

 
