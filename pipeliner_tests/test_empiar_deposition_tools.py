#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from glob import glob
from unittest.mock import patch
from pathlib import Path

from pipeliner import __version__ as pipe_vers
from pipeliner_tests import test_data
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EMPIAR_DEPOSITION_COMMENT,
    prepare_empiar_parts,
    prepare_empiar_raw_mics,
    prepare_empiar_mics,
    EmpiarParticles,
    EmpiarCorrectedMics,
    EmpiarRefinedParticles,
    EmpiarMovieSet,
)
from pipeliner.deposition_tools.empiar_deposition import (
    get_deposition_objects_empiar,
    prepare_empiar_deposition,
    make_empiar_deposition_data_object,
    assemble_deposition_objects_empiar,
    EMPIAR_DEPOSITION_DIR,
    parse_empiar_jobstar,
)
from pipeliner_tests.testing_tools import (
    slow_test,
    get_relion_tutorial_data,
    expected_warning,
)


class DepoToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_make_empiar_data_object_parts(self):
        parts = EmpiarParticles()
        data_obj = make_empiar_deposition_data_object(data_obj=parts)
        assert data_obj.field_name == "empiar"
        assert type(data_obj.data_items) is EmpiarParticles
        assert data_obj.dc_type == EmpiarParticles
        assert data_obj.cif_type == "pairs"
        assert data_obj.merge_strategy == "overwrite"
        assert data_obj.reverse is False

    def test_make_empiar_data_object_movies(self):
        movs = EmpiarMovieSet()
        data_obj = make_empiar_deposition_data_object(data_obj=movs)
        assert data_obj.field_name == "empiar"
        assert type(data_obj.data_items) is EmpiarMovieSet
        assert data_obj.dc_type == EmpiarMovieSet
        assert data_obj.cif_type == "loop"
        assert data_obj.merge_strategy == "multi"
        assert data_obj.reverse is False

    def test_EMPIAR_raw_mics_mrcs(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        for n in range(1, 25):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_empiar_raw_mics("Import/job001/movies.star")
        expected = {
            "name": "Multiframe micrograph movies",
            "directory": "Import/job001/Movies",
            "category": "('T2', '')",
            "header_format": "('T2', '')",
            "data_format": "('OT', '16-bit float')",
            "num_images_or_tilt_series": 24,
            "frames_per_image": 215,
            "voxel_type": "('OT', '16-bit float')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "Voltage 1.4; Spherical aberration 1.4; "
            "Movie data in file: Import/job001/movies.star; "
            f"{EMPIAR_DEPOSITION_COMMENT}",
            "image_width": 64,
            "image_height": 64,
            "micrographs_file_pattern": "Import/job001/Movies"
            "/20170629_000*_frameImage.mrcs",
        }
        assert depoobjs[0].__dict__ == expected

    def test_EMPIAR_raw_mics_mrcs_multiple_dirs(self):
        # setup the import dir
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_empiar_raw_mics("Import/job001/movies.star")

        exp_image_sets = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; "
                "Movie data in file: Import/job001/movies.star; "
                f"{EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629"
                "_000*_frameImage.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; "
                "Movie data in file: Import/job001/movies.star; "
                f"{EMPIAR_DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629"
                "_000*_frameImage.mrcs",
            },
        ]

        for i in depoobjs:
            assert i.__dict__ in exp_image_sets
        for i in exp_image_sets:
            assert i in [x.__dict__ for x in depoobjs]

    @slow_test
    def test_EMPIAR_corr_mics(self):
        # setup the import dir
        os.makedirs("CtfFind/job003/")
        os.makedirs("MotionCorr/job002/Movies")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs_ordered.star"),
            os.path.join(self.test_dir, "CtfFind/job003/micrographs_ctf.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "single_100.mrc")
        for n in range(1, 25):
            target = os.path.join(
                self.test_dir,
                f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc",
            )
            shutil.copy(movie_file, target)

        depoobjs = prepare_empiar_mics("CtfFind/job003/micrographs_ctf.star")

        expected = {
            "name": "Corrected micrographs",
            "directory": "MotionCorr/job002/Movies",
            "category": "('T1', '')",
            "header_format": "('T1', '')",
            "data_format": "('T7', '')",
            "num_images_or_tilt_series": 24,
            "frames_per_image": 1,
            "voxel_type": "('T7', '')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "Voltage 1.4; Spherical aberration 1.4; "
            "Image data in file: CtfFind/job003/micrographs_ctf.star; "
            f"{EMPIAR_DEPOSITION_COMMENT}",
            "image_width": 100,
            "image_height": 100,
            "micrographs_file_pattern": "MotionCorr/job002/Movies/"
            "20170629_000*_frameImage.mrc",
        }

        assert depoobjs[0].__dict__ == expected

    def test_EMPIAR_parts(self):
        """Parts and Corr parts are essentially the same, only need to test 1"""
        # setup the import dir
        os.makedirs("CtfRefine/job024/")
        os.makedirs("Extract/job007/Movies/")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "class2d_data.star"),
            os.path.join(self.test_dir, "CtfRefine/job024/particles_ctf_refine.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")
        for n in list(range(21, 32)) + list(range(35, 41)) + list(range(42, 50)):
            target = os.path.join(
                self.test_dir, f"Extract/job007/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)

        # Expect a numpy overflow warning from image std() calculation
        with expected_warning(RuntimeWarning, "overflow"):
            depoobjs = prepare_empiar_parts(
                "CtfRefine/job024/particles_ctf_refine.star",
            )

        expected = {
            "name": "Particle images",
            "directory": "Extract/job007/Movies",
            "category": "('T5', '')",
            "header_format": "('T2', '')",
            "data_format": "('OT', '16-bit float')",
            "num_images_or_tilt_series": 10,
            "frames_per_image": 1,
            "voxel_type": "('OT', '16-bit float')",
            "pixel_width": 0.885,
            "pixel_height": 0.885,
            "details": "2377 total particles; Voltage"
            " 1.4; Spherical aberration 1.4; Image"
            " data in file: CtfRefine/job024/particles"
            f"_ctf_refine.star; {EMPIAR_DEPOSITION_COMMENT}",
            "image_width": 64,
            "image_height": 64,
            "micrographs_file_pattern": "Extract/job007/Movies/20170629_"
            "000*_frameImage.mrcs",
            "picked_particles_file_pattern": "CtfRefine/job024/particl"
            "es_ctf_refine.star",
        }

        assert depoobjs[0].__dict__ == expected

    @staticmethod
    def mock_movies():
        movpath = Path("Movies")
        movpath.mkdir()
        for n in [
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            35,
            36,
            37,
            39,
            40,
            42,
            43,
            44,
            45,
            46,
            47,
            48,
            49,
        ]:
            (movpath / f"20170629_{n:05d}_frameImage.tiff").touch()

    @slow_test
    @patch("pipeliner.deposition_tools.empiar_deposition_objects.get_tiff_stats")
    def test_get_objs_full_EMPIAR_deposition_all_types_selected(self, ts_patch):
        ts_patch.return_value = [
            [
                (3710, 3838),
                0,
                25,
                0.9467082614063648,
                0.9745422394926604,
                None,
                "16-bit unsigned integer: uint16",
            ]
        ] * 24

        self.mock_movies()
        get_relion_tutorial_data(relion_version=4)
        depobjs = get_deposition_objects_empiar("PostProcess/job030/")
        rparts = "Per-particle motion corrected particle images"
        parts = "Particle images"
        mics = "Corrected micrographs"
        movs = "Multiframe micrograph movies"
        assert len(depobjs[0]) == 1
        assert all([x.data_items.name == rparts for x in depobjs[3]])
        assert len(depobjs[1]) == 2
        assert all([x.data_items.name == parts for x in depobjs[2]])
        assert len(depobjs[2]) == 9
        assert all([x.data_items.name == mics for x in depobjs[1]])
        assert len(depobjs[3]) == 2
        assert all([x.data_items.name == movs for x in depobjs[0]])

    @slow_test
    @patch("pipeliner.deposition_tools.empiar_deposition_objects.get_tiff_stats")
    def test_merge_objs_full_EMPIAR_deposition_all_types_selected(self, ts_patch):
        ts_patch.return_value = [
            [
                (3710, 3838),
                0,
                25,
                0.9467082614063648,
                0.9745422394926604,
                None,
                "16-bit unsigned integer: uint16",
            ]
        ] * 24
        self.mock_movies()
        get_relion_tutorial_data(relion_version=4)
        depobjs = get_deposition_objects_empiar("PostProcess/job030/")
        merged = assemble_deposition_objects_empiar(depobjs)

        expected_merged = [
            EmpiarMovieSet(
                name="Multiframe micrograph movies",
                directory="Import/job001/Movies",
                category="('T2', '')",
                header_format="('T3', '')",
                data_format="('T3', '')",
                num_images_or_tilt_series=24,
                frames_per_image=24,
                voxel_type="('T3', '')",
                pixel_width=0.885,
                pixel_height=0.885,
                details="Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; CCP-EM pipeliner {pipe_vers}",
                image_width=3710,
                image_height=3838,
                micrographs_file_pattern="Import/job001/Movies/20170629_000*_"
                "frameImage.tiff",
            ),
            EmpiarCorrectedMics(
                name="Corrected micrographs",
                directory="MotionCorr/job002/Movies",
                category="('T1', '')",
                header_format="('T1', '')",
                data_format="('OT', '16-bit float')",
                num_images_or_tilt_series=24,
                frames_per_image=1,
                voxel_type="('OT', '16-bit float')",
                pixel_width=0.885,
                pixel_height=0.885,
                details="Voltage 1.4; Spherical aberration 1.4; Image data in file: "
                "CtfFind/job003/micrographs_ctf.star; CCP-EM "
                f"pipeliner {pipe_vers}",
                image_width=3710,
                image_height=3838,
                micrographs_file_pattern="MotionCorr/job002/Movies/20170629_000*_"
                "frameImage.mrc",
            ),
            EmpiarParticles(
                name="Particle images",
                directory="Extract/job018/Movies",
                category="('T5', '')",
                header_format="('T2', '')",
                data_format="('OT', '16-bit float')",
                num_images_or_tilt_series=24,
                frames_per_image=1,
                voxel_type="('OT', '16-bit float')",
                pixel_width=0.885,
                pixel_height=0.885,
                details="4748 total particles; Voltage 1.4; Spherical aberration 1.4; "
                "Image data in file: Refine3D/job025/run_data.star; "
                f"CCP-EM pipeliner {pipe_vers}",
                image_width=256,
                image_height=256,
                micrographs_file_pattern="Extract/job018/Movies/20170629_000*_"
                "frameImage.mrcs",
                picked_particles_file_pattern="Refine3D/job025/run_data.star",
            ),
            EmpiarRefinedParticles(
                name="Per-particle motion corrected particle images",
                directory="Polish/job028/Movies",
                category="('T6', '')",
                header_format="('T2', '')",
                data_format="('T7', '')",
                num_images_or_tilt_series=24,
                frames_per_image=1,
                voxel_type="('T7', '')",
                pixel_width=0.885,
                pixel_height=0.885,
                details="4748 total particles; Voltage 1.4; Spherical aberration 1.4;"
                " Image data in file: Refine3D/job029/run_data.star; CCP-EM "
                f"pipeliner {pipe_vers}",
                image_width=256,
                image_height=256,
                micrographs_file_pattern="Polish/job028/Movies/20170629_000*_"
                "frameImage_shiny.mrcs",
                picked_particles_file_pattern="Refine3D/job029/run_data.star",
            ),
        ]
        for n, i in enumerate(merged):
            assert i.data_items.__dict__ == expected_merged[n].__dict__

    @slow_test
    @patch("pipeliner.deposition_tools.empiar_deposition_objects.get_tiff_stats")
    def test_full_empiar_deposition(self, ts_patch):
        ts_patch.return_value = [
            [
                (3710, 3838),
                0,
                25,
                0.9467082614063648,
                0.9745422394926604,
                None,
                "16-bit unsigned integer: uint16",
            ]
        ] * 24
        self.mock_movies()
        get_relion_tutorial_data(relion_version=4)
        prepare_empiar_deposition("PostProcess/job030/")
        assert os.path.isdir(EMPIAR_DEPOSITION_DIR)
        df = glob(f"{EMPIAR_DEPOSITION_DIR}/*_EMPIAR.json")
        num = os.path.basename(df[0]).split("_")[0]
        depdir = os.path.join(EMPIAR_DEPOSITION_DIR, num)
        # check the deposition directory was created properly
        dep_files = os.walk(depdir)
        expected = [
            (["Refine3D", "MotionCorr", "Polish", "Extract"], []),
            (["job025", "job029"], []),
            ([], ["run_data.star"]),
            ([], ["run_data.star"]),
            (["job002"], []),
            (["Movies"], []),
            (
                [],
                [
                    "20170629_00048_frameImage.mrc",
                    "20170629_00039_frameImage.mrc",
                    "20170629_00031_frameImage.mrc",
                    "20170629_00024_frameImage.mrc",
                    "20170629_00040_frameImage.mrc",
                    "20170629_00023_frameImage.mrc",
                    "20170629_00036_frameImage.mrc",
                    "20170629_00047_frameImage.mrc",
                    "20170629_00037_frameImage.mrc",
                    "20170629_00022_frameImage.mrc",
                    "20170629_00046_frameImage.mrc",
                    "20170629_00025_frameImage.mrc",
                    "20170629_00030_frameImage.mrc",
                    "20170629_00049_frameImage.mrc",
                    "20170629_00043_frameImage.mrc",
                    "20170629_00027_frameImage.mrc",
                    "20170629_00044_frameImage.mrc",
                    "20170629_00035_frameImage.mrc",
                    "20170629_00028_frameImage.mrc",
                    "20170629_00029_frameImage.mrc",
                    "20170629_00045_frameImage.mrc",
                    "20170629_00021_frameImage.mrc",
                    "20170629_00042_frameImage.mrc",
                    "20170629_00026_frameImage.mrc",
                ],
            ),
            (["job028"], []),
            (["Movies"], []),
            (
                [],
                [
                    "20170629_00045_frameImage_shiny.mrcs",
                    "20170629_00023_frameImage_shiny.mrcs",
                    "20170629_00035_frameImage_shiny.mrcs",
                    "20170629_00047_frameImage_shiny.mrcs",
                    "20170629_00029_frameImage_shiny.mrcs",
                    "20170629_00021_frameImage_shiny.mrcs",
                    "20170629_00037_frameImage_shiny.mrcs",
                    "20170629_00027_frameImage_shiny.mrcs",
                    "20170629_00049_frameImage_shiny.mrcs",
                    "20170629_00031_frameImage_shiny.mrcs",
                    "20170629_00039_frameImage_shiny.mrcs",
                    "20170629_00025_frameImage_shiny.mrcs",
                    "20170629_00043_frameImage_shiny.mrcs",
                    "20170629_00022_frameImage_shiny.mrcs",
                    "20170629_00044_frameImage_shiny.mrcs",
                    "20170629_00036_frameImage_shiny.mrcs",
                    "20170629_00028_frameImage_shiny.mrcs",
                    "20170629_00046_frameImage_shiny.mrcs",
                    "20170629_00040_frameImage_shiny.mrcs",
                    "20170629_00030_frameImage_shiny.mrcs",
                    "20170629_00048_frameImage_shiny.mrcs",
                    "20170629_00026_frameImage_shiny.mrcs",
                    "20170629_00042_frameImage_shiny.mrcs",
                    "20170629_00024_frameImage_shiny.mrcs",
                ],
            ),
            (["job018"], []),
            (["Movies"], []),
            (
                [],
                [
                    "20170629_00044_frameImage.mrcs",
                    "20170629_00027_frameImage.mrcs",
                    "20170629_00028_frameImage.mrcs",
                    "20170629_00031_frameImage.mrcs",
                    "20170629_00036_frameImage.mrcs",
                    "20170629_00043_frameImage.mrcs",
                    "20170629_00039_frameImage.mrcs",
                    "20170629_00037_frameImage.mrcs",
                    "20170629_00021_frameImage.mrcs",
                    "20170629_00042_frameImage.mrcs",
                    "20170629_00045_frameImage.mrcs",
                    "20170629_00026_frameImage.mrcs",
                    "20170629_00029_frameImage.mrcs",
                    "20170629_00030_frameImage.mrcs",
                    "20170629_00035_frameImage.mrcs",
                    "20170629_00040_frameImage.mrcs",
                    "20170629_00023_frameImage.mrcs",
                    "20170629_00024_frameImage.mrcs",
                    "20170629_00047_frameImage.mrcs",
                    "20170629_00048_frameImage.mrcs",
                    "20170629_00025_frameImage.mrcs",
                    "20170629_00046_frameImage.mrcs",
                    "20170629_00049_frameImage.mrcs",
                    "20170629_00022_frameImage.mrcs",
                ],
            ),
            (["Movies"], []),
            (
                [],
                [
                    "20170629_00049_frameImage.tiff",
                    "20170629_00025_frameImage.tiff",
                    "20170629_00046_frameImage.tiff",
                    "20170629_00022_frameImage.tiff",
                    "20170629_00040_frameImage.tiff",
                    "20170629_00023_frameImage.tiff",
                    "20170629_00035_frameImage.tiff",
                    "20170629_00048_frameImage.tiff",
                    "20170629_00024_frameImage.tiff",
                    "20170629_00047_frameImage.tiff",
                    "20170629_00021_frameImage.tiff",
                    "20170629_00042_frameImage.tiff",
                    "20170629_00037_frameImage.tiff",
                    "20170629_00029_frameImage.tiff",
                    "20170629_00030_frameImage.tiff",
                    "20170629_00045_frameImage.tiff",
                    "20170629_00026_frameImage.tiff",
                    "20170629_00028_frameImage.tiff",
                    "20170629_00031_frameImage.tiff",
                    "20170629_00044_frameImage.tiff",
                    "20170629_00027_frameImage.tiff",
                    "20170629_00043_frameImage.tiff",
                    "20170629_00039_frameImage.tiff",
                    "20170629_00036_frameImage.tiff",
                ],
            ),
        ]
        for i in dep_files:
            assert tuple(i)[1:] in expected

    @slow_test
    @patch("pipeliner.deposition_tools.empiar_deposition_objects.get_tiff_stats")
    def test_empiar_deposition_with_jobstar_file(self, ts_patch):
        ts_patch.return_value = [
            [
                (3710, 3838),
                0,
                25,
                0.9467082614063648,
                0.9745422394926604,
                None,
                "16-bit unsigned integer: uint16",
            ]
        ] * 24
        self.mock_movies()
        os.makedirs("Deposition/job100")
        jobstar = "Deposition/job100/job.star"
        shutil.copy(
            os.path.join(
                self.test_data, "JobFiles/EmpiarDeposition/empiar_deposition_job.star"
            ),
            jobstar,
        )
        get_relion_tutorial_data(relion_version=4)
        prepare_empiar_deposition(
            terminal_job="PostProcess/job030/", jobstar_file=jobstar
        )
        depfile = glob("Deposition/job100/*_EMPIAR.json")[0]
        with open(depfile) as df:
            wrote = json.load(df)
        with open(
            os.path.join(self.test_data, "Metadata/empiar_deposition_complete.json")
        ) as ex:
            expected = json.load(ex)
            # replace the datetime tags
            dtt = os.path.basename(depfile).split("_")[0]
            for n, imset in enumerate(expected["imagesets"]):
                for field in imset:
                    val = expected["imagesets"][n][field]
                    if isinstance(val, str):
                        expected["imagesets"][n][field] = val.replace(
                            "20230525091908", dtt
                        )
                        expected["imagesets"][n][field] = val.replace(
                            "0.1.0", pipe_vers
                        )
        assert wrote == expected

    def test_parse_empiar_jobstar(self):
        jstar = os.path.join(
            self.test_data, "JobFiles/EmpiarDeposition/empiar_deposition_job.star"
        )
        shutil.copy(jstar, "job.star")
        with open(
            os.path.join(self.test_data, "Metadata/empiar_deposition_template.json")
        ) as f:
            expected = json.load(f)
        jstar_data = parse_empiar_jobstar("job.star")
        assert jstar_data == expected


if __name__ == "__main__":
    unittest.main()
