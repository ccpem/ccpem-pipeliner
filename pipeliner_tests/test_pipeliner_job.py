#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch, Mock

from glob import glob

from pipeliner import pipeliner_job
from pipeliner_tests import test_data
from pipeliner.utils import touch
from pipeliner.results_display_objects import (
    ResultsDisplayPending,
    ResultsDisplayText,
    ResultsDisplayMontage,
)
from pipeliner_tests.testing_tools import DummyJob, expected_warning
from pipeliner.job_factory import new_job_of_type, get_job_types, job_from_dict
from pipeliner.starfile_handler import JobStar
from pipeliner.data_structure import TRUES, FALSES, JOBINFO_FILE
from pipeliner.job_options import (
    StringJobOption,
    MultiStringJobOption,
    InputNodeJobOption,
    MultiInputNodeJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
    FloatJobOption,
    BooleanJobOption,
    JobOptionCondition,
    ExternalFileJobOption,
    MultiExternalFileJobOption,
    SearchStringJobOption,
    DirPathJobOption,
)
from pipeliner.nodes import DensityMapNode
from pipeliner.utils import update_jobinfo_file


class PipelinerJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_pipeliner_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            pipeliner_job.PipelinerJob()

    def test_get_default_params_import_movies(self):
        job = new_job_of_type("relion.import.movies")
        params_dict = job.get_default_params_dict()
        expected_params = {
            "_rlnJobTypeLabel": "relion.import.movies",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
            "fn_in_raw": "Movies/*.tif",
            "is_multiframe": "Yes",
            "optics_group_name": "opticsGroup1",
            "fn_mtf": "",
            "angpix": 1.4,
            "kV": 300,
            "Cs": 2.7,
            "Q0": 0.1,
            "beamtilt_x": 0.0,
            "beamtilt_y": 0.0,
            "is_synthetic": "No",
            "microscope_data_file": "",
            "validate_output": "Yes",
        }
        assert params_dict == expected_params

    def test_get_default_params_all_jobs(self):
        defaults_dir = os.path.join(self.test_data, "JobFiles/Default_JobStar")
        default_files = glob(f"{defaults_dir}/*.star")

        # parameters not to check because they are set by env vars or not in the
        # joboptions dict
        params_to_skip = [
            "_rlnJobTypeLabel",
            "_rlnJobIsContinue",
            "_rlnJobIsTomo",
            "scratch_dir",
            "fn_gctf_exe",
            "fn_motioncor2_exe",
            "qsubscript",
            "do_queue",
            "queuename",
            "qsubscript",
            "qsub",
            "fn_ctffind_exe",
            "fn_resmap",
            "fn_topaz_exec",
        ]

        for f in default_files:
            options = JobStar(f).all_options_as_dict()
            jobtype = options["_rlnJobTypeLabel"]
            new_job = new_job_of_type(jobtype)
            new_job.add_compatibility_joboptions()
            params_dict = new_job.get_default_params_dict()
            for param in options:
                if param not in params_to_skip:
                    try:
                        info = (jobtype, param, str(params_dict[param]), options[param])
                        assert str(params_dict[param]) == options[param], info
                    except KeyError:
                        raise ValueError(f"jobtype {jobtype} missing param: {param}")

            for param in params_dict:
                if param not in params_to_skip:
                    info = (jobtype, param, str(params_dict[param]), options[param])
                    assert str(params_dict[param]) == options[param], info

    def test_joboption_validation_with_errors(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].regex_error_message = None
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].suggested_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "This option is required", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            (
                "warning",
                "Value outside suggested range: Min 100.0",
                "Pixel size (Angstrom):",
            ),
            (
                "warning",
                "Value outside suggested range: Max 3",
                "Pixel size (Angstrom):",
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_warnings(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].regex_error_message = None
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].hard_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "This option is required", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            ("error", "Value cannot be less than 100.0", "Pixel size (Angstrom):"),
            (
                "warning",
                "Value outside suggested range: Max 3",
                "Pixel size (Angstrom):",
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_adv_parameter_errors(self):
        job = new_job_of_type("relion.autopick.log")
        job.joboptions["fn_input_autopick"].value = "test.star"
        job.joboptions["log_diam_min"].value = 100
        job.joboptions["log_diam_max"].value = 99
        job.joboptions["do_queue"].value = "No"

        errors = job.validate_joboptions()
        exp_errors = [
            (
                "error",
                "Max diameter of LoG filter (log_diam_max) must be > min diameter "
                "(log_diam_min).",
                [
                    "Min. diameter for LoG filter (A)",
                    "Max. diameter for LoG filter (A)",
                ],
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, [x.label for x in i.raised_by])
            formatted_errors.append(error)

        for err in formatted_errors:
            assert err in exp_errors

        for err in exp_errors:
            assert err in formatted_errors, err

    def test_parse_additional_args_even_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["other_args"].value = (
            '--1 test --2 test2 --3 "This is complicated"'
        )
        assert job.parse_additional_args() == [
            "--1",
            "test",
            "--2",
            "test2",
            "--3",
            "This is complicated",
        ]

    def test_joboption_required_if_true(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["do_queue"].value = "Yes"
        job.joboptions["qsubscript"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 1
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Submit to queue?" = Yes'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "Standard submission script:"

    def test_joboption_required_if_false(self):
        job = new_job_of_type("em_placement.atomic_model_fit")
        job.joboptions["use_reference_structure"].value = "No"
        job.joboptions["sequence"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 1
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Estimate composition from model" = No'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "FASTA Sequence"

    def test_joboption_required_if_multiple_blank_string(self):
        job = new_job_of_type("servalcat.map_utilities.trim")
        job.joboptions["mask"].value = ""
        job.joboptions["input_models"].value = ""
        job.joboptions["shifts_json"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 2
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Atomic models" is blank and'
            ' "Precalculated shifts" is blank'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "Mask"

        assert errors[1].type == "error"
        assert errors[1].message == (
            'This option is required because "Mask" is blank and "Precalculated shifts"'
            " is blank"
        )
        assert len(errors[1].raised_by) == 1
        assert errors[1].raised_by[0].label == "Atomic models"

    def test_joboption_required_if_string_not_blank(self):
        job = new_job_of_type("pipeliner.deposition.empiar")
        job.joboptions["citation_title"].value = "Title"
        job.joboptions["entry_authors"].value = "Author 1"
        job.joboptions["cite_country"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 1
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Citation title" is not blank'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "Publication country"

    def test_joboption_required_if_string_equal(self):
        job = new_job_of_type("ccpem_utils.map_utilities.crop_pad_mask_map")
        job.joboptions["crop_mode"].value = "input_mask"
        job.joboptions["apply_mask"].value = "Yes"
        job.joboptions["input_mask"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 1
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Apply input mask?" = Yes and "Crop based'
            ' on" = input_mask'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "Input mask"

    def test_joboption_required_if_num_comparison(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["nr_mpi"].value = 2
        job.joboptions["mpi_command"].value = ""

        errors = job.validate_dynamically_required_joboptions()
        assert len(errors) == 1
        assert errors[0].type == "error"
        assert errors[0].message == (
            'This option is required because "Number of MPI procs:" > 1'
        )
        assert len(errors[0].raised_by) == 1
        assert errors[0].raised_by[0].label == "MPI run command:"

    def test_parse_additional_args_starts_with_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["other_args"].value = (
            '"1 test" --2 test2 --3 "This is complicated"'
        )

        assert job.parse_additional_args() == [
            "1 test",
            "--2",
            "test2",
            "--3",
            "This is complicated",
        ]

    def test_parse_additional_args_starts_no_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["other_args"].value = "1 test --2 test2 --3 This is simple"

        assert job.parse_additional_args() == [
            "1",
            "test",
            "--2",
            "test2",
            "--3",
            "This",
            "is",
            "simple",
        ]

    def test_grouping_joboptions_complete_list(self):
        job = new_job_of_type("relion.refine3d")
        new_priority = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        job.set_joboption_order(new_priority)
        groupings = job.get_joboption_groups()
        exp_groups = {
            "Main": [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "particle_diameter",
                "do_zero_mask",
                "do_solvent_fsc",
                "sampling",
                "offset_range",
                "offset_step",
                "auto_local_sampling",
                "relax_sym",
                "auto_faster",
                "do_blush",
            ],
            "Compute options": [
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
            ],
            "Running options": ["nr_mpi", "mpi_command", "nr_threads", "other_args"],
            "Queue submission options": [
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
            ],
        }
        assert groupings == exp_groups

    def test_grouping_joboptions_partial_list(self):
        job = new_job_of_type("relion.refine3d")
        priorities = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        job.set_joboption_order(priorities)
        groupings = job.get_joboption_groups()
        exp_groups = {
            "Main": [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "particle_diameter",
                "do_zero_mask",
                "do_solvent_fsc",
                "do_blush",
                "sampling",
                "offset_range",
                "offset_step",
                "auto_local_sampling",
                "relax_sym",
                "auto_faster",
            ],
            "Compute options": [
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
            ],
            "Running options": ["nr_mpi", "mpi_command", "nr_threads", "other_args"],
            "Queue submission options": [
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
            ],
        }
        assert groupings == exp_groups

    def test_reordering_joboptions_complete_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
            "do_blush",
        ]
        assert list(job.joboptions) != new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == new_order

    def test_reordering_joboptions_partial_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        full_new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "do_blush",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        assert list(job.joboptions) != full_new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == full_new_order

    def test_reordering_joboptions_error_in_not_present(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_img",
            "fn_ref",
            "Badbadbad",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        with self.assertRaises(ValueError):
            job.set_joboption_order(new_order)

    def test_node_creation_inputnode_joboptions(self):
        """test creation of an input node from an inputnode joboption"""
        the_job = DummyJob()
        jobop = InputNodeJobOption(label="inputnodetype", node_type="Test")
        jobop.value = "testfile.star"
        the_job.joboptions = {"test": jobop}
        the_job.create_input_nodes()
        assert len(the_job.input_nodes) == 1
        node = the_job.input_nodes[0]
        assert node.name == "testfile.star"
        assert node.toplevel_type == "Test"
        assert node.type == "Test.star"
        assert node.format == "star"

    def test_add_output_node(self):
        job = DummyJob()
        job.output_dir = "Test/job234/"
        assert len(job.output_nodes) == 0

        job.add_output_node("example.txt", "TestNode", ["kwd1", "kwd2"])

        assert len(job.output_nodes) == 1
        assert job.output_nodes[0].name == "Test/job234/example.txt"
        assert job.output_nodes[0].type == "TestNode.txt.kwd1.kwd2"

    def test_raw_options_from_jobstar(self):
        job = new_job_of_type("relion.joinstar.micrographs")
        job.read(
            os.path.join(
                self.test_data, "JobFiles/JoinStar/joinstar_mics_relionstyle_job.star"
            )
        )
        exp_jos = {
            "do_mic": "Yes",
            "do_mov": "No",
            "do_part": "No",
            "fn_mic1": "Mics1.star",
            "fn_mic2": "Mics2.star",
        }

        for ejo in exp_jos:
            assert job.raw_options[ejo] == exp_jos[ejo]

    def test_raw_options_from_runjob(self):
        job = new_job_of_type("relion.joinstar.micrographs")
        job.read(
            os.path.join(
                self.test_data, "JobFiles/JoinStar/joinstar_mics_relionstyle_run.job"
            )
        )
        exp_jos = {
            "Combine micrograph STAR files?": "Yes",
            "Combine movie STAR files?": "No",
            "Combine particle STAR files?": "No",
            "Micrograph STAR file 1:": "Mic1.star",
            "Micrograph STAR file 2:": "Mic2.star",
        }
        print(job.raw_options)
        for ejo in exp_jos:
            assert job.raw_options[ejo] == exp_jos[ejo]

    def test_get_mpi_command_substitution(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["nr_mpi"].value = 8
        job.joboptions["mpi_command"].value = "test XXXmpinodesXXX test XXXmpinodesXXX"
        subbed_com = ["test", "8", "test", "8"]
        assert job.get_mpi_command() == subbed_com

    def test_get_mpi_command_substitution_complicated(self):
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["nr_mpi"].value = 16
        job.joboptions["mpi_command"].value = "'A complicated one' XXXmpinodesXXX"
        subbed_com = ["A complicated one", "16"]
        assert job.get_mpi_command() == subbed_com

    def test_all_jobs_joboption_defaults_are_appropriate(self):
        """Make sure all JobOptions for jobs have appropriate data types for
        their default values. This can cause hard to diagnose errors in Doppio"""

        types = {
            StringJobOption: [str],
            MultiStringJobOption: [str],
            InputNodeJobOption: [str],
            MultiInputNodeJobOption: [str],
            ExternalFileJobOption: [str],
            MultiExternalFileJobOption: [str],
            MultipleChoiceJobOption: [str],
            IntJobOption: [int],
            FloatJobOption: [int, float],
            BooleanJobOption: [str],
            DirPathJobOption: [str],
            SearchStringJobOption: [str],
        }

        for j in get_job_types():
            job = new_job_of_type(j.PROCESS_NAME)
            for jo in job.joboptions:
                exp_type = types[type(job.joboptions[jo])]
                dvtype = type(job.joboptions[jo].default_value)
                assert dvtype in exp_type, (jo, dvtype, exp_type)
                if isinstance(job.joboptions[jo], BooleanJobOption):
                    assert job.joboptions[jo].default_value.lower() in TRUES + FALSES

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_handle_uploads_changes_joboptions_and_writes_jobstar(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["fn_img"].value = "data.star"
        job.joboptions["fn_mask"].value = "DoppioUploads/mask.mrc"
        job.joboptions["fn_ref"].value = "reference.mrc"
        job.joboptions["nr_mpi"].value = 3
        job.output_dir = "Refine3D/job002/"
        job.handle_doppio_uploads(dry_run=True)
        # job option has been updated
        assert job.joboptions["fn_mask"].value == "Refine3D/job002/InputFiles/mask.mrc"

        # nodes have been updated
        job.create_input_nodes()
        in_nodes = [x.name for x in job.input_nodes]
        assert in_nodes == [
            "data.star",
            "reference.mrc",
            "Refine3D/job002/InputFiles/mask.mrc",
        ]
        job.create_output_nodes()
        out_nodes = [x.name for x in job.output_nodes]
        assert out_nodes == [
            "Refine3D/job002/run_data.star",
            "Refine3D/job002/run_optimiser.star",
            "Refine3D/job002/run_half1_class001_unfil.mrc",
            "Refine3D/job002/run_half2_class001_unfil.mrc",
            "Refine3D/job002/run_class001.mrc",
        ]

        # make sure the name of the mask file was updated in the commands
        coms = job.get_final_commands()
        expected_coms = [
            [
                "/path/to/mpirun",
                "-n",
                "3",
                "/path/to/relion_refine_mpi",
                "--i",
                "data.star",
                "--o",
                "Refine3D/job002/run",
                "--auto_refine",
                "--split_random_halves",
                "--ref",
                "reference.mrc",
                "--firstiter_cc",
                "--ini_high",
                "60",
                "--dont_combine_weights_via_disc",
                "--pool",
                "3",
                "--pad",
                "2",
                "--ctf",
                "--particle_diameter",
                "200",
                "--flatten_solvent",
                "--zero_mask",
                "--solvent_mask",
                "Refine3D/job002/InputFiles/mask.mrc",
                "--oversampling",
                "1",
                "--healpix_order",
                "2",
                "--auto_local_healpix_order",
                "4",
                "--offset_range",
                "5",
                "--offset_step",
                "2",
                "--sym",
                "C1",
                "--low_resol_join_halves",
                "40",
                "--norm",
                "--scale",
                "--j",
                "1",
                "--pipeline_control",
                "Refine3D/job002/",
            ],
        ]
        assert coms == expected_coms

    def test_handle_uploads_deletes_files_on_error_dest_already_exists(self):
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/data.star")
        os.makedirs("Refine3D/job002/InputFiles")
        touch("Refine3D/job002/InputFiles/data.star")
        job = new_job_of_type("relion.refine3d")
        job.joboptions["fn_img"].value = "Refine3D/job002/InputFiles/data.star"
        job.joboptions["fn_mask"].value = "DoppioUploads/data.star"
        job.joboptions["fn_ref"].value = "reference.mrc"
        job.joboptions["nr_mpi"].value = 3
        job.output_dir = "Refine3D/job002/"
        with self.assertRaisesRegex(ValueError, "already exists"):
            job.handle_doppio_uploads()
        assert not os.path.isfile("DoppioUploads/data.star")

    def test_handle_uploads_changes_joboptions_and_writes_jobstar_multifiles(self):
        # test this with a dummy job
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/file_1.txt")
        touch("DoppioUploads/file_2.txt")
        job = DummyJob(commands=[])
        job.joboptions["mf"] = MultiInputNodeJobOption(
            label="test", node_type="TestNode"
        )
        job.joboptions["mf"].value = (
            "DoppioUploads/file_1.txt:::DoppioUploads/file_2.txt"
        )
        job.output_dir = "Test/job001/"
        job.handle_doppio_uploads(dry_run=True)
        assert (
            job.joboptions["mf"].value
            == "Test/job001/InputFiles/file_1.txt:::Test/job001/InputFiles/file_2.txt"
        )
        job.create_input_nodes()
        assert [x.name for x in job.input_nodes] == [
            "Test/job001/InputFiles/file_1.txt",
            "Test/job001/InputFiles/file_2.txt",
        ]
        coms = job.get_final_commands()
        assert coms == []

    def test_handle_uploads_changes_joboptions_and_writes_jobstar_multifiles_mixed(
        self,
    ):
        # test this with a dummy job
        os.makedirs("DoppioUploads")
        touch("DoppioUploads/file_1.txt")
        touch("file_2.txt")
        job = DummyJob(commands=[])
        job.joboptions["mf"] = MultiInputNodeJobOption(
            label="test", node_type="TestNode"
        )
        job.joboptions["mf"].value = "DoppioUploads/file_1.txt:::file_2.txt"
        job.output_dir = "Test/job001/"
        job.handle_doppio_uploads(dry_run=True)
        assert (
            job.joboptions["mf"].value
            == "Test/job001/InputFiles/file_1.txt:::file_2.txt"
        )
        job.create_input_nodes()
        assert [x.name for x in job.input_nodes] == [
            "Test/job001/InputFiles/file_1.txt",
            "file_2.txt",
        ]
        coms = job.get_final_commands()
        assert coms == []

    def test_reference_eq_doi_match(self):
        r1 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="1234567"
        )
        r2 = pipeliner_job.Ref(
            title="test1-dif", journal="journal numero uno", year="1999", doi="1234567"
        )
        assert r1 == r2

    def test_reference_eq_doi_dont_match(self):
        r1 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="1234567"
        )
        r2 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="xxxxxx"
        )
        assert r1 != r2

    def test_reference_eq_exact_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="journal numero uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="journal numero uno", year="1999")
        assert r1 == r2

    def test_reference_eq_journal_close_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="Journal Numero Uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="j. num. uno", year="1999")
        assert r1 == r2

    def test_reference_eq_title_close_match(self):
        r1 = pipeliner_job.Ref(
            title="test1: a paper", journal="journal numero uno", year="1999"
        )
        r2 = pipeliner_job.Ref(
            title="Test1 - a paper!", journal="journal numero uno", year="1999"
        )
        assert r1 == r2

    def test_reference_eq_title_no_match(self):
        r1 = pipeliner_job.Ref(
            title="test1: a paper", journal="journal numero uno", year="1999"
        )
        r2 = pipeliner_job.Ref(
            title="Test1 - a bad paper!", journal="journal numero uno", year="1999"
        )
        assert r1 != r2

    def test_reference_eq_journal_no_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="Journal Numero Uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="j. num. 1", year="1999")
        assert r1 != r2

    def test_validate_files_with_deactivated(self):
        """File is missing but ok because JobOption is deactivated"""
        job = DummyJob()
        job.joboptions = {
            "test": ExternalFileJobOption(
                label="test", deactivate_if=JobOptionCondition([("ref", "=", True)])
            ),
            "ref": BooleanJobOption(label="check", default_value=True),
        }
        job.joboptions["ref"].value = "Yes"
        job.joboptions["test"].value = "test_file.txt"

        assert not job.validate_input_files()

    def test_validate_files_with_dynamically_required(self):
        """File is missing and joboption has been toggled to required"""
        job = DummyJob()
        job.joboptions = {
            "test": ExternalFileJobOption(
                label="test", required_if=JobOptionCondition([("ref", "=", True)])
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "Yes"
        job.joboptions["test"].value = "test_file.txt"

        errs = job.validate_input_files()
        assert len(errs) == 1
        assert errs[0].__dict__["message"] == "File test_file.txt not found"

    def test_validate_files_with_value_withfile(self):
        """If jo has a value and hasn't been deactivated then needs an existing file"""
        touch("test_file.txt")
        job = DummyJob()
        job.joboptions = {
            "test": ExternalFileJobOption(
                label="test", deactivate_if=JobOptionCondition([("ref", "=", True)])
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "No"
        job.joboptions["test"].value = "test_file.txt"

        assert not job.validate_input_files()

    def test_validate_files_with_value_nofile(self):
        """If jo has a value and hasn't been deactivated then needs an existing file"""
        job = DummyJob()
        job.joboptions = {
            "test": ExternalFileJobOption(
                label="test", deactivate_if=JobOptionCondition([("ref", "=", True)])
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "No"
        job.joboptions["test"].value = "test_file.txt"

        errs = job.validate_input_files()
        assert len(errs) == 1
        assert errs[0].__dict__["message"] == "File test_file.txt not found"

    def test_default_results_display(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file2.mrc"))

        node = DensityMapNode(name="file.mrc")
        node2 = DensityMapNode(name="file2.mrc")
        job = DummyJob()
        job.output_nodes = [node, node2]

        dispobjs = job.create_results_display()
        assert len(dispobjs) == 2
        for dob in dispobjs:
            assert isinstance(dob, ResultsDisplayMontage)
        assert dispobjs[0].title == "Map slices: file.mrc"
        assert dispobjs[1].title == "Map slices: file2.mrc"
        assert dispobjs[0].associated_data == ["file.mrc"]
        assert dispobjs[1].associated_data == ["file2.mrc"]

    def test_default_results_display_onefails(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file2.star"))

        node = DensityMapNode(name="file.mrc")
        node2 = DensityMapNode(name="file2.mrc")
        job = DummyJob()
        job.output_nodes = [node, node2]

        dispobjs = job.create_results_display()
        assert len(dispobjs) == 2
        assert isinstance(dispobjs[0], ResultsDisplayMontage)
        assert isinstance(dispobjs[1], ResultsDisplayPending)
        assert dispobjs[0].title == "Map slices: file.mrc"
        assert dispobjs[1].title == "Results pending..."
        assert dispobjs[0].associated_data == ["file.mrc"]
        assert dispobjs[1].reason.startswith("Traceback (most recent call last):")

    @patch("pipeliner_tests.testing_tools.DummyJob.create_results_display")
    def test_saving_results_display_files_1_text(self, mock_create_results_display):
        display_objs = [
            ResultsDisplayText(
                title="Test display 1", display_data="Text 1", associated_data=[]
            )
        ]
        mock_create_results_display.return_value = display_objs

        os.mkdir("DummyJobDir")
        job = DummyJob()
        job.output_dir = "DummyJobDir"
        returned = job.save_results_display_files()
        assert returned is display_objs
        assert os.path.isfile("DummyJobDir/.results_display000_text.json")

    @patch("pipeliner_tests.testing_tools.DummyJob.create_results_display")
    def test_saving_results_display_files_2_text_1_pending(
        self, mock_create_results_display
    ):
        display_objs = [
            ResultsDisplayText(
                title="Test display 1", display_data="Text 1", associated_data=[]
            ),
            ResultsDisplayPending(),
            ResultsDisplayText(
                title="Test display 2", display_data="Text 2", associated_data=[]
            ),
        ]
        mock_create_results_display.return_value = display_objs

        os.mkdir("DummyJobDir")
        job = DummyJob()
        job.output_dir = "DummyJobDir"
        returned = job.save_results_display_files()
        assert returned is display_objs
        assert os.path.isfile("DummyJobDir/.results_display000_text.json")
        assert os.path.isfile("DummyJobDir/.results_display001_pending.json")
        assert os.path.isfile("DummyJobDir/.results_display002_text.json")

    @patch("pipeliner_tests.testing_tools.DummyJob.create_results_display")
    def test_saving_results_with_error(self, mock_create_results_display):
        mock_create_results_display.side_effect = RuntimeError("Error making results")

        os.mkdir("DummyJobDir")
        job = DummyJob()
        job.output_dir = "DummyJobDir"
        returned = job.save_results_display_files()
        assert len(returned) == 1
        assert type(returned[0]) is ResultsDisplayPending
        assert os.path.isfile("DummyJobDir/.results_display000_pending.json")

    def test_removing_existing_results_files(self):
        dummy_job_dir = Path("DummyJobDir")
        dummy_job_dir.mkdir()
        (dummy_job_dir / ".results_display000_pending.json").touch()
        (dummy_job_dir / ".results_display001_text.json").touch()
        (dummy_job_dir / ".results_display002_montage.json").touch()
        (dummy_job_dir / "Thumbnails").mkdir()
        (dummy_job_dir / "Thumbnails" / "montage_s000.png").touch()
        # Node display outputs should be left in place
        (dummy_job_dir / "NodeDisplay").mkdir()
        (dummy_job_dir / "NodeDisplay" / "Thumbnails").mkdir()
        (dummy_job_dir / "NodeDisplay" / "Thumbnails" / "montage_s000.png").touch()
        assert len(list(dummy_job_dir.iterdir())) == 5

        job = DummyJob()
        job.output_dir = str(dummy_job_dir)
        job._remove_results_display_files()
        assert len(list(dummy_job_dir.iterdir())) == 1
        assert not (dummy_job_dir / ".results_display001_text.json").exists()
        assert not (dummy_job_dir / "Thumbnails").exists()

        # Check node display outputs have been left alone
        assert (dummy_job_dir / "NodeDisplay").is_dir()
        assert (dummy_job_dir / "NodeDisplay" / "Thumbnails").is_dir()
        assert (
            dummy_job_dir / "NodeDisplay" / "Thumbnails" / "montage_s000.png"
        ).is_file()

    @patch("pipeliner_tests.testing_tools.DummyJob._remove_results_display_files")
    def test_saving_results_removes_existing_files_first(
        self, mock_remove_results_display_files
    ):
        job = DummyJob()
        job.output_dir = "DummyJobDir"
        job.save_results_display_files()
        mock_remove_results_display_files.assert_called_once()

    def test_loading_results_files(self):
        results_files_dir = Path(self.test_data) / "ResultsFiles"
        job_dir = Path("DummyJobDir")
        job_dir.mkdir()
        shutil.copy(
            results_files_dir / "class2d_results.json",
            job_dir / ".results_display000_gallery.json",
        )
        shutil.copy(
            results_files_dir / "postprocess_fsc.json",
            job_dir / ".results_display001_graph.json",
        )
        shutil.copy(
            results_files_dir / "validation_clusters.json",
            job_dir / ".results_display002_table.json",
        )
        job = DummyJob()
        job.output_dir = str(job_dir)
        rdos = job.load_results_display_files()
        assert len(rdos) == 3
        assert rdos[0].dobj_type == "gallery"
        assert rdos[1].dobj_type == "graph"
        assert rdos[2].dobj_type == "table"

        # Check a few example data items
        assert rdos[0].title == "2D class averages - Iteration 25/25"
        assert rdos[1].yvalues[3][0] == 0.143
        assert rdos[2].headers[0] == "Chain:Cluster"

    def test_loading_results_files_with_errors(self):
        job_dir = Path("DummyJobDir")
        job_dir.mkdir()
        (job_dir / ".results_display000_text.json").write_text('{"dobj_type": "text"}')
        (job_dir / ".results_display001_bad_type.json").write_text(
            '{"dobj_type": "bad_type"}'
        )
        (job_dir / ".results_display002_image.json").touch()
        job = DummyJob()
        job.output_dir = str(job_dir)
        rdos = job.load_results_display_files()
        assert len(rdos) == 3
        assert type(rdos[0]) is ResultsDisplayPending
        assert "missing 3 required keyword-only arguments" in rdos[0].reason
        assert rdos[1].reason == "Unknown DisplayObject type: bad_type"
        assert (
            "Error reading DummyJobDir/.results_display002_image.json" in rdos[2].reason
        )

    @staticmethod
    def make_job_info_file(comment=None):
        Path("Import/job001").mkdir(parents=True)
        jobinfo = {
            "job_directory": "Import/job001/",
            "comments": [] if comment is None else [comment],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: Ran job"],
            "command_history": ["2024-05-16 12:00:00.000: command number one"],
        }
        with open(Path("Import/job001/") / JOBINFO_FILE, "w") as jif:
            json.dump(jobinfo, jif, indent=2)

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_existing_jobinfo_file_no_comments(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        self.make_job_info_file()
        job = new_job_of_type("relion.import")
        job.output_dir = "Import/job001"
        update_jobinfo_file(
            job.output_dir,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001/") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "2024-05-16 12:00:00.000: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "2024-05-16 12:00:00.000: 1 2 3",
            ],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_existing_jobinfo_file_with_comments(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.002"
        self.make_job_info_file(comment="1st comment")
        job = new_job_of_type("relion.import")
        job.output_dir = "Import/job001/"
        update_jobinfo_file(
            job.output_dir,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["1st comment", "New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "2024-05-16 12:00:00.002: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "2024-05-16 12:00:00.002: 1 2 3",
            ],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_noexistant_jobinfo_file(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        Path("Import/job001").mkdir(parents=True)
        job = new_job_of_type("relion.import")
        job.output_dir = "Import/job001/"
        update_jobinfo_file(
            job.output_dir,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: New run"],
            "command_history": ["2024-05-16 12:00:00.000: 1 2 3"],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_jobinfo_file_error_on_read(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        indir = Path("Import/job001")
        indir.mkdir(parents=True)
        jof = indir / JOBINFO_FILE
        jof.touch()

        job = new_job_of_type("relion.import")
        job.output_dir = "Import/job001/"
        update_jobinfo_file(
            job.output_dir,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: New run"],
            "command_history": ["2024-05-16 12:00:00.000: 1 2 3"],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_jobinfo_file_dtt_conflict(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        self.make_job_info_file()
        job = new_job_of_type("relion.import")
        job.output_dir = "Import/job001"
        update_jobinfo_file(
            job.output_dir,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001/") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "2024-05-16 12:00:00.000: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "2024-05-16 12:00:00.000: 1 2 3",
            ],
        }

    @patch.dict(os.environ, {"SCRATCH_DIR": "subbed_scratch"})
    def test_substituting_env_var_in_command(self):
        jobfile = str(Path(self.test_data) / "StarFiles/env_var_job.star")
        shutil.copy(jobfile, "job.star")
        js = JobStar("job.star")
        jobops = js.joboptions
        jobops["_rlnJobTypeLabel"] = js.jobtype
        job = job_from_dict(jobops)
        job.output_dir = "Refine3D/job001/"
        com = job.get_final_commands()
        assert len(com) == 1
        assert "subbed_scratch" in com[0]

    @patch.dict(os.environ, {})
    def test_substituting_env_var_in_command_env_var_not_found(self):
        jobfile = str(Path(self.test_data) / "StarFiles/env_var_job.star")
        shutil.copy(jobfile, "job.star")
        js = JobStar("job.star")
        jobops = js.joboptions
        jobops["_rlnJobTypeLabel"] = js.jobtype
        job = job_from_dict(jobops)
        job.output_dir = "Refine3D/job001/"
        com = job.get_final_commands()
        assert len(com) == 1
        assert "$SCRATCH_DIR" in com[0]

    @patch("pipeliner.user_settings.get_qsub_extra_count")
    def test_qsub_template_eval_all_good(self, mock_count):
        mock_count.return_value = 4
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        with open(str(Path(self.test_data) / "submission_script.sh")) as subscript:
            sub_text = subscript.read()
        with expected_warning(nwarn=0):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_extra_count_wrong(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        with open(str(Path(self.test_data) / "submission_script.sh")) as subscript:
            sub_text = subscript.read()
        exp = (
            "Submission script template at 'sub' contains extra substitution variables"
            " 'XXXextra1XXX', 'XXXextra2XXX', 'XXXextra3XXX', 'XXXextra4XXX' but these"
            " have not been properly configured in PIPELINER_QSUB_EXTRA_COUNT, which"
            " currently specifies 0 extra qsub variables"
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_bad_sub_var(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        sub_text = "XXXcommandXXX\nXXXBADXXX"
        exp = (
            "Submission script template at 'sub' contains the substitution variable(s):"
            " 'XXXBADXXX' which are not recognised by the CCP-EM Pipeliner"
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_mpicom_subbed(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        sub_text = "runnit XXXcommandXXX"
        exp = (
            "Submission script template at 'sub' contains 'runnit' which looks like an"
            " mpirun command. Unlike RELION, for the CCP-EM Pipeliner this should be"
            " omitted from the template and instead be given in the 'MPI run command'"
            " job option."
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_mpicom_generic(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        sub_text = "mpirun -n XXXmpinodesXXX XXXcommandXXX"
        exp = (
            "Submission script template at 'sub' contains 'mpirun -n '"
            " which looks like an mpirun command. Unlike RELION, for the CCP-EM"
            " Pipeliner this should be omitted from the template and instead be given"
            " in the 'MPI run command' job option."
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_extra0(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        sub_text = "XXXcommandXXX\nXXXextra0XXX"
        exp = (
            "Submission script template at 'sub' contains substitution variable"
            " XXXextra0XXX, which is invalid. 'XXXextra' substitution variables begin"
            " at 1"
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)

    def test_qsub_template_eval_command_missing(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["qsubscript"].value = "sub"
        job.joboptions["mpi_command"].value = "runnit"
        sub_text = "XXXcoresXXX"
        exp = (
            "Submission script template at 'sub' does not contain the 'XXXcommandXXX'"
            " substitution variable and will not contain the job commands when executed"
        )
        with expected_warning(RuntimeWarning, msg=exp, nwarn=1):
            job.evaluate_qsub_template(sub_text)


def test_external_program_when_command_is_available_with_version(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    def mock_run_subprocess(command, **kwargs):
        class CompletedProcessStub:
            stdout = "1.0.0".encode()

        return CompletedProcessStub()

    monkeypatch.setattr(pipeliner_job, "run_subprocess", mock_run_subprocess)
    prog = pipeliner_job.ExternalProgram(
        "my_command", vers_com=["my_command", "--version"]
    )
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert prog.get_version() == "1.0.0"


def test_external_program_when_command_is_available_without_version(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    prog = pipeliner_job.ExternalProgram("my_command")
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert prog.get_version() is None


def test_external_program_when_command_is_unavailable(monkeypatch):
    def mock_which(command):
        return None

    monkeypatch.setattr(shutil, "which", mock_which)

    prog = pipeliner_job.ExternalProgram("my_command")
    assert prog.command == "my_command"
    assert prog.exe_path is None
    assert prog.get_version() is None


def test_external_program_when_version_command_fails(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    def mock_run_subprocess(command, **kwargs):
        raise RuntimeError("Version command failed")

    monkeypatch.setattr(pipeliner_job, "run_subprocess", mock_run_subprocess)
    prog = pipeliner_job.ExternalProgram(
        "my_command", vers_com=["my_command", "--version"]
    )
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert (
        prog.get_version()
        == "Error trying to determine version: Version command failed"
    )


def test_job_info_when_commands_are_available(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)
    prog1 = pipeliner_job.ExternalProgram("my_command_1")
    prog2 = pipeliner_job.ExternalProgram("my_command_2")
    job_info = pipeliner_job.JobInfo(external_programs=[prog1, prog2])

    assert job_info.is_available is True
    assert job_info.unavailable_message is None


def test_job_info_when_commands_are_unavailable(monkeypatch):
    def mock_which(command):
        return None

    monkeypatch.setattr(shutil, "which", mock_which)
    prog1 = pipeliner_job.ExternalProgram("my_command_1")
    prog2 = pipeliner_job.ExternalProgram("my_command_2")
    job_info = pipeliner_job.JobInfo(external_programs=[prog1, prog2])

    assert job_info.is_available is False
    assert job_info.unavailable_message == (
        "This job has been marked unavailable because: executable 'my_command_1' not "
        "found; executable 'my_command_2' not found"
    )


def test_job_info_when_no_commands_are_given(monkeypatch):
    job_info = pipeliner_job.JobInfo()
    assert job_info.is_available is True


if __name__ == "__main__":
    unittest.main()
