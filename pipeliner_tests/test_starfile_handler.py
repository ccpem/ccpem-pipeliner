#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from gemmi import cif
from typing import List
from pathlib import Path

from pipeliner.starfile_handler import (
    StarFile,
    BodyFile,
    JobStar,
    DataStarFile,
    interconvert_box_star_coords,
    get_starfile_loop_headers,
    read_relion_optimiser_starfile,
    fix_reserved_words,
    quotate_starfile_line,
)
from pipeliner_tests import test_data


class StarfileHandlerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @staticmethod
    def compare_two_files(file1, file2) -> List[List[List[str]]]:
        with open(file1) as f:
            f1_lines = f.readlines()
        with open(file2) as f:
            f2_lines = f.readlines()
        diffs: List[List[List[str]]] = []

        cleanf1, cleanf2 = [], []
        for line in f1_lines:
            if line.split():
                cleanf1.append(line.split())
        for line in f2_lines:
            if line.split():
                cleanf2.append(line.split())

        for n, oline in enumerate(cleanf1):
            try:
                if oline != cleanf2[n]:
                    diffs.append([oline, cleanf2[n]])
            except IndexError:
                diffs.append([oline, ["<no line>"]])
        return diffs

    def test_creating_StarFile_object(self):
        """Check reading a motioncorr file where the data labels
        have been changed to avoid using reserved words"""
        f = os.path.join(self.test_data, "StarFiles/motioncorr_changed.star")
        shutil.copy(f, os.path.join(self.test_dir, "thefile.star"))
        obj = StarFile("thefile.star")
        assert obj.file_name == "thefile.star"
        assert not os.path.isfile("thefile.star.old")

    def test_creating_StarFile_object_with_quotated_reserved_words_errors(self):
        """Check read a files that contains reserved words as labels
        but they have been quotated so they don't need to be corrected"""
        f = os.path.join(self.test_data, "StarFiles/motioncorr_quotated.star")
        shutil.copy(f, os.path.join(self.test_dir, "thefile.star"))
        obj = StarFile("thefile.star")
        assert obj.file_name == "thefile.star"
        assert not os.path.isfile("thefile.star.old")

    def test_creating_StarFile_object_with_reserved_words_errors(self):
        file = os.path.join(
            self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"
        )
        shutil.copy(file, "with_errors.star")
        obj = StarFile("with_errors.star")
        assert os.path.isfile("with_errors.star.orig")
        assert obj.file_name == "with_errors.star"
        diffs = self.compare_two_files("with_errors.star.orig", "with_errors.star")
        assert diffs == [
            [["fn_defect", '""'], ["fn_defect", "''"]],
            [
                ["gain_flip", '"No', "flipping", '(0)"'],
                ["gain_flip", "'No", "flipping", "(0)'"],
            ],
            [
                ["gain_rot", '"No', "rotation", '(0)"'],
                ["gain_rot", "'No", "rotation", "(0)'"],
            ],
            [["other_args", '""'], ["other_args", "''"]],
            [["other_motioncor2_args", '""'], ["other_motioncor2_args", "''"]],
            [["qsubscript", '""'], ["qsubscript", "''"]],
            [["save_noDW", "No"], ["'save_noDW'", "No"]],
            [["save_ps", "Yes"], ["'save_ps'", "Yes"]],
        ]

    def test_creating_BodyFile(self):
        """Test reading a body file for multibody refinement"""
        f = os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star")
        bodyfile = BodyFile(f)
        assert bodyfile.file_name == f
        assert bodyfile.bodycount == 3

    def test_creating_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        assert obj.file_name == "thefile_job.star"
        assert obj.joboptions == {"db_id": "7NDO", "format": "PDB"}
        assert obj.jobtype == "pipeliner.fetch.pdb"
        assert not obj.is_tomo
        assert not obj.is_continue

    def test_creating_JobStar_from_wrong_star_file_type(self):
        file = os.path.join(self.test_data, "autopick2.star")
        shutil.copy(file, "thefile_job.star")
        with self.assertRaises(ValueError):
            JobStar("thefile_job.star")

    def test_writing_from_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.is_continue = True
        obj.joboptions["db_id"] = "mbili"
        obj.write("altered.star")
        new = JobStar("altered.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "mbili"
        assert new.joboptions["format"] == "PDB"

    def test_writing_from_JobStar_overwrite(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.is_continue = True
        obj.joboptions["db_id"] = "mbili"
        obj.write()
        new = JobStar("thefile_job.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "mbili"
        assert new.joboptions["format"] == "PDB"

    def test_modify_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.modify({"_rlnJobIsContinue": "1", "db_id": "moja", "format": "mmCIF"})
        obj.write("altered.star")
        new = JobStar("altered.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "moja"
        assert new.joboptions["format"] == "mmCIF"

    def test_modify_JobStar_no_adding_allowed(self):
        with self.assertRaises(ValueError):
            file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
            shutil.copy(file, "thefile_job.star")
            obj = JobStar("thefile_job.star")
            obj.modify(
                {
                    "_rlnJobIsContinue": "1",
                    "db_id": "moja",
                    "format": "mmCIF",
                    "mbili": "tatu",
                }
            )

    def test_modify_JobStar_with_adding(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.modify(
            {
                "_rlnJobIsContinue": "1",
                "db_id": "moja",
                "format": "mmCIF",
                "mbili": "tatu",
            },
            allow_add=True,
        )
        obj.write("altered.star")
        new = JobStar("altered.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "moja"
        assert new.joboptions["format"] == "mmCIF"
        assert new.joboptions["mbili"] == "tatu"

    def test_DataStarFile_creation(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        assert obj.file_name == "data.star"
        assert type(obj.data) is cif.Document

    def test_DataStarFile_count_block(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        assert obj.count_block("optics") == 1
        assert obj.count_block("particles") == 2377

    def test_DataStarFile_get_block(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        block = obj.get_block("particles")
        assert type(block) is cif.Block
        loop = block.find_loop("_rlnCoordinateX")
        assert len(loop) == 2377
        assert loop[0] == "1614.217657"
        assert loop[-1] == "1083.670595"

    def test_DataStarFile_list_from_column(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        col = obj.column_as_list(blockname="particles", colname="_rlnCoordinateX")
        assert len(col) == 2377
        assert col[0] == "1614.217657"
        assert col[-1] == "1083.670595"

    def test_star_loop_as_list_all_single_row_with_headers(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list("optics", headers=True)
        expected = [
            [
                "_rlnOpticsGroupName",
                "_rlnOpticsGroup",
                "_rlnMtfFileName",
                "_rlnMicrographOriginalPixelSize",
                "_rlnVoltage",
                "_rlnSphericalAberration",
                "_rlnAmplitudeContrast",
                "_rlnMicrographPixelSize",
            ],
            [
                "opticsGroup1",
                "1",
                "mtf_k2_200kV.star",
                "0.885000",
                "200.000000",
                "1.400000",
                "0.100000",
                "0.885000",
            ],
        ]
        assert found == expected

    def test_star_loop_as_list_all_single_row_no_headers(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list("optics")
        expected = [
            [
                "opticsGroup1",
                "1",
                "mtf_k2_200kV.star",
                "0.885000",
                "200.000000",
                "1.400000",
                "0.100000",
                "0.885000",
            ],
        ]
        assert found == expected

    def test_star_loop_as_list_all_multi_rows(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list("micrographs", headers=True)
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
            "_rlnOpticsGroup",
            "_rlnAccumMotionTotal",
            "_rlnAccumMotionEarly",
            "_rlnAccumMotionLate",
        ]

        assert found[0] == columns
        assert len(found[1:]) == 24
        for i in found[1:]:
            assert len(i) == 7

    def test_star_loop_as_list_sample(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list(
            block="micrographs",
            columns=["_rlnCtfPowerSpectrum", "_rlnMicrographName"],
        )
        assert len(found) == 24
        assert all([len(x) == 2 for x in found])

    def test_star_loop_as_list_sample_with_headers(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list(
            block="micrographs",
            columns=["_rlnCtfPowerSpectrum", "_rlnMicrographName"],
            headers=True,
        )
        assert len(found) == 25
        assert all([len(x) == 2 for x in found])
        assert found[0] == ["_rlnCtfPowerSpectrum", "_rlnMicrographName"]

    def test_star_loop_as_list_specific_multi_rows(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        found = test_file.loop_as_list("micrographs", columns, headers=True)
        assert found[0] == columns
        assert len(found[1:]) == 24
        for i in found[1:]:
            assert len(i) == 3

    def test_star_loop_as_list_block_error(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        with self.assertRaises(ValueError):
            test_file.loop_as_list("bad", columns)

    def test_star_loop_as_list_column_error(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = ["bad_column"]
        with self.assertRaises(ValueError):
            test_file.loop_as_list("micrographs", columns)

    def test_star_loop_as_list_wrong_blocktype_error(self):
        test_file = StarFile(
            os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        )
        with self.assertRaises(ValueError):
            test_file.loop_as_list("job")

    def test_star_pairs_as_dict(self):
        test_file = StarFile(
            os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        )
        actual = test_file.pairs_as_dict("job")
        expected = {
            "_rlnJobTypeLabel": "relion.import.movies",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
        }
        assert actual == expected

    def test_star_pairs_as_dict_error_not_valuepair(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        with self.assertRaises(ValueError):
            test_file.pairs_as_dict("micrographs")

    def test_convert_star_to_box_2d_samename(self):
        starfile = os.path.join(self.test_data, "coords.star")
        shutil.copy(starfile, "coords.star")
        outfile = interconvert_box_star_coords("coords.star")
        assert outfile == "coords.box"
        with open(os.path.join(self.test_data, "coords.box")) as expd:
            expected = [x.strip("\n") for x in expd.readlines()]
        with open(os.path.join("coords.box")) as act:
            assert [x.strip("\n") for x in act.readlines()] == expected

    def test_convert_star_to_box_3d_samename(self):
        starfile = os.path.join(self.test_data, "coords3d.star")
        shutil.copy(starfile, "coords.star")
        outfile = interconvert_box_star_coords("coords.star")
        assert outfile == "coords.box"
        with open(os.path.join(self.test_data, "coords3d.box")) as expd:
            expected = [x.strip("\n") for x in expd.readlines()]
        with open(os.path.join("coords.box")) as act:
            assert [x.strip("\n") for x in act.readlines()] == expected

    def test_convert_star_to_box_2d_newname(self):
        starfile = os.path.join(self.test_data, "coords.star")
        outfile = interconvert_box_star_coords(starfile, "test/coords.box")
        assert outfile == "test/coords.box"
        with open(os.path.join(self.test_data, "coords.box")) as expd:
            expected = [x.strip("\n") for x in expd.readlines()]
        with open(os.path.join("test/coords.box")) as act:
            assert [x.strip("\n") for x in act.readlines()] == expected

    def test_jobstar_substitutions(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        js = JobStar("thefile_job.star")
        js.joboptions = {
            "1": "Import/job001/file1.star",
            "2": "Import/job002/file2.star",
            "3": "Import/job003/file3.star",
            "4": "Import/job004/file4.star",
            "6": "Import/job005/file5.star",
        }
        convs = {
            "Import/job001/": "Import/job002/",
            "Import/job002/": "Import/job001/",
            "Import/job003/file3.star": "Import/job002/file3.star",
        }
        js.make_substitutions(convs)
        assert js.joboptions == {
            "1": "Import/job002/file1.star",
            "2": "Import/job001/file2.star",
            "3": "Import/job002/file3.star",
            "4": "Import/job004/file4.star",
            "6": "Import/job005/file5.star",
        }

    def test_get_loop_headers_from_starfile(self):
        headers = get_starfile_loop_headers(
            str(Path(self.test_data) / "particles.star")
        )
        assert headers == {
            "optics": [
                "_rlnOpticsGroupName",
                "_rlnOpticsGroup",
                "_rlnMtfFileName",
                "_rlnMicrographOriginalPixelSize",
                "_rlnVoltage",
                "_rlnSphericalAberration",
                "_rlnAmplitudeContrast",
                "_rlnImagePixelSize",
                "_rlnImageSize",
                "_rlnImageDimensionality",
            ],
            "particles": [
                "_rlnCoordinateX",
                "_rlnCoordinateY",
                "_rlnAutopickFigureOfMerit",
                "_rlnClassNumber",
                "_rlnAnglePsi",
                "_rlnImageName",
                "_rlnMicrographName",
                "_rlnOpticsGroup",
                "_rlnCtfMaxResolution",
                "_rlnCtfFigureOfMerit",
                "_rlnDefocusU",
                "_rlnDefocusV",
                "_rlnDefocusAngle",
                "_rlnCtfBfactor",
                "_rlnCtfScalefactor",
                "_rlnPhaseShift",
            ],
        }

    def test_reading_job_star_with_relion5_name_conversion(self):
        js = JobStar(
            str(Path(self.test_data) / "JobFiles/Extract/reextract_name_job.star")
        )
        assert js.jobtype == "relion.extract"

    def test_read_optimiser_file_with_duplicate_entry_bug(self):
        opt_dict = read_relion_optimiser_starfile(
            str(Path(self.test_data) / "optimiser_duplicate_entries.star")
        )
        assert opt_dict["_rlnOutputRootName"] == "Class2D/job013/run"
        assert opt_dict["_rlnModelStarFile"] == "Class2D/job013/run_it100_model.star"
        assert opt_dict["_rlnExperimentalDataStarFile"] == (
            "Class2D/job013/run_it100_data.star"
        )
        assert len(opt_dict.keys()) == 98

    def test_fix_reserved_words_with_envvars(self):
        js = Path(self.test_data) / "StarFiles/env_var_job.star"
        shutil.copy(js, "job.star")
        been_corrected = fix_reserved_words("job.star")
        assert been_corrected
        os.system("cat job.star")
        # make sure gemmi can read it
        cif.read_file("job.star")

    def test_quotate_starfile_line_no_quote(self):
        assert quotate_starfile_line("item item item item") is None

    def test_quotate_starfile_line_space(self):
        assert quotate_starfile_line("item 'item b' item") is None

    def test_quotate_starfile_line_single_quotes(self):
        assert quotate_starfile_line("'item' 'item' 'item'") == "item item item"

    def test_quotate_starfile_line_double_quotes(self):
        assert quotate_starfile_line('"item" "item" "item"') == "item item item"

    def test_quotate_starfile_line_reserved_word(self):
        assert quotate_starfile_line("item _loop item") == "item '_loop' item"

    def test_quotate_starfile_line_underscore(self):
        assert quotate_starfile_line("item_1 item_2") is None

    def test_quotate_starfile_line_env_var(self):
        assert quotate_starfile_line("item $ITEM item") == "item '$ITEM' item"

    def test_quotate_starfile_line_empty_value_single_quotes(self):
        assert quotate_starfile_line("item item ''") is None

    def test_quotate_starfile_line_empty_value_double_quotes(self):
        assert quotate_starfile_line('item item ""') == "item item ''"

    def test_quotate_starfile_line_quote_and_empty_sgl(self):
        assert quotate_starfile_line("item 'item b' ''") is None

    def test_quotate_starfile_line_quote_and_empty_dbl(self):
        # Double quotes become single if an item needs quoting
        assert quotate_starfile_line('item "item b" ""') == "item 'item b' ''"

    def test_quotate_starfile_line_nested_quotes_sgl_in_dbl(self):
        assert quotate_starfile_line("\"'item'\"") is None

    def test_quotate_starfile_line_nested_quotes_dbl_in_sgl(self):
        assert quotate_starfile_line("\"'item'\"") is None

    def test_quotate_starfile_line_single_quote_inside(self):
        assert quotate_starfile_line('"it\'s complicated"') is None


if __name__ == "__main__":
    unittest.main()
