#
#    Copyright (C) 2024 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import io
import unittest
from unittest.mock import patch

from pipeliner.scripts.job_scripts import download_map_model


class DownloadMapModelTest(unittest.TestCase):

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_dl_progress_50_percent(self, mock_stdout):
        download_map_model.dl_progress(count=5, blockSize=100, totalSize=1000)
        assert mock_stdout.getvalue() == "0 KB, 50%\n"

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_dl_progress_100_percent(self, mock_stdout):
        download_map_model.dl_progress(count=10, blockSize=1000, totalSize=10000)
        assert mock_stdout.getvalue() == "9 KB, 100%\n"

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_dl_progress_over_100_percent(self, mock_stdout):
        download_map_model.dl_progress(count=10, blockSize=110, totalSize=1000)
        assert mock_stdout.getvalue() == "1 KB, 100%\n"

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_dl_progress_10_kb(self, mock_stdout):
        download_map_model.dl_progress(count=10, blockSize=1024, totalSize=-1)
        assert mock_stdout.getvalue() == "10 KB\n"

    @patch("sys.stdout", new_callable=io.StringIO)
    def test_dl_progress_1_kb(self, mock_stdout):
        download_map_model.dl_progress(count=11, blockSize=100, totalSize=-1)
        assert mock_stdout.getvalue() == "1 KB\n"
