#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_running_test, compare_starfiles


class JobStarWriterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def split_lines(self, linelist):
        return [x.split() for x in linelist]

    def test_with_postprocess_withmtf(self):
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/PostProcess/postprocess_adhocbf.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    str(Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", str(Path(self.test_data) / "emd_3488_mask.mrc")),
                ("Import/mtffile", str(Path(self.test_data) / "mtf_k2_200kV.star")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )
        actual_path = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_job.star"
        )
        wrote_path = os.path.join(self.test_dir, "PostProcess/job998/job.star")
        compare_starfiles(actual_path, wrote_path)

    def test_with_maskcreate_job(self):
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/MaskCreate/maskcreate.job"
            ),
            input_files=[("Import/job001", str(Path(self.test_data) / "emd_3488.mrc"))],
            expected_outfiles=("run.out", "run.err", "mask.mrc"),
        )
        actual_path = Path(self.test_data) / "JobFiles/MaskCreate/maskcreate_job.star"

        wrote_path = os.path.join(self.test_dir, "MaskCreate/job998/job.star")
        compare_starfiles(actual_path, wrote_path)


if __name__ == "__main__":
    unittest.main()
