# CCPEM-pipeliner

[![Documentation](https://readthedocs.org/projects/ccpem-pipeliner/badge/ "Documentation on ReadTheDocs")](https://ccpem-pipeliner.readthedocs.io/)

![ccpem-pipeliner header image](docs/source/header.png)

## Installation

Once the package has been downloaded navigate into the `ccpem-pipeliner` directory and
install the pipeliner with the command:

``pip install -e .``

## Check the installation

Once the pipeliner is installed use the command `pipeliner.check_setup` to check that
the setup is complete and the pipeliner can find the Relion programs it needs to run.

## Documentation

Documentation is available online at:
[ccpem-pipeliner.readthedocs.io/en/latest/](https://ccpem-pipeliner.readthedocs.io/en/latest/)

To build the documentation yourself, install the documentation build requirements
as follows:

`pip install -e .[docs]`

Then navigate to the `ccpem-pipeliner/docs` directory and run the command:

`make html`

Then open the file `ccpem-pipeliner/docs/_build/html/index.html` in a web browser to access the
documentation.

## Adding plugins

To add additional job plugins the plugin must first be written as a `PipelinerJob` object.
See the [pipeliner documentation](https://ccpem-pipeliner.readthedocs.io/en/latest/source/pipeliner_jobs.html)
for a description of the format.

The plugin must then be placed in the `Pipeliner/jobs/` folder or one of its subfolders, or create a new subfolder.

Once the plugin file is in place add it to the list of entry points in `pyproject.toml`.

If `pyproject.toml` has been modified update the pipeliner by running `pip install -e .` for the changes to
take effect.

## For Developers

It's a good idea to work in a virtual environment for this project. Set one up as
follows:

```
python3 -m venv venv/
source venv/bin/activate
pip install -e .[dev]
```

This project uses [pre-commit](https://pre-commit.com/) to run
[Black](https://black.readthedocs.io/) for code formatting,
[flake8](https://flake8.pycqa.org/) for linting and some other simple checks,
and [mypy](https://mypy.readthedocs.io/en/stable/) for type checks.

You might want to install Black separately yourself too, so you can run it from your
IDE.

According to the flake8 documentation, flake8 should not stop a git commit from going
ahead unless `flake8.strict` is set in the git config. That doesn't actually seem to
work: with the current configuration, commits fail if flake8 finds any problems. There
are some flake8 warnings that have not been fixed yet, so to get around this, flake8
checks can be disabled: `SKIP=flake8 git commit ...`

## Unit tests

Run the tests with `pytest`.

Some tests are quite slow, these tests are skipped by default. Set the
environment variable `PIPELINER_TEST_SLOW` to a non-empty string to run the slower
tests as well.

Some tests run actual programs, which can also make the test suite take much longer to
run. Set the environment variable `PIPELINER_TEST_LIVE` to a non-empty string to run
these tests as well. Live tests will only be run if the programs needed for them are
available.
