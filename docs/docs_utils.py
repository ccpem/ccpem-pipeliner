import sys
from pipeliner.node_factory import NODE_CLASSES


def get_nodes_table() -> None:
    """Print out all the nodes formatted for the table in 'guide_to_writing_jobs.rst'"""
    for node in NODE_CLASSES:
        nn = NODE_CLASSES[node]("node")
        print(f"\t* - {node}")
        print(f"\t  - {nn.toplevel_description}")
        print(f"\t  - NODE_{node.upper()}")


functions = [get_nodes_table]
find_funct = my_dict = {x.__name__: x for x in functions}

if __name__ == "__main__":
    funct = sys.argv[1]
    find_funct[funct]()
