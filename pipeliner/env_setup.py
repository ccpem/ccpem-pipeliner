#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import subprocess

from pipeliner import user_settings


def update_env_from_source_script(script) -> None:
    """
    Source bash script and update environment variables
    """
    # Adapted from https://gist.github.com/mammadori/3891614
    proc = subprocess.run(
        f". {script}; env -0", shell=True, capture_output=True, text=True
    )
    data = proc.stdout
    split_lines = [line.partition("=") for line in data.split("\0")]
    env_dict = {key: value for key, _, value in split_lines if key}
    os.environ.update(env_dict)


def prepend_paths_to_path(paths) -> None:
    os.environ["PATH"] = os.pathsep.join(paths) + os.pathsep + os.environ["PATH"]


def setup_python_environment(scripts=None) -> None:
    """Set python enviroment by:
    1)  Find source script locations from settings and update python
        environment
    2)  Add additional paths defined in settings to be added to PATH"""

    # Source script locations
    if scripts is None:
        scripts = user_settings.get_path_to_source_files()

    for script in scripts:
        update_env_from_source_script(script=script)

    # Set additinal paths
    paths = user_settings.get_additional_program_paths()
    if paths:
        prepend_paths_to_path(paths=paths)
