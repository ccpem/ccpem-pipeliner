#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import mrcfile
import json
from typing import Dict, Tuple, Callable, Optional, List
from gemmi import cif
from PIL import Image, UnidentifiedImageError
import logging

from pipeliner.utils import get_mrc_map_header_string, make_atomic_model_summary
from pipeliner.process import Process
from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_stack,
    mini_montage_from_starfile,
    get_next_resultsfile_name,
    make_particle_coords_thumb,
    make_mrcs_central_slices_montage,
)
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
    ResultsDisplayPending,
)
from pipeliner.mrc_image_tools import tiff_thumbnail, mrc_thumbnail
from pipeliner.utils import file_in_project
from pipeliner.data_structure import (
    NODE_ATOMCOORDS,
    NODE_ATOMCOORDSGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_DENSITYMAPMETADATA,
    NODE_DENSITYMAPGROUPMETADATA,
    NODE_EULERANGLES,
    NODE_EVALUATIONMETRIC,
    NODE_IMAGE2D,
    NODE_IMAGE2DSTACK,
    NODE_IMAGE2DMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_IMAGE3D,
    NODE_IMAGE3DMETADATA,
    NODE_IMAGE3DGROUPMETADATA,
    NODE_LIGANDDESCRIPTION,
    NODE_LOGFILE,
    NODE_MASK2D,
    NODE_MASK3D,
    NODE_MICROGRAPHCOORDS,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPH,
    NODE_MICROGRAPHMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIE,
    NODE_MICROGRAPHMOVIEMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_MLMODEL,
    NODE_OPTIMISERDATA,
    NODE_PARAMSDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_PROCESSDATA,
    NODE_RESTRAINTS,
    NODE_RIGIDBODIES,
    NODE_SEQUENCE,
    NODE_SEQUENCEGROUP,
    NODE_SEQUENCEALIGNMENT,
    NODE_STRUCTUREFACTORS,
    NODE_TILTSERIES,
    NODE_TILTSERIESMETADATA,
    NODE_TILTSERIESGROUPMETADATA,
    NODE_TILTSERIESMOVIE,
    NODE_TILTSERIESMOVIEMETADATA,
    NODE_TILTSERIESMOVIEGROUPMETADATA,
    NODE_TOMOGRAM,
    NODE_TOMOGRAMMETADATA,
    NODE_TOMOGRAMGROUPMETADATA,
    NODE_DISPLAY_FILE,
    NODE_DISPLAY_DIR,
    NODE_SUMMARIES_FILE,
    NODE_SUMMARIES_BLOCK_NAME,
    NODE_TOMOMANIFOLDDATA,
    NODE_TOMOTRAJECTORYDATA,
    NODE_TOMOOPTIMISATIONSET,
)

logger = logging.getLogger(__name__)


def check_file_is_mrc(file: str) -> bool:
    """Validate that a file is a mrc file"""
    try:
        mrcfile.open(file, header_only=True)
        return True
    except Exception:
        logger.info(
            f"File '{file}' does not appear to be a valid MRC file", exc_info=True
        )
        return False


def check_file_is_cif(file: str) -> bool:
    """Validate that a file is a cif or star file"""
    try:
        cif.read(file)
        return True
    except Exception:
        logger.info(f"File '{file}' does not appear to be CIF/STAR file", exc_info=True)
        return False


def check_file_is_tif(file: str) -> bool:
    try:
        with Image.open(file) as im:
            imgformat = im.format
    except UnidentifiedImageError:
        return False
    if imgformat == "TIFF":
        return True
    return False


def check_file_is_text(file: str) -> bool:
    try:
        with open(file, "r") as f:
            # Just try reading up to 1 MB of the file to see if it is decodable as text.
            # This should avoid wasting excessive amounts of time or memory if the file
            # is very large while still giving a reasonably safe indication of whether
            # the file can be decoded as text or not.
            f.read(1048576)
            return True
    except UnicodeDecodeError:
        return False


def check_file_is_json(file: str) -> bool:
    # File must be decodable as text before we can even try to parse it as JSON
    if not check_file_is_text(file):
        return False
    try:
        with open(file, "r") as f:
            json.load(f)
            return True
    except Exception:
        return False


def add_to_node_display_file(node_name: str, display_file: str) -> None:
    """Add an entry to the node display file

    Args:
        node_name (str): The name of the node file
        display_file (str): Path to the display file
    """

    nodes = None
    if os.path.isfile(NODE_DISPLAY_FILE):
        ciffile = cif.read_file(NODE_DISPLAY_FILE)
        block = ciffile.find_block("pipeliner_node_display")
        nodes = block.find("_pipeliner", ["NodeName", "DisplayFile"])

    # updated cif document
    new_cif = cif.Document()
    new_bl = new_cif.add_new_block("pipeliner_node_display")
    updated = new_bl.init_loop("_pipeliner", ["NodeName", "DisplayFile"])

    # see if the node already exists
    if nodes:
        for line in nodes:
            if node_name != line[0]:
                updated.add_row(line)
    updated.add_row([node_name, display_file])

    new_cif.write_file(NODE_DISPLAY_FILE)


def get_node_results_display_file_name(node_name: str) -> Optional[str]:
    """Get the results display file name for a node from the node display file.

    Returns: the name of the node results display file, or ``None`` if there is no entry
        for this node name in the node display file.
    """
    if os.path.isfile(NODE_DISPLAY_FILE):
        try:
            ciffile = cif.read_file(NODE_DISPLAY_FILE)
            block = ciffile.find_block("pipeliner_node_display")
            nodes = block.find("_pipeliner", ["NodeName", "DisplayFile"])
            for line in nodes:
                if line[0] == node_name:
                    return line[1]
        except Exception:
            logger.exception("Error trying to get node results file name")
    return None


def remove_from_node_display_file(node_name: str) -> None:
    """
    Remove an entry from the node display file

    Args:
        node_name (str): The file to remove

    """
    if not os.path.isfile(NODE_DISPLAY_FILE):
        logger.error(
            f"Could not remove {node_name} from {NODE_DISPLAY_FILE} because "
            "the entry was not found"
        )
        return

    ciffile = cif.read_file(NODE_DISPLAY_FILE)
    block = ciffile.find_block("pipeliner_node_display")
    nodes = block.find("_pipeliner", ["NodeName", "DisplayFile"])
    if not nodes:
        return

    # updated cif document
    new_cif = cif.Document()
    new_bl = new_cif.add_new_block("pipeliner_node_display")
    updated = new_bl.init_loop("_pipeliner", ["NodeName", "DisplayFile"])
    for line in nodes:
        if node_name != line[0]:
            updated.add_row(line)

    new_cif.write_file(NODE_DISPLAY_FILE)


def add_to_node_summary_list(node_name: str, summary_file: str) -> None:
    """Add an entry to the node summary list

    Args:
        node_name (str): The name of the node
        summary_file (str): Path to the summary file
    """

    nodes = None
    if os.path.isfile(NODE_SUMMARIES_FILE):
        ciffile = cif.read_file(NODE_SUMMARIES_FILE)
        block = ciffile.find_block(NODE_SUMMARIES_BLOCK_NAME)
        nodes = block.find("_pipeliner", ["NodeName", "SummaryFile"])

    # updated cif document
    new_cif = cif.Document()
    new_bl = new_cif.add_new_block(NODE_SUMMARIES_BLOCK_NAME)
    updated = new_bl.init_loop("_pipeliner", ["NodeName", "SummaryFile"])

    # see if the node already exists
    if nodes is not None:
        for line in nodes:
            if node_name != line[0]:
                updated.add_row(line)
    updated.add_row([node_name, summary_file])

    new_cif.write_file(NODE_SUMMARIES_FILE)


def get_node_summary_file_from_list(node_name: str) -> Optional[str]:
    """Get the summary file for a node from the node summary list.

    Returns: the name of the node summary file, or ``None`` if there is no entry
        for this node name in the summary list.
    """
    if os.path.isfile(NODE_SUMMARIES_FILE):
        try:
            ciffile = cif.read_file(NODE_SUMMARIES_FILE)
            block = ciffile.find_block(NODE_SUMMARIES_BLOCK_NAME)
            nodes = block.find("_pipeliner", ["NodeName", "SummaryFile"])
            for line in nodes:
                if line[0] == node_name:
                    return line[1]
        except Exception:
            logger.exception("Error trying to get node summary from list")
    return None


def remove_from_node_summary_list(node_name: str) -> None:
    """
    Remove an entry from the node summary list

    Args:
        node_name (str): The file to remove

    """
    if not os.path.isfile(NODE_SUMMARIES_FILE):
        logger.error(
            f"Could not remove {node_name} from {NODE_SUMMARIES_FILE} because "
            "the entry was not found"
        )
        return

    ciffile = cif.read_file(NODE_SUMMARIES_FILE)
    block = ciffile.find_block(NODE_SUMMARIES_BLOCK_NAME)
    nodes = block.find("_pipeliner", ["NodeName", "SummaryFile"])
    if not nodes:
        return

    # updated cif document
    new_cif = cif.Document()
    new_bl = new_cif.add_new_block(NODE_SUMMARIES_BLOCK_NAME)
    updated = new_bl.init_loop("_pipeliner", ["NodeName", "SummaryFile"])
    for line in nodes:
        if node_name != line[0]:
            updated.add_row(line)

    new_cif.write_file(NODE_SUMMARIES_FILE)


class GenNodeFormatConverter(object):
    """Convert node types for files that have different but equivalent file extensions

    Attributes:
        allowed_ext (dict): A dict of extensions that are allowed for that nodetype
            IE: {("mrc", "mrcs", "map"): "mrc"}
        check_funct (staticmethod): A function that checks a file is of the expected
            type, must return a bool
    """

    def __init__(
        self,
        allowed_ext: Dict[Tuple[str, ...], str],
        check_funct: Dict[str, Callable],
    ) -> None:
        """Instantiate a GenNodeFormatConverter

        Args:
            allowed_ext (Dict[Tuple[str],str]): Extensions that are possible for that
                type of node file and the general name they converge to IE:
                {("mrc", "mrcs", "map"): "mrc"}.  This is just for generalising node
                types, the file CAN have other extensions besides ones in the dict
            check_funct (Dict[str, staticmethod]): A function that checks a file is of
                the expected type, must take the file as input and return an error
                 message if there is a problem or an empty string if all good.
                 Matched to the general nodetypes in the dict IE: {"mrc": check_if_mrc,
                  "star": check_if_star}
        """
        self.allowed_ext = allowed_ext
        self.check_funct = check_funct

    def gen_nodetype(
        self, filename: str, override_format: str = "", validate_file: bool = True
    ) -> str:
        """Assign a file to a general node type

        Args:
            filename (str): File to create the node for
            override_format (str): The node's format
            validate_file (bool): DO validation on the file type - turn it off to speed
                up rewriting an entire pipeline
        """
        gen_ext = ""
        ext = os.path.splitext(filename)[1].strip(".")
        ext = override_format if override_format else ext
        ext = "XX" if not ext else ext

        for type_tup in self.allowed_ext:
            if ext in type_tup:
                gen_ext = self.allowed_ext[type_tup]
                break

        gen_ext = ext if not gen_ext else gen_ext

        if gen_ext in self.check_funct and os.path.isfile(filename) and validate_file:
            if not self.check_funct[gen_ext](filename):
                return "X" + gen_ext + "X"

        return gen_ext


class Node(object):
    """Nodes store info about input and output files of jobs that have been run

    Attributes:
        name (str): The name of the file the node represents
        toplevel_type (str): The toplevel node type
        toplevel_description: (str): A description of the toplevel type
        format (str): The node's generalised file type IE ('mrc' for mrc, 'mrcs' or
            'map')
        kwds (list[str]): Keywords associated with the node
        output_from_process (:class:`~pipeliner.process.Process`): The
            Process object for process that created the file
        input_for_processes_list (list): A list of
            :class:`~pipeliner.process.Process` objects for processes
            that use the node as an input
        format_converter (Optional[GenNodeFormatConverter]): An object that defines
            the expected extensions for the node and how to validate them
        type (str): The full node type
    """

    def __init__(
        self,
        name: str,
        toplevel_type: str,
        kwds: Optional[List[str]] = None,
        toplevel_description: str = (
            "A general node type for files without a more specific Node class defined"
        ),
        format_converter: Optional[GenNodeFormatConverter] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        """Create a Node object
        Args:
            name (str): The name of the file the node represents
            toplevel_type (str): The toplevel nodetype
            kwds (list): node keywords
            toplevel_description (str): A description of what the nodetype is for
            format_converter (GenNodeFormatConverter): The object that is used to
                generalise the node format and validate the files
            do validation (bool): Should the node attempt to validate the file type
                might wnat to turn this off if a large number of nodes are generated
                at the same time (like when the ProjectGraph reads a pipeline)
            override_format (str): Override the automatically assigned format for a node
                can be used to assign format to a file that has no extension, defaults
                to 'XX' for files of unknown format with no extension to infer it from.

        Raises:
            ValueError: If the name is empty
        """
        if name:
            self.name = str(name)
        else:
            raise ValueError("Cannot create a node with no name")
        self.kwds = [] if not kwds else [x.lower() for x in kwds]
        self.toplevel_type = toplevel_type
        self.toplevel_description = toplevel_description

        # updated by the node factory if the node format can be generalised
        ext = os.path.splitext(self.name)[1].strip(".")
        if override_format:
            self.format = override_format
        elif ext:
            self.format = ext
        else:
            self.format = "XX"
        self.format_converter = format_converter
        if self.format_converter:
            self.format = self.format_converter.gen_nodetype(
                self.name, self.format, validate_file=do_validation
            )

        # type is taken as given, subclasses will validate and update
        self.type = self.toplevel_type + "." + self.format
        if self.kwds:
            joinkwds = ".".join(self.kwds)
            self.type += f".{joinkwds}"

        # must have producer process, can have consumers
        self.output_from_process: Optional[Process] = None
        self.input_for_processes_list: list = []

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}("{self.name}", "{self.type}")'

    def generalise_format(self) -> None:
        """Set the node's format

        generalise node format for files that have different but equivalent file
        extensions EX 'mrc' for '.map', or '.mrc' files

        defaults to the file extension
        """
        if self.format_converter:
            self.format = self.format_converter.gen_nodetype(self.name)

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        """Generate a ResultsDisplayObject for the node

        This method will be used for every node in Process if there is no
        'create_results_display' function defined
        """

        # if the node is not in the project directory it won't be displayed so as not
        # to make display files outside the project
        projdir = os.getcwd()
        if not file_in_project(self.name):
            return create_results_display_object(
                "text",
                title="Node is outside of project",
                display_data=(
                    "This node cannot be displayed because it is outside of the "
                    f"project directory {projdir}."
                ),
                associated_data=[],
            )

        message = "Nothing to display"
        reason = (
            f"The node type for this file in {output_dir} ({self.type}) has no "
            "default display method"
        )
        try:
            if check_file_is_json(self.name):
                return create_results_display_object("json", file_path=self.name)
            elif check_file_is_text(self.name):
                return create_results_display_object("textfile", file_path=self.name)
        except FileNotFoundError as e:
            message = str(e)
            reason = f"The file {self.name} was not found"

        return create_results_display_object(
            "pending",
            title=self.name,
            message=message,
            reason=reason,
        )

    def get_summary_file_name(
        self, output_dir: Optional[str], tag: Optional[str] = None
    ) -> str:
        """Get the summary file name"""
        summary_file = os.path.basename(self.name).replace(".", "_")
        if output_dir:
            summary_file = os.path.join(
                output_dir,
                summary_file,
            )
        if tag:
            summary_file = summary_file + "_" + tag
        summary_file += "_summary.txt"
        return summary_file

    def write_summary_string_as_text_file(
        self, text_str: str, summary_file: str
    ) -> None:
        """Write a summary text file with input summary string"""
        with open(summary_file, "w") as s:
            s.write(text_str)

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        """Generate a summary file for the node

        This method should be overridden by every node where a summary needs to
        be generated
        """
        raise NotImplementedError(f"Summary not implemented for {self.name}")

    def not_compatible_with_default_output(self) -> ResultsDisplayObject:
        """Return a ResultsDisplayText object for jobs with no default display object"""
        return create_results_display_object(
            "text",
            title="No graphical display for this node",
            display_data=f"File format not compatible with {self.toplevel_type} node's "
            "default display method",
            associated_data=[self.name],
        )

    def load_results_display_file(self) -> Optional[ResultsDisplayObject]:
        """Load the node's results display object from a file on disk.

        This method must be fast because it is used by the GUI to load node results.
        Therefore, if the display object fails to load properly, no attempt is made to
        recalculate it and a ``ResultsDisplayPending`` object is returned instead.

        If there is no results display file yet, ``None`` is returned.

        Returns:
            A :class:`~pipeliner.results_display_objects.ResultsDisplayObject`, or
            ``None``.
        """
        rd_file = get_node_results_display_file_name(self.name)
        if rd_file is not None:
            try:
                with open(rd_file, "r") as dfile:
                    ddict = json.load(dfile)
                    return create_results_display_object(**ddict)
            except Exception as ex:
                return ResultsDisplayPending(
                    message="Error loading results display",
                    reason=f"Error reading {rd_file}: {ex}",
                )
        return None

    def find_summary_file(self) -> Optional[str]:
        """Get this node's summary file name from the summary list.

        If there is no summary file yet, ``None`` is returned.

        Returns:
            Summary file path
        """
        summary_file = get_node_summary_file_from_list(self.name)
        if summary_file is not None:
            return summary_file
        return None

    def write_summary_file(self) -> None:
        """Write the summary file for this node and add it to the summary list."""
        if file_in_project(self.name):
            outdir = os.path.join(os.path.dirname(self.name), NODE_DISPLAY_DIR)
        else:
            # TODO: is this condition ever true?
            # if the node is not in the project, create its display item in the general
            # project dir instead of outside the project
            outdir = NODE_DISPLAY_DIR
        os.makedirs(outdir, exist_ok=True)

        # write the file
        try:
            summary_file = self.get_summary_file_name(output_dir=outdir)
            summary_str = self.make_summary_file(output_dir=outdir)
            if not os.path.isfile(summary_file):  # file may be created above
                self.write_summary_string_as_text_file(
                    text_str=summary_str,
                    summary_file=summary_file,
                )
        except NotImplementedError:
            # if not implemented, get the full file contents for display
            summary_file = self.name
        except Exception as exc:
            summary_file = self.get_summary_file_name(output_dir=outdir, tag="failed")
            self.write_summary_string_as_text_file(
                text_str="Summary generation failed: \n" + str(exc),
                summary_file=summary_file,
            )
        add_to_node_summary_list(self.name, summary_file)

    def write_default_result_file(self) -> None:
        """Write the default display file for this node"""
        if file_in_project(self.name):
            outdir = os.path.join(os.path.dirname(self.name), NODE_DISPLAY_DIR)
        else:
            # if the node is not in the project, create its display item in the general
            # project dir instead of outside the project
            outdir = NODE_DISPLAY_DIR
        os.makedirs(outdir, exist_ok=True)

        # write the file
        do = self.default_results_display(output_dir=outdir)
        final_out = get_next_resultsfile_name(
            dir=outdir, search_str=f"results_display*_{do.dobj_type}.json"
        )
        with open(final_out, "w") as outf:
            json.dump(do.__dict__, outf, indent=4)

        add_to_node_display_file(self.name, final_out)

    def get_result_display_object(self) -> ResultsDisplayObject:
        """Get the default results display object for the node.

        This method loads the results display object from a saved file on disk if there
        is one. If not, a new results display object is generated and saved.

        Returns:
            ResultsDisplayObject: The default results display object for the node
        """
        rdo = self.load_results_display_file()
        if rdo is None:
            try:
                self.write_default_result_file()
                rdo = self.load_results_display_file()
            except Exception as ex:
                rdo = ResultsDisplayPending(
                    message="Error creating results display",
                    reason=str(ex),
                )
        if rdo is None:
            rdo = ResultsDisplayPending(
                message="Could not create results display",
                reason="",
            )
        return rdo


# Node classes
class AtomCoordsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_ATOMCOORDS,
            toplevel_description="An atomic model file",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("pdb", "ent"): "pdb", ("mmcif", "cif"): "cif"},
                check_funct={"cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "mapmodel",
            title=self.name,
            models=[self.name],
            associated_data=[self.name],
        )

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        summary_file = self.get_summary_file_name(output_dir)
        make_atomic_model_summary(self.name, summary_file)
        if os.path.isfile(summary_file):
            with open(summary_file, "r") as s:
                return s.read()
        else:
            return super().make_summary_file(output_dir)


class AtomCoordsGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_ATOMCOORDSGROUPMETADATA,
            toplevel_description=(
                "Metadata about a set of atomic coordinates files, a list of atomic "
                "models for example"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class DensityMapNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAP,
            toplevel_description=(
                "A 3D cryoEM density map (could be half map, full map, sharpened etc)"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs", "map"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return make_mrcs_central_slices_montage(
            output_dir=output_dir,
            in_files={self.name: ""},
        )

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        return get_mrc_map_header_string(mapfile=self.name)


class DensityMapMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAPMETADATA,
            toplevel_description="Metadata about a single density map",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class DensityMapGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAPGROUPMETADATA,
            toplevel_description="Metadata about multiple density maps",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class EulerAnglesNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_EULERANGLES,
            toplevel_description="Data about Euler angles",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class EvaluationMetricNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        toplevel_description: str = "A file containing evaluation metrics",
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name,
            toplevel_type=NODE_EVALUATIONMETRIC,
            kwds=kwds,
            toplevel_description=toplevel_description,
            do_validation=do_validation,
            override_format=override_format,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("out",): "txt"},
                check_funct={},
            ),
        )


class Image2DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2D,
            toplevel_description="A single 2D image",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("tif", "tiff"): "tif"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        thumbs_dir = os.path.join(output_dir, "Thumbnails")
        os.makedirs(thumbs_dir, exist_ok=True)
        imgname = os.path.splitext(os.path.basename(self.name))[0]
        image_path = ""
        if self.format == "mrc":
            mrc_thumbnail(
                self.name,
                640,
                os.path.join(thumbs_dir, imgname + ".png"),
            )
            image_path = os.path.join(output_dir, f"Thumbnails/{imgname}.png")

        elif self.format == "tif":
            tiff_thumbnail(
                self.name,
                640,
                os.path.join(thumbs_dir, imgname + ".png"),
            )
            image_path = os.path.join(output_dir, f"Thumbnails/{imgname}.png")

        elif self.format == "png":
            image_path = self.name

        if image_path:
            return create_results_display_object(
                "image",
                title=self.name,
                image_path=image_path,
                image_desc=self.name,
                associated_data=[self.name],
            )

        else:
            return super().default_results_display(output_dir)

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class Image2DMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DMETADATA,
            toplevel_description="Metadata about a single 2D image or stack",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class Image2DGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DGROUPMETADATA,
            toplevel_description="Metadata about a group of 2D images or stacks",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion 2D class averages
        if self.format == "star":
            dispobj = mini_montage_from_starfile(
                starfile=self.name,
                block="",
                column="_rlnReferenceImage",
                outputdir=output_dir,
            )
            if dispobj.dobj_type == "montage":
                return dispobj

        # all others use default
        return super().default_results_display(output_dir)


class Image2DStackNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DSTACK,
            toplevel_description="A single file containing a stack of 2D images",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return mini_montage_from_stack(stack_file=self.name, outputdir=output_dir)

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class Image3DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3D,
            toplevel_description=(
                "Any 3D image that is not a density map or mask, for example a local"
                " resolution map, 3D FSC or cryoEF 3D transfer function"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir):
        if self.format == "mrc":
            return make_mrcs_central_slices_montage(
                output_dir=output_dir,
                in_files={self.name: self.name},
            )
        else:
            return super().default_results_display(output_dir)

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class Image3DMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3DMETADATA,
            toplevel_description=(
                "Metadata about a single 3D image, except density maps, which have "
                "their own specific node type (DensityMap)"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default format - too many file type possibilities


class Image3DGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3DGROUPMETADATA,
            toplevel_description=(
                "Metadata about a group of 3D images (but not masks or "
                "density maps, which have their own specific types)"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class LigandDescriptionNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_LIGANDDESCRIPTION,
            toplevel_description="The stereochemical description of a ligand molecule",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("smi",): "txt"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default_display format - don't know what the format of these files is


class LogFileNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_LOGFILE,
            toplevel_description=(
                "A log file from a process. Could be PDF, text or another format"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("err", "log", "out", "txt"): "txt"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for pdfs
        if self.format == "pdf":
            return create_results_display_object(
                "pdffile",
                title=self.name,
                file_path=self.name,
            )
        return super().default_results_display(output_dir)


class Mask2DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MASK2D,
            toplevel_description="A mask for use with 2D images",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        thumbs_dir = os.path.join(output_dir, "Thumbnails")
        os.makedirs(thumbs_dir, exist_ok=True)
        imgname = os.path.splitext(os.path.basename(self.name))[0]
        image_path = ""
        if self.format == "mrc":
            mrc_thumbnail(
                self.name,
                640,
                os.path.join(thumbs_dir, imgname + ".png"),
            )
            image_path = os.path.join(output_dir, f"Thumbnails/{imgname}.png")
        elif self.format == "tif":
            tiff_thumbnail(
                self.name,
                640,
                os.path.join(thumbs_dir, imgname + ".png"),
            )
            image_path = os.path.join(output_dir, f"Thumbnails/{imgname}.png")
        elif self.format == "png":
            image_path = self.name

        if image_path:
            return create_results_display_object(
                "image",
                title=self.name,
                image_path=image_path,
                image_desc=self.name,
                associated_data=[self.name],
            )

        else:
            return super().default_results_display(output_dir)

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class Mask3DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MASK3D,
            toplevel_description="A mask for use with 3D volumes",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs", "map"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return make_mrcs_central_slices_montage(
            output_dir=output_dir,
            in_files={self.name: ""},
        )

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        return get_mrc_map_header_string(mapfile=self.name)


class MicrographCoordsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHCOORDS,
            toplevel_description=(
                "A file containing coordinate info for a single micrograph, "
                "e.g. a .star or .box file produced from picking"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MicrographCoordsGroupNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHCOORDSGROUP,
            toplevel_description=(
                "A file containing coordinate info for multiple micrographs, "
                "e.g. a STAR file with a list of coordinate files as created by "
                "a RELION picking job"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        if "relion" in self.kwds:
            try:
                coords = cif.read_file(self.name)
                cb = coords.find_block("coordinate_files")
                files = cb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]
                dobj = make_particle_coords_thumb(files[0], files[1], output_dir)
                if dobj.dobj_type == "image":
                    return dobj

            except Exception as e:
                return create_results_display_object(
                    "pending",
                    title="Error reading RELION coordinates file",
                    message=(
                        "The file is expected to be a RELION format coordinates file "
                        "but cannot be parsed correctly"
                    ),
                    reason=str(e),
                )
        return super().default_results_display(output_dir)


class MicrographNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPH,
            toplevel_description="A single micrograph",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc", ("tiff", "tif"): "tif"},
                check_funct={"mrc": check_file_is_mrc, "tif": check_file_is_tif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "image",
            title=self.name,
            image_path=self.name,
            image_descr="",
            associated_data=[self.name],
        )

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class MicrographMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMETADATA,
            toplevel_description="Metadata about a single micrograph",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MicrographGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHGROUPMETADATA,
            toplevel_description=(
                "Metadata about a set of micrographs, for example a RELION "
                "corrected_micrographs.star file"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion micrographs files
        if "relion" in self.kwds and self.format == "star":
            return mini_montage_from_starfile(
                starfile=self.name,
                block="micrographs",
                column="_rlnMicrographName",
                outputdir=output_dir,
                nimg=3,
            )
        # all others use defults
        return super().default_results_display(output_dir)


class MicrographMovieNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIE,
            toplevel_description="A single multi-frame micrograph movie",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc", ("tiff", "tif"): "tif"},
                check_funct={"mrc": check_file_is_mrc, "tif": check_file_is_tif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return mini_montage_from_stack(
            stack_file=self.name,
            outputdir=output_dir,
        )

    def make_summary_file(self, output_dir: Optional[str] = None) -> str:
        if self.format == "mrc":
            return get_mrc_map_header_string(mapfile=self.name)
        else:
            return super().make_summary_file(output_dir=output_dir)


class MicrographMovieMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIEMETADATA,
            toplevel_description="Metadata about a single multi-frame micrograph movie",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default results display too many possible file formats


class MicrographMovieGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            toplevel_description=(
                "Metadata about multiple micrograph movies, e.g. movies.star"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats
    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion movie files
        if "relion" in self.kwds and self.format == "star":
            return mini_montage_from_starfile(
                starfile=self.name,
                block="movies",
                column="_rlnMicrographMovieName",
                outputdir=output_dir,
                nimg=2,
            )

        # all others use default
        return super().default_results_display(output_dir)


class MicroscopeDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROSCOPEDATA,
            toplevel_description=(
                "Data about the microscope, such as collection parameters, defect "
                "files or MTF curves"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={
                    ("cif", "mmcif"): "cif",
                    ("mrc", "mrcs"): "mrc",
                    ("tiff", "tif"): "tif",
                },
                check_funct={
                    "star": check_file_is_cif,
                    "mrc": check_file_is_mrc,
                    "cif": check_file_is_cif,
                    "tif": check_file_is_tif,
                },
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MlModelNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_MLMODEL,
            toplevel_description="A machine learning model",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "text",
            title="No graphical output for ML Models",
            display_data=(
                f"The node type for this file in {output_dir} ({self.type}) has no "
                "graphical output"
            ),
            associated_data=[self.name],
        )


class OptimiserDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_OPTIMISERDATA,
            toplevel_description=(
                "Specific type for RELION optimiser data from a refinement or"
                " classification job"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class ParamsDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_PARAMSDATA,
            toplevel_description=(
                "Contains parameters for running an external program"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class ParticleGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_PARTICLEGROUPMETADATA,
            toplevel_description=(
                "Metadata for a set of particles, e.g particles.star from RELION"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        if self.format == "star" and "relion" in self.kwds:
            return mini_montage_from_starfile(
                starfile=self.name,
                block="particles",
                column="_rlnImageName",
                outputdir=output_dir,
            )

        return self.not_compatible_with_default_output()


class ProcessDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_PROCESSDATA,
            toplevel_description=(
                "Other data resulting from a process that might be of use for other "
                "processes, for example an optimiser STAR file, postprocess STAR "
                "file or particle polishing parameters"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display format - to many possible file formats


class RestraintsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_RESTRAINTS,
            toplevel_description=(
                "Distance and angle restraints for atomic model refinement"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method, too many possible file formats


class RigidBodiesNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_RIGIDBODIES,
            toplevel_description="A description of rigid bodies in an atomic model",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method, too many possible file formats


class SequenceNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_SEQUENCE,
            toplevel_description="A protein or nucleic acid sequence file",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("fasta", "seq"): "fasta"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a SequenceDisplayObject


class SequenceGroupNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_SEQUENCEGROUP,
            toplevel_description="A group of protein or nucleic acid sequences",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("fasta", "seq"): "fasta"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a SequenceDisplayObject


class SequenceAlignmentNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_SEQUENCEALIGNMENT,
            toplevel_description=(
                "Files that contain or are used to generate multi sequence alignments"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a SequenceDisplayObject


class StructureFactorsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_STRUCTUREFACTORS,
            toplevel_description=(
                "A set of structure factors in reciprocal space, usually "
                "corresponding to a real space density map"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method don't know file format


class TiltSeriesNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIES,
            toplevel_description="A single tomographic tilt series",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMETADATA,
            toplevel_description="Metadata about a single tomographic tilt series",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method maybe can make more specific ones later


class TiltSeriesGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESGROUPMETADATA,
            toplevel_description=(
                "Metadata about a group of multiple tomographic tilt series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIE,
            toplevel_description=(
                "A file containing a multi-frame movie for a single tomographic tilt "
                "series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIEGROUPMETADATA,
            toplevel_description=(
                "Metadata about multi-frame movies for multiple tomographic tilt series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIEMETADATA,
            toplevel_description=(
                "Metadata about multi-frame movies for a single tomographic tilt series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need specific methods for specific file formats


class TomogramNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAM,
            toplevel_description="A single tomogram",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TomoGram display object, could try a MapModel
    # display object but don't know how Mol* would handle it.


class TomogramMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAMMETADATA,
            toplevel_description="Metadata about a single tomogram",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method could make specific ones for specific file formats


class TomogramGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAMGROUPMETADATA,
            toplevel_description="Metadata about multiple tomograms",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible file formats


class TomoOptimisationSetNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOOPTIMISATIONSET,
            toplevel_description="Data about a RELION tomography optimisation set",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class TomoTrajectoryDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOTRAJECTORYDATA,
            toplevel_description="Data about a RELION tomography trajectory",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class TomoManifoldDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ) -> None:
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOMANIFOLDDATA,
            toplevel_description="Data about a RELION tomography manifold",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )
