#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#


"""The ProjectGraph handles all the management of the pipeline keeping track
of which jobs have been run, their statuses, and what files are input and outputs
to the various jobs"""

import os
import shutil
import stat
import warnings
import logging
from glob import glob
from pathlib import Path
from operator import attrgetter
from typing import Optional, List, Tuple, Union, Set

from gemmi import cif

from pipeliner import star_keys
from pipeliner import star_writer
from pipeliner.starfile_handler import convert_old_relion_pipeline
from pipeliner.job_factory import new_job_of_type, read_job, active_job_from_proc
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.node_factory import create_node
from pipeliner.nodes import Node
from pipeliner.process import Process
from pipeliner.relion_compatibility import (
    get_pipeline_version,
    relion4_to_pipeliner_node_conversions,
    additional_job_conversions,
)
from pipeliner.data_structure import (
    NODES_DIR,
    ABORT_FILE,
    FAIL_FILE,
    SUCCESS_FILE,
    JOBSTATUS_SCHED,
    JOBSTATUS_RUN,
    JOBSTATUS_ABORT,
    JOBSTATUS_FAIL,
    JOBSTATUS_SUCCESS,
    CLEANUP_LOG,
    TRASH_DIR,
    CLEANUP_DIR,
)
from pipeliner.utils import (
    check_for_illegal_symbols,
    date_time_tag,
    decompose_pipeline_filename,
    touch,
    DirectoryBasedLock,
    update_jobinfo_file,
)

logger = logging.getLogger()


class ProjectGraph:
    """The main ProjectGraph object is used for manipulating the pipeline

    Attributes:
        node_list (list): A :class:`~pipeliner.nodes.Node` object for
            every file that is an input or output for a job in the project
        process_list (list): A :class:`~pipeliner.process.Process` object for
            each job in the project
        job_counter (int): The number of the *next* job in the project IE: If there are
            10 jobs in a project job_counter is 11
    """

    def __init__(
        self,
        name: str = "default",
        pipeline_dir: Union[str, "os.PathLike[str]"] = ".",
        read_only: bool = True,
        create_new: bool = False,
    ) -> None:
        """Create a ProjectGraph object.

        Unless ``create_new`` is given, this will look for an existing pipeline STAR
        file in the ``pipeline_dir``. Old pipeline files in RELION 3 format will be
        updated to RELION 4 / CCP-EM Pipeliner format automatically, unless
        ``read_only`` is True in which case an error will be raised.

        Args:
            name (str): The name for the project. It is best to let it default to
                "default".
            pipeline_dir (str or Path): The directory for the project (default "." for
                the current working directory). Warning: currently this affects only the
                location of the pipeline file itself, and its lock directory. Other
                files will be written in (or relative to) the current working directory.
                This might change in future pipeliner versions.
            read_only (bool): Should the pipeline be read-only? If ``True`` (the
                default) methods that change the pipeline will raise exceptions and any
                changes made in other ways will not be written back to disk. If
                ``False``, the pipeline will try to acquire the lock and then open for
                editing. If the lock cannot be acquired, a :class:`RuntimeError` will
                be raised.
            create_new (bool): Are we creating a new pipeline? If ``True``, the value
                given for ``read_only`` is ignored and the pipeline will be editable.

        Raises:
            ValueError: If ``name`` contains a directory part (i.e. if it is a path
                containing a "/" character).
            FileExistsError: If ``create_new`` is True and the pipeline STAR file
                already exists.
            FileNotFoundError: If ``create_new`` is False and the pipeline STAR file
                does not already exist.
            RuntimeError: If the pipeline is already locked by another process and the
                lock is not released after waiting for up to 1 minute.
            RuntimeError: If there is an error reading the pipeline STAR file.
            ValueError: If ``read_only`` is True and the pipeline STAR file is in
                RELION 3 format (because the pipeliner needs to convert RELION 3 files
                to RELION 4 / CCP-EM Pipeliner format before opening them).
        """
        # Initialise the lock attribute first, to ensure it exists when it is accessed
        # by __del__() even if there is an exception in __init__()
        self._lock = None

        if str(Path(name)) != Path(name).name:
            raise ValueError(
                f"Name {name} should not contain a directory part. Use the pipeline_dir"
                " argument instead."
            )

        self._name = name
        self._pipeline_dir = Path(pipeline_dir)
        self._star_file = self.pipeline_dir / f"{name}_pipeline.star"

        self.node_list: List[Node] = []
        self.process_list: List[Process] = []

        # TODO correct this to 0, it's currently at 1 for compatibility reasons
        # For now, think of this as the number of the next job. But in the long run it's
        # probably less confusing to count the number of jobs already in the pipeline.
        self.job_counter = 1

        if create_new:
            read_only = False
            if self.star_file.exists():
                raise FileExistsError(f"Pipeline file {self.star_file} already exists")

        if not read_only:
            # Check that the pipeline file is writable to try and avoid overwriting a
            # read-only pipeline. Note that because the pipeline is actually written
            # by writing to a temporary file and then renaming it, read-only permissions
            # on the pipeline file itself will not prevent it being overwritten. The
            # only way to enforce that at the file system level is to make sure the
            # directory that contains the pipeline is itself not writable (in which
            # case this check will pass but the lock acquisition will fail).
            # Also, this check is only a best-effort attempt to identify early if the
            # pipeline will not be writable. It does not guarantee that the write
            # operation will not fail later, for example if the file permissions change.
            if self.star_file.is_file() and not os.access(self.star_file, os.W_OK):
                raise PermissionError(f"Pipeline file {self.star_file} is not writable")

            lock = DirectoryBasedLock(self.pipeline_dir / ".relion_lock")
            acquired = lock.acquire()
            if acquired:
                self._lock = lock
            else:
                # This happens if the lock is present and does not disappear after
                # waiting for 60 seconds
                raise RuntimeError(
                    f"Trying to open pipeline {self.star_file} for editing but"
                    " could not acquire lock after waiting for 60 seconds"
                )

        if not create_new:
            try:
                self._read()

            except Exception:
                # Ensure the lock is released if the pipeline read fails for any reason
                self.close()
                raise

    @property
    def name(self):
        """The name of the pipeline, usually "default".

        `_pipeline.star` is added to the name to generate the pipeline file name.
        """
        return self._name

    @property
    def pipeline_dir(self):
        """The directory containing the pipeline file."""
        return self._pipeline_dir

    @property
    def star_file(self):
        """The name of the pipeline STAR file, usually "default_pipeline.star"."""
        return self._star_file

    @property
    def read_only(self):
        """If this object should be able to make changes to the pipeline."""
        # The read-only attribute is itself read-only
        # The presence or absence of the lock shows if this pipeline is editable
        return self._lock is None

    def __repr__(self) -> str:
        return f'ProjectGraph("{self.star_file}", read_only={self.read_only})'

    def __enter__(self):
        """Enter a context manager."""
        # All initialisation (including locking) is done in __init__ so there's nothing
        # to do here except return self
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        """Exit context manager.

        If the pipeline is not read-only, write it to the pipeline file and then close
        the pipeline.
        """
        if not self.read_only:
            self._write()
        self.close()

    def __del__(self) -> None:
        """Ensure the lock is released when this object is deleted."""
        if self._lock:
            warnings.warn(
                "ProjectGraph object was not unlocked properly. Ensure you always use"
                " it in a `with` block or call `close()` after use.",
                RuntimeWarning,
            )
            self.close()

    def close(self) -> None:
        """Close the pipeline by releasing the lock.

        After closing, this object will be read-only.

        Note that this does not write the pipeline to disk. If you have made changes
        that should be saved, call :meth:``_write`` first or (preferably) open the
        pipeline in a context manager.
        """
        if self._lock:
            self._lock.release()
            # If the lock has been released, clear the lock attribute to make this
            # pipeline read-only
            self._lock = None

    def validate_pipeline_file(self) -> Tuple[cif.Document, str]:
        """Check if the pipeline file is valid and what version it is

        Returns:
            Tuple: (cif.Document containing the pipeline data, pipeline version string)
        """
        try:
            doc = cif.read_file(str(self.star_file))
            pipeline_version = get_pipeline_version(doc)
            if pipeline_version not in ("ccpem-pipeliner", "relion4"):
                raise ValueError(
                    f"File {self.star_file} appears to be an older RELION STAR file."
                    " Not attempting conversion in read-only mode."
                )
        except Exception as ex:
            if self.read_only:
                raise RuntimeError(
                    f"Error reading pipeline file {self.star_file}."
                ) from ex
            else:
                been_converted = convert_old_relion_pipeline(str(self.star_file))
                if been_converted:
                    doc = cif.read_file(str(self.star_file))
                    pipeline_version = get_pipeline_version(doc)
                    if pipeline_version != "ccpem-pipeliner":
                        raise ValueError(f"Conversion failed for file {self.star_file}")
                else:
                    raise RuntimeError(
                        f"Could not read or convert pipeline file {self.star_file}"
                    ) from ex
        return doc, pipeline_version

    def _read(self) -> None:
        """Read the pipeline

        Raises:
            FileNotFoundError: If the pipeline file is not found
        """
        if not self.star_file.is_file():
            raise FileNotFoundError(f"ERROR: Pipeline file {self.star_file} not found")

        # read the pipeline file and check what version it is
        doc, pipeline_version = self.validate_pipeline_file()

        node_suffixes = star_keys.NODE_SUFFIXES
        proc_suffixes = star_keys.PROCESS_SUFFIXES

        # read jobCounter
        job_count = int(
            doc.find_block(star_keys.GENERAL_BLOCK).find_value(star_keys.JOB_COUNTER)
        )
        self.job_counter = int(job_count)

        # read node and process blocks
        node_block = doc.find_block(star_keys.NODE_BLOCK)
        proc_block = doc.find_block(star_keys.PROCESS_BLOCK)
        if node_block is not None:
            node_table = node_block.find(star_keys.NODE_PREFIX, node_suffixes)

            # write the nodelist
            for node_row in node_table:
                file = cif.as_string(node_row[0])
                node_name_parts = cif.as_string(node_row[1]).split(".")
                tl_type = node_name_parts[0]
                # Update RELION 4 node names on-the-fly
                if (
                    pipeline_version == "relion4"
                    and tl_type in relion4_to_pipeliner_node_conversions
                ):
                    tl_type = relion4_to_pipeliner_node_conversions[tl_type]
                kwds = node_name_parts[2:]
                # skipping validation on nodes because it is slow with large pipelines
                self.node_list.append(
                    create_node(file, nodetype=tl_type, kwds=kwds, do_validation=False)
                )

        # get the processes
        if proc_block is not None:
            process_table = proc_block.find(star_keys.PROCESS_PREFIX, proc_suffixes)
            # write the process list
            for process_row in process_table:
                name = cif.as_string(process_row[0])
                ptype = additional_job_conversions(
                    cif.as_string(process_row[2])
                )  # updated
                status = cif.as_string(process_row[3])  # updated
                alias: Optional[str] = cif.as_string(process_row[1])
                if alias == "None":
                    alias = None
                self.process_list.append(Process(name, ptype, status, alias))

        # read input edges
        in_edge_block = doc.find_block(star_keys.INPUT_EDGE_BLOCK)
        if in_edge_block is not None:
            input_edge_table = in_edge_block.find(
                star_keys.INPUT_EDGE_PREFIX, star_keys.INPUT_EDGE_SUFFIXES
            )
            for input_edge_row in input_edge_table:
                node_name = cif.as_string(input_edge_row[0])
                process_name = cif.as_string(input_edge_row[1])
                node = self.find_node(node_name)
                process = self.find_process(process_name)
                if node is None:
                    logger.warning(
                        "Creating input edge failed - cannot find "
                        f"input node {node_name} for process with "
                        f"name {process_name}"
                    )

                elif process is None:
                    logger.warning(
                        "Creating input edge failed - cannot find parent "
                        f"process with name {process_name} for node "
                        f"{node_name}:"
                    )
                else:
                    node.input_for_processes_list.append(process)
                    process.input_nodes.append(node)

        # set out edges
        out_edge_block = doc.find_block(star_keys.OUTPUT_EDGE_BLOCK)
        if out_edge_block is not None:
            output_edge_table = out_edge_block.find(
                star_keys.OUTPUT_EDGE_PREFIX, star_keys.OUTPUT_EDGE_SUFFIXES
            )
            for output_edge_row in output_edge_table:
                process_name = cif.as_string(output_edge_row[0])
                node_name = cif.as_string(output_edge_row[1])
                process = self.find_process(process_name)
                node = self.find_node(node_name)
                if node is None:
                    logger.warning(
                        "Creating output edge failed - cannot find child "
                        f"node {node_name} from process with name "
                        f"{process_name}"
                    )
                elif process is None:
                    logger.warning(
                        "Creating output edge failed - cannot find parent "
                        f"process with name {process_name} for node "
                        f"{node_name}:"
                    )
                else:
                    process.output_nodes.append(node)
                    node.output_from_process = process

    def _write(self) -> None:
        """Write the pipeline file.

        :attr:`read_only` must be ``False``, otherwise a RuntimeError is raised.

        The pipeline lock will *not* be released after the file is written. Call
        :meth:`close` to release the lock and close the pipeline.
        """
        self._check_writable()
        # Write will always recieve a new-style pipeline from read
        # pipeline_fname = self.get_pipeline_filename()

        node_suffixes = star_keys.NODE_SUFFIXES
        proc_suffixes = star_keys.PROCESS_SUFFIXES

        # Prepare a new Gemmi CIF document to hold the output document structure
        doc = cif.Document()

        # Add general block with job counter
        general_block = doc.add_new_block(star_keys.GENERAL_BLOCK)
        general_block.set_pair(star_keys.JOB_COUNTER, str(self.job_counter))

        # Add processes, while keeping track of input and output edges
        input_edges, output_edges = self.get_pipeline_edges()
        if len(self.process_list) > 0:
            process_block = doc.add_new_block(star_keys.PROCESS_BLOCK)
            process_loop = process_block.init_loop(
                star_keys.PROCESS_PREFIX, proc_suffixes
            )
            for process in self.process_list:
                process_loop.add_row(
                    [
                        process.name,
                        str(process.alias),
                        str(process.type),
                        str(process.status),
                    ]
                )

        # Add nodes
        # TODO: check consistency of input and output edges?
        if len(self.node_list) > 0:
            node_block = doc.add_new_block(star_keys.NODE_BLOCK)
            node_loop = node_block.init_loop(star_keys.NODE_PREFIX, node_suffixes)
            for node in self.node_list:
                node_loop.add_row([node.name, str(node.type)])

        # Add input edges
        if len(input_edges) > 0:
            input_edge_block = doc.add_new_block(star_keys.INPUT_EDGE_BLOCK)
            input_edge_loop = input_edge_block.init_loop(
                star_keys.INPUT_EDGE_PREFIX, star_keys.INPUT_EDGE_SUFFIXES
            )
            for input_edge in input_edges:
                input_edge_loop.add_row(list(input_edge))

        # Add output edges
        if len(output_edges) > 0:
            output_edge_block = doc.add_new_block(star_keys.OUTPUT_EDGE_BLOCK)
            output_edge_loop = output_edge_block.init_loop(
                star_keys.OUTPUT_EDGE_PREFIX, star_keys.OUTPUT_EDGE_SUFFIXES
            )
            for output_edge in output_edges:
                output_edge_loop.add_row(list(output_edge))

        # Write the file
        star_writer.write(doc, self.star_file)

    def _check_writable(self) -> None:
        """Ensure this pipeline is writable. Raises :class:`RuntimeError` if not."""
        if self.read_only:
            raise RuntimeError("Trying to edit a read-only pipeline")
        assert self._lock is not None

    def update_lock_message(self, lock_message: str) -> None:
        """Updates the contents of the lockfile for the pipeline

        This enables the user to see which process has locked the pipeline
        """
        pass
        # TODO: reimplement this
        # fn_lock = ".relion_lock/lock_" + self.name + "_pipeline.star"
        # with open(fn_lock, "w") as lockfile:
        #     lockfile.write(lock_message)

    def find_node(self, name: str) -> Optional[Node]:
        """Retrieve the :class:`~pipeliner.nodes.Node` object for a file

        Args:
            name (str): The name of the file to get the node for

        Returns:
            :class:`~pipeliner.nodes.Node`: The file's `Node` object.
            ``None`` if the file is not found.
        """
        found = None
        for node in self.node_list:
            if node.name == name:
                found = node
        return found

    def find_process(self, name_or_alias: str) -> Optional[Process]:
        """Retrieve the :class:`~pipeliner.process.Process` object for a job
        in the pipeline

        Args:
            name_or_alias (str): The job name or its alias

        Returns:
            :class:`pipeliner.process.Process`: The job's `Process`, or `None`
            if the job was not found.

        Raises:
            RuntimeError: If multiple processes with the same name are found
        """
        found = [
            x
            for x in self.process_list
            if (x.name == name_or_alias or x.alias == name_or_alias)
        ]
        if len(found) > 1:
            raise RuntimeError(
                "ERROR: find_process() found multiple processes with the name/alias "
                f"{name_or_alias}.  This should not happen!"
            )
        try:
            return found[0]
        except IndexError:
            return None

    def add_node(self, node: Node, touch_if_not_exists: bool = False) -> Optional[Node]:
        """Add a :class:`~pipeliner.nodes.Node` to the pipeline

        A node is only added if it doesn't already exist in the
        pipeline, so if a node is used as an input the keywords
        from the process that wrote the node will overrule any
        added in the node's definition from the process that used
        it as input

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to add
            touch_if_not_exists (bool): If the file for the node does not exist
                should it be created?

        Returns:
            :class:`~pipeliner.nodes.Node`: The Node that was added.
            If this node already existed, returns the existing copy

        Raises:
            RuntimeError: If the node name is empty

        """
        self._check_writable()
        if node.name == "":
            raise RuntimeError("Cannot add a node with an empty name")
        fn_node = node.name
        for proc in self.process_list:
            if proc.alias is not None and proc.alias in fn_node:
                fn_node = proc.name + fn_node.replace(proc.alias, "")
                node.name = fn_node
                break

        found = None
        for n in self.node_list:
            if n.name == node.name:
                found = n
                break
        if found is None:
            self.node_list.append(node)
            found = node
        self.touch_temp_node_file(node, touch_if_not_exists)
        return found

    @staticmethod
    def get_node_name(node: Node) -> str:
        """Get the relative path of a node file with its alias if it exists

        This returns the relative path (which is the same as the file name)
        unless the job that created the node has an alias in which case it
        returns the file path with the alias instead of <jobtype>/jobxxx/

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to get the name for

        Returns:
            str: The relative path of the file to node points to
            with the job's alias if applicable
        """
        if node.output_from_process is not None:
            fn_alias = node.output_from_process.alias
        else:
            fn_alias = None

        if fn_alias is not None:
            # make sure alias ends with a slash
            if fn_alias[-1] != "/":
                fn_alias += "/"
            fn_post = decompose_pipeline_filename(node.name)[2]
            fnt = fn_alias + fn_post
        else:
            fnt = node.name
        return fnt

    def touch_temp_node_file(self, node: Node, touch_if_not_exists: bool) -> None:
        """Create a placeholder file for a node that will be created later

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to create the
                file for
            touch_if_not_exists (bool): Should the file be created if it does not
                already exist
        """
        self._check_writable()
        fnt = self.get_node_name(node)
        if os.path.isfile(node.name) or touch_if_not_exists:
            fn_type = node.toplevel_type + "/"
            mynode = NODES_DIR + fn_type + fnt
            mydir = os.path.dirname(mynode)
            os.makedirs(mydir, exist_ok=True)
            touch(mynode)

    def touch_temp_node_files(self, process: Process) -> None:
        """Create placeholder files for all nodes in a
        :class:`~pipeliner.process.Process`

        Args:
            process (:class:`~pipeliner.process.Process`): The Process to create
                the files for
        """
        self._check_writable()
        touch_if_not_exists = False
        if process.status == JOBSTATUS_SCHED:
            touch_if_not_exists = True

        for node in process.output_nodes:
            self.touch_temp_node_file(node, touch_if_not_exists)

    def delete_temp_node_file(self, node: Node) -> None:
        """Remove files associated with a :class:`~pipeliner.nodes.Node`

        Also removes the directory if it is empty

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to remove the
                file for
        """
        self._check_writable()
        fnt = self.get_node_name(node)
        # remove the node
        if os.path.isfile(node.name):
            fn_type = node.toplevel_type + "/"
            mynode = NODES_DIR + fn_type + fnt
            if os.path.isfile(mynode):
                os.remove(mynode)
            # also remove the directory if empty
            mydir = os.path.dirname(mynode)
            if os.path.isdir(mydir):
                if len(os.listdir(mydir)) == 0:
                    shutil.rmtree(mydir)

    def delete_temp_node_files(self, process: Process) -> None:
        """Delete all the files for the nodes in a specific
        :class:`~pipeliner.process.Process`

        Args:
            process (:class:`~pipeliner.process.Process`): The Process to create
                the files for
        """
        self._check_writable()
        for node in process.output_nodes:
            self.delete_temp_node_file(node)

    def add_new_input_edge(self, node: Node, process: Process) -> None:
        """Add a :class:`~pipeliner.nodes.Node` to a
        :class:`~pipeliner.process.Process` as in input

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to add
            process (:class:`~pipeliner.process.Process`): The Process to add
                the Node to
        """
        self._check_writable()
        # 1. Check whether Node with that name already exists in the Node list
        newnode = self.add_node(node)  # if new it's added to the list

        # 2. Set the input_for_process in the inputForProcessList of this Node but only
        # if it doesn't yet exist
        found = None
        if newnode:
            for proc in newnode.input_for_processes_list:
                if proc == process:
                    found = proc
                    break
        if found is None and newnode:
            newnode.input_for_processes_list.append(process)
            process.input_nodes.append(newnode)
        for proc in self.process_list:
            if newnode and newnode is not node:
                # Previously unobserved node. Check whether it came from an old process.
                nodename = newnode.name
                if proc.name in nodename:
                    proc.output_nodes.append(newnode)
                    newnode.output_from_process = proc
                    break

    def add_new_output_edge(
        self, process: Process, node: Node, mini: bool = False
    ) -> None:
        """Add a :class:`~pipeliner.nodes.Node` to a
        :class:`~pipeliner.process.Process` as in output

        Args:
            node (:class:`~pipeliner.nodes.Node`): The node to add
            process (:class:`~pipeliner.process.Process`): The Process to add
                the Node to
            mini (bool): Is the pipeline being operated on a mini pipeline
                written inside a job directory
        """
        self._check_writable()

        # 1. Check whether Node with that name already exists in the node_list
        # Touch .Nodes entries even if they don't exist for scheduled jobs
        touch_if_not_exist = process.status == JOBSTATUS_SCHED

        # 2. Set the output_from_process of this Node
        node.output_from_process = process
        newnode = self.add_node(node, touch_if_not_exist)

        # 3. Only for new Nodes, add this Node to the outputNodeList of myProcess
        if not mini:
            if newnode is node:
                process.output_nodes.append(newnode)

    def add_process(self, process: Process, do_overwrite: bool) -> Process:
        """Add a :class:`~pipeliner.process.Process` to the pipeline

        Args:
            process (:class:`~pipeliner.process.Process`): The `Process` to add
            do_overwrite (bool): If the process already exists should it be
                overwritten?

        Returns:
            (:class:`~pipeliner.process.Process`): The `Process` that was added
            or the existing `Process` if it already existed

        Raises:
            RuntimeError: If the `Process` already exists and overwrite is ``False``
        """
        self._check_writable()

        found = None
        for proc in self.process_list:
            if proc.name == process.name:
                found = proc
                proc.status = process.status
                proc.alias = process.alias
                break
        if found is None:
            found = process
            self.process_list.append(process)
            self.job_counter += 1
        elif not do_overwrite:
            raise RuntimeError(
                "project_graph.add_process(): trying to add existing Process "
                "to the pipeline while overwriting is not allowed."
            )
        return found

    def add_job(
        self,
        job: PipelinerJob,
        as_status: str,
        do_overwrite: bool,
        alias: Optional[str] = None,
    ) -> Process:
        """Add a job to the pipeline

        Adds the :class:`~pipeliner.process.Process` for the job, a
        :class:`~pipeliner.nodes.Node` for each of its
        input and output files, and writes a mini-pipeline containing
        just that job

        Args:
            job (:class:`~pipeliner.pipeliner_job.PipelinerJob`): The
                job to add.
            as_status (str): The status of the job in the pipeline
            do_overwrite (bool): If the job already exists, should it be overwritten?
            alias (str): Alias to assign to job

        Returns:
            :class:`~pipeliner.process.Process`: The `Process` for the new job
        """
        self._check_writable()

        # Create process and check alias first, so we don't create an empty
        # mini-pipeline if there's a problem with the alias
        process = Process(job.output_dir, job.PROCESS_NAME, as_status)
        if alias:
            alias = self.alias_checks(process, alias)
            process.alias = alias

        # Also write a mini-pipeline in the output directory
        # First check if it already exists - if so, and if do_overwrite is True, remove
        # it so we can create a new version.
        minipipe_file = os.path.join(job.output_dir, "job_pipeline.star")
        if do_overwrite and os.path.isfile(minipipe_file):
            os.remove(minipipe_file)
        with ProjectGraph(
            name="job", pipeline_dir=job.output_dir, create_new=True
        ) as mini_pipeline:
            if alias:
                # make new symlink - relative to the jobdir
                os.makedirs(".Nodes", exist_ok=True)
                os.symlink(os.path.relpath(process.name[:-1], ".Nodes"), alias[:-1])

            newprocess = self.add_process(process, do_overwrite)

            mini_pipeline.add_process(process, True)
            if newprocess is not None:
                process = newprocess
            # Add all input nodes
            for node in job.input_nodes:
                self.add_new_input_edge(node, process)
                mini_pipeline.add_new_input_edge(node, mini_pipeline.process_list[0])

            # Add all output nodes - if job completed successfully
            for node in job.output_nodes:
                if as_status not in [JOBSTATUS_FAIL, JOBSTATUS_ABORT]:
                    self.add_new_output_edge(process, node)
                    mini_pipeline.add_new_output_edge(
                        mini_pipeline.process_list[0], node, mini=True
                    )
        self.touch_temp_node_files(process)
        return process

    def find_immediate_child_processes(self, process: Process) -> List[Process]:
        """Find just the immediate child processes of a process

        Args:
            process (:class:`~pipeliner.process.Process`): The process to
                find children for

        Returns:
            list: The `Process` object for each job connected to the input `Process`
        """
        children = []
        parent_proc = process.name
        for proc in self.process_list:
            innodes = [os.path.dirname(x.name) + "/" for x in proc.input_nodes]
            if parent_proc in innodes:
                children.append(proc)
        return children

    def remake_node_directory(self) -> None:
        """Erase and rewrite RELION's .Nodes directory"""
        self._check_writable()

        # clear existing directory
        if os.path.isdir(NODES_DIR):
            shutil.rmtree(NODES_DIR)
        os.makedirs(NODES_DIR)

        # remake nodes
        for node in self.node_list:
            myproc = node.output_from_process
            if myproc not in self.process_list:
                touch_if_not_exists = False
            else:
                touch_if_not_exists = bool(myproc.status == JOBSTATUS_SCHED)

            self.touch_temp_node_file(node, touch_if_not_exists)

        # set permissions
        mode_777 = stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO
        for root, dirs, files in os.walk(NODES_DIR):
            for f in files:
                path = os.path.join(root, f)
                os.chmod(path, mode_777)

    @staticmethod
    def check_process_status(
        proc: Process,
        finished: List[Process],
        failed: List[Process],
        aborted: List[Process],
    ) -> Tuple[List[Process], List[Process], List[Process]]:
        """Check if a job has a completion status file and assign to the right list

        Args:
            proc (Process): The process to check
            finished (List[Process]): Process that are finished successfully
            failed (List[Process]): Processes that have failed
            aborted (List[Process]): Processes that were killed by the user

        Returns:
            Tuple[List[Process], List[Process], List[Process]]: The updated input lists
        """

        if proc.status == JOBSTATUS_RUN:
            if os.path.isfile(os.path.join(proc.name, SUCCESS_FILE)):
                finished.append(proc)
            if os.path.isfile(os.path.join(proc.name, FAIL_FILE)):
                failed.append(proc)
            if os.path.isfile(os.path.join(proc.name, ABORT_FILE)):
                aborted.append(proc)
        return finished, failed, aborted

    def check_process_completion(self) -> None:
        """Check to see if any processes have finished running, update their status."""
        self._check_writable()
        finished: List[Process] = []
        failed: List[Process] = []
        aborted: List[Process] = []
        for proc in self.process_list:
            finished, failed, aborted = self.check_process_status(
                proc, finished, failed, aborted
            )

        # gather the changes
        lock_message = ""
        changes = len(finished) + len(failed) + len(aborted)
        if changes:
            outcomes = {
                "successfully finished:\n": finished,
                "failed with an error:\n": failed,
                "been aborted:\n": aborted,
            }
            msg = "check_process_completion: the following jobs have "
            for outcome in outcomes:
                if len(outcomes[outcome]) > 0:
                    lock_message += msg + outcome
                    for proc in outcomes[outcome]:
                        lock_message += proc.name + "\n"

        # only write pipeline if some processes' statuses have changed
        if not changes:
            return

        self.update_lock_message(lock_message)

        # set the statuses of the newly finished processes
        for proc in finished:
            proc.status = JOBSTATUS_SUCCESS

            # Get output nodes from the job pipeline file, in case any nodes were added
            # in create_post_run_output_nodes
            try:
                with ProjectGraph(name="job", pipeline_dir=proc.name) as job_pipeline:
                    job_proc = job_pipeline.find_process(proc.name)
                    for node in job_proc.output_nodes:
                        self.add_new_output_edge(proc, node)
            except Exception:
                logger.exception(f"Error reading job pipeline for {proc.name}")

            # make any output nodes
            if len(proc.output_nodes) == 0:
                logger.error(
                    "ERROR: The output nodes list for job" f" {proc.name} is empty"
                )

            for node in proc.output_nodes:
                if os.path.isfile(node.name):
                    self.touch_temp_node_file(node, False)
                else:
                    logger.warning(
                        f"Output node {node.name} does not exist"
                        f" while job {proc.name} should have finished "
                        "You can manually mark this job as failed"
                        " to suppress this message."
                    )
            update_jobinfo_file(proc.name, action=f"Job ended; {JOBSTATUS_SUCCESS}")

        for proc in failed:
            proc.status = JOBSTATUS_FAIL
            rm_nodes = proc.remove_nonexistent_nodes()
            self.node_list = [x for x in self.node_list if x.name not in rm_nodes]
            update_jobinfo_file(proc.name, action=f"Job ended; {JOBSTATUS_FAIL}")

        for proc in aborted:
            proc.status = JOBSTATUS_ABORT
            rm_nodes = proc.remove_nonexistent_nodes()
            self.node_list = [x for x in self.node_list if x.name not in rm_nodes]
            update_jobinfo_file(proc.name, action=f"Job ended; {JOBSTATUS_ABORT}")

        # write the changed pipeline to disk
        self._write()

    @staticmethod
    def update_job_pipeline_file(process: Process):
        job_pipeline_file = Path(process.name) / "job_pipeline.star"
        with ProjectGraph(
            name="job",
            pipeline_dir=process.name,
            read_only=False,
            create_new=not job_pipeline_file.is_file(),
        ) as job_pipeline:
            job_pipeline.add_process(process, do_overwrite=True)

    def update_status(self, the_proc: Process, new_status: str) -> None:
        """Change the status of a job

        The job can be marked as "Succeeded", "Failed", "Aborted", "Running" or
        "Scheduled"

        Args:
            the_proc (:class:`~pipeliner.process.Process`): The `Process` to
                update the status for
            new_status (str): The new status for the job

        Returns:
            bool: Was the status changed?

        Raises:
            ValueError: If the new_status is not in the approved list
            ValueError: If a job with any other status than 'Running' is marked
                'Aborted'
            ValueError: If a job's updated status is the same as it's current status
        """
        self._check_writable()
        statuses = {
            JOBSTATUS_RUN: None,
            JOBSTATUS_SCHED: None,
            JOBSTATUS_ABORT: ABORT_FILE,
            JOBSTATUS_FAIL: FAIL_FILE,
            JOBSTATUS_SUCCESS: SUCCESS_FILE,
        }
        stats = list(statuses)
        if new_status not in stats:
            raise ValueError(f"ERROR: New status must be one of {stats}")

        # read in the existing pipeline - get the process
        _proc = self.find_process(the_proc.name)
        if _proc:
            the_proc = _proc

        lock_message = f"Updating status of {the_proc.name} to {new_status}"
        self.update_lock_message(lock_message)

        # error if trying to mark a running job as aborted (a running job should be
        # aborted using the ``job_manager.abort_job`` function)
        if new_status == JOBSTATUS_ABORT and the_proc.status == JOBSTATUS_RUN:
            # self.remove_lock()
            raise ValueError(
                "ERROR: A running job cannot be marked as aborted. Try to abort the job"
                " instead, or if the job is definitely not actually running, set the"
                " status as Succeeded or Failed first."
            )

        # error if trying to change to current status
        if new_status == the_proc.status:
            # self.remove_lock()
            raise ValueError(f"Status of {the_proc.name} is already {new_status}")

        # remove any existing control files
        for control_file in [
            FAIL_FILE,
            ABORT_FILE,
            SUCCESS_FILE,
        ]:
            control_file_path = os.path.join(the_proc.name, control_file)
            if os.path.isfile(control_file_path):
                os.remove(control_file_path)

        the_proc.status = new_status
        _status = statuses[new_status]
        if _status is not None:
            touch(os.path.join(the_proc.name, _status))

        if new_status == JOBSTATUS_SUCCESS:
            # create a dummy job to get any intermediate outputnodes
            d_job = active_job_from_proc(the_proc)
            new_output_nodes = d_job.get_current_output_nodes()

            # Add any new output nodes
            for node in new_output_nodes:
                node.output_from_process = the_proc
                self.add_node(node)
                self.add_new_output_edge(the_proc, node)
                the_proc.output_nodes.append(node)

        # remove any of the output nodes if the files don't exist
        # removes nonexistent terminal nodes if the process had been scheduled
        for outnode in the_proc.output_nodes:
            if not os.path.isfile(outnode.name):
                the_proc.output_nodes.remove(outnode)

        self._write()

        # update the job's job_pipeline.star
        self.update_job_pipeline_file(the_proc)

        # update the jobinfo file
        update_jobinfo_file(
            the_proc.name, action=f"Status manually set to {new_status}"
        )

    def get_pipeline_edges(self) -> Tuple[List[Tuple[str, str]], List[Tuple[str, str]]]:
        """Find the edges that track connections between processes

        Returns:
            tuple: ([input edges], [output edges])
            input edges is a list of tuples (input file, process name)
            output edges is a list of tuples (process name, output file)
            Note that the order in the tuples is different in the input and output
            edges! This is done to match the order of the pipeline STAR file columns.
        """
        input_edges = []
        output_edges = []
        for process in self.process_list:
            for node in process.input_nodes:
                if (node.name, process.name) not in input_edges:
                    input_edges.append((node.name, process.name))
            for node in process.output_nodes:
                if (process.name, node.name) not in output_edges:
                    output_edges.append((process.name, node.name))

        return input_edges, output_edges

    def delete_job(self, process: Process) -> None:
        """Remove a job from the pipeline

        Args:
            process (:class:`~pipeliner.process.Process`:): The job to remove

        """
        self._check_writable()
        # get the processes and nodes to delete
        job_proc = self.find_process(process.name)
        if not job_proc:
            raise ValueError(f"Process {process.name} not found in pipeline")

        # update the lock message
        lock_message = f"Deleting process {process.name} and child jobs"
        self.update_lock_message(lock_message)

        # make the list of jobs to delete
        del_processes = [job_proc]
        del_nodes = job_proc.output_nodes

        for proc in self.process_list:
            del_this_proc = False
            for node in proc.input_nodes:
                if node in del_nodes and proc not in del_processes:
                    del_processes.append(proc)
                    del_this_proc = True
            if del_this_proc:
                for onode in proc.output_nodes:
                    if onode not in del_nodes:
                        del_nodes.append(onode)

        # move the processes to the trash
        for delproc in del_processes:
            logger.info("Deleting: " + delproc.name)
            trashdir = Path(TRASH_DIR) / delproc.name
            trashdir.parent.mkdir(exist_ok=True, parents=True)
            Path(delproc.name).rename(trashdir)

            # remove any aliases
            alias = delproc.alias
            if alias is not None:
                os.unlink(alias[:-1])

            # remove the process from the process list
            self.process_list.remove(delproc)

        # nodes are already deleted with the processes
        for node in del_nodes:
            self.node_list.remove(node)

        # remove the nodes directory(s)
        self.remake_node_directory()

        # update the pipeline file
        self._write()

    def alias_checks(self, process, new_alias) -> str:
        """A set of checks for alias to make sure they fit the requirements

        Args:
            process (Process): The Process object that is getting the new alias
            new_alias (str): The alias that will be applied to the process
        """
        # replace any spaces
        if " " in new_alias:
            new_alias = new_alias.replace(" ", "_")
            logger.warning(
                f"No spaces are permitted in aliases. New alias changed to {new_alias}"
            )

            # check the alias name rules
        if new_alias == "None":
            raise ValueError("ERROR: New alias cannot be 'None'")

        if len(new_alias) < 2:
            raise ValueError("ERROR: Alias cannot be fewer than two characters")

        if new_alias[:3] == "job":
            raise ValueError("ERROR: Alias cannot begin with 'job'")
        error_message = check_for_illegal_symbols(new_alias, "alias")
        if error_message:
            raise ValueError(error_message)

        # add the trailing slash
        final_alias = (
            decompose_pipeline_filename(process.name)[0] + "/" + new_alias + "/"
        )
        for proc in self.process_list:
            if proc.alias == final_alias and proc.name != process.name:
                raise ValueError("Alias is not unique, please provide another one")
        return final_alias

    def set_job_alias(
        self, process: Optional[Process], new_alias: Optional[str]
    ) -> None:
        """Set a job's alias

        Sets the alias in the pipeline and creates the alias symlink that points to the
        job directory.

        Args:
            process (:class:`~pipeliner.process.Process`): The `Process` to
                make an alias for
            new_alias  (str): The new alias for the job

        Raises:
            ValueError: If `process` is `None`
            ValueError: If the :class:`~pipeliner.process.Process` is not found
            ValueError: If the new alias is `None`, which is not allowed
            ValueError: If the new alias is shorter than 2 characters
            ValueError: If the new alias begins with "job", which would cause problems
            ValueError: If the new alias is not unique

        """
        self._check_writable()
        # make sure job is valid
        if process is None:
            raise ValueError("ERROR: no process given to set_job_alias()")
        fn_pre, fn_jobnr = decompose_pipeline_filename(process.name)[0:2]
        if fn_pre == "" or fn_jobnr == "":
            raise ValueError("ERROR: invalid pipeline process name: " + process.name)

        if new_alias is not None and len(new_alias) == 0:
            new_alias = None

        if new_alias is not None:
            new_alias = self.alias_checks(process, new_alias)

        # read in the pipeline
        # self.read(do_lock=True) # TODO: fix this
        # find the process again because its ID has changed
        new_process = self.find_process(process.name)
        if not new_process:
            raise ValueError(
                f"Process {process.name} cannot be found again after ID change"
            )

        # update the lock message
        lock_message = f"Updating alias for {new_process.name} to {new_alias}"
        self.update_lock_message(lock_message)

        # remove the existing .Nodes entry
        self.delete_temp_node_files(new_process)

        # unlink the alias
        if new_process.alias is not None:
            if os.path.isdir(new_process.alias):
                os.remove(new_process.alias[:-1])

        if new_alias is None:
            new_process.alias = None
        else:
            # set alias in pipeline
            new_process.alias = new_alias
            # make new symlink - relative to the jobdir
            os.makedirs(NODES_DIR, exist_ok=True)
            os.symlink(os.path.relpath(new_process.name[:-1], ".Nodes"), new_alias[:-1])

        # remake new nodes entry
        self.touch_temp_node_files(new_process)

        # rewrite the pipeline
        self._write()

        # update the job's job_pipeline.star
        self.update_job_pipeline_file(new_process)

    def undelete_job(self, del_job: str) -> None:
        """Get job out of the trash and restore it to the pipeline

        Also restores any alias the job may have had as long as it does not
        conflict wit the current aliases

        Args:
            del_job (str): The name of the deleted job
        """
        self._check_writable()
        # reread the pipeline
        trashed_procs = [del_job]
        restore_procs = []

        # get current aliases to make sure none are duplicated
        curr_proclist_aliases = [x.alias for x in self.process_list]

        for del_proc in trashed_procs:
            del_proc_dir = Path("Trash") / del_proc

            # find other process that were inputs and may have also been deleted
            if del_proc not in self.process_list and del_proc_dir.is_dir():
                # Use read_only=False in case job pipeline file needs conversion
                with ProjectGraph(
                    name="job", pipeline_dir=del_proc_dir, read_only=False
                ) as del_proc_pipe:
                    the_process = del_proc_pipe.process_list[0]
                for node in the_process.input_nodes:
                    fn_pre, fn_jobnr = decompose_pipeline_filename(node.name)[0:2]
                    inproc = fn_pre + f"/job{fn_jobnr:03d}/"
                    if inproc not in trashed_procs:
                        trashed_procs.append(inproc)

                    # restore the alias if it exists - check for duplication
                    the_alias = the_process.alias
                    if the_alias not in curr_proclist_aliases:
                        the_process.alias = the_alias
                    elif the_alias is not None:
                        logger.warning(
                            f"Cannot restore alias {the_alias} for "
                            f"{the_process.name} because this would create a duplicate "
                            "alias"
                        )
                        the_process.alias = None
                restore_procs.append(the_process)

        logger.info("The following processes will be restored:")
        lock_message = "The following processes will be restored:\n"
        # reverse so they write in the right order
        restore_procs.reverse()
        for rproc in restore_procs:
            # put the process back in the pipeline
            logger.info(rproc.name, " alias: ", rproc.alias)
            lock_message += rproc.name + " alias: " + str(rproc.alias) + "\n"
            self.process_list.append(rproc)
            # put the nodes back in the pipeline
            for onode in rproc.output_nodes:
                if onode.name not in self.node_list:
                    self.node_list.append(onode)
            # restore the files
            trashed_proc = Path(TRASH_DIR) / rproc.name
            restored_proc = trashed_proc.relative_to(TRASH_DIR)
            restored_proc.parent.mkdir(parents=True, exist_ok=True)
            trashed_proc.rename(restored_proc)
            # restore the alias if it exists
            if rproc.alias is not None:
                os.symlink(rproc.name[:-1], rproc.alias[:-1])

        self.update_lock_message(lock_message)
        self._write()
        self.remake_node_directory()

    @staticmethod
    def write_cleanuplog(message: str) -> None:
        """Write the message to the cleanup log file

        Args:
            message (str): The message to write to the logfile
        """

        with open(CLEANUP_LOG, "a") as clup:
            clup.write(f"{date_time_tag()}: {message}\n")

    def clean_up_job(self, process: Process, do_harsh: bool) -> bool:
        """Cleans up a job by deleting intermediate files

        Gets a list of files to delete from the specific job's cleanup function.
        First checks that none of the files that are slated for deletion are
        on the Node list or are of a few specific types that RELION needs.  Then
        moves all the intermediate files to the trash

        There are two tiers of clean up 'harsh' and normal.  Each job defines what
        files are cleaned up by each cleanup type

        Args:
            process (:class:`~pipeliner.process.Process`): The `Process` of
                the job to clean up
            do_harsh (bool): Should harsh cleaning be performed?

        Returns:
            bool: ``True`` if cleaning was performed
            ``False`` if the specified job had no clean up method or it was protected
            from harsh cleaning
        """
        # Because this method doesn't actually edit the pipeline itself,
        # we don't need to call self._check_writable()

        if process.status != "Succeeded":
            self.write_cleanuplog(
                f"Cannot clean up {process.name}; only finished jobs can be cleaned up."
                f" Job status is {process.status}"
            )
            return False

        # start the log entry
        cluptype = "Harsh cleanup" if do_harsh else "Cleanup"
        self.write_cleanuplog(f"{cluptype} job {process.name}")

        # first make sure jobname is in pipeliner format
        jobfile = os.path.join(process.name, "job.star")
        reread_job = read_job(jobfile)
        the_job = new_job_of_type(reread_job.PROCESS_NAME)
        the_job.output_dir = process.name
        the_job.output_nodes = process.output_nodes
        protected = os.path.isfile(os.path.join(process.name, "NO_HARSH_CLEAN"))
        if do_harsh and protected:
            self.write_cleanuplog(
                f"WARNING: {process.name} was not harsh cleaned because it "
                "has been protected.\nTo remove the protection delete the file "
                f"{process.name}/NO_HARSH_CLEAN"
            )
            update_jobinfo_file(
                process.name,
                action=f"{cluptype} requested; not performed; job protected",
            )
            return False
        try:
            del_files, del_dirs = the_job.prepare_clean_up_lists(do_harsh)
        except AttributeError:
            self.write_cleanuplog(
                f"WARNING: Jobtype {process.type} has no cleanup method"
            )
            update_jobinfo_file(
                process.name,
                action=(
                    f"{cluptype} requested; not performed; job has no cleanup method"
                ),
            )
            return False
        if len(del_files) + len(del_dirs) == 0:
            self.write_cleanuplog(" - No files or dirs to clean up")
            update_jobinfo_file(
                process.name, action=f"{cluptype}; 0 files, 0 dirs removed"
            )
            return False

        # Check none of the files slated for deletion are input or output nodes
        keeper_dirs = []
        keeper_nodes = [x.name for x in self.node_list]
        # make sure none of the files removed are output nodes
        for f in del_files:
            if f in keeper_nodes:
                del_files.remove(f)
                if os.path.dirname(f) not in keeper_dirs:
                    keeper_dirs.append(os.path.dirname(f))

        for a_dir in del_dirs:
            if a_dir in keeper_dirs:
                del_dirs.remove(a_dir)

        # Then move everything to the trash:
        fcount = 0

        for ddir in del_dirs:
            fcount += len(glob(f"{ddir}/*"))
            trashed = Path(CLEANUP_DIR) / ddir
            trashed.mkdir(parents=True, exist_ok=True)
            Path(ddir).rename(trashed)

        for dfile in del_files:
            fpath = Path(dfile)
            trash_dir = Path(CLEANUP_DIR) / fpath.parent
            trash_dir.mkdir(exist_ok=True, parents=True)
            Path(dfile).rename(trash_dir / fpath.name)
        fcount += len(del_files)

        final_stat = (
            f"{cluptype}; {fcount} file(s), {len(del_dirs)} directory(s) removed"
        )

        self.write_cleanuplog(final_stat)
        update_jobinfo_file(process.name, action=final_stat)
        return True

    def restore_cleaned_up_files(self, proc: Process):
        self.write_cleanuplog(f"Restoring cleaned up files from {proc.name}")
        if not Path(proc.name).is_dir():
            self.write_cleanuplog(
                f"Job {proc.name} does not exist, it may have been deleted"
            )
            return
        trashed_proc_dir = Path(CLEANUP_DIR) / proc.name
        dirdata = list(os.walk(trashed_proc_dir, topdown=False))
        if all([len(filenames) == 0 for _, _, filenames in dirdata]):
            self.write_cleanuplog("No files found to restore")
            return
        restore_count = 0
        for dirpath, _, filenames in dirdata:
            dir_path = Path(dirpath)
            # make the parent dir again, as there may be deleted subdirs
            dir_path.relative_to(CLEANUP_DIR).mkdir(parents=True, exist_ok=True)
            for filename in filenames:
                file = dir_path / filename
                file.rename(file.relative_to(CLEANUP_DIR))
                restore_count += 1
            if not any(dir_path.iterdir()):
                dir_path.rmdir()
        restore_result = f"Restored {restore_count} cleaned up items"
        self.write_cleanuplog(restore_result)
        update_jobinfo_file(restore_result)

    def cleanup_all_jobs(self, do_harsh: bool) -> int:
        """Clean up all jobs in the project

        Args:
            do_harsh (bool): Should harsh cleaning be performed?

        Returns:
            int: The number of jobs that were successfully cleaned
        """
        # Because this method doesn't actually edit the pipeline itself,
        # we don't need to call self._check_writable()
        finished_jobs = list()
        for job in self.process_list:
            if job.status == "Succeeded":
                finished_jobs.append(job)

        cleancount = 0
        for job in finished_jobs:
            cleaned = self.clean_up_job(job, do_harsh)
            if cleaned:
                cleancount += 1

        return cleancount

    def get_downstream_network(
        self, process: Process
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Gets data for drawing a network downstream from process

        Args:
            process (:class:`~pipeliner.process.Process`): The process to trace

        Returns:
            list: Contains a tuple for each edge:
            (:class:`~pipeliner.nodes.Node`,
            parent :class:`~pipeliner.process.Process`,
            child :class:`~pipeliner.process.Process`). Each edge describes one
            file in the network.

        """
        new_process = self.find_process(process.name)
        if not new_process:
            raise ValueError(f"Process {process.name} not found after ID change")
        associated_procs = [new_process]
        logger.info("Drawing downstream process graph for " + new_process.name)
        # find the children
        edges_list: list = []
        for proc in self.process_list:
            for node in proc.input_nodes:
                if (
                    node.output_from_process in associated_procs
                    and proc not in associated_procs
                ):
                    associated_procs.append(proc)

                    ds_node = (node, node.output_from_process, proc)
                    if ds_node not in edges_list:
                        edges_list.append(ds_node)

        # now do it again backwards to find the parents
        for proc in reversed(self.process_list):
            for node in proc.input_nodes:
                if node.output_from_process in associated_procs:
                    # get the node type from its source and assign color
                    actual = node.output_from_process.output_nodes.index(node)
                    color_node = node.output_from_process.output_nodes[actual]

                    ds_edge = (node, color_node.output_from_process, proc)

                    if ds_edge not in edges_list:
                        edges_list.append(ds_edge)

                    if proc not in associated_procs:
                        associated_procs.append(proc)
        return edges_list

    def get_upstream_network(
        self, process: Process
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Gets data for drawing a network upstream from process

        Args:
            process (:class:`~pipeliner.process.Process`): The process to trace

        Returns:
            list: Contains a tuple for each edge:
            (:class:`~pipeliner.nodes.Node`,
            parent :class:`~pipeliner.process.Process`,
            child :class:`~pipeliner.process.Process`). Each edge describes one
            file in the network.

        """
        new_process = self.find_process(process.name)
        associated_procs = [new_process]
        # find the children
        edges_list: list = []
        # get all the nodes that contributed to the job
        con_node_list: List[Node] = []
        cn_count2, cn_count1 = 1, 0
        while cn_count2 > cn_count1:
            cn_count1 = len(con_node_list)
            for proc in associated_procs:
                if proc:
                    for node in proc.input_nodes:
                        if node not in con_node_list:
                            con_node_list.append(node)
                        if (
                            node.output_from_process not in associated_procs
                            and node.output_from_process is not None
                        ):
                            associated_procs.append(node.output_from_process)
            cn_count2 = len(con_node_list)

        # get the processes that produced them
        for node in con_node_list:
            for into_proc in node.input_for_processes_list:
                if into_proc in associated_procs:
                    edges_list.append((node, node.output_from_process, into_proc))

        return edges_list

    def get_project_procs_list(
        self, terminal_job: str, reverse: bool = True
    ) -> List[Process]:
        """make a list of all the process objects for a job and its parents

        Args:
            terminal_job (str): The name of the final job in the workflow
            reverse (bool): sort in reverse order (High to low)

        Returns:
            list: The :class:`~pipeliner.process.Process` objects for the
            terminal job and its parents, in reverse order by job number

        Raises:
            ValueError: If the terminal job was not found
        """
        term_proc = self.find_process(terminal_job)
        if term_proc is None:
            raise ValueError(f"Process for {terminal_job} not found")
        upstream = self.get_upstream_network(term_proc)
        procs: Set[Process] = {term_proc}
        for j in upstream:
            if j[1] is not None:
                procs.add(j[1])
        proclist = sorted(procs, key=attrgetter("job_number"), reverse=reverse)
        return proclist

    def get_whole_project_network(
        self,
    ) -> List[Tuple[Optional[Node], Optional[Process], Process]]:
        """Get the edges and nodes for the entire project

        Returns:
            list: Edges [nod type, parent_job, child_job, extra info]
            set: The names of all nodes
        """
        # find the children
        edges_list = []
        # get all the nodes
        con_node_list: List[Node] = []
        cn_count2, cn_count1 = 1, 0
        while cn_count2 > cn_count1:
            cn_count1 = len(con_node_list)
            for proc in reversed(self.process_list):
                for node in proc.input_nodes:
                    if node not in con_node_list:
                        con_node_list.append(node)
            cn_count2 = len(con_node_list)

        # get the processes that produced them
        done_procs = []
        for node in con_node_list:
            for into_proc in node.input_for_processes_list:
                edges_list.append((node, node.output_from_process, into_proc))
                done_procs += [node.output_from_process, into_proc]

        # catch any missed processes where nodes don't connect to other processes
        for proc in self.process_list:
            if proc not in done_procs:
                edges_list.append((None, None, proc))

        return edges_list
