#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from copy import deepcopy
from typing import List, Dict, Union, Any, Tuple, Optional
from gemmi import cif
from uuid import uuid4
from itertools import chain
from pipeliner.deposition_tools.onedep_deposition_objects import (
    ONEDEP_OBJECTS,
    OneDepData,
)
from pipeliner.utils import clean_jobname, is_uuid4
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc

"""
Go through all the jobs make a list of each type of dep object
update all the job references to the uuids for those jobs
Update job references with UUIDs from that job's depdata objects
Substitute all the uuids for all the internal ids
Make a cif doc for the final data and write each block to
"""


class DepositionData(object):
    """An object that holds data to be depositied via the onedep system

    Attributes:
        field_name (str): The name of the field in the ped/emdb these should be drawn
            from
            https://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Groups/index.html
        data_items (object): A dataclass from
            `pipeliner.deposition_tools.onedep_deposition_objects` containing the data
        dc_type (DepositionData): The subclass of the dataclass used for data_items
        cif_type (str): ("loop" or "pairs") How the data will be written into the final
            cif file.
        merge_strategy (str): ("multiple", "combine", or "overwrite") How multiple
            display objects will be combined, Multiple: means each DepositionData object
            will be given its own entry in a loop in the cif file.  Overwrite: Only the
            first/last (depending on `self.reverse`) will be used. Combine: the objects
            will be combined, with each field being overwritten by subsequent data in a
            given field if it is not None.
        reverse (bool): What order should the objects be considered in when combined
            False: The latest items take precedence, True: The earliest items take
            precedence
    """

    def __init__(
        self,
        field_name: str,
        data_items: object,
        merge_strat: str,  # "multiple", "combine", "overwrite"
        reverse: bool = False,
    ):
        """Instantiate a DepositionData objects

        Args:
        field_name (str): The name of the field in the ped/emdb these shoul dbe drawn
            from
            https://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Groups/index.html
        data_items (object): A dataclass from
            `pipeliner.deposition_tools.onedep_deposition_objects` containing the data
        merge_strat (str): ("multi", "combine", or "overwrite") How multiple
            display objects will be combined, Multiple: means each DepositionData object
            will be given its own entry in a loop in the cif file.  Overwrite: Only the
            first/last (depending on `self.reverse`) will be used. Combine: the objects
            will be combined, with each field being overwritten by subsequent data in a
            given field if it is not None.
        reverse (bool): What order should the objects be considered in when combined
            False: The latest items take precedence, True: The earliest items take
            precedence

        Raises:
            ValueError: If the merge strategy is not in ("multi", "combine",
                "overwrite")
        """
        if merge_strat not in ("multi", "combine", "overwrite"):
            raise ValueError(f"Invalid merging strategy: {merge_strat}")
        self.field_name = field_name
        self.data_items = data_items
        self.dc_type = type(data_items)
        self.cif_type = "pairs" if merge_strat in ["combine", "overwrite"] else "loop"
        self.merge_strategy = merge_strat
        self.reverse = reverse

    @staticmethod
    def cifformat(val: Union[int, str, float, bool, None], depoid: str):
        """Format data for writing to cif files in gemmi

        All data must be strings
        Anything with spaces gets single quotated
        None is replaced with "?"

        Args:
            val (Any): The data value
            depoid (str): The id of the deposition, which will be substituted for
                if the val is the placeholder "ENTRY_ID"
        Returns:
            str: The updated value
        """
        val = depoid if val == "ENTRY_ID" else val
        val = "?" if val is None else str(val)
        val = cif.quote(val)
        return val

    def add_to_cif(self, block: cif.Block, did: str):
        """Add a DepositionData object to a deposition cif file

        Args:
            block (gemmi.cif.Block): The block of the cif.Document that is being written
            did (str): The id of the deposition that is being written

        Raises:
            ValueError: If the cif type is not in ("loop", "pairs")
        """
        data = self.data_items.__dict__
        if self.cif_type == "pairs":
            for field in data:
                val = self.cifformat(data[field], did)
                block.set_pair(f"_{self.field_name}.{field}", val)
        elif self.cif_type == "loop":
            cols = list(data.keys())
            loop = block.find_loop(f"_{self.field_name}.{cols[0]}").get_loop()
            if loop is None:
                loop = block.init_loop("_", [f"{self.field_name}.{x}" for x in cols])
            loopdata = [str(self.cifformat(data[x], did)) for x in cols]
            loop.add_row(loopdata)
        else:
            raise ValueError("Invalid cif type")

    def write_json(self, output_dir: str):
        """Write a json file from DepositionData object

        Writes a file called deposition_<field_name>.json
        Args:
            output_dir (str): The dir to write the file in.
        """
        filename = os.path.join(output_dir, f"deposition_{self.field_name}.json")
        with open(filename, "w") as f:
            json.dump(self.data_items.__dict__, f, indent=4)

    def update_job_references(self, jobs_dict: Dict[str, list]):
        """Update the job references in a DepositionData object

        If an object needs to refer to a DepositionData object from another job it
        uses the placeholder "JOBREF:<jobname>".  Replaces the placeholder with the
        uuid of the appropriate DepositionData object
        """
        ddict = self.data_items.__dict__
        updated = ddict.copy()
        for i in ddict:
            if isinstance(ddict[i], str):
                if ddict[i].startswith("JOBREF:"):
                    itype = i.rstrip("_id")
                    job = clean_jobname(ddict[i].split(":")[1].strip())
                    updated[i] = None
                    for ddobj in jobs_dict[job]:
                        if itype in [ddobj.field_name, ddobj.field_name.lstrip("em_")]:
                            updated[i] = ddobj.data_items.id

        self.data_items = self.dc_type(**updated)


def merge_depdata_items(
    new_di: DepositionData, old_di: List[DepositionData]
) -> List[DepositionData]:
    """Merge together DepositionData objects, using their merge strategy

    Args:
        new_di (DepositionData): The object to be merged in
        old_di (List[DepositionData]): Object(s) to be merged into.  For DepositionData
            objects with the 'multiple' merge strategy this will be multiple objects
            for all others it will be a single object in the list

    Returns:
        List[DepositionData]: The merged object(s) a single item for "combine" and
            "overwrite" merge strategies and multiple items for "multiple"

    Raises:
        ValueError: If the objects have different merge strategies

    """
    if not all([isinstance(new_di, type(x)) for x in old_di]):
        raise ValueError("Can't merge DepositionData objects of different types")
    if old_di[0].merge_strategy == "combine":
        update_dict = deepcopy(old_di[0].data_items.__dict__)
        new_dict = deepcopy(new_di.data_items.__dict__)

        for adi in new_dict:
            if new_dict[adi] and adi != "id":
                update_dict[adi] = new_dict[adi]
        if update_dict.get("id") is not None:
            update_dict["id"] = update_dict["id"] + new_dict["id"]
        updated_data = old_di[0].dc_type(**update_dict)
        merged = [
            DepositionData(
                old_di[0].field_name,
                updated_data,
                old_di[0].merge_strategy,
                old_di[0].reverse,
            )
        ]

    elif old_di[0].merge_strategy == "overwrite":
        if hasattr(new_di.data_items, "id"):
            new_di.data_items.id.extend(  # type: ignore[attr-defined]
                old_di[0].data_items.id  # type: ignore[attr-defined]
            )
        merged = [new_di]

    elif old_di[0].merge_strategy == "multi":
        merged = old_di
        old_dicts = [x.data_items.__dict__ for x in old_di]
        if new_di.data_items.__dict__ not in old_dicts:
            merged.append(new_di)
    else:
        raise ValueError(f"Invalid merge strategy: {old_di[0].merge_strategy}")
    return merged


def make_deposition_data_object(
    data_obj,
    uid: Union[int, List[Optional[str]], str, None] = "",
) -> DepositionData:
    """Makes a new DepositionData object of the desired type

    Args:
        data_obj (object): The data class for the object with the data in it
        uid (str): A uuid4 for the object, if not specified one will be created, if
            None, no UID is needed

    Returns:
        DepositionData: The created DepositionData object

    Raises:
        ValueError: If the data dict is not appropriate for the selected dataclass
    """
    dtype = type(data_obj)
    uid = str(uuid4()) if uid in ["", None] else uid
    data_obj.id = [uid]
    lookup = ONEDEP_OBJECTS[dtype]
    return DepositionData(lookup[0], data_obj, lookup[1], lookup[2])


def gather_onedep_jobs_and_depobjects(
    terminal_job: str,
) -> Tuple[List[List[DepositionData]], Dict[str, List[DepositionData]]]:
    """Prepare a onedep deposition

    Args:
        terminal_job (str): The job to use
    Returns:
        List[List[DepositionData]]: The gathered DepositionData objects.  Each sublist
            contains objects of one type.
    Raises:
        ValueError: If the terminal job is not found.
    """

    with ProjectGraph() as pipeline:
        tproc = pipeline.find_process(terminal_job)
        if not tproc:
            raise ValueError("Unable to find terminal process")
        upstream = pipeline.get_project_procs_list(terminal_job)
    jobs = [x.name for x in upstream]

    # # get all the deposition objects for the processes in the chain
    print("Preparing deposition info")
    jobs_dict: Dict[str, List[DepositionData]] = {}
    depobjs_dict: Dict[Any, List[DepositionData]] = {}
    for job in jobs:
        print(f"-{job}")
        with ProjectGraph() as pipeline:
            the_proc = pipeline.find_process(job)
        if the_proc is None:
            continue
        proc = active_job_from_proc(the_proc)
        depdata = proc.prepare_deposition_data(depo_type="ONEDEP")

        for dep_obj_data in depdata:
            assert isinstance(dep_obj_data, OneDepData)
            dobj = make_deposition_data_object(
                data_obj=dep_obj_data, uid=dep_obj_data.id
            )
            if job in jobs_dict:
                jobs_dict[job].append(dobj)
            else:
                jobs_dict[job] = [dobj]

            dt = type(dep_obj_data)
            if dt in depobjs_dict:
                depobjs_dict[dt].append(dobj)
            else:
                depobjs_dict[dt] = [dobj]

    # return the final list of dep objects lists
    final_dos: list = []
    for dolist in depobjs_dict:
        final_dos.append(depobjs_dict[dolist])
    return final_dos, jobs_dict


class OneDepDeposition(object):
    """Object for making a onedep deposition

    Broken down in submethods for testing

    Attributes:
        raw_depobjs (list): A list of lists each contain deposition dataclasses of the
         same type
        merged_depobjs (list): The raw_repobjs list, which each subgroup merged
            according to the merge strategy of that type of deposition object
        final_depobjs (list): The merged depobjs with their UIDs and job references
            updated to their actual values
        int_ids (dict): The integer ids that correspond to the UIDs assigned to each of
            the raw deposition objects
    """

    def __init__(self, terminal_job) -> None:
        """Instantiate an OneDepDeposition

        Args:
            terminal_job (str): The name of the job to create the deposition from
        """

        self.raw_depobjs, self.jobs = gather_onedep_jobs_and_depobjects(terminal_job)
        self.merged_depobjs: List[List[DepositionData]] = []
        self.final_depobjs: List[DepositionData] = []
        self.int_ids: Dict[str, int] = {}

    def update_jobrefs(self):
        """Update job references from 'JOBREF: jobname' to the UID for that job'"""
        # update all dep object IDs to UIDs from JOBREFS
        for dobj_group in self.raw_depobjs:
            for dobj in dobj_group:
                dobj.update_job_references(self.jobs)

    def merge_depobjs(self):
        """Merge deposition objects according to their merge strategy"""
        # merge together all the dep objects
        for depobj_group in self.raw_depobjs:
            if depobj_group[0].reverse:
                depobj_group.reverse()
            n = 1
            merged = [depobj_group[0]]
            while n < len(depobj_group):
                merged = merge_depdata_items(depobj_group[n], merged)
                n += 1
            self.merged_depobjs.append(merged)

    def make_int_ids(self):
        """Make a dictionary {UID: integer ID}"""
        # create a dict of which integer ids go with which UIDs
        for dobj_group in self.raw_depobjs:
            if len(dobj_group) == 1:
                for uid in dobj_group[0].data_items.id:  # type: ignore[attr-defined]
                    self.int_ids[uid] = 1
            elif len(dobj_group) > 1:
                n = 1
                for dob in dobj_group:
                    self.int_ids[dob.data_items.id[0]] = n  # type: ignore[attr-defined]
                    n += 1

    def update_uids_to_int_ids(self):
        """Update all UIDS in deposition objects to the integer ids"""
        # update all depobject ID to integers
        for group in list(chain(self.merged_depobjs)):
            for depobj in group:
                ddict = depobj.data_items.__dict__
                ddict["id"] = self.int_ids[ddict["id"][0]]
                for field in ddict:
                    if (
                        field.endswith("_id")
                        and field != "entry_id"
                        and isinstance(ddict[field], list)
                    ):
                        if is_uuid4(ddict[field][0]):
                            ddict[field] = self.int_ids[ddict[field][0]]
                depobj.data_items = depobj.dc_type(**ddict)

    def prepare_deposition(self):
        """Do all the steprs to get ready for a deposition"""
        self.update_jobrefs()
        self.merge_depobjs()
        self.make_int_ids()
        self.update_uids_to_int_ids()
        self.final_depobjs = []
        for group in self.merged_depobjs:
            for dobj in group:
                self.final_depobjs.append(dobj)

    def write_deposition_cif_file(self, depo_id: Optional[str]):
        """Write a cif file for deposition through the onedep system

        Args:
            depo_id: The id of the deposition, assigned by EMDB/PDB

        Raises:
            ValueError: If the deposition has not been prepared to write yet
        """
        if not self.final_depobjs:
            raise ValueError(
                "Deposition is not ready to write, must run self.prepare_deposition"
            )
        depdoc = cif.Document()
        did = depo_id.upper() if depo_id is not None else "?"
        if did == "?":
            print(
                "WARNING: No deposition ID was provided, the '_entry.id' field in the "
                "deposition file must be updated with the actual deposition ID before "
                "it can be used."
            )
        depdoc.add_new_block(did)
        block = depdoc.find_block(did)
        block.set_pair("_entry.id", did)
        for depobj in self.final_depobjs:
            depobj.add_to_cif(block, did)
        fn = f"{did}_deposition_data.cif" if did != "?" else "draft_deposition_data.cif"
        depdoc.write_file(fn, style=cif.Style.Aligned)
        print(f"wrote {os.path.abspath(fn)}")
        return fn
