#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import logging
import argparse
import shlex
import sys
from glob import glob
from typing import Optional, List

from pipeliner.api.manage_project import (
    PipelinerProject,
    convert_pipeline,
    look_for_project,
    get_commands_and_nodes,
)
from pipeliner.utils import (
    check_for_illegal_symbols,
    print_nice_columns,
    make_pretty_header,
    wrap_text,
)
from pipeliner.api.api_utils import (
    write_default_jobstar,
    write_default_runjob,
    validate_starfile,
    get_job_info,
)
from pipeliner.pipeliner_job import Ref, PipelinerJob
from pipeliner.data_structure import (
    JOBSTATUS_ABORT,
    JOBSTATUS_SCHED,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_FAIL,
    JOBSTATUS_RUN,
)
from pipeliner.job_factory import new_job_of_type, get_job_types

logger = logging.getLogger(__name__)


def set_up_logging():
    """Basic logging configuration for the Pipeliner as a command line tool.

    Logs are written to stdout by default.
    """
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        datefmt="%Y-%m-%d %H:%M:%S",
        format="%(asctime)s.%(msecs)03d: %(module)s - %(levelname)s: %(message)s",
    )


def get_arguments():

    parser = argparse.ArgumentParser(description="CCPEM Pipeliner command line utility")
    parser._optionals.title = "Arguments"
    parser.add_argument(
        "--new_project",
        help=(
            "Initialize a new project in this directory."
            " Project name is optional, if none is specified the project will"
            " be called 'default' (recommended)"
        ),
        nargs="?",
        default=None,
        const="default",
        metavar="project name",
    )

    parser.add_argument(
        "--available_jobs",
        help=(
            "Show a list of all available jobs, optionally add a search string"
            " to limit the search, Leave blank to show all available job types"
        ),
        nargs="?",
        default=None,
        const="",
        metavar="search string",
    )

    parser.add_argument(
        "--check_jobs",
        help=(
            "Get a list of available and unavailable jobs with detailed info on"
            "the programs they use and why they are unavailable (if applicable)"
        ),
        nargs="?",
        default=None,
        const="",
        metavar="search string",
    )

    runjob = parser.add_argument_group("Running jobs")
    runjob.add_argument(
        "--run_job",
        help="Create a job using a run.job or job.star file to get the parameters",
        nargs="?",
        default=None,
        metavar="run.job or job.star file",
    )

    runjob.add_argument(
        "--overwrite",
        help="Use with --run_job to overwrite a current job rather than making"
        " a new job.  Jobs can only be overwritten with the same job type",
        nargs="?",
        default=None,
        metavar="job name",
    )

    runjob.add_argument(
        "--continue_job",
        help="Continue a job that has been previously run."
        " Edit the job's continue_job.star file to modify parameters"
        " for the continuation",
        nargs="?",
        default=None,
        metavar="job name",
    )

    runjob.add_argument(
        "--abort_job",
        help="Abort a running job",
        nargs="?",
        default=None,
        metavar="job name",
    )

    runjob.add_argument(
        "--print_command",
        help="Read a job.star or run.job file and print the command(s) it"
        " would produce",
        nargs="?",
        default=None,
        metavar="run.job or job.star file",
    )

    runjob.add_argument(
        "--schedule_job",
        help="Add a job to list of scheduled jobs using a run.job or job.star "
        "file to get the parameters.  If followed by a job.star file it will create a"
        " new job if followed by a job name it will schedule a continuation of that "
        "job",
        nargs="?",
        default=None,
        metavar="run.job, job.star file or job name",
    )

    schedjob = parser.add_argument_group("Executing Schedules")

    schedjob.add_argument(
        "--run_schedule",
        help="Create a schedule choosing from the currently scheduled jobs and "
        "run it",
        action="store_true",
    )

    schedjob.add_argument(
        "--name",
        help="(required) Enter a name for the new schedule",
        nargs="?",
        default=None,
    )

    schedjob.add_argument(
        "--jobs",
        help="(required) Enter the jobs that will be run.  Make sure to list the"
        " jobs in the order which they should be run",
        nargs="*",
        default=None,
        metavar="job name",
    )

    schedjob.add_argument(
        "--min_between",
        help="(optional) Wait at least this many minutes between jobs",
        nargs="?",
        default=0,
        metavar="n",
    )
    schedjob.add_argument(
        "--nr_repeats",
        help="(optional) Repeat the schedule this many times",
        nargs="?",
        default=1,
        metavar="1",
    )

    schedjob.add_argument(
        "--wait_sec_after",
        help=(
            "(optional) Wait this many seconds after finishing before starting"
            " the next job"
        ),
        nargs="?",
        default=2,
        metavar="2",
    )

    schedjob.add_argument(
        "--wait_min_before",
        help="(optional) Wait this many minutes before starting the schedule",
        nargs="?",
        default=0,
        metavar="0",
    )

    schedjob.add_argument(
        "--stop_schedule",
        help="(required) Enter a name for the schedule to be stopped",
        nargs="?",
        default=None,
    )

    delete = parser.add_argument_group("Deleting jobs")
    delete.add_argument(
        "--delete_job",
        help="Remove job(s) and put in the trash, deletes the job and all of its child"
        " processes",
        nargs="?",
        metavar="job name",
        default=None,
    )

    undelete = parser.add_argument_group("Undeleting jobs")
    undelete.add_argument(
        "--undelete_job",
        help="Restore a deleted job and any of its deleted parent processes from the "
        "trash",
        nargs="?",
        metavar="job name",
        default=None,
    )

    modify = parser.add_argument_group("Modifying jobs")
    modify.add_argument(
        "--set_alias",
        help="Set the alias of a job",
        nargs=2,
        metavar=("[job name]", "[new alias]"),
        default=None,
    )

    modify.add_argument(
        "--clear_alias",
        help="Clear the alias of a job",
        nargs=1,
        metavar="[job name]",
        default=None,
    )

    modify.add_argument(
        "--set_status",
        help="Set the status of a job; choose from 'finished, failed, or aborted",
        nargs=2,
        metavar=("[job name]", "{finished, failed, aborted}"),
        default=None,
    )

    cleanup = parser.add_argument_group("Cleaning Up Job(s)")
    cleanup.add_argument(
        "--cleanup",
        help=(
            "Delete intermediate files from these job(s) to save disk space"
            "; enter ALL to clean up all jobs"
        ),
        nargs="*",
        metavar="job name",
        default=None,
    )

    cleanup.add_argument(
        "--harsh",
        help="Add this argument to --cleanup to delete even more files",
        action="store_true",
    )

    cleanup.add_argument(
        "--restore_cleaned_files",
        help=(
            "Restore files that have been moved to Trash by cleanup back to their "
            "original jobs.  Specify the jobs to restore or run alone to restore all"
            "cleaned files"
        ),
        nargs="?",
        metavar="job name",
        const="all",
    )

    utils = parser.add_argument_group("Utilities")
    utils.add_argument(
        "--validate_starfile",
        help=(
            "Check a star file and make sure it is written in the correct format.  If"
            " errors are found will attempt to fix them"
        ),
        nargs="?",
        metavar="star file",
        default=None,
    )

    utils.add_argument(
        "--convert_pipeline_file",
        help="Convert a pipeline file from Relion 3.1 to the CCPEM pipeliner format",
        nargs="?",
        metavar="_pipeline.star file",
        default=None,
    )

    utils.add_argument(
        "--default_jobstar",
        help=(
            "Make a job.star file with the default values for a specific job type "
            "for use with the ccpem-pipeliner"
        ),
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--relionstyle",
        help=(
            "OPTIONAL: Add this argument to --default_jobstar to write job.star files "
            "which are compatible with RELION 4.0.  RELIONstyle job.star files are "
            "fully compatible with the pipeliner, pipeliner job.star files may have"
            "differences that cause bugs in RELION"
        ),
        action="store_true",
    )
    utils.add_argument(
        "--default_runjob",
        help=(
            "Make a _run.job file with the default values for a specific job type."
            " These files can also be used to run jobs and a more human readable"
        ),
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--job_info",
        help="Get info about a specific job type, including any reference(s)",
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--empty_trash",
        help="Delete the files in the trash.  THIS CANNOT BE UNDONE!",
        action="store_true",
    )

    utils.add_argument(
        "--job_references",
        help="Get literature reference(s) for a specific job type",
        nargs="?",
        metavar="job type",
        default=None,
    )

    utils.add_argument(
        "--references_report",
        help="Get all the literature references for a project with the selected job as"
        " the termial job",
        nargs="?",
        metavar="job name",
        default=None,
    )

    analysis = parser.add_argument_group("Project Analysis")

    analysis.add_argument(
        "--metadata_report",
        help=(
            "Prepares a report in .json format for the terminal job and all"
            "of its parent jobs"
        ),
        nargs="?",
        const="nojob",
        metavar="terminal job",
        default=None,
    )
    archiving = parser.add_argument_group("Project Archiving")
    archiving.add_argument(
        "--full_archive",
        help=(
            "Create a full archive for a project.  This will contain the entire"
            " job dirs for the stated terminal job and all of its parents"
        ),
        nargs="?",
        default=None,
        metavar="terminal job name",
    )

    archiving.add_argument(
        "--simple_archive",
        help=(
            "Create a simple archive for a project.  This will contain just the"
            " directory structure and parameter files for the stated terminal job and"
            "all of it's parents along with a script to automatically re-run the "
            "project through the terminal job"
        ),
        nargs="?",
        default=None,
        metavar="terminal job name",
    )
    deposition = parser.add_argument_group("Prepare EMDB/EMPIAR/PDB depositions")
    deposition.add_argument(
        "--empiar_deposition",
        help="Prepare a deposition for upload to the EMPIAR database",
        nargs="?",
        default=None,
        metavar="terminal job name",
    )
    deposition.add_argument(
        "--onedep_deposition",
        help="Prepare a deposition for upload to the pdb/emdb databases",
        nargs="?",
        default=None,
        metavar="terminal job name",
    )
    deposition.add_argument(
        "--deposition_id",
        help=(
            "[Optional] The deposition ID. This must be assigned by EBI.  If not "
            "specified, a draft deposition will be created"
        ),
        nargs="?",
        default=None,
        metavar="Deposition id (Assigned by the database)",
    )

    deposition.add_argument(
        "--empiar_jobstar_file",
        help=(
            "(optional) A jobstar file from a pipeliner.deposition.empiar job. This "
            "contains data necessary from the deposition that cannot be automatically "
            "found. If not included, the deposition will only contain the data that "
            "can be found and will need to be manually completed"
        ),
        nargs="?",
        default=None,
        metavar="Template file to update",
    )
    deposition.add_argument(
        "--empiar_movies",
        help=(
            "(optional) Add this argument to deposition to include unprocessed movies "
            "in the EMPIAR deposition"
        ),
        action="store_true",
    )
    deposition.add_argument(
        "--empiar_mics",
        help=(
            "(optional) Add this argument to deposition to include corrected "
            "micrographs in the EMPIAR deposition"
        ),
        action="store_true",
    )
    deposition.add_argument(
        "--empiar_parts",
        help=(
            "(optional) Add this argument to deposition to include particles in the "
            "EMPIAR deposition"
        ),
        action="store_true",
    )
    deposition.add_argument(
        "--empiar_corr_parts",
        help=(
            "(optional) Add this argument to deposition to include corrected "
            "(polished) particles in the EMPIAR deposition"
        ),
        action="store_true",
    )
    # get the args
    return parser


def f_new_project(args):
    """pipeliner sub function to create a new project"""
    # look for illegal names...
    error_message = check_for_illegal_symbols(args.new_project, "pipeline name")
    if error_message:
        raise ValueError(error_message)

    if args.new_project == "mini":
        raise ValueError(
            "ERROR: The pipline cannot be named 'mini' this name is used"
            " internally by the pipeliner. "
        )
    # initialize the project
    proj_dict = look_for_project()
    if proj_dict is not None:
        raise ValueError(
            "ERROR: Could not initialize a new project in this directory. A"
            f" pipeline called {proj_dict['pipeline file']} already exists"
        )
    logger.info("Initializing a new project...")
    proj = PipelinerProject(pipeline_name=args.new_project, make_new_project=True)
    logger.info(f"Successfully created {proj.pipeline_name}_pipeline.star")


def f_validate_starfile(args):
    """pipeliner sub function to validate a starfile"""
    fn_in = args.validate_starfile
    validate_starfile(fn_in)


def f_print_command(args):
    """pipeliner sub function to print command from a job.star file"""
    try:
        # If there is no existing project, don't make a new one but continue anyway
        proj = PipelinerProject()
    except FileNotFoundError:
        proj = None
    job_file = args.print_command
    commands, innodes, outnodes, warns, progerrs = get_commands_and_nodes(job_file)

    print("\n{}".format(make_pretty_header(f"{len(commands)} commands to be run: ")))

    for command in commands:
        print(shlex.join(command))

    if len(innodes) > 0:
        print(make_pretty_header("{} expected input nodes:".format(len(innodes))))
        for node in innodes:
            print("{}     {}".format(node[0], node[1]))
    if len(outnodes) > 0:
        print(make_pretty_header("{} expected output nodes:".format(len(outnodes))))
        for node in outnodes:
            print("{}     {}".format(node[0], node[1]))
    if proj is None:
        logger.warning(
            "No pipeliner project present, so command is generated as `job000`"
        )
    if warns:
        logger.warning("The following parameter validation warnings were raised:")
        for w in warns:
            logger.warning(f"{9 * ' '}{w.raised_by[0].label}: {w.message}")
    if progerrs:
        logger.warning("Command will not be able to run for the following reasons:")
        for per in progerrs:
            logger.warning(f"{9 * ' '}{per}")


def f_job_info(args):
    """pipeliner sub function to print the info from a job"""
    info = get_job_info(args.job_info)
    if info is not None:
        print("\n")
        print(
            make_pretty_header(
                f"CCPEM-pipeliner {args.job_info} job vers {info.version}"
            )
        )
        print(f"{info.short_desc}\n")
        print(f"Job author: {info.job_author}")
        print("Program(s) used:")
        for prg in info.programs:
            print(f"  {prg.command}")
            print(f"\tvers: {prg.get_version()}\n\tpath: {prg.exe_path}")
        # --
        wrap_text(info.long_desc)

        print(f"\nOnline documentation:\n  {info.documentation}\n")

        if len(info.references) > 0:
            print("Reference(s):")
            for ref in info.references:
                # make sure the references are in the correct format
                if not isinstance(ref, Ref):
                    logger.warning(
                        "  Reference formatting error\n  Reference format was "
                        f"{type(ref)}; It should be in the pipeliner Ref format\n"
                        f"  {ref}"
                    )
                    break
                for line in str(ref).split("\n"):
                    print(f"  {line}")
                print("\n")
        err = False
        for prog in info.programs:
            if not prog.exe_path:
                err = True
            if err:
                print("This job is currently unavailable")


def f_convert_pipeline_file(args):
    """pipeliner sub function to convert pipeline from relion 3 to pipeliner"""
    pipeline_file = args.convert_pipeline_file.replace("_pipeline.star", "")
    convert_pipeline(pipeline_file)


def f_default_jobstar(args):
    """pipeliner sub function to generate a default job.star file"""
    write_default_jobstar(args.default_jobstar, relionstyle=args.relionstyle)


def f_default_runjob(args):
    """pipeliner sub function to generate a default run.job file"""
    write_default_runjob(args.default_runjob)


def f_available_jobs(args):
    """pipeliner sub function to print all available jobs"""
    search_term = (
        "*all*" if len(args.available_jobs) == 0 else args.available_jobs.lower()
    )
    print(make_pretty_header(f"Available jobtypes: {search_term}"))
    # get all jobs
    jobs = get_job_types(args.available_jobs)
    available, not_available = [], []
    for job in jobs:
        if job.jobinfo.is_available:
            available.append(job)
        else:
            not_available.append(job)

    # print out the results
    print_nice_columns([x.PROCESS_NAME for x in available], "No available jobs")
    if not_available:
        print(make_pretty_header("Unavailable jobs"))
        print_nice_columns([x.PROCESS_NAME for x in not_available])


def f_check_jobs(args):
    """pipeliner sub function to print info on if jobs can be run"""
    search_term = "*all*" if len(args.check_jobs) == 0 else args.check_jobs.lower()
    print(make_pretty_header(f"Available jobtypes: {search_term}"))
    # get all jobs
    jobs = get_job_types(args.check_jobs)
    available, not_available = [], []
    for job in jobs:
        if job.jobinfo.is_available:
            available.append(job)
        else:
            not_available.append(job)

    # print out the results
    for j in available:
        print(f"\n{j.PROCESS_NAME}")
        for p in j.jobinfo.programs:
            print(
                f"  * {p.command}:\n    version: {p.get_version()}\n    "
                f"path: {p.exe_path}"
            )
    if not available:
        print("No available jobs")
    if not_available:
        print(make_pretty_header("Unavailable jobs"))
    for j in not_available:
        print(f"\n{j.PROCESS_NAME}")
        for p in j.jobinfo.programs:
            print(f"  - {p.command}\tERROR: '{p.command}' not found in system path")


def f_cleanup(args):
    """pipeliner sub function to run cleanup on jobs"""
    proj = PipelinerProject()
    if args.cleanup not in [None, ["ALL"]]:
        procs = proj.parse_proclist(args.cleanup)
        proj.run_cleanup(procs, args.harsh)
    elif args.cleanup == ["ALL"]:
        proj.cleanup_all(args.harsh)
    else:
        raise ValueError("ERROR: No jobs were specified to clean up")


def f_delete_job(args):
    """pipeliner sub function to delete a job"""
    proj = PipelinerProject()
    proc = proj.parse_procname(args.delete_job)
    proj.delete_job(proc)


def f_undelete_job(args):
    """pipeliner sub function to undelete a job"""
    proj = PipelinerProject()
    proc = proj.parse_procname(args.undelete_job, search_trash=True)
    proj.undelete_job(proc)


def f_set_status(args):
    """pipeliner sub function to set the status of a job"""
    proj = PipelinerProject()
    job, new_status_input = args.set_status
    status_cases = {
        "aborted": JOBSTATUS_ABORT,
        "scheduled": JOBSTATUS_SCHED,
        "success": JOBSTATUS_SUCCESS,
        "failed": JOBSTATUS_FAIL,
        "running": JOBSTATUS_RUN,
    }

    new_status = ""
    for stat in status_cases:
        if new_status_input.lower() in stat:
            new_status = status_cases[stat]
    if not new_status:
        raise ValueError(
            f"ERROR: The job status must be in {list(status_cases)}."
            f' The status entered was "{new_status_input}"'
        )
    job = proj.parse_procname(job)
    proj.update_job_status(job, new_status)


def f_set_alias(args):
    """pipeliner sub function to set the alias of a job"""
    proj = PipelinerProject()
    job, new_alias = args.set_alias
    job = proj.parse_procname(job)
    proj.set_alias(job, new_alias)


def f_clear_alias(args):
    """pipeliner sub function to clear the alias of a job"""
    proj = PipelinerProject()
    job = args.clear_alias[0]
    job = proj.parse_procname(job)
    proj.set_alias(job, None)


def f_run_job(args) -> PipelinerJob:
    """pipeliner sub function to run a new job"""
    proj = PipelinerProject()
    overwrite = None
    if args.overwrite is not None:
        target = proj.parse_procname(args.overwrite, search_trash=False)
        overwrite = target
    the_job = proj.run_job(args.run_job, overwrite)
    print(make_pretty_header(f"Ran job as: {the_job.output_dir}"))
    # for testing only
    return the_job


def f_continue_job(args):
    """pipeliner sub function to continue a job"""
    proj = PipelinerProject()
    job_name = proj.parse_procname(args.continue_job)
    job = proj.continue_job(job_name)
    logger.info("Continuing " + job_name)
    # for testing onloy
    return job


def f_abort_job(args):
    """pipeliner sub function to abort a running job"""
    proj = PipelinerProject()
    job_name = proj.parse_procname(args.abort_job)
    proj.abort_job(job_name)
    logger.info(f"Aborted job: {job_name}")


def f_schedule_job(args):
    """pipeliner sub function to schedule a job"""
    proj = PipelinerProject()
    # if a starfile is given it is a new job
    if ".star" in args.schedule_job:
        the_job = proj.schedule_job(args.schedule_job).output_dir
    else:
        the_job = proj.parse_procname(args.schedule_job, search_trash=False)
        proj.schedule_continue_job(the_job)

    logger.info(f"Scheduled job as: {the_job}")


def f_run_schedule(args):
    """pipeliner sub function to run scheduled jobs"""
    proj = PipelinerProject()
    if not (args.name and args.jobs):
        raise ValueError("ERROR: --name and --jobs arguments are required")
    job_ids = proj.parse_proclist(args.jobs)
    proj.run_schedule(
        args.name,
        job_ids,
        int(args.nr_repeats),
        int(args.min_between),
        int(args.wait_min_before),
        int(args.wait_sec_after),
    )


def f_stop_schedule(args):
    """pipeliner sub function to stop a running schedule"""
    proj = PipelinerProject()
    # allow for entry of the full file name too
    sched_prefix = f"RUNNING_PIPELINER_{proj.pipeline_name}_"
    sched_name = args.stop_schedule.replace(sched_prefix, "")
    proj.stop_schedule(sched_name)


def f_empty_trash(_):
    """pipeliner sub function to empty the trash"""
    proj = PipelinerProject()
    trash_files = glob("Trash/*/*/*")
    if len(trash_files) > 0:

        logger.info("Emptying trash")
        print(f"Do want to delete {len(trash_files)} files in the trash?")
        doit = input("THIS CANNOT BE UNDONE! (Y/N): ")
        if doit in ["Y", "y", "Yes", "YES"]:
            proj.empty_trash()
        else:
            logger.info("Exiting without emptying trash")
    else:
        raise ValueError("WARNING: No files found in Trash/")


def f_simple_archive(args):
    """pipeliner sub function to create a simple archive"""
    proj = PipelinerProject()
    jobname = proj.parse_procname(args.simple_archive)
    archive = proj.create_archive(jobname, full=False, tar=True)
    wrap_text(
        f"\n{make_pretty_header('Preparing simple archive')}\n"
        "This archive will allow re-running of all jobs leading up to"
        f" and including {args.simple_archive}. \nFiles not produced "
        "by these jobs (IE: raw data, queue submission script templates, or "
        "external executables) will not be included in the archive.\nResults"
        " from re-running the archive may not be identical to the original due"
        " to stochastic steps in processing or differences in the systems they"
        " are run on.\n\nTo produce an archive with exact fidelity to the "
        "original use the '--full_archive' option"
    )
    logger.info(f"Created simple archive {archive}")


def f_full_archive(args):
    """pipeliner sub function to create a full archive"""
    proj = PipelinerProject()
    jobname = proj.parse_procname(args.full_archive)
    archive = proj.create_archive(jobname, full=True, tar=True)
    logger.info(f"Created full archive {archive}")


def f_metadata_report(args):
    """pipeliner sub function to generate a project metadata report"""
    proj = PipelinerProject()
    if args.metadata_report == "nojob":
        logger.error("Could not create report, no terminal job specified")
        print("\nUSAGE: pipeliner --metadata_report <job_name>")
        return
    jobname = proj.parse_procname(args.metadata_report)
    metadata_outfile, njobs = proj.prepare_metadata_report(jobname)
    logger.info(
        f"Preparing metadata report for {jobname} and {njobs - 1} parent jobs..."
    )
    logger.info(f"Wrote metadata report to {metadata_outfile}")


def f_jobtype_ref(args):
    if not args.job_references:
        logger.error(
            "ERROR: A job type must be specified\npipeliner --job_references "
            "<job type>"
        )
    job = new_job_of_type(args.job_references)
    refs = job.jobinfo.references
    for ref in refs:
        print(f"\n{str(ref)}")


def f_project_ref(args):
    if not args.references_report:
        logger.error("Could not make reference report. No terminal job specified")
        print("USAGE: pipeliner --references_report <terminal job>")

    proj = PipelinerProject()
    jobname = proj.parse_procname(args.references_report)
    outname, njobs = proj.create_reference_report(jobname)
    logger.info(
        f"Saved reference report: {outname} for {jobname} and {njobs} parent_jobs"
    )


def f_empiar_deposition(args):
    print(make_pretty_header("Preparing EMPIAR deposition..."))
    proj = PipelinerProject()
    jobname = proj.parse_procname(args.empiar_deposition)
    movs = args.empiar_movies
    mics = args.empiar_mics
    parts = args.empiar_parts
    rparts = args.empiar_corr_parts
    if not any([movs, mics, parts, rparts]):
        movs, mics, parts, rparts = True, True, True, True
    printouts = {
        "Micrograph movies": movs,
        "Corrected micrographs": mics,
        "Particles": parts,
        "Corrected particles": rparts,
    }
    print("Will include the following data in deposition if available:")
    for po in printouts:
        if printouts[po]:
            print(f"  - {po}")

    output_file = proj.prepare_deposition(
        terminal_job=jobname,
        depo_type="empiar",
        jobstar_file=args.empiar_jobstar_file,
        empiar_do_mov=movs,
        empiar_do_mics=mics,
        empiar_do_parts=parts,
        empiar_do_rparts=rparts,
    )
    print(f"Finished! Deposition file is: {output_file}")


def f_onedep_deposition(args):
    print("Preparing ONEDEP deposition...")
    proj = PipelinerProject()
    depid = args.deposition_id
    jobname = proj.parse_procname(args.onedep_deposition)
    output_file = proj.prepare_deposition(
        terminal_job=jobname, depo_type="onedep", depo_id=depid
    )
    print(f"Finished! Deposition file is: {output_file}")


def f_restore_cleaned_files(args):
    proj = PipelinerProject()
    if args.restore_cleaned_files == "all":
        print("Restoring cleaned files for all jobs")
        proj.restore_all_cleaned_files()
    else:
        job = proj.parse_procname(args.restore_cleaned_files)
        print(f"Restoring cleaned up files for {job}")
        proj.restore_from_cleanup(job)


def main(in_args: Optional[List[str]] = None):
    """Utility for running pipeliner functions from the command-line

    Args:
        in_args (list): The command-line arguments, including "pipeliner" at
            position 0
    """
    set_up_logging()
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    # make sure only one function is run at a time
    # args that need values
    funct2run = None
    argcount = 0
    for arg in (
        (args.run_job, f_run_job),
        (args.delete_job, f_delete_job),
        (args.undelete_job, f_undelete_job),
        (args.set_status, f_set_status),
        (args.set_alias, f_set_alias),
        (args.clear_alias, f_clear_alias),
        (args.cleanup, f_cleanup),
        (args.schedule_job, f_schedule_job),
        (args.print_command, f_print_command),
        (args.validate_starfile, f_validate_starfile),
        (args.continue_job, f_continue_job),
        (args.abort_job, f_abort_job),
        (args.stop_schedule, f_stop_schedule),
        (args.new_project, f_new_project),
        (args.convert_pipeline_file, f_convert_pipeline_file),
        (args.default_jobstar, f_default_jobstar),
        (args.default_runjob, f_default_runjob),
        (args.job_info, f_job_info),
        (args.available_jobs, f_available_jobs),
        (args.check_jobs, f_check_jobs),
        (args.simple_archive, f_simple_archive),
        (args.full_archive, f_full_archive),
        (args.metadata_report, f_metadata_report),
        (args.references_report, f_project_ref),
        (args.job_references, f_jobtype_ref),
        (args.empiar_deposition, f_empiar_deposition),
        (args.onedep_deposition, f_onedep_deposition),
        (args.restore_cleaned_files, f_restore_cleaned_files),
    ):
        if arg[0] is not None:
            argcount += 1
            funct2run = arg[1]

    # true/false args
    for arg in (
        (args.run_schedule, f_run_schedule),
        (args.empty_trash, f_empty_trash),
    ):
        if arg[0]:
            argcount += 1
            funct2run = arg[1]

    if argcount > 1:
        raise ValueError("ERROR: please only select one type of function at a time")
    elif argcount == 0:
        parser.print_help()

    # run the appropriate function
    if funct2run is not None:
        funct2run(args)


if __name__ == "__main__":
    main()
