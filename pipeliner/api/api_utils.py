#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import Optional
import logging

from pipeliner.job_factory import new_job_of_type
from pipeliner.starfile_handler import StarFile, JobStar
from pipeliner.star_writer import write_jobstar
from pipeliner.pipeliner_job import JobInfo
from pipeliner.utils import get_pipeliner_root

logger = logging.getLogger(__name__)


def write_default_jobstar(
    job_type: str, out_fn: Optional[str] = None, relionstyle: bool = False
):
    """
    Write a job.star file for the specified type of job

    The default jobstar contains all the job options with their values
    set as the defaults

    Args:
        job_type (str): The type of job
        out_fn (str): Name of the file to write the output to. If left blank
            defaults to `<job_type>_job.star`
        relionstyle (bool): Should the job.star files be written in the relion
            format? Relion files are compatible with the pipeliner, but the
            pipeliner versions are not back compatible with Relion. If this
            option is selected a Relion job type should be used for job_type
    Returns:
        str: The name of the output file written
    """
    if out_fn is None:
        out_fn = "{}_job.star".format(job_type.lower().replace(".", "_"))

    # pipeliner style jobstar files
    if not relionstyle:
        the_job = new_job_of_type(job_type)
        the_job.write_jobstar("", output_fn=out_fn)
        logger.info(f"Wrote {out_fn} with default values")
        return out_fn

    # relion_style jobstar files
    # TODO: look at whether the relionstyle option and the default_relion_..._job.star
    #  files can be completely removed. The "compatibility" job options should be able
    #  to handle this instead.
    relion_jobtype = job_type.split(".")[-1]
    jobstar_path = os.path.join(
        get_pipeliner_root(),
        f"jobs/relion/relion_jobstars/default_relion_{relion_jobtype}_job.star",
    )
    try:
        js_data = JobStar(jobstar_path).all_options_as_dict()
    except FileNotFoundError:
        raise ValueError("This is not a recognised RELION job type.")

    write_jobstar(js_data, out_fn)


def write_default_runjob(job_type: str, out_fn: Optional[str] = None) -> str:
    """
    Write a run.job file for the specified type of job

    The default runjob contains all the job option labels with their values
    set as the defaults

    Args:
        job_type (str): The type of job
        out_fn (str): Name of the file to write the output to.  If left blank
            defaults to `<job_type>_run.job`

    Returns:
        str: The name of the output file written
    """
    the_job = new_job_of_type(job_type)
    if out_fn is None:
        out_fn = "{}_run.job".format(job_type.lower().replace(".", "_"))
    the_job.write_runjob(out_fn)
    logger.info(f"Wrote {out_fn} with default values")
    return out_fn


def job_default_parameters_dict(jobtype: str) -> dict:
    """
    Get dictionary of a job's parameters

    Args:
        jobtype (str): The type of job to get the dict for

    Returns:
        dict: The parameters dict. Suitable for running a job from
            :meth:`~pipeliner.api.manage_project.PipelinerProject.run_job`

    """

    job = new_job_of_type(jobtype)
    params = job.get_default_params_dict()
    return params


def validate_starfile(fn_in: str):
    """Checks for inappropriate use of reserved words in starfiles

    Writes a corrected version with proper quotation if possible. The
    original file is saved with a '.orig' suffix added.

    Args:
        fn_in (str): The name of the file to check
    """
    # The StarFile initialiser tries to read the file and convert it if there are
    # reserved words
    _ = StarFile(fn_in)


def get_job_info(job_type: str) -> Optional[JobInfo]:
    """Get information about a job

    Args:
        job_type (str): The type of job to return info on

    Returns:
        :class:`~pipeliner.pipeliner_job.JobInfo`: JobInfo object with
        info about the job and it's references

    Raises:
        ValueError: If the job type is not found
    """

    the_job = new_job_of_type(job_type)
    try:
        info = the_job.jobinfo

    except AttributeError:
        logger.error(f"Jobtype {job_type} has no info attribute")
        return None
    return info


def edit_jobstar(fn_template: str, params_to_change: dict, out_fn: str) -> str:
    """Modify one or more parameters in a job.star file

    Args:
        fn_template (str): The name of the job.star file to use as a template
        params_to_change (dict): The parameters to change in the
            format ``{param_name: new_value}``
        out_fn (str): Name for the new file to be written

    Returns:
        str: The name of the output file written
    """
    js = JobStar(fn_template)
    js.modify(params_to_change)
    js.write(out_fn)
    return out_fn
