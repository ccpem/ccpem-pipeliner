#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import logging
from glob import glob
from typing import List, Optional, Union, Tuple, Dict, Literal
from pathlib import Path

import shutil
import json
from pipeliner.deposition_tools.empiar_deposition import prepare_empiar_deposition
from pipeliner.deposition_tools.onedep_deposition import OneDepDeposition
from pipeliner import job_manager
from pipeliner.data_structure import (
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_SCHED,
    CLASS2D_HELICAL_NAME_EM,
    CLASS2D_PARTICLE_NAME_EM,
    CLASS2D_HELICAL_NAME_VDAM,
    CLASS2D_PARTICLE_NAME_VDAM,
    INIMODEL_JOB_NAME,
    CLASS3D_PARTICLE_NAME,
    CLASS3D_HELICAL_NAME,
    REFINE3D_HELICAL_NAME,
    REFINE3D_PARTICLE_NAME,
    MULTIBODY_REFINE_NAME,
    MULTIBODY_FLEXANALYSIS_NAME,
    CLEANUP_DIR,
    TRASH_DIR,
)

from pipeliner.starfile_handler import JobStar, convert_old_relion_pipeline
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import (
    read_job,
    job_from_dict,
    active_job_from_proc,
)
from pipeliner.utils import (
    clean_jobname,
    decompose_pipeline_filename,
    date_time_tag,
    touch,
    run_subprocess,
    make_pretty_header,
    get_job_number,
    get_directory_size,
    make_pretty_size,
    atomic_write_json,
)
from pipeliner.metadata_tools import (
    get_metadata_chain,
    add_md_to_summary_data,
    remove_md_from_summary_data,
    METADATA_REPORT_DIR,
)
from pipeliner.data_structure import PROJECT_FILE, SUCCESS_FILE, JOBINFO_FILE
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.reference_reports import (
    prepare_reference_report,
    add_ref_report_to_summary_file,
    remove_ref_report_from_summary_file,
    REFERENCE_REPORT_DIR,
)
from pipeliner.summary_data_tools import remove_from_summary_file, get_summary_file_data
from pipeliner.archive_tools import (
    prepare_full_archive,
    prepare_script_archive,
    add_archive_to_summary_file,
)

logger = logging.getLogger(__name__)


def look_for_project(pipeline_name: str = "default") -> Optional[dict]:
    """See if a pipeliner project exists in the current directory

    Args:
        pipeline_name (str): The name of the pipeline to look for. This is the same as
            the pipeline file name with "_pipeline.star" removed.

    Returns:
        Info about the project, as a :class:`dict`, or ``None`` if there is no existing
        project.
    """

    # first look for a project file
    if os.path.isfile(PROJECT_FILE):
        try:
            with open(PROJECT_FILE) as project_file:
                project_info = json.load(project_file)
            new_pipe = project_info["pipeline file"].replace("_pipeline.star", "")
            if pipeline_name != new_pipe:
                logger.warning(
                    f"Pipeline name '{pipeline_name}' does not match the "
                    f"pipeline name specified in the project file ({new_pipe})."
                    "\nReverting to the name in the project file"
                )
            pipeline_name = new_pipe
            return project_info
        except json.decoder.JSONDecodeError:
            logger.warning(
                "An outdated CCP-EM Pipeliner project file was found. Removing"
                " it and making a fresh one."
            )

    # If no project file, see if the pipeline file exists. If so, the project was
    # created in Relion rather than the pipeliner.
    if os.path.isfile(f"{pipeline_name}_pipeline.star"):
        project_info = {
            "project name": "Project imported from RELION",
            "description": (
                'This project was imported from RELION. "Date created" is the date it'
                " was imported into the CCP-EM Pipeliner."
            ),
            "pipeline file": f"{pipeline_name}_pipeline.star",
            "date created": date_time_tag(),
        }
        return project_info

    # otherwise it is a new project
    return None


class PipelinerProject(object):
    """This class forms the basis for a project.

    Attributes:
        pipeline_name (str): The name of the pipeline. Defaults to `default` if
            not set. There is really no good reason to give the pipeline any other
            name.
    """

    def __init__(
        self,
        pipeline_name: str = "default",
        project_name: Optional[str] = None,
        description: Optional[str] = None,
        make_new_project: bool = False,
    ):
        """Create a PipelinerProject object

        Args:
            pipeline_name (str): The name for the pipeline. There is no good reason
                to use anything else except the default.
            project_name (str): A short descriptive name for the project, editable
                by the user. Will overwrite current if the project already exists
            description (str): A verbose description of the project, editable by the
                user. Will overwrite current if the project already exists
            make_new_project (bool): If an existing project is not found in the current
                directory, should a new one be created? If ``True``, a new empty project
                will be made. If ``False``, an error will be raised instead.

        Raises:
            FileNotFoundError: If no existing project is found and ``make_new_project``
                is ``False``.
        """
        # Get the project info. For a RELION project, this will create a new Pipeliner
        # project info file. If a pipeliner project already exists in the current
        # directory, the pipeline name specified in the info file will be used instead
        # of ``pipeline_name``.
        project_info = look_for_project(pipeline_name)

        if project_info is None:
            if make_new_project:
                project_info = self._make_new_project(pipeline_name)
            else:
                # This should not happen. If it does, either the current working
                # directory is wrong, or we are in a situation where it really does make
                # sense to make a new project and the make_new_project flag should have
                # been set.
                raise FileNotFoundError(
                    f"Could not find a project to open in directory {os.getcwd()} and"
                    f' "make_new_project" was not set'
                )

        self.pipeline_name = project_info["pipeline file"].replace("_pipeline.star", "")

        if project_name is not None:
            project_info["project name"] = project_name
        if description is not None:
            project_info["description"] = description

        project_info["last opened"] = date_time_tag()

        try:
            atomic_write_json(project_info, PROJECT_FILE, indent=2)
        except Exception:
            logger.warning("Could not write project info file", exc_info=True)

    @staticmethod
    def _make_new_project(pipeline_name):
        logger.info(f"Creating new project {pipeline_name}")
        # Make a new empty pipeline
        with ProjectGraph(name=pipeline_name, create_new=True):
            pass

        # relion related stuff
        # this will be deprecated when support for relion GUI is dropped

        # create the .gui_projectdir file so the gui knows it is initialized
        # this is for relion's benefit
        touch(".gui_projectdir")

        # Create and return default project info for a new project
        project_info = {
            "project name": "New project",
            "description": "A new CCP-EM pipeliner project",
            "pipeline file": f"{pipeline_name}_pipeline.star",
            "date created": date_time_tag(),
        }
        return project_info

    def parse_procname(self, in_proc: str, search_trash: bool = False) -> str:
        """Find process name with the ability for parse ambiguous input.

        Returns full process names IE: `Import/job001/` from `job001` or `1`
        Can look in both active processes and the Trash Can, accepts
        inputs containing only job number and process type and alias
        IE `Import/my_alias`

        Args:
            in_proc (str): The text that is being checked against the list of processes
            search_trash (bool): Should it return the process name if the process is in
                the trash?

        Returns:
            str: the process name


        Raises:
            ValueError: if the process was in the trash but search_trash is false
            ValueError: if the process name is not in the pipeliner format, jobxxx,
                or a number.  IE: An unrelated string
            ValueError: if the process name is not found

        """
        # first see if the process is in the pipeline or trash
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            in_proc = in_proc.replace(" ", "")
            if in_proc[-1] != "/":
                in_proc += "/"
            found_process = pipeline.find_process(in_proc)

            if found_process:
                return found_process.name

            trashprocs = glob("Trash/*/job*/")
            if in_proc[0:6] == "Trash/":
                in_proc = in_proc[6:]
            if "Trash/" + in_proc in trashprocs:
                found_it = in_proc
                if search_trash:
                    return found_it
                raise ValueError(
                    f"ERROR: Job {found_it} is in the trash. Trashed jobs cannot"
                    " be used for this function.  If you wish to use this job, "
                    "undelete it first"
                )

            # if the user entered just job number IE: job005 or just a number
            in_jobnr = decompose_pipeline_filename(in_proc)[1]
            if not in_jobnr:
                try:
                    in_jobnr = int(in_proc.replace("/", ""))
                except ValueError:
                    raise ValueError(
                        f"ERROR: Could not parse the process name {in_proc}"
                    )

            for pipeproc in pipeline.process_list:
                if pipeproc.job_number == in_jobnr:
                    return pipeproc.name
            for trashproc in trashprocs:
                tp_jobnr = decompose_pipeline_filename(trashproc)[1]
                if tp_jobnr == in_jobnr:
                    if search_trash:
                        return trashproc[6:]
                    raise ValueError(
                        f"ERROR: Job {found_process.name if found_process else None} is"
                        " in the trash. Trashed jobs cannot"
                        " be used for this function.  If you wish to use this job,"
                        " undelete it first"
                    )

            raise ValueError(
                f"ERROR: {in_proc} does not seem to be a job in this project"
            )

    def parse_proclist(self, list_o_procs: list, search_trash: bool = False) -> list:
        """Finds full process names for multiple processes

        Returns full process names IE: `Import/job001/` from `job001` or `1`

        Args:
            list_o_procs (list): A list of string process names
            search_trash (bool): Should the trash also be search?

        Returns:
            list: All of the full process names

        """
        checked = [self.parse_procname(proc, search_trash) for proc in list_o_procs]
        return checked

    def run_cleanup(self, jobs: list, harsh: bool = False) -> bool:
        """Run the cleanup function for multiple jobs

        Each job defines its own method for cleanup and harsh cleanup

        Args:
            jobs (list): List of string job names to operate on
            harsh (bool): Should harsh cleaning be performed

        Returns:
            bool: ``True`` if cleanup is successful, otherwise ``False``

        """
        jobstring = ", ".join(jobs)
        logger.info(f"Running cleanup on: {jobstring}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            for job in jobs:
                job = clean_jobname(job)
                jobproc = pipeline.find_process(job)
                if jobproc is not None:
                    pipeline.clean_up_job(jobproc, harsh)
                elif jobproc is None:
                    logger.warning(f"Could not find job {job} to clean up")
                    return False
            pipeline.check_process_completion()
        return True

    def cleanup_all(self, harsh: bool = False):
        """Runs cleanup on all jobs in a project

        Args:
            harsh (bool): Should harsh cleaning be performed?
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.cleanup_all_jobs(harsh)
            pipeline.check_process_completion()

    def restore_from_cleanup(self, job: str) -> None:
        """Restore files from a job that were removed by the cleanup function

        Args:
            job (str): The name of the job to restore files from
        """
        logger.info(f"Restoring cleaned up files from {job} from the trash")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            proc = pipeline.find_process(job)
            pipeline.restore_cleaned_up_files(proc)

    def restore_all_cleaned_up_files(self) -> None:
        """Restore all cleaned up files back to their original jobs"""
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            for proc in pipeline.process_list:
                pipeline.restore_cleaned_up_files(proc)

    def delete_job(self, job: str) -> bool:
        """Delete a job

        Removes the job from the main project and moves it and its children
        it to the Trash

        Args:
            job (str): The name of the job to be deleted

        Returns:
            bool: ``True`` If a job was deleted, ``False`` if no jobs were deleted

        """
        job = clean_jobname(job)
        logger.info(f"Moving {job} to trash")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            jobproc = pipeline.find_process(job)
            if jobproc is not None:
                pipeline.delete_job(jobproc)
                logger.info(f"Job {job} and child processes were moved to the trash")
                pipeline.check_process_completion()
                return True
            else:
                logger.error(f"Could not find job {job} to delete")
        return False

    def undelete_job(self, job: str) -> bool:
        """Restores a job from the Trash back into the project

        Also restores the job's alias if one existed


        Args:
            job (str): The job to undelete

        Returns:
            bool: ``True`` If a job was restored, otherwise ``False``

        """
        job = clean_jobname(job)
        logger.info(f"Restoring {job} from trash")
        if not os.path.isdir(os.path.join("Trash/", job)):
            logger.error(f"Could not find job {job} in the trash, can't undelete it.")
            return False
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.undelete_job(job)
            pipeline.check_process_completion()
        return True

    def undelete_all_trashed_jobs(self) -> None:
        trashed_jobs = get_deleted_jobs_and_sizes().keys()
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            for job in trashed_jobs:
                pipeline.undelete_job(job)
            pipeline.check_process_completion()

    def set_alias(self, job: str, new_alias: Optional[str]):
        """Set the alias for a job

        Args:
            job (str): The name of the job to set the alias for
            new_alias (str):  The new alias

        Raises:
            ValueError: If the alias could not be set for any reason.
        """
        job = clean_jobname(job)
        logger.info(f"Setting alias of {job} to {new_alias}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            jobproc = pipeline.find_process(job)
            if jobproc is None:
                raise ValueError(f"ERROR: could not find job {job}")
            pipeline.set_job_alias(jobproc, new_alias)
            pipeline.check_process_completion()

    def update_job_status(self, job: str, new_status: str):
        """Mark a job as finished, failed or aborted

        If is_failed and is_aborted are both ``False`` the job is marked as
        finished.

        Args:
            job (str): The name of the job to update
            new_status (str): The new status for the job; Choose from `"Running"`,
                , `"Scheduled"`, `"Succeeded"`, `"Failed` or `"Aborted"`.
                Status names are not case sensitive

        Raises:
            ValueError: If the new status is not one of the options
        """
        statuses = (
            JOBSTATUS_ABORT,
            JOBSTATUS_RUN,
            JOBSTATUS_FAIL,
            JOBSTATUS_SUCCESS,
            JOBSTATUS_SCHED,
        )
        if new_status not in statuses:
            raise ValueError(
                f"{new_status} is not an available status.  Select from {statuses}"
            )
        job = clean_jobname(job)
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.check_process_completion()
            jobproc = pipeline.find_process(job)
            if not jobproc:
                return False
            pipeline.update_status(jobproc, new_status)

    def run_scheduled_job(
        self,
        job: PipelinerJob,
        run_in_foreground: bool = False,
    ):
        """Run a job that has been scheduled

        Args:
            job (PipelinerJob): The job to run
            run_in_foreground (bool): Should the job be run in the foreground instead
                of being spun off as a separate process, for testing
        """
        logger.info(f"Running scheduled job {job.output_dir}")
        with ProjectGraph(self.pipeline_name, read_only=False) as pipeline:
            proc = pipeline.find_process(job.output_dir)
            if not proc:
                raise ValueError(
                    f"Attempted to run a scheduled job called {job.output_dir}, but it "
                    "could not be found in the pipeline"
                )
            if proc.status != JOBSTATUS_SCHED:
                raise ValueError(
                    f"Attempted to run a scheduled job called {proc.name}, but its "
                    f"status is {proc.status} and must be {JOBSTATUS_SCHED}"
                )
            job_manager.run_scheduled_job(
                pipeline=pipeline,
                job=job,
                process=proc,
                run_in_foreground=run_in_foreground,
            )
            pipeline.check_process_completion()

        return job

    def run_job(
        self,
        jobinput: Union[str, dict, PipelinerJob],
        overwrite: Optional[str] = None,
        alias: Optional[str] = None,
        ignore_invalid_joboptions=False,
        run_in_foreground=False,
    ) -> PipelinerJob:
        """Run a new job in the project

        If a file is specified the job will be created from the parameters in that file
        If a dict is input the job will be created with defaults for all options except
        those specified in the dict.

        If a dict is used for input it MUST contain at minimum
        {"_rlnJobTypeLabel": <the jobtype>}

        Args:
            jobinput (str, dict, :class:`~pipeliner.pipeliner_job.PipelinerJob`):
                The path to a run.job or job.star file that defines the parameters
                for the job or a dict specifying job parameters or a
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object
            overwrite (str): The name of a job to overwrite, if ``None`` a new
                job will be created.  A job can only be overwritten by a job of the
                same type
            alias (str): Alias to assign to the new job
            ignore_invalid_joboptions (bool): Run the job anyway even if the job
                options appear to be invalid
            run_in_foreground (bool): Run job in the main process blocking anything
                else from happening until it completes

        Returns:
            str: The name of the job that was run

        Raises:
            ValueError: If this method is used to continue a job
            ValueError: If the job options are invalid and ``ignore_invalid_joboptions``
                is not set
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            # if the job is overwrite get the old job
            if overwrite is not None:
                overwrite_jobname = clean_jobname(overwrite)
                original_proc = pipeline.find_process(overwrite_jobname)
                target_proc = original_proc
            # otherwise make a new job
            else:
                target_proc = None

            # if input is dict
            if isinstance(jobinput, dict):
                job = job_from_dict(jobinput)
                runfrom = f"dict: {job.PROCESS_NAME}"
            elif isinstance(jobinput, PipelinerJob):
                job = jobinput
                runfrom = f"PipelinerJob: {job.PROCESS_NAME}"
            # if input is file
            elif os.path.isfile(jobinput):
                job = read_job(jobinput)
                runfrom = f"file: {job}"
            # if input is bad
            else:
                raise ValueError(
                    "Can't parse job input; use a job.star, run.job file or a dict "
                    "containing '_rlnJobTypeLabel': <job type> and other parameters"
                    " that will have non-default values"
                )
            logger.info(f"Running job from {runfrom}")

            if job.is_continue:
                raise ValueError(
                    "ERROR: The job file is for continuing a job "
                    "Use the --continue_job <job name> function instead.\n"
                    "To modify the parameters in a continued job edit the"
                    "continue_job.star file in its job directory"
                )

            job_manager.run_job(
                pipeline=pipeline,
                job=job,
                alias=alias,
                overwrite=target_proc,
                ignore_invalid_joboptions=ignore_invalid_joboptions,
                run_in_foreground=run_in_foreground,
            )
            pipeline.check_process_completion()

            return job

    def abort_job(self, job_name: str) -> None:
        """Abort a running job.

        This function signals to the job that it should abort, but does not wait for
        the job to respond.

        Args:
            job_name: The job name. This must be exact, use ``parse_procname`` to find
                the job if you need to find it from a partial name, number or alias.

        Raises:
            ValueError: If there is no job with the given name
            RuntimeError: If the job is in any state except Running
        """
        logger.info(f"Aborting {job_name}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_manager.abort_job(pipeline, job_name)

    def continue_job(
        self,
        job_to_continue: str,
        comment: Optional[str] = None,
        run_in_foreground=False,
    ) -> PipelinerJob:
        """Continue a job that has already been run

        To change the parameters in a continuation the user needs to edit
        the continue_job.star file in the job's directory

        Args:
            job_to_continue (str): The name of the job to continue
            comment (str): Comments for the job's jobinfo file

        Returns:
            :class:`~pipeliner.pipeliner_job.PipelinerJob`: The
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object for
                the created job

        Raises:
            ValueError: If the continue_job.star file is not found and there is no
                job.star file in the job's directory to use as a backup
            ValueError: If the job is of a type that needs a optimizer file to continue
                and this file is not found
            ValueError: The job has iterations but the parameters specified would
                result in no additional iterations being run
        """
        logger.info(f"Continuing job: {job_to_continue}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_to_continue = clean_jobname(job_to_continue)
            # find the job_to_continue
            curr_proc = pipeline.find_process(job_to_continue)
            if curr_proc is None:
                raise ValueError(
                    f"Asked to continue job {job_to_continue} but it does not exist"
                )

            # find and read the continuation file
            continue_file = os.path.join(curr_proc.name, "continue_job.star")
            if not os.path.isfile(continue_file):
                logger.warning(
                    f"Could not find the file {continue_file}.\nWill instead"
                    " use the job options in the original job.star file."
                )
                continue_options = {}
            else:
                continue_options = JobStar(continue_file).joboptions

            # find and read the original job.star
            original_options_file = os.path.join(curr_proc.name, "job.star")
            if not os.path.isfile(original_options_file):
                raise ValueError(
                    f"ERROR: The file {original_options_file} was not found. There is"
                    " an error with the job and it cannot be continued."
                )

            # make a dummy job to get a job options dict for writing the job.star
            # substitute in the continue options
            djob = read_job(original_options_file)
            djob.is_continue = True
            for jo in djob.joboptions:
                if jo in continue_options:
                    djob.joboptions[jo].value = continue_options[jo]

            # TO DO: remove relion specific code here move to individual jobs
            # check if an optimiser is needed; if so and missing, raise error..
            needs_optimiser = [
                CLASS2D_PARTICLE_NAME_EM,
                CLASS2D_PARTICLE_NAME_VDAM,
                CLASS2D_HELICAL_NAME_EM,
                CLASS2D_HELICAL_NAME_VDAM,
                INIMODEL_JOB_NAME,
                CLASS3D_PARTICLE_NAME,
                CLASS3D_HELICAL_NAME,
                REFINE3D_HELICAL_NAME,
                REFINE3D_PARTICLE_NAME,
                MULTIBODY_REFINE_NAME,
                MULTIBODY_FLEXANALYSIS_NAME,
            ]
            if curr_proc.type in needs_optimiser:
                optimiser_file = djob.joboptions["fn_cont"].get_string()
                if "_optimiser.star" not in optimiser_file:
                    raise ValueError(
                        "ERROR: No optimiser file was specified to continue from. Edit"
                        f" {continue_file} to list an optimiser.star file in the field"
                        " 'fn_cont'"
                    )

                # check that additional iterations have been added
                optifile = os.path.basename(optimiser_file)
                iternumber = int(optifile.split("_")[1].replace("it", ""))
                if djob.joboptions["nr_iter"].get_number() == iternumber:
                    raise ValueError(
                        "ERROR: The job has been continued from iteration {} but "
                        f"the specified number of iterations is {iternumber} so no"
                        "additional iterations will be run\nUpdate the 'nr_iter'"
                        " parameter in the continue_job.star file for this job."
                    )

            # rename the old job.star file
            jobstars = glob(os.path.join(curr_proc.name, "job.star.*"))
            if len(jobstars) > 0:
                jobstars.sort()
                last_ct_no = int(jobstars[-1].split(".")[-1].replace("ct", ""))
                ct_no = last_ct_no + 1
                new_name = f"{original_options_file}.ct{ct_no:03d}"
                shutil.move(original_options_file, new_name)
            else:
                shutil.move(original_options_file, f"{original_options_file}.ct000")

            # make a new jobstar file and run it
            djob.write_jobstar(curr_proc.name)
            job = read_job(original_options_file)
            job.output_dir = curr_proc.name
            os.remove(os.path.join(job.output_dir, SUCCESS_FILE))
            job_manager.run_job(
                pipeline=pipeline, job=job, run_in_foreground=run_in_foreground
            )
            pipeline.check_process_completion()

            return job

    def schedule_job(
        self,
        job_input: Union[str, Dict[str, Union[str, float, int, bool]]],
        comment: Optional[str] = None,
        alias: Optional[str] = None,
    ) -> PipelinerJob:
        """Schedule a job to run

        Adds the job to the pipeline with scheduled status, does not run it

        Args:
            job_input (str): The path to a run.job or job.star file that defines the
                parameters for the job or a dictionary containing job parameters
            comment (str): Comments to put in the job's jobinfo file
            alias (str): Alias to give to the job
        Returns:
            The :class:`~pipeliner.pipeliner_job.PipelinerJob` object for the scheduled
            job
        """
        logger.info(f"Scheduling job {job_input}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            # if input is dict
            if isinstance(job_input, dict):
                job = job_from_dict(job_input)

            # if input is file
            elif os.path.isfile(job_input):
                job = read_job(job_input)
            else:
                raise ValueError(f"Could not find job for {job_input}")

            if job.is_continue:
                raise ValueError(
                    "Do not use this function to continue jobs! Use"
                    "pipeliner.api.manage_project.schedule_continue_job()"
                )

            job_manager.schedule_job(pipeline=pipeline, job=job, alias=alias)

            # write the continuation file - for continuing the job later
            continue_name = os.path.join(job.output_dir, "continue_")
            job.write_jobstar(continue_name, is_continue=True)

            pipeline.check_process_completion()

            return job

    def schedule_continue_job(
        self,
        job_to_continue: str,
        params_dict: Optional[dict] = None,
        comments: Optional[str] = None,
    ) -> PipelinerJob:
        """Schedule a job to run

        Adds the job to the pipeline with scheduled status, does not run it

        Args:
            job_to_continue (str): the name of the job to continue
            params_dict (dict): Parameters to change in the continuation job.star
                file. {param name: value}
            comments (str): comments to add to the job's jobinfo file

        Returns:
            :class:`~pipeliner.pipeliner_job.PipelinerJob`: The
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object for the
                newly scheduled job
        """
        logger.info(f"Scheduling job continuation: {job_to_continue}")
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            old_proc = pipeline.find_process(job_to_continue)
            # if input is bad
            if old_proc is None:
                raise ValueError(f"No job named {job_to_continue} found")

            cont_file = os.path.join(old_proc.name, "continue_job.star")
            if params_dict is not None:
                js = JobStar(cont_file)
                js.modify(params_dict)
                js.write(cont_file)
            cont_options = JobStar(cont_file).joboptions
            main_file = os.path.join(old_proc.name, "job.star")
            run_dict = JobStar(main_file).joboptions
            for param in cont_options:
                run_dict[param] = cont_options[param]

            run_dict["_rlnJobTypeLabel"] = old_proc.type
            run_dict["_rlnJobIsContinue"] = "Yes"
            new_job = job_from_dict(run_dict)
            new_job.output_dir = old_proc.name

            # put a copy of the job file in the output directory
            new_job.write_jobstar(new_job.output_dir + "job.star")

            job_manager.schedule_job(pipeline=pipeline, job=new_job)

            pipeline.check_process_completion()

            return new_job

    def run_schedule(
        self,
        fn_sched: str,
        job_ids: List[str],
        nr_repeat: int = 1,
        minutes_wait: int = 0,
        minutes_wait_before: int = 0,
        seconds_wait_after: int = 5,
    ) -> str:
        """Runs a list of scheduled jobs

        Args:
            fn_sched (str): A name to assign to the schedule
            job_ids (list): A list of string job names to run
            nr_repeat (int): Number of times to repeat the entire schedule
            minutes_wait (int): Minimum number of minutes to wait between running
                each subsequent job
            minutes_wait_before (int): Initial number of minutes to wait before
                starting to run the schedules.
            seconds_wait_after (int): Time to wait after running each job

        Returns:
            str: The name of the schedule that is run

        Raises:
            ValueError: If the schedule name is already in use
        """
        runfile_name = "RUNNING_PIPELINER_" + self.pipeline_name + "_" + fn_sched
        if os.path.isfile(runfile_name):
            raise ValueError(
                f"ERROR: a file called {runfile_name} already exists. \n This "
                "implies another set of scheduled jobs with this name is already "
                "running. \n Cancelling job execution..."
            )

        logger.info(
            f"Created schedule: {fn_sched} with {len(job_ids)} jobs repeated "
            f"{nr_repeat} times"
        )

        logger.info("Running " + fn_sched)
        job_manager.run_schedule(
            fn_sched=fn_sched,
            job_ids=job_ids,
            nr_repeat=nr_repeat,
            minutes_wait=minutes_wait,
            minutes_wait_before=minutes_wait_before,
            seconds_wait_after=seconds_wait_after,
            pipeline_name=self.pipeline_name,
        )
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.check_process_completion()
        return fn_sched

    @staticmethod
    def empty_trash():
        """Deletes all the files and dirs in the Trash directory

        Returns:
            bool:``True`` if any files were deleted, ``False`` If no files were deleted

        """
        trash_files = glob("Trash/*/*/*")
        if len(trash_files) > 0:
            shutil.rmtree("Trash")
            logger.info(
                f"Trash has been emptied. {len(trash_files)} files were deleted"
            )
            return True
        else:
            logger.warning("Attempted to empty trash but no files were found")
            return False

    def stop_schedule(self, schedule_name: str) -> bool:
        """Stops a currently running schedule

        Kills the process running the schedule and marks the currently running
        job as aborted.  Works to stop schedules that were started using the
        RELION GUI or pipeliner.

        Args:
            schedule_name (str): The name of the schedule to stop

        Returns:
            bool: ``True`` If the schedule was stopped, ``False`` if the schedule could
            not be found to stop
        """
        # TODO: rework this now we have a proper job abort function. The job manager
        #  should watch for removal of its schedule lock file and be responsible for
        #  aborting its jobs and exiting cleanly
        # find RUNNING_PIPELINER_ file
        sched_info = []
        the_file = None
        running_files = glob("RUNNING*")
        for f in running_files:
            sname = f.replace("RUNNING_PIPELINER_", "").split("_")
            pipe = sname[0]
            name = "_".join(sname[1:])
            if name == schedule_name and pipe == self.pipeline_name:
                the_file = f
                with open(the_file) as run_file:
                    sched_info = run_file.readlines()

        # error if the schedule is not found
        if the_file is None:
            logger.error(
                "Tried to cancel running schedule but cannot find file"
                f" RUNNING_PIPELINER_{self.pipeline_name}_{schedule_name}\n"
                f"'{schedule_name}' does not appear to be a running schedule"
            )
            return False

        # if the schedule was started by the GUI delete the file
        if len(sched_info) == 0:
            os.remove(the_file)
            return True

        # if the schedule was started by the ccpem_pipeliner kill it and abort the job
        else:
            running_jobs = []
            for line in sched_info:
                if "RELION_SCHEDULE:" in line:
                    # kill the schedule id it was started by the pipeliner
                    sched_pid = line.split()[1].rstrip()
                    run_subprocess(["kill", "-9", sched_pid])
                else:
                    running_jobs.append(line)
            # abort the current running job
            if len(running_jobs) > 0:
                current_job = running_jobs[-1].rstrip()
                self.abort_job(current_job)

            # remove the file
            os.remove(the_file)
            return True

    def prepare_metadata_report(self, jobname: str) -> Tuple[str, int]:
        """Returns a full metadata trace for a job and all upstream jobs

        Args:
            jobname: The name of the job to run on

        Returns:
            tuple: (dict: str: file written, int:
                number of jobs in the report)
        """
        logger.info(f"Preparing metadata report for terminal job: {jobname}")
        self.add_missing_nodes_from_all_jobs()
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            proc = pipeline.find_process(jobname)
            if not proc:
                return "", 0
            metadata_dict, njobs = get_metadata_chain(pipeline, proc)
        os.makedirs(METADATA_REPORT_DIR, exist_ok=True)
        output_name = os.path.join(
            METADATA_REPORT_DIR, date_time_tag(compact=True) + "_metadata_report.json"
        )
        atomic_write_json(metadata_dict, output_name, indent=4)
        add_md_to_summary_data(output_name, jobname, njobs)
        return output_name, njobs

    def create_archive(self, job: str, full: bool = False, tar: bool = True) -> str:
        """Creates an archive

        Archives can be full or simple. Simple archives contain the directory
        structure of the project, the parameter files for each job and a script
        to rerun the project through the terminal job.  The full archive contains the
        full job dirs for the terminal job and all of its children

        Args:
            job (str): The name of the terminal job in the workflow
            full (bool): If ``True`` a full archive is written else a simple archive is
                written
            tar (bool): Should the newly written archive be compressed?

        Returns:
            str: A message telling the type of archive and its name

        """
        atype = "full" if full else "script"
        logger.info(f"Creating {atype} archive from terminal job {job}")
        self.add_missing_nodes_from_all_jobs()
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            process = pipeline.find_process(job)
            if not process:
                return ""
        if full:
            archive_name = prepare_full_archive(process, tar)
        else:
            archive_name = prepare_script_archive(process, tar)
        if tar:
            archive_name += ".tar.gz"
        add_archive_to_summary_file(
            archive_name, process.name, "full" if full else "simple"
        )
        return archive_name

    def edit_comment(
        self,
        job_name: str,
        comment: Optional[str] = None,
        overwrite: bool = False,
    ):
        """Edit the comment of a job

        Args:
            job_name (str): The name of the job to eddit the comment for
            comment (str): The comment to add/append
            overwrite (bool): if `True` overwrites otiginal comment, otherwise
                appends it to the current comment

        Raises:
            ValueError: If the new rank is not `None` or an integer
        """
        if comment is None:
            raise ValueError("Nothing to do! To clear comment use comment=[]")

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            the_job = pipeline.find_process(job_name)
            if not the_job:
                raise ValueError(
                    f"No process found in {self.pipeline_name} pipeline for {job_name}"
                )
            jobinfo_file = os.path.join(the_job.name, JOBINFO_FILE)
            with open(jobinfo_file, "r") as jof:
                job_data = json.load(jof)
            if comment is not None:
                if overwrite:
                    job_data["comments"] = [comment]
                else:
                    job_data["comments"].append(comment)
            atomic_write_json(job_data, jobinfo_file, indent=2)

    def find_job_by_comment(
        self,
        contains: Optional[List[str]] = None,
        not_contains: Optional[List[str]] = None,
        job_type: Optional[str] = None,
        command: bool = False,
    ) -> List[str]:
        """Find Jobs by their comments or command

        Args:
            contains (list): Find jobs that contain all of the strings in this list
            not_contains (list): Find jobs that do not contain any of these strings
            job_type (str): Only consider jobs who's type contain this string
            command (bool): If `True` searches the job's command history rather than
                its comments

        Returns:
            list: Names of all the jobs found

        Raises:
            ValueError: If nothing is specified for contains and not_contains
        """
        if contains is None and not_contains is None:
            raise ValueError("Must specifiy contains= or not_contains=")

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            jobs = pipeline.process_list
            job_commands, job_comments = {}, {}
            for job in jobs:
                if str(job_type) in job.type or job_type is None:
                    with open(os.path.join(job.name, JOBINFO_FILE), "r") as notefile:
                        jobinfo = json.load(notefile)
                        if jobinfo.get("comments") is not None:
                            job_comments[job] = jobinfo["comments"]
                        if "command_history" in jobinfo:
                            job_commands[job] = jobinfo["command_history"]

            sel_jobs = []

            def check_cnc(which_dict: dict) -> bool:
                combined = "\t".join(which_dict[job])
                if contains is not None:
                    in_contains = any([x in combined for x in contains])
                else:
                    in_contains = True

                if not_contains is not None:
                    reject = any([x in combined for x in not_contains])
                else:
                    reject = False

                if job_type is not None:
                    jt_match = job_type in job.type
                else:
                    jt_match = True

                if in_contains and not reject and jt_match:
                    return True
                else:
                    return False

            if command:
                for job in job_commands:
                    if check_cnc(job_commands):
                        sel_jobs.append(job.name)

            else:
                for job in job_comments:
                    if check_cnc(job_comments):
                        sel_jobs.append(job.name)

            return sel_jobs

    def compare_job_parameters(self, jobs_list: List[str]) -> dict:
        """Compare the running parameters of multiple jobs

        Args:
            jobs_list (list): The jobs to compare

        Returns:
            dict: {parameter: [value, value, value]}

        Raises:
            ValueError: If any of the jobs is not found
            ValueError: If the jobs being compared are not of the same type
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            jobs = []
            for job in jobs_list:
                j = pipeline.find_process(job)
                if j is None:
                    raise ValueError(f"job {job} is not a job in the current project")
                jobs.append(j)
            params_dicts = []
            for j in jobs:
                jfile = os.path.join(j.name, "job.star")
                params_dicts.append(JobStar(jfile).all_options_as_dict())

            jts = [x["_rlnJobTypeLabel"] for x in params_dicts]
            typecheck = len(set(jts))
            if typecheck != 1:
                raise ValueError(
                    "Jobs are not of the same type: "
                    f"{list(zip([x.name for x in jobs], jts))}"
                )
            out_dict = {}
            for param in params_dicts[0]:
                out_dict[param] = [params_dicts[0][param]]
                for other in params_dicts[1:]:
                    val = other[param]
                    out_dict[param].append(val)
            return out_dict

    def get_job(self, job_name: str) -> PipelinerJob:
        """Get an existing job from the project.

        Args:
            job_name (str): The name of the job to get

        Raises:
            ValueError: if the named job cannot be found
        """
        cleaned_job_name = clean_jobname(job_name)
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            proc = pipeline.find_process(cleaned_job_name)
            if proc:
                the_job = active_job_from_proc(proc)
            else:
                raise ValueError(f"Job {job_name} not found")
        return the_job

    def create_reference_report(self, terminal_job: str) -> Tuple[str, int]:
        """Create a report on all the references used in the project

        Args:
            terminal_job (str): use this job and all its parents
        Returns:
            tuple: (The name of the report created, number of jobs in report)
        """
        logger.info(f"Creating reference report with terminal job {terminal_job}")
        output_file = os.path.join(
            REFERENCE_REPORT_DIR, date_time_tag(compact=True) + "_reference_report.json"
        )
        njobs = prepare_reference_report(terminal_job, output_file)
        add_ref_report_to_summary_file(output_file, terminal_job, njobs)
        return output_file, njobs

    @staticmethod
    def prepare_deposition(
        terminal_job: str,
        depo_type: Literal["onedep", "empiar"],
        depo_id: Optional[str] = None,
        jobstar_file: Optional[str] = None,
        empiar_do_mov: bool = True,
        empiar_do_mics: bool = True,
        empiar_do_parts: bool = True,
        empiar_do_rparts: bool = True,
    ) -> str:
        """Prepare a deposition for EMPIAR, EMDB, or PDB databases

        Args:
            terminal_job (str): This job and all its parents will be included in the
                deposition
            depo_type (Literal["onedep", "empiar"]): 'ondep' is used for PDB and EMDB,
                'empir for EMPIAR'
            depo_id (Optional[str]): A name for the deposition
            jobstar_file: (Optional[str]): For EMPIAR; A job.star file that contains
                additional required information that cannot be gathered from the jobs
                themselves see
                ':class:~pipeliner.jobs.other.empiar_deposition_job.EmpiarDepositionJob'
                for the specifics
            empiar_do_mov (bool): For EMPIAR; should raw movies be included?
            empiar_do_mics (bool): For EMPIAR; Should corrected micrographs be included?
            empiar_do_parts (bool): For EMPIAR; Should particles be included?
            empiar_do_parts (bool): For EMPIAR; Should polished particles be included?

        Returns:
            str: The name path of the created archive
        """
        if depo_type == "empiar":
            out_file = prepare_empiar_deposition(
                terminal_job=terminal_job,
                jobstar_file=jobstar_file,
                do_movs=empiar_do_mov,
                do_mics=empiar_do_mics,
                do_parts=empiar_do_parts,
                do_rparts=empiar_do_rparts,
            )
        elif depo_type == "onedep":
            # temp message
            message = make_pretty_header(
                "The pdb/emdb deposition system is currently a prototype\n"
                "Only limited job types are included"
            )
            logger.warning(message)
            dep = OneDepDeposition(terminal_job=terminal_job)
            dep.prepare_deposition()
            out_file = dep.write_deposition_cif_file(depo_id=depo_id)
        else:
            raise ValueError("deposition type must be in ['empiar', 'onedep']")
        return out_file

    def add_missing_nodes_from_all_jobs(self) -> List[str]:
        """Add any missing nodes from all jobs in the pipeline.

        One reason why this might be necessary is that the Pipeliner makes more nodes
        than Relion, so if a pipeline is converted the missing nodes must be added.
        """
        with ProjectGraph(self.pipeline_name, read_only=False) as pipeline:
            old_nodes = set([x.name for x in pipeline.node_list])
            for proc in pipeline.process_list:
                job = active_job_from_proc(proc)

                # add missing input nodes
                job.create_input_nodes()
                for input_node in job.input_nodes:
                    pipeline.add_new_input_edge(input_node, proc)

                # add missing output nodes
                job.create_output_nodes()

                # job.output_nodes and proc.output_nodes both refer to the same list,
                # so we need to take a copy to stop `add_new_output_edge` getting stuck
                # in an infinite loop (because it adds the node unconditionally to the
                # end of proc.output_nodes and then this `for` loop picks it up on the
                # next iteration).
                # TODO: tidy this up by improving handling of duplicate nodes.
                for output_node in job.output_nodes.copy():
                    pipeline.add_new_output_edge(proc, output_node)

        new_nodes = set([x.name for x in pipeline.node_list])
        return list(new_nodes - old_nodes)


def convert_pipeline(pipeline_name: str = "default") -> bool:
    """Converts a pipeline file from the RELION 2.0-3.1 format

    This format has integer node, process, and status IDs.  The pipeliner format
    uses string IDs

    Args:
        pipeline_name (str): The name of the pipeline to be converted

    Returns:
        bool: The result of the conversion

        ``True`` if the pipeline was converted, ``False`` if the pipeline was
        already in pipeliner format

    """
    # update the pipeline
    been_converted = convert_old_relion_pipeline(f"{pipeline_name}_pipeline.star")

    # check the pipeline version if it's old style convert
    if been_converted:
        return True
    else:
        logger.warning(
            f"Tried to convert pipeline file {pipeline_name}_pipeline.star, but it is"
            " already in pipeliner format"
        )
        return False


def get_commands_and_nodes(job_file: str) -> tuple:
    """Tell what commands a job file would return and nodes that would be created

    Args:
        job_file (str): The path to a run.job or job.star file

    Returns:
        tuple:

        - A list of commands. Each item in the commands
          list is a list of commands arguments. IE:
          ``[[com1-arg1, com1-arg2],[com2-arg1]]``
        - A list of input nodes that would be created.  Each item in the list
          is a tuple: ``[(name, type), (name, type)]``
        - A list of output nodes that would be created. Each item in the list
          is a tuple: ``[(name, type), (name, type)]``
        - A list of any PipelinerWarning raised by joboption validation
        - A list of the ExternalProgram objects used by the job
    """
    is_project = look_for_project() is not None
    job = read_job(job_file)

    # validate the joboptions
    vres = job.validate_joboptions()
    errs = []
    for joe in vres:
        if joe.type == "error":
            errs.append(f"'{joe.raised_by[0].label}': {joe.message}")
    if errs:
        raise ValueError(
            "Job cannot generate command because of the following errors in the "
            f"job options: {', '.join(errs)}"
        )

    if is_project:
        with ProjectGraph() as pipeline:
            jobnumber = pipeline.job_counter
            job.output_dir = f"{job.OUT_DIR}/job{jobnumber:03d}/"
            commands = job.get_final_commands()

    else:
        job.output_dir = f"{job.OUT_DIR}/job000/"
        commands = job.get_final_commands()

    inputnodes, outputnodes = [], []

    job.create_input_nodes()
    if len(job.input_nodes) > 0:
        for innode in job.input_nodes:
            inputnodes.append((innode.name, innode.type))

    job.create_output_nodes()
    if len(job.output_nodes) > 0:
        for outnode in job.output_nodes:
            outputnodes.append((outnode.name, outnode.type))

    return commands, inputnodes, outputnodes, vres, job.jobinfo.programs


def delete_summary_data_reference_report(filename: str):
    """Remove a reference report from the summary data

    Args: filename (str): The name of the report file
    """
    logger.info(f"Deleting reference report: {filename}")
    remove_ref_report_from_summary_file(filename)
    if os.path.isfile(filename):
        os.remove(filename)


def delete_summary_data_metadata_report(filename: str):
    """Remove a metadata report from the summary data

    Args: filename (str): The name of the report file
    """
    logger.info(f"Deleting metadata report: {filename}")

    remove_md_from_summary_data(filename)
    if os.path.isfile(filename):
        os.remove(filename)


def delete_summary_data_archive(filename: str):
    """Remove an archive from the summary data

    Args: filename (str): The name of the archive zip file or dir
    """

    remove_from_summary_file("project_archives", filename)
    if os.path.isfile(filename):
        os.remove(filename)
    elif os.path.isdir(filename):
        shutil.rmtree(filename)


def get_ref_reports_from_summary_file() -> Tuple[List[str], List[List[str]]]:
    """Get a list of the reference reports in the summary file

    Returns:
        tuple: ([column, headers], [[line, 1, data], [line, 2, data]]
    """
    return get_summary_file_data("reference_reports")


def get_metadata_reports_from_summary_file() -> Tuple[List[str], List[List[str]]]:
    """Get a list of the metadata reports in the summary file

    Returns:
        tuple: ([column, headers], [[line, 1, data], [line, 2, data]]
    """
    return get_summary_file_data("metadata_reports")


def get_archives_list_from_summary_file() -> Tuple[List[str], List[List[str]]]:
    """Get a list of the reference reports in the summary file

    Returns:
        tuple: ([column, headers], [[line, 1, data], [line, 2, data]]
    """
    return get_summary_file_data("project_archives")


def get_deleted_jobs_and_sizes() -> Dict[str, str]:
    """Get all the jobs that have been moved to the trash and their sizes

    Returns:
        Dict[str, str]: The names of the trashed jobs and the sizes of their
        deleted directories.
    """
    all_trashed = Path(TRASH_DIR).glob("*/*")
    deleted = [str(x) for x in all_trashed if not str(x).startswith(CLEANUP_DIR)]
    formatted = [clean_jobname(x.replace(TRASH_DIR, "")) for x in deleted]
    formatted.sort(key=get_job_number)

    return {
        x: make_pretty_size(get_directory_size(Path(TRASH_DIR) / x)) for x in formatted
    }


def get_cleaned_up_jobs_and_sizes(show_deleted: bool = False) -> Dict[str, str]:
    """Get names and sizes of all files that have been removed by cleanup

    Args:
        show_deleted (bool): Show cleaned files from deleted jobs. Should be false if
            it is for display as these files cannot be restored if the job is in trash
    Returns:
        Dict[str, str]: {filename: size} for each file
    """
    all_cleaned = Path(CLEANUP_DIR).glob("*/*")
    formatted = [clean_jobname(str(x).replace(CLEANUP_DIR, "")) for x in all_cleaned]

    if not show_deleted:
        deleted = get_deleted_jobs_and_sizes()
        final_cleaned = [x for x in formatted if x not in deleted.keys()]
    else:
        final_cleaned = formatted

    final_cleaned.sort(key=get_job_number)

    return {
        x: make_pretty_size(get_directory_size(Path(CLEANUP_DIR) / x))
        for x in final_cleaned
    }


def delete_trashed_job_files(job) -> None:
    """Delete files in the trash for a specific job

    If the job is not found, a message is printed and nothing else happens.

    Args:
        job (str): The job name; WITHOUT the trash dir prepended
    """
    logger.info(f"Deleting {job} from the trash")
    job = clean_jobname(job)
    trashed = Path(TRASH_DIR) / job
    if not trashed.is_dir():
        logger.warning(f"{job} was not found in the trash, cannot delete")
        return

    shutil.rmtree(trashed)

    # also delete cleaned up files for this job
    cleaned = Path(CLEANUP_DIR) / job
    if cleaned.is_dir():
        shutil.rmtree(cleaned)


def delete_all_trashed_job_files() -> None:
    """Delete all files the jobs that have been moved to the trash"""
    logger.info("Emptying all deleted jobs from trash")
    trashed = get_deleted_jobs_and_sizes().keys()
    for tdir in trashed:
        delete_trashed_job_files(tdir)


def delete_cleaned_job_files(job) -> None:
    """Delete files in the trash from a cleaned up job

    If the job is not found, a message is printed and nothing else happens.

    Args:
        job (str): The job name; WITHOUT the cleanup dir prepended
    """
    logger.info(f"Removing all cleaned up files from {job} from trash")
    job = clean_jobname(job)
    cleaned = Path(CLEANUP_DIR) / job
    if not cleaned.is_dir():
        logger.warning(f"No cleaned up files found for {job}, cannot delete any files")
        return
    shutil.rmtree(cleaned)


def delete_all_cleaned_job_files() -> None:
    """Delete all files the jobs that have been moved to the trash"""
    logger.info("Deleting all cleaned up job files from trash")
    cleaned = get_cleaned_up_jobs_and_sizes(show_deleted=True).keys()
    for tdir in cleaned:
        delete_cleaned_job_files(tdir)
