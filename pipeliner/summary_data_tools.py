#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from gemmi import cif
from typing import List, Tuple
from pipeliner.starfile_handler import DataStarFile

SUMMARY_DATA_FILE = "pipeliner_summary_data.star"
SUMMARY_DATA_HEADERS = {
    "metadata_reports": ["MetadataReportName", "InitiatedFrom", "NumberOfJobs"],
    "project_archives": ["ArchiveName", "InitiatedFrom", "ArchiveType"],
    "reference_reports": ["ReferenceReportName", "InitiatedFrom", "NumberOfJobs"],
}


def create_new_summary_file():
    """Create a new summary file

    Can't be done with Gemmi because it refuses to write empty blocks
    """
    with open(SUMMARY_DATA_FILE, "w") as f:
        f.write("data_metadata_reports\ndata_project_archives\ndata_reference_reports")


def get_summary_file() -> cif.Document:
    """Get the summary file as a cif.Document

    Any missing blocks are created

    Returns:
        cif.Document: The Document obj for the summer file

    """
    if not os.path.isfile(SUMMARY_DATA_FILE):
        create_new_summary_file()
    sumfile = cif.read_file(SUMMARY_DATA_FILE)
    for bl in SUMMARY_DATA_HEADERS:
        if not sumfile.find_block(bl):
            sumfile.add_new_block(bl)
    return sumfile


def get_loop_for_updating(block) -> Tuple[cif.Loop, cif.Document]:
    """Get a loop from the summary daya file

    Args:
        block (str): which block to get the loop for

    Returns:
        tuple: (The Loop object, the cif Document object)
    """
    sumfile = get_summary_file()
    blindex = None
    for n, b in enumerate(sumfile):
        if b.name == block:
            blindex = n
    if blindex is not None:
        del sumfile[blindex]
    updated_block = sumfile.add_new_block(block)
    updated_loop = updated_block.init_loop("_pipeliner", SUMMARY_DATA_HEADERS[block])
    return updated_loop, sumfile


def add_to_summary_file(block: str, add_line: List[str]):
    """Add a file to the summary data table

    Args:
        block (str): which block to add it to
        line List[str]: the line to add
    """
    updated_loop, sumfile = get_loop_for_updating(block)
    try:
        data = DataStarFile(SUMMARY_DATA_FILE).loop_as_list(block)
    except ValueError:
        data = []

    found = False
    for line in data:
        if line[0] != add_line[0]:
            updated_loop.add_row(line)
        else:
            found = True
            updated_loop.add_row(add_line)
    if not found:
        updated_loop.add_row(add_line)

    sumfile.write_file(SUMMARY_DATA_FILE)


def remove_from_summary_file(block: str, filename: str):
    """Remove a file from the summary file

    Args:
        block (str): which block to remove it from
        filename (str): name of the file to remove
    """
    updated_loop, sumfile = get_loop_for_updating(block)
    data = DataStarFile(SUMMARY_DATA_FILE).loop_as_list(block)
    for line in data:
        if line[0] != filename:
            updated_loop.add_row(line)

    sumfile.write_file(SUMMARY_DATA_FILE)


def get_summary_file_data(block) -> Tuple[List[str], List[List[str]]]:
    """Get data from the summary file

    Args:
        block (str): which block to get

    Returns:
        Tuple[List[str], List[List[str]]]: The headers and the data lines
            ([Header, names], [[line1, data], ... [lineN, data]])
    """
    if not os.path.isfile(SUMMARY_DATA_FILE):
        return SUMMARY_DATA_HEADERS[block], [[]]
    sumdat = DataStarFile(SUMMARY_DATA_FILE)
    try:
        raw = sumdat.loop_as_list(block, headers=True)
    except ValueError:
        return SUMMARY_DATA_HEADERS[block], [[]]
    heads = [x.replace("_pipeliner", "") for x in raw[0]]
    lines = raw[1:]
    return heads, lines
