#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shlex
import shutil
import time
import traceback
import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Optional
from time import sleep
from pipeliner.project_graph import ProjectGraph
from pipeliner import job_factory
from pipeliner.utils import (
    date_time_tag,
    launch_detached_process,
    get_job_runner_command,
    update_jobinfo_file,
)
from pipeliner.process import Process
from pipeliner.data_structure import (
    SUCCESS_FILE,
    JOBSTATUS_RUN,
    JOBSTATUS_SCHED,
    JOBSTATUS_FAIL,
    ABORT_TRIGGER,
    JOBSTATUS_ABORT,
    FAIL_FILE,
    ABORT_FILE,
    JOBSTATUS_SUCCESS,
    JOBINFO_FILE,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.scripts import job_runner

OUTPUT_NODE_MICS = f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
OUTPUT_NODE_PARTS = f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
logger = logging.getLogger(__name__)


def schedule_job(
    pipeline: ProjectGraph,
    job: PipelinerJob,
    alias: Optional[str] = None,
    overwrite: Optional[Process] = None,
    ignore_invalid_joboptions=False,
) -> Process:
    """Schedule a job to run later.

    Note that jobs must be scheduled before they can be run.

    The job will be added to the pipeline in the Scheduled state, and the job's options
    will be written to "run.job" and "job.star" files in the job directory.

    If the job is new, it will be assigned a job number and a new output directory will
    be created for it. If the job is a continuation (i.e. ``job.is_continue`` is True)
    the previous job directory and number will be used. If ``overwrite`` is given, the
    job in ``overwrite.name`` will be deleted and replaced with the new job (as long as
    the old and new jobs are of the same type).

    If any of the job's input files are in the project "DoppioUploads" directory, they
    will be moved into an "InputFiles" directory inside the job directory, and the
    relevant job options will be updated to point to the new file locations.

    Args:
        pipeline: A writable (i.e. non-read-only) ``ProjectGraph`` object to add the new
            job to.
        job: The job to schedule and add to the pipeline.
        alias: An optional alias for the job, which can be used to refer to the job
            instead of its number.
        overwrite: An optional Process object representing an old job (of the same type)
            to be overwritten and replaced by the new job.
        ignore_invalid_joboptions (bool): Schedule the job anyway even if the job
            options appear to be invalid.

    Returns:
        The Process object representing the newly-scheduled job.

    Raises:
        ValueError: If ``overwrite`` is given, but the old job is a different type or
            other processes in the pipeline have used its outputs.
        ValueError: If ``job.is_continue`` is True but the job does not already have an
            output directory assigned.
        ValueError: If ``ignore_invalid_joboptions`` is False and the job options appear
            to be invalid.
        RuntimeError: If ``pipeline.read_only`` is True.
    """
    # First ensure we have a writable (i.e. locked) pipeline. It will need to be written
    # later anyway, but we check up front to avoid the risk of duplicate job numbers.
    pipeline._check_writable()
    # Pick a job directory
    if overwrite is not None:
        child_procs = pipeline.find_immediate_child_processes(overwrite)
        if child_procs:
            raise ValueError(
                f"Cannot overwrite job {overwrite.name} because other jobs have used it"
                " as an input"
            )
        job_dir = Path(overwrite.name)
        if job_dir.parts[0] != job.OUT_DIR:
            raise ValueError(
                f"Cannot overwrite job {overwrite.name} because the new job type"
                f" ({job.OUT_DIR}) is not the same as the previous one"
                f" ({job_dir.parts[0]})"
            )
        overwrite_proc = True
        # We can overwrite, so remove the old job directory completely
        # (but preserve the job info file)
        jif = job_dir / JOBINFO_FILE
        try:
            jobinfo = jif.read_bytes()
        except FileNotFoundError:
            pass
        shutil.rmtree(job_dir)
        try:
            job_dir.mkdir()
            jif.write_bytes(jobinfo)
        except Exception as ex:
            logger.error(f"Error writing job info file {jif}: {ex}")
    elif job.is_continue:
        # Continuation job should already have its job directory set
        if not job.output_dir:
            raise ValueError("Cannot continue a job if its output_dir is not set")
        job_dir = Path(job.output_dir)
        overwrite_proc = True
    else:
        # New job; assign a new job directory
        job_dir = Path(job.OUT_DIR) / f"job{pipeline.job_counter:03}"
        overwrite_proc = False

    # Prepare the job to run. This creates the job directory and writes the run.job and
    # job.star files
    job.output_dir = os.fspath(job_dir) + "/"
    job.prepare_to_run(ignore_invalid_joboptions=ignore_invalid_joboptions)

    # Add the job and its nodes to the pipeline
    process = pipeline.add_job(
        job=job, as_status=JOBSTATUS_SCHED, do_overwrite=overwrite_proc, alias=alias
    )

    # Update the job info file
    action = "Scheduled"
    if overwrite is not None:
        action += " overwrite last run"
    if job.is_continue:
        action += " continuation"
    update_jobinfo_file(process.name, action=action)

    # Put a back up copy of the pipeline in the job directory
    pipeline._write()
    fn_pipe = pipeline.star_file
    if os.path.isfile(fn_pipe):
        shutil.copy(fn_pipe, job.output_dir)

    return process


def run_scheduled_job(
    pipeline: ProjectGraph,
    job: PipelinerJob,
    process: Process,
    run_in_foreground: bool = False,
) -> Process:
    """Run a job that has already been scheduled.

    By default, the job will be launched in the background in a detached process. If
    necessary (mainly for testing) the job can instead be run in the foreground by
    passing ``run_in_foreground=True``. Note that this option overrides the job's
    queue submission settings.

    Args:
        pipeline: A writable (i.e. non-read-only) ``ProjectGraph`` pipeline containing
            the scheduled job.
        job: The job to run.
        process: The Process object representing the job's entry in the pipeline.
        run_in_foreground: If True, run the job in the foreground and do not return
            until it has finished. If False, the job will be launched in the background
            and this function will return immediately. This option is mainly intended
            for testing of the pipeliner. Note that if ``run_in_foreground`` is True,
            the project will be kept locked by this process for the entire duration of
            the job, preventing any other actions happening in the project in the
            meantime.

    Returns:
        The Process object representing the job. Note that this might be a new and
        different object from the one passed in to the ``process`` argument.

    Raises:
        ValueError: If the job is not already present in the pipeline in the Scheduled
            state.
        FileNotFoundError: If the job's "job.star" file cannot be found in the job
            directory.
        RuntimeError: If ``pipeline.read_only`` is True.
    """
    if process.status != JOBSTATUS_SCHED:
        raise ValueError(
            f"Cannot run job {job.output_dir} because it has not been scheduled"
        )
    # Change the status to running
    process.status = JOBSTATUS_RUN
    process = pipeline.add_process(process=process, do_overwrite=True)
    update_jobinfo_file(process.name, action="Run")

    job_star_path = Path(job.output_dir) / "job.star"

    if run_in_foreground:
        # Call the job runner in this process
        job_runner.run_job(job_star_path)
        # After the job has finished, update the status in the pipeline STAR file
        pipeline.check_process_completion()
    else:
        try:
            # Launch the job runner in a separate process and return immediately
            job_run_command = get_job_runner_command()
            job_run_command.append(os.fspath(job_star_path))

            if job.is_submit_to_queue():
                qsub_command = job.joboptions["qsub"].get_string()
                qsub_script = job.save_job_submission_script([job_run_command])
                launch_command = [qsub_command, qsub_script]
                timeout_secs = 5
            else:
                launch_command = job_run_command
                timeout_secs = 1

            rc = launch_detached_process(launch_command, timeout=timeout_secs)
            # Raise an exception if return code is not None or zero
            if rc:
                raise RuntimeError(
                    f"Command '{shlex.join(launch_command)}'"
                    f" failed with return code {rc}"
                )
        except Exception as ex:
            process.status = JOBSTATUS_FAIL
            pipeline.add_process(process=process, do_overwrite=True)
            raise RuntimeError(f"Error starting job {job.output_dir}") from ex
    return process


def run_job(
    pipeline: ProjectGraph,
    job: PipelinerJob,
    alias: Optional[str] = None,
    overwrite: Optional[Process] = None,
    ignore_invalid_joboptions=False,
    run_in_foreground=False,
) -> Process:
    """Run a job.

    The job will first be scheduled by passing it to ``schedule_job`` and then run by
    calling ``run_scheduled_job``. See those functions for more details.

    Args:
        pipeline: A writable (i.e. non-read-only) ``ProjectGraph`` object to add the new
            job to.
        job: The job to run.
        alias: An optional alias for the job, which can be used to refer to the job
            instead of its number.
        overwrite: An optional Process object representing an old job (of the same type)
            to be overwritten and replaced by the new job.
        ignore_invalid_joboptions (bool): Run the job anyway even if the job options
            appear to be invalid.
        run_in_foreground: If True, run the job in the foreground and do not return
            until it has finished. If False, the job will be launched in the background
            and this function will return immediately. This option is mainly intended
            for testing of the pipeliner. Note that if ``run_in_foreground`` is True,
            the project will be kept locked by this process for the entire duration of
            the job, preventing any other actions happening in the project in the
            meantime.

    Returns:
        The Process object representing the job.

    Raises:
        ValueError: If ``overwrite`` is given, but the old job is a different type or
            other processes in the pipeline have used its outputs.
        ValueError: If ``job.is_continue`` is True but the job does not already have an
            output directory assigned.
        ValueError: If ``ignore_invalid_joboptions`` is False and the job options appear
            to be invalid.
        ValueError: If ``run_in_foreground`` is True and the job is set to be
            submitted to a queue.
        RuntimeError: If ``pipeline.read_only`` is True.
    """
    # Before doing anything, check that the arguments make sense
    if job.is_submit_to_queue() and run_in_foreground:
        raise ValueError("Cannot wait for completion when submitting jobs to a queue")

    # Validate that the input files exist.  This can't be done at the JobOption level
    # because jobs should still be able to be scheduled before their inputs are created
    file_errors = job.validate_input_files()
    if file_errors:
        err_string = "\n".join([x.message for x in file_errors])
        err_msg = f"Job cannot be run because:\n{err_string}"
        raise ValueError(err_msg)

    # First, add the job to the pipeline in scheduled state
    process = schedule_job(
        pipeline=pipeline,
        job=job,
        alias=alias,
        overwrite=overwrite,
        ignore_invalid_joboptions=ignore_invalid_joboptions,
    )

    # Now run the scheduled job
    process = run_scheduled_job(
        pipeline=pipeline,
        job=job,
        process=process,
        run_in_foreground=run_in_foreground,
    )
    return process


def abort_job(pipeline: ProjectGraph, job_name: str) -> None:
    """Abort a running job.

    This function writes a file to signal that the job should abort, but does not wait
    for the job to respond.

    Args:
        pipeline: A writable (i.e. non-read-only) ProjectGraph object
        job_name: The job name to abort (including a trailing slash "/")

    Raises:
        ValueError: If there is no job with the given name
        RuntimeError: If the job is in any state except Running
    """
    process = pipeline.find_process(job_name)
    if process is None:
        raise ValueError(f"Could not find a job with name {job_name}")
    if process.status != JOBSTATUS_RUN:
        raise RuntimeError(
            f"Only running jobs can be aborted. Job {job_name} is {process.status}"
        )
    (Path(process.name) / ABORT_TRIGGER).touch()


def run_schedule(
    fn_sched: str,
    job_ids: Optional[List[str]] = None,
    nr_repeat: int = 1,
    minutes_wait: int = 0,
    minutes_wait_before: int = 0,
    seconds_wait_after: int = 0,
    pipeline_name: str = "default",
) -> None:
    """Run the jobs in a schedule.

    Args:
        fn_sched (str): The name to be assigned to the schedule
        job_ids (list): A list of :class:`str` job names
        nr_repeat (int): Number of times to repeat the entire schedule
        minutes_wait (int): Minimum time to wait between jobs in minutes.  If this
            has been passed whilst the job is running the next job will start
            immediately
        minutes_wait_before (int): Wait this amount of time before initially
            starting to run the schedule
        seconds_wait_after (int): Wait this many seconds before starting each job
            this wait always occurs, even if the minimum time between jobs has
            already been surpassed
        pipeline_name (str): The name of the pipeline to use

    Raises:
        ValueError: If a schedule lock file exists with the selected schedule name,
            suggesting another schedule with the same name is already running
        ValueError: If the job directory for a scheduled job could not be found
        ValueError: (If a job.star file is not found in one of the directories of a
            job to be run
        ValueError: If an input node for a job cannot be found
        ValueError: If a job in the schedule fails
    """
    runner = ScheduleRunner(
        fn_sched=fn_sched, pipeline_name=pipeline_name, job_ids=job_ids
    )
    runner.run(
        nr_repeat=nr_repeat,
        minutes_wait=minutes_wait,
        minutes_wait_before=minutes_wait_before,
        seconds_wait_after=seconds_wait_after,
    )


class ScheduleRunner:
    """Class to manage running of a schedule.

    Users should generally call ``run_schedule()`` rather than using this class
    directly.
    """

    def __init__(
        self,
        fn_sched: str,
        pipeline_name: str = "default",
        job_ids: Optional[List[str]] = None,
    ):
        self.schedule_name = fn_sched
        self.schedlog = f"pipeline_{fn_sched}.log"
        self.pipeline_name = pipeline_name
        self.sched_lock_name = f"RUNNING_PIPELINER_{pipeline_name}_{fn_sched}"
        self.sched_lock = Path(self.sched_lock_name)

        # make sure there are jobs to run
        if not job_ids or len(job_ids) == 0:
            raise ValueError("ERROR: run_schedule: Nothing to do...")
        self.job_ids = job_ids

    def write_to_sched_log(self, message: str):
        """Write a message to the schedule log file."""
        with open(self.schedlog, "a+") as schedlog:
            print(message, file=schedlog)

    def schedule_fail(self, message: str):
        """Write to the log and then delete the schedule lock file."""
        self.write_to_sched_log(f"+ {date_time_tag()}")
        self.write_to_sched_log(message)
        self.sched_lock.unlink()
        logger.error(message)

    def run(
        self,
        nr_repeat: int = 1,
        minutes_wait: int = 0,
        minutes_wait_before: int = 0,
        seconds_wait_after: int = 0,
    ):

        # make the schedule lock file
        if self.sched_lock.is_file():
            raise ValueError(
                f"ERROR: a file called {self.sched_lock_name} already exists.\n This"
                " implies another set of scheduled jobs with this name is already"
                " running.\n Cancelling schedule execution..."
            )

        # touch the schedule lockfile
        self.sched_lock.touch()

        try:
            # write the PID to schedule control file
            proc_pid = os.getpid()
            with open(self.sched_lock, "a") as runfile:
                runfile.write("RELION_SCHEDULE: {}\n".format(proc_pid))

            self.write_to_sched_log("+" * 35)
            self.write_to_sched_log(
                f"Starting a new scheduler execution called {self.schedule_name}"
            )
            self.write_to_sched_log("The scheduled jobs are:")

            for jobname in self.job_ids:
                self.write_to_sched_log(f"- {jobname}")

            if nr_repeat > 1:
                self.write_to_sched_log(
                    f"Will execute the scheduled jobs {nr_repeat} times"
                )
                self.write_to_sched_log(
                    f"Will wait until at least {minutes_wait} minute(s) have passed"
                    " between each repeat"
                )
            self.write_to_sched_log(
                f"{self.sched_lock_name} will be used to control the schedule"
            )
            self.write_to_sched_log("+" * 35)

            if minutes_wait_before > 0:
                time.sleep(minutes_wait_before * 60)

            for repeat in range(int(nr_repeat)):
                failed = self.run_repeat(
                    repeat=repeat,
                    nr_repeat=nr_repeat,
                    minutes_wait=minutes_wait,
                    seconds_wait_after=seconds_wait_after,
                )
                if failed:
                    return

            self.write_to_sched_log("+ " + date_time_tag())
            self.write_to_sched_log(
                f"+ Performed all requested repeats in schedule {self.schedule_name}."
                " Stopping pipeliner now ..."
            )
            self.write_to_sched_log("+" * 35)
        except BaseException as ex:
            try:
                traceback.print_exc()
                self.write_to_sched_log("ERROR: Unexpected error running schedule")
                self.write_to_sched_log(str(ex))
            finally:
                raise ex
        finally:
            self.sched_lock.unlink(missing_ok=True)

    def run_repeat(
        self, repeat: int, nr_repeat: int, minutes_wait: int, seconds_wait_after: int
    ):
        repeat_start = datetime.now()
        self.write_to_sched_log("+ " + date_time_tag())
        self.write_to_sched_log(f"-- Starting repeat {repeat + 1}/{nr_repeat}")

        for job_name in self.job_ids:
            self.write_to_sched_log("+ " + date_time_tag())
            self.write_to_sched_log(f"---- Executing {job_name}")

            with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
                pipeline.check_process_completion()
                current_proc = pipeline.find_process(job_name)
                if current_proc is None:
                    self.schedule_fail(
                        f"ERROR: Cannot find process with name/alias: {job_name},"
                        " schedule terminated"
                    )
                    return

                # use the job.star files, only use run.job if not available
                jobstar = Path(job_name) / "job.star"
                runjob = Path(job_name) / "run.job"
                job_file = str(jobstar) if jobstar.is_file() else str(runjob)
                try:
                    myjob = job_factory.read_job(job_file)
                    myjob.output_dir = current_proc.name
                except ValueError:
                    traceback.print_exc()
                    self.schedule_fail(
                        "ERROR: there was an error reading job:"
                        f" {current_proc.name}, schedule terminated"
                    )
                    return
                # if certain jobs are repeats they should always be continued
                if myjob.always_continue_in_schedule and repeat >= 1:
                    myjob.is_continue = True
                    myjob.write_jobstar(myjob.output_dir)
                # check for input nodes before executing job
                for curr_node in current_proc.input_nodes:
                    node_job = os.path.dirname(curr_node.name)
                    waiting_for_node = 0
                    while pipeline.find_node(curr_node.name) is None:
                        logger.warning(
                            f"Node {curr_node.name} does not exist,"
                            " waiting 1 second...",
                        )
                        time.sleep(1)
                        waiting_for_node += 1
                        if waiting_for_node > 600:
                            self.schedule_fail(
                                "ERROR: Waited 10 minutes for node"
                                f" {curr_node.name} to appear after job {node_job}"
                                " finished successfully, but it never did. Could"
                                " there be a file system issue?"
                            )
                            return
                # write current job to lock file, in case it needs to be cancelled
                with open(self.sched_lock, "a") as sched_lockfile:
                    sched_lockfile.write(current_proc.name + "\n")

                # start the job, then release the pipeline lock while it runs
                try:
                    run_scheduled_job(
                        pipeline=pipeline,
                        job=myjob,
                        process=current_proc,
                        run_in_foreground=True,
                    )
                except ValueError:
                    traceback.print_exc()
                    self.schedule_fail(
                        "ERROR: There was a problem running job"
                        f" {current_proc.name}, schedule terminated"
                    )
                    return

            # Wait up to 24 hours for the job to finish
            result = wait_for_job_to_finish(myjob)

            if result != JOBSTATUS_SUCCESS:
                # Update the job status in the pipeline
                with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
                    pipeline.check_process_completion()
                self.schedule_fail(
                    f"Schedule terminated because process {myjob.output_dir} failed"
                    " or did not finish within 24 hours"
                )
                return

            # Update the job status in the pipeline
            with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
                pipeline.check_process_completion()
                # If we are repeating again, set the status back to Scheduled
                if repeat < nr_repeat - 1:
                    os.remove(os.path.join(myjob.output_dir, SUCCESS_FILE))
                    pipeline.add_job(
                        job=myjob, as_status=JOBSTATUS_SCHED, do_overwrite=True
                    )

            time.sleep(seconds_wait_after)

        repeat_finish = datetime.now()
        wait_time = timedelta(minutes=minutes_wait)
        while repeat_finish < repeat_start + wait_time and repeat != nr_repeat - 1:
            time.sleep(1)
            repeat_finish = datetime.now()


def wait_for_job_to_finish(
    job: PipelinerJob,
    ping: float = 1.0,
    timeout: float = 24 * 60 * 60.0,
    error_on_fail: bool = False,
    error_on_abort: bool = False,
) -> str:
    """Wait for the job to finish, with any status

    Args:
        job (PipelinerJob): The job to wait for
        ping (float): How long to wait before checking for the file again (in seconds)
        timeout (float): Raise an error after this much time has elapsed, even if the
            job hasn't finished (in seconds). The default is 24 hours.
        error_on_fail (bool): Raise an error if the job fails
        error_on_abort (bool): Raise an error if the job is aborted

    Returns:
        str: The final status of the job, Failed, Succeeded, or Aborted
    Raises:
        RuntimeError: upon the watched job failing, being aborted, or either if
            error_on_fail or error_on_abort are True
        RuntimeError: If the job still hasn't finished by the timeout time
    """
    total_time = 0.0
    while total_time <= timeout:
        checks = {
            JOBSTATUS_SUCCESS: os.path.isfile(
                os.path.join(job.output_dir, SUCCESS_FILE)
            ),
            JOBSTATUS_FAIL: os.path.isfile(os.path.join(job.output_dir, FAIL_FILE)),
            JOBSTATUS_ABORT: os.path.isfile(os.path.join(job.output_dir, ABORT_FILE)),
        }
        for status in checks:
            if checks[status]:
                if error_on_fail and status == JOBSTATUS_FAIL:
                    raise RuntimeError(f"Job {job.output_dir} failed")
                elif error_on_abort and status == JOBSTATUS_ABORT:
                    raise RuntimeError(f"Job {job.output_dir} was aborted")
                else:
                    return status

        sleep(ping)
        total_time += ping

    raise RuntimeError(
        f"Job timeout: Job has not finished after {total_time/3600} hours"
    )
