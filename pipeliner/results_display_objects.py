#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import numpy as np
import json
import os
from glob import glob
from typing import (
    List,
    Union,
    Optional,
    Sequence,
    Dict,
    Any,
    cast,
    Literal,
)
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import math

# import plotly.io as po
import pandas as pd
from pipeliner.utils import str_is_hex_colour


def get_next_resultsfile_name(dir: str, search_str: str) -> str:
    """Get the name of the next results file

    taking into account existing files of this type in the output dir to prevent
    overwriting existing ones

    Args:
        dir (str): The output directory
        search_str (str): The full name for the file with * in place of the number

    Returns:
        str: The name of the file

    """
    pre, suf = search_str.split("*")
    existing_files = glob(dir + "/" + pre + "*")
    current_n = [int(os.path.basename(x).replace(pre, "")[:3]) for x in existing_files]
    update_number = max(current_n) + 1 if current_n else 0
    return os.path.join(dir, f"{pre}{update_number:03d}{suf}")


class ResultsDisplayObject(object):
    """Abstract super-class for results display objects

    Attributes:
        title (str): The title
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
        dobj_type (str): Used to identify what kind of ResultsDisplayObject it is
        flag (str): A message that is displayed if the results display object is showing
            somthing scientifically dubious.
    """

    def __init__(self, title: str, start_collapsed: bool = False, flag="") -> None:
        self.title = title
        self.start_collapsed = start_collapsed
        self.dobj_type = ""
        self.flag = flag

    def write_displayobj_file(self, outdir) -> None:
        """Write a json file from a ResultsDisplayObject object

        Args:
            outdir (str): The directory to write the output in

        Raises:
            NotImplementedError: If a write attempt is made from the superclass
        """
        if not self.dobj_type:
            raise NotImplementedError

        outfile = get_next_resultsfile_name(
            outdir, f".results_display*_{self.dobj_type}.json"
        )
        with open(outfile, "w") as of:
            json.dump(self.__dict__, of)


class ResultsDisplayPending(ResultsDisplayObject):
    """A placeholder class for when a job is not able to produce results yet"""

    def __init__(
        self,
        *,
        title: str = "Results pending...",
        message: str = "The result not available yet",
        reason: str = "unknown",
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Instantiate a ResultsDisplayPending object

        Args:
            message (str): The message to display to the user
            reason (str): The reason that the result was not prepared
                usually an exception from the get_results_display() of the
                job
            start_collapsed (Optional[bool]): Should the display start out collapsed
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "pending"
        self.flag = flag
        self.message = message
        self.reason = str(reason)


class ResultsDisplayText(ResultsDisplayObject):
    """A class to display general text in the GUI results tab

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): the title of the section
        display_data (str): The text to display
        associated_data (list): Data files associated with this result
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        display_data: str,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Instantiate a ResultsDisplayText object

        Args:
            title (str): the title of the section
            display_data (str): A the text to display
            associated_data (list): A list of data files associated with these data
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "text"
        self.flag = flag
        self.display_data = display_data
        self.associated_data = associated_data


class ResultsDisplayMontage(ResultsDisplayObject):
    """An object to send to the GUI to make an image montage

    This one is an image montage with info about the specific images
    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the object/graph
        xvalues: (list): The x coordinates by image
        yvalues: (list): The y coordinates by image
        labels (list): Data labels for the images
        associated_data (list): A list of files that contributed the data used in the
            image/graph
        img (str): Path to an image to display
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        xvalues: List[int],
        yvalues: List[int],
        img: str,
        title: str,
        associated_data: List[str],
        labels: Optional[List[str]] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Create a ResultsDisplayMontage object

        Args:
            xvalues: (list): The x coordinates by image
            yvalues: (list): The y coordinates by image
            labels (list): Data labels for the images, in order
            img (str): Path to an image to display
            title (str): The title of the object/graph
            associated_data (list): A list of files that contributed the data used
                in the image/graph
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "montage"
        self.flag = flag
        self.xvalues = xvalues
        self.yvalues = yvalues
        self.labels = [] if not labels else labels
        self.associated_data = associated_data
        self.img = img

        if not len(xvalues) == len(yvalues):
            raise ValueError("The number of x points and y points do not match")


class ResultsDisplayGallery(ResultsDisplayObject):
    """Display object for Doppio's interactive image gallery

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the object/graph
        images (str): Path to a .json file containing a list of image objects
            (base64 images and class IDs)
        labels (list): Data labels for the images
        associated_nodes (list[dict]): {name: str, type: str} A list of nodes associated
            with this gallery along with their full node types
        associated_data (list): A list of files that contributed the data used in the
            image/graph
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        images: str,
        labels: Optional[List[str]] = None,
        associated_nodes: List[Dict[str, str]],
        associated_data: List[str],
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Create a ResultsDisplayGallery object

        Args:
            title (str): The title of the object/graph
            images (str): Path to a .json file containing a list of image objects
                (base64 images and class IDs)
            labels (list): Data labels for the images, in order
            associated_nodes (list[dict]): {name: str, type: str} A list of associated
                nodes and their types
            associated_data (list): A list of files that contributed the data used
                in the image/graph
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "gallery"
        self.images = images
        self.labels = [] if not labels else labels
        self.associated_nodes = associated_nodes
        self.associated_data = associated_data
        self.flag = flag


class ResultsDisplayGraph(ResultsDisplayObject):
    """A simple graph for the GUI to display

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the object/graph
        xvalues: (list): list of x coordinate data series,
            can have multiple data series
        xaxis_label (str): Label for the x axis if a graph
        xrange (list): Range of x to be displayed, displays the
            full range if `None`.  If the x axis needs to be reveresd
            then enter the values backwards [max, min]
        yvalues: (list): List y coordinate data series
            can have multiple data series
        yaxis_label (str): Label for the y axis if a graph
        yrange (list): Range of y to be displayed, displays the
            full range if `None`. If the y axis needs to be reveresd
            then enter the values backwards [max, min]
        data_series_labels (list): List of names of the different data series
        associated_data (list): A list of files that contributed the data used in the
            image/graph
        modes (list): Controls the appearance of each data series,
            choose from 'lines', 'markers' 'or lines+markers'
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        xvalues: List[List[Union[int, float]]],
        yvalues: List[List[Union[int, float]]],
        title: str,
        associated_data: List[str],
        data_series_labels: List[str],
        xaxis_label: str = "",
        xrange: Optional[List[float]] = None,
        yaxis_label: str = "",
        yrange: Optional[List[float]] = None,
        modes: Optional[List[Literal["lines", "markers", "lines+markers"]]] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Create a ResultsDisplayGraph object

        Args:
            title (str): The title of the object/graph
            xvalues: (list): list of x coordinate data series,
                can have multiple data series
            xaxis_label (str): Label for the x axis if a graph
            xrange (list): Range of x to be displayed, displays the
                full range if `None`. If the x axis needs to be reveresd
                then enter the values backwards [max, min]
            yvalues: (list): List y coordinate data series
                can have multiple data series
            yaxis_label (str): Label for the y axis if a graph
            yrange (list): Range of y to be displayed, displays the
                full range if `None`. If the x axis needs to be reveresd
                then enter the values backwards [max, min]
            data_series_labels (list): List of names of the different data series
            associated_data (list): A list of files that contributed the data
                used in the image/graph
            modes (list): Controls the appearance of each data series,
                choose from 'lines', 'markers' 'or lines+markers'
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        Raises:
            ValueError: If the counts of the xvalues, yvalues are not the same
            ValueError: If the any data series doesn't have an equal number of
                x and y values
            ValueError: If the mode is an invalid choice
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "graph"
        self.flag = flag
        if None in [xvalues, yvalues]:
            raise ValueError(
                "x and y value lists must be provided for ResultsDisplayGraph objects"
            )
        if title is None:
            raise ValueError("A ResultsDisplayGraph object must have a title")
        modes = ["lines"] if modes is None else modes
        for mode in modes:
            if mode not in ["markers", "lines", "lines+markers"]:
                raise ValueError(
                    "mode must be in ['markers', 'lines', 'lines+markers']"
                )
        self.xvalues = xvalues
        self.xaxis_label = xaxis_label
        xr_minmax = sum(xvalues, [])  # type: List[Union[float, int]]
        self.xrange = xrange if xrange else [min(xr_minmax), max(xr_minmax)]
        self.yvalues = yvalues
        self.yaxis_label = yaxis_label
        yr_minmax = sum(yvalues, [])  # type: List[Union[float, int]]
        self.yrange = yrange if yrange else [min(yr_minmax), max(yr_minmax)]
        self.associated_data = associated_data
        self.data_series_labels = [] if not data_series_labels else data_series_labels

        if len(modes) != len(xvalues):
            if len(modes) == 1:
                modes = modes * len(xvalues)
            else:
                raise ValueError(
                    "The number of plot mode types and the data series do not match"
                )

        self.modes = modes

        if len(xvalues) != len(yvalues):
            raise ValueError(
                "The number of x data series and y data series are not equal"
            )
        if data_series_labels and len(data_series_labels) != len(xvalues):
            raise ValueError(
                "The number of data series labels doesn't match the number "
                "of data series"
            )

        comb = zip(xvalues, yvalues)
        for i in comb:
            if len(i[0]) != len(i[1]):
                raise ValueError(
                    "One or more data series doesn't have a matching number "
                    "of x and y points"
                )


class ResultsDisplayImage(ResultsDisplayObject):
    """A class for the GUI to display a single image

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title for the image
        image_path (str): The path to the image
        image_desc (str): A description of the image
        associated_data (list): Data files associated with the image
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        image_path: str,
        image_desc: str,
        associated_data: List[str],
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Instantiate a ResultsDisplayImage object

        Args:
            title (str): The title for the image
            image_path (str): The path to the image
            image_desc (str): A description of the image
            associated_data (list): Data files associated with the image
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """

        super().__init__(title, start_collapsed)
        self.dobj_type = "image"
        self.flag = flag
        self.image_path = image_path
        self.image_desc = image_desc
        self.associated_data = associated_data if associated_data is not None else {}


class ResultsDisplayHistogram(ResultsDisplayObject):
    """A class for the GUI to display a histogram

    It is best to not instantiate this class directly. Instead, create
    it using `create_results_display_object`

    Args:
        title (str): The title of the histogram
        data_to_bin (list): The data to bin
        xlabel (str): Label for the x axis
        ylabel (str): Label for the y axis
        associated_data (list): List of data files associated with the histogram
        bins (list): A list of bin counts, if they are known
        bin_edges (list): A list of the bin edges, if they are already known
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    Raises:
        ValueError: If no data or bins are specified
        ValueError: If an attempt is made to specify bins or bin edges when
            data to bin are being provided
        ValueError: If the associated data is not a list, or not provided
    """

    def __init__(
        self,
        *,
        title: str,
        associated_data: List[str],
        data_to_bin: Optional[List[float]] = None,
        xlabel: str = "",
        ylabel: str = "",
        bins: Optional[List[int]] = None,
        bin_edges: Optional[List[float]] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(title, start_collapsed)
        self.dobj_type = "histogram"
        self.flag = flag
        # checks
        if not data_to_bin and (not bins or not bin_edges):
            raise ValueError("Data to bin must be provided or pre-made bins specified")
        if data_to_bin:
            if bins:
                raise ValueError("Cannot specify bins if data are provided")
            if len(data_to_bin) == 0:
                raise ValueError("No data to operate on")

        if not bins and data_to_bin:
            binz: Union[Literal["fd"], List[float]] = bin_edges if bin_edges else "fd"
            bins_np, bin_edges_np = np.histogram(data_to_bin, bins=binz)
            self.bins = [int(x) for x in list(bins_np.astype(float))]
            self.bin_edges = list(bin_edges_np.astype(float))
        elif bins and bin_edges:
            self.bins = bins
            self.bin_edges = bin_edges
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.associated_data = associated_data


class ResultsDisplayPlotlyFigure(ResultsDisplayObject):
    """This class displays an existing Plotly Figure object.

    Call fig.to_json() on your Figure and then pass the JSON string to the ``plotlyfig``
    argument when creating this object.
    """

    def __init__(
        self,
        title: str,
        plotlyfig: str,
        associated_data: Optional[List[str]] = None,
        start_collapsed: bool = False,
    ):
        super().__init__(title=title, start_collapsed=start_collapsed)
        self.dobj_type = "plotlyfigure"
        self.plotlyfig = plotlyfig
        self.associated_data = associated_data if associated_data is not None else []

    def write_displayobj_file(self, outdir):
        dispobj_filename = get_next_resultsfile_name(
            outdir, ".results_display*_plotlyfigure.json"
        )
        with open(dispobj_filename, "w") as dispobj_file:
            json.dump(
                {
                    "dobj_type": "plotlyfigure",
                    "plotlyfig": self.plotlyfig,
                    "title": self.title,
                    "start_collapsed": self.start_collapsed,
                    "associated_data": self.associated_data,
                },
                dispobj_file,
            )


class ResultsDisplayPlotlyObj(ResultsDisplayObject):
    """This uses the plotly express class to create plotly.graph_objects.Figure object
    https://plotly.com/python/plotly-express/
    Use this class to generate plotly Figure objects for custom plots including
    facet-plots: https://plotly.com/python/facet-plots/
    subplots: https://plotly.com/python/subplots/
    multi_series: e.g.
    https://plotly.com/python/creating-and-updating-figures/#adding-traces

    Attributes:
        data: The data to plot.
            For a single plot, following types are allowed:
            list - list of values to be binned
            array and dict - converted to a pandas dataframe internally
            pandas dataframe - ensure column names are added
            if 'x' indicates a column name
            More details https://plotly.com/python/px-arguments/
            For subplots and/or multi_series:
            list - list with dictionary of arguments for each plot/series
        plot_type (str): Required, type of plot.
            For a single plot, it is the plotly express function to call
            https://plotly.com/python-api-reference/plotly.express.html
            For subplots and/or multi_series, plotly.graph_objects function to call
            https://plotly.com/python-api-reference/plotly.graph_objects.html
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[list, pd.DataFrame, np.ndarray, dict],
        plot_type: Union[list, str],
        title: str,
        associated_data: List[str],
        multi_series: bool = False,
        subplot: bool = False,
        make_subplot_args: Optional[dict] = None,
        subplot_order: Optional[Union[str, List[tuple]]] = None,
        subplot_size: Optional[Sequence[int]] = None,
        subplot_args: Optional[List[dict]] = None,
        series_args: Optional[List[dict]] = None,
        layout_args: Optional[dict] = None,
        trace_args: Optional[dict] = None,
        xaxes_args: Optional[Union[List[dict], dict]] = None,
        yaxes_args: Optional[Union[List[dict], dict]] = None,
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ) -> None:
        """
        Args:
            data (list, array, dataframe): The data to bin
                For a single plot, following types are allowed:
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
                For subplots and/or multi_series:
                list - list with dictionary of arguments for each plot/series. e.g.
                https://plotly.com/python/creating-and-updating-figures/#adding-traces-to-subplots
            plot_type (str): Required, type of plot.
                For single/multiseries plots, the plotly express function to call,
                https://plotly.com/python-api-reference/plotly.express.html
                For subplots, this is the graph object function to call,
                https://plotly.com/python-api-reference/plotly.graph_objects.html
            title (str): The title of the histogram
            associated_data (list): List of data files associated with the histogram
            multi_series (bool): multiple data series to plot?. False by default
            subplot (bool): has subplots? False by default
            make_subplot_args (dict): arguments to format subplots, e.g.
                https://plotly.com/python/subplots/#subplots-with-shared-xaxes
                https://plotly.com/python-api-reference/plotly.subplots.html#subplots
            subplot_order (str,list): data order for the subplot. Can be
                a string, either 'column' or 'row' wise or
                a list of (row number,column number) provided explicitly
            subplot_size (list): row-size, column-size
            subplot_args (list): list of argument dictionary for each subplot
            layout_args (dict): dictionary of arguments for plot layout
                https://plotly.com/python/creating-and-updating-figures/#updating-figure-layouts
            trace_args (dict): dictionary of arguments to format all traces
                https://plotly.com/python/creating-and-updating-figures/#updating-traces
            xaxes_args (list,dict): arguments to format axes. Can be
                a dictionary with arguments to update xaxes
                https://plotly.com/python/creating-and-updating-figures/#updating-figure-axes
                or a list of such dictionaries for each subplot
            yaxes_args: (list,dict): arguments to format axes. Can be
                a dictionary with arguments to update xaxes
                https://plotly.com/python/creating-and-updating-figures/#updating-figure-axes
                or a list of such dictionaries for each subplot
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments specific for the plot type
        Raises:
            ValueError: If the input data and arguments dont agree with plotly express
                or graph objects
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyobj"
        self.flag = flag
        self.associated_data = associated_data
        self.plot_type = plot_type
        self.multi_series = multi_series
        self.subplot = subplot
        self.make_subplot_args = make_subplot_args
        self.subplot_order = subplot_order
        self.subplot_size = subplot_size
        self.subplot_args = subplot_args
        self.series_args = series_args
        self.layout_args = layout_args
        self.trace_args = trace_args
        self.xaxes_args = xaxes_args
        self.yaxes_args = yaxes_args
        # set plotly args
        dict_args = {}
        for arg in kwargs:
            if arg != "plotlyfig":
                dict_args[arg] = kwargs[arg]
                setattr(self, arg, kwargs[arg])
        # argument checks
        if subplot:
            self.check_subplot_arguments(
                data,
                subplot_size,
                subplot_order,
                plot_type,
                subplot_args,
                xaxes_args,
                yaxes_args,
            )
        if multi_series:
            self.check_multiseries_arguments(data, plot_type, series_args)
        # set data and plotlyfig
        if multi_series or subplot:
            self.set_multiplot_data(data)
            if not subplot_args:
                subplot_args = [dict_args]
            if subplot:
                plotlyfigure = self.generate_subplots(
                    subplot_size,
                    plot_type,
                    subplot_order,
                    subplot_args,
                    make_subplot_args,
                )
                if xaxes_args and isinstance(xaxes_args, list):
                    for x_args in xaxes_args:
                        plotlyfigure.update_xaxes(**x_args)
                if yaxes_args and isinstance(yaxes_args, list):
                    for y_args in yaxes_args:
                        plotlyfigure.update_yaxes(**y_args)
            else:
                plotlyfigure = self.generate_multiseries_plots(plot_type, series_args)
        else:
            self.check_singleplot_arguments(plot_type)
            self.set_singleplot_data(data)
            plotlyfigure = getattr(px, cast(str, plot_type))(data, **dict_args)
        if xaxes_args and isinstance(xaxes_args, dict):
            plotlyfigure.update_xaxes(**xaxes_args)
        if yaxes_args and isinstance(yaxes_args, dict):
            plotlyfigure.update_yaxes(**yaxes_args)
        if trace_args:
            plotlyfigure.update_traces(**trace_args)
        if layout_args:
            plotlyfigure.update_layout(**layout_args)
        # convert to json
        self.plotlyfig = plotlyfigure.to_json()
        self.associated_data = associated_data

    def set_multiplot_data(self, data) -> None:
        list_data = []
        for plotdata in data:
            if isinstance(plotdata, pd.DataFrame):  # not json serializable
                list_data.append(plotdata.to_dict())
            elif isinstance(plotdata, dict):
                list_data.append(plotdata)
            else:
                raise ValueError(
                    "Input data should be a list of dictionary of data args for "
                    "subplots and multi-series. e.g. : [{x=[.....],y=[.....]},...]"
                )
        self.data: Union[list, pd.DataFrame, np.ndarray, dict] = list_data

    def set_singleplot_data(self, data) -> None:
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data = data.to_dict()
        else:
            self.data = data

    def generate_multiseries_plots(self, plot_type, plot_args) -> go.Figure:
        fig = go.Figure()
        count_seriesplot: int = 0
        for seriesplot_data in self.data:
            if isinstance(plot_type, list):
                seriesplot_type = plot_type[count_seriesplot]
            else:
                seriesplot_type = plot_type
            if len(plot_args) == 1:
                seriesplot_args = plot_args[0]
            else:
                seriesplot_args = plot_args[count_seriesplot]
            fig.add_trace(
                getattr(go, seriesplot_type)(**seriesplot_data, **seriesplot_args),
            )
            count_seriesplot += 1
        return fig

    def generate_subplots(
        self, subplot_size, plot_type, subplot_order, plot_args, make_subplot_args
    ) -> go.Figure:
        if make_subplot_args:
            fig = make_subplots(
                rows=subplot_size[0], cols=subplot_size[1], **make_subplot_args
            )
        else:
            fig = make_subplots(rows=subplot_size[0], cols=subplot_size[1])
        count_subplot: int = 0
        for subplot_data in self.data:
            if isinstance(plot_type, list):
                subplot_type = plot_type[count_subplot]
            else:
                subplot_type = plot_type
            if len(plot_args) == 1:
                subplot_args = plot_args[0]
            else:
                subplot_args = plot_args[count_subplot]
            if isinstance(subplot_order, list):
                subplot_position = subplot_order[count_subplot]
            elif subplot_order == "column":
                subplot_position = (
                    (count_subplot // subplot_size[1]) + 1,
                    (count_subplot % subplot_size[1]) + 1,
                )
            elif subplot_order == "row":
                subplot_position = (
                    (count_subplot % subplot_size[0]) + 1,
                    (count_subplot // subplot_size[0]) + 1,
                )
            fig.add_trace(
                getattr(go, subplot_type)(**subplot_data, **subplot_args),
                row=subplot_position[0],
                col=subplot_position[1],
            )
            count_subplot += 1
        return fig

    def check_multiseries_arguments(self, data, plot_type, series_args) -> None:
        if not isinstance(data, list):
            raise ValueError(
                "data argument should be a list of datasets for subplots \
                or multi_series"
            )
        elif isinstance(plot_type, list):
            self.check_plottype_list(plot_type, data)
        elif not hasattr(go, plot_type):
            raise ValueError(
                f"Input plot type {plot_type} not recognised"
                "See: https://plotly.com/python-api-reference/plotly"
                ".graph_objects.html"
                " for supported types for multi_series"
            )
        elif series_args and len(series_args) > 1 and len(series_args) != len(data):
            raise ValueError(
                "series_args argument should be a list of dictionary of arguments "
                "matching the number of data series"
            )

    def check_plottype_list(self, plot_type, data) -> None:
        if len(plot_type) != len(data):
            raise ValueError(
                "Length of subplot and/or series datasets"
                " does not match length of plot_type"
            )
        else:
            for seriesplot_type in plot_type:
                if not hasattr(go, seriesplot_type):
                    raise ValueError(
                        f"Input plot type {seriesplot_type} not recognised"
                        "See: https://plotly.com/python-api-reference/plotly."
                        "graph_objects.html"
                        " for supported types for multi_series and subplots"
                    )

    def check_subplot_arguments(
        self,
        data,
        subplot_size,
        subplot_order,
        plot_type,
        subplot_args,
        xaxes_args,
        yaxes_args,
    ) -> None:
        if not subplot_size or not subplot_order:
            raise ValueError("subplot_size and subplot_order arguments required!")
        elif not isinstance(data, list):
            raise ValueError(
                "data argument should be a list of subplot datasets"
                "provided as dictionaries or arguments"
            )
        elif len(data) < math.prod(list(subplot_size)):
            raise ValueError("Length of subplot+series datasets less than subplot_size")
        elif len(data) > math.prod(list(subplot_size)):  # subplots with multi_series
            if not isinstance(subplot_order, list):
                raise ValueError(
                    "Length of subplot datasets greater than subplot_size."
                    "This means that some/all subplots contain multi series."
                    "subplot_order should be a list of (row_num,col_num)."
                )
        if isinstance(subplot_order, list) and len(subplot_order) != len(data):
            raise ValueError(
                "Length of subplot datasets does not match subplot_order."
                "subplot_order should be a string from [column,row] "
                "or a list of (row_num,col_num)"
            )
        elif isinstance(subplot_order, str) and subplot_order not in ["column", "row"]:
            raise ValueError(
                "subplot_order should be a string from [column,row]"
                " or a list of (row_num,col_num)"
            )
        if isinstance(plot_type, list):
            self.check_plottype_list(plot_type, data)
        elif isinstance(plot_type, str):
            if not hasattr(go, plot_type):
                raise ValueError(
                    f"Input plot type {plot_type} not recognised"
                    "See: https://plotly.com/python-api-reference/plotly"
                    ".graph_objects.html"
                    " for supported types"
                )
        elif subplot_args and len(subplot_args) != len(data):
            raise ValueError(
                "Length of subplot datasets does not match subplot_args"
                "subplot_args should be a list of dict of args for each subplot"
            )
        elif xaxes_args and isinstance(xaxes_args, list):
            if len(xaxes_args) != len(data):
                raise ValueError(
                    "Length of subplot datasets does not match xaxes_args"
                    "xaxes_args should be a list of dict of args for each subplot xaxes"
                    "e.g. [{title_text='xaxis 3 title',showgrid=False,row=2,col=1},..]"
                )
        elif yaxes_args and isinstance(yaxes_args, list):
            if len(yaxes_args) != len(data):
                raise ValueError(
                    "Length of subplot datasets does not match yaxes_args"
                    "yaxes_args should be a list of dict of args for each subplot xaxes"
                    "e.g. [{title_text='yaxis 3 title',showgrid=False,row=2,col=1},.]"
                )

    def check_singleplot_arguments(self, plot_type) -> None:
        if not isinstance(plot_type, str) or not hasattr(px, plot_type):
            raise ValueError(
                "Input plot type not recognised by Plotly Express. "
                "See: https://plotly.com/python/plotly-express/"
            )


class ResultsDisplayPlotlyHistogram(ResultsDisplayObject):
    """A class that generates plotly.graph_objects.Figure object
    to display a histogram
    Uses plotly express histogram
    https://plotly.com/python-api-reference/generated/plotly.express.histogram.html
    Examples here:
    https://plotly.com/python/histograms/

    Attributes:
        data: The data to bin. Following types are allowed
                    list - list of values to be binned
                    array and dict - converted to a pandas dataframe internally
                    pandas dataframe - ensure column names are added
                    if 'x' indicates a column name
                    More details https://plotly.com/python/px-arguments/
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[List[float], pd.DataFrame, np.ndarray, dict, None] = None,
        title: str,
        x: Union[str, list, None] = None,
        y: Union[str, list, None] = None,
        color: Union[str, int, list, None] = None,
        nbins: Optional[int] = None,
        range_x: Optional[list] = None,
        range_y: Optional[list] = None,
        category_orders: Optional[dict] = None,
        labels: Optional[dict] = None,
        bin_counts: Optional[List[float]] = None,
        bin_centres: Optional[List[float]] = None,
        associated_data: List[str],
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ) -> None:
        """
        Args:
            data (list, array, dataframe): The data to bin
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
            title (str): The title of the histogram
            x (str, list): Column name from the dataframe or dict
                str input should match with column name in  dataframe or
                key in dict
            y (str, list): Column name from the dataframe or dict
                str input should match with column name in  dataframe or
                key in dict
            color (str, int, list, tuple): color of histogram bars
                If column name or a list/tuple provided,
                values are used to assign color to marks.
            nbins (int): Number of bins
            range_x (list of two numbers):
                overrides auto-scaling on the x-axis in cartesian coordinates.
            range_y (list of two numbers):
                overrides auto-scaling on the y-axis in cartesian coordinates.
            category_orders (dict): Order of the data categories for the histogram
                use for categorical data only. For example,
                with categorical data from the column name 'day'
                {'day':['Monday','Tuesday','Wednesday']}
            labels (dict): X and Y labels. Example:
                {'x':'total_bill', 'y':'count'}
                Overrides other options and column names
            bin_counts (list): A list of bin counts, if they are known
            bin_centres (list): A list of the bin centres (same length as bin counts),
                if they are already known
            associated_data (list): List of data files associated with the histogram
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments
        Raises:
            ValueError: If no data OR (bin_counts and bin_centre) provided
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyhistogram"
        self.flag = flag
        dict_args: Dict[str, Any] = {}

        # check and set input
        if data is None and not bin_counts and not bin_centres:
            raise ValueError("No data to operate on")
        if x:
            dict_args["x"] = x
        if y:
            dict_args["y"] = y
        if color:
            dict_args["color"] = color
        if nbins:
            dict_args["nbins"] = nbins
        if range_x:
            dict_args["range_x"] = range_x
        if range_y:
            dict_args["range_y"] = range_y
        if category_orders:
            dict_args["category_orders"] = category_orders
        if labels:
            dict_args["labels"] = labels
        for arg in kwargs:
            if arg != "plotlyfig":
                dict_args[arg] = kwargs[arg]
                setattr(self, arg, kwargs[arg])
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data: Union[List[float], pd.DataFrame, np.ndarray, dict, None] = (
                data.to_dict()
            )
        else:
            self.data = data

        self.associated_data = associated_data
        if bin_counts and bin_centres:
            dict_args["x"] = bin_centres
            dict_args["y"] = bin_counts
            plotlyfigure = px.bar(**dict_args)
        else:
            plotlyfigure = px.histogram(data, **dict_args)
        self.plotlyfig = plotlyfigure.to_json()


class ResultsDisplayPlotlyScatter(ResultsDisplayObject):
    """A class that generates plotly.graph_objects.Figure object
    to display a scatter plot
    Uses plotly express scatter
    https://plotly.com/python-api-reference/generated/plotly.express.scatter.html
    Examples here:
    https://plotly.com/python/line-and-scatter/

    Attributes:
        data: The data to bin. Following types are allowed
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[List[List[float]], pd.DataFrame, np.ndarray, dict, None] = None,
        title: str,
        x: Union[str, List[float], None] = None,
        y: Union[str, List[float], None] = None,
        color: Union[str, int, Sequence[str], None] = None,
        size: Union[str, int, Sequence[str], None] = None,
        symbol: Union[str, int, Sequence[str], None] = None,
        hover_name: Union[str, int, Sequence[str], None] = None,
        range_color: Optional[list] = None,
        range_x: Optional[list] = None,
        range_y: Optional[list] = None,
        category_orders: Optional[dict] = None,
        labels: Optional[dict] = None,
        associated_data: List[str],
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ) -> None:
        """
        Args:
            data (list of lists, dict, numpy array, pandas dataframe): The data to bin
                list - list of list of x and y values to be plotted
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
            title (str): The title of the histogram
            x (str, list): List of x values OR
                Column name from the dataframe or dict, OR
                A list of the bin edges, if they are already known
            y (str, list): List of y values OR
                Column name from the dataframe or dict, OR
                A list of bin counts, if they are known
            color (str, int, list, tuple): color of the plot
                If column name or a list/tuple provided,
                values are used to assign color to markers.
            size (str, int, list, tuple): size of the markers
                If column name or a list/tuple provided,
                values are used to assign color to marker sizes.
            symbol (str, int, list, tuple): symbol of the markers
                If column name or a list/tuple provided,
                values are used to assign symbols to markers.
            hover_name (str, int, list, tuple): If column name or a list/tuple provided,
                values are highlighted in the hover tooltip.
            range_color (list of two numbers):
                overrides auto-scaling on the continuous color scale.
            range_x (list of two numbers):
                overrides auto-scaling on the x-axis in cartesian coordinates.
            range_y (list of two numbers):
                overrides auto-scaling on the y-axis in cartesian coordinates.
            category_orders (dict): Order of the data categories for the plot
                use for categorical data only. For example,
                with categorical data from the column name 'day'
                {'day':['Monday','Tuesday','Wednesday']}
            labels (dict): X and Y labels. Example:
                {'x':'total_bill', 'y':'count'}
                Overrides other options and column names
            associated_data (list): List of data files associated with the histogram
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments
        Raises:
            ValueError: If no data OR (list of x and y values) provided
        """

        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyscatter"
        self.flag = flag
        dict_args: Dict[str, Any] = {}
        # check and set input
        if data is None and not isinstance(x, list) and not isinstance(y, list):
            raise ValueError("No data to operate on")
        if x:
            dict_args["x"] = x
        if y:
            dict_args["y"] = y
        if color:
            dict_args["color"] = color
        if size:
            dict_args["size"] = size
        if symbol:
            dict_args["symbol"] = symbol
        if hover_name:
            dict_args["hover_name"] = hover_name
        if range_color:
            dict_args["range_color"] = range_color
        if range_x:
            dict_args["range_x"] = range_x
        if range_y:
            dict_args["range_y"] = range_y
        if category_orders:
            dict_args["category_orders"] = category_orders
        if labels:
            dict_args["labels"] = labels
        for arg in kwargs:
            if arg != "plotlyfig":
                dict_args[arg] = kwargs[arg]
                setattr(self, arg, kwargs[arg])
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data: Union[
                List[List[float]], pd.DataFrame, np.ndarray, dict, None
            ] = data.to_dict()
        else:
            self.data = data
        self.associated_data = associated_data
        # if data is provided as x and y
        if isinstance(x, list) and isinstance(y, list):
            plotlyfigure = px.scatter(**dict_args)
        else:
            plotlyfigure = px.scatter(data, **dict_args)
        self.plotlyfig = plotlyfigure.to_json()


class ResultsDisplayTable(ResultsDisplayObject):
    """An object for the GUI to display a table

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the table
        headers (list): The column headers for the table
        table_data (list): A list of lists, on per row
        associated_data (list): A list of the associated data files
        header_tooltips (list): Tooltips for each column. Column header by default
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        headers: List[str],
        table_data: List[List[str]],
        associated_data: List[str],
        header_tooltips: Optional[List[str]] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        """Create a ResultsDisplayTable object
        Args:
            title (str): The title of the table
            headers (List[List[str]]): The column headers for the table
            table_data (list): A list of lists, one per row
            associated_data (list): A list of the associated data files
            header_tooltips (list): Tooltips for each column. Column header by default
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        Raises:
            ValueError: If the number of columns in the data are inconsistent
            ValueError: If the number of columns in the data doesn't match the
                number of headers
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "table"
        self.flag = flag
        # checks
        data_lengths = set([len(x) for x in table_data])
        if len(data_lengths) != 1:
            raise ValueError(
                f"Mismatch in number of columns in data: {[len(x) for x in table_data]}"
            )
        if list(data_lengths)[0] != len(headers):
            raise ValueError(
                "Mismatch between number of headers and data columns"
                f"{list(data_lengths)[0]}, {len(headers)}"
            )
        if not header_tooltips or len(header_tooltips) != len(headers):
            header_tooltips = headers

        self.header_tooltips = header_tooltips
        self.headers = headers
        self.table_data = table_data
        self.associated_data = associated_data


class ResultsDisplayMapModel(ResultsDisplayObject):
    """An object for overlaying multiple maps and/or models

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title that appears at the top of the accordian in
            the GUI
        associated_data (list): A list of associated data files
        maps (list): List of map paths, mrc format
        models (list): List of model paths, pdb or mmcif format
        maps_opacity (list): Opacity for each map from 0-1 if not specified set
            at 0.5 for all maps
        models_data (str): Any extra info about the models
        maps_data (str): Any extra info about the maps
        maps_colours (list): Hex values for colouring the maps specific colours, in
            the form "#XXXXXX" where X is a hex digit (0-9 or a-f). If None, the
            standard colours will be used
        models_colours (list): Hex values for colouring the models specific colours, in
            the form "#XXXXXX" where X is a hex digit (0-9 or a-f). If None, the
            standard colours will be used

    Raises:
        ValueError: If no maps or models were specified
        ValueError: If the map is not .mrc format
        ValueError: If models are not in pdb of mmcif format
        ValueError: If the number of maps and map opacities don't match
    """

    def __init__(
        self,
        title: str,
        associated_data: List[str],
        maps: Optional[List[str]] = None,
        models: Optional[List[str]] = None,
        maps_data: str = "",
        models_data: str = "",
        maps_opacity: Optional[List[float]] = None,
        maps_colours: Optional[List[str]] = None,
        models_colours: Optional[List[str]] = None,
        start_collapsed: bool = True,
        flag: str = "",
    ) -> None:
        title = "3D viewer: " + title if not title.startswith("3D viewer:") else title
        super().__init__(title, start_collapsed=start_collapsed)
        self.dobj_type = "mapmodel"
        self.flag = flag
        maps = maps if maps is not None else []
        models = models if models is not None else []
        if maps == [] and models == []:
            raise ValueError(
                "No maps or models specified for ResultsDisplayMapModel objects"
            )
        maps_opacity = [] if maps_opacity is None else maps_opacity
        if len(maps_opacity) == 0:
            mapoval = 1.0 if len(maps) == 1 and len(models) == 0 else 0.5
            maps_opacity = [mapoval] * len(maps)
        if len(maps_opacity) != len(maps):
            raise ValueError(
                "Number of maps and specified opacities don't match for"
                "creating ResultsDisplayMapModel object"
            )
        for map_path in maps:
            _, ext = os.path.splitext(map_path)
            if ext.lower() not in [".mrc", ".map", ".ccp4"]:
                raise ValueError(
                    "ResultsDisplayMapModel objects must have a .mrc map as input"
                )
        self.maps = maps
        self.maps_opacity = maps_opacity

        maps_colours = [] if maps_colours is None else maps_colours
        if maps_colours:
            if len(maps_colours) != len(maps):
                raise ValueError(
                    "Number of specified colors not equal to number of maps"
                )
            if not all([str_is_hex_colour(x, allow_0x=True) for x in maps_colours]):
                raise ValueError("Invalid colour hex code in 'maps_colours'")
            maps_colours = [x.replace("#", "0x") for x in maps_colours]

        self.maps_colours = maps_colours

        models_colours = [] if models_colours is None else models_colours
        if models_colours:
            if len(models_colours) != len(models):
                raise ValueError(
                    "Number of specified colors not equal to number of models"
                )
            if not all([str_is_hex_colour(x, allow_0x=True) for x in models_colours]):
                raise ValueError("Invalid colour hex code in 'models_colours'")
            models_colours = [x.replace("#", "0x") for x in models_colours]
        self.models_colours = models_colours

        formats = [".pdb", ".cif", ".mmcif", ".pdbx", ".ent"]
        for model_path in models:
            _, ext = os.path.splitext(model_path)
            if ext.lower() not in formats:
                raise ValueError(
                    "ResultsDisplayMapModel objects must have a model in one of the"
                    f" following formats as input: {formats}. File was {model_path}"
                )
        self.models = models
        self.maps_data = maps_data
        self.models_data = models_data
        self.associated_data = associated_data


class ResultsDisplayHtml(ResultsDisplayObject):
    """An object for the GUI to display html

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for general HTML display in Doppio.  Either provide a directory
    with index.html or specify a html file or provide a html string as input.

    Attributes:
        html_dir (str): Path to the html directory (optional)
        html_file (str): Path to a standalone html file or in the given html_dir
                        (optional)
        html_str (str): Input html as string (optional)
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        associated_data: List[str],
        html_dir: str = "",
        html_file: str = "",
        html_str: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(title, start_collapsed)
        self.dobj_type = "html"
        self.flag = flag
        if os.path.isdir(html_dir):
            self.html_dir = html_dir
            if os.path.isfile(html_file):
                self.html_file = html_file  # specific html file in the directory
            else:
                self.html_file = "index.html"
        elif os.path.isfile(html_file):
            self.html_file = html_file
        elif html_str:
            self.html_str = html_str
        else:
            raise ValueError(
                f"Directory {html_dir} and path {html_file} do not exist."
                f"html string input {html_str} is also empty."
                "Please provide a valid value for atleast one of them"
            )
        self.associated_data = associated_data


class ResultsDisplayRvapi(ResultsDisplayObject):
    """An object for the GUI to display rvapi objects

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for general HTML display in Doppio.  Create a directory
    with index.html and it will be shown in the results display tab

    Attributes:
        rvapi_dir (str): Path to the rvapi directory
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        rvapi_dir: str,
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(title, start_collapsed)
        self.dobj_type = "rvapi"
        self.flag = flag
        self.rvapi_dir = rvapi_dir
        if not os.path.isdir(rvapi_dir):
            raise ValueError(f"Directory {rvapi_dir} does not exist")


class ResultsDisplayTextFile(ResultsDisplayObject):
    """An object for the GUI to display ascii tecxt files

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for default display of files that have ascii encoded text but
    the formats are too variable to make a more complex ResultsDisplayFile

    Attributes:
        file_path (str): Path to the file
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        file_path: str,
        title: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(file_path if not title else title, start_collapsed)
        self.dobj_type = "textfile"
        self.flag = flag
        self.file_path = file_path
        with open(file_path) as f:
            try:
                f.read()
            except UnicodeDecodeError:
                raise ValueError(f"File '{file_path}' is not a valid text file")


class ResultsDisplayPdfFile(ResultsDisplayObject):
    """An object for the GUI to display pdf files

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        file_path (str): Path to the file
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        file_path: str,
        title: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(file_path if not title else title, start_collapsed)
        self.dobj_type = "pdffile"
        self.flag = flag
        self.file_path = file_path


class ResultsDisplayJson(ResultsDisplayObject):
    """An object for the GUI to display JSON files

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        file_path (str): Path to the file
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        file_path: str,
        title: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ) -> None:
        super().__init__(file_path if not title else title, start_collapsed)
        self.dobj_type = "json"
        self.flag = flag
        self.file_path = file_path


RESULTS_DISPLAY_OBJECTS = {
    "graph": ResultsDisplayGraph,
    "histogram": ResultsDisplayHistogram,
    "html": ResultsDisplayHtml,
    "image": ResultsDisplayImage,
    "mapmodel": ResultsDisplayMapModel,
    "montage": ResultsDisplayMontage,
    "pdffile": ResultsDisplayPdfFile,
    "pending": ResultsDisplayPending,
    "plotlyfigure": ResultsDisplayPlotlyFigure,
    "plotlyobj": ResultsDisplayPlotlyObj,
    "plotlyhistogram": ResultsDisplayPlotlyHistogram,
    "plotlyscatter": ResultsDisplayPlotlyScatter,
    "rvapi": ResultsDisplayRvapi,
    "table": ResultsDisplayTable,
    "text": ResultsDisplayText,
    "textfile": ResultsDisplayTextFile,
    "json": ResultsDisplayJson,
    "gallery": ResultsDisplayGallery,
}
