#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import math
import shutil
import shlex
import re
import traceback
from collections import OrderedDict
from pathlib import Path
from typing import Optional, List, Union, Dict, Any, Sequence, Tuple
import warnings
from gemmi import cif
import logging

from pipeliner import user_settings
from pipeliner.starfile_handler import JobStar
from pipeliner.relion_compatibility import get_job_type
from pipeliner.star_writer import write
from pipeliner.data_structure import TRUES, NODE_DISPLAY_DIR
from pipeliner.job_options import (
    JobOption,
    StringJobOption,
    BooleanJobOption,
    IntJobOption,
    JobOptionValidationResult,
    InputNodeJobOption,
    JobOptionCondition,
    MultiInputNodeJobOption,
    ExternalFileJobOption,
    MultiExternalFileJobOption,
    DirPathJobOption,
)
from pipeliner.utils import (
    truncate_number,
    run_subprocess,
    get_python_command,
    file_in_project,
)
from pipeliner.nodes import Node
from pipeliner.node_factory import create_node
from pipeliner.display_tools import create_results_display_object
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
    ResultsDisplayPending,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticles,
    EmpiarCorrectedMics,
    EmpiarMovieSet,
    EmpiarRefinedParticles,
)
from pipeliner.deposition_tools.onedep_deposition_objects import OneDepData

logger = logging.getLogger(__name__)


class PipelinerCommand(object):
    """
    Holds a command that will be run by the pipeliner

    Attributes:
        com (List[str]): The command that will be run each list item is one arg
        relion_control (bool): Does the command need the relion '--pipeline_control'
            argument appended before being run
    """

    def __init__(
        self, args: Sequence[Union[str, float, int]], relion_control: bool = False
    ):
        """Create a PipelinerCommand"""
        self.relion_control = relion_control

        command_strs = [str(arg) for arg in args]
        if command_strs[0] in ("python", "python3"):
            final_command = get_python_command()
            final_command.extend(command_strs[1:])
        else:
            final_command = command_strs
        self.com = final_command

    def add_pipeline_control(self, outputdir: str) -> None:
        self.com.extend(["--pipeline_control", outputdir])


class ExternalProgram(object):
    """
    Class to store info about external programs called by the pipeliner

    Attributes:
        command (str): The command that will be used to run the program
        name (str): The name for the program, command will be used unless this is
            specified
        exe_path (str): The path to the executable for the program
        vers_com (List[str]): The command that needs to be run to get the version
        vers_lines (List[int]): The lines from the output of the version command that
            contain the version info
    """

    def __init__(
        self,
        command: str,
        name: Optional[str] = None,
        vers_com: Optional[List[str]] = None,
        vers_lines: Optional[List[int]] = None,
    ) -> None:
        """
        Create an ExternalProgram object

        Args:
            command (str): The command used to call the program from the unix shell
            vers_com (List[str]):  A list of command name and arguments to be run to get
                the program's version
            vers_lines: Optional[List[int]]=None: A list of lines from the stdout
                of the vers_com that contain the version information.  If `None` the
                entire stdout is used
        """
        self.command = command
        self.vers_com = vers_com
        self.vers_lines = vers_lines
        self.name = name if name is not None else self.command
        # check that the exe exists
        compath = shutil.which(command)
        self.exe_path = compath

    def get_version(self) -> Optional[str]:
        if self.vers_com and self.exe_path:
            try:
                if self.vers_com[0] == "acedrg":
                    # Special workaround for acedrg because it leaves temporary
                    # directories and files behind after printing its version info. The
                    # problem has been reported and will hopefully be fixed in a future
                    # version of acedrg.
                    # (We don't do this for all job types because using a temporary
                    #  directory slows things down a lot. Version checking is already
                    #  too slow so we only want to use a temporary directory when really
                    #  needed.)
                    import tempfile

                    with tempfile.TemporaryDirectory() as tempdir:
                        versproc = run_subprocess(
                            self.vers_com,
                            capture_output=True,
                            timeout=1,
                            cwd=tempdir.name,  # type: ignore[attr-defined]
                        )
                # executable from conda envs take upto 3-4s to get version
                elif self.vers_com[0] in ["ribfind", "va"]:
                    versproc = run_subprocess(
                        self.vers_com, capture_output=True, timeout=10.0
                    )
                else:
                    versproc = run_subprocess(
                        self.vers_com, capture_output=True, timeout=1
                    )
                vers_raw = versproc.stdout.decode().split("\n")

                # a few attempts to clean up messy version info
                # try an alternate split if \n didn't work
                if len(vers_raw) == 1:
                    vers_raw = vers_raw[0].split(";")

                if self.vers_lines:
                    vers_info = ";".join([vers_raw[x] for x in self.vers_lines])
                else:
                    vers_info = ";".join(vers_raw)
                # remove excess spaces
                vers_info = vers_info.replace("    ", " ")
                return vers_info

            except Exception as e:
                return f"Error trying to determine version: {str(e)}"
        else:
            return None


class Ref(object):
    """
    Class to hold metadata about a citation or reference, typically a journal article.

    Attributes:
        authors (list): The authors of the reference.
        title (str): The reference's title.
        journal (str): The journal.
        year (str): The year of publication.
        volume (str): The volume number.
        issue (str): The issue number.
        pages (str): The page numbers.
        doi (str): The reference's Digital Object Identifier.
        other_metadata (dict): Other metadata as needed. Gathered from kwargs
    """

    def __init__(
        self,
        authors: Optional[Union[str, List[str]]] = None,
        title: str = "",
        journal: str = "",
        year: str = "",
        volume: str = "",
        issue: str = "",
        pages: str = "",
        doi: str = "",
        **kwargs,
    ) -> None:
        """
        Create a new Ref object.

        All arguments are optional. Use additional keyword arguments to store other
        metadata if necessary.

        Args:
            authors (list of str): The authors of the reference.
            title (str): The reference's title.
            journal (str): The journal.
            year (str): The year of publication.
            volume (str): The volume number.
            issue (str): The issue number.
            pages (str): The page numbers.
            doi (str): The reference's Digital Object Identifier.
            **kwargs (str): Other metadata as needed.
        """
        # Ensure self.authors is stored as a list
        if authors is not None:
            if isinstance(authors, str):
                self.authors = [authors]
            else:
                self.authors = list(authors)
        else:
            self.authors = []

        if not all((title, journal, year)):
            raise ValueError(
                "Reference objects need a title, journal, and year at minimum"
            )

        self.title = title
        self.journal = journal
        self.year = year
        self.volume = volume
        self.issue = issue
        self.pages = pages
        self.doi = doi
        self.other_metadata = kwargs

    def __str__(self) -> str:
        """Prints out the reference in the pipeliner's display format

        Format is:
            <authors>
            <title>
            <journal>(<year>) <vol>(<issue>) doi: <doi>
            <other metadata key 1>: <other_metadata value 1>
            ...
            <other metadata key n>: <other_metadata value n>

        Returns:
            str: The formatted reference
        """
        thestring = []
        if 0 < len(self.authors) < 5:
            thestring.append(f"{', '.join(self.authors)}")
        elif len(self.authors) > 5:
            thestring.append(f"{self.authors[0]} et al.")
        if self.title != "":
            thestring.append(f"\n{self.title}")

        journal = f"{self.journal} " if self.journal != "" else ""
        year = f"({self.year})" if self.year != "" else ""
        vol = f" {self.volume}" if self.volume != "" else ""
        issue = f"({self.issue})" if self.issue != "" else ""
        pages = f":{self.pages}" if self.pages != "" else ""
        doi = f" doi: {self.doi}" if self.doi != "" else ""
        thestring.append(f"\n{journal}{year}{vol}{issue}{pages}{doi}")
        if len(self.other_metadata) > 0:
            for addarg in self.other_metadata:
                thestring.append(f"\n{addarg}: {self.other_metadata[addarg]}")

        return "".join(thestring)

    def __eq__(self, other) -> bool:
        """Determine if two references are the same paper...

        Try to give some leeway for slightly different punctuation and abbreviations in
        journal names
        """
        # easy way: do the dois match?
        if self.doi and other.doi:
            return True if self.doi == other.doi else False

        # or do it the hard way
        # compare titles
        titles = [re.findall("[a-z0-9]", x.title.lower()) for x in (self, other)]
        titles_match = titles[0] == titles[1]

        # compare journals
        jsplit1 = [re.findall("[a-z0-9]", x.lower()) for x in self.journal.split(" ")]
        jsplit2 = [re.findall("[a-z0-9]", x.lower()) for x in other.journal.split(" ")]

        jtests = []
        for n, i in enumerate(jsplit1):
            if len(i) == len(jsplit2[n]):
                jtests.append(i == jsplit2[n])
            elif len(i) < len(jsplit2[n]):
                jtests.append(jsplit2[n][: len(i)] == i)
            else:
                jtests.append(jsplit2[n] == i[: len(jsplit2[n])])
        journals_match = all(jtests)

        # compare years
        years_match = self.year == other.year

        return all((years_match, titles_match, journals_match))


class JobInfo(object):
    """
    Class for storing info about jobs.

    This is used to generate documentation for the job within the
    pipeliner

    Attributes:
        display_name (str): A user-friendly name to describe the job in a GUI, this
            should not include the software used, because that info is pulled from
            the job type
        version (str): The version number of the pipeliner job
        job_author (str): Who wrote the pipeliner job
        short_desc (str): A one line "title" for the job
        long_desc (str): A detained description about what the job does
        documentation (str): A URL for online documentation
        programs (List[`~pipeliner.pipeliner_job.ExternalProgram`]): A list of
            3rd party software used by the job.  These are used by the pipeliner
            to determine if the job can be run, so they need to include
            all executables the job might call. If any program on this list
            cannot be found with `which` then the job will be marked as unable to run.
        references (list): A list of :class:`~pipeliner.pipeliner_job.Ref` objects used
        alternative_unavailable_reason (str): This can be set to a string explaining
            why the job is unavailable if other checks for the job to be available
            (besides programs missing from the $PATH) have failed, e.g. a necessary
            library is missing.
    """

    def __init__(
        self,
        display_name: str = "Pipeliner job",
        version: str = "0.0",
        job_author: Optional[str] = None,
        short_desc: str = "No short description for this job",
        long_desc: str = "No long description for this job",
        documentation: str = "No online documentation available",
        external_programs: Optional[List[ExternalProgram]] = None,
        references: Optional[List[Ref]] = None,
    ) -> None:
        """Create a JobInfo object

        Args:
            display_name (str): A user-friendly name to describe the job in a GUI, this
                should not include the software used, because that info is pulled from
                the job type
            version (str): The job version number
            job_author (str): Who wrote the job for the pipeliner
            short_desc (str): A one line description of what the job does
            long_desc (str):  A longer description with background info
            documentation (str): Address for on-line documentation
            external_programs (List[:class:~`pipeliner.pipeliner_job.ExternalProgram]):
                A list of :class:ExternalProgram objects for the  programs used by the
                job
            references (list): A list of :class:Ref objects for the job's for the
                main papers describing the software
        """
        self.display_name = display_name
        self.version = version
        self.job_author = job_author
        self.short_desc = short_desc
        self.long_desc = long_desc
        self.documentation = documentation
        self.programs = external_programs if external_programs is not None else []
        self.references = references if references is not None else []
        self.alternative_unavailable_reason: Optional[str] = None

    @property
    def is_available(self):
        """Is the job available to run?

        ``True`` if executables were found for all the job's programs or if
        ``alternative_unavailable_reason`` has been set, or ``False`` otherwise.
        """
        checks = [bool(prog.exe_path) for prog in self.programs]
        checks.append(not bool(self.alternative_unavailable_reason))
        return all(checks)

    @property
    def unavailable_message(self) -> Optional[str]:
        """Gives the reason the pipeliner has marked the job as unavailable

        Returns:
            Optional[str]: The reason the job is marked unavailable, or ``None``
            if it is available
        """

        if self.is_available:
            return None
        reasons = []
        for prog in self.programs:
            if not bool(prog.exe_path):
                reasons.append(f"executable '{prog.command}' not found")
        if self.alternative_unavailable_reason:
            reasons.append(self.alternative_unavailable_reason)
        reasonstr = "; ".join(reasons)
        return f"This job has been marked unavailable because: {reasonstr}"


class PipelinerJob(object):
    """
    Super-class for job objects.

    Each job type has its own sub-class.

    WARNING: do not instantiate this class directly, use the factory functions in this
    module.

    Attributes:
        jobinfo (:class:`~pipeliner.pipeliner_job.JobInfo`): Contains
            information about the job such as references
        output_dir (str): The path of the output directory created by this job
        alias (str): the alias for the job if one has been assigned
        is_continue (bool): If this job is a continuation of an older job or a new one
        input_nodes (list): A list of :class:`~pipeliner.nodes.Node` objects
            for each file used as in input
        output_nodes (list): A list of :class:`~pipeliner.nodes.Node` objects
            for files produced by the job
        joboptions (dict): A dict of :class:`~pipeliner.job_options.JobOption` objects
            specifying the parameters for the job
        is_tomo (bool): Is the job a tomography job?
        working_dir (str): The working directory to be used when running the job. This
            should normally be left as ``None``, meaning the job will be run in the
            project directory. Jobs that write files in their working directory should
            instead work somewhere within the job's output directory, and take care to
            adjust the paths of input and output files accordingly.
        raw_options (dict): A dict of all raw joboptions as they were read in

    """

    PROCESS_NAME = ""
    OUT_DIR = ""
    CATEGORY_LABEL = ""

    def __init__(self) -> None:
        """Creates a PipelinerJob object

        Raises:
            NotImplementedError: If this class is instantiated directly except in
                testing
        """
        if type(self) is PipelinerJob:
            raise NotImplementedError(
                "Cannot create PipelinerJob objects directly - use a sub-class"
            )

        self.jobinfo = JobInfo()
        self.output_dir = ""
        self.alias = None
        self.is_continue = False
        self.input_nodes: List[Node] = []
        self.output_nodes: List[Node] = []
        self.joboptions: Dict[str, JobOption] = {}
        self.is_tomo = False

        # this should typically be None or self.output_dir
        self.working_dir: Optional[str] = None

        self.raw_options: Dict[str, str] = {}

        # If the job is run multiple times in a schedule
        # should every subsequent run always be a continuation job?
        self.always_continue_in_schedule = False

        # if the job is continued should the previous output
        # nodes be deleted first?
        self.del_nodes_on_continue = False

    def get_category_label(self) -> str:
        """Get a label for the category that this job belongs to.

        If the job defines a ``CATEGORY_LABEL`` attribute, its value is simply returned.
        Otherwise, the second part of the process name is processed to produce a label
        by replacing underscores with spaces and converting to title case.
        """
        if self.CATEGORY_LABEL:
            job_category = self.CATEGORY_LABEL
        else:
            name_split = self.PROCESS_NAME.split(".")
            try:
                job_category = name_split[1].replace("_", " ").title()
            except IndexError:
                job_category = "Uncategorised Jobs"
        return job_category

    def set_option(self, line: str) -> None:
        """Sets a value in the joboptions dict from a run.job file

        Args:
            line (str): A line from a run.job file

        Raises:
            RuntimeError: If the line does not contain '=='
            RuntimeError: If the value of the line does not match any of the
                joboptions keys
        """

        if len(line) == 0:
            return
        if "==" not in line:
            raise RuntimeError("No '==' entry on JobOption line: {}".format(line))
        key, val = line.split(" == ")
        found = None
        for k, v in self.joboptions.items():
            if v.label == key:
                found = k
                break
        if found is None:
            raise RuntimeError("Job does not contain label: {}".format(key))
        self.joboptions[found].set_string(val)

    def read(self, filename: str) -> None:
        """Reads parameters from a run.job or job.star file

        Args:
            filename (str): The file to read.  Can be a run.job or job.star file

        Raises:
            ValueError: If the file is a job.star file and job option from the
                :class:`~pipeliner.pipeliner_job.PipelinerJob` is missing
                from the input file
        """
        if filename.endswith(".job"):
            # make a dict of params from the file {joboption_label: joboption_value}
            with open(filename) as job_file:
                job_lines = job_file.readlines()
            job_lines_split = [x.partition("==") for x in job_lines]
            j_param = {key.strip(): val.strip() for key, _, val in job_lines_split}
            self.raw_options = j_param

            # overwrite joboption values in the job with those from the file
            self.is_continue = True if j_param.get("is_continue") == "true" else False
            self.is_tomo = True if j_param.get("is_tomo") == "true" else False

            for joboption in self.joboptions.values():
                try:
                    joboption.value = j_param[joboption.label]

                # if a joboption is missing from the file just use the default
                except KeyError:
                    logger.warning(
                        f"{filename}; no value for `{joboption.label}` "
                        f"using default value: {joboption.default_value}"
                    )
        elif filename.endswith("job.star"):
            job_options = JobStar(filename).all_options_as_dict()
            job_type_label = get_job_type(job_options)[1]
            self.raw_options = job_options

            self.is_continue = job_options["_rlnJobIsContinue"].lower() in TRUES
            del job_options[job_type_label]
            del job_options["_rlnJobIsContinue"]
            del job_options["_rlnJobIsTomo"]
            for joboption_name in self.joboptions:
                # overwrite joboption values with those from the file
                try:
                    self.joboptions[joboption_name].value = job_options[joboption_name]

                # if a joboption is missing from the file just use the defaule
                except KeyError:
                    logger.warning(
                        f"{filename}; no value for "
                        f"'{self.joboptions[joboption_name].label}' using default"
                        f" value: {self.joboptions[joboption_name].default_value}"
                    )

    def write_runjob(self, fn: Optional[str] = None) -> None:
        """Writes a run.job file

        Args:
            fn (str): The name of the file to write. Defaults to the file the pipeliner
                uses for storing GUI parameters.  A directory can also be entered and it
                will add on the file name 'run.job'
        """
        self.add_compatibility_joboptions()
        final_order = []
        groupings = self.get_joboption_groups()
        for group in groupings:
            final_order += groupings[group]

        hidden_name = f".gui_{self.PROCESS_NAME.replace('.','_')}"
        filename = hidden_name if fn is None else fn
        if filename != hidden_name and not filename.endswith("run.job"):
            filename += "run.job"
        with open(filename, "w+") as f:
            f.write("job_type == {}\n".format(self.PROCESS_NAME))
            cont_val = "true" if self.is_continue else "false"
            f.write(f"is_continue == {cont_val}\n")
            tomo_val = "true" if self.is_tomo else "false"
            f.write(f"is_tomo == {tomo_val}\n")
            for jo in final_order:
                label = self.joboptions[jo].label
                value = str(self.joboptions[jo].value)
                f.write(label + " == " + value + "\n")

    def add_compatibility_joboptions(self) -> None:
        """Write additional joboptions for back compatibility

        Some JobOptions are needed by the original program (hey Relion 4), but
        not the pipeliner, they are added here so the files pipeliner writes will
        be back compatible with their original program.
        """
        pass

    def get_default_params_dict(self) -> Dict[str, str]:
        """Get a dict with the job's parameters and default values

        Returns:
            dict: All the job's parameters {parameter: default value}
        """
        params_dict = {}
        for jo in self.joboptions:
            params_dict[jo] = self.joboptions[jo].default_value
        params_dict["_rlnJobTypeLabel"] = self.PROCESS_NAME
        continue_val = "1" if self.is_continue else "0"
        params_dict["_rlnJobIsContinue"] = continue_val
        tomo_val = "1" if self.is_tomo else "0"
        params_dict["_rlnJobIsTomo"] = tomo_val
        return params_dict

    def write_jobstar(
        self,
        output_dir: str,
        output_fn: str = "job.star",
        is_continue: bool = False,
    ):
        """Write a job.star file.

        Args:
            output_dir (str): The output directory.
            output_fn (str): The name of the file to write. Defaults to job.star
            is_continue (bool): Is the file for a continuation of a previously run job?
                If so only the parameters that can be changed on continuation are
                written.  Overrules is_continue attribute of the job
        """
        self.add_compatibility_joboptions()
        filename = output_dir + output_fn
        jobstar = cif.Document()

        job_block = jobstar.add_new_block("job")
        job_block.set_pair("_rlnJobTypeLabel", str(self.PROCESS_NAME))
        continue_val = "1" if is_continue else str(int(self.is_continue))
        job_block.set_pair("_rlnJobIsContinue", continue_val)
        tomo_val = "1" if self.is_tomo else "0"
        job_block.set_pair("_rlnJobIsTomo", tomo_val)

        jobop_block = jobstar.add_new_block("joboptions_values")
        jobop_loop = jobop_block.init_loop(
            "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
        )

        rows = []
        for option in self.joboptions:
            if not is_continue:
                rows.append(
                    [
                        option,
                        str(self.joboptions[option].value),
                    ]
                )
            else:
                if self.joboptions[option].in_continue:
                    rows.append(
                        [
                            option,
                            str(self.joboptions[option].value),
                        ]
                    )
        rows.sort(key=lambda x: x[0])
        for row in rows:
            jobop_loop.add_row(row)

        write(jobstar, filename)

    def evaluate_qsub_template(self, sub_text: str) -> None:
        """Check that the qsub template is appropriate for the pipeliner

        Looks for the common differences between relion style sub script and pipeliner
        style. Issues warnings if suspected problems are found but still allows it to
        run.

        Args:
            sub_text (str): The text of the submission script
        """
        template = self.joboptions["qsubscript"].get_string()
        msg = f"Submission script template at '{template}'"

        # command substitution present
        if "XXXcommandXXX" not in sub_text:
            warnings.warn(
                f"{msg} does not contain the 'XXXcommandXXX' substitution variable and"
                " will not contain the job commands when executed",
                RuntimeWarning,
            )

        # no incorrect sub vars
        pattern = r"XXX\S*XXX"
        matches = re.findall(pattern, sub_text)
        allowed_subs = [
            "XXXmpinodesXXX",
            "XXXthreadsXXX",
            "XXXcoresXXX",
            "XXXdedicatedXXX",
            "XXXnameXXX",
            "XXXerrfileXXX",
            "XXXoutfileXXX",
            "XXXqueueXXX",
            "XXXcommandXXX",
        ]
        badvars = []
        for hit in matches:
            if not hit.startswith("XXXextra") and hit not in allowed_subs:
                badvars.append(hit)
        if badvars:
            baddies = "', '".join(badvars)
            warnings.warn(
                f"{msg} contains the substitution variable(s): '{baddies}' which are"
                " not recognised by the CCP-EM Pipeliner",
                RuntimeWarning,
            )

        # check for unsubstituted XXXextra
        extras_pattern = r"XXXextra(\d+)XXX"
        extras = re.findall(extras_pattern, sub_text)
        extras_numbers = [int(num) for num in extras]
        extra_count = user_settings.get_qsub_extra_count()
        bad_extras = [x for x in extras_numbers if x > extra_count]
        if bad_extras:
            baddies = "', '".join([f"XXXextra{x}XXX" for x in bad_extras])
            warnings.warn(
                f"{msg} contains extra substitution variables '{baddies}' but these"
                " have not been properly configured in PIPELINER_QSUB_EXTRA_COUNT,"
                f" which currently specifies {extra_count} extra qsub variables",
                RuntimeWarning,
            )
        if "XXXextra0XXX" in sub_text:
            warnings.warn(
                f"{msg} contains substitution variable XXXextra0XXX, which is invalid."
                " 'XXXextra' substitution variables begin at 1",
                RuntimeWarning,
            )

        # check the mpi run command is not present
        mpi_coms = ["mpirun -n ", "mpiexec -n "]
        jobop = self.joboptions.get("mpi_command")
        if jobop:
            mpi_coms.append(jobop.get_string())
        setmpicom = user_settings.get_mpirun_command()
        if setmpicom and not any([setmpicom.startswith(x) for x in mpi_coms]):
            mpi_coms.append(user_settings.get_mpirun_command())

        for com in mpi_coms:
            if com in sub_text:
                warnings.warn(
                    f"{msg} contains '{com}' which looks like an mpirun command. Unlike"
                    " RELION, for the CCP-EM Pipeliner this should be omitted from the"
                    " template and instead be given in the 'MPI run command' job"
                    " option.",
                    RuntimeWarning,
                )

    def save_job_submission_script(self, commands: list) -> str:
        """Writes a submission script for jobs submitted to a queue

        Args:
            commands (list): The job's commands. In a list of lists format

        Returns:
            str: The name of the submission script that was written

        Raises:
            ValueError: If no submission script template was specified in the job's
                joboptions
            ValueError: If the submission script template is not found
            RuntimeError: If the output script could not be written
        """
        output_script = Path(self.output_dir) / "run_submit.script"
        # error checking
        fn_qsub = self.joboptions["qsubscript"].get_string()
        if not os.path.isfile(fn_qsub):
            raise ValueError("Error reading template submission script in: " + fn_qsub)
        with open(fn_qsub) as template_text:
            self.evaluate_qsub_template(template_text.read())

        try:
            output_script.touch()
        except Exception as ex:
            raise RuntimeError(
                f"Error writing to job submission script {output_script}"
            ) from ex

        nr_mpi = self.get_nr_mpi()
        nr_threads = self.get_nr_threads()

        ncores = nr_mpi * nr_threads
        ndedi = self.joboptions["min_dedicated"].get_number()
        fnodes = ncores / ndedi
        nnodes = math.ceil(fnodes)
        # should this be written to the run.err as well?
        # should we set ndedi to ncores if ncores < ndedi?
        if fnodes % 1 != 0:
            logger.warning(
                "You're using {} MPI processes with {} threads each "
                "(i.e. {} cores), while asking for {} nodes with {} cores.\n"
                "It is more efficient to make the number of cores (i.e. mpi*threads)"
                " a multiple of the minimum number of dedicated cores per"
                " node ".format(
                    truncate_number(nr_mpi, 0),
                    truncate_number(nr_threads, 0),
                    truncate_number(ncores, 0),
                    truncate_number(nnodes, 0),
                    truncate_number(ndedi, 0),
                )
            )

        queuename = self.joboptions["queuename"].get_string()

        replacements = {
            "XXXmpinodesXXX": str(nr_mpi),
            "XXXthreadsXXX": str(nr_threads),
            "XXXcoresXXX": str(int(ncores)),
            "XXXdedicatedXXX": str(int(ndedi)),
            "XXXnameXXX": self.output_dir,
            "XXXerrfileXXX": os.path.relpath(
                os.path.join(self.output_dir, "run.err"), self.working_dir
            ),
            "XXXoutfileXXX": os.path.relpath(
                os.path.join(self.output_dir, "run.out"), self.working_dir
            ),
            "XXXqueueXXX": queuename,
            "XXXcommandXXX": "\n".join([shlex.join(com) for com in commands]),
        }

        extra_vals = []
        for n in range(1, user_settings.get_qsub_extra_count() + 1):
            extra_vals.append(self.joboptions.get(f"qsub_extra_{n}"))
        for n, extra_val in enumerate(extra_vals):
            if extra_val is not None:
                replacements[f"XXXextra{n+1}XXX"] = extra_val.get_string()

        with open(fn_qsub) as template_data:
            template = template_data.readlines()

        with open(output_script, "w") as outscript:
            for line in template:
                for sub in replacements:
                    if sub in line:
                        if replacements[sub] is not None:
                            line = line.replace(sub, replacements[sub])
                outscript.write(line)
        return str(output_script)

    def handle_doppio_uploads(self, dry_run=False) -> None:
        """Tasks that have to be performed to deal with Doppio file uploads.

        - Move files from DoppioUploads to the job dir:
              DoppioUploads/tmpdir/file -> JobType/jobNNN/InputFiles/file
        - Update the job option values to point to the new file locations, so when the
          job input nodes are created they refer to the moved files

        Args:
            dry_run: If True, do not actually try to move any files, just update the
                job option values. This option is only intended for use in testing.
        """
        input_files_dir = Path(self.output_dir) / "InputFiles"
        for jo in self.joboptions.values():
            if not jo.value:
                continue
            files: List[str] = []
            if isinstance(jo, MultiInputNodeJobOption) or (
                isinstance(jo, MultiExternalFileJobOption) and jo.allow_upload
            ):
                files = jo.get_list()

            elif isinstance(jo, InputNodeJobOption):
                files = [jo.get_string()]

            for f in files:
                assert isinstance(
                    jo,
                    (
                        InputNodeJobOption,
                        MultiInputNodeJobOption,
                        ExternalFileJobOption,
                        MultiExternalFileJobOption,
                    ),
                )
                src_path = Path(f)
                if src_path.parts[0] == "DoppioUploads" or not file_in_project(
                    str(src_path)
                ):
                    dest_path = input_files_dir / src_path.name
                    if dest_path.exists():
                        # If we can't move the file, delete it to avoid building up
                        # clutter in the DoppioUploads directory
                        was_del = ""
                        if not dry_run and src_path.parts[0] == "DoppioUploads":
                            src_path.unlink()
                            was_del = f" {src_path} was deleted."
                        raise ValueError(
                            f"Can't move {src_path} because the destination"
                            f" {dest_path} already exists.{was_del}"
                        )
                    if not dry_run:
                        input_files_dir.mkdir(exist_ok=True)

                        # move the files from DoppioUploads, copy others
                        if src_path.parts[0] == "DoppioUploads":
                            src_path.replace(dest_path)
                            try:
                                # Remove the temporary directory that held the file
                                src_path.parent.rmdir()
                            except Exception:
                                logger.exception(
                                    "An error occurred removing directory"
                                    f' "{src_path.parent}"'
                                )
                                traceback.print_exc()
                        else:
                            shutil.copy(src_path, dest_path)

                    jo.value = jo.value.replace(
                        os.fspath(src_path), os.fspath(dest_path)
                    )

    def prepare_to_run(self, ignore_invalid_joboptions=False) -> None:
        """Prepare the job to run.

        This function is intended to be called by the pipeliner before the job file is
        saved to disk. It does several things including:
        - Validate the job options
        - Make the job directory
        - Move uploaded Doppio user files into the job directory

        Args:
            ignore_invalid_joboptions (bool): Prepare the job to run anyway even if the
                job options appear to be invalid

        Raises:
            ValueError: If the job options appear to be invalid and
                ``ignore_invalid_joboptions`` is not set
            RuntimeError: If the job does not already have an output directory assigned
        """
        # Check for error conditions
        if self.output_dir is None or self.output_dir == "":
            raise RuntimeError(
                "Cannot prepare job to run because an output directory has not been"
                " assigned"
            )

        if not ignore_invalid_joboptions:
            results = self.validate_joboptions()
            errors = [str(result) for result in results if result.type == "error"]
            if errors:
                errors_str = "\n".join(errors)
                raise ValueError(
                    f"Cannot run job {self.output_dir} because the job options appear"
                    f" to be invalid:\n{errors_str}"
                )

        # Create output directory
        job_dir = Path(self.output_dir)
        job_dir.mkdir(parents=True, exist_ok=True)

        # Move any uploaded Doppio user files into the job directory
        self.handle_doppio_uploads()

        # Create the input and output nodes so they can be written to the pipeline file
        self.create_input_nodes()
        self.create_output_nodes()

        # Write the jobstar and runjob files
        self.write_runjob(self.output_dir)
        self.write_jobstar(self.output_dir)
        self.write_jobstar(".gui_" + self.PROCESS_NAME.replace(".", "_"))

        # Make empty log files
        (job_dir / "run.out").touch()
        (job_dir / "run.err").touch()

        # Write the continuation file - for continuing the job later
        continue_name = os.path.join(self.output_dir, "continue_")
        self.write_jobstar(continue_name, is_continue=True)

    def get_final_commands(self) -> List[List[str]]:
        """Assemble the commands to be run for a job.

        This function is intended to be called by the job runner just before the
        commands are run. Any setup required before the job starts should be done in
        ``prepare_to_run()``.

        Returns:
            The commands, in a lists of lists format. Each item in the main list is a
            single command composed of a list of strings (as used by ``subprocess.run``,
            i.e. [com, arg1, arg2, ...])
        """
        # Call the subclass implementation of get_commands() to get the commands to run
        final_commands = []

        # Make final alterations to the command list
        for command in self.get_commands():
            # Expand command names to fully qualified paths
            exe_path = shutil.which(command.com[0])
            if exe_path is not None:
                command.com[0] = exe_path

            # substitute any env vars found
            for n, comarg in enumerate(command.com):
                if "$" in comarg:
                    logger.info(
                        f"Found '$' symbol in command arg '{comarg}', trying to"
                        " substitute environment variables... ",
                    )
                    subbed = os.path.expandvars(comarg)
                    if subbed == comarg:
                        logger.info("No substitutions made")
                    else:
                        command.com[n] = subbed
                        logger.info(f"'{comarg}' replaced with '{subbed}'")

            # Add "--pipeline_control" arg to Relion commands that need it
            if command.relion_control:
                command.add_pipeline_control(self.output_dir)

            final_commands.append(command.com)

        return final_commands

    def get_runtab_options(
        self,
        mpi: bool = False,
        threads: bool = False,
        addtl_args: bool = False,
        mpi_default_min: int = 1,
        mpi_must_be_odd: bool = False,
    ) -> None:
        """Get the options found in the Run tab of the GUI,
        which are common to for all jobtypes

        Adds entries to the joboptions dict for queueing, MPI, threading, and
        additional arguments. This method should be used when initialising a
        :class:`~pipeliner.pipeliner_job.PipelinerJob` subclass

        Args:
            mpi (bool): Should MPI options be included?
            threads (bool): Should multi-threading options be included
            addtl_args (bool): Should and 'additional arguments' be added
            mpi_default_min (int): The minimum for the default number of MPIs, will be
                used if mpi_default_min > user defined min number of MPI
            mpi_must_be_odd (bool): Does the number of mpis have to be odd, like for
                relion refine_jobs.

        """
        if mpi:
            mpimax = user_settings.get_mpi_max()
            nrmpi = user_settings.get_default_nrmpi()
            nrmpi = nrmpi - 1 if not nrmpi % 2 and mpi_must_be_odd else nrmpi
            if mpimax:
                mpimax = mpimax - 1 if not mpimax % 2 and mpi_must_be_odd else mpimax
                nrmpi = nrmpi if nrmpi <= mpimax else mpimax
            mpi_default = nrmpi if nrmpi > mpi_default_min else mpi_default_min
            self.joboptions["nr_mpi"] = IntJobOption(
                label="Number of MPI procs:",
                default_value=mpi_default,
                hard_min=1,
                suggested_max=mpimax,
                step_value=1,
                help_text=(
                    "Number of MPI nodes to use in parallel. When set to 1, MPI will"
                    " not be used. The maximum can be set through the environment"
                    " variable PIPELINER_MPI_MAX and the default can be set using"
                    " PIPELINER_DEFAULT_NRMPI."
                ),
                in_continue=True,
                jobop_group="Running options",
            )
            mpirun_cmd = user_settings.get_mpirun_command()
            self.joboptions["mpi_command"] = StringJobOption(
                label="MPI run command:",
                default_value=mpirun_cmd,
                help_text=(
                    "This command will be prepended to jobs that use MPI. The number of"
                    " MPIs specified in 'Number of MPI procs:' will be substituted for"
                    " 'XXXmpinodesXXX' in the final command. The default can be set"
                    " through the environment variable PIPELINER_MPIRUN_COMMAND. Note"
                    " that the RELION_MPI_RUN environment variable is ignored."
                ),
                in_continue=True,
                required_if=JobOptionCondition([("nr_mpi", ">", 1)]),
                deactivate_if=JobOptionCondition([("nr_mpi", "=", 1)]),
                jobop_group="Running options",
            )

        if threads:
            threadmax = user_settings.get_thread_max()
            default_nrthreads = user_settings.get_default_nrthreads()
            self.joboptions["nr_threads"] = IntJobOption(
                label="Number of threads:",
                default_value=default_nrthreads,
                hard_min=1,
                suggested_max=threadmax,
                step_value=1,
                help_text=(
                    "Number of shared-memory (POSIX) threads to use in"
                    " parallel. When set to 1, no multi-threading will"
                    " be used. The maximum can be set through the environment"
                    " variable PIPELINER_THREAD_MAX and the default can be set using"
                    " PIPELINER_DEFAULT_NRTHREADS."
                ),
                in_continue=True,
                jobop_group="Running options",
            )

        self.make_queue_options()

        if addtl_args:
            self.make_additional_args()

    def is_submit_to_queue(self) -> bool:
        do_queue_jo = self.joboptions.get("do_queue")
        if do_queue_jo is not None:
            do_queue = do_queue_jo.get_boolean()
        else:
            do_queue = False
        return do_queue

    def get_nr_mpi(self) -> int:
        nr_mpi_jo = self.joboptions.get("nr_mpi")
        if nr_mpi_jo is not None:
            nr_mpi = int(nr_mpi_jo.get_number())
        else:
            nr_mpi = 1
        return nr_mpi

    def get_nr_threads(self) -> int:
        nthr_jo = self.joboptions.get("nr_threads")
        if nthr_jo is not None:
            nthr = int(nthr_jo.get_number())
        else:
            nthr = 1
        return nthr

    def get_mpi_command(self) -> List[Union[int, float, str]]:
        nr_mpi = self.get_nr_mpi()

        if not self.is_submit_to_queue():
            warn = user_settings.get_warning_local_mpi()
            if warn is not None and nr_mpi > warn:
                errmsg = (
                    f"WARNING: you're submitting a local job with {nr_mpi} parallel MPI"
                    f" processes. Only {warn} are allowed by the"
                    f" PIPELINER_WARNING_LOCAL_MPI environment variable. This might"
                    f" cause the job to fail."
                )
                logger.error(errmsg)
                with open(os.path.join(self.output_dir, "run.err"), "a") as errfile:
                    errfile.write(errmsg)

        mpi_str = self.joboptions["mpi_command"].get_string()
        update_mpi_str = mpi_str.replace("XXXmpinodesXXX", str(nr_mpi))
        final_com: List[Union[int, float, str]] = []
        for com in shlex.split(update_mpi_str):
            final_com.append(com)
        return final_com

    def make_additional_args(self) -> None:
        """Get the additional arguments job option"""
        self.joboptions["other_args"] = StringJobOption(
            label="Additional arguments:",
            default_value="",
            help_text="",
            in_continue=True,
            jobop_group="Running options",
        )

    def make_queue_options(self) -> None:
        """Get options related to queueing and queue submission,
        which are common to for all jobtypes"""

        queue_default = user_settings.get_queue_use()

        self.joboptions["do_queue"] = BooleanJobOption(
            label="Submit to queue?",
            default_value=queue_default,
            help_text=(
                "If set to Yes, the job will be submit to a queue,"
                " otherwise the job will be executed locally. Note "
                "that only MPI jobs may be sent to a queue. The default "
                "can be set through the environment variable PIPELINER_QUEUE_USE."
            ),
            in_continue=True,
            jobop_group="Queue submission options",
        )

        queue_name_default = user_settings.get_queue_name()
        self.joboptions["queuename"] = StringJobOption(
            label="Queue name:",
            default_value=queue_name_default,
            help_text=(
                "Name of the queue to which to submit the job. The"
                " default name can be set through the environment"
                " variable PIPELINER_QUEUE_NAME."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_queue", "=", False)]),
            required_if=JobOptionCondition([("do_queue", "=", True)]),
            jobop_group="Queue submission options",
        )

        qsub_default = user_settings.get_qsub_command()
        self.joboptions["qsub"] = StringJobOption(
            label="Queue submit command:",
            default_value=qsub_default,
            help_text=(
                "Name of the command used to submit scripts to the queue,"
                " e.g. qsub or bsub. Note that the person who installed PIPELINER"
                " should have made a custom script for your cluster/queue setup."
                " Check this is the case (or create your own script"
                " if you have trouble submitting jobs. The default "
                "command can be set through the environment variable "
                "PIPELINER_QSUB_COMMAND."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_queue", "=", False)]),
            required_if=JobOptionCondition([("do_queue", "=", True)]),
            validation_regex="^[\\S]+$",
            regex_error_message="Cannot contain spaces",
            jobop_group="Queue submission options",
        )

        qsub_template_default = user_settings.get_qsub_template()
        self.joboptions["qsubscript"] = ExternalFileJobOption(
            label="Standard submission script:",
            default_value=qsub_template_default,
            help_text=(
                "The template for your standard queue job submission script. Its"
                " default location may be changed by setting the environment variable"
                " PIPELINER_QSUB_TEMPLATE. In the template script a number of variables"
                " will be replaced: \nXXXcommandXXX = relion command + arguments;"
                " \nXXXqueueXXX = The queue name;\nXXXmpinodesXXX = The number of MPI"
                " nodes;\nXXXthreadsXXX = The number of threads;\nXXXcoresXXX ="
                " XXXmpinodesXXX * XXXthreadsXXX;\nXXXdedicatedXXX = The minimum number"
                " of dedicated cores on each node;\nXXXnodesXXX = The number of"
                " requested nodes = CEIL(XXXcoresXXX / XXXdedicatedXXX);\nIf these"
                " options are not enough for your standard jobs, you may define a"
                " user-specified number of extra variables: XXXextra1XXX, XXXextra2XXX,"
                " etc. The number of extra variables is controlled through the"
                " environment variable PIPELINER_QSUB_EXTRA_COUNT. Their help text is"
                " set by the environment variables PIPELINER_QSUB_EXTRA1,"
                " PIPELINER_QSUB_EXTRA2, etc For example, setenv"
                " PIPELINER_QSUB_EXTRA_COUNT 1, together with setenv"
                " PIPELINER_QSUB_EXTRA1 'Max number of hours in queue' will result in"
                " an additional (text) ein the GUI Any variables XXXextra1XXX in the"
                " template script will be replaced by the corresponding value.Likewise,"
                " default values for the extra entries can be set through environment"
                " variables PIPELINER_QSUB_EXTRA1_DEFAULT,"
                " PIPELINER_QSUB_EXTRA2_DEFAULT, etc. But note that (unlike all other"
                " entries in the GUI) the extra values are not remembered from one run"
                " to the other."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_queue", "=", False)]),
            required_if=JobOptionCondition([("do_queue", "=", True)]),
            jobop_group="Queue submission options",
        )

        min_cores_default = user_settings.get_minimum_dedicated()
        self.joboptions["min_dedicated"] = IntJobOption(
            label="Minimum dedicated cores per node:",
            default_value=min_cores_default,
            suggested_min=1,
            suggested_max=64,
            step_value=1,
            help_text=(
                "Minimum number of dedicated cores that need to be requested "
                "on each node. This is useful to force the queue to fill up entire "
                "nodes of a given size. The default can be set through the environment"
                " variable PIPELINER_MINIMUM_DEDICATED."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_queue", "=", False)]),
            required_if=JobOptionCondition([("do_queue", "=", True)]),
            jobop_group="Queue submission options",
        )

        self.get_extra_options()

    def get_extra_options(self) -> None:
        """Get user specified extra queue submission options"""
        do_queue_option = self.joboptions.get("do_queue")
        if do_queue_option is None:
            return

        n_extra = user_settings.get_qsub_extra_count()
        if n_extra > 0:
            for extranum in range(1, n_extra + 1):
                extras = user_settings.get_qsub_extras(extranum)
                self.joboptions["qsub_extra_{}".format(extranum)] = StringJobOption(
                    label=extras["name"],
                    default_value=extras["default"],
                    help_text=extras["help"],
                    in_continue=True,
                    jobop_group="Queue submission options",
                    deactivate_if=JobOptionCondition([("do_queue", "=", False)]),
                )

    def gather_metadata(self) -> Dict[str, Any]:
        """
        Placeholder function for metadata gathering

        Each job class should define this individually

        Returns:
            dict: A placeholder "No metadata available" and the reason why

        """

        return {"No metadata": "No metadata available for this job type"}

    def prepare_clean_up_lists(
        self, do_harsh: bool = False
    ) -> Tuple[List[str], List[str]]:
        """
        Placeholder function for preparation of list of files to clean up

        Each job class should define this individually

        Args:
            do_harsh (bool): Should a harsh cleanup be performed

        Returns:
            tuple: Two empty lists ``([files, to, delete], [dirs, to, delete])``
        """
        if do_harsh:
            return [], []

        return [], []

    def create_input_nodes(self) -> None:
        """Automatically add the job's input nodes to its input node list.

        Input nodes are created from each of the job's job options.
        """
        curr_in_nodes = [(x.name, x.type) for x in self.input_nodes]
        for jop in self.joboptions.values():
            nodes = jop.get_input_nodes()
            for node in nodes:
                node_exists = (node.name, node.type) in curr_in_nodes
                if not node_exists:
                    self.input_nodes.append(node)

    def add_output_node(
        self, file_name: str, node_type: str, keywords: Optional[List[str]] = None
    ) -> None:
        """Helper function to add a new Node for a file in the job's output directory.

        This is a wrapper around ``node_factory.create_node`` which simply adds
        ``self.output_dir`` to the start of the file name before creating the node and
        adding it to ``self.output_nodes``.

        Args:
            file_name: The name of the file that the new node will refer to. It is
                assumed that the file will be written to the job'a output directory.
                Note that the existence of the file is not checked, because this method
                will usually be called before the job has run.
            node_type: The top-level type for the new node. This should almost always
                be one of the constants defined in ``pipeliner.nodes``.
            keywords: A list of keywords to append to the node type.
        """
        file_path = os.path.join(self.output_dir, file_name)
        node = create_node(filename=file_path, nodetype=node_type, kwds=keywords)
        self.output_nodes.append(node)

    def create_output_nodes(self) -> None:
        """Make the job's output nodes.

        This method should be overridden by PipelinerJob subclasses.

        The output nodes should be added to the list in the ``output_nodes`` attribute.
        The ``add_output_node`` function is helpful to create and add a new node in a
        single call.

        If your job doesn't make any output nodes, or doesn't know what their names will
        be until the job has been run, you still need to override this method but your
        implementation can simply ``pass`` and do nothing. If you need to add output
        nodes at the end of the job, create them in ``create_post_run_output_nodes``.

        Note that this method is called by the job manager (via
        ``PipelinerJob.prepare_to_run``) before the job is added to the pipeline. The
        job's output directory does exist when this method is called, but that could
        change in future versions of the pipeliner and jobs should avoid making any file
        system changes in this method.
        """
        raise NotImplementedError(
            "The create_output_nodes method must be overridden in subclasses of"
            " PipelinerJob"
        )

    def get_commands(self) -> List[PipelinerCommand]:
        """Get the commands to be run for a specific job.

        This method should be overridden by PipelinerJob subclasses.

        Jobs are normally run with the project directory as the working directory. If
        your job needs to run in a different working directory (for example if it calls
        a program which always writes files into the current directory), set the
        ``self.working_dir`` attribute in this method.

        Note that this method should run quickly! Any long-running actions should be
        done in one of the job's commands instead. (If necessary, put Python code that
        needs to be run into a separate script in ``pipeliner.scripts.job_scripts`` and
        then call it as a command.)

        Returns:
            The commands as a list of PipelinerCommand objects
        """
        raise NotImplementedError(
            "The get_commands method must be overridden in subclasses of PipelinerJob"
        )

    def create_post_run_output_nodes(self):
        """
        Placeholder function for post run node creation

        Some jobs have output nodes that can only be created after the job has run
        because their names are not known until after they have been created. They can
        be added here. This function should ONLY add output nodes; any other work should
        be done in commands run by the job.
        """

        pass

    def get_current_output_nodes(self) -> List[Node]:
        """
        Get the current output nodes if the job was stopped prematurely

        For most jobs there will not be any but for jobs with many iterations
        the most recent interation can be used if the job is aborted or failed
        and then later marked as successful

        Returns:
            list: of :class:`~pipeliner.nodes.Node` objects
        """

        return []

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        """Advanced validation of job parameters

        This is a placeholder function for additional validation to be done by
        individual job subtypes, such as comparing JobOption values IE:
        JobOption A must be > JobOption B

        Avoid using self.get_string or self.get_number in this function as they may
        raise an error if the JobOption is required and has no value. Use
        self.joboptions["jobopname"].value.

        returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        return []

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        """Make sure all the joboptions meet their validation criteria

        Returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects

        """
        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]
            errs = jo.validate()
            errors.extend(errs)

        # some basic validation errors may cause error in the advanced validation
        # so these are wrapped in a try/except
        found_basic_errors = len(errors) > 0
        try:
            adval = self.additional_joboption_validation()
            adval = adval if adval is not None else []
            if len(adval) > 0:
                errors.extend(adval)
        except ValueError:
            if found_basic_errors:
                pass
            else:
                raise

        try:
            dynamic_req_errors = self.validate_dynamically_required_joboptions()
            if len(dynamic_req_errors) > 0:
                errors.extend(dynamic_req_errors)
        except ValueError:
            if found_basic_errors:
                pass
            else:
                raise

        return errors

    def check_joboption_is_now_deactivated(self, jo: str) -> bool:
        """Check if a joboption has become deactivated in relation to others

        For example if job option A is False, job option B is now deactiavted

        Args:
            jo (str): The name of the JobOption to test
        Returns:
            bool: Has the JobOption been deactivated
        """
        jobop = self.joboptions[jo]
        is_deact = False
        if jobop.deactivate_if:
            is_deact = jobop.deactivate_if.check_condition_is_met(self.joboptions)[0]
        return is_deact

    def check_joboption_is_now_required(self, jo: str) -> list:
        """Check if a joboption has become required in relation to others

        For example if job option A is True, job option B is now required

        Args:
            jo (str): The name of the joboption to test
        Returns:
            list: :class:`pipeliner.job_options.JobOptionValidationResult`:
                for any errors found
        """
        jobop = self.joboptions[jo]
        if jobop.required_if is None:
            return []

        errors = []
        is_now_req, conditions = jobop.required_if.check_condition_is_met(
            self.joboptions
        )[:2]
        if is_now_req and jobop.value in [None, ""]:
            # Create a user-friendly message to explain the cause
            cause_msgs = []
            for con in conditions:
                operator = con[1]
                con_val = con[2]
                cause_jo = self.joboptions[con[0]]

                # Special handling for empty strings
                if con_val == "":
                    if operator == "=":
                        cause_desc = "is blank"
                    elif operator == "!=":
                        cause_desc = "is not blank"
                    else:
                        raise ValueError(
                            f'Cannot use operator "{operator}" for a string value'
                        )
                # Special handling for booleans
                elif con_val is True:
                    if operator == "=":
                        cause_desc = "= Yes"
                    elif operator == "!=":
                        cause_desc = "= No"
                elif con_val is False:
                    if operator == "=":
                        cause_desc = "= No"
                    elif operator == "!=":
                        cause_desc = "= Yes"
                elif " " in str(con_val):
                    # Only put quotes around value if it contains spaces
                    cause_desc = f'{operator} "{con_val}"'
                else:
                    # Otherwise do not use quotes, to reduce clutter
                    cause_desc = f"{operator} {con_val}"
                cause_msg = f'"{cause_jo.label}" {cause_desc}'
                cause_msgs.append(cause_msg)
            msg = "This option is required because " + " and ".join(cause_msgs)
            errors.append(JobOptionValidationResult("error", [jobop], msg))
        return errors

    def validate_dynamically_required_joboptions(
        self,
    ) -> List[JobOptionValidationResult]:
        """Check all joboptions if they have become required because of if_required

        For example if job option A is True, job option B is now required

        Returns:
            list: :class:`pipeliner.job_options.JobOptionValidationResult`:
                for any errors found
        """

        errors = []
        for jo in self.joboptions:
            now_req_errors = self.check_joboption_is_now_required(jo)
            if now_req_errors:
                errors.extend(now_req_errors)
        return errors

    def validate_input_files(self) -> List[JobOptionValidationResult]:
        """Check that files specified as inputs actually exist

        Returns:
            list: A list of :class:`pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]
            is_req = bool(self.check_joboption_is_now_required(joname))
            deactivated = self.check_joboption_is_now_deactivated(joname)
            if (not deactivated and jo.value) or is_req:
                if jo.joboption_type in [
                    "FILENAME",
                    "INPUTNODE",
                    "MULTIFILENAME",
                    "MULTIINPUT_NODE",
                    "SEARCHSTRING",
                ]:
                    errs = jo.check_file()
                    errors.extend(errs)

                if isinstance(jo, DirPathJobOption):
                    if jo.must_exist:
                        errors.extend(jo.check_dir())

        return errors

    def prepare_deposition_data(self, depo_type: str) -> Sequence[
        Union[
            EmpiarRefinedParticles,
            EmpiarParticles,
            EmpiarCorrectedMics,
            EmpiarMovieSet,
            OneDepData,
        ]
    ]:
        """Placeholder for function to return deposition data objects

        The specific list returned should be defined by each jobtype

        Args:
            depo_type (str): EMPIAR or ONEDEP

        Returns:
            list: The deposition object(s) returned by the specific job.  These
                need to be of the types defined in
                `pipeliner.deposition_tools.onedep_deposition` and
                `pipeliner.deposition_tools.empiar_deposition`
        """
        if depo_type not in ["EMPIAR", "ONEDEP"]:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )
        else:
            return []

    def parse_additional_args(self) -> List[str]:
        """Parse the additional arguments job option and return a list

        Returns:
            list: A list ready to append to the command.  Quotated strings are preserved
                as quoted strings all others are split into individual items
        """

        return shlex.split(self.joboptions["other_args"].get_string())

    def get_additional_reference_info(self) -> List[Ref]:
        """A placeholder function for job that need to return additional references

        This if for references that are not included in self.job info, such as ones
        pulled from the EMDB/PDB in fetch jobs
        """
        return []

    def _get_results_display_files(self) -> Sequence[Path]:
        """Return a list of the current results display files for this job"""
        return sorted(Path(self.output_dir).glob(".results_display*.json"))

    def load_results_display_files(self) -> Sequence[ResultsDisplayObject]:
        """Load the job's results display objects from files on disk.

        This method must be fast because it is used by the GUI to load job results.
        Therefore, if a display object fails to load properly, no attempt is made to
        recalculate it and a ``ResultsDisplayPending`` object is returned instead.

        If there are no results display files yet, an empty list is returned.

        Returns:
            A list of :class:`~pipeliner.results_display_objects.ResultsDisplayObject`
        """
        rd_files = self._get_results_display_files()
        rdos = []
        for rd_file in rd_files:
            try:
                with open(rd_file, "r") as dfile:
                    ddict = json.load(dfile)
                    rdos.append(create_results_display_object(**ddict))
            except Exception as ex:
                rdos.append(
                    ResultsDisplayPending(
                        message="Error loading results display",
                        reason=f"Error reading {rd_file}: {ex}",
                    )
                )
        return rdos

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """Create results display objects to be displayed by the GUI

        This default implementation simply creates the default results display object
        for each of the job's output nodes. Subclasses that want customised results
        should override this method.

        Returns:
            A list of :class:`~pipeliner.results_display_objects.ResultsDisplayObject`
        """
        rdos = []
        for node in self.output_nodes:
            dispo = node.default_results_display(self.output_dir)
            rdos.append(dispo)
        return rdos

    def save_results_display_files(self) -> Sequence[ResultsDisplayObject]:
        """Create new results display objects and save them to disk.

        This method removes any existing results display files first, and returns the
        new display objects after they have been created and saved.

        Returns:
            The newly-created results display objects.
        """
        self._remove_results_display_files()
        try:
            rdos = self.create_results_display()
        except Exception as ex:
            tb = str(traceback.format_exc())
            rdos = [
                ResultsDisplayPending(
                    message="Error creating results display",
                    reason=f"{str(ex)}\n{tb}",
                )
            ]
        for rdo in rdos:
            rdo.write_displayobj_file(self.output_dir)
        return rdos

    def _remove_results_display_files(self) -> None:
        """Remove all existing results display files from this job

        Note that this method removes the job results display files and the Thumbnails
        directory that contains processed files linked to the display files, but it does
        not remove the NodeDisplay directory that contains default display files for
        the job's output nodes.
        """
        for results_file in self._get_results_display_files():
            results_file.unlink()
        shutil.rmtree(Path(self.output_dir) / "Thumbnails", ignore_errors=True)

    def set_joboption_order(self, new_order=List[str]) -> None:
        """Replace the joboptions dict with an ordered dict

        Use this to set the order the joboptions will appear in the GUI. If a joboption
        is not specified in the list it will be tagged on to the end of the list.

        Args:
            new_order (list[str]): A list of joboption keys, in the order they should
                appear

        Raises:
            ValueError: If a nonexistent joboption is specified
        """
        # error checking
        jo_keys = list(self.joboptions)
        bad = []
        for njo in new_order:
            if njo not in jo_keys:
                bad.append(njo)
        if bad:
            raise ValueError(f"{bad} not in self.joboptions dict")

        # make the ordered dict
        ordered_jos = [self.joboptions[x] for x in new_order]
        ordered_jodict = OrderedDict(zip(new_order, ordered_jos))

        # add any not included in the list
        for jokey in jo_keys:
            if jokey not in ordered_jodict:
                ordered_jodict[jokey] = self.joboptions[jokey]

        self.joboptions = ordered_jodict

    def get_joboption_groups(self) -> Dict[str, List[str]]:
        """Put the joboptions in groups according to their jobop_group attribute

        Assumes that the joboptions have already been put in order of priority by
        self.set_joboption_order() or were in order to begin with.

        Groups are ordered based on the highest priority joboption in that group from
        the order of the joboptions, except that "Main" is always the first group.
        Joboptions within the groups are ordered by priority.

        Returns:
            Dict[str, List[str]]: The joboptions groupings {group: [jopbop, ... jobop]}
        """

        groupings: Dict[str, List[str]] = {"Main": []}
        for jo in self.joboptions:
            group = self.joboptions[jo].jobop_group
            if group in groupings:
                groupings[group].append(jo)
            else:
                groupings[group] = [jo]

        return groupings

    def get_job_subdirs(self):
        """Get all the subdirectories contained in the jobdir excluding the NodeDisplay

        returns List[str]: The dirs
        """
        subdirs = [
            dirpath
            for (dirpath, _, _) in os.walk(self.output_dir, topdown=False)
            if dirpath not in [self.output_dir, NODE_DISPLAY_DIR]
        ]
        return subdirs
