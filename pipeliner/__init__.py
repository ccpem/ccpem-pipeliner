from importlib_metadata import version
import logging
from . import env_setup

logger = logging.getLogger(__name__)
__version__ = version("pipeliner")

# Set up python environment for project
try:
    env_setup.setup_python_environment()
except Exception:
    logger.warning("Unable to set up pipeliner python environment")
