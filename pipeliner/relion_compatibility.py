#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""
---------------------------
Star file reading utilities
---------------------------

Tools for reading and modifying star files
"""
import os
import shutil

from gemmi import cif
import logging
from collections import OrderedDict
from typing import Tuple, Optional, List, Union, Any, Dict, Callable, Mapping
from pipeliner.star_writer import write
from pipeliner.utils import decompose_pipeline_filename
from pipeliner.job_options import JobOption

from pipeliner.data_structure import (
    TRUES,
    NODE_OPTIMISERDATA,
    NODE_RIGIDBODIES,
    NODE_PROCESSDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MASK3D,
    NODE_LOGFILE,
    NODE_IMAGE2DGROUPMETADATA,
    MANUALPICK_JOB_NAME,
    EXTRACT_PARTICLE_NAME,
    CLASS2D_PARTICLE_NAME,
    CLASS3D_PARTICLE_NAME,
    REFINE3D_PARTICLE_NAME,
    CTFREFINE_REFINE_NAME,
    BAYESPOLISH_POLISH_NAME,
    IMPORT_RELIONSTYLE_NAME,
    MOTIONCORR_RELIONSTYLE_NAME,
    CTFFIND_RELIONSTYLE_NAME,
    AUTOPICK_RELIONSTYLE_NAME,
    SELECT_RELIONSTYLE_NAME,
    JOINSTAR_RELIONSTYLE_NAME,
    SUBTRACT_RELIONSTYLE_NAME,
    LOCALRES_RELIONSTYLE_NAME,
    MULTIBODY_RELIONSTYLE_NAME,
    IMPORT_DIR,
    MOTIONCORR_DIR,
    CTFFIND_DIR,
    MANUALPICK_DIR,
    AUTOPICK_DIR,
    EXTRACT_DIR,
    SELECT_DIR,
    CLASS2D_DIR,
    CLASS3D_DIR,
    REFINE3D_DIR,
    MASKCREATE_DIR,
    JOINSTAR_DIR,
    SUBTRACT_DIR,
    POSTPROCESS_DIR,
    LOCALRES_DIR,
    INIMODEL_DIR,
    MULTIBODY_DIR,
    BAYESPOLISH_DIR,
    CTFREFINE_DIR,
    EXTERNAL_DIR,
    AUTOPICK_LOG_HELICAL_NAME,
    AUTOPICK_LOG_NAME,
    AUTOPICK_REF3D_NAME,
    AUTOPICK_TOPAZ_NAME,
    AUTOPICK_TOPAZ_TRAIN_NAME,
    AUTOPICK_TOPAZ_HELICAL_NAME,
    AUTOPICK_REF2D_HELICAL_NAME,
    AUTOPICK_REF3D_HELICAL_NAME,
    AUTOPICK_REF2D_NAME,
    CLASS2D_PARTICLE_NAME_EM,
    CLASS2D_PARTICLE_NAME_VDAM,
    CLASS2D_HELICAL_NAME_EM,
    CLASS3D_HELICAL_NAME,
    CLASS2D_HELICAL_NAME_VDAM,
    CTFFIND_GCTF_NAME,
    CTFFIND_CTFFIND4_NAME,
    EXTRACT_HELICAL_NAME,
    IMPORT_OTHER_NAME,
    IMPORT_MOVIES_NAME,
    INIMODEL_JOB_NAME,
    LOCALRES_RESMAP_NAME,
    LOCALRES_OWN_NAME,
    MANUALPICK_HELICAL_NAME,
    BAYESPOLISH_TRAIN_NAME,
    MOTIONCORR_OWN_NAME,
    MOTIONCORR_MOTIONCOR2_NAME,
    POSTPROCESS_JOB_NAME,
    REFINE3D_HELICAL_NAME,
    SELECT_DISCARD_NAME,
    SELECT_INTERACTIVE_NAME,
    SELECT_ONVALUE_NAME,
    SELECT_SPLIT_NAME,
    SELECT_AUTO2D_NAME,
    MASKCREATE_JOB_NAME,
    SUBTRACT_JOB_NAME,
    SUBTRACT_REVERT_NAME,
    JOINSTAR_MICS_NAME,
    JOINSTAR_MOVIES_NAME,
    JOINSTAR_PARTS_NAME,
    MULTIBODY_REFINE_NAME,
    PROC_NUM_2_GENERAL_NAME,
    STATUS2LABEL,
    NODE_INT2NODELABEL,
)

from pipeliner.star_keys import (
    GENERAL_BLOCK,
    PROCESS_BLOCK,
    PROCESS_PREFIX,
    PROCESS_SUFFIXES,
    PROCESS_SUFFIXES_OLD,
    NODE_BLOCK,
    NODE_PREFIX,
    NODE_SUFFIXES,
    NODE_SUFFIXES_OLD,
    INPUT_EDGE_BLOCK,
    INPUT_EDGE_PREFIX,
    INPUT_EDGE_SUFFIXES,
    OUTPUT_EDGE_BLOCK,
    OUTPUT_EDGE_PREFIX,
    OUTPUT_EDGE_SUFFIXES,
)

logger = logging.getLogger(__name__)

relion4_to_pipeliner_node_conversions = {
    "MicrographsData": NODE_MICROGRAPHGROUPMETADATA,
    "MicrographsCoords": NODE_MICROGRAPHCOORDSGROUP,
    "MicrographMoviesData": NODE_MICROGRAPHMOVIEGROUPMETADATA,
    "Images2DData": NODE_IMAGE2DGROUPMETADATA,
    "ParticlesData": NODE_PARTICLEGROUPMETADATA,
}


def jobstar_options_as_dict(fn_in: str) -> Dict[str, str]:
    """Return all options of a job.star file as a dict."""
    jobstar = cif.read_file(fn_in)
    jobdata_block = jobstar.find_block("job")
    is_continue = jobdata_block.find_pair("_rlnJobIsContinue")
    is_tomo = jobdata_block.find_pair("_rlnJobIsTomo")
    jobtype = jobdata_block.find_pair("_rlnJobTypeLabel")
    if not jobtype:
        jobtype = jobdata_block.find_pair("_rlnJobType")

    jobops_dict = {}
    for i in is_continue, is_tomo, jobtype:
        jobops_dict[i[0]] = i[1]

    jobops_block = jobstar.find_block("joboptions_values")
    for line in jobops_block.find("_rln", ["JobOptionVariable", "JobOptionValue"]):
        jobops_dict[cif.as_string(line[0])] = cif.as_string(line[1])
    return jobops_dict


# Additional job name conversions, for example where RELION 5 job names don't match
# pipeliner or deprecated job names from older versions of pipeliner

ad_hoc_job_name_conversions = {
    "relion.extract.reextract": "relion.extract",  # RELION 5 - pipeliner mismatch
    "relion.ctfrefine.anisomag": "relion.ctfrefine",  # RELION 5 - pipeliner mismatch
    "relion.import.other": "relion.import",  # Old pipeliner versions
}


def additional_job_conversions(jobname: str) -> str:
    """Additional job name conversions,

    For example: Where RELION 5 job names don't match pipeliner ordeprecated job names
    from older versions of pipeliner

    Args:
        jobname (str): The job name

    Returns:
        str: The converted job name or the original if not converted
    """

    if jobname in ad_hoc_job_name_conversions:
        return ad_hoc_job_name_conversions[jobname]
    return jobname


def get_job_type(job_options_dict: dict) -> Tuple[str, str]:
    """Get the job type from a job.star file

    Includes back compatibility with the old naming convention in RELION
    3.1 files (_rlnJobType vs _rlnJobTypeLabel)

    Args:
        job_options_dict (dict): A dict of the parameters from a job.star file in
            the format ``{param_name: param_value}``

    Returns:
        tuple: (job_type (:class:`str`), job_type_label (:class:`str`))

        The job type is the pipeliner format string job type. The job_type_label
        is the label used in the star file to designate the job type, which is
        different between RELION 3.x and RELION 4.0/Pipeliner
    """
    # new style ID
    job_type = job_options_dict.get("_rlnJobTypeLabel")
    job_type_label = "_rlnJobTypeLabel"
    if job_type is None:
        job_type_label = "_rlnJobType"

        # for back compatibility with 3.1
        try:
            jt_str = job_options_dict.get("_rlnJobType")
            if jt_str:
                job_type = int(jt_str.strip())
                gen_job_type = PROC_NUM_2_GENERAL_NAME[
                    job_type
                ]  # for back compatibility
                job_type = convert_proctype(gen_job_type, job_options_dict)[0]

        # for back compatibility with mixed files - old style label - new style ID
        except ValueError:
            jt_str = job_options_dict.get("_rlnJobType")
            if not jt_str:
                raise ValueError(f"_rlnJobType not found in {job_options_dict}")
            job_type = jt_str.strip()

    return job_type, job_type_label


def relion31_jobtype_conversion(val, joboptions):
    gen_jobtype = PROC_NUM_2_GENERAL_NAME.get(int(val))
    if not gen_jobtype:
        raise ValueError(f"Can't find pipeliner job type for {val}")
    return convert_proctype(gen_jobtype, joboptions)[0]


def convert_relion3_1_block(
    prefix, suffixes, suffixes_old, block, errors
) -> List[Tuple[str, str]]:
    """subfunction converts single relion3.1 pipeline starfile block to pipeliner

    overwrites the block in the gemmi.cif

    Args:
        prefix (str): The first part of the node name
        suffixes (list): Keywords to be appended to the converted node name
        suffixes_old (list): Keywords on the original node
        block (gemmi.cif.Block): The block to be converted
        errors (list): A running list of errors encountered contains Tuples:
            (name of node it was attempting to convert, which it was attempting to
            convert it to)

    Returns:
        list: the updated errors list List[Tuple[str,str]]
    """
    oldstyle_table = list(block.find(prefix, suffixes_old))
    new_table: List[Union[Tuple[str, str], List[Union[str, Any]]]] = []
    # conversions for nodes
    if prefix == NODE_PREFIX:
        # replace the node types with labels
        for node in oldstyle_table:
            nodename = convert_oldstyle_names(cif.as_string(node[0]))
            if not nodename[1]:
                errors.append([node[0], nodename[0]])
            new_table.append(convert_31_pipeline_node(nodename, cif.as_string(node[1])))

    # conversions for processes
    if prefix == PROCESS_PREFIX:
        for proc in oldstyle_table:
            try:
                conv_type = convert_proctype(proc[0])
            except Exception as e:
                newtype = "UNKNOWN"
                conv_type = (
                    f"Error determining process type: {e}",
                    False,
                )
            if conv_type[1]:
                newtype = conv_type[0]

            elif not conv_type[1]:
                if PROC_NUM_2_GENERAL_NAME[int(proc[2])] == "Select":
                    conv_type = ("relion.select.interactive", True)
                    newtype = conv_type[0]
                else:
                    newtype = PROC_NUM_2_GENERAL_NAME[int(proc[2])] + ".AMBIGUOUS_TYPE"

            if not conv_type[1]:
                errors.append([proc[0], conv_type[0]])
            new_table.append([proc[0], proc[1], newtype, STATUS2LABEL[int(proc[3])]])

    if prefix in (OUTPUT_EDGE_PREFIX, INPUT_EDGE_PREFIX):
        new_table = [
            (
                convert_oldstyle_names(cif.as_string(row[0])),
                convert_oldstyle_names(cif.as_string(row[1])),
            )
            for row in oldstyle_table
        ]

    # replace the loop with the converted
    new_loop = block.init_loop(prefix, suffixes)
    for line in new_table:
        new_loop.add_row(line)

    return errors


def convert_relion3_1_pipeline(fn_in, display_warning: bool = True):
    """Convert a pipeline from RELION 3.x to pipeliner format

    Backs up the original file as <filename>.old

    Args:
        fn_in (str): Name of the file to convert
        display_warning (bool): Should a warning be displayed on screen?
    """

    if display_warning:
        logger.warning(
            "WARNING: The pipeline appears to be a older version of the "
            "pipeline.star format\nAttempting to convert it..."
        )

    # read node and process blocks
    cifdoc = cif.read_file(fn_in)

    node_block = cifdoc.find_block(NODE_BLOCK)
    proc_block = cifdoc.find_block(PROCESS_BLOCK)
    in_edge_block = cifdoc.find_block(INPUT_EDGE_BLOCK)
    out_edge_block = cifdoc.find_block(OUTPUT_EDGE_BLOCK)

    # convert the node and process blocks
    blocktypes = {
        node_block: (NODE_PREFIX, NODE_SUFFIXES, NODE_SUFFIXES_OLD),
        proc_block: (PROCESS_PREFIX, PROCESS_SUFFIXES, PROCESS_SUFFIXES_OLD),
        in_edge_block: (
            INPUT_EDGE_PREFIX,
            INPUT_EDGE_SUFFIXES,
            INPUT_EDGE_SUFFIXES,
        ),
        out_edge_block: (
            OUTPUT_EDGE_PREFIX,
            OUTPUT_EDGE_SUFFIXES,
            OUTPUT_EDGE_SUFFIXES,
        ),
    }
    errors: List[Tuple[str, str]] = []
    for block in blocktypes:
        if block is not None:
            prefix = blocktypes[block][0]
            suffixes = blocktypes[block][1]
            suffixes_old = blocktypes[block][2]
            errors = convert_relion3_1_block(
                prefix, suffixes, suffixes_old, block, errors
            )

    if len(errors) != 0:
        logger.warning(
            "Errors encountered with the "
            "following nodes/processes:\n{}\nThey have been "
            "marked as UNKNOWN or .AMBIGUOUS".format(
                "\n".join([x[0] + " : " + x[1] for x in errors])
            )
        )

    # backup the old pipeline file and write a new one
    logger.info(
        f"Pipeline was converted.  The original file has been backed up as {fn_in}.old"
    )
    shutil.move(fn_in, fn_in + ".old")
    write(cifdoc, fn_in)


def relion4_processdata_node_conversions(line_in: str) -> str:
    """Convert ambiguous ProcessData node names to pipeliner format"""
    line_split = line_in.split()
    nodetype = line_split[1].split(".")
    kwds = []
    if len(nodetype) > 2:
        kwds = nodetype[2:]
    new_node_type = line_split[1]
    if "optimiser" in kwds:
        kwd_list = [x for x in kwds if x != "optimiser"]
        fin_kwd = f".{'.'.join(kwd_list)}" if kwd_list else ""
        new_node_type = f"{NODE_OPTIMISERDATA}.{nodetype[1]}{fin_kwd}"
    elif "bodyfile" in kwds:
        new_node_type = f"{NODE_RIGIDBODIES}.{nodetype[1]}.relion.body_definitions"
    return f"{line_split[0]} {new_node_type}"


def convert_relion_4_pipeline(fn_in):
    """Convert a relion 4.0 pipeline to the pipeliner format

    Just a few nodes names that are different.  Saves old pipeline as <filename>.old

    Args:
        fn_in (str): The name of the file to convert
    """
    with open(fn_in, "r") as original:
        pipe_data = original.readlines()
    converted, is_conv = [], False
    for line in pipe_data:
        for sub in relion4_to_pipeliner_node_conversions:
            if sub in line:
                line = line.replace(sub, relion4_to_pipeliner_node_conversions[sub])
                is_conv = True
        # special check for
        if NODE_PROCESSDATA in line:
            line = relion4_processdata_node_conversions(line)
        converted.append(line)

    if is_conv:
        logger.info(f"{fn_in} updated from RELION 4.0 format to ccpem-pipeliner format")
        shutil.copy(fn_in, f"{fn_in}.old")
        with open(fn_in, "w") as outfile:
            for line in converted:
                outfile.write(line)


def bool_jobop(jobop: str) -> bool:
    """Checks if a value should be interpreted as True

    Returns:
        bool: `True` if the value is true, otherwise `False`
    """
    if str(jobop).lower() in TRUES:
        return True
    return False


def convert_coordinates_job(fn_in: str) -> Union[str, None]:
    """Convert RELION 3.1 style coordinates files to RELION 4 style

    RELION 3.x has an empty file called coords_suffix_xxx.star RELION 4.0/pipeliner
    has a starfile with two columns for micrograph name and coords file.  If errors are
    encountered with the conversion it sets the node type  for that file as
    UNSUPPORTED_deprecated_format

    Args:
        fn_in (str): Path to the RELION 3.x style file

    Returns:
        str: The new file name

    """
    try:
        job_dir, the_file = os.path.split(fn_in)
        suffix = the_file.split(".")[0].replace("coords_suffix_", "")
        jobops = jobstar_options_as_dict(os.path.join(job_dir, "job.star"))
        input_file = jobops.get("fn_in")
        if input_file is None:
            input_file = jobops.get("fn_input_autopick")
        if input_file is None:
            # this means it is an extract file
            input_file = jobops.get("star_mics")
            if jobops["do_reextract"] == "Yes":
                suffix = "reextract"
            else:
                suffix = "helical_segments"
        if input_file is None:
            raise ValueError("Input file not found")
        new_coord_file = cif.Document()
        coords_block = new_coord_file.add_new_block("coordinate_files")
        coords_loop = coords_block.init_loop(
            "_", ["rlnMicrographName", "rlnMicrographCoordinates"]
        )
        micsdata = cif.read(input_file)
        mics_block = micsdata.find_block("micrographs")
        mics = mics_block.find_loop("_rlnMicrographName")
        new_file_name = "{}/{}.star".format(job_dir, suffix)

        for mic in mics:
            mic_path = decompose_pipeline_filename(mic[0])[2].split(".")[0]
            coord_path = job_dir + mic_path + "_" + suffix + ".star"
            coords_loop.add_row([mic[0], coord_path])
            write(new_coord_file, new_file_name)
        return new_file_name

    except Exception:
        logger.warning(
            f"WARNING: There was an error converting the file {fn_in} from RELION 3.1"
            " to RELION 4.0 format.\nThis node has been marked"
            f" 'UNSUPPORTED_deprecated_format' and will not be usable.",
            exc_info=True,
        )
        return fn_in


def convert_oldstyle_names(fn_in: str) -> str:
    """Converts model and particle nodes from RELION 3.x to RELION 4 names

    The model files is RELION 3.x have been merged in to the _optimiser files in
    RELION 4. The particle files have moved from a system where the name and location
    of the file define the particles to the particles being explicitly defined in the
    file itself.

    Args:
        fn_in (str): The path to the file to convert

    Returns:
        str: The name of that file's node in Pipeliner
    """

    nodename = fn_in
    if "_model.star" in fn_in:
        nodename = fn_in.replace("_model.star", "_optimiser.star")
    elif "coords_suffix_" in fn_in:
        new_nodename = convert_coordinates_job(fn_in)
        if new_nodename is not None:
            nodename = new_nodename
        else:
            nodename = fn_in
    return nodename


def get_joboption(dict_name: str, label: str, jobops: dict) -> str:
    """Get a job option value from muliple dictionary types

    Return a job option where the jobops dict might contain
    the dictionary entries (from jobstar file) or the labels
    (from runjob file)

    Args:
        dict_name (str): The joboption key IE: from a job.star file
        label (str): The joboption label IE: from a run.job file
        jobops (dict): The dict of joboptions with `{dict_name or label: value}`

    Returns:
        str: The job option value
    """
    jo = jobops.get(dict_name)
    if jo is None:
        jo = str(jobops.get(label))
    if jo == "None":
        return ""
    return jo


# functions for converting individual process types
# Autopick jobs
def autopick_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    log = get_joboption("do_log", "OR: use Laplacian-of-Gaussian?", jobops)
    ref3d = get_joboption("do_ref3d", "OR: provide a 3D reference?", jobops)
    topaz_pick = get_joboption("do_topaz_pick", "", jobops)
    topaz_train = get_joboption("do_topaz_train", "", jobops)
    helical_jo = get_joboption(
        "do_pick_helical_segments", "Pick 2D helical segments?", jobops
    )

    do_log = bool_jobop(log)
    do_ref3d = bool_jobop(ref3d)
    do_topaz_pick = bool_jobop(topaz_pick)
    do_topaz_train = bool_jobop(topaz_train)
    do_helical = bool_jobop(helical_jo)
    if do_log and do_ref3d:
        return (
            "Both LoG and Ref3D options were selected, can't determine job type",
            False,
        )
    elif do_log:
        return (
            (AUTOPICK_LOG_HELICAL_NAME, True)
            if do_helical
            else (AUTOPICK_LOG_NAME, True)
        )
    elif do_ref3d:
        return (
            (AUTOPICK_REF3D_HELICAL_NAME, True)
            if do_helical
            else (AUTOPICK_REF3D_NAME, True)
        )
    elif do_topaz_pick:
        return (
            (AUTOPICK_TOPAZ_HELICAL_NAME, True)
            if do_helical
            else (AUTOPICK_TOPAZ_NAME, True)
        )

    elif do_topaz_train:
        return AUTOPICK_TOPAZ_TRAIN_NAME, True

    else:
        return (
            (AUTOPICK_REF2D_HELICAL_NAME, True)
            if do_helical
            else (AUTOPICK_REF2D_NAME, True)
        )


# Class 2D
def class2d_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    helical_jo = get_joboption("do_helix", "Classify 2D helical segments?", jobops)
    grad = get_joboption("do_grad", "Use VDAM algorithm?", jobops)
    is_grad = grad.lower() in TRUES
    # if it is already in the pipeliner naming convention
    if not helical_jo:
        if not is_grad:
            return CLASS2D_PARTICLE_NAME_EM, True
        else:
            return CLASS2D_PARTICLE_NAME_VDAM, True
    do_helical = bool_jobop(helical_jo)
    if do_helical:
        if not is_grad:
            return CLASS2D_HELICAL_NAME_EM, True
        else:
            return CLASS2D_HELICAL_NAME_VDAM, True
    else:
        if not is_grad:
            return CLASS2D_PARTICLE_NAME_EM, True
        else:
            return CLASS2D_PARTICLE_NAME_VDAM, True


# Class 3D
def class3d_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    helical_jo = get_joboption("do_helix", "Do helical reconstruction?", jobops)
    # if it is already in the pipeliner naming convention
    if not helical_jo:
        return CLASS3D_PARTICLE_NAME, True
    do_helical = bool_jobop(helical_jo)
    if do_helical:
        return CLASS3D_HELICAL_NAME, True
    else:
        return CLASS3D_PARTICLE_NAME, True


# CtfFind
def ctffind_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    gctf_jo = get_joboption("use_gctf", "Use Gctf instead?", jobops)

    if not gctf_jo:
        return CTFFIND_CTFFIND4_NAME, True

    gctf = bool_jobop(gctf_jo)
    if gctf:
        return CTFFIND_GCTF_NAME, True
    else:
        return CTFFIND_CTFFIND4_NAME, True


# CtfRefine
def ctfrefine_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    return CTFREFINE_REFINE_NAME, True


# Extract
def extract_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    helical_jo = get_joboption("do_extract_helix", "Extract helical segments?", jobops)

    # if it is already in the pipeliner naming convention
    if not helical_jo:
        return EXTRACT_PARTICLE_NAME, True
    do_helical = bool_jobop(helical_jo)
    if do_helical:
        return EXTRACT_HELICAL_NAME, True
    else:
        return EXTRACT_PARTICLE_NAME, True


# Import
def import_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    raw_jo = get_joboption("do_raw", "Import raw movies/micrographs?", jobops)
    other_jo = get_joboption("do_other", "Import other node types?", jobops)
    nt = get_joboption("node_type", "Node type:", jobops)
    if raw_jo and other_jo:
        do_raw = bool_jobop(raw_jo)
        do_other = bool_jobop(other_jo)
    else:
        return IMPORT_OTHER_NAME, True

    if do_raw and do_other:
        return (
            "Both do_raw and do_other were selected, can't determine jobtype",
            False,
        )
    elif do_raw:
        return IMPORT_MOVIES_NAME, True
    elif do_other:
        if nt != "2D/3D particle coordinates (*.box, *_pick.star)":
            return IMPORT_OTHER_NAME, True
        else:
            return (
                "Relion coordinate import job.star files are not compatible with "
                "ccpem-pipeliner/doppio.\nCreate a new job.star file with:"
                "'pipeliner --default_jobstar relion.import.coordinates'",
                False,
            )
    else:
        return (
            "Neither do_raw nor do_other were selected, can't determine jobtype",
            False,
        )


# Initial Model
def inimodel_proc_conv() -> Tuple[str, bool]:
    return INIMODEL_JOB_NAME, True


# Local Res
def locres_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    resmap_jo = get_joboption("do_resmap_locres", "Use ResMap?", jobops)
    relion_jo = get_joboption("do_relion_locres", "Use Relion?", jobops)
    if not resmap_jo or not relion_jo:
        return "Could not determine type of LocalRes Job", False
    resmap = bool_jobop(resmap_jo)
    relionlocal = bool_jobop(relion_jo)
    if resmap and relionlocal:
        return (
            "Both RESMAP and RELION localres selected, can't determine job type",
            False,
        )
    elif resmap:
        return LOCALRES_RESMAP_NAME, True
    elif relionlocal:
        return LOCALRES_OWN_NAME, True
    else:
        return (
            "Neither RESMAP nor RELION localres selected, can't determine job type",
            False,
        )


# ManualPick
def manual_pick_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    helix_jo = get_joboption(
        "do_starend", "Pick start-end coordinates helices?", jobops
    )
    # if already in pipeliner naming convention
    if not helix_jo:
        return MANUALPICK_JOB_NAME, True
    helix = bool_jobop(helix_jo)
    if helix:
        return MANUALPICK_HELICAL_NAME, True
    else:
        return MANUALPICK_JOB_NAME, True


# MaskCreate
def maskcreate_proc_conv() -> Tuple[str, bool]:
    return MASKCREATE_JOB_NAME, True


def polish_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    train_jo = get_joboption("do_param_optim", "Train optimal parameters?", jobops)
    polish_jo = get_joboption("do_polish", "Perform particle polishing?", jobops)

    # if already in pipeliner naming convention
    if not train_jo:
        return BAYESPOLISH_POLISH_NAME, True
    train = bool_jobop(train_jo)
    polish = bool_jobop(polish_jo)
    if train and polish:
        return (
            "Both training and polishing selected, can't determine job type",
            False,
        )
    elif polish:
        return BAYESPOLISH_POLISH_NAME, True
    elif train:
        return BAYESPOLISH_TRAIN_NAME, True
    else:
        return (
            "Neither training nor polishing selected, can't determine job type",
            False,
        )


# MotionCorr
def motioncorr_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    own = get_joboption("do_own_motioncor", "Use RELION's own implementation?", jobops)
    if not own:
        return "Ambiguous MotionCorr Job type", False
    do_own = bool_jobop(own)
    if do_own:
        return MOTIONCORR_OWN_NAME, True
    return MOTIONCORR_MOTIONCOR2_NAME, True


# PostProcess
def postprocess_proc_conv() -> Tuple[str, bool]:
    return POSTPROCESS_JOB_NAME, True


# Refine3D
def refine3d_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    helical_jo = get_joboption("do_helix", "Do helical reconstruction?", jobops)
    if not helical_jo:
        return REFINE3D_PARTICLE_NAME, True
    is_helical = bool_jobop(helical_jo)
    if is_helical:
        return REFINE3D_HELICAL_NAME, True
    return REFINE3D_PARTICLE_NAME, True


# Select
def select_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    dups_jo = get_joboption(
        "do_remove_duplicates",
        "OR: remove duplicates?",
        jobops,
    )
    discard_jo = get_joboption(
        "do_discard",
        "OR: select on image statistics?",
        jobops,
    )
    split_jo = get_joboption(
        "do_split",
        "OR: split into subsets?",
        jobops,
    )
    on_vals_jo = get_joboption(
        "do_select_values",
        "Select based on metadata values?",
        jobops,
    )
    autoranker = get_joboption(
        "do_class_ranker",
        "Automatically select 2D classes?",
        jobops,
    )

    for opt in dups_jo, discard_jo, split_jo, on_vals_jo, autoranker:
        if not opt:
            return (
                "Ambiguous Select type job.  Options: {}, {}, {}, {}, "
                "or {}".format(
                    SELECT_DISCARD_NAME,
                    SELECT_INTERACTIVE_NAME,
                    SELECT_ONVALUE_NAME,
                    SELECT_SPLIT_NAME,
                    SELECT_AUTO2D_NAME,
                ),
                False,
            )
    dups = bool_jobop(dups_jo)
    discard = bool_jobop(discard_jo)
    split = bool_jobop(split_jo)
    on_vals = bool_jobop(on_vals_jo)
    do_autoranker = bool_jobop(autoranker)

    count = 0
    for bool_opt in [dups, discard, split, on_vals, do_autoranker]:
        if bool_opt:
            count += 1
    if count > 1:
        return "Multiple functions were selected, can't determine job type", False
    elif on_vals:
        return SELECT_ONVALUE_NAME, True
    elif discard:
        return SELECT_DISCARD_NAME, True
    elif split:
        return SELECT_SPLIT_NAME, True
    elif do_autoranker:
        return SELECT_AUTO2D_NAME, True
    else:
        return SELECT_INTERACTIVE_NAME, True


# Subtract
def subtract_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    fliplabel_jo = get_joboption(
        "do_fliplabel", "OR revert to original particles?", jobops
    )
    # if already in pipeliner format
    if not fliplabel_jo:
        return SUBTRACT_JOB_NAME, True
    fliplabel = bool_jobop(fliplabel_jo)
    if fliplabel:
        return SUBTRACT_REVERT_NAME, True
    return SUBTRACT_JOB_NAME, True


# modelangelo
def modelangelo_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    return "modelangelo", True


# JoinStar
def joinstar_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    do_parts = get_joboption("do_part", "Combine particle STAR files?", jobops)
    do_movies = get_joboption("do_mov", "Combine movie STAR files?", jobops)
    do_mics = get_joboption("do_mic", "Combine micrograph STAR files?", jobops)

    if not do_parts and not do_movies and not do_mics:
        return "No type of file to join was selected", False
    count = 0
    parts = bool_jobop(do_parts)
    if parts:
        jtype = JOINSTAR_PARTS_NAME
        count += 1
    movies = bool_jobop(do_movies)
    if movies:
        jtype = JOINSTAR_MOVIES_NAME
        count += 1
    mics = bool_jobop(do_mics)
    if mics:
        jtype = JOINSTAR_MICS_NAME
        count += 1
    if count > 1:
        return "Multiple types of files were selected to join", False
    else:
        return jtype, True


# MultiBody
def multibody_proc_conv(jobops: Dict[str, JobOption]) -> Tuple[str, bool]:
    analysis = get_joboption("do_analyse", "Run flexibility analysis?", jobops)

    if not analysis:
        return "Ambiguous MultiBody Job type", False

    do_analysis = bool_jobop(analysis)
    if do_analysis:
        logger.warning(
            "The ccpem-pipeliner has separated the MultiBody Refine"
            " job from the flexibility analysis. This job will only run "
            "the refinement\n Run or schedule an additional "
            "relion.multibody.flexanalysis job after this job has finished\n"
        )
    return MULTIBODY_REFINE_NAME, True


def dynamight_proc_conv(jobops: Optional[dict] = None):
    return "dynamight", True


def other_proc_conv(jobops: Optional[dict] = None) -> Tuple[str, bool]:
    return "Unrecognised job type", False


def convert_proctype(
    jobname: str,
    jobopdict: Optional[Mapping[str, Union[str, int, float, bool]]] = None,
) -> Tuple[str, bool]:
    """Convert a process from the RELION to pipeliner style

    The pipeliner has much more specific job types than RELION 3.x/4.0
    so conversion is necessary

    Args:
        jobname (str): The name of the job
        jobopdict (dict): Dict of joboptions with `{dict_name or label: value}`

    Returns:
        tuple: job name or error and if it was successful:
        (:class:`str`, :class:`bool`)
    """

    jobname = additional_job_conversions(jobname)
    proc_conversion_functions: Dict[str, Callable] = {
        BAYESPOLISH_DIR: polish_proc_conv,
        BAYESPOLISH_POLISH_NAME: polish_proc_conv,
        MULTIBODY_DIR: multibody_proc_conv,
        MULTIBODY_RELIONSTYLE_NAME: multibody_proc_conv,
        JOINSTAR_DIR: joinstar_proc_conv,
        JOINSTAR_RELIONSTYLE_NAME: joinstar_proc_conv,
        SUBTRACT_DIR: subtract_proc_conv,
        SUBTRACT_RELIONSTYLE_NAME: subtract_proc_conv,
        SELECT_DIR: select_proc_conv,
        SELECT_RELIONSTYLE_NAME: select_proc_conv,
        REFINE3D_DIR: refine3d_proc_conv,
        REFINE3D_PARTICLE_NAME: refine3d_proc_conv,
        POSTPROCESS_DIR: postprocess_proc_conv,
        MOTIONCORR_DIR: motioncorr_proc_conv,
        MOTIONCORR_RELIONSTYLE_NAME: motioncorr_proc_conv,
        MASKCREATE_DIR: maskcreate_proc_conv,
        MANUALPICK_DIR: manual_pick_proc_conv,
        MANUALPICK_JOB_NAME: manual_pick_proc_conv,
        LOCALRES_DIR: locres_proc_conv,
        LOCALRES_RELIONSTYLE_NAME: locres_proc_conv,
        IMPORT_DIR: import_proc_conv,
        IMPORT_RELIONSTYLE_NAME: import_proc_conv,
        EXTRACT_DIR: extract_proc_conv,
        EXTRACT_PARTICLE_NAME: extract_proc_conv,
        CTFREFINE_DIR: ctfrefine_proc_conv,
        CTFREFINE_REFINE_NAME: ctfrefine_proc_conv,
        CTFFIND_DIR: ctffind_proc_conv,
        CTFFIND_RELIONSTYLE_NAME: ctffind_proc_conv,
        INIMODEL_DIR: inimodel_proc_conv,
        CLASS3D_DIR: class3d_proc_conv,
        CLASS3D_PARTICLE_NAME: class3d_proc_conv,
        CLASS2D_DIR: class2d_proc_conv,
        CLASS2D_PARTICLE_NAME: class2d_proc_conv,
        AUTOPICK_DIR: autopick_proc_conv,
        AUTOPICK_RELIONSTYLE_NAME: autopick_proc_conv,
        "dynamight": dynamight_proc_conv,
        "modelangelo": modelangelo_proc_conv,
    }

    # A unique conversion is required for each process type
    jobtype = jobname.split("/")[0]
    need_to_read_jobstar = [
        AUTOPICK_DIR,
        CLASS2D_DIR,
        CLASS3D_DIR,
        EXTRACT_DIR,
        IMPORT_DIR,
        CTFFIND_DIR,
        LOCALRES_DIR,
        MOTIONCORR_DIR,
        BAYESPOLISH_DIR,
        REFINE3D_DIR,
        SELECT_DIR,
        MANUALPICK_DIR,
        CTFREFINE_DIR,
    ]

    # doing the actual conversions
    # some jobs need to check the jobfile params to determine type
    if jobtype in need_to_read_jobstar and jobopdict is None:
        jobfile = os.path.join(jobname, "job.star")
        jobops = jobstar_options_as_dict(jobfile)
        return proc_conversion_functions.get(jobtype, other_proc_conv)(jobops)

    elif isinstance(jobopdict, dict):
        jobops = jobopdict
        return proc_conversion_functions.get(jobtype, other_proc_conv)(jobops)

    else:
        return proc_conversion_functions.get(jobtype, other_proc_conv)()


class IntNodeTypeConvert(object):
    """Class for converting relion 3.x style integer node types into pipeliner format

    Attributes:
        n_path (str): The project relative path of the node
        jobtype (str): The general type of job taken from the first dir its path
            EG: Class2D
        jobname (str): The job's type and number EG: CLass2D/job001
        filename (str): The nae of the actual file
        filename_conv (Dict[str, str]): Holds the node types for the different types of
            files a specific job will make
        use_gen_name (bool): Will the node types generated need to be used a general
            name for more complicated nodetypes
        halfmap_str (str): string that needs to be added to the node if it is a halfmap
        single_node_suffix (str): The suffix that will be added to all files if they all
            need to be treated the same

    """

    def __init__(self, n_path: str, oldtype: str):
        self.n_path = n_path
        self.jobtype = n_path.split("/")[0]
        self.filename = os.path.basename(n_path)
        self.jobname = os.path.dirname(n_path)
        self.filename_conv: Dict[str, str] = {}
        self.use_gen_name = False
        self.halfmap_str = ""
        self.single_node_suffix = ""
        try:
            self.ext = "." + self.filename.split(".")[-1]
        except IndexError:
            self.ext = ""
        self.node_genname = NODE_INT2NODELABEL.get(int(oldtype), "")

    def add_kwds(self, options: List[str], kwd: str) -> dict:
        """Add keyword to all of nodes in the nodes dict based on joboption values

        Args:
            options (list): List of JobOption keys for the BooleanJobOptions that
                should be checked the keywords will be added if all the JobOptions`
                values are in TRUES
            kwd (str): The keyword to add to the node if option in true.
        returns:
            dict: {relion_node_type: pipeliner_node_type}
        """
        job_ops = jobstar_options_as_dict(os.path.join(self.jobname, "job.star"))

        add_kwd = all([job_ops[x].lower() in TRUES for x in options])

        if add_kwd:
            fnc_copy = self.filename_conv.copy()
            for i in self.filename_conv:
                fnc_copy[i] = self.filename_conv[i] + f".{kwd}"
            return fnc_copy
        return self.filename_conv

    # Conversion functions for relion job types
    # Autopick
    def autopick_conv(self):
        self.filename_conv = {
            "logfile.pdf": ".pdf.relion.autopick",
            "autopick.star": ".star.relion.autopick",
            "coords_suffix_autopick.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # Class 2D
    def class2d_conv(self):
        self.use_gen_name = True
        self.halfmap_str = ""
        self.filename_conv = {
            NODE_OPTIMISERDATA: ".star.relion.class2d",
            NODE_PARTICLEGROUPMETADATA: ".star.relion.class2d",
        }

        # check if it's helical and add the 'helical' kwd if necessary
        self.filename_conv = self.add_kwds(["do_helix"], "helical")

    # Class 3D
    def class3d_conv(self):
        hms = "Half" if ".mrc" in self.filename and "half" in self.filename else ""
        self.halfmap_str = hms
        self.use_gen_name = True
        self.filename_conv = {
            NODE_OPTIMISERDATA: ".star.relion.class3d",
            NODE_PARTICLEGROUPMETADATA: ".star.relion.class3d",
            NODE_DENSITYMAP: ".mrc.relion.class3d",
            NODE_DENSITYMAP + "Half": ".mrc.relion.class3d.halfmap",
        }
        # check if it's helical and add the 'helical' kwd if necessary
        self.filename_conv = self.add_kwds(["do_helix"], "helical")

    # CtfFind
    def ctffind_conv(self):
        self.filename_conv = {
            "micrographs_ctf.star": ".star.relion.ctf",
            "logfile.pdf": ".pdf.relion.ctffind",
        }

    # CtfRefine
    def ctfrefine_conv(self):
        jobops = jobstar_options_as_dict(os.path.join(self.jobname, "job.star"))

        aniso = bool_jobop(str(jobops.get("do_aniso_mag")))
        if aniso:
            self.filename_conv = {
                "particles_ctf_refine.star": ".star.relion.anisomagrefine",
                "logfile.pdf": ".pdf.relion.ctfrefine",
            }
        else:
            self.filename_conv = {
                "particles_ctf_refine.star": ".star.relion.ctfrefine",
                "logfile.pdf": ".pdf.relion.ctfrefine",
            }

    # External
    def external_conv(self):
        self.single_node_suffix = self.ext
        self.node_genname = "UnknownNodeType"

    # Extract
    def extract_conv(self):
        # need to check on the naming of the coords files first
        self.filename_conv = {
            "particles.star": ".star.relion",
            "reextract.star": ".star.relion",
            "helix_segments.star": ".star.relion.helicalsegments",
            "coords_suffix_extract.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # Import
    def import_conv(self):
        hms = "Half" if ".mrc" in self.filename and "half" in self.filename else ""
        self.halfmap_str = hms
        self.use_gen_name = True
        self.filename_conv = {
            NODE_MICROGRAPHGROUPMETADATA: ".star.relion",
            NODE_MICROGRAPHMOVIEGROUPMETADATA: ".star.relion",
            NODE_MICROGRAPHCOORDSGROUP: ".star.relion",
            NODE_DENSITYMAP + "Half": ".mrc.halfmap",
            NODE_DENSITYMAP: ".mrc",
            NODE_MASK3D: ".mrc",
        }

    # Initial Model
    def initial_model_conv(self):
        self.use_gen_name = True
        self.filename_conv = {
            NODE_DENSITYMAP: ".mrc.relion.initialmodel",
            NODE_PARTICLEGROUPMETADATA: ".star.relion.initialmodel",
            NODE_OPTIMISERDATA: ".star.relion.initialmodel",
        }

    # JoinStar
    def joinstar_conv(self):
        self.single_node_suffix = ".star.relion"

    # LocalRes
    def local_res_conv(self):
        self.filename_conv = {
            "histogram.pdf": ".pdf.relion.localres",
            "relion_locres_filtered.mrc": ".mrc.relion.localresfiltered",
            "relion_locres.mrc": ".mrc.relion.localres",
            "half1_resmap.mrc": ".mrc.resmap.localres",
        }

    # ManualPick
    def manual_pick_conv(self):
        self.filename_conv = {
            "manualpick.star": ".star.relion.manualpick",
            "micrographs_selected.star": ".star.relion",
            "coords_suffix_manualpick.star": ".star.UNSUPPORTED_deprecated_format",
        }

    # MaskCreate
    def mask_create_conv(self):
        self.single_node_suffix = ".mrc.relion"

    # MotionCorr
    def motioncorr_conv(self):
        self.filename_conv = {
            "corrected_micrographs.star": ".star.relion.motioncorr",
            "logfile.pdf": ".pdf.relion.motioncorr",
        }

    # MultiBody
    def multibody_conv(self):
        hms = "Half" if ".mrc" in self.filename and "half" in self.filename else ""
        self.halfmap_str = hms
        self.use_gen_name = True
        self.filename_conv = {
            NODE_DENSITYMAP + "Half": ".mrc.relion.halfmap.multibody",
            # this isn't used yet but might be added in
            NODE_DENSITYMAP: ".mrc.relion.multibody",
            NODE_PARTICLEGROUPMETADATA: ".star.relion.multibody.eigenselection",
            NODE_LOGFILE: ".pdf.relion.multibody.flexanalyse",
            NODE_OPTIMISERDATA: ".star.relion.multibody",
        }

    # Polish
    def polish_conv(self):
        self.filename_conv = {
            "shiny.star": ".star.relion.polished",
            "opt_params_all_groups.txt": ".txt.relion.polishparams",
            "logfile.pdf": ".pdf.relion.polish",
        }

    # PostProcess
    def postprocess_conv(self):
        self.filename_conv = {
            "postprocess.mrc": ".mrc.relion.postprocessed",
            "postprocess_masked.mrc": ".mrc.relion.postprocessed.masked",
            "logfile.pdf": ".pdf.relion.postprocess",
            "postprocess.star": ".star.relion.postprocess",
        }

    # Refine3D
    def refine3d_conv(self):
        self.filename_conv = {
            "run_class001.mrc": ".mrc.relion.refine3d",
            "run_data.star": ".star.relion.refine3d",
            "run_half1_class001_unfil.mrc": ".mrc.relion.refine3d.halfmap",
        }
        # check if it's helical and add the 'helical' kwd if necessary
        self.filename_conv = self.add_kwds(["do_helix"], "helical")

    # Select
    def select_conv(self):
        if "class_averages" in self.filename:
            self.single_node_suffix = ".star.relion.classaverages"
        else:
            self.single_node_suffix = ".star.relion"

    # subtract
    def subtract_conv(self):
        self.filename_conv = {
            "particles_subtracted.star": ".star.relion.subtracted",
            "original.star": ".star.relion.subtractreverted",
        }

    def set_up_conversion(self):
        conversions = {
            AUTOPICK_DIR: self.autopick_conv,
            CLASS2D_DIR: self.class2d_conv,
            CLASS3D_DIR: self.class3d_conv,
            CTFFIND_DIR: self.ctffind_conv,
            CTFREFINE_DIR: self.ctfrefine_conv,
            EXTRACT_DIR: self.extract_conv,
            IMPORT_DIR: self.import_conv,
            INIMODEL_DIR: self.initial_model_conv,
            LOCALRES_DIR: self.local_res_conv,
            MANUALPICK_DIR: self.manual_pick_conv,
            MASKCREATE_DIR: self.mask_create_conv,
            BAYESPOLISH_DIR: self.polish_conv,
            POSTPROCESS_DIR: self.postprocess_conv,
            REFINE3D_DIR: self.refine3d_conv,
            SELECT_DIR: self.select_conv,
            MOTIONCORR_DIR: self.motioncorr_conv,
            EXTERNAL_DIR: self.external_conv,
            JOINSTAR_DIR: self.joinstar_conv,
            MULTIBODY_DIR: self.multibody_conv,
            SUBTRACT_DIR: self.subtract_conv,
        }

        # setup the dicts for node conversion
        conv_function = conversions.get(self.jobtype)
        if conv_function:
            conv_function()
        else:
            pass


def convert_31_pipeline_node(name: str, oldtype: str) -> Tuple[str, str]:
    """Convert node names from a RELION 3.1 to the Pipeliner formats

    Every job type has a specific method for converting its own node types

    Args:
        name (str): The node name
        oldtype (str): The original node type

    Returns:
        Tuple: (node name, node type)
    """

    the_node = IntNodeTypeConvert(name, oldtype)
    the_node.set_up_conversion()

    # for nodes that use a relion 3.1 node type, but have no associated job
    if the_node.use_gen_name:
        try:
            new_type = the_node.filename_conv[
                the_node.node_genname + the_node.halfmap_str
            ]
            return the_node.n_path, the_node.node_genname + new_type
        except KeyError:
            if the_node.single_node_suffix:
                return (
                    the_node.n_path,
                    the_node.node_genname + the_node.single_node_suffix,
                )
            else:
                return the_node.n_path, "UnknownNodeType" + the_node.ext
    else:
        try:
            return (
                the_node.n_path,
                the_node.node_genname + the_node.filename_conv[the_node.filename],
            )
        except KeyError:
            if the_node.single_node_suffix:
                return (
                    the_node.n_path,
                    the_node.node_genname + the_node.single_node_suffix,
                )
            else:
                return the_node.n_path, "UnknownNodeType" + the_node.ext


def star_loop_as_list(
    starfile: str, block: str, columns: Optional[List[str]] = None
) -> Tuple[List[str], List[List[str]]]:
    """Returns a set of columns from a starfile loop as a list

    Args:
        starfile (str): The file to read from
        block (str): The name of the block to get the data from
        columns (list): Names of the columns to get, if None then all
            columns are returned

    Returns:
        tuple: (:class:`list`, :class:`list`) [0] The names of the columns in order,
        [c1, c2, c3]. [1] The rows of the column(s) as a list of lists
        [[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3]]

    Raises:
        ValueError: If the specified block is not found
        ValueError: If the specified block does not contain a loop
        ValueError: If any of the specified columns are not found
    """
    sf = cif.read(starfile)
    fblock = sf.find_block(block)
    if fblock is None:
        raise ValueError(f"Block '{block}' not found")
    loop = fblock[0].loop
    if loop is None:
        raise ValueError(
            f"Block '{block}' does not contain a loop; Use star_pairs_as_dict()"
            " instead"
        )
    cols = loop.tags[0:] if columns is None else columns
    for col in cols:
        if col not in loop.tags[0:]:
            raise ValueError(f"Column '{col}' not found in block '{block}'")
    data = []
    for row in fblock.find(cols):
        data.append([x for x in row])
    return cols, data


def star_pairs_as_dict(starfile: str, block: str) -> dict:
    """Returns paired values from a starfile as a dict

    Args:
        starfile (str): The file to read from
        block (str): The name of the block to get the data from

    Returns:
        dict: `{parameter: value}`

    Raises:
        ValueError: If the specified block is not found
        ValueError: If the specified block is a loop and not a pair-value
    """
    sf = cif.read(starfile)
    fblock = sf.find_block(block)
    if fblock is None:
        raise ValueError(f"Block '{block}' not found")
    out_dict = {}
    if fblock[0].pair is None:
        raise ValueError(
            f"Block '{block}' does not contain item-value pairs. "
            "Try using star_loop_as_list() instead"
        )
    for item in fblock:
        out_dict[item.pair[0]] = item.pair[1]
    return out_dict


def compare_starfiles(starfile1: str, starfile2: str) -> Tuple[bool, bool]:
    """See if two starfiles contain the same information

    Direct comparison can be difficult because the starfile columns or blocks
    can be in different orders

    Args:
        starfile1 (str): Name of the first file
        starfile2 (str): Name of the second file

    Returns:
        tuple: (:class:`bool`, :class:`bool`) [0] True if the starfile structures
        are identical (Names of blocks and columns) [1] True if the data in
        the two files are identical
    """
    sf1, sf2 = cif.read(starfile1), cif.read(starfile2)
    # check the blocks
    sf1_blocks = set([x.name for x in sf1])
    sf2_blocks = set([x.name for x in sf2])
    if sf1_blocks != sf2_blocks:
        return False, False

    # check loops/pairs within blocks
    for block in sf1_blocks:
        try:
            l1 = set(star_loop_as_list(starfile1, block)[0])
            l2 = set(star_loop_as_list(starfile2, block)[0])

        except ValueError:
            l1 = set(star_pairs_as_dict(starfile1, block))
            l2 = set(star_pairs_as_dict(starfile2, block))
        if l1 != l2:
            return False, False

    # check the data
    for block in sf1_blocks:
        try:
            l1_lists = star_loop_as_list(starfile1, block)[1]
            l2_lists = star_loop_as_list(starfile2, block)[1]
            l1_sets = [set(x) for x in l1_lists]
            l2_sets = [set(x) for x in l2_lists]
            for i in l1_sets:
                if i not in l2_sets:
                    return True, False
            for i in l2_sets:
                if i not in l1_sets:
                    return True, False

        except ValueError:
            d1 = star_pairs_as_dict(starfile1, block)
            d2 = star_pairs_as_dict(starfile2, block)
            l1 = set([d1[x] for x in d1])
            l2 = set([d2[x] for x in d2])
            if l1 != l2:
                return True, False

    return True, True


def convert_relion20_datafile(
    starfile: str,
    dtype: str,
    outname: Optional[str] = None,
    boxsize: Optional[int] = None,
    og_name: str = "convertedOpticsGroup1",
    og_num: int = 1,
):
    """Convert a relion 2.x format starfile to relion 3.x/4.x format

    Adds on an optics groups block and renames the main data block with
    the relion 3/x/4.x format. Fixes te descrepency between how Relion 2.x
    and Relion 3.x/4.x define pixel size. Adds missing fields to the data
    block.

    Args:
        starfile (str): The name of the file to process
        outname (str): The name of the output file, 'converted_<starfile>.star'
            by default.
        dtype(str): The type of file, must be in (movies, micrographs,
            ctf_micrographs, particles)
        boxsize (int): The box size of particles in pix.  This is required for
            particles
        og_name (str): The name for the optics group that will be created for the data
            in the file
        og_num (int): Optics group number for the optics group that
            will be created for the data in the file
    """

    data = cif.read_file(starfile).sole_block()

    # get the parameters for the optics group of the correct data file type
    # default values - if None then it is required to be in the original file
    parts_cols = OrderedDict(
        [
            ("_rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", "Empty"),
            ("_rlnVoltage", "Empty"),
            ("_rlnSphericalAberration", "Empty"),
            ("_rlnAmplitudeContrast", "Empty"),
            ("_rlnImagePixelSize", "Empty"),
            ("_rlnImageSize", str(boxsize)),
            ("_rlnImageDimensionality", "2"),
        ]
    )
    mics_cols = OrderedDict(
        [
            ("rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", "Empty"),
            ("_rlnVoltage", "Empty"),
            ("_rlnSphericalAberration", "Empty"),
            ("_rlnAmplitudeContrast", "Empty"),
            ("_rlnMicrographPixelSize", "Empty"),
        ]
    )
    movs_cols = OrderedDict(
        [
            ("_rlnOpticsGroupName", og_name),
            ("_rlnOpticsGroup", str(og_num)),
            ("_rlnMtfFileName", '""'),
            ("_rlnMicrographOriginalPixelSize", "Empty"),
            ("_rlnVoltage", "Empty"),
            ("_rlnSphericalAberration", "Empty"),
            ("_rlnAmplitudeContrast", "Empty"),
        ]
    )

    if dtype == "particles":
        og_cols = parts_cols
    elif dtype == "micrographs":
        og_cols = mics_cols
    elif dtype == "movies":
        og_cols = movs_cols
    else:
        raise ValueError(
            "Invalid Relion data file type specified. Choose from particles,"
            " micrographs, or movies"
        )

    # columns present in Relion 2.x files but not 3.x
    r2x_og_cols = ["_rlnDetectorPixelSize", "_rlnMagnification"]

    # find all the OG columns in the input file
    # replace the values in og_cols if they were present in the input file
    all_og_cols = list(og_cols) + r2x_og_cols
    found_cols = [data.find_loop(col) for col in all_og_cols]
    for col in found_cols:
        if str(col) != "<gemmi.cif.Column nil>":
            col_name = str(col).split()[1]
            dat = [x for x in col]
            num_ogs = len(set(dat))
            if num_ogs > 1:
                raise ValueError(
                    "File has multiple optics groups - currently not setup to convert"
                    " these files"
                )
            og_cols[col_name] = dat[0]

    # calculate the image pixel size from the detector pixel size and magnification
    # Relion 2.x files shouldn't have a _rlnImagePixelSize column
    dps = og_cols["_rlnDetectorPixelSize"]
    mag = og_cols["_rlnMagnification"]
    apix_i = og_cols.get("rlnImagePixelSize")
    apix_m = og_cols.get("rlnMicrographPixelSize")

    if "Empty" in [dps, mag] and (apix_m == "Empty" and apix_i == "Empty"):
        raise ValueError("Pixel size information missing from starfile")
    apix = str(float(dps) / float(mag) * 10000)
    if dtype == "particles" and apix_i is None:
        og_cols["_rlnImagePixelSize"] = apix
    elif dtype == "micrographs" and apix_m is None:
        og_cols["_rlnMicrographPixelSize"] = apix

    og_cols["_rlnMicrographOriginalPixelSize"] = apix

    logger.warning(
        "Using this tool on binned data may cause issues\n"
        "Relion 2.x does not save binning information in its data star files "
        "so this tool will produced converted files where the binned pixel size"
        "is treated as the original pixel size."
    )

    # handle boxsize for particles files
    if dtype == "particles":
        if og_cols["_rlnImageSize"] is None:
            raise ValueError(
                "Box size not specified, you must specify box size for files containing"
                " particles"
            )

    # make "rlnOpticsGroupName" block and populate it
    outdoc = cif.Document()
    og_block = outdoc.add_new_block("optics")
    og_loop = og_block.init_loop("", list(og_cols))
    og_row = []
    for og_col in og_cols:
        val = og_cols[og_col]
        if val is None:
            raise ValueError(f"Required value {og_col} was not founnd")

        og_row.append(val)
    og_loop.add_row(og_row)

    # get the columns necessary for the data block
    data_loop = data[0].loop
    all_dcols = data_loop.tags[0:]
    data_cols = list(set(all_dcols) - set(all_og_cols))

    # make main data block and populate
    out_data_block = outdoc.add_new_block(dtype)
    out_data_loop = out_data_block.init_loop("", data_cols + ["_rlnOpticsGroup"])

    # populate the data rows
    data_table = data.find(data_cols)
    for row in data_table:
        row_data = [x for x in row]
        row_data.append(og_cols["_rlnOpticsGroup"])
        out_data_loop.add_row(row_data)

    if outname is None:
        outname = f"{os.path.basename(starfile).split('.')[0]}_converted.star"
    elif not outname.endswith(".star"):
        outname += ".star"
    write(outdoc, outname)


def get_pipeline_version(data: cif.Document) -> str:
    """See if a pipeline is the RELION 3.x, RELION 4.0 or pipeliner style

    Returns:
        str: The pipeline version "relion3", "relion4" or "ccpem-pipeliner"

    Raises:
        ValueError: If the file is empty, does not contain recognisable pipeline data or
            the pipeline type cannot be determined.
    """
    general_block = data.find_block(GENERAL_BLOCK)

    if general_block is None:
        raise ValueError(
            f"Block {GENERAL_BLOCK} not found. File is empty, corrupt or not a pipeline"
            " STAR file."
        )

    proc_block = data.find_block(PROCESS_BLOCK)

    # If no process block, assume we have a new empty pipeline
    if proc_block is None:
        return "ccpem-pipeliner"

    try:
        proc_loop = proc_block[0].loop
    except IndexError:
        raise ValueError(f"No data found in {PROCESS_BLOCK} block")

    if proc_loop is not None:
        last_column_header = proc_loop.tags[-1]
    else:
        raise ValueError(f"No data loop found in {PROCESS_BLOCK} block")

    old_suffix = PROCESS_SUFFIXES_OLD[-1]
    new_suffix = PROCESS_SUFFIXES[-1]
    if last_column_header.endswith(old_suffix):
        return "relion3"
    elif last_column_header.endswith(new_suffix):
        # look for the relion 4.0 style node names
        # there are more that will need to be identified
        node_block = data.find_block(NODE_BLOCK)
        if node_block is not None:
            node_string = node_block.as_string()
            r4_nodes = ["MicrographsData", "Images2DData", "MicrographMoviesData"]
            for r4n in r4_nodes:
                if r4n in node_string:
                    return "relion4"
        return "ccpem-pipeliner"
    else:
        raise ValueError(
            f"Pipeline file has formatting errors: {PROCESS_BLOCK} block should contain"
            f" {new_suffix} or {old_suffix} in its labels"
        )
