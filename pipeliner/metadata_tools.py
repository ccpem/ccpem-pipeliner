#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
from glob import glob
from typing import Any, Dict, Tuple, Union, Optional
import traceback
from gemmi import cif
import logging

from pipeliner.data_structure import JOBSTATUS_SUCCESS
from pipeliner.process import Process
from pipeliner.job_factory import new_job_of_type, active_job_from_proc
from pipeliner.summary_data_tools import add_to_summary_file, remove_from_summary_file
from pipeliner.job_options import (
    JobOption,
    IntJobOption,
    FloatJobOption,
    BooleanJobOption,
)

METADATA_REPORT_DIR = "MetadataReports"
logger = logging.getLogger(__name__)


def get_metadata_chain(
    pipeline, this_job: Process, full: bool = False
) -> Tuple[dict, int]:
    """Get the metadata for a job and all its upstream jobs

    Metadata is gathered by each individual job's gather_metadata
    function

    Args:
        pipeline (:class:`~pipeliner.project_graph.ProjectGraph`)
        this_job(:class:`~pipeliner.process.Process`):  The `Process`
            object for the terminal job
        full (bool): Should the metadata report also contain information about
            continuations and multiple runs of the jobs or just the current one
    Returns:
        tuple: (the metadata dict, number of jobs in it)
    """
    metadata_dict: Dict[str, Any] = {}
    tracked_job: Dict[str, Any] = {}
    jobs_metadata = {}
    job_network = []
    tracked_job["Terminal job"] = this_job.name
    upstream = pipeline.get_upstream_network(this_job)
    upstream_procs = {os.path.dirname(x[0].name) + "/" for x in upstream}
    procs = [x.name for x in pipeline.process_list]
    tracked_job["Number of parent jobs"] = len(
        [x for x in upstream_procs if x in procs]
    )

    jobs_metadata[this_job.name] = get_job_metadata(this_job)
    for job in upstream_procs:
        jobproc = pipeline.find_process(job)
        if jobproc is None:
            continue
        jobs_metadata[job] = get_job_metadata(jobproc)
        if full:
            prev_runs = glob(f"{jobproc.name}*job_metadata.json")
            prev_runs.sort()
            if len(prev_runs) > 1:
                for pr in prev_runs[:-1]:
                    pjob_name = os.path.basename(pr).split("_")[0]
                    pjob = f"{jobproc.name} previous run {pjob_name}"
                    with open(pr, "r") as prdata:
                        jobs_metadata[pjob] = json.loads(prdata.read())

    for edge in upstream:
        parent = None if edge[1] is None else edge[1].name
        child = None if edge[2] is None else edge[2].name
        job_network.append([parent, child, edge[0].name])

    metadata_dict["CCPEM pipeliner job metadata trace"] = tracked_job
    metadata_dict["Jobs metadata"] = jobs_metadata
    metadata_dict["Jobs network edges"] = job_network
    return metadata_dict, len(upstream_procs) + 1


def format_for_metadata(joboption: JobOption) -> Optional[Union[str, float, int, bool]]:
    """Format data from relion starfiles for JSON.

    Changes 'Yes'/'No" to True/False, None for blank vals and removes any
    quotation marks

    Args:
        joboption (JobOption): The joboption
            to format

    Returns:
        str: A properly formatted :class:`str`, :class:`float` or
            :class:`int` or :class:`bool`

    Raises:
        ValueError: If a boolean job option doesn't have a value compatible
            with a class:`bool`"
    """
    if isinstance(joboption, FloatJobOption):
        return joboption.get_number()
    elif isinstance(joboption, IntJobOption):
        return joboption.get_number()
    elif isinstance(joboption, BooleanJobOption):
        return joboption.get_boolean()
    else:
        val = cif.as_string(joboption.get_string())
        if val == "":
            return None
        return val


def get_job_metadata(this_job: Process) -> dict:
    """Run a job's metadata gathering method

    Args:
        this_job (:class:`pipeliner.process.Process`): The `Process` object
            for the job to gather metadata from

    Returns:
        dict: Metadata dict for the job
        Returns ``None`` if the job has no metadata gathering function
    """
    if this_job.status != JOBSTATUS_SUCCESS:
        return {
            "No metadata": f"job {this_job.name} has not finished"
            "successfully, you can only gather metadata from successfully "
            "finished jobs"
        }
    try:
        the_job = active_job_from_proc(this_job)

        # job options are cleaned in case a RELION style job star is used
        # instead of a pipeliner style one
        proglist = []
        for prog in the_job.jobinfo.programs:
            proglist.append({"Name": prog.command, "Version": prog.get_version()})

        md_jobops = {
            "JobType": the_job.PROCESS_NAME,
            "Continued": the_job.is_continue,
            "ProgramsUsed": proglist,
        }

        for opt in the_job.joboptions:
            md_jobops[opt] = format_for_metadata(the_job.joboptions[opt])

        the_job.output_dir = this_job.name
        metadata = {"run_parameters": md_jobops, "results": the_job.gather_metadata()}
        # add in the logfile and outputnode info to the results metadata

        metadata["results"]["OutputFiles"] = [x.name for x in this_job.output_nodes]

        logfiles = [
            os.path.join(this_job.name, "run.out"),
            os.path.join(this_job.name, "run.err"),
        ]
        if metadata["results"].get("LogFiles") is None:
            metadata["results"]["LogFiles"] = logfiles
        else:
            metadata["results"]["LogFiles"] += logfiles

        # TODO: do the validation tasks here:
        # - check if the schema exist
        # - read the schema
        # - validate the created dict against the schema

        return metadata
    except Exception as e:
        tb = str(traceback.format_exc())
        logger.exception("METADATA COLLECTION ERROR")
        return {
            "metadata collection error": str(e),
            "traceback": tb,
        }


def make_job_parameters_schema(job_type: str) -> dict:
    """
    Write the json schema for the running parameters of a job

    The metadata schema have two parts: the running parameters part is generated
    automatically and the results part is written by the user

    Args:
        job_type (str): The job type to write the schema for

    Returns:
        dict: The json schema ready to be dumped to json file
    """
    the_job = new_job_of_type(job_type)
    scheme_dict: dict = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": f"PARAMS: {job_type}",
        "description": f"{the_job.jobinfo.short_desc}",
        "type": "object",
        "properties": {},
    }

    for jo in the_job.joboptions:
        jo_types = {
            "STRING": "string",
            "MULTIPLECHOICE": "string",
            "FILENAME": "string",
            "INPUTNODE": "string",
            "MULTIINPUTNODE": "string",
            "FLOAT": "number",
            "INT": "integer",
            "BOOLEAN": "boolean",
            "MULTISTRING": "string",
            "DIRPATH": "string",
            "SEARCHSTRING": "string",
            "MULTIFILENAME": "string",
        }
        scheme_dict["properties"][jo] = {
            "description": the_job.joboptions[jo].label,
            "type": jo_types[the_job.joboptions[jo].joboption_type],
        }
    return scheme_dict


def make_job_results_schema(job_type: str) -> dict:
    the_job = new_job_of_type(job_type)
    empty_scheme = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": f"RESULTS: {job_type}",
        "description": the_job.jobinfo.short_desc,
        "type": "object",
        "properties": None,
    }
    return empty_scheme


def add_md_to_summary_data(filename: str, terminal_job: str, njobs: int):
    """Add a metadata report to the summary file
    Args:
        filename (str): The name of the report file
        terminal_job (str): The job the report was launched from
        njobs (int): The number of jobs contained in the report
    """
    add_to_summary_file("metadata_reports", [filename, terminal_job, str(njobs)])


def remove_md_from_summary_data(filename: str):
    """Remove a report from the summary file

    Args:
        filename (str): Name of the report file to remove
    """
    remove_from_summary_file("metadata_reports", filename)
