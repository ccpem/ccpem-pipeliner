#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import os
import shutil
from typing import Dict, Set, Tuple, Optional, List
from pathlib import Path
from collections import OrderedDict
import logging

from pipeliner.utils import (
    date_time_tag,
    decompose_pipeline_filename,
    run_subprocess,
    update_jobinfo_file,
)
from pipeliner.summary_data_tools import add_to_summary_file, remove_from_summary_file
from pipeliner.data_structure import JOBSTATUS_RUN, JOBSTATUS_SCHED, PROJECT_FILE
from pipeliner.process import Process
from pipeliner.project_graph import ProjectGraph
from pipeliner.starfile_handler import JobStar
from pipeliner.job_factory import active_job_from_proc
from pipeliner.job_options import ExternalFileJobOption, MultiExternalFileJobOption

ARCHIVE_DIR = "ProjectArchives"
logger = logging.getLogger(__name__)


def check_status_for_archiving(job_proc: Process, do_full: bool) -> None:
    if job_proc.status in [JOBSTATUS_RUN, JOBSTATUS_SCHED]:
        raise ValueError(
            f"ERROR: Job {job_proc.name} is still marked as running or "
            "scheduled! Archives cannot be created unless all jobs "
            "have terminated\nNo archive created."
        )
    if not do_full and job_proc.type == "external.processingimport":
        raise ValueError(
            f"ERROR: The job {job_proc.name} contains external processing steps "
            "that will not be preserved by a simple archive.\nA full archive"
            " is more appropriate for this project.\nNo archive created."
        )


def write_rerunning_script(job_procs: List[Process], archive_name) -> None:
    # make the archive and script
    jobs_list = list(job_procs)
    jobs_list.sort(key=lambda x: decompose_pipeline_filename(x.name)[1])
    with open(os.path.join(archive_name, "run_project.py"), "w") as script:
        script.write("#!/usr/bin/env python3\n")
        script.write("from pipeliner.api.manage_project import PipelinerProject\n")
        script.write("from pipeliner.job_manager import wait_for_job_to_finish\n\n")
        script.write("proj = PipelinerProject(make_new_project=True)\n")
        runlines = []

        # add scheduling jobs
        for n, job_proc in enumerate(jobs_list):
            job_var = f"{job_proc.outdir.lower()}_{n+1:03d}"
            archive_jobfile = os.path.join(f"{job_var}_job.star")
            jobstar = os.path.join("JobFiles", archive_jobfile)

            if job_proc.alias:
                script.write(
                    f'{job_var} = proj.schedule_job("{jobstar}", '
                    f'alias="{Path(job_proc.alias).name}")\n'
                )
            else:
                script.write(f'{job_var} = proj.schedule_job("{jobstar}")\n')

            runlines.extend(
                [
                    f"proj.run_scheduled_job({job_var})",
                    f"wait_for_job_to_finish({job_var}, error_on_fail=True, "
                    "error_on_abort=True)\n",
                ]
            )
        # add runnning all the jobs
        script.write("\n")
        script.write("\n".join(runlines))


def find_archive_external_files(job_procs: List[Process]) -> Dict[str, List[str]]:
    """Return info about files used in the project that were not put in the archive"""
    files_not_included: Dict[str, List[str]] = {}
    for job_proc in job_procs:
        job = active_job_from_proc(job_proc)
        mf = []
        for jobop in job.joboptions:
            j_op = job.joboptions[jobop]
            if isinstance(j_op, ExternalFileJobOption):
                if j_op.get_string():
                    mf.append(j_op.get_string())
            elif isinstance(j_op, MultiExternalFileJobOption):
                if j_op.get_list():
                    for f in j_op.get_list():
                        mf.append(f)
        if mf:
            files_not_included[job_proc.name] = list(set(mf))

    ad_files = OrderedDict(sorted(files_not_included.items()))
    return ad_files


def get_archive_procs(
    process: Process,
) -> Tuple[Set[Process], Set[Optional[str]], Set[Optional[str]]]:
    """Get all the processes for an archive

    Args:
        process (Process): The terminal process

    Returns:
        Tuple[List[Process], List[str], List[str]: All the processes for the archive
            the input file names, and the output file names
    """
    # make sets of jobs, their jobstar files, and inout_nodes
    with ProjectGraph(read_only=False) as pipeline:
        edges_list = pipeline.get_upstream_network(process)
        job_procs, inputs, outputs = set(), set(), set()
        for edge in edges_list:
            if edge[1] is not None:
                job_procs.add(edge[1])
                for innode in edge[1].input_nodes:
                    inputs.add(innode.name)
                for outnode in edge[1].output_nodes:
                    outputs.add(outnode.name)

    # add the terminal job's name and nodes
    job_procs.add(process)
    for inp in process.input_nodes:
        inputs.add(inp.name)
    for outp in process.output_nodes:
        outputs.add(outp.name)

    return job_procs, inputs, outputs


def make_tarball(movdir: str, archive_name: str) -> None:
    movdir += ".tar.gz"
    tar_proc = run_subprocess(["tar", "-zcvf", movdir, archive_name], text=True)
    if tar_proc.returncode != 0:
        arch_fail = f"{archive_name}_FAILURE.log"
        with open(arch_fail, "w") as fail_log:
            fail_log.write(f'Command was "tar -zcvf {movdir} {archive_name}"')
            fail_log.write("\nProcess stdout:\n")
            fail_log.write(tar_proc.stdout)
            fail_log.write("\nProcess stderr:\n")
            fail_log.write(tar_proc.stderr)
        raise RuntimeError(
            "There was an error creating the zipped tarball for this archive."
            f" See process logs in {arch_fail}"
        )


def add_additional_files_info(job_procs: List[Process], archive_name: str) -> None:
    additional_files = find_archive_external_files(job_procs)

    if additional_files:
        ad_files = str(Path(archive_name) / "additional_files.json")
        logger.warning(
            "Some files used in the project have not been included in the archive\n"
            f"Information about these files can be found in {ad_files}"
        )
        with open(ad_files, "w") as af:
            json.dump(additional_files, af, indent=4)


def is_non_node_input(filename: os.PathLike) -> bool:
    with ProjectGraph() as pipeline:
        node = pipeline.find_node(str(filename))
    if node is None:
        return False
    elif node.output_from_process is None:
        return True
    return False


def prepare_full_archive(process: Process, tar: bool = True) -> str:
    """Create an archive for a job

    A full archive copies the full job directories for the terminal job
      and all of its parents

    Args:
        process (:class:`~pipeliner.process.Process`): The `Process`
            object for the terminal job in the project
        tar (bool): Should the archive be compressed after creation?
    Returns:
        str: Name of the archive file or dir created

    """

    # get the archive name
    os.makedirs(ARCHIVE_DIR, exist_ok=True)
    archive_name = date_time_tag(compact=True) + "_project_archive"

    # check that all the jobs have completed and ready for archiving
    jp, inputs, outputs = get_archive_procs(process)
    job_procs = list(jp)
    job_procs.sort(key=lambda x: decompose_pipeline_filename(x.name)[1])
    for job_proc in job_procs:
        check_status_for_archiving(job_proc, do_full=True)

    # make the archive directory
    os.makedirs(archive_name)

    # make a pipeline for the archive
    with ProjectGraph(
        name="default",
        pipeline_dir=archive_name,
        create_new=True,
    ) as new_pipeline:
        job_inputs: List[Path] = []
        # make the archive directory and add the procs to the archive pipeline
        for job_proc in job_procs:
            archive_jobdir = str(Path(archive_name) / job_proc.name)
            shutil.copytree(job_proc.name, archive_jobdir)
            arch_job = active_job_from_proc(job_proc)
            arch_job.output_dir = archive_jobdir
            # Need do_overwrite=True or it fails trying to write the job's mini pipeline
            new_pipeline.add_job(arch_job, as_status=job_proc.status, do_overwrite=True)
            job_inputs.extend([Path(x.name) for x in job_proc.input_nodes])
    # put the project file into the directory
    shutil.copy(PROJECT_FILE, archive_name)

    # check for input files that weren't included because they're not outputs from jobs
    # check the existing files in case the file was uploaded with the doppio interface
    archive_files = [file for file in Path(archive_name).rglob("*") if file.is_file()]
    for input_file in job_inputs:
        if is_non_node_input(input_file) and input_file not in archive_files:
            archive_file_path = Path(archive_name) / input_file.parent
            archive_file_path.mkdir(parents=True, exist_ok=True)
            shutil.copy(input_file, archive_file_path)

    # add info about any extra files that were not input/output nodes but were used
    add_additional_files_info(job_procs, archive_name)

    movdir = os.path.join(ARCHIVE_DIR, archive_name)
    if tar:
        make_tarball(movdir, archive_name)
        shutil.rmtree(archive_name)
    else:
        shutil.move(archive_name, movdir)

    message = f"Created full archive {movdir}"
    update_jobinfo_file(process.name, action=message)
    return movdir


def prepare_script_archive(process: Process, tar: bool = True) -> str:
    """Create a script archive for a job

    A script (simple) archive just recreates the directory structure, saves the
    parameter files for each job, and writes a script to re-run the project.

    Args:
        process (:class:`~pipeliner.process.Process`): The `Process`
            object for the terminal job in the project
        tar (bool): Should the archive be compressed after creation?
    Returns:
        str: Name of the archive file or dir created

    """

    # get the archive name
    os.makedirs(ARCHIVE_DIR, exist_ok=True)
    archive_name = date_time_tag(compact=True) + "_project_archive"
    # make sets of jobs, their jobstar files, and input / output nodes
    jp, inputs, outputs = get_archive_procs(process)
    job_procs = list(jp)
    job_procs.sort(key=lambda x: decompose_pipeline_filename(x.name)[1])

    # check that all the jobs have completed and ready for archiving
    # collect job name conversions
    jobname_convs = {}
    for n, job_proc in enumerate(job_procs):
        check_status_for_archiving(job_proc, do_full=False)
        jobname_convs[job_proc.name] = f"{job_proc.outdir}/job{n+1:03d}/"

    # make the archive directory
    os.makedirs(archive_name)
    jobfile_dir = os.path.join(archive_name, "JobFiles")
    os.makedirs(jobfile_dir)

    # add any missing input files
    job_inputs = []
    for proc in job_procs:
        job_inputs.extend([Path(x.name) for x in proc.input_nodes])
    for input_file in job_inputs:
        if is_non_node_input(input_file):
            archive_file_path = Path(archive_name) / input_file.parent
            archive_file_path.mkdir(parents=True, exist_ok=True)
            shutil.copy(input_file, archive_file_path)

    # add info about files not included in the archive
    add_additional_files_info(job_procs, archive_name)

    # make the updated jobstar files
    for n, job in enumerate(job_procs):
        jobstar = JobStar(str(Path(job.name) / "job.star"))

        # convert all the job names first
        jobstar.make_substitutions(conversions=jobname_convs)

        updated_jobstar = f"{job.outdir.lower()}_{n+1:03d}_job.star"
        jobstar.write(str(Path(jobfile_dir) / updated_jobstar))

    # write the run script
    write_rerunning_script(job_procs, archive_name)

    movdir = os.path.join(ARCHIVE_DIR, archive_name)
    if tar:
        make_tarball(movdir, archive_name)
        shutil.rmtree(archive_name)
    else:
        shutil.move(archive_name, movdir)

    message = f"Created script archive {movdir}"
    update_jobinfo_file(process.name, action=message)
    return movdir


def add_archive_to_summary_file(
    archive_name: str, init_from: str, archive_type: str
) -> None:
    """Add an archive to the summary file

    Args:
        archive_name (str): The name of the archive
        init_from (str): Name of the job it was initiated from
        archive_type (str): "simple" or "full"
    """
    add_to_summary_file("project_archives", [archive_name, init_from, archive_type])


def remove_archive_from_summary_file(archive_name: str) -> None:
    """Remove an archive from the summary file

    Args:
        archive_name (str): The name of the archive
    """

    remove_from_summary_file("project_archives", archive_name)
