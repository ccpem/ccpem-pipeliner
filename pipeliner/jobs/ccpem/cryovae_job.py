#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    JobOptionCondition,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_stack,
    mini_montage_from_starfile,
)
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_IMAGE2DSTACK
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.utils import get_job_script


class CryoVAE(PipelinerJob):
    PROCESS_NAME = "cryovae.image_analysis"
    OUT_DIR = "CryoVAE"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "CryoVAE particle denoising"

        self.jobinfo.short_desc = "Denoise particle images"
        self.jobinfo.long_desc = (
            "VAE based denoising of particle images."
            "Takes a set of particles denoises them using cryoVAE"
        )
        self.jobinfo.programs = [
            ExternalProgram(command="cryovae"),
            ExternalProgram(command="relion_image_handler"),
            ExternalProgram(command="relion_stack_create"),
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Sony Malhotra, Matt Iadanza"
        self.jobinfo.references = [
            Ref(
                authors=["Malhotra S", "Jackson S"],
                title=(
                    "Deep learning-based automated filtering of particles"
                    "in single-particle cryoEM "
                ),
                journal="TBC",
                year="TBC",
                volume="TBC",
                issue="TBC",
                pages="TBC",
                doi="TBC",
            )
        ]
        self.jobinfo.documentation = (
            "https://gitlab.com/samueljackson/cryo-vae/-/tree/main/cryovae"
        )

        self.joboptions["input_star"] = InputNodeJobOption(
            label="Input particles file name:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles file", [".star"]),
            help_text=(
                "The input file is a Relion particles star file (optional) "
                "matching the input .mrcs file.\n"
            ),
            deactivate_if=JobOptionCondition([("input_raw_stack", "!=", "")]),
        )

        self.joboptions["input_raw_stack"] = InputNodeJobOption(
            label="OR: Input a raw particle stack:",
            node_type=NODE_IMAGE2DSTACK,
            default_value="",
            pattern=files_exts("Raw input stack", [".mrc", ".mrcs"]),
            help_text=("The input file is a stack of particles"),
            deactivate_if=JobOptionCondition([("input_star", "!=", "")]),
        )

        self.joboptions["lowpass_filter"] = BooleanJobOption(
            label="Low pass filer before denoising?",
            default_value=True,
            help_text="This will low pass filter the particles to 10Å before denoising",
        )

        self.joboptions["beta"] = FloatJobOption(
            label="Beta value for VAE:",
            default_value=0.1,
            help_text="beta value for VAE, recommended value is 0.1",
            is_required=True,
        )

        self.joboptions["epochs"] = IntJobOption(
            label="Max number of epochs:",
            default_value=100,
            suggested_min=10,
            suggested_max=100,
            step_value=1,
            help_text="Number of epochs",
            is_required=True,
        )

        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self):
        self.add_output_node(
            "recons.mrcs",
            NODE_IMAGE2DSTACK,
            ["cryovae", "denoised"],
        )
        self.add_output_node(
            "denoised_particles.star",
            NODE_PARTICLEGROUPMETADATA,
            ["cryovae", "denoised"],
        )

    # static so it can be called by cryoDANN job as well
    @staticmethod
    def get_make_stack_command(input: str, output_dir: str) -> List[str]:
        stack_com = ["relion_stack_create"]
        stack_com += ["--i", input]
        # relion creates .mrcs and .star as --o is rootname
        stack_com += ["--o", str(Path(output_dir) / "aligned_stacks")]
        stack_com += ["--apply_transformation"]
        return stack_com

    # static so it can be called by cryoDANN job as well
    @staticmethod
    def get_filter_stack_command(stackfile: str, output_dir: str) -> List[str]:
        lowpass_com = ["relion_image_handler"]
        lowpass_com += ["--i", stackfile]
        lowpass_com += ["--lowpass", "10"]
        lowpass_com += ["--o", str(Path(output_dir) / "lp_filtered_stack.mrcs")]
        return lowpass_com

    # static so it can be called by cryoDANN job as well
    @staticmethod
    def get_cryovae_command(
        stack_file: str, output_dir: str, epochs: int, beta: float
    ) -> List[str]:
        vae_com = ["cryovae"]
        vae_com += [stack_file]
        vae_com += [output_dir]
        vae_com += ["--max_epochs", str(epochs)]
        vae_com += ["--beta", str(beta)]
        return vae_com

    def get_commands(self) -> List[PipelinerCommand]:
        coms = []
        stack_name = self.joboptions["input_raw_stack"].get_string()
        star_name = self.joboptions["input_star"].get_string()

        # create the stack if necessary
        if self.joboptions["input_star"].get_string():
            coms.append(
                self.get_make_stack_command(input=star_name, output_dir=self.output_dir)
            )
            stack_name = str(Path(self.output_dir) / "aligned_stacks.mrcs")

        # do filtering if requested
        if self.joboptions["lowpass_filter"].get_boolean():
            coms.append(
                self.get_filter_stack_command(
                    stackfile=stack_name, output_dir=self.output_dir
                )
            )
            stack_name = str(Path(self.output_dir) / "lp_filtered_stack.mrcs")

        # add the cryoVAE command
        coms.append(
            self.get_cryovae_command(
                stack_file=stack_name,
                output_dir=self.output_dir,
                epochs=int(self.joboptions["epochs"].get_number()),
                beta=self.joboptions["beta"].get_number(),
            )
        )

        # make the output starfile
        make_sf_com = [
            "python3",
            get_job_script("cryovae/make_cryovae_starfile.py"),
            "--stackfile",
            str(Path(self.output_dir) / "recons.mrcs"),
            "--outdir",
            str(Path(self.output_dir)),
        ]
        if star_name:
            make_sf_com.extend(["--starfile", star_name])
        coms.append(make_sf_com)

        return [PipelinerCommand(x) for x in coms]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        stack_name = self.joboptions["input_raw_stack"].get_string()
        star_name = self.joboptions["input_star"].get_string()
        results_display: List[ResultsDisplayObject] = []
        if star_name:
            if self.joboptions["lowpass_filter"].get_boolean():
                stack_name = str(Path(self.output_dir) / "lp_filtered_stack.mrcs")
                title_in = "Sample input particles lowpass filtered to 10A"
            else:
                stack_name = str(Path(self.output_dir) / "aligned_stacks.mrcs")
                title_in = "Sample input particles"

        results_display.append(
            mini_montage_from_stack(
                stack_file=stack_name,
                outputdir=self.output_dir,
                nimg=20,
                title=title_in,
            )
        )
        results_display.append(
            mini_montage_from_starfile(
                starfile=str(Path(self.output_dir) / "denoised_particles.star"),
                block="particles",
                column="_rlnImageName",
                outputdir=self.output_dir,
                title="Sample denosied particles",
            )
        )
        results_display.append(
            create_results_display_object(
                "image",
                title="Latent space vectors",
                image_path=os.path.join(self.output_dir, "umap_embedding_scatter.png"),
                image_desc="Scatter plot of UMAP embedding of"
                "learned latent space vectors",
                associated_data=[os.path.join(self.output_dir, "umap_embedding.npy")],
            )
        )
        return results_display
