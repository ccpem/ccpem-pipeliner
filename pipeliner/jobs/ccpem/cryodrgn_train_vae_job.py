#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

from typing import List, Sequence, Tuple, Dict
import shlex
from pathlib import Path
from dataclasses import dataclass

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    IntJobOption,
    StringJobOption,
    FloatJobOption,
    BooleanJobOption,
    JobOptionCondition,
    EXT_STARFILE,
    JobOption,
)

# from pipeliner.utils import get_job_script
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MLMODEL,
    Node,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import create_results_display_object
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.starfile_handler import DataStarFile


def setup_common_jobops(joboptions: Dict[str, JobOption]):
    """Set up JobOptions which cryoDRGN VAE jobs have in common

    Args:
        joboptions (Dict[str, JobOption]): Job joboptions
    """
    # preprocessing-related jobops
    joboptions["fn_img"] = InputNodeJobOption(
        label="Input particles STAR file",
        node_type=NODE_PARTICLEGROUPMETADATA,
        default_value="",
        pattern=files_exts("STAR files", EXT_STARFILE),
        help_text=(
            "A STAR file with all images (and their metadata). Must contain"
            " CTF and pose information."
        ),
        is_required=True,
        in_continue=False,
    )

    joboptions["downsample"] = IntJobOption(
        label="Downsample picked particle images to size:",
        default_value=128,
        suggested_min=128,
        hard_min=4,
        suggested_max=256,
        step_value=32,
        help_text=(
            "Downsample picked particle images to have this many pixels."
            " Must be divisible by 2."
        ),
        is_required=True,
        in_continue=False,
    )

    # rand seed
    joboptions["rand_seed"] = IntJobOption(
        label="Random seed",
        default_value=-1,
        step_value=1,
        help_text=(
            "Random seed to use for reproducible training. If negative, the seed is"
            " not set and a default seed will be chosen instead."
        ),
        is_required=False,
        in_continue=False,
    )

    joboptions["use_chunk"] = BooleanJobOption(
        label="Split stacks into chunks?",
        default_value=False,
        help_text=(
            "If set to Yes, then mrcs files will be split into chunks"
            " during preprocessing. This allows a lower RAM footprint."
        ),
    )
    joboptions["chunk"] = IntJobOption(
        label="Chunk size",
        default_value=50000,
        suggested_min=1000,
        hard_min=1,
        step_value=1000,
        help_text=(
            "Chunksize (in # of particle images) to split mrcs file(s) into."
            " Useful for limiting RAM usage during preprocessing."
        ),
        deactivate_if=JobOptionCondition([("use_chunk", "=", False)]),
    )

    # toggle
    joboptions["advanced_options"] = BooleanJobOption(
        label="Edit advanced options?",
        default_value=False,
        help_text=(
            "Activate editing of advanced job options? Note that this option is only"
            " used to control the behaviour of the Doppio GUI. The values of the"
            " advanced options will still be used even if they appear disabled."
        ),
        in_continue=False,
    )

    # dataset loading
    joboptions["uninvert_data"] = BooleanJobOption(
        label="Do not invert data sign",
        default_value=False,
        help_text=(
            "By default, cryoDRGN assumes the particle images are light on a"
            " dark background. This is typical for most data sets. If instead the"
            " particles in this data set are inverted (i.e. dark on a light"
            " background), set this option and cryoDRGN will uninvert them before"
            " processing."
        ),
        in_continue=False,
        deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
        jobop_group="Dataset Loading",
    )

    # other args
    joboptions["cryoDRGN_other_args"] = StringJobOption(
        label="Additional CryoDRGN arguments:",
        default_value="",
        help_text=(
            "These additional arguments will be passed on to CryoDRGN train_vae"
            " program."
        ),
        deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
        jobop_group="Additional Arguments",
    )


def get_checkpoint_epochs(num_epochs: int, ckpt_freq: int) -> List[int]:
    """Determine which epoch indices were saved

    Args:
        num_epochs (int): Cumulative training epochs
        ckpt_freq (int): Weights checkpoint frequency

    Returns:
        List[int]: Indices of weights which should have been saved
    """
    return list(
        range(
            0,
            num_epochs,
            ckpt_freq,
        )
    )


def locate_weights_pkl(train_dir: Path, num_epochs: int, ckpt_freq: int) -> str:
    """If a cryoDRGN VAE job is manually changed from failed to succeeded we look
    for the final weights file. If not found, we look for intermediate weights files.
    If none are found we return an empty string

    Args:
        train_dir (Path): Directory containing ML model weights pickle (pkl) files
        num_epochs (int): Number of epochs of training
        ckpt_freq (int): Checkpoint frequency (in epochs) during training

    Returns:
        str: Filepath of weights pkl
    """
    final_weights = train_dir / "weights.pkl"
    output_weights_file = ""
    # check for final weights
    if Path(final_weights).is_file():
        output_weights_file = str(final_weights)
    # if intermediate weights exist take the final set of weights
    else:
        checkpoint_idx = get_checkpoint_epochs(num_epochs, ckpt_freq)
        # grab the last epoch index which should have saved weights
        for idx in reversed(checkpoint_idx):
            # check the weights file exists
            # if so, add as the output node and break out of the loop
            inter_weights = train_dir / f"weights.{idx}.pkl"
            if Path(inter_weights).is_file():
                output_weights_file = str(inter_weights)
                break
            # if not, iteratively keep going backwards through the list
            # to find a weights file until none are left
    return output_weights_file


@dataclass
class CryodrgnJobArgs:
    """Holds job arguments which are used during preprocessing in cryoDRGN
    VAE jobs
    """

    output_dir: Path
    metadata: str
    ctf_pkl: Path
    mrcs: Path
    downsample: int
    use_chunk: bool
    chunk: int
    particles: Path
    num_epochs: int
    train_dir: Path


def get_job_args(job_dir: str, joboptions: Dict[str, JobOption]) -> CryodrgnJobArgs:
    """Create a dataclass to hold the job arguments which are used during
    preprocessing in cryoDRGN VAE jobs

    Args:
        job_dir (str): Job output directory
        joboptions (Dict[str, JobOption]): Job joboptions

    Returns:
        CryodrgnJobArgs: A filled CryodrgnJobArgs dataclass object
    """
    output_dir = Path(job_dir)
    mrcs = output_dir / "mrcs"
    downsample = int(joboptions["downsample"].get_number())
    return CryodrgnJobArgs(
        output_dir=output_dir,
        metadata=joboptions["fn_img"].get_string(),
        ctf_pkl=output_dir / "ctf.pkl",
        mrcs=mrcs,
        downsample=downsample,
        use_chunk=joboptions["use_chunk"].get_boolean(),
        chunk=int(joboptions["chunk"].get_number()),
        particles=mrcs / f"particles.{downsample}.mrcs",
        num_epochs=int(joboptions["num_epochs"].get_number() - 1),
        train_dir=output_dir / f"train_{downsample}",
    )


def get_parse_ctf_cmd(ctf_pkl: Path, metadata: str) -> List[str]:
    """Construct the command to create a pickle file containing CTF
    information from a PARTICLEGROUPMETADATA STAR file

    Args:
        ctf_pkl (Path): File name for pickle file containing CTF information
        metadata (str): PARTICLEGROUPMETADATA STAR file

    Returns:
        List[str]: Command to run to create pickle file containing CTF information
    """
    return [
        "cryodrgn",
        "parse_ctf_star",
        "-o",
        str(ctf_pkl),
        metadata,
    ]


def get_downsample_cmd(
    downsample: int,
    metadata: str,
    mrcs: Path,
    particles: Path,
    use_chunk: bool,
    chunk: int,
) -> Tuple[List[str], List[str], str]:
    """Construct the command to downsample a particle stack and create a
    particle stack containing the downsampled particles in a subdirectory.
    A command to create the subdirectory is also returned along with the
    filename of the downsampled particle stack (or, if chunking has been
    used, a metadata file tracking the particle stack from each chunk)


    Args:
        downsample (int): Number of pixels to downsample particles to
        metadata (str): PARTICLEGROUPMETADATA STAR file
        mrcs (Path): Subdirectory to contain downsampled particle stacks
        particles (Path): Filename of downsampled particle stack
        use_chunk (bool): Whether to split particle stack into chunks
        chunk (int): Number of particle images to use in each chunk

    Raises:
        ValueError: Check for _rlnImageName column in STAR file
        ValueError: Ensure that STAR files contains particles from a single
                    extract job

    Returns:
        Tuple[List[str], List[str], str]: Command to create subdirectory for
                                          particle stacks, command to
                                          downsample particle stacks and the
                                          file name of the downsampled particle
                                          stack
    """
    # downsample
    # read the starfile column 6 _rlnImageName
    # split on @ and take the non-basename part
    # to provide to --datadir
    mrc_file_paths = []
    mrc_file_paths = DataStarFile(metadata).column_as_list(
        blockname="particles",
        colname="_rlnImageName",
    )
    if len(mrc_file_paths) == 0:
        raise ValueError(
            "Provided starfile likely does not have a _rlnImageName column "
            " in the particles data block. Please check and proceed with a "
            " starfile with this field present."
        )

    mrc_file_paths = [
        str(Path(mrc_fp.split("@")[1]).parent) for mrc_fp in mrc_file_paths
    ]
    # Only allowing starfiles from a single datadir
    # which is necessary to allow pipeliner+cryodrgn
    # to work together to point at the extracted particles
    uniq_dirs = set(mrc_file_paths)
    if not len(uniq_dirs) == 1:
        raise ValueError(
            "There are extracted particles from multiple directories present"
            " in the STAR file supplied. Please re-extract particles and"
            " supply the new particles STAR file instead."
        )
    mrc_file_path = uniq_dirs.pop()

    # we repeatedly use particles as a str from this point
    particles_str = str(particles)

    mrcs_subdir_cmd = ["mkdir", str(mrcs)]
    downsample_cmd = [
        "cryodrgn",
        "downsample",
        "-D",
        str(downsample),
        "-o",
        particles_str,
        "--datadir",
        mrc_file_path,
        metadata,
    ]
    if use_chunk:
        downsample_cmd = [
            "cryodrgn",
            "downsample",
            "-D",
            str(downsample),
            "-o",
            particles_str,
            "--datadir",
            mrc_file_path,
            "--chunk",
            str(chunk),
            metadata,
        ]
        # The following must be after downsampling cmd and before train_vae_cmd.
        # In this case there is a .txt file which allows cryoDRGN to track all
        # the downsampled particle stacks
        particles_str = ".".join(particles_str.split(".")[:-1]) + ".txt"

    return mrcs_subdir_cmd, downsample_cmd, particles_str


def get_analyze_cmd(num_epochs: int, train_dir: Path) -> List[str]:
    """Construct the command to analyze the trained model

    Args:
        num_epochs (int): Number of epochs of training
        train_dir (Path): Directory containing ML model weights pickle (pkl) files

    Returns:
        List[str]: Command to run analysis on trained model and create analysis plots
    """
    # analyze
    ad_name = f"analyze.{num_epochs}"
    analyze_dir = str(train_dir / ad_name)
    return [
        "cryodrgn",
        "analyze",
        str(train_dir),
        str(num_epochs),  # base 0
        "-o",
        analyze_dir,
    ]


def analyze_results_display(
    output_dir: Path, joboptions: Dict[str, JobOption]
) -> Sequence[ResultsDisplayObject]:
    """Create the ResultsDisplayObjects for cryoDRGN train_vae and abinit_het
    jobs

    Args:
        output_dir (Path): Job output directory
        joboptions (Dict[str, JobOption]): Job joboptions

    Returns:
        Sequence[ResultsDisplayObject]: Selected plots created by the cryodrgn
                                        analyze command
    """
    results_to_display = []

    # get downsampling, final epoch index, #kmeans cluster and #pc-dims
    downsampling = str(joboptions["downsample"].get_number())
    num_epochs = str(joboptions["num_epochs"].get_number() - 1)
    kmeans = 20
    pcs = 2
    # there are no outputs from training alone
    # but there are png image outputs from the analyze job, including:

    # loss per epoch of training (learning_curve_epoch{}.png)
    train_path = str(output_dir / f"train_{downsampling}")
    analyze_path = str(Path(train_path) / f"analyze.{num_epochs}")

    loss_plot_path = str(Path(analyze_path) / f"learning_curve_epoch{num_epochs}.png")
    loss_per_epoch = create_results_display_object(
        "image",
        title="Training Loss",
        image_path=loss_plot_path,
        image_desc="",
        associated_data=[],
    )
    results_to_display.append(loss_per_epoch)

    # z_pca.png, z_pca_hexbin.png, z_pca_marginals.png
    z_pca_marginals_path = str(Path(analyze_path) / "z_pca_marginals.png")
    z_pca_marginals = create_results_display_object(
        "image",
        title="PCA of latent space with density profiles",
        image_path=z_pca_marginals_path,
        image_desc="",
        associated_data=[],
    )
    z_pca_hexbin_path = str(Path(analyze_path) / "z_pca_hexbin.png")
    z_pca_hexbin = create_results_display_object(
        "image",
        title="PCA of latent space hex-binned",
        image_path=z_pca_hexbin_path,
        image_desc="",
        associated_data=[],
    )
    z_pca_path = str(Path(analyze_path) / "z_pca.png")
    z_pca = create_results_display_object(
        "image",
        title="PCA of latent space",
        image_path=z_pca_path,
        image_desc="",
        associated_data=[],
    )
    results_to_display.append(z_pca_marginals)
    results_to_display.append(z_pca_hexbin)
    results_to_display.append(z_pca)

    # umap.png, umap_hexbin.png, umap_marginals.png
    umap_marginals_path = str(Path(analyze_path) / "umap_marginals.png")
    umap_marginals = create_results_display_object(
        "image",
        title="UMAP of latent space with density profiles",
        image_path=umap_marginals_path,
        image_desc="",
        associated_data=[],
    )
    umap_hexbin_path = str(Path(analyze_path) / "umap_hexbin.png")
    umap_hexbin = create_results_display_object(
        "image",
        title="UMAP of latent space hex-binned",
        image_path=umap_hexbin_path,
        image_desc="",
        associated_data=[],
    )
    umap_path = str(Path(analyze_path) / "umap.png")
    umap = create_results_display_object(
        "image",
        title="UMAP of latent space",
        image_path=umap_path,
        image_desc="",
        associated_data=[],
    )
    results_to_display.append(umap_marginals)
    results_to_display.append(umap_hexbin)
    results_to_display.append(umap)

    # Then subdir with centres of {} kmeans clusters (from 2dim pca)
    # kmeans{}/z_pca.png|z_pca_hex.png|umap.png|umap_hex.png
    kmeans_path = Path(analyze_path) / f"kmeans{kmeans}"
    kmeans_z_pca_path = str(kmeans_path / "z_pca.png")
    kmeans_z_pca = create_results_display_object(
        "image",
        title="K-means centres on PCA of latent space",
        image_path=kmeans_z_pca_path,
        image_desc="",
        associated_data=[],
    )
    kmeans_z_pca_hex_path = str(kmeans_path / "z_pca_hex.png")
    kmeans_z_pca_hex = create_results_display_object(
        "image",
        title="K-means centres on hex-binned PCA of latent space",
        image_path=kmeans_z_pca_hex_path,
        image_desc="",
        associated_data=[],
    )
    kmeans_umap_path = str(kmeans_path / "umap.png")
    kmeans_umap = create_results_display_object(
        "image",
        title="K-means centres on UMAP of latent space",
        image_path=kmeans_umap_path,
        image_desc="",
        associated_data=[],
    )
    kmeans_umap_hex_path = str(kmeans_path / "umap_hex.png")
    kmeans_umap_hex = create_results_display_object(
        "image",
        title="K-means centres on hex-binned UMAP of latent space",
        image_path=kmeans_umap_hex_path,
        image_desc="",
        associated_data=[],
    )
    results_to_display.append(kmeans_z_pca)
    results_to_display.append(kmeans_z_pca_hex)
    results_to_display.append(kmeans_umap)
    results_to_display.append(kmeans_umap_hex)

    # Then subdirs with pc{} (default is pc2 via --pc flag of analyze) and
    # umap.png|umap_traversal.png|umap_traversal_connected.png and
    # pca_traversal.png|pca_traversal_hex.png
    for p_comp in range(pcs):
        pca_path = Path(analyze_path) / f"pc{p_comp + 1}"  # base 1
        pca_path_umap_traversal_connected = str(
            pca_path / "umap_traversal_connected.png"
        )
        p_comp_plot = create_results_display_object(
            "image",
            title=f"PCA component {p_comp + 1} traversal",
            image_path=pca_path_umap_traversal_connected,
            image_desc="",
            associated_data=[],
        )
        results_to_display.append(p_comp_plot)

    return results_to_display


class CryodrgnTrainVAEJob(PipelinerJob):
    """
    CryoDRGN train_vae v1 job for ccpem-pipeliner
    """

    PROCESS_NAME = "cryodrgn.train_vae"
    OUT_DIR = "CryoDRGN"
    CATEGORY_LABEL = "Continuous Heterogeneity"

    # (required) Instantiate the object: define info about it and its input parameters
    def __init__(self) -> None:
        # super(self.__class__, self).__init__()
        super().__init__()
        self.jobinfo.display_name = "CryoDRGN heterogeneous reconstruction"
        self.jobinfo.version = "0.0.1"
        self.jobinfo.job_author = "Joel Greer"
        self.jobinfo.programs = [
            ExternalProgram(command="cryodrgn"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Zhong ED", "Bepler T", "Berger B", "Davis J"],
                title=(
                    "CryoDRGN: reconstruction of heterogeneous cryo-EM structures"
                    " using neural networks"
                ),
                journal="Nature Methods",
                year="2021",
                volume="18",
                issue="2",
                pages="176-185",
                doi="https://doi.org/10.1038/s41592-020-01049-4",
            )
        ]
        self.jobinfo.short_desc = "CryoDRGN heterogeneous reconstruction"
        self.jobinfo.long_desc = (
            "CryoDRGN heterogeneous reconstruction using reconstructed poses."
            " Converts star files into pkls and uses those to train cryodrgn"
            " vae. Also runs cryoDRGN analyze on the final model."
        )
        self.jobinfo.documentation = "https://ez-lab.gitbook.io/cryodrgn"
        self.images = 0
        self.particles_per_image = 0

        # set up the common jobops between cryodrgn jobs
        setup_common_jobops(self.joboptions)

        # standard options
        self.joboptions["zdim"] = IntJobOption(
            label="Dimension of latent variable",
            default_value=8,
            hard_min=1,
            suggested_max=64,
            step_value=8,
            help_text="Dimension of latent variable to encode heterogeneity.",
            is_required=True,
            in_continue=False,
        )

        self.joboptions["num_epochs"] = IntJobOption(
            label="Total number of training epochs",
            default_value=25,
            suggested_min=5,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of training epochs (default: 25). "
                "Cumulative if using pretrained weights"
            ),
            is_required=True,
            in_continue=False,
        )

        # Epochs are continued on from the previous indices
        # which works out okay for the resultsdisplay
        self.joboptions["continue_from_weights"] = BooleanJobOption(
            label="Continue training starting from these initial weights?",
            default_value=False,
            help_text=(
                "If set to Yes, then CryoDRGN will continue training from the"
                " weights in the supplied pkl file."
            ),
        )
        self.joboptions["weights"] = InputNodeJobOption(
            label="CryoDRGN trained weights pkl file to continue from:",
            pattern=files_exts("Pickle files", [".pkl"]),
            default_value="",
            help_text="CryoDRGN trained weights pkl file to continue from.",
            is_required=False,
            in_continue=False,
            required_if=JobOptionCondition([("continue_from_weights", "=", True)]),
            node_type=NODE_MLMODEL,
            node_kwds=["cryoDRGN", "weights"],
            deactivate_if=JobOptionCondition([("continue_from_weights", "=", False)]),
        )

        # advanced options
        self.joboptions["checkpoint"] = IntJobOption(
            label="Checkpointing interval in N_EPOCHS",
            default_value=1,
            hard_min=1,
            suggested_max=25,
            step_value=1,
            help_text="Number of epochs between saving (intermediate) weights",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )
        self.joboptions["log_interval"] = IntJobOption(
            label="Logging interval in N_IMGS",
            default_value=1000,
            suggested_min=500,
            hard_min=1,
            suggested_max=10000,
            step_value=500,
            help_text="",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )

        self.joboptions["max_threads"] = IntJobOption(
            label="Maximum number of CPU cores for data loading",
            default_value=16,
            hard_min=1,
            suggested_max=32,
            step_value=1,
            help_text="Maximum number of CPU cores for data loading (default: 16)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Dataset Loading",
        )

        # training parameters
        self.joboptions["batch_size"] = IntJobOption(
            label="Minibatch size",
            default_value=8,
            suggested_min=8,
            hard_min=1,
            suggested_max=64,
            step_value=8,
            help_text="Minibatch size (default: 8)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["weight_decay"] = FloatJobOption(
            label="Weight decay in Adam optimizer",
            default_value=0.0,
            help_text="Weight decay in Adam optimizer (default: 0.0)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["learning_rate"] = FloatJobOption(
            label="Learning rate in Adam optimizer",
            default_value=0.0001,
            help_text="Learning rate in Adam optimizer (default: 0.0001)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )

        # encoder network
        self.joboptions["enc_layers"] = IntJobOption(
            label="Number of hidden layers in encoder",
            default_value=3,
            hard_min=1,
            suggested_max=6,
            step_value=1,
            help_text="Number of hidden layers in encoder network (default: 3)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Encoder Network",
        )
        self.joboptions["enc_dim"] = IntJobOption(
            label="Size of hidden layers in encoder",
            default_value=1024,
            suggested_min=128,
            hard_min=1,
            suggested_max=4096,
            step_value=256,
            help_text=(
                "Number of nodes in hidden layers in encoder network (default: 1024)"
            ),
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Encoder Network",
        )

        # decoder network
        self.joboptions["dec_layers"] = IntJobOption(
            label="Number of hidden layers in decoder",
            default_value=3,
            hard_min=1,
            suggested_max=6,
            step_value=1,
            help_text="Number of hidden layers in decoder network (default: 3)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Decoder Network",
        )
        self.joboptions["dec_dim"] = IntJobOption(
            label="Size of hidden layers in decoder",
            default_value=1024,
            suggested_min=128,
            hard_min=1,
            suggested_max=4096,
            step_value=256,
            help_text=(
                "Number of nodes in hidden layers in decoder network (default: 1024)"
            ),
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Decoder Network",
        )

        self.get_runtab_options(mpi=False, threads=False, addtl_args=False)

        # set jobop order
        self.set_joboption_order(
            [
                "fn_img",
                "zdim",
                "downsample",
                "num_epochs",
                "rand_seed",
                "use_chunk",
                "chunk",
                "continue_from_weights",
                "weights",
                "advanced_options",
                "checkpoint",
                "log_interval",
                "uninvert_data",
                "max_threads",
                "batch_size",
                "weight_decay",
                "learning_rate",
                "enc_layers",
                "enc_dim",
                "dec_layers",
                "dec_dim",
                "cryoDRGN_other_args",
            ]
        )

    # create output nodes
    def create_output_nodes(self) -> None:
        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(f"train_{downsample}")
        final_weights = train_dir / "weights.pkl"
        self.add_output_node(str(final_weights), NODE_MLMODEL, ["cryoDRGN"])

    def get_current_output_nodes(self) -> List[Node]:
        # called if user manually changes job status from failed to succeeded
        # looks for the weights file that should be present first, then if not
        # found, it looks for intermediate weights files
        nodes_found = []

        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(self.output_dir) / Path(f"train_{downsample}")
        # locate_weights_pkl file if exists
        output_weights_file = locate_weights_pkl(
            train_dir,
            int(self.joboptions["num_epochs"].get_number()),
            int(self.joboptions["checkpoint"].get_number()),
        )

        if output_weights_file:
            nodes_found.append(
                create_node(output_weights_file, NODE_MLMODEL, ["cryodrgn"])
            )
        return nodes_found

    def get_commands(self) -> List[PipelinerCommand]:
        # get args common between cryodrgn jobs
        args = get_job_args(self.output_dir, self.joboptions)

        # also construct pose pkl for train_vae
        pose_pkl = str(args.output_dir / "pose.pkl")
        # parse_pose
        parse_pose_cmd = [
            "cryodrgn",
            "parse_pose_star",
            "-o",
            pose_pkl,
            args.metadata,
        ]

        # parse_ctf
        parse_ctf_cmd = get_parse_ctf_cmd(args.ctf_pkl, args.metadata)

        # get mrcs_subdir_cmd, downsample_cmd and particles file
        mrcs_subdir_cmd, downsample_cmd, particles_str = get_downsample_cmd(
            args.downsample,
            args.metadata,
            args.mrcs,
            args.particles,
            args.use_chunk,
            args.chunk,
        )

        # train_vae
        train_vae_cmd = [
            "cryodrgn",
            "train_vae",
            particles_str,
            "--poses",
            pose_pkl,
            "--ctf",
            str(args.ctf_pkl),
            "--zdim",
            str(self.joboptions["zdim"].get_number()),
            "-n",
            str(args.num_epochs + 1),  # convert base 0 to base 1
            "-o",
            str(args.train_dir),
            "--checkpoint",
            str(self.joboptions["checkpoint"].get_number()),
            "--log-interval",
            str(self.joboptions["log_interval"].get_number()),
            "--max-threads",
            str(self.joboptions["max_threads"].get_number()),
            "-b",
            str(self.joboptions["batch_size"].get_number()),
            "--wd",
            str(self.joboptions["weight_decay"].get_number()),
            "--lr",
            str(self.joboptions["learning_rate"].get_number()),
            "--enc-layers",
            str(self.joboptions["enc_layers"].get_number()),
            "--enc-dim",
            str(self.joboptions["enc_dim"].get_number()),
            "--dec-layers",
            str(self.joboptions["dec_layers"].get_number()),
            "--dec-dim",
            str(self.joboptions["dec_dim"].get_number()),
        ]
        if self.joboptions["rand_seed"].get_number() >= 0:
            train_vae_cmd.append("--seed")
            train_vae_cmd.append(str(self.joboptions["rand_seed"].get_number()))
        if self.joboptions["uninvert_data"].get_boolean():
            train_vae_cmd.append("--uninvert-data")
        if self.joboptions["continue_from_weights"].get_boolean():
            train_vae_cmd.append("--load")
            train_vae_cmd.append(self.joboptions["weights"].get_string())
        # add usr args
        if self.joboptions["cryoDRGN_other_args"].get_string():
            other_args = shlex.split(
                self.joboptions["cryoDRGN_other_args"].get_string()
            )
            for arg in other_args:
                train_vae_cmd.append(arg)

        # analyze training results
        analyze_cmd = get_analyze_cmd(args.num_epochs, args.train_dir)

        final_commands_list: List[List[str]] = [
            parse_pose_cmd,
            parse_ctf_cmd,
            mrcs_subdir_cmd,
            downsample_cmd,
            train_vae_cmd,
            analyze_cmd,
        ]

        return [PipelinerCommand(x) for x in final_commands_list]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return analyze_results_display(Path(self.output_dir), self.joboptions)
