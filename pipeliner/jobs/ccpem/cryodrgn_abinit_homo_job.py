#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

from typing import List, Sequence
from pathlib import Path
import shlex

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    IntJobOption,
    FloatJobOption,
    JobOptionCondition,
    EXT_STARFILE,
)

# from pipeliner.utils import get_job_script
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MLMODEL,
    NODE_DENSITYMAP,
    Node,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.jobs.ccpem.cryodrgn_train_vae_job import (
    setup_common_jobops,
    locate_weights_pkl,
    get_job_args,
    get_parse_ctf_cmd,
    get_downsample_cmd,
    get_checkpoint_epochs,
)


class CryodrgnAbinitHomoJob(PipelinerJob):
    """
    CryoDRGN abinit homo v1 job for ccpem-pipeliner
    """

    PROCESS_NAME = "cryodrgn.abinit_homo"
    OUT_DIR = "CryoDRGN"
    CATEGORY_LABEL = "Continuous Heterogeneity"

    # (required) Instantiate the object: define info about it and its input parameters
    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "CryoDRGN ab-initio homogeneous reconstruction"
        self.jobinfo.version = "0.0.1"
        self.jobinfo.job_author = "Joel Greer"
        self.jobinfo.programs = [
            ExternalProgram(command="cryodrgn"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Zhong ED", "Lerer A", "Davis JH", "Berger B"],
                title=(
                    "CryoDRGN2: Ab initio neural reconstruction of 3D protein"
                    " structures from real cryo-EM images"
                ),
                journal=("IEEE/CVF International Conference on Computer Vision (ICCV)"),
                year="2021",
                volume="18",
                pages="4046-4055",
                doi="https://doi.org/10.1109/ICCV48922.2021.00403",
            )
        ]
        self.jobinfo.short_desc = "CryoDRGN ab-initio homogeneous reconstruction"
        self.jobinfo.long_desc = (
            "CryoDRGN ab-initio homogeneous reconstruction."
            " Converts star files into pkls and uses those to train cryodrgn"
            " abinit_homo to produce a reconstructed density map."
        )
        self.jobinfo.documentation = "https://ez-lab.gitbook.io/cryodrgn"
        self.images = 0
        self.particles_per_image = 0

        # set up the common jobops between cryodrgn jobs
        setup_common_jobops(self.joboptions)

        # standard options
        self.joboptions["fn_img"] = InputNodeJobOption(
            label="Input particles STAR file",
            node_type=NODE_PARTICLEGROUPMETADATA,
            default_value="",
            pattern=files_exts("STAR files", EXT_STARFILE),
            help_text=(
                "A STAR file with all images (and their metadata). Must contain"
                " CTF information."
            ),
            is_required=True,
            in_continue=False,
        )

        self.joboptions["num_epochs"] = IntJobOption(
            label="Total number of training epochs",
            default_value=30,
            suggested_min=5,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of training epochs (default: 30). "
                "Cumulative if using pretrained weights"
            ),
            is_required=True,
            in_continue=False,
        )

        # advanced options
        self.joboptions["checkpoint"] = IntJobOption(
            label="Checkpointing interval in N_EPOCHS",
            default_value=1,
            hard_min=1,
            suggested_max=25,
            step_value=1,
            help_text="Number of epochs between saving (intermediate) weights",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )
        self.joboptions["log_interval"] = IntJobOption(
            label="Logging interval in N_IMGS",
            default_value=1000,
            suggested_min=500,
            hard_min=1,
            suggested_max=10000,
            step_value=500,
            help_text="",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )

        # training parameters
        self.joboptions["batch_size"] = IntJobOption(
            label="Minibatch size",
            default_value=8,
            suggested_min=8,
            hard_min=1,
            suggested_max=64,
            step_value=8,
            help_text="Minibatch size (default: 8)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["weight_decay"] = FloatJobOption(
            label="Weight decay in Adam optimizer",
            default_value=0.0,
            help_text="Weight decay in Adam optimizer (default: 0.0)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["learning_rate"] = FloatJobOption(
            label="Learning rate in Adam optimizer",
            default_value=0.0001,
            help_text="Learning rate in Adam optimizer (default: 0.0001)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )

        self.get_runtab_options(mpi=False, threads=False, addtl_args=False)

        # set jobop order
        self.set_joboption_order(
            [
                "fn_img",
                "downsample",
                "num_epochs",
                "rand_seed",
                "use_chunk",
                "chunk",
                "advanced_options",
                "checkpoint",
                "log_interval",
                "uninvert_data",
                "batch_size",
                "weight_decay",
                "learning_rate",
                "cryoDRGN_other_args",
            ]
        )

    # create output nodes
    def create_output_nodes(self) -> None:
        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(f"train_{downsample}")
        final_weights = train_dir / "weights.pkl"
        self.add_output_node(str(final_weights), NODE_MLMODEL, ["cryoDRGN"])

        self.add_output_node(
            str(train_dir / "reconstruct.mrc"),
            NODE_DENSITYMAP,
            ["cryoDRGN"],
        )

    def get_current_output_nodes(self) -> List[Node]:
        # called if user manually changes job status from failed to succeeded
        # looks for the weights and reconstruct pkl and mrc
        # files that should be present first, then if not
        # found, it looks for intermediate weights files
        # If none are found a FileNotFound error is reported. There are no
        # other output nodes, so this should be sufficient
        nodes_found = []
        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(self.output_dir) / Path(f"train_{downsample}")
        # locate_weights_pkl file if exists
        output_weights_file = locate_weights_pkl(
            train_dir,
            int(self.joboptions["num_epochs"].get_number()),
            int(self.joboptions["checkpoint"].get_number()),
        )
        if output_weights_file:
            nodes_found.append(
                create_node(output_weights_file, NODE_MLMODEL, ["cryodrgn"])
            )

        # reconstruct.mrc nodes searched for
        final_reco = train_dir / "reconstruct.mrc"
        output_reco_file = ""
        # check for final reconstruction
        if Path(final_reco).is_file():
            output_reco_file = str(final_reco)
        # if intermediate reco exist take the final reco
        else:
            checkpoint_idx = get_checkpoint_epochs(
                int(self.joboptions["num_epochs"].get_number()),
                int(self.joboptions["checkpoint"].get_number()),
            )
            # grab the last epoch index which should have saved reco
            for idx in reversed(checkpoint_idx):
                # check the reco file exists
                # if so, add as the output node and break out of the loop
                inter_reco = train_dir / f"reconstruct.{idx}.mrc"
                if Path(inter_reco).is_file():
                    output_reco_file = str(inter_reco)
                    break
                # if not, iteratively keep going backwards through the list
                # to find a reco file until none are left

        if output_reco_file:
            nodes_found.append(
                create_node(output_reco_file, NODE_DENSITYMAP, ["cryodrgn"])
            )

        return nodes_found

    def get_commands(self) -> List[PipelinerCommand]:
        # get args common between cryodrgn jobs
        args = get_job_args(self.output_dir, self.joboptions)

        # parse_ctf
        parse_ctf_cmd = get_parse_ctf_cmd(args.ctf_pkl, args.metadata)

        # get mrcs_subdir_cmd, downsample_cmd and particles file
        mrcs_subdir_cmd, downsample_cmd, particles_str = get_downsample_cmd(
            args.downsample,
            args.metadata,
            args.mrcs,
            args.particles,
            args.use_chunk,
            args.chunk,
        )

        # abinit_homo
        abinit_homo_cmd = [
            "cryodrgn",
            "abinit_homo",
            particles_str,
            "--ctf",
            str(args.ctf_pkl),
            "-n",
            str(args.num_epochs + 1),  # convert base 0 to base 1
            "-o",
            str(args.train_dir),
            "--checkpoint",
            str(self.joboptions["checkpoint"].get_number()),
            "--log-interval",
            str(self.joboptions["log_interval"].get_number()),
            "-b",
            str(self.joboptions["batch_size"].get_number()),
            "--wd",
            str(self.joboptions["weight_decay"].get_number()),
            "--lr",
            str(self.joboptions["learning_rate"].get_number()),
        ]
        if self.joboptions["rand_seed"].get_number() >= 0:
            abinit_homo_cmd.append("--seed")
            abinit_homo_cmd.append(str(self.joboptions["rand_seed"].get_number()))
        if self.joboptions["uninvert_data"].get_boolean():
            abinit_homo_cmd.append("--uninvert-data")
        # add usr args
        if self.joboptions["cryoDRGN_other_args"].get_string():
            other_args = shlex.split(
                self.joboptions["cryoDRGN_other_args"].get_string()
            )
            for arg in other_args:
                abinit_homo_cmd.append(arg)

        final_commands_list: List[List[str]] = [
            parse_ctf_cmd,
            mrcs_subdir_cmd,
            downsample_cmd,
            abinit_homo_cmd,
        ]

        return [PipelinerCommand(x) for x in final_commands_list]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        total_epochs_idx = self.joboptions["num_epochs"].get_number() - 1

        # grab ouptut file dir locations
        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(self.output_dir) / f"train_{downsample}"

        # if the process is done use the output nodes
        final_map_path = train_dir / "reconstruct.mrc"
        if final_map_path.is_file():
            mrc_ons = [str(final_map_path)]
            curr_iter = total_epochs_idx
        else:
            mrc_results = [str(x) for x in train_dir.glob("reconstruct.*.mrc")]
            mrc_results.sort()
            curr_iter = int(mrc_results[-1].split(".")[1])
            mrc_ons = [str(x) for x in train_dir.glob(f"reconstruct.{curr_iter}.mrc")]
            mrc_ons.sort()

        maps = {}
        for mrc_out in mrc_ons:
            if curr_iter == total_epochs_idx:
                title = "Ab initio Model"
            else:
                title = (
                    f"Ab initio model - {Path(mrc_out).name} "
                    f"intermediate result iteration {curr_iter+1}/{total_epochs_idx+1}"
                )
            maps[mrc_out] = title
        return make_maps_slice_montage_and_3d_display(
            in_maps=maps,
            output_dir=self.output_dir,
        )
