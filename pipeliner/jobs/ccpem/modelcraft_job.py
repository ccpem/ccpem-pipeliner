#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import shutil
from glob import glob
from typing import List, Dict, Any, Sequence, Union, Tuple

from pipeliner import user_settings
from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_SEQUENCE,
    NODE_RESTRAINTS,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class ModelCraftJob(PipelinerJob):
    PROCESS_NAME = "modelcraft.atomic_model_build"
    OUT_DIR = "ModelCraft"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "ModelCraft"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.short_desc = "Automated atomic model building with ModelCraft"
        self.jobinfo.long_desc = (
            "Requires version 4.0.1 or above. "
            "ModelCraft via Buccaneer performs statistical chain"
            " tracing by identifying connected alpha-carbon positions using a "
            "likelihood-based density target. The target distributions are generated"
            "by a simulation calculation using a known reference structure for which"
            " calculated phases are available. The success of the method is dependent"
            " on the features of the reference structure matching those of the "
            "unsolved, work structure. For almost all cases, a single reference "
            "structure can be used, with modifications automatically applied to "
            "the reference structure to match its features to the work structure."
            " N.B. requires CCP4."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/cbuccaneer.html"
        self.modelcraft_program = ExternalProgram(
            command=user_settings.get_modelcraft_executable(),
            vers_com=[user_settings.get_modelcraft_executable(), "--version"],
            vers_lines=[0],
        )
        self.jobinfo.programs = [self.modelcraft_program]
        self.jobinfo.alternative_unavailable_reason = self.check_modelcraft_version()
        self.jobinfo.references = [
            Ref(
                authors=["Cowtan K"],
                title=(
                    "The Buccaneer software for automated model building."
                    "1. Tracing protein chains"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2006",
                volume="62",
                issue="Pt 9",
                pages="1002-11",
                doi="10.1107/S0907444906022116",
            ),
            Ref(
                authors=["Hoh SW", "Burnley T", "Cowtan K"],
                title=(
                    "Current approaches for automated model building"
                    " into cryo-EM maps using Buccaneer with CCP-EM."
                ),
                journal="Acta Crystallogr D Struct Biol",
                year="2020",
                volume="76",
                issue="Pt 6",
                pages="531-541",
                doi="10.1107/S2059798320005513",
            ),
            Ref(
                authors=["Bond P", "Cowtan K"],
                title=(
                    "ModelCraft: an advanced automated model-building"
                    " pipeline using Buccaneer."
                ),
                journal="Acta Crystallogr D Struct Biol",
                year="2022",
                volume="78",
                issue="Pt 9",
                pages="1090-1098",
                doi="10.1107/S2059798322007732",
            ),
        ]

        self.joboptions["job_title"] = StringJobOption(
            label="Job title",
            default_value="",
            help_text=(
                "This is the title of the job.  It can be a long string with spaces"
            ),
            is_required=False,
        )

        self.joboptions["input_map1"] = InputNodeJobOption(
            label="Input map 1 (half map 1 or full map)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            help_text=(
                "Either two half-maps or a single map in MRC format."
                " Input maps will be trimmed using Servalcat and a mask"
                " calculated by EMDA mapmask. If two half-maps are"
                " provided then Servalcat will be used to calculate a"
                " normalised expected (NE) map for model building"
            ),
            is_required=True,
        )

        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            help_text=(
                "Either two half-maps or a single map in MRC format."
                " Input maps will be trimmed using Servalcat and a mask"
                " calculated by EMDA mapmask. If two half-maps are"
                " provided then Servalcat will be used to calculate a"
                " normalised expected (NE) map for model building"
            ),
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Input map resolution",
            default_value=-1,
            suggested_min=1,
            suggested_max=10,
            step_value=0.1,
            hard_min=0.01,
            help_text="High resolution limit (Å)",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["input_seq"] = InputNodeJobOption(
            label="Input sequence",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Sequence", [".fasta", ".fas"]),
            help_text=(
                "A file with a description of the assymetric unit"
                " contents, either as a sequence file (in FASTA or PIR "
                " format) with both protein and nucleic acid sequences. "
            ),
            is_required=True,
        )

        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input map mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "N.B. Optional and requires ModelCraft >=3.4.0."
                " The mask to apply to the map. If not supplied a mask will be "
                " generated with EMDA."
            ),
            required_if=JobOptionCondition([("input_half_map2", "=", "")]),
        )

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Starting model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text=(
                "A starting model in PDB or mmCIF format. This could be "
                "a placed molecular-replacement model or partially-built model "
                "from another source. Residues that are not protein, RNA, DNA "
                "or water will be kept in place."
            ),
            in_continue=True,
        )

        self.joboptions["ncycle"] = IntJobOption(
            label="Max cycles of build-refine runs",
            default_value=25,
            suggested_min=1,
            hard_min=0,
            suggested_max=50,
            step_value=1,
            help_text=(
                "Maximum number of build-refine pipelines to perform. "
                "Pipeline will stop if there are no improvements on FSC "
                "when compared to previous best value for a set number of "
                "auto stop cycles."
            ),
            is_required=False,
        )

        self.joboptions["auto_stop_cycles"] = IntJobOption(
            label="Auto stop cycles without improvements",
            default_value=4,
            suggested_max=25,
            step_value=1,
            help_text=(
                "The number of cycles without improvements before the "
                "program stops automatically. Improvements is measured "
                "by FSC. A cycle must improve on the previous best value "
                "to be marked as an improvement. Setting this to less than 1 "
                "makes the program to run the maximum number of cycles."
            ),
            is_required=False,
        )

        # self.joboptions["ncycle_refmac"] = IntJobOption(
        #    label="Number of refmac cycles",
        #    default_value=20,
        #    suggested_min=1,
        #    hard_min=0,
        #    suggested_max=60,
        #    step_value=1,
        #    help_text="How many cycles of refmac to run",
        #    #deactivate_if=[("run_modelcraft", "=", True)],
        # )

        # self.joboptions["ncycle_buc1st"] = IntJobOption(
        #    label="Number of Buccaneer cycles in 1st pipeline",
        #    default_value=5,
        #    suggested_min=1,
        #    hard_min=0,
        #    suggested_max=10,
        #    step_value=1,
        #    help_text=(
        #        "Number of internal buccaneer cycles in the first "
        #        "pipeline. Default 5"
        #    ),
        #    #deactivate_if=[("run_modelcraft", "=", True)],
        # )

        # self.joboptions["ncycle_bucnth"] = IntJobOption(
        #    label="Number of Buccaneer cycles in subsequent pipelines",
        #    default_value=3,
        #    suggested_min=1,
        #    hard_min=0,
        #    suggested_max=10,
        #    step_value=1,
        #    help_text=(
        #        "Number of internal Buccaneer cycles in subsequent pipelines."
        #        " Default 3"
        #    ),
        #    #deactivate_if=[("run_modelcraft", "=", True)],
        # )

        self.joboptions["map_sharpen"] = FloatJobOption(
            label="B-factor to sharpen or blur",
            default_value=0.0,
            suggested_min=-200,
            suggested_max=200,
            step_value=1,
            help_text=(
                "B-factor to apply to the map, positive values "
                "for blurring and negative values for sharpening. "
                "Will be applied only when a single input map is provided."
            ),
            deactivate_if=JobOptionCondition([("input_half_map2", "!=", "")]),
            in_continue=True,
        )

        self.joboptions["lib_in"] = InputNodeJobOption(
            label="Ligand restraint dictionary file",
            node_type=NODE_RESTRAINTS,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            help_text=(
                "Library dictionary file for ligand(s) "
                "(lib/cif format) present in extend model"
            ),
        )
        self.get_runtab_options(addtl_args=True, threads=True)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # automated mask creation
        if not self.joboptions["input_mask"].get_string() and not shutil.which("emda"):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_mask"]],
                    message=(
                        "Input mask not provided and "
                        "'emda' is not available in the system PATH, "
                        "check that it is installed for automated mask calculation. "
                    ),
                )
            )
        return errors

    def check_modelcraft_version(self) -> str:
        modelcraft_version = self.modelcraft_program.get_version()
        if modelcraft_version:
            version_split = modelcraft_version.split(".")
            try:
                main_num = int(version_split[0])
                if main_num < 4 or main_num >= 5:
                    return (
                        f"Modelcraft version {modelcraft_version} found.\n"
                        "Modelcraft version < 4 and >= 5 are not currently supported"
                    )
            except (TypeError, IndexError, ValueError):
                return "Could not find Modelcraft version"
        return ""

    def create_output_nodes(self) -> None:
        self.add_output_node("modelcraft.cif", NODE_ATOMCOORDS, ["modelcraft"])

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        bucc_command: List[Union[int, float, str]] = [self.jobinfo.programs[0].command]
        # Get parameters
        input_map1 = self.joboptions["input_map1"].get_string()
        input_half_map2 = self.joboptions["input_half_map2"].get_string()
        input_seq = self.joboptions["input_seq"].get_string()
        input_ligand = self.joboptions["lib_in"].get_string()
        input_mask = self.joboptions["input_mask"].get_string()
        resolution = self.joboptions["resolution"].get_number()
        ncycle = self.joboptions["ncycle"].get_number()
        auto_stop_n = self.joboptions["auto_stop_cycles"].get_number()
        input_model = self.joboptions["input_model"].get_string()
        blur = self.joboptions["map_sharpen"].get_number()
        # other_args = self.joboptions["other_args"].get_string()
        # if len(other_args) > 0:
        #    bucc_command.extend(self.parse_additional_args())
        bucc_command += [
            "em",
            "--contents",
            os.path.relpath(input_seq, self.working_dir),
        ]
        if input_half_map2 != "":
            bucc_command += [
                "--map",
                os.path.relpath(input_map1, self.working_dir),
                os.path.relpath(input_half_map2, self.working_dir),
            ]
        else:
            bucc_command += ["--map", os.path.relpath(input_map1, self.working_dir)]

        bucc_command += ["--resolution", str(resolution)]
        if ncycle != 25:
            bucc_command += ["--cycles", str(ncycle)]
        if input_model != "":
            bucc_command += ["--model", os.path.relpath(input_model, self.working_dir)]
        if input_mask != "":
            bucc_command += ["--mask", os.path.relpath(input_mask, self.working_dir)]
        if input_ligand != "":
            bucc_command += [
                "--restraints",
                os.path.relpath(input_ligand, self.working_dir),
            ]
        if blur != 0.0:
            bucc_command += ["--blur", str(blur)]
        if auto_stop_n != 4:
            bucc_command += ["--auto-stop-cycles", str(auto_stop_n)]
        threads = int(self.joboptions["nr_threads"].get_number())
        bucc_command += ["--threads", threads]
        # to keep all files in pipeline
        # might need to configure Modelcraft in future to include keyword
        # to keep certain files
        bucc_command += ["--keep-files"]
        bucc_command += ["--directory", "."]
        bucc_command += ["--overwrite-directory"]

        commands = [PipelinerCommand(bucc_command)]
        return commands

    def get_modelcraft_json(self) -> str:
        return os.path.join(self.output_dir, "modelcraft.json")

    def gather_metadata(self) -> Dict[str, Any]:
        metadata_dict: Dict[str, Any] = {}
        with open(self.get_modelcraft_json(), "r") as jf:
            modelcraft_summary = json.load(jf)

        metadata_dict["BestCycle"] = modelcraft_summary["final"]["cycle"]
        # metadata_dict["residues_built"] = modelcraft_summary["final"]["residues"]
        metadata_dict["FSCAverage"] = modelcraft_summary["final"]["fsc"]
        cycle = 0
        for job in modelcraft_summary["jobs"]:
            if "cbuccaneer" in job["name"]:
                cycle += 1
            if cycle == metadata_dict["BestCycle"]:
                metadata_dict["CompletenessByResidues"] = float(job["completeness_res"])
                metadata_dict["CompletenessByChains"] = float(job["completeness_chn"])
                metadata_dict["ChainsBuilt"] = int(job["chains_built"])
                metadata_dict["FragmentsBuilt"] = int(job["fragments_built"])
                metadata_dict["ResiduesBuilt"] = int(job["residues_built"])
                metadata_dict["ResiduesSequenced"] = int(job["residues_sequenced"])
                metadata_dict["ResiduesUnique"] = int(job["residues_unique"])
                metadata_dict["LongestFragment"] = int(job["longest_fragment"])

        # root = et.parse(self.output_dir + "program1.xml").getroot()
        #
        # for x in root.findall("Final"):
        #    metadata_dict["CompletenessByResidues"] = float(
        #        x.find("CompletenessByResiduesBuilt").text
        #    )
        #    metadata_dict["CompletenessByChains"] = float(
        #        x.find("CompletenessByChainsBuilt").text
        #    )
        #    metadata_dict["ChainsBuilt"] = int(x.find("ChainsBuilt").text)
        #    metadata_dict["FragmentsBuilt"] = int(x.find("FragmentsBuilt").text)
        #    metadata_dict["ResiduesUnique"] = int(x.find("ResiduesUnique").text)
        #    metadata_dict["ResiduesBuilt"] = int(x.find("ResiduesBuilt").text)
        #    metadata_dict["ResiduesSequenced"] = int(x.find("ResiduesSequenced").text)
        #    metadata_dict["ResiduesLongestFragment"] = int(
        #        x.find("ResiduesLongestFragment").text
        #    )
        #
        # with open(self.output_dir + "refined1_summary.json", "r") as jf:
        #    refined_summary = json.load(jf)
        #
        # for k, v in refined_summary["cycles"][-1].items():
        #    if k != "cycle":
        #        refined_summary["cycles"][-1][k] = float(v)

        # metadata_dict["RefineStatistics"] = refined_summary["cycles"][-1]

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # model = self.output_dir + "refined1.pdb"
        # Create model map-model thumbnail and display
        model = os.path.join(self.output_dir, "modelcraft.cif")
        map_ = self.joboptions["input_map1"].get_string()
        mapmodel_disp = make_map_model_thumb_and_display(
            maps=[map_],
            maps_opacity=[0.5],
            models=[model],
            outputdir=self.output_dir,
        )
        # Create table and graphs
        display_objects: List[ResultsDisplayObject] = []
        summary_file = self.get_modelcraft_json()
        with open(summary_file, "r") as jf:
            modelcraft_summary = json.load(jf)
        # Make a table with built and refined summary for modelcraft.cif
        summary_data = []
        labels = ["Residues built", "FSC average"]
        summary_data.append(
            [
                modelcraft_summary["final"]["residues"],
                modelcraft_summary["final"]["fsc"],
            ]
        )
        table_bestmodel = create_results_display_object(
            "table",
            title="Residues built and FSC average for modelcraft.cif",
            headers=labels,
            table_data=summary_data,
            associated_data=[summary_file],
        )
        display_objects.append(table_bestmodel)
        display_objects.append(mapmodel_disp)
        # Get built and refined summary for each pipeline cycle
        res_built = [x["residues"] for x in modelcraft_summary["cycles"]]
        fscavg = [x["fsc"] for x in modelcraft_summary["cycles"]]
        # Make graphs only if pipeline ran more than one cycle
        # One dot on the graph is ugly
        if len(res_built) > 1:
            xv = [list(range(1, len(res_built) + 1))]
            graph_fscavg = create_results_display_object(
                "graph",
                xvalues=xv,
                xrange=[0, len(res_built) + 1],
                yvalues=[fscavg],
                yrange=[min(0.5, min(fscavg)), 1.0],
                title="FSC average",
                associated_data=[summary_file],
                data_series_labels=["fsc_average"],
                xaxis_label="Cycle",
                yaxis_label="FSC",
                modes=["lines+markers"],
                start_collapsed=False,
            )
            display_objects.append(graph_fscavg)
            graph_resbuilt = create_results_display_object(
                "graph",
                xvalues=xv,
                xrange=[0, len(res_built) + 1],
                yvalues=[res_built],
                yrange=[max(0, min(res_built) - 5), max(res_built) + 5],
                title="Residues built",
                associated_data=[summary_file],
                data_series_labels=["residues_built"],
                xaxis_label="Cycle",
                yaxis_label="Number of Residues",
                modes=["lines+markers"],
                start_collapsed=False,
            )
            display_objects.append(graph_resbuilt)

        return display_objects

    def prepare_clean_up_lists(
        self, do_harsh: bool = False
    ) -> Tuple[List[str], List[str]]:
        files_to_remove: list = []
        dirs_to_remove: list = []
        ext_rmlist = [
            "*.sh",
            "stderr.txt",
            "*.mtz",
            "*.mrc",
            "*.ccp4",
            "*.seq",
            "*.xml",
            "*.crd",
            "*.inp",
        ]
        # modelcraft_wdir = os.path.join(self.output_dir, "modelcraft")
        for ext in ext_rmlist:
            tmp_files = glob(os.path.join(self.output_dir, f"job_*/{ext}"))
            files_to_remove.extend(tmp_files)
        # harsh, rm all modelcraft/job_* directories
        # that's default for Modelcraft
        if do_harsh:
            tmp_dir = self.output_dir + "/job_*"
            dirs_to_remove.extend(tmp_dir)

        return files_to_remove, dirs_to_remove
