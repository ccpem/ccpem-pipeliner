#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path
from pipeliner.utils import get_job_script

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    IntJobOption,
    JobOptionCondition,
    StringJobOption,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_stack,
)
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_LOGFILE, NODE_IMAGE2DSTACK
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.display_tools import mini_montage_from_starfile

from pipeliner.jobs.ccpem.cryovae_job import CryoVAE


class CryoDANN(PipelinerJob):
    PROCESS_NAME = "cryodann.select.particles"
    OUT_DIR = "CryoDANN"
    CATEGORY_LABEL = "Subset Selection"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "CryoDANN particle filtering"

        self.jobinfo.short_desc = "Filter particle images to remove junk particles"
        self.jobinfo.long_desc = (
            "Selecting good quality particles using machine leanring"
            "Takes a set of denoised particle star file which is obtained from cryovae"
            "and returns quality score for each particle"
        )
        self.jobinfo.programs = [ExternalProgram(command="cryodann")]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Sony Malhotra and Matt Iadanza"
        self.jobinfo.references = [
            Ref(
                authors=["Malhotra S", "Jackson S"],
                title=(
                    "Deep learning-based automated filtering of particles in SPA cryoEM"
                ),
                journal="TBC",
                year="TBC",
                volume="TBC",
                issue="TBC",
                pages="TBC",
                doi="TBC",
            )
        ]
        self.jobinfo.documentation = "https://gitlab.com/samueljackson/cryodann/"

        self.joboptions["input_starfile"] = InputNodeJobOption(
            label="Input particles starfile:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles file", [".star"]),
            help_text=(
                "The input file is a Relion particles star file (optional) "
                "matching the input .mrcs file.\n"
                "If supplied an output star file will be written called good_particles"
                "which includes top N number of paticles using the threshold defined"
                "in keep_percent"
            ),
            is_required=False,
            required_if=JobOptionCondition([("input_stack", "=", "")]),
        )

        self.joboptions["input_stack"] = InputNodeJobOption(
            label="OR: Input a particle stack:",
            node_type=NODE_IMAGE2DSTACK,
            default_value="",
            pattern=files_exts("Input stack", [".mrc", ".mrcs"]),
            help_text=(
                "The input file is a Relion particles star file or a "
                ".mrcs file which is a particle stack obtained from cryovae"
            ),
            is_required=False,
            required_if=JobOptionCondition([("input_starfile", "=", "")]),
        )

        self.joboptions["run_cryovae"] = BooleanJobOption(
            label="Do cryoVAE denoising?",
            default_value=True,
            help_text="Run cryovae for denoising particles"
            "before assigning quality scores"
            "recommended as particle images are very noisy",
            jobop_group="Denoising options",
        )

        self.joboptions["vae_beta"] = FloatJobOption(
            label="Beta value for cryoVAE:",
            default_value=0.1,
            help_text="beta value for VAE, recommended value is 0.1",
            is_required=True,
            jobop_group="Denoising options",
            deactivate_if=JobOptionCondition([("run_cryovae", "!=", True)]),
            required_if=JobOptionCondition([("run_cryovae", "=", True)]),
        )

        self.joboptions["vae_epochs"] = IntJobOption(
            label="Max number of epochs for cryoVAE:",
            default_value=100,
            suggested_min=10,
            suggested_max=100,
            step_value=1,
            help_text="Number of epochs",
            jobop_group="Denoising options",
            deactivate_if=JobOptionCondition([("run_cryovae", "!=", True)]),
            required_if=JobOptionCondition([("run_cryovae", "=", True)]),
        )

        self.joboptions["threshold"] = FloatJobOption(
            label="Score threshold:",
            default_value=0.006,
            help_text="score threshold to retain top N particles",
            is_required=False,
            jobop_group="CryoDANN options",
        )
        self.joboptions["gamma"] = FloatJobOption(
            label="Gamma value:",
            default_value=0.01,
            help_text="The gamma parameter controls the trade-off between"
            "domain adaption & classification accuacy."
            "This is the most important parameter affecting domain adaption."
            "Default value is 0.01",
            is_required=False,
            jobop_group="CryoDANN options",
        )

        self.joboptions["dann_epochs"] = IntJobOption(
            label="Max number of epochs for CryoDANN:",
            default_value=50,
            suggested_min=10,
            suggested_max=50,
            step_value=1,
            help_text="Number of epochs",
            is_required=True,
            jobop_group="CryoDANN options",
        )

        self.joboptions["batch_size"] = IntJobOption(
            label="Batch size:",
            default_value=128,
            suggested_min=16,
            suggested_max=128,
            step_value=1,
            help_text="Batch size for SGD optimization",
            is_required=True,
            jobop_group="CryoDANN options",
        )

        self.joboptions["lr"] = FloatJobOption(
            label="Learning rate:",
            default_value=0.001,
            help_text="Learning rate step size for SGD optimization",
            is_required=False,
            jobop_group="CryoDANN options",
        )

        self.joboptions["train_set"] = StringJobOption(
            label="Path for training data:",
            help_text="Path for the training data downloaded from git repo",
            is_required=True,
            jobop_group="CryoDANN options",
        )
        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self):
        run_cryovae = self.joboptions["run_cryovae"].get_boolean()
        threshold = self.joboptions["threshold"].get_number()
        if run_cryovae:
            self.add_output_node(
                "recons.mrcs",
                NODE_IMAGE2DSTACK,
                ["cryovae", "denoised"],
            )
            self.add_output_node(
                "denoised_particles.star",
                NODE_PARTICLEGROUPMETADATA,
                ["cryovae", "denoised"],
            )

        self.add_output_node(
            os.path.join("lightning_logs", "index_confidence.txt"),
            NODE_LOGFILE,
            ["cryodann", "scores"],
        )
        if self.joboptions["input_starfile"].get_string():
            self.add_output_node(
                os.path.join(
                    "lightning_logs", "good_particles_t" + str(threshold) + ".star"
                ),
                NODE_PARTICLEGROUPMETADATA,
                ["cryodann", "good_particles"],
            )

    def get_commands(self) -> List[PipelinerCommand]:
        run_cryovae = self.joboptions["run_cryovae"].get_boolean()
        threshold = self.joboptions["threshold"].get_number()
        lr = self.joboptions["lr"].get_number()
        gamma = self.joboptions["gamma"].get_number()
        epochs = self.joboptions["dann_epochs"].get_number()
        batch_size = self.joboptions["batch_size"].get_number()

        # TODO: This needs to be in the project or a proper tmp dir
        # This is training data every cryodann job will look for,
        # where is the best place to keep this?
        relion_training_data = self.joboptions["train_set"].get_string()
        commands = []

        input_star = self.joboptions["input_starfile"].get_string()
        input_stack = self.joboptions["input_stack"].get_string()

        # if starfile input and no stack input -> make a stack
        if input_star and not input_stack:
            commands.append(
                CryoVAE.get_make_stack_command(
                    input=input_star, output_dir=self.output_dir
                )
            )
            input_stack = str(Path(self.output_dir) / "aligned_stacks.mrcs")

        # run cryoVAE if requested
        if run_cryovae:
            commands.append(
                CryoVAE.get_filter_stack_command(
                    stackfile=input_stack, output_dir=self.output_dir
                )
            )
            input_stack = str(Path(self.output_dir) / "lp_filtered_stack.mrcs")

            commands.append(
                CryoVAE.get_cryovae_command(
                    stack_file=input_stack,
                    output_dir=self.output_dir,
                    epochs=int(self.joboptions["vae_epochs"].get_number()),
                    beta=self.joboptions["vae_beta"].get_number(),
                )
            )
            make_sf_com = [
                "python3",
                get_job_script("cryovae/make_cryovae_starfile.py"),
                "--stackfile",
                # this only exists if cryovae is run
                str(Path(self.output_dir) / "recons.mrcs"),
                "--outdir",
                self.output_dir,
            ]
            if input_star:
                make_sf_com.extend(["--starfile", input_star])
            commands.append(make_sf_com)

        # get the cryoDANN cammand
        dann_command = [
            "cryodann",
            relion_training_data,
            input_stack,
            self.output_dir,
            "--keep_percent",
            str(threshold),
            "--max_epochs",
            str(epochs),
            "--gamma",
            str(gamma),
            "--lr",
            str(lr),
            "--batch_size",
            str(batch_size),
        ]

        if input_star:
            dann_command.extend(["--particle_file", input_star])

        commands.append(dann_command)
        return [PipelinerCommand(x) for x in commands]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        run_cryovae = self.joboptions["run_cryovae"].get_boolean()
        results_display: List[ResultsDisplayObject] = []
        stack_name = self.joboptions["input_stack"].get_string()
        log_dir = os.path.join(self.output_dir, "lightning_logs")
        # Input stack
        if stack_name and not run_cryovae:
            title_in = "Input particles low-pass filtered to 10A"
            results_display.append(
                mini_montage_from_stack(
                    stack_file=stack_name,
                    outputdir=self.output_dir,
                    nimg=20,
                    title=title_in,
                )
            )
        if run_cryovae:
            stack_name = str(Path(self.output_dir) / "lp_filtered_stack.mrcs")
            title_in = "Input particles low-pass filtered to 10A"
            results_display.append(
                mini_montage_from_stack(
                    stack_file=stack_name,
                    outputdir=self.output_dir,
                    nimg=20,
                    title=title_in,
                )
            )
            results_display.append(
                mini_montage_from_starfile(
                    starfile=str(Path(self.output_dir) / "denoised_particles.star"),
                    block="particles",
                    column="_rlnImageName",
                    outputdir=self.output_dir,
                    title="Sample denosied particles",
                )
            )
            results_display.append(
                create_results_display_object(
                    "image",
                    title="Latent space vectors",
                    image_path=os.path.join(
                        self.output_dir, "umap_embedding_scatter.png"
                    ),
                    image_desc=(
                        "Scatter plot of UMAP embedding of learned latent space"
                        " vectors"
                    ),
                    associated_data=[
                        os.path.join(self.output_dir, "umap_embedding.npy")
                    ],
                )
            )

        # Histogram of scores
        datfile = os.path.join(log_dir, "index_confidence.txt")
        with open(datfile, "r") as df:
            histodat = [float(x.split()[-1]) for x in df.readlines()]
        results_display.append(
            create_results_display_object(
                "histogram",
                title="Distribution of particle scores",
                data_to_bin=histodat,
                xlabel="cryoDANN score",
                ylabel="Number of particles",
                associated_data=[datfile],
            )
        )

        return results_display
