import os
from typing import List
from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    FloatJobOption,
    InputNodeJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
    MultipleChoiceJobOption,
    BooleanJobOption,
)
from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
)


class ModelShift(PipelinerJob):
    PROCESS_NAME = "ccpem_utils.atomic_model_utilities.shift_origin_nstart"
    OUT_DIR = "ModelShift"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Model shift"
        self.jobinfo.short_desc = (
            "Translate model based on map origin or nstart "
            "header records, or by (x,y,z) Angstroms"
        )
        self.jobinfo.long_desc = (
            "Use this job to shift model(s) based on a reference map origin record or a"
            " translation vector. Different tools handle the map origin/header records "
            "differently. A model may be fitted to a map considering its origin/nstart "
            "records, or ignoring it (zero origin or nstart). If the map origin record "
            "is ignored, the model may have to be shifted back to the non-zero origin "
            "to use it with another processing or visualisation tool. On the other "
            "hand, if the model coordinates are aligned with a map considering its "
            "origin/nstart, it may need to be shifted to align the position where the "
            "origin is zero, so they can be used with a certain tool that doesnt "
            "consider the origin record. For example, some of the CCP4 tools ignore "
            "map origin record, and the jobs that use these tools may fail as the "
            "model might need to be repositioned accordingly before using as input to "
            " these jobs. Or the output models from some of these jobs may have to be "
            "moved back to respect the map origin record.\n"
            "Another simple use case is when the user knows the exact translation "
            "vector (in Angstroms) required to move the model to another position."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Wojdyr, M.",
                ],
                title="GEMMI: A library for structural biology",
                journal="JOSS",
                year="2022",
                volume="73",
                issue="7",
                pages="4200",
                doi="10.21105/joss.04200",
            ),
            Ref(
                authors=[
                    "Burnley T",
                    "Palmer C",
                    "Winn M",
                ],
                title="Recent developments in the CCP-EM software suite",
                journal="Acta Cryst. D",
                year="2017",
                volume="73",
                issue="1",
                pages="469-477",
                doi="10.1107/S2059798317007859",
            ),
        ]

        self.joboptions["input_models"] = MultiInputNodeJobOption(
            label="Input associated models",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text=(
                "Add models that are to be shifted. All associated models will "
                "be shifted based on the input parameters."
            ),
            is_required=True,
        )
        # reference
        self.joboptions["reference_map"] = InputNodeJobOption(
            label="Input reference map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "The map to be used as reference to extract origin and/or nstart "
                "records. All the input models will be shifted based on the "
                "reference. "
            ),
            is_required=False,
        )
        self.joboptions["origin_or_nstart"] = MultipleChoiceJobOption(
            label="Header record to use",
            choices=["origin", "nstart"],
            default_value_index=0,
            help_text=(
                "Select reference map header record to use for shift: origin or nstart"
            ),
            deactivate_if=JobOptionCondition([("reference_map", "=", "")]),
        )
        self.joboptions["shift_nonzero"] = BooleanJobOption(
            label="Shift forward by map origin/nstart?",
            default_value=True,
            help_text=(
                "By default, coordinates will be translated by adding a vector that "
                "corresponds to map origin/nstart record. This is true in the case "
                "where the model was previously fitted ignoring the selected header "
                "record and needs to be translated back to a position that respects "
                "this. Select 'No' if this is not the intended purpose."
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "=", ""), ("shift_zero", "=", True)]
            ),
        )
        self.joboptions["shift_zero"] = BooleanJobOption(
            label="Shift back by map origin/nstart?",
            default_value=False,
            help_text=(
                "By default, model coordinates will be translated back by subtracting "
                "a vector that corresponds to map origin/nstart record. "
                "This is true in the case where the model was previously fitted "
                "respecting the selected header record and needs to be translated "
                "to a position that ignores this (zero). Select 'No' if this is not "
                "the intended purpose."
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "=", ""), ("shift_nonzero", "=", True)]
            ),
        )
        # no reference
        self.joboptions["trans_x"] = FloatJobOption(
            label="X Translation (Å)",
            default_value=0,
            help_text="Translate coordinates by this value along X axis",
            deactivate_if=JobOptionCondition([("reference_map", "!=", "")]),
        )
        self.joboptions["trans_y"] = FloatJobOption(
            label="Y Translation (Å)",
            default_value=0,
            help_text="Translate coordinates by this value along Y axis",
            deactivate_if=JobOptionCondition([("reference_map", "!=", "")]),
        )
        self.joboptions["trans_z"] = FloatJobOption(
            label="Z Translation (Å)",
            default_value=0,
            help_text="Translate coordinates by this value along Z axis",
            deactivate_if=JobOptionCondition([("reference_map", "!=", "")]),
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        if (
            not self.joboptions["reference_map"].get_string()
            and not self.joboptions["trans_x"].get_number()
            and not self.joboptions["trans_y"].get_number()
            and not self.joboptions["trans_z"].get_number()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["reference_map"]],
                    message=(
                        "Ensure either a reference map or a correct translation vector "
                        "are input."
                    ),
                )
            )
        if (
            self.joboptions["shift_nonzero"].get_boolean()
            and self.joboptions["shift_zero"].get_boolean()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["shift_nonzero"],
                        self.joboptions["shift_zero"],
                    ],
                    message=(
                        "Select only one of 'Shift forward' or 'Shift back' options. "
                    ),
                )
            )
        elif (
            not self.joboptions["shift_nonzero"].get_boolean()
            and not self.joboptions["shift_zero"].get_boolean()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["shift_nonzero"],
                        self.joboptions["shift_zero"],
                    ],
                    message="Select one of 'Shift forward' or 'Shift back' options. ",
                )
            )
        return errors

    @staticmethod
    def get_output_name(modelid, ext) -> str:
        return modelid + "_shifted" + ext

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        reference_map = self.joboptions["reference_map"].get_string()
        refmap_name = os.path.splitext(os.path.basename(reference_map))[0]
        keywords = ["shifted"]
        if reference_map:
            output_keyword = refmap_name + "_"
            origin_or_nstart = self.joboptions["origin_or_nstart"].get_string()
            output_keyword += origin_or_nstart
            if self.joboptions["shift_nonzero"].get_boolean():
                output_keyword += "_nonzero"
            keywords.append(output_keyword)
        for input_model in input_model_list:
            modelid, ext = os.path.splitext(unique_model_ids[input_model])
            shifted_model = self.get_output_name(modelid, ext)
            self.add_output_node(shifted_model, NODE_ATOMCOORDS, keywords)

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get parameters
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        origin_or_nstart = self.joboptions["origin_or_nstart"].get_string()
        reference_map = self.joboptions["reference_map"].get_string()
        for input_model in input_model_list:
            modelid, ext = os.path.splitext(unique_model_ids[input_model])
            input_model = os.path.relpath(input_model, self.working_dir)
            shift_command = ["python3", get_job_script("shift_model_refmap_origin.py")]
            shift_command += ["--model", input_model]
            if reference_map:
                shift_command += [
                    "--refmap",
                    os.path.relpath(reference_map, self.working_dir),
                ]
                shift_command += ["--use_record", origin_or_nstart]
                if self.joboptions["shift_nonzero"].get_boolean():
                    shift_command += ["--fitted_zero"]
            else:
                shift_command += [
                    "--tx",
                    str(self.joboptions["trans_x"].get_number()),
                    "--ty",
                    str(self.joboptions["trans_y"].get_number()),
                    "--tz",
                    str(self.joboptions["trans_z"].get_number()),
                ]
            shift_command += ["--ofile", self.get_output_name(modelid, ext)]
            commands.append(shift_command)
        return [PipelinerCommand(command) for command in commands]

    def create_results_display(
        self,
    ) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        reference_map = self.joboptions["reference_map"].get_string()
        list_shifted_models = []
        for input_model in input_model_list:
            modelid, ext = os.path.splitext(unique_model_ids[input_model])
            shifted_model = os.path.join(
                self.output_dir, self.get_output_name(modelid, ext)
            )
            list_shifted_models.append(shifted_model)
        display_maps = []
        if reference_map:
            display_maps = [reference_map]
        display_objects.append(
            make_map_model_thumb_and_display(
                maps=display_maps,
                models=list_shifted_models,
                outputdir=self.output_dir,
            )
        )
        return display_objects
