#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
from glob import glob
from typing import List, Sequence

from pipeliner.pipeliner_job import (
    PipelinerJob,
    Ref,
    ExternalProgram,
    PipelinerCommand,
    JobOptionValidationResult,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    BooleanJobOption,
    files_exts,
    IntJobOption,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import NODE_ATOMCOORDS
from pipeliner.results_display_objects import ResultsDisplayObject


class ProcessPredictedModel(PipelinerJob):
    """
    Process predicted model using MMTBX from CCP4.
    """

    PROCESS_NAME = "process_predicted_model.atomic_model_utilities"
    OUT_DIR = "ProcessPredictedModel"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Process predicted model"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = (
            "Process predicted models for downstream model building"
        )
        self.jobinfo.long_desc = (
            "MMTBX process predicted models.  Replace values in B-factor field with"
            " estimated B values. Removes low-confidence residues"
            " and split into domains. Recommend using PDB format inputs as mmCIF files"
            " are not fully supported at the moment. mmCIF format inputs will be"
            " converted to PDB internally before processing."
        )
        self.jobinfo.documentation = (
            "https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phasertng"
        )
        self.jobinfo.programs = [
            ExternalProgram("ccp4-python"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Oeffner RD",
                    "Croll TI",
                    "Millán C",
                    "Poon BK",
                    "Schlicksup CJ",
                    "Read RJ",
                    "Terwilliger TC",
                ],
                title=(
                    "Putting AlphaFold models to work with"
                    "phenix.process_predicted_model and ISOLDE"
                ),
                journal="Acta Cryst. D",
                year="2020",
                volume="D78",
                issue="",
                pages="1303-1314",
                doi="110.1107/S2059798322010026",
            ),
        ]

        self.joboptions["model"] = InputNodeJobOption(
            label="Predicted model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".pdb"]),
            default_value="",
            help_text=(
                "Predicted AF2 (or similar) model fetched from AF2 database."
                " Must be in PDB format"
            ),
            is_required=True,
        )

        self.joboptions["remove_low_confidence_residues"] = BooleanJobOption(
            label="Remove low confidence residues",
            default_value=True,
            help_text=("Remove residues with low pLDDT scores from the output model"),
        )

        self.joboptions["split_model_by_compact_regions"] = BooleanJobOption(
            label="Split model by compact regions",
            default_value=True,
            help_text=(
                "This will split the model by compact regions generating individual"
                " subdomains which can be used independently in downstream applications"
            ),
        )

        self.joboptions["max_domains"] = IntJobOption(
            label="Maximum domains to obtain",
            default_value=3,
            help_text=(
                "Maximum domains to obtain. You can use this to merge the closest"
                " domains at the end of splitting the model. Make it bigger (and"
                " optionally make domain_size smaller) to get more domains."
            ),
        )

        self.joboptions["domain_size"] = IntJobOption(
            label="Domain size",
            default_value=70,
            help_text=(
                "Approximate size of domains to be found (Å units). This is the"
                " resolution that will be used to make a domain map. If you are"
                " getting too many domains, try making domain_size bigger"
                " (maximum is 70 Å)."
            ),
        )

        self.get_runtab_options(addtl_args=True)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # model extension
        model_file = self.joboptions["model"].get_string()
        ext = os.path.splitext(model_file)[1].lower()
        if ext.lower() in (".cif", ".mmcif"):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["model"]],
                    message=(
                        "The input model will be converted to PDB format before "
                        "processing. "
                    ),
                )
            )
        return errors

    def create_output_nodes(self) -> None:
        # All output nodes are created after the job in create_post_run_output_nodes
        pass

    @staticmethod
    def get_cifconvert_command(input_model: str, converted_model: str) -> List[str]:
        gemmi_convert_model: List[str] = [
            "gemmi",
            "convert",
            "--shorten",
            input_model,
            converted_model,
        ]
        return gemmi_convert_model

    def get_commands(self) -> List[PipelinerCommand]:
        commands = []
        # Run in the job output directory
        self.working_dir = self.output_dir
        model_file = self.joboptions["model"].get_string()
        model_file_rel = os.path.relpath(model_file, self.working_dir)
        remove_low = self.joboptions["remove_low_confidence_residues"].get_boolean()
        split_model = self.joboptions["split_model_by_compact_regions"].get_boolean()
        # cif inputs seem to fail at the moment - possible bug in
        # mmtbx.process_predicted_model
        ext = os.path.splitext(model_file)[1].lower()
        if ext.lower() in (".cif", ".mmcif"):
            input_model_pdb = Path(model_file).name.replace(ext, ".pdb")
            commands.append(
                self.get_cifconvert_command(model_file_rel, input_model_pdb)
            )
            model_file_rel = input_model_pdb
        # The process_predicted_model from CCP4 is not placed on the path.
        get_ccp4_env_var = str(os.getenv("CCP4"))
        p_p_model_command = [
            os.path.join(
                get_ccp4_env_var,
                "bin/mmtbx.process_predicted_model",
            )
        ]
        p_p_model_command.append(model_file_rel)
        if remove_low:
            p_p_model_command.extend(["remove_low_confidence_residues=True"])
        else:
            p_p_model_command.extend(["remove_low_confidence_residues=False"])
        if split_model:
            p_p_model_command.extend(["split_model_by_compact_regions=True"])
        else:
            p_p_model_command.extend(["split_model_by_compact_regions=False"])
        max_domains = self.joboptions["max_domains"].get_number()
        p_p_model_command.append(f"maximum_domains={max_domains}")
        domain_size = self.joboptions["domain_size"].get_number()
        p_p_model_command.append(f"domain_size={domain_size}")
        p_p_model_command.extend(self.parse_additional_args())
        commands.append(p_p_model_command)
        return [PipelinerCommand(x) for x in commands]

    def create_post_run_output_nodes(self) -> None:
        model_file = self.joboptions["model"].get_string()
        search = Path(model_file).stem
        search_pdbs = f"{search}_processed*.pdb"
        pdbs = Path(self.output_dir).glob(search_pdbs)
        for pdb in sorted(pdbs):
            self.add_output_node(
                pdb.name, NODE_ATOMCOORDS, ["process_predicted", "synthetic"]
            )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:

        models = []
        for pdb in sorted(glob(os.path.join(self.output_dir, "*.pdb"))):
            models.append(pdb)

        return [
            make_map_model_thumb_and_display(
                maps=[],
                maps_opacity=[],
                models=models,
                title="Process predicted models",
                outputdir=self.output_dir,
            )
        ]
