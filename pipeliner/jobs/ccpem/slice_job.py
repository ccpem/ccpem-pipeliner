#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os

# import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colours
from matplotlib.axes import Axes
from matplotlib import gridspec
from matplotlib.figure import Figure
from matplotlib.colors import Normalize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from glob import glob
from PIL import Image
from typing import List, Dict, Any, Union

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    MultipleChoiceJobOption,
    MultiStringJobOption,
    InputNodeJobOption,
    MultiInputNodeJobOption,
    IntJobOption,
    FloatJobOption,
    JobOptionValidationResult,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    mini_montage_from_many_files,
)
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_LOGFILE, NODE_EVALUATIONMETRIC


class Slice(PipelinerJob):
    PROCESS_NAME = "slicendice.atomic_model_split"
    OUT_DIR = "Slice"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Slice"
        self.jobinfo.short_desc = "Slice model into sub-domains "
        self.jobinfo.long_desc = (
            "Slice atomic models into sub-domains, including options to "
            "trim models based on the B-factor column, and to cluster "
            "slices using different methods. The default clustering method "
            "is Birch"
        )
        self.jobinfo.programs = [
            ExternalProgram("slicendice")
        ]  # pathway to callable program
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        # XXX Todo add ref when available
        # self.jobinfo.references = [
        #     Ref(
        #         authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
        #         title=(
        #             "Cryo-EM single particle structure refinement and map "
        #             "calculation using Servalcat."
        #         ),
        #         journal="Acta Cryst. D",
        #         year="2022",
        #         volume="77",
        #         issue="1",
        #         pages="1282-1291",
        #         doi="10.1107/S2059798321009475",
        #     )
        # ]
        self.jobinfo.documentation = "https://gitlab.com/rmk65/slicendice"

        self.joboptions = self.add_slice_job_options(self.joboptions)

        self.get_runtab_options()

    def add_slice_job_options(self, joboptions):
        joboptions["input_model"] = MultiInputNodeJobOption(
            label="Input model(s)",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            # directory="",
            help_text="The input model(s) to be sliced",
            is_required=True,
            jobop_group="Slice options",
        )

        joboptions["bfactor_column"] = MultipleChoiceJobOption(
            label="B-factor column",
            choices=[
                "None",
                "pLDDT",
                "RMS",
                "Fractional pLDDT",
                "Predicted B-factor",
                "B-factor",
            ],
            default_value_index=1,
            help_text="The format of values stored in the B-factor column "
            "e.g. pLDDT for AlphaFold2 models, RMS for RosettaFold",
            jobop_group="Slice options",
        )

        joboptions["clustering_method"] = MultipleChoiceJobOption(
            label="Clustering method",
            choices=[
                "Agglomerative",
                "Birch",
                "dbscan",
                "kMeans",
                "MeanShift",
                "Optics",
                "PAE Clustering",
            ],
            default_value_index=1,
            help_text="Clustering method to slice up the atomic model",
            jobop_group="Slice options",
        )

        joboptions["minimum_splits"] = IntJobOption(
            label="Minimum Splits",
            default_value=3,
            suggested_min=1,
            hard_min=0,
            help_text="Minimum number of splits to make",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "=", "MeanShift"),
                    ("clustering_method", "=", "dbscan"),
                    ("clustering_method", "=", "PAE Clustering"),
                    ("xyz_split_list", "!=", ""),
                ],
                operation="ANY",
            ),
            is_required=True,
            jobop_group="Slice options",
        )

        joboptions["maximum_splits"] = IntJobOption(
            label="Maximum Splits",
            default_value=3,
            help_text="Maximum number of splits to make",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "=", "MeanShift"),
                    ("clustering_method", "=", "dbscan"),
                    ("clustering_method", "=", "PAE Clustering"),
                    ("xyz_split_list", "!=", ""),
                ],
                operation="ANY",
            ),
            is_required=True,
            jobop_group="Slice options",
            hard_min=1,
        )

        joboptions["xyz_split_list"] = MultiStringJobOption(
            label="Split list" " (comma separated list of split numbers, e.g. 1,2,3)",
            default_value="",
            help_text="When splitting multiple models, this option allows the user "
            "to specify the number of splits for each model."
            " If left blank, an error will be raised. ",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "=", "MeanShift"),
                    ("clustering_method", "=", "dbscan"),
                    ("clustering_method", "=", "PAE Clustering"),
                ],
                operation="ANY",
            ),
            jobop_group="Slice options",
        )
        # trimming options
        joboptions["plddt_threshold"] = IntJobOption(
            label="pLDDT Threshold",
            default_value=70,
            hard_min=0,
            hard_max=100,
            help_text="Removes residues from AlphaFold models below this "
            "pLDDT threshold",
            deactivate_if=JobOptionCondition([("bfactor_column", "!=", "pLDDT")]),
            jobop_group="Model trimming options",
        )

        joboptions["fractional_plddt_threshold"] = FloatJobOption(
            label="Fractional pLDDT Threshold",
            default_value=0.7,
            step_value=0.1,
            suggested_min=0.0,
            suggested_max=1.0,
            help_text="Removes residues from predicted models models below this "
            "fractional pLDDT threshold",
            deactivate_if=JobOptionCondition(
                [("bfactor_column", "!=", "Fractional pLDDT")]
            ),
            jobop_group="Model trimming options",
        )

        joboptions["rms_threshold"] = FloatJobOption(
            label="RMS threshold",
            default_value=1.75,
            suggested_min=0.0,
            help_text="Removes residues from Rosetta models below this RMS"
            " threshold",
            deactivate_if=JobOptionCondition([("bfactor_column", "!=", "RMS")]),
            jobop_group="Model trimming options",
        )

        # hyperparameters for clustering

        joboptions["auto_bandwidth"] = BooleanJobOption(
            label="Estimate bandwidth",
            default_value=True,
            help_text="Automatically estimate the bandwidth value used by "
            "MeanShift in the RBF kernel",
            deactivate_if=JobOptionCondition(
                [("clustering_method", "!=", "MeanShift")]
            ),
            jobop_group="Clustering options",
        )

        joboptions["bandwidth"] = FloatJobOption(
            label="Bandwidth",
            default_value=1.0,
            help_text="Set the bandwidth value used by MeanShift in the RBF kernel",
            deactivate_if=JobOptionCondition(
                [
                    ("auto_bandwidth", "=", True),
                    ("clustering_method", "!=", "MeanShift"),
                ],
                operation="ANY",
            ),
            jobop_group="Clustering options",
        )

        joboptions["eps_value"] = IntJobOption(
            label="Epsilon value",
            default_value=10,
            help_text="Set the epsilon value used by DBSCAN that defines how "
            "close together points need to be to be considered part "
            "of a cluster",
            deactivate_if=JobOptionCondition([("clustering_method", "!=", "dbscan")]),
            jobop_group="Clustering options",
        )

        # add Pae Clustering options
        joboptions["pae_file"] = InputNodeJobOption(
            label="Predicted Aligned Error (PAE) file input",
            node_type=NODE_EVALUATIONMETRIC,
            pattern=files_exts("PAE file", [".pkl", ".json"]),
            default_value="",
            help_text="PAE json file provided by the AFDB or PAE pkl "
            "file, generated by running AlphaFold2 locally in pTM "
            "mode. This is required if using pae_networkx or "
            "pae_igraph clustering methods",
            required_if=JobOptionCondition(
                [
                    ("clustering_method", "=", "PAE Clustering"),
                ],
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "!=", "PAE Clustering"),
                ],
            ),
            jobop_group="PAE clustering options",
        )

        joboptions["pae_method"] = MultipleChoiceJobOption(
            label="PAE method",
            choices=["networkx", "igraph"],
            default_value_index=0,
            help_text="Choice between networkx and igraph methods for PAE clustering",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "!=", "PAE Clustering"),
                ],
            ),
            jobop_group="PAE clustering options",
        )

        joboptions["pae_cutoff"] = IntJobOption(
            label="PAE cutoff",
            default_value=5,
            help_text="Graph edges will only be created for residue pairs "
            "with pae<pae_cutoff. This is only used by pae_networkx "
            "or pae_igraph clustering methods",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "!=", "PAE Clustering"),
                ],
            ),
            jobop_group="PAE clustering options",
        )

        joboptions["graph_resolution"] = IntJobOption(
            label="Graph resolution",
            default_value=1,
            help_text="Regulates how aggressively the clustering algorithm "
            "is. Smaller values lead to larger clusters. Value "
            "should be larger than zero, and values larger than 5 "
            "are unlikely to be useful. This is only used by "
            "pae_networkx or pae_igraph clustering methods",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "!=", "PAE Clustering"),
                ],
            ),
            jobop_group="PAE clustering options",
        )

        joboptions["pae_power"] = IntJobOption(
            label="PAE power",
            default_value=1,
            help_text="Each edge in the graph will be weighted proportional "
            "to (1/pae**pae_power) This is only used by"
            "pae_networkx or pae_igraph clustering methods",
            deactivate_if=JobOptionCondition(
                [
                    ("clustering_method", "!=", "PAE Clustering"),
                ],
            ),
            jobop_group="PAE clustering options",
        )

        return joboptions

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []

        jobop_min = self.joboptions["minimum_splits"]
        jobop_max = self.joboptions["maximum_splits"]
        if jobop_min.get_number() > jobop_max.get_number():
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[jobop_min, jobop_max],
                    message="The maximum number of splits must be "
                    "greater than the minimum number of splits",
                )
            )

        xyz_split = self.joboptions["xyz_split_list"].get_string()
        input_model_list = self.joboptions["input_model"].get_list()
        if len(xyz_split) == 0 and len(input_model_list) > 1:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["xyz_split_list"]],
                    message="The number of splits per model must be specified "
                    "for multiple input models",
                )
            )

        xyz_split_list = xyz_split.split(",")
        if len(xyz_split_list) != len(input_model_list):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["xyz_split_list"]],
                    message="The number of splits specified must match the "
                    "number of input models",
                )
            )

        if (
            not all([split.isdigit() for split in xyz_split_list])
            and len(input_model_list) > 1
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["xyz_split_list"]],
                    message="The split list must be a comma separated list of integers",
                )
            )

        if xyz_split != "" and len(input_model_list) <= 1:
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["xyz_split_list"]],
                    message="The split list is only required when multiple models "
                    "are provided. This will be ignored.",
                )
            )

        return errors

    def create_output_nodes(self):
        self.add_output_node(
            os.path.join("slicendice_0", "slicendice.log"),
            NODE_LOGFILE,
            ["slice", "report"],
        )

    def create_post_run_output_nodes(self):
        split_dirs = glob(os.path.join(self.output_dir, "slicendice_0/split_*"))
        if not split_dirs:
            split_dirs = glob(os.path.join(self.output_dir, "slicendice_0/*/split_*"))
        for split_dir in sorted(split_dirs):
            search_pdbs = os.path.join(split_dir, "*.pdb")
            pdbs = glob(search_pdbs)

            for pdb in pdbs:
                self.add_output_node(
                    os.path.relpath(pdb, self.output_dir),
                    NODE_ATOMCOORDS,
                    ["slice", "model"],
                )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        command: List[Union[int, float, str]] = [self.jobinfo.programs[0].command]
        # Get parameters
        input_models = self.joboptions["input_model"].get_list()
        bfactor_column = self.joboptions["bfactor_column"].get_string()
        clustering_method = self.joboptions["clustering_method"].get_string().lower()
        command += [
            "--bfactor_column",
            bfactor_column.lower().replace("-", ""),
        ]

        if len(input_models) > 1:
            command += ["--xyz_list"]
            command += [
                os.path.relpath(input_model, self.working_dir)
                for input_model in input_models
            ]
            command += ["--xyz_list_splits"]
            command += self.joboptions["xyz_split_list"].get_string().split(",")

        else:
            command += ["--xyzin", os.path.relpath(input_models[0], self.working_dir)]

        # Model trimming options
        if bfactor_column == "pLDDT":
            plddt_threshold = self.joboptions["plddt_threshold"].get_number()
            command += ["--plddt_threshold", str(plddt_threshold)]

        elif bfactor_column == "fractional pLDDT":
            fractional_plddt_threshold = self.joboptions[
                "fractional_plddt_threshold"
            ].get_number()
            command += ["--plddt_threshold", str(fractional_plddt_threshold * 100)]

        elif bfactor_column == "rosetta":
            rms_threshold = self.joboptions["rms_threshold"].get_number()
            command += ["--rmsd_threshold", str(rms_threshold)]

        if clustering_method == "meanshift":
            if not self.joboptions["auto_bandwidth"].get_boolean():
                bandwidth = self.joboptions["bandwidth"].get_number()
                command += ["--bandwidth", str(bandwidth)]

            command += ["--clustering_method", clustering_method]

        elif clustering_method == "dbscan":
            eps_value = self.joboptions["eps_value"].get_number()
            command += ["--clustering_method", clustering_method]
            command += ["--eps_value", str(eps_value)]

        # Pae options
        elif clustering_method == "pae clustering":
            pae_file = self.joboptions["pae_file"].get_string()
            pae_method = self.joboptions["pae_method"].get_string()
            pae_cutoff = self.joboptions["pae_cutoff"].get_string()
            pae_power = self.joboptions["pae_power"].get_string()
            command += ["--clustering_method", f"pae_{pae_method}"]
            command += ["--pae_file", os.path.relpath(pae_file, self.working_dir)]
            command += ["--pae_cutoff", pae_cutoff]
            command += ["--pae_power", pae_power]

        else:
            min_splits = self.joboptions["minimum_splits"].get_number()
            max_splits = self.joboptions["maximum_splits"].get_number()
            command += ["--clustering_method", clustering_method]
            command += ["--min_splits", str(min_splits)]
            command += ["--max_splits", str(max_splits)]

        commands = [PipelinerCommand(command)]
        return commands

    def gather_metadata(self) -> Dict[str, Any]:
        metadata_dict: Dict[str, Any] = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for _, line in enumerate(outlines):
            if "Splitting" in line:
                metadata_dict["SplitClusters"] = int(line.split()[3])

        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)

        split_displays, models_len = self.split_display(thumbdir)

        if models_len > 1:
            fig = self.create_subplot_grid(
                plots=[(self.create_colour_key, [models_len])]
            )
            self.plt_to_tif(fig, thumbdir)
            split_displays.append(
                mini_montage_from_many_files(
                    filelist=[os.path.join(thumbdir, "color_table.tif")],
                    title="Slice Colour Key",
                    outputdir=thumbdir,
                    start_collapsed=True,
                )
            )
        return split_displays

    def split_display(self, thumbdir: str):
        split_dirs = glob(os.path.join(self.output_dir, "slicendice_0/split_*/"))
        if not split_dirs:
            split_dirs = glob(os.path.join(self.output_dir, "slicendice_0/*/split_*/"))
        split_displays = []
        models_len = 0
        split_dirs = sorted(split_dirs)
        for split_dir in split_dirs:
            models = []
            for pdb in sorted(glob(os.path.join(split_dir, "*.pdb"))):
                models.append(pdb)
            title = os.path.basename(os.path.normpath(split_dir))
            title = title.replace("split_", "Slice: Split ")
            tab20 = plt.get_cmap("tab20")

            models_len = max(models_len, len(models))
            split_displays.append(
                make_map_model_thumb_and_display(
                    title=title,
                    models_colours=[
                        colours.rgb2hex(tab20(i)) for i in range(len(models))
                    ],
                    models=models,
                    outputdir=thumbdir,
                )
            )
        return split_displays, models_len

    def create_colour_key(self, ax: Axes, n: int, title: str = "Colour map: "):
        """
        n: int - number of cluster made during slicing
        """
        tab20 = plt.get_cmap("tab20")
        if n >= 20:
            n = 20
        labels = [f"{i}" for i in range(n)]

        cell_text = [[label] for label in labels]
        cell_text = [["Cluster:"]] + cell_text
        cell_colors = [[tab20(i)] for i in range(n)]
        cell_colors = [["w"]] + cell_colors

        bbox_height = 0.1 + 0.06 * n

        table = plt.table(
            cellText=[
                [" " * len(text[0])] + text for _, text in zip(cell_colors, cell_text)
            ],
            cellColours=[color + ["w"] for color in cell_colors],
            cellLoc="left",
            loc="bottom",
            bbox=[0.2, 1 - bbox_height, 0.6, bbox_height],
        )
        for _, cell in table.get_celld().items():
            cell.set_linewidth(0)

        cell_width = max([len(label) for label in labels]) * 0.1
        table.auto_set_column_width([0, 1])
        table.scale(cell_width, 1.5)

        ax.axis("off")
        ax.set_title(title)

        return table

    def create_colour_bar(self, ax: Axes, cmap: colours.Colormap, title="Colour Bar: "):
        """
        ax: matplotlib.axes.Axes - the axes object to draw the color bar on
        cmap: str - the name of the colormap
        """
        norm = Normalize(vmin=0, vmax=1)
        axins = inset_axes(ax, width="30%", height="80%", loc="center")
        cbar = plt.colorbar(
            plt.cm.ScalarMappable(norm=norm, cmap=cmap),
            ax=ax,
            orientation="vertical",
            location="right",
            aspect=10,
            cax=axins,
        )
        ax.axis("off")
        ax.set_title(title)

        return cbar

    def create_subplot_grid(self, plots: List[tuple]):
        """
        plots: dict - plotting functions as keys and their arguments as values
        axis: bool - whether to show the axis or not (default: False)
        """
        num_plots = len(plots)

        if num_plots == 0:
            raise ValueError("No plots to display")

        elif num_plots == 1:
            fig, ax = plt.subplots()
            plot_func, args = plots[0]
            plot_func(ax, *args)
            return fig

        elif not num_plots % 2 == 0:
            plots.append((self.empty_white_plot, []))
            num_plots += 1

        num_cols = 2
        num_rows = (num_plots + num_cols - 1) // num_cols

        fig = plt.figure()
        gs = gridspec.GridSpec(num_rows, num_cols)

        for i in range(num_plots):
            ax = fig.add_subplot(gs[i])
            plot_func, args = plots[i]
            plot_func(ax, *args)

        return fig

    def empty_white_plot(self, ax: Axes):
        """
        ax: matplotlib.axes.Axes - the axes object to draw the plot on
        """
        ax.axis("off")
        ax.set_facecolor("white")
        ax.margins(x=0, y=0)

    def plt_to_tif(
        self, fig: Figure, output_dir: str, outname: str = "color_table.tif"
    ):
        """
        n: int - number of cluster made during slicing
        output_dir: str - path to the output directory
        """
        plt.subplots_adjust(left=0.2, bottom=0.2, right=0.8, top=0.8)
        # is there a way to remove the white space around the plot?

        fig.tight_layout()

        fig.savefig(
            os.path.join(output_dir, outname),
            bbox_inches="tight",
            format="tif",
            dpi=300,
        )
        plt.close()

        rgba_fig = Image.open(os.path.join(output_dir, outname)).convert("RGBA")
        rgb_fig = rgba_fig.convert("RGB")
        rgb_fig.save(os.path.join(output_dir, outname))
