import os
import shutil
import json
from collections import OrderedDict
from typing import List, Dict, Union, Optional, Tuple
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    BooleanJobOption,
    MultiInputNodeJobOption,
    FloatJobOption,
    InputNodeJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import Ref
from ccpem_utils.map.mrcfile_utils import (
    calc_mrc_sigma_contour,
)
from pipeliner.display_tools import (
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_EVALUATIONMETRIC,
)


class TEMPyMapFit(PipelinerJob):
    PROCESS_NAME = "tempy.atomic_model_validation.map_fit"
    OUT_DIR = "TEMPy_scores"
    job_references = [
        Ref(
            authors=[
                "Cragnolini T",
                "Sahota H",
                "Joseph AP",
                "Sweeney A",
                "Malhotra S",
                "Vasishtan D",
                "Topf M",
            ],
            title=(
                "TEMPy2: a Python library with improved 3D electron microscopy "
                "density-fitting and validation workflows"
            ),
            journal="Acta Cryst. D",
            year="2021",
            volume="77",
            issue="1",
            pages="41-47",
            doi="10.1107/S2059798320014928",
        ),
        Ref(
            authors=[
                "Joseph AP",
                "Malhotra S",
                "Burnley, T",
                "Wood C",
                "Clare DK",
                "Winn M",
                "Topf M",
            ],
            title=(
                "Refinement of atomic models in high resolution "
                "EM reconstructions using Flex-EM and local assessment."
            ),
            journal="Methods",
            year="2016",
            volume="100",
            issue="1",
            pages="42-49",
            doi="10.1016/j.ymeth.2016.03.007",
        ),
    ]
    deactivation_conditions: Dict[str, JobOptionCondition] = {
        "run_smoc_only": JobOptionCondition([("run_global_only", "=", True)]),
        "run_global_only": JobOptionCondition([("run_smoc_only", "=", True)]),
        "input_mask": JobOptionCondition([("run_smoc_only", "=", True)]),
        "input_contour": JobOptionCondition(
            [
                ("run_smoc_only", "=", True),
                ("input_mask", "!=", ""),
                ("calc_contour", "=", True),
            ],
            "ANY",
        ),
        "contourlevel": JobOptionCondition(
            [
                ("input_contour", "=", False),
                ("input_mask", "!=", ""),
                ("run_smoc_only", "=", True),
            ],
            "ANY",
        ),
        "calc_contour": JobOptionCondition(
            [
                ("input_contour", "=", True),
                ("input_mask", "!=", ""),
                ("run_smoc_only", "=", True),
            ],
        ),
    }
    tempyscore_names = {
        "ccc_mask": "CCC overlap mask",
        "mi_mask": "MI overlap mask",
        "overlap": "Model overlap fraction",
        "ccc_contour": "CCC contoured map",
    }

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "TEMPy Scores"
        self.jobinfo.short_desc = "Atomic model map-fit assessment"
        self.jobinfo.long_desc = (
            "Assess fit-to-map using TEMPy real space scores.\n"
            "Reports multiple global scores including cross-correlation coefficient, \n"
            "model-map overlap, and mutual information.\n"
            "Also reports per-residue outliers based on the SMOC score.\n"
            "To install TEMPy, use \n"
            "'pip install biotempy'. Requires biotempy>=2.2.0a3"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(command="TEMPy.smoc"),
            ExternalProgram(command="TEMPy.scores"),
        ]
        self.jobinfo.references = TEMPyMapFit.job_references
        self.jobinfo.documentation = "https://tempy.topf-group.com/"

        self.joboptions["input_models"] = MultiInputNodeJobOption(
            label="Input models",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="Add multiple models to compare the scores.",
            is_required=True,
        )
        # OTHER OPTIONS BASED ON METRIC SELECTION
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The input map to evaluate the model against",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            hard_min=0.1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in angstroms",
            is_required=True,
        )
        # Specific modes
        self.joboptions["run_global_only"] = BooleanJobOption(
            label="Calculate global scores only?",
            default_value=False,
            help_text=(
                "Calculates global map-fit scores (single values for each model-map)"
                " only."
            ),
            deactivate_if=self.deactivation_conditions["run_global_only"],
            jobop_group="Run options",
        )
        self.joboptions["run_smoc_only"] = BooleanJobOption(
            label="Calculate SMOC score only?",
            default_value=False,
            help_text="Calculate per-residue SMOC scores only",
            deactivate_if=self.deactivation_conditions["run_smoc_only"],
            jobop_group="Run options",
        )
        # other inputs for global scores
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input map mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The mask to apply to the map (preferred over Contour Level)"
            " (used by global scores)",
            is_required=False,
            deactivate_if=self.deactivation_conditions["input_mask"],
            jobop_group="Global score options",
        )
        self.joboptions["input_contour"] = BooleanJobOption(
            label="Input contour threshold?",
            default_value=False,
            help_text=(
                "The map will be contoured at this threshold"
                ", ignored if input mask provided."
            ),
            deactivate_if=self.deactivation_conditions["input_contour"],
            jobop_group="Global score options",
        )
        self.joboptions["contourlevel"] = FloatJobOption(
            label="Contour Level",
            default_value=-9999,
            step_value=0.01,
            help_text=(
                "Contour level for masking. Recommended when an input mask is "
                "not provided."
            ),
            deactivate_if=self.deactivation_conditions["contourlevel"],
            jobop_group="Global score options",
        )
        self.joboptions["calc_contour"] = BooleanJobOption(
            label="Auto estimate contour?",
            default_value=False,
            help_text=(
                "A contour level is calculated based on the sigma of map distribution. "
                "This may not work well for certain unusual distributions"
            ),
            deactivate_if=self.deactivation_conditions["calc_contour"],
            jobop_group="Global score options",
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # maximum input models
        input_model_list = self.joboptions["input_models"].get_list()
        if len(input_model_list) > 3:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_models"]],
                    message="A maximum of 3 models can be assessed",
                )
            )
        if not self.joboptions["run_smoc_only"].get_boolean() and not shutil.which(
            "TEMPy.scores"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_global_only"]],
                    message=(
                        "'TEMPy.scores' is not available in the system path, "
                        "check that TEMPy2 is installed."
                    ),
                )
            )
        if not self.joboptions["run_global_only"].get_boolean() and not shutil.which(
            "TEMPy.smoc"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_smoc_only"]],
                    message=(
                        "'TEMPy.smoc' is not available in the system path, "
                        "check that TEMPy2 is installed."
                    ),
                )
            )
        if (
            not self.joboptions["run_smoc_only"].get_boolean()
            and not self.joboptions["input_mask"].get_string()
            and not self.joboptions["input_contour"].get_boolean()
            and not self.joboptions["calc_contour"].get_boolean()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_contour"]],
                    message=(
                        "Please either provide a mask, input a contour threshold "
                        "or auto estimate"
                    ),
                )
            )
        if (
            not self.joboptions["run_smoc_only"].get_boolean()
            and self.joboptions["input_contour"].get_boolean()
            and self.joboptions["contourlevel"].get_number() == -9999
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["contourlevel"]],
                    message=("Ensure a valid contour level is input"),
                )
            )
        return errors

    @staticmethod
    def get_scores_outcsv(modelid: str, mapid: str) -> str:
        return (
            "globscores_{}_vs_{}".format(
                modelid,
                mapid,
            )
            + ".csv"
        )

    @staticmethod
    def get_smoc_outjson(modelid: str, mapid: str) -> str:
        return modelid + "_" + mapid + "_smoc_score.json"

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        input_map = self.joboptions["input_map"].get_string()
        if input_map:
            mapid = os.path.splitext(os.path.basename(input_map))[0]
        run_global_only = self.joboptions["run_global_only"].get_boolean()
        run_smoc_only = self.joboptions["run_smoc_only"].get_boolean()
        # loop through each model
        for input_model in input_model_list:
            modelid = os.path.splitext(unique_model_ids[input_model])[0]
            # Now make the nodes
            if not run_smoc_only:
                score_outcsv = self.get_scores_outcsv(modelid, mapid)
                self.add_output_node(
                    score_outcsv, NODE_EVALUATIONMETRIC, ["tempy", "global_scores"]
                )
            if not run_global_only:
                smoc_outjson = self.get_smoc_outjson(modelid, mapid)
                self.add_output_node(
                    smoc_outjson, NODE_EVALUATIONMETRIC, ["tempy", "smoc_scores"]
                )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get parameters
        input_model_list = self.joboptions["input_models"].get_list()
        input_map = self.joboptions["input_map"].get_string()
        if input_map:
            input_map = os.path.relpath(input_map, self.working_dir)
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        resolution = self.joboptions["resolution"].get_number()
        input_contour = self.joboptions["input_contour"].get_boolean()
        contourlevel = None
        if input_contour:
            contourlevel = self.joboptions["contourlevel"].get_number()
        calc_contour = self.joboptions["calc_contour"].get_boolean()
        run_global_only = self.joboptions["run_global_only"].get_boolean()
        run_smoc_only = self.joboptions["run_smoc_only"].get_boolean()
        input_mask = self.joboptions["input_mask"].get_string()
        # get or estimate contour threshold
        contour = self.calc_contour_from_input_options(
            input_map=input_map,
            input_mask=input_mask,
            contourlevel=contourlevel,
            calc_contour=calc_contour,
        )
        # apply mask to the map if provided
        if not run_smoc_only and input_mask:  # use masked map with contour > 0
            input_mask = os.path.relpath(input_mask, self.working_dir)
            commands.append(
                self.get_applymask_command(input_map=input_map, input_mask=input_mask)
            )
            masked_map = (
                os.path.splitext(os.path.basename(input_map))[0] + "_masked.mrc"
            )
        else:  # use input map with a contour
            masked_map = input_map
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        for input_model in input_model_list:
            modelid = os.path.splitext(unique_model_ids[input_model])[0]
            input_model = os.path.relpath(input_model, self.working_dir)
            # modelid = os.path.splitext(os.path.basename(input_model))[0]
            if not run_smoc_only:
                commands.append(
                    self.get_tempy_score_command(
                        input_model=input_model,
                        input_map=masked_map,
                        resolution=resolution,
                        contour=contour,
                        mapid=mapid,
                        modelid=modelid,
                    )
                )
            if not run_global_only:
                commands.append(
                    self.get_smoc_run_command(
                        input_map, input_model, resolution, modelid, mapid
                    )
                )
                # zscore calculation
                residue_coordinates_command = self.get_residue_coordinates_command(
                    input_model=input_model, modelid=modelid
                )
                commands.append(residue_coordinates_command)
                commands.append(self.get_smoc_results_command(modelid, mapid))
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_applymask_command(input_map: str, input_mask: str = "") -> List[str]:
        applymask_command = [
            "python3",
            get_job_script("model_validation/apply_map_mask.py"),
            "-m",
            input_map,
            "-ma",
            input_mask,
            "--ignore_edge",
        ]
        return applymask_command

    @staticmethod
    def calc_contour_from_input_options(
        input_map: str,
        input_mask: str = "",
        contourlevel: Optional[float] = None,
        calc_contour: bool = True,
    ) -> float:
        contour = 0.000001
        # apply mask
        if not input_mask and contourlevel:  # input_contourlevel
            contour = contourlevel
        elif not input_mask:  # no mask and contour input
            if os.path.isfile(input_map) and calc_contour:
                contour = round(calc_mrc_sigma_contour(input_map), 3)
        return contour

    @staticmethod
    def get_tempy_score_command(
        input_model: str,
        input_map: str,
        resolution: float,
        contour: float,
        mapid: str = "",
        modelid: str = "",
    ) -> List:
        # generate model-map and halfmap fsc with model mask
        tempyglob_command: List[Union[int, float, str]] = [
            "TEMPy.scores",
            "-m",
            input_map,
            "-p",
            input_model,
            "-r",
            str(resolution),
            "--contour-level",
            str(contour),
            "--output-prefix",
            os.path.splitext(
                TEMPyMapFit.get_scores_outcsv(modelid=modelid, mapid=mapid)
            )[0],
            "--output-format",
            "csv",
        ]
        return tempyglob_command

    @staticmethod
    def get_smoc_run_command(
        input_map: str,
        input_model: str,
        resolution: float,
        modelid: str,
        mapid: str,
    ) -> List[str]:
        # pdb_num = 1  # this needs to be set for multiple model input
        smoc_command = ["TEMPy.smoc"]
        smoc_command += ["-m", input_map]
        smoc_command += ["-p", input_model]
        smoc_command += ["-r", str(resolution)]
        smoc_command += ["--smoc-window", str(1)]
        smoc_command += ["--output-format", "json"]
        smoc_command += [
            "--output-prefix",
            os.path.splitext(
                TEMPyMapFit.get_smoc_outjson(modelid=modelid, mapid=mapid)
            )[0],
        ]
        return smoc_command

    @staticmethod
    def get_residue_coordinates_command(
        input_model: str,
        modelid: str,
    ) -> List[str]:
        residue_coordinates_command = [
            "python3",
            get_job_script("model_validation/get_residue_coordinates.py"),
        ]
        residue_coordinates_command += ["-p", input_model]
        residue_coordinates_command += ["-mid", modelid]
        return residue_coordinates_command

    @staticmethod
    def get_smoc_results_command(
        modelid: str,
        mapid: str,
    ) -> List[str]:
        smoc_results_command = [
            "python3",
            get_job_script("model_validation/get_smoc_results.py"),
            "-smoc",
            TEMPyMapFit.get_smoc_outjson(modelid=modelid, mapid=mapid),
            "-coord",
            modelid + "_residue_coordinates.json",
            "-id",
            modelid + "_" + mapid,
        ]
        return smoc_results_command

    def create_results_display(self) -> List[ResultsDisplayObject]:
        # TODO: long files names to be trimmed ?
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        input_map = self.joboptions["input_map"].get_string()
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        display_objects: List[ResultsDisplayObject] = []
        run_global_only = self.joboptions["run_global_only"].get_boolean()
        run_smoc_only = self.joboptions["run_smoc_only"].get_boolean()
        if not run_smoc_only:
            score_table = self.create_map_fit_summary_table(
                input_model_list,
                mapid=mapid,
                unique_model_ids=unique_model_ids,
                output_dir=self.output_dir,
            )
            if score_table:  # in some cases the required scores are not calculated
                display_objects.append(score_table)
        if not run_global_only:
            smoc_plots = self.get_smoc_plots(
                input_model_list=input_model_list,
                mapid=mapid,
                unique_model_ids=unique_model_ids,
                output_dir=self.output_dir,
            )
            display_objects.extend(smoc_plots)
        return display_objects

    def create_map_fit_summary_table(
        self,
        input_model_list: List,
        mapid: str,
        unique_model_ids: Dict[str, str] = {},
        output_dir: str = "",
    ) -> Optional[ResultsDisplayObject]:
        fit_summary = None
        labels = ["Metric"]
        dict_mapfit = OrderedDict()
        associated_data = []
        model_n = 0
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            glob_scores_data = self.get_scores_outcsv(modelid=modelid, mapid=mapid)
            if output_dir:
                scores_path = os.path.join(output_dir, glob_scores_data)
            else:
                scores_path = glob_scores_data
            for metric in self.tempyscore_names:
                with open(scores_path, "r") as c:
                    for line in c:
                        lsplit = line.split(",")
                        if lsplit[0].strip() == metric:
                            metric_name = self.tempyscore_names[metric]
                            if model_n == 0:
                                dict_mapfit[metric_name] = ["-"] * len(input_model_list)
                            # ensure scores added at the correct index
                            dict_mapfit[metric_name][model_n] = lsplit[1].strip()
                            break
            labels.append(modelid)
            associated_data.append(glob_scores_data)
            model_n += 1
        list_mapfit = []
        for m in dict_mapfit:
            list_mapfit.append([m])
            list_mapfit[-1].extend(dict_mapfit[m])
        if list_mapfit:
            fit_summary = create_results_display_object(
                "table",
                title="Global map-fit scores",
                headers=labels,
                table_data=list_mapfit,
                associated_data=associated_data,
            )
        return fit_summary

    @staticmethod
    def get_chain_scores_raw(
        input_model_list: List,
        mapid: str,
        unique_model_ids: Dict[str, str] = {},
        output_dir: str = "",
    ) -> Tuple[Dict[str, List[str]], List[str]]:
        smoc_score_jsons = []
        dict_chain_scores: Dict[str, List] = {}
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            smoc_json = TEMPyMapFit.get_smoc_outjson(modelid, mapid)
            smoc_score_jsons.append(smoc_json)
            if output_dir:
                smoc_json_path = os.path.join(output_dir, smoc_json)
            else:
                smoc_json_path = smoc_json
            with open(smoc_json_path, "r") as j:
                smoc_scores = json.load(j)
            for chain in smoc_scores["chains"]:
                list_resnum = []
                list_scores = []
                for resnum in smoc_scores["chains"][chain]:
                    try:
                        list_resnum.append(int(resnum))
                    except TypeError:
                        continue
                    list_scores.append(float(smoc_scores["chains"][chain][resnum]))
                try:
                    dict_chain_scores[chain].append(
                        [modelid, {"x": list_resnum, "y": list_scores}]
                    )
                except KeyError:
                    dict_chain_scores[chain] = [
                        [modelid, {"x": list_resnum, "y": list_scores}]
                    ]
        return dict_chain_scores, smoc_score_jsons

    @staticmethod
    def get_chain_scores_processed(
        input_model_list: List,
        mapid: str,
        unique_model_ids: Dict[str, str] = {},
        output_dir: str = "",
    ) -> Tuple[Dict[str, List[str]], Dict[str, List[str]], List[str]]:
        smoc_score_jsons = []
        dict_chain_scores: Dict[str, List] = {}
        dict_chain_zscores: Dict[str, List] = {}
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            smoc_res_json = modelid + "_" + mapid + "_residue_smoc.json"
            if output_dir:
                smoc_json_path = os.path.join(output_dir, smoc_res_json)
            else:
                smoc_json_path = smoc_res_json
            smoc_score_jsons.append(smoc_json_path)
            with open(smoc_json_path, "r") as smocj:
                smoc_data = json.load(smocj)
                for m in smoc_data:
                    for c in smoc_data[m]:
                        list_resnum = []
                        list_scores = []
                        list_zscores = []
                        list_zresnum = []
                        for r in smoc_data[m][c]:
                            try:
                                score = round(float(smoc_data[m][c][r][0]), 3)
                                list_resnum.append(int(r))
                                list_scores.append(score)
                            except (ValueError, TypeError):
                                continue
                            try:
                                zscore = round(float(smoc_data[m][c][r][1]), 3)
                                if zscore < -2.0:
                                    list_zresnum.append(int(r))
                                    # holds raw score for plot
                                    list_zscores.append(score)
                            except (ValueError, TypeError):
                                continue
                        try:
                            dict_chain_scores[c].append(
                                [modelid, {"x": list_resnum, "y": list_scores}]
                            )
                        except KeyError:
                            dict_chain_scores[c] = [
                                [modelid, {"x": list_resnum, "y": list_scores}]
                            ]
                        if list_zscores:
                            try:
                                dict_chain_zscores[c].append(
                                    [modelid, {"x": list_zresnum, "y": list_zscores}]
                                )
                            except KeyError:
                                dict_chain_zscores[c] = [
                                    [modelid, {"x": list_zresnum, "y": list_zscores}]
                                ]
        return dict_chain_scores, dict_chain_zscores, smoc_score_jsons

    @staticmethod
    def get_smoc_plots(
        input_model_list: List,
        mapid: str,
        unique_model_ids: Dict[str, str] = {},
        output_dir: str = "",
        output_processed: bool = True,
    ) -> List[ResultsDisplayObject]:
        display_objects = []
        smoc_score_jsons: List[str] = []
        dict_chain_zscores: Dict[str, List[str]] = {}
        if not output_processed:
            dict_chain_scores, smoc_score_jsons = TEMPyMapFit.get_chain_scores_raw(
                input_model_list,
                mapid=mapid,
                unique_model_ids=unique_model_ids,
                output_dir=output_dir,
            )
        else:
            dict_chain_scores, dict_chain_zscores, smoc_score_jsons = (
                TEMPyMapFit.get_chain_scores_processed(
                    input_model_list,
                    mapid=mapid,
                    unique_model_ids=unique_model_ids,
                    output_dir=output_dir,
                )
            )
        for chain in dict_chain_scores:
            list_series_args = []
            data = []
            for name_data in dict_chain_scores[chain]:
                model_id = name_data[0]
                series_data = name_data[1]
                series_args = {
                    "name": model_id,
                    "mode": "lines",
                    "line": {"width": 2},
                }
                list_series_args.append(series_args)
                data.append(series_data)
            if output_processed:
                for name_data in dict_chain_zscores[chain]:
                    model_id = name_data[0]
                    series_data = name_data[1]
                    series_args = {
                        "name": model_id + "_Outliers",
                        "mode": "markers",
                        "line": {"width": 8},
                    }
                    list_series_args.append(series_args)
                    data.append(series_data)
            xaxes_args = {
                "gridcolor": "lightgrey",
                "title_text": "Residue number",
            }
            yaxes_args = {
                "title_text": "SMOC score",
                "gridcolor": "lightgrey",
            }
            # multi_series plot
            disp_obj = create_results_display_object(
                "plotlyobj",
                data=data,
                plot_type="Scatter",  # all series scatter type
                subplot=False,
                multi_series=True,
                series_args=list_series_args,
                layout_args={
                    "plot_bgcolor": "white",
                },
                xaxes_args=xaxes_args,
                yaxes_args=yaxes_args,
                title="SMOC_residue_fit_chain_" + chain,
                associated_data=smoc_score_jsons,
                flag="",
                start_collapsed=False,
            )
            display_objects.append(disp_obj)
        return display_objects
