#
#     Copyright (C) 2024 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import fnmatch
from typing import List, Sequence
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
)
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MLMODEL,
)
from pipeliner.data_structure import SELECT_DIR
from pipeliner.display_tools import mini_montage_from_starfile
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.starfile_handler import DataStarFile, JobStar


class CryodrgnFilterJob(PipelinerJob):
    """
    CryoDRGN filter job v1 for ccpem-pipeliner
    """

    PROCESS_NAME = "cryodrgn.filter"
    OUT_DIR = SELECT_DIR
    CATEGORY_LABEL = "Subset Selection"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "CryoDRGN subset selection (interactive)"
        self.jobinfo.version = "0.0.1"
        self.jobinfo.job_author = "Joel Greer"
        self.jobinfo.programs = [
            ExternalProgram(command="cryodrgn"),
            ExternalProgram(command="cryodrgn_utils"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Zhong ED", "Bepler T", "Berger B", "Davis J"],
                title=(
                    "CryoDRGN: reconstruction of heterogeneous cryo-EM structures"
                    " using neural networks"
                ),
                journal="Nature Methods",
                year="2021",
                volume="18",
                issue="2",
                pages="176-185",
                doi="https://doi.org/10.1038/s41592-020-01049-4",
            )
        ]
        self.jobinfo.short_desc = "CryoDRGN subset selection"
        self.jobinfo.long_desc = (
            "CryoDRGN interactive subset selection using a lasso tool. Note this job"
            " uses an X11 GUI window so will not work on a remote connection unless"
            " X-forwarding is enabled."
        )
        self.jobinfo.documentation = "https://ez-lab.gitbook.io/cryodrgn"

        # standard options ~(ignore kmeans clusters and prefiltered indices)
        self.joboptions["weights"] = InputNodeJobOption(
            label=("CryoDRGN trained weights pkl file"),
            pattern=files_exts("Pickle files", [".pkl"]),
            default_value="",
            help_text=(
                "CryoDRGN trained weights pkl file. This should normally be the final"
                " output file from a CryoDRGN heterogeneous reconstruction job. If you"
                " want to use a result from an intermediate epoch, first run cryoDRGN"
                " analyze on the epoch you want to use and then select the weights"
                " file of that epoch here (ie: 'weights.X.pkl' for the X'th epoch)."
            ),
            is_required=True,
            in_continue=False,
            node_type=NODE_MLMODEL,
            node_kwds=["cryoDRGN", "weights"],
        )

    def create_output_nodes(self) -> None:
        # selected particles as .star and inverse selection also
        # replacing with the subsequent to fix pytesting

        # define output node files
        selection_starfile = "selection.star"
        inverse_selection_starfile = "inverse_selection.star"

        self.add_output_node(
            str(selection_starfile),
            NODE_PARTICLEGROUPMETADATA,
            ["cryoDRGN"],
        )
        self.add_output_node(
            str(inverse_selection_starfile),
            NODE_PARTICLEGROUPMETADATA,
            ["cryoDRGN"],
        )
        return

    def get_commands(self) -> List[PipelinerCommand]:
        weights_file = Path(self.joboptions["weights"].get_string())

        # define output node files
        selection_starfile = Path(self.output_dir) / "selection.star"
        inverse_selection_starfile = Path(self.output_dir) / "inverse_selection.star"

        # get particles metadata file
        # match CryoDRGN str to get dir
        # grab job.star file
        path_dirs = str(weights_file).split("/")
        job_idx = fnmatch.filter(path_dirs, "job???*")
        if len(job_idx) > 1:
            raise ValueError("More than one job directory for weights file was found!")
        elif len(job_idx) == 0:
            raise ValueError("No job directory for weights file was found!")
        path_job_idx = path_dirs.index(job_idx[0])
        parent_dir = "/".join(path_dirs[: path_job_idx + 1])
        job_star = Path(parent_dir) / "job.star"
        metadata = JobStar(str(job_star)).all_options_as_dict()["fn_img"]

        # determine the datadir from metadata file
        # read the starfile column 6 _rlnImageName
        # split on @ and take the non-basename part
        # to provide to --datadir
        mrc_file_paths = []
        mrc_file_paths = DataStarFile(metadata).column_as_list(
            blockname="particles",
            colname="_rlnImageName",
        )
        if len(mrc_file_paths) == 0:
            raise ValueError(
                "Provided STAR file likely does not have a _rlnImageName column"
                " in the particles data block. Please check and use a STAR file"
                " with this field present."
            )

        mrc_file_paths = [
            str(Path(mrc_fp.split("@")[1]).parent) for mrc_fp in mrc_file_paths
        ]
        # Only allowing starfiles from a single datadir
        # which is necessary to allow pipeliner+cryodrgn
        # to work together to point at the extracted particles
        uniq_dirs = set(mrc_file_paths)
        if not len(uniq_dirs) == 1:
            raise ValueError(
                "There are extracted particles from multiple directories present"
                " in the STAR file supplied. Please re-extract particles and"
                " supply the new particles STAR file instead."
            )
        # we now have --datadir arg
        datadir = uniq_dirs.pop()

        # get epoch of weights
        epoch = "-1"
        # if an intermediate weights file is used, use this epoch instead of -1
        # example filename: <train_dir>/weights.3.pkl
        if len(path_dirs[-1].split(".")) == 3:
            epoch = path_dirs[-1].split(".")[1]

        train_dir = "/".join(str(weights_file).split("/")[:-1])
        # run filtering
        filtering_cmd = [
            "cryodrgn",
            "filter",
            train_dir,  # train_<> dir
            "--epoch",
            epoch,
            "--force",
            "--sel-dir",
            self.output_dir,
        ]

        # move filter pkl outputs to output directory and remove from . dir
        select = "indices.pkl"
        select_dest = Path(self.output_dir) / select
        select_inv = "indices_inverse.pkl"
        select_inv_dest = Path(self.output_dir) / select_inv

        # create starfiles using write_star cryodrgn utility
        starfile_cmd = [
            "cryodrgn_utils",
            "write_star",
            metadata,  # starfile
            "--ind",
            str(select_dest),
            "--datadir",
            datadir,
            "-o",
            str(selection_starfile),
        ]

        starfile_inv_cmd = [
            "cryodrgn_utils",
            "write_star",
            metadata,  # starfile
            "--ind",
            str(select_inv_dest),
            "--datadir",
            datadir,
            "-o",
            str(inverse_selection_starfile),
        ]
        final_commands_list = [filtering_cmd, starfile_cmd, starfile_inv_cmd]

        return [PipelinerCommand(x) for x in final_commands_list]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        out_dobs: List[ResultsDisplayObject] = []

        out_parts = [x.name for x in self.output_nodes]
        for partstar in out_parts:
            out_dobs.append(
                mini_montage_from_starfile(
                    starfile=partstar,
                    block="particles",
                    column="_rlnImageName",
                    outputdir=self.output_dir,
                    nimg=30,
                )
            )

        return out_dobs
