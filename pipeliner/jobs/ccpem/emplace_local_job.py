#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from shutil import which
from pathlib import Path
from glob import glob
from typing import List, Dict, Any
import logging

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    JobOptionCondition,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject

logger = logging.getLogger(__name__)


class EMPlaceLocal(PipelinerJob):
    """
    Phaser EM Placement Job
    Fits model into cryoEM map.
    """

    PROCESS_NAME = "emplace_local.atomic_model_fit"
    OUT_DIR = "EMPlacement"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "emplace_local"
        self.jobinfo.version = "0.2"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = "Locally fit model in map with em_placement"
        self.jobinfo.long_desc = (
            "EMPlace_local  Dock a model in a cryo EM map with a likelihood target "
            "into a user specified local region.  Region defined around "
            "a set of x,y,z coordinates (Å).  Requires CCP4 9.0.002 or greater"
        )
        self.jobinfo.documentation = (
            "https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phasertng"
        )
        self.jobinfo.programs = [
            ExternalProgram(self.get_emplace_local_command()),
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Millan C",
                    "McCoy AJ",
                    "Terwilliger TC",
                    "Read RJ",
                ],
                title="Likelihood-based docking of models into cryo-EM maps",
                journal="Acta Cryst D",
                year="2023",
                volume="79",
                issue="D",
                pages="281-289",
                doi="10.1107/S2059798323001602",
            ),
            Ref(
                authors=[
                    "McCoy AJ",
                    "Grosse-Kunstleve RW",
                    "Adams PD",
                    "Winn MD",
                    "Storoni LC",
                    "Read RJ",
                ],
                title="Phaser Crystallographic Software",
                journal="J Appl Crystallography",
                year="2007",
                volume="40",
                issue="1",
                pages="658-674",
                doi="10.1107/S0021889807021206",
            ),
        ]

        self.joboptions["model"] = InputNodeJobOption(
            label="Model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="Atomic model to dock",
            is_required=True,
        )

        self.joboptions["input_map1"] = InputNodeJobOption(
            label="Input map 1 (half map 1 or full map)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text=(
                "The first map to dock the model with.  Either a single "
                "full map or the first half map"
            ),
            is_required=True,
        )
        self.joboptions["use_halfmaps"] = BooleanJobOption(
            label="Use half maps",
            default_value=True,
            help_text=(
                "Use half maps for docking, alternative is to use single full map"
            ),
        )
        self.joboptions["input_map2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The second half map to dock the model with",
            required_if=JobOptionCondition([("use_halfmaps", "=", True)]),
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution of full map (Å)",
            is_required=True,
        )

        self.joboptions["centre_x"] = FloatJobOption(
            label="X search centre",
            default_value=0,
            step_value=1,
            help_text="Centre of local search sphere (X coordinate, Å)",
            is_required=True,
        )

        self.joboptions["centre_y"] = FloatJobOption(
            label="Y search centre",
            default_value=0,
            step_value=1,
            help_text="Centre of local search sphere (Y coordinate, Å)",
            is_required=True,
        )

        self.joboptions["centre_z"] = FloatJobOption(
            label="Z search centre",
            default_value=0,
            step_value=1,
            help_text="Centre of local search sphere (Z coordinate, Å)",
            is_required=True,
        )

        self.joboptions["starting_model_vrms"] = FloatJobOption(
            label="Starting model vrms",
            default_value=1.2,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text=(
                "1.0 should be used for an identical structure, typically 1.2 for "
                "an AlphaFold or similarly predicted structure or the expected "
                "rms for an experimentally derived homologue"
            ),
            is_required=True,
        )

        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        # this is all done post run
        pass

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        model_file = self.joboptions["model"].get_string()
        molecule_name = Path(model_file).stem
        input_map1 = self.joboptions["input_map1"].get_string()
        input_map2 = self.joboptions["input_map2"].get_string()
        resolution = self.joboptions["resolution"].get_number()
        starting_model_vrms = self.joboptions["starting_model_vrms"].get_number()

        phil_name_local = "emplace_local.phil"

        if self.joboptions["use_halfmaps"].get_boolean():
            full_map = "None"
            half_map_1 = os.path.relpath(input_map1, self.working_dir)
            half_map_2 = os.path.relpath(input_map2, self.working_dir)
        else:
            full_map = os.path.relpath(input_map1, self.working_dir)
            half_map_1 = "None"
            half_map_2 = "None"

        centre_x = self.joboptions["centre_x"].get_number()
        centre_y = self.joboptions["centre_y"].get_number()
        centre_z = self.joboptions["centre_z"].get_number()

        config_command = [
            "python3",
            get_job_script("em_placement/emplace_local_config.py"),
            full_map,
            half_map_1,
            half_map_2,
            str(resolution),
            molecule_name,
            os.path.relpath(model_file, self.working_dir),
            str(starting_model_vrms),
            str(centre_x),
            str(centre_y),
            str(centre_z),
            phil_name_local,
        ]

        emplace_local_command = [self.get_emplace_local_command(), phil_name_local]
        commands = [
            PipelinerCommand(x) for x in [config_command, emplace_local_command]
        ]
        return commands

    # Work around as phaser_voyager commands are in $CCP4/libexec not
    # $CCP4/bin and therefore not on path
    @staticmethod
    def get_emplace_local_command() -> str:
        ccp4_python = which("ccp4-python")
        if ccp4_python is not None:
            return ccp4_python.replace(
                "/bin/ccp4-python",
                "/libexec/phaser_voyager.emplace_local",
            )
        else:
            return "phaser_voyager.em_placement not found"

    def create_post_run_output_nodes(self) -> None:
        search_pdbs = os.path.join(self.output_dir, "top_model*.pdb")
        pdbs = glob(search_pdbs)
        for pdb in sorted(pdbs):
            self.output_nodes.append(
                create_node(pdb, NODE_ATOMCOORDS, ["em_placement"])
            )
        search_maps = os.path.join(self.output_dir, "*.map")
        maps = glob(search_maps)
        for map in sorted(maps):
            self.output_nodes.append(
                create_node(map, NODE_DENSITYMAP, ["em_placement"])
            )

    def gather_metadata(self) -> Dict[str, Any]:
        """
        em_placement generates 'output_table_docking.csv' which contains summary
        metadata.
        """
        dict_scores = self.get_output_scores()
        return dict_scores

    def get_output_scores(self) -> Dict[str, Any]:
        dict_scores: Dict[str, Any] = {}
        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            solutions_flag = False
            model_flag = False
            for line in of:
                try:
                    if "Number of solutions found:" in line:
                        solutions_flag = True
                        model_flag = False
                    elif solutions_flag and "Model # " in line:
                        model_name = line.strip().split()[-1]
                        model_flag = True
                        dict_scores[model_name] = {}
                    elif solutions_flag and "corresponding map" in line:
                        map_name = line.strip().split()[-1]
                        dict_scores[model_name][map_name] = {}
                    elif model_flag and "mapLLG" in line:
                        dict_scores[model_name][map_name]["mapLLG"] = round(
                            float(line.strip().split()[-1]), 3
                        )
                    elif model_flag and "mapCC" in line:
                        dict_scores[model_name][map_name]["mapCC"] = round(
                            float(line.strip().split()[-1]), 3
                        )
                except (KeyError, ValueError) as e:
                    logger.exception(e)

        return dict_scores

    def create_results_display(self) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        maps = [os.path.join(self.output_dir, "top_model_1.map")]
        models = [os.path.join(self.output_dir, "top_model_1.pdb")]
        if Path(maps[0]).is_file() and Path(models[0]).is_file():
            display_objects.append(
                make_map_model_thumb_and_display(
                    maps=maps,
                    maps_opacity=[0.5],
                    models=models,
                    title="emplace_local best scoring model and map",
                    outputdir=self.output_dir,
                )
            )
        dict_scores = self.get_output_scores()
        if dict_scores:
            scores_data = []
            for s in dict_scores["top_model_1.pdb"]["top_model_1.map"]:
                scores_data.append(
                    [s, dict_scores["top_model_1.pdb"]["top_model_1.map"][s]]
                )
            display_objects.append(
                create_results_display_object(
                    "table",
                    title="Map-fit scores (top_model_1)",
                    headers=["Name", "score"],
                    table_data=scores_data,
                    associated_data=[os.path.join(self.output_dir, "run.out")],
                )
            )
        return display_objects
