import os
import json
import shutil
from typing import List, Optional, Sequence, Union, Dict, Any
from pathlib import Path
from collections import OrderedDict
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    BooleanJobOption,
    MultiInputNodeJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_LOGFILE,
    NODE_EVALUATIONMETRIC,
)


class ValidateGeometry(PipelinerJob):
    PROCESS_NAME = "molprobity.atomic_model_validation.geometry"
    OUT_DIR = "Molprobity"
    job_references = [
        Ref(
            authors=[
                "Williams CJ",
                "Headd JJ",
                "Moriarty NW",
                "Prisant MG",
                "Videau LL",
                "Deis LN",
                "Verma V",
                "Keedy DA",
                "Hintze BJ",
                "Chen VB",
                "Jain S",
                "Lewis SM",
                "Arendall WB 3rd",
                "Snoeyink J",
                "Adams PD",
                "Lovell SC",
                "Richardson JS",
                "Richardson DC",
            ],
            title=(
                "MolProbity: More and better reference data for improved"
                " all-atom structure validation ."
            ),
            journal="Protein Sci",
            year="2018",
            volume="27",
            issue="1",
            pages="293-315",
            doi="10.1002/pro.3330",
        )
    ]

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Molprobity"
        self.jobinfo.short_desc = "Atomic model geometry assessment"
        self.jobinfo.long_desc = (
            "Assess atomic model stereo-chemistry using molprobity.\n"
            "Reports outliers for:\n"
            "atomic clashes\n"
            "Backbone phi/psi Ramachandran angles and Z-scores\n"
            "Side-chain rotamers\n"
            "C-β deviations\n"
            "cis-peptides.\n"
            "Reports both global and local statistics from above assessments. "
            "The job provides options to run specific scores as well.\n"
            " N.B. requires CCP4."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(command="molprobity.molprobity"),
        ]
        self.jobinfo.references = ValidateGeometry.job_references
        self.jobinfo.documentation = "https://github.com/rlabduke/MolProbity"

        self.joboptions["input_models"] = MultiInputNodeJobOption(
            label="Input models",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="Add multiple models to compare the scores.",
            is_required=True,
        )
        # Specific modes
        self.joboptions["run_global"] = BooleanJobOption(
            label="Comprehensive assessment?",
            default_value=False if not shutil.which("molprobity.molprobity") else True,
            help_text=(
                "Returns global molprobity statistics and local outliers."
                " Slower than other modes."
            ),
            jobop_group="Molprobity run options",
        )
        self.joboptions["run_clashscore"] = BooleanJobOption(
            label="Clashscore?",
            default_value=False,
            help_text="Run Molprobity clashscore. Useful for a quick clash evaluation.",
            deactivate_if=JobOptionCondition([("run_global", "=", True)]),
            jobop_group="Molprobity run options",
        )
        self.joboptions["run_rama"] = BooleanJobOption(
            label="Ramalyze?",
            default_value=False,
            help_text=(
                "Run Molprobity ramalyze for backbone Ramachandran angle assessment."
            ),
            deactivate_if=JobOptionCondition([("run_global", "=", True)]),
            jobop_group="Molprobity run options",
        )
        self.joboptions["run_rota"] = BooleanJobOption(
            label="Rotalyze?",
            default_value=False,
            help_text="Run Molprobity rotalyze for side-chain rotamer assessment.",
            deactivate_if=JobOptionCondition([("run_global", "=", True)]),
            jobop_group="Molprobity run options",
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # maximum input models
        input_model_list = self.joboptions["input_models"].get_list()
        if len(input_model_list) > 3:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_models"]],
                    message="A maximum of 3 models can be assessed",
                )
            )
        if self.joboptions["run_global"].get_boolean() and not shutil.which(
            "molprobity.molprobity"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_global"]],
                    message=(
                        "'molprobity.molprobity' is not available in the system path, "
                        "check that CCP4 is installed."
                    ),
                )
            )
        if self.joboptions["run_clashscore"].get_boolean() and not shutil.which(
            "molprobity.clashscore"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_clashscore"]],
                    message=(
                        "'molprobity.clashscore' is not available in the system path, "
                        "check that CCP4 is installed."
                    ),
                )
            )
        if self.joboptions["run_rama"].get_boolean() and not shutil.which(
            "molprobity.ramalyze"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_rama"]],
                    message=(
                        "'molprobity.ramalyze' is not available in the system path, "
                        "check that CCP4 is installed."
                    ),
                )
            )
        if self.joboptions["run_rota"].get_boolean() and not shutil.which(
            "molprobity.rotalyze"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_rota"]],
                    message=(
                        "'molprobity.rotalyze' is not available in the system path, "
                        "check that CCP4 is installed."
                    ),
                )
            )
        if not (
            self.joboptions["run_global"].get_boolean()
            or self.joboptions["run_clashscore"].get_boolean()
            or self.joboptions["run_rama"].get_boolean()
            or self.joboptions["run_rota"].get_boolean()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_global"]],
                    message="None of the Molprobity run modes chosen.",
                )
            )
        return errors

    @staticmethod
    def get_cif_ext(input_model: str) -> Optional[str]:
        ext = os.path.splitext(input_model)[1]
        if ext.lower() in (".cif", ".mmcif"):
            return ext
        else:
            return None

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        run_global = self.joboptions["run_global"].get_boolean()
        run_clashscore = self.joboptions["run_clashscore"].get_boolean()
        run_rama = self.joboptions["run_rama"].get_boolean()
        run_rota = self.joboptions["run_rota"].get_boolean()
        # loop through each model
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            # Now make the nodes
            cif_ext = self.get_cif_ext(input_model)
            if cif_ext:
                self.add_output_node(
                    Path(input_model).name.replace(cif_ext, ".pdb"),
                    NODE_ATOMCOORDS,
                    ["gemmi", "pdbconvert"],
                )
            if run_global:
                molprobity_out = self.get_molprobity_logfile_name(modelid)
                self.add_output_node(
                    molprobity_out, NODE_LOGFILE, ["molprobity", "output"]
                )
            else:
                if run_clashscore:
                    clashscore_out = self.get_clashscore_jsonfile_name(modelid)
                    self.add_output_node(
                        clashscore_out, NODE_EVALUATIONMETRIC, ["clashscore", "output"]
                    )
                if run_rama:
                    ramalyze_out = self.get_ramalyze_jsonfile_name(modelid)
                    self.add_output_node(
                        ramalyze_out, NODE_EVALUATIONMETRIC, ["ramalyze", "output"]
                    )
                if run_rota:
                    rotalyze_out = self.get_rotalyze_jsonfile_name(modelid)
                    self.add_output_node(
                        rotalyze_out, NODE_EVALUATIONMETRIC, ["rotalyze", "output"]
                    )

    @staticmethod
    def get_clashscore_jsonfile_name(modelid) -> str:
        return modelid + "_clashscore.json"

    @staticmethod
    def get_ramalyze_jsonfile_name(modelid) -> str:
        return modelid + "_ramalyze.json"

    @staticmethod
    def get_rotalyze_jsonfile_name(modelid) -> str:
        return modelid + "_rotalyze.json"

    @staticmethod
    def get_molprobity_logfile_name(modelid) -> str:
        return modelid + "_molprobity.out"

    @staticmethod
    def get_molprobity_cootscript_name(modelid) -> str:
        return modelid + "_molprobity_coot.py"

    @staticmethod
    def get_reduce_model_name(input_model, modelid) -> str:
        if (
            os.path.splitext(input_model)[-1].lower() == ".cif"
            or os.path.splitext(input_model)[-1].lower() == ".mmcif"
        ):
            reduce_out = "reduce_out.cif"
        else:
            reduce_out = "reduce_out.pdb"
        return modelid + "_" + reduce_out

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands: list = []
        # Get parameters
        input_model_list = self.joboptions["input_models"].get_list()
        run_global = self.joboptions["run_global"].get_boolean()
        run_clashscore = self.joboptions["run_clashscore"].get_boolean()
        run_rama = self.joboptions["run_rama"].get_boolean()
        run_rota = self.joboptions["run_rota"].get_boolean()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            input_model = os.path.relpath(input_model, self.working_dir)
            # Convert cif inputs to pdb as Molprobity does not accept cif format
            input_model_pdb = input_model
            cif_ext = self.get_cif_ext(input_model)
            if cif_ext:
                input_model_pdb = Path(input_model).name.replace(cif_ext, ".pdb")
                commands.append(
                    self.get_cifconvert_command(input_model, input_model_pdb)
                )
            if run_global:
                commands += self.get_molprobity_run_commands(input_model_pdb, modelid)
                commands.append(
                    self.get_molprobity_results_command(modelid, run_global=True)
                )
            else:
                if run_clashscore:
                    commands.append(
                        self.get_clashscore_run_command(
                            input_model_pdb,
                            modelid,
                        )
                    )
                if run_rama:
                    commands.append(self.get_rama_run_command(input_model_pdb, modelid))
                if run_rota:
                    commands.append(self.get_rota_run_command(input_model_pdb, modelid))
                commands.append(
                    self.get_molprobity_results_command(
                        modelid,
                        run_global=False,
                        run_clashscore=run_clashscore,
                        run_rama=run_rama,
                        run_rota=run_rota,
                    )
                )
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_cifconvert_command(
        input_model: str, input_model_corrected: str
    ) -> List[str]:
        gemmi_convert_model: List[str] = [
            "gemmi",
            "convert",
            "--shorten",
            input_model,
            input_model_corrected,
        ]
        return gemmi_convert_model

    @staticmethod
    def get_molprobity_run_commands(input_model: str, modelid: str) -> List[List[str]]:
        commands: List[List[str]] = []
        # reduce
        reduce_out = ValidateGeometry.get_reduce_model_name(
            input_model=input_model, modelid=modelid
        )
        commands += ValidateGeometry.get_reduce_commands(
            input_model=input_model, reduce_out=reduce_out
        )
        # run molprobity
        molprobity_command: List[str] = [
            "molprobity.molprobity",
            reduce_out,
        ]
        molprobity_command += ["output.percentiles=True"]
        commands += [molprobity_command]
        molprobity_out = ValidateGeometry.get_molprobity_logfile_name(modelid)
        molprobity_cootscript = ValidateGeometry.get_molprobity_cootscript_name(modelid)
        commands += [["mv", "molprobity.out", molprobity_out]]
        commands += [["mv", "molprobity_coot.py", molprobity_cootscript]]
        return commands

    @staticmethod
    def get_reduce_commands(input_model: str, reduce_out: str) -> List[List[str]]:
        commands = []
        # use input model if reduce run fails
        copy_reduce_command: List[str] = [
            "cp",
            input_model,
            reduce_out,
        ]
        commands += [copy_reduce_command]
        reduce_wrap_command = [
            "python3",
            get_job_script("model_validation/run_molprobity_reduce.py"),
            "-i",
            input_model,
            "-o",
            reduce_out,
        ]
        commands += [reduce_wrap_command]
        return commands

    @staticmethod
    def get_clashscore_run_command(
        input_model: str,
        modelid: str,
    ) -> List[str]:
        # run clashscore
        clashscore_out = ValidateGeometry.get_clashscore_jsonfile_name(modelid=modelid)
        clashscore_command = [
            "molprobity.clashscore",
            input_model,
            "--json",
            "--json-filename",
            clashscore_out,
        ]
        return clashscore_command

    @staticmethod
    def get_rama_run_command(
        input_model: str,
        modelid: str,
    ) -> List[str]:
        # run ramalyze
        ramalyze_out = ValidateGeometry.get_ramalyze_jsonfile_name(modelid)
        ramalyze_command = [
            "molprobity.ramalyze",
            input_model,
            "--json",
            "--json-filename",
            ramalyze_out,
        ]
        return ramalyze_command

    @staticmethod
    def get_rota_run_command(
        input_model: str,
        modelid: str,
    ) -> List[str]:
        # run rotalyze
        rotalyze_out = ValidateGeometry.get_rotalyze_jsonfile_name(modelid)
        rotalyze_command = [
            "molprobity.rotalyze",
            input_model,
            "--json",
            "--json-filename",
            rotalyze_out,
        ]
        return rotalyze_command

    @staticmethod
    def get_molprobity_results_command(
        modelid: str,
        run_global: bool = True,
        run_clashscore: bool = False,
        run_rama: bool = False,
        run_rota: bool = False,
    ) -> List[str]:
        # gather results
        molprobity_results_command: List[str] = [
            "python3",
            get_job_script("model_validation/get_molprobity_results.py"),
            "-id",
            modelid,
        ]
        if run_global:
            molprobity_out = ValidateGeometry.get_molprobity_logfile_name(modelid)
            molprobity_cootscript = ValidateGeometry.get_molprobity_cootscript_name(
                modelid
            )
            molprobity_results_command += [
                "-molp",
                molprobity_out,
                "-cootscript",
                molprobity_cootscript,
            ]
        if run_clashscore:
            clashscore_out = ValidateGeometry.get_clashscore_jsonfile_name(modelid)
            molprobity_results_command += ["-clash", clashscore_out]
        if run_rama:
            ramalyze_out = ValidateGeometry.get_ramalyze_jsonfile_name(modelid)
            molprobity_results_command += [
                "-rama",
                ramalyze_out,
            ]
        if run_rota:
            rotalyze_out = ValidateGeometry.get_rotalyze_jsonfile_name(modelid)
            molprobity_results_command += [
                "-rota",
                rotalyze_out,
            ]
        return molprobity_results_command

    @staticmethod
    def get_consolidate_results_command() -> List[Union[int, float, str]]:
        results_command: List[Union[int, float, str]] = [
            "python3",
            get_job_script("model_validation/validation_results.py"),
        ]
        return results_command

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        # TODO: long files names to be trimmed ?
        input_model_list = self.joboptions["input_models"].get_list()
        unique_model_ids = self.joboptions["input_models"].get_basename_mapping()
        # make global geometry stats table
        display_objects.append(
            self.create_molprobity_summary_table(
                input_model_list, self.output_dir, unique_model_ids=unique_model_ids
            )
        )
        return display_objects

    @staticmethod
    def create_molprobity_summary_table(
        input_model_list: List[str],
        output_dir: str,
        unique_model_ids: Dict[str, str] = {},
        run_tortoize: bool = False,
    ) -> ResultsDisplayObject:
        list_mp_jsons = []
        list_modelids = []
        for input_model in input_model_list:
            if unique_model_ids:
                modelid = os.path.splitext(unique_model_ids[input_model])[0]
            else:
                modelid = os.path.splitext(os.path.basename(input_model))[0]
            mp_json = os.path.join(output_dir, modelid + "_molprobity_summary.json")
            list_mp_jsons.append(mp_json)
            list_modelids.append(modelid)
        labels = ["Metric"] + list_modelids
        labels += ["Expected/Percentile"]
        dict_mpdata = ValidateGeometry.merge_molprobity_summaries(
            list_mp_jsons, run_tortoize=run_tortoize
        )
        molprob_data = ValidateGeometry.set_molprobity_summary_data(dict_mpdata)
        mp_summary = create_results_display_object(
            "table",
            title="Global geometry scores",
            headers=labels,
            table_data=molprob_data,
            associated_data=list_mp_jsons,
        )
        return mp_summary

    # merges molprobity results of initial and refined models for comparison
    @staticmethod
    def merge_molprobity_summaries(
        list_mp_jsons: List[str],
        add_percentile: bool = True,
        run_tortoize: bool = False,
    ) -> Dict[str, Any]:
        dict_all_molp_scores: dict = OrderedDict()
        with open(list_mp_jsons[0], "r") as mpj:
            mp_data = json.load(mpj)
        list_metrics = list(mp_data.keys())
        for metric in list_metrics:
            # skip RamaZ if tortoize is run
            if "RamachandranZ" in metric and run_tortoize:
                continue
            for m in range(len(list_mp_jsons)):
                mp_json = list_mp_jsons[m]
                with open(mp_json, "r") as mpj:
                    mp_data = json.load(mpj)
                try:
                    score = mp_data[metric][0]
                    pct = mp_data[metric][1]
                except (KeyError, IndexError):
                    continue
                try:
                    dict_all_molp_scores[metric].extend([score])
                except KeyError:
                    dict_all_molp_scores[metric] = [score]
                if add_percentile and m == len(list_mp_jsons) - 1:
                    dict_all_molp_scores[metric].extend([pct])
        return dict_all_molp_scores

    @staticmethod
    def set_molprobity_summary_data(dict_mpdata: dict) -> List[Any]:
        molprob_data: List[Any] = []
        for m in dict_mpdata:
            mptable_row = [m]
            mptable_row.extend(dict_mpdata[m])
            molprob_data.append(mptable_row)
        return molprob_data

    # TODO: this function is currently not being used as it requires major changes to
    #  validation_results.py script to process results of multiple models
    def create_outlier_cluster_table(self) -> ResultsDisplayObject:
        # outlier clusters table
        clusters_file = os.path.join(self.output_dir, "outlier_clusters.csv")
        clusters = []
        with open(clusters_file, "r") as cf:
            for line in cf:
                if line[0] == "#":
                    continue
                line_split = line.split(",")
                clusters.append(line_split)
        clusters_table = create_results_display_object(
            "table",
            title="Outlier clusters",
            headers=[
                "Cluster",
                "Chain_Residue",
                "Outlier Type(s)",
            ],
            table_data=clusters,
            associated_data=[clusters_file],
        )
        return clusters_table
