#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Dict, Any, Sequence, Union
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
    JobOptionCondition,
    BooleanJobOption,
    MultiInputNodeJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
    NODE_PROCESSDATA,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_mrcs_central_slices_montage,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class ServalcatTrimJob(PipelinerJob):
    PROCESS_NAME = "servalcat.map_utilities.trim"
    OUT_DIR = "ServalcatTrim"

    def __init__(self) -> None:
        super(self.__class__, self).__init__()
        self.vers_com = ["servalcat", "--version"]
        self.jobinfo.display_name = "Servalcat Trim"
        self.jobinfo.short_desc = "Trim maps and shift models into a small new box"
        self.jobinfo.long_desc = (
            "Servalcat Trim trims maps to a smaller box using a mask or atomic model as"
            " a guide. Atomic models are also shifted to the new box so that they"
            " align with the trimmed maps. The smaller box size makes the map files"
            " smaller and speeds up downstream modelling operations."
            "\nThe trim and shift parameters are saved and can be used again as input"
            " to a new trimming job, allowing additional maps and models to be trimmed"
            " later."
            "\nAtomic models can be shifted back to align with the original untrimmed"
            " maps using the 'servalcat shiftback' command."
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="servalcat", vers_com=["servalcat", "--version"], vers_lines=[0]
            ),
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza & Colin Palmer"
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://github.com/keitaroyam/servalcat"

        self.joboptions["input_maps"] = MultiInputNodeJobOption(
            label="Input Map(s)",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("Density map", [".mrc"]),
            default_value="",
            help_text="Maps to trim",
        )
        self.joboptions["mask"] = InputNodeJobOption(
            label="Mask",
            node_type=NODE_MASK3D,
            pattern=files_exts("3D mask", [".mrc"]),
            default_value="",
            help_text=(
                "Trim the maps to this mask (unless a previous set of shift parameters"
                " is given). If no mask or previous shift parameters are provided, at"
                " least one atomic model must be given instead."
            ),
            required_if=JobOptionCondition(
                [("input_models", "=", ""), ("shifts_json", "=", "")], operation="ALL"
            ),
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
        )
        self.joboptions["input_models"] = MultiInputNodeJobOption(
            label="Atomic models",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".pdb", ".cif"]),
            default_value="",
            help_text=(
                "Atomic models to be shifted along with the maps. If no mask is"
                " provided, the first atomic model will be used to determine the extent"
                " of the trimming operation."
            ),
            required_if=JobOptionCondition(
                [("mask", "=", ""), ("shifts_json", "=", "")], operation="ALL"
            ),
        )
        self.joboptions["padding"] = FloatJobOption(
            label="Padding (Å)",
            default_value=10.0,
            hard_min=0,
            help_text="Add this much padding around the the mask/model, in angstroms",
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
            is_required=True,
        )

        # Advanced options
        self.joboptions["mask_threshold"] = FloatJobOption(
            label="Mask threshold",
            default_value=0.5,
            suggested_min=0.0,
            suggested_max=1.0,
            help_text="Mask value to determine the boundary for trimming",
            is_required=True,
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
            jobop_group="Advanced",
        )
        self.joboptions["noncubic"] = BooleanJobOption(
            label="Non-cubic trimming?",
            default_value=True,
            help_text=(
                "A non-cubic map allows for even smaller file sizes, but some software"
                " (e.g. some image processing and map filtering tools) does not work"
                ' properly with non-cubic maps. Usually, it\'s best to run any "fussy"'
                " tools on the full-sized original maps and then trim the results for"
                " downstream processing, but you can also turn off this option to force"
                " the trimmed maps to be cubic."
            ),
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
            jobop_group="Advanced",
        )
        self.joboptions["noncentered"] = BooleanJobOption(
            label="Non-centred trimming?",
            default_value=True,
            help_text=(
                "If selected, the trimmed maps might not have the same centre point as"
                " the original maps. It is recommended to disable this option if the"
                " map has symmetry."
            ),
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
            jobop_group="Advanced",
        )
        self.joboptions["do_shift"] = BooleanJobOption(
            label="Shift maps and models?",
            default_value=True,
            help_text=(
                "If this option is disabled, atomic models will not be shifted and the"
                " trimmed maps will have an origin offset applied to align them with"
                " the original maps. Some software does not handle the shifted origin"
                " properly, so it is usually better to shift the maps and models during"
                " trimming. Shifted models can be moved back to align with the original"
                " maps using the 'servalcat shiftback' command."
            ),
            deactivate_if=JobOptionCondition([("shifts_json", "!=", "")]),
            jobop_group="Advanced",
        )
        self.joboptions["shifts_json"] = InputNodeJobOption(
            label="Precalculated shifts",
            node_type=NODE_PROCESSDATA,
            pattern=files_exts("Shifts JSON file", [".json"]),
            default_value="",
            help_text=(
                "Precalculated trim and shift parameters from a previous Servalcat Trim"
                " job. If provided, these will be used instead of calculating the"
                " trimming from any mask or model files."
            ),
            jobop_group="Advanced",
        )
        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self) -> None:
        maps = self.joboptions["input_maps"].get_list()
        for in_map in maps:
            out_name = f"{os.path.splitext(os.path.basename(in_map))[0]}_trimmed.mrc"
            self.add_output_node(out_name, NODE_DENSITYMAP, ["trimmed"])

        mask = self.joboptions["mask"].get_string()
        if mask:
            mask_split = os.path.splitext(os.path.basename(mask))[0]
            out_name = f"{mask_split}_trimmed.mrc"
            self.add_output_node(out_name, NODE_MASK3D, ["trimmed"])

        models = self.joboptions["input_models"].get_list()
        for model in models:
            mod_split = os.path.splitext(os.path.basename(model))
            out_name = f"{mod_split[0]}_trimmed{mod_split[1]}"
            self.add_output_node(out_name, NODE_ATOMCOORDS, ["trimmed"])

        # Shifts are not written out if they are given as input
        if not self.joboptions["shifts_json"].get_string():
            self.add_output_node(
                "trim_shifts.json", NODE_PROCESSDATA, ["servalcat", "shifts"]
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        cmd: List[Union[int, float, str]] = ["servalcat", "trim"]

        maps = self.joboptions["input_maps"].get_list()
        if maps:
            cmd.append("--maps")
            cmd.extend([os.path.relpath(map, self.working_dir) for map in maps])

        models = self.joboptions["input_models"].get_list()
        if models:
            cmd.append("--model")
            cmd.extend([os.path.relpath(model, self.working_dir) for model in models])

        shifts = self.joboptions["shifts_json"].get_string()
        if shifts:
            cmd.extend(["--shifts", os.path.relpath(shifts, self.working_dir)])
        else:
            mask = self.joboptions["mask"].get_string()
            if mask:
                cmd.extend(["--mask", os.path.relpath(mask, self.working_dir)])

            padding = self.joboptions["padding"].get_number()
            cmd.extend(["--padding", padding])

            mask_threshold = self.joboptions["mask_threshold"].get_number()
            cmd.extend(["--mask_cutoff", mask_threshold])

            if self.joboptions["noncubic"].get_boolean():
                cmd.append("--noncubic")
            if self.joboptions["noncentered"].get_boolean():
                cmd.append("--noncentered")
            if not self.joboptions["do_shift"].get_boolean():
                cmd.append("--no_shift")

        return [PipelinerCommand(cmd)]

    def gather_metadata(self) -> Dict[str, Any]:
        logfile = Path(self.output_dir) / "run.out"
        with open(logfile) as lf:
            loglines = lf.readlines()
        in_maps: Dict[str, dict] = {}
        out_maps: Dict[str, dict] = {}
        mdict: Dict[str, Dict[str, Dict[str, float]]] = {}
        in_inputs = True
        current = ""
        new_grid, new_cell = {}, {}
        for line in loglines:
            ls = line.split()
            if len(ls) > 0:
                if "Reading CCP4/MRC map file" in line and in_inputs:
                    current = ls[-1]
                    in_maps[current] = {}
                    mdict = in_maps
                    continue
                elif line.startswith("Writing"):
                    current = ls[-1]
                    out_maps[current] = {}
                    mdict = out_maps
                    in_inputs = False
                    continue
                if ls[0] == "Shape:" and in_inputs:
                    mdict[current]["dimensions_voxel"] = {
                        "x": int(ls[1]),
                        "y": int(ls[2]),
                        "z": int(ls[3]),
                    }
                if ls[0] == "Cell:" and in_inputs:
                    mdict[current]["dimensions_angstrom"] = {
                        "x": float(ls[1]),
                        "y": float(ls[2]),
                        "z": float(ls[3]),
                    }
                if line.startswith("New Cell:"):
                    new_cell = {"x": float(ls[2]), "y": float(ls[3]), "z": float(ls[4])}
                if line.startswith("New grid:"):
                    new_grid = {"x": int(ls[2]), "y": int(ls[3]), "z": int(ls[4])}

        return {
            "input_maps": in_maps,
            "output_map_dimensions": {
                "dimensions_voxel": new_grid,
                "dimensions_angstrom": new_cell,
            },
        }

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        data = self.gather_metadata()
        input_maps = data["input_maps"]
        table_rows = []
        for map_name, map_data in input_maps.items():
            dims_vox = map_data["dimensions_voxel"]
            dims_ang = map_data["dimensions_angstrom"]
            table_rows.append(
                [
                    map_name,
                    f"{dims_vox['x']} x {dims_vox['y']} x {dims_vox['z']}",
                    f"{dims_ang['x']} x {dims_ang['y']} x {dims_ang['z']}",
                ]
            )
        out_dims_vox = data["output_map_dimensions"]["dimensions_voxel"]
        trim_vox = f"{out_dims_vox['x']} x {out_dims_vox['y']} x {out_dims_vox['z']}"
        out_dims_ang = data["output_map_dimensions"]["dimensions_angstrom"]
        trim_ang = f"{out_dims_ang['x']} x {out_dims_ang['y']} x {out_dims_ang['z']}"
        table_rows.append(["", "", ""])
        table_rows.append(["Trimmed maps", trim_vox, trim_ang])
        display_objs = [
            create_results_display_object(
                "table",
                title="Trimming results",
                headers=["Map", "size (voxel)", "size (Å)"],
                table_data=table_rows,
                associated_data=[x.name for x in self.output_nodes],
            )
        ]

        output_maps = [node.name for node in self.output_nodes if node.format == "mrc"]
        display_objs.append(
            make_mrcs_central_slices_montage(
                in_files={outmap: "" for outmap in output_maps},
                output_dir=self.output_dir,
            )
        )
        return display_objs
