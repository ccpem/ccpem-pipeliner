#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import shutil
from pathlib import Path
from typing import List, Tuple, Optional, Dict, Any, Sequence, Union
import math
import numpy as np
from ccpem_utils.other.calc import get_skewness
from ccpem_utils.map.mrcfile_utils import check_origin_zero
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionValidationResult,
    JobOptionCondition,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import create_results_display_object
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_SEQUENCE,
    NODE_EVALUATIONMETRIC,
    NODE_PROCESSDATA,
    NODE_LOGFILE,
)
from pipeliner.utils import get_job_script
from pipeliner.jobs.other.checkmysequence_job import ModelSequenceValidate
from pipeliner.jobs.ccpem.molprobity_job import ValidateGeometry
from pipeliner.jobs.ccpem.tempy_scores_job import TEMPyMapFit
from pipeliner.jobs.ccpem.confidence_map_job import ConfidenceMap


class ModelValidate(PipelinerJob):
    PROCESS_NAME = "pipeliner.atomic_model_validation"
    OUT_DIR = "Model_Validation"
    deactivation_conditions: Dict[str, JobOptionCondition] = {
        "run_fsc": JobOptionCondition([("quick_eval", "=", True)]),
        "run_glob": JobOptionCondition([("quick_eval", "=", True)]),
        "run_fdr": JobOptionCondition([("quick_eval", "=", True)]),
        "run_checkmysequence": JobOptionCondition([("quick_eval", "=", True)]),
        "resolution": JobOptionCondition(
            [
                ("run_glob", "=", False),
                ("run_smoc", "=", False),
                ("run_fsc", "=", False),
            ],
            "ALL",
        ),
        "input_halfmap1": JobOptionCondition([("run_fsc", "=", False)]),
        "input_halfmap2": JobOptionCondition([("run_fsc", "=", False)]),
        "input_mask": JobOptionCondition([("run_glob", "=", False)]),
        "use_mask_fsc": JobOptionCondition(
            [("run_fsc", "=", False), ("input_mask", "=", "")]
        ),
        "input_contour": JobOptionCondition(
            [
                ("run_glob", "=", False),
                ("input_mask", "!=", ""),
                ("run_fdr", "=", True),
                ("input_fdrmap", "!=", ""),
            ]
        ),
        "contourlevel": JobOptionCondition(
            [
                ("run_glob", "=", False),
                ("input_mask", "!=", ""),
                ("run_fdr", "=", True),
                ("input_contour", "=", False),
                ("input_fdrmap", "!=", ""),
            ]
        ),
        "input_fdrmap": JobOptionCondition([("run_fdr", "=", False)]),
        "input_sequence": JobOptionCondition([("run_checkmysequence", "=", False)]),
    }

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Atomic model validation"
        self.jobinfo.short_desc = "Validate atomic models with multiple methods"
        self.jobinfo.long_desc = (
            "Validate protein atomic model quality and agreement with map.\n"
            "Reports outliers based on:"
            "Stereochemistry:  (Molprobity)\n"
            "Ramachandran and torsion Z-scores: (Tortoize)\n"
            "Global model fit in map : (Servalcat model-map FSC)"
            "Local fit of atomic model in map: (TEMPy SMOC).\n"
            "Local backbone trace in map: (FDR-backbone)\n"
            "\n\nTo run molprobity and servalcat "
            "install CCP4 versions 8.0.015 or above.\n"
            "To run TEMPy, run \n"
            "pip install biotempy \n"
            "Requires biotempy>=2.2.0a3\n"
        )
        self.jobinfo.programs = [
            ExternalProgram("gemmi"),
        ]
        self.jobinfo.version = "0.2"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.references = []
        self.jobinfo.references.extend(ValidateGeometry.job_references)  # molprobity
        self.jobinfo.references.extend(
            [
                Ref(
                    authors=[
                        "Joseph AP",
                        "Olek M",
                        "Malhotra S",
                        "Zhang P",
                        "Cowtan K",
                        "Burnley, T",
                        "Winn M",
                    ],
                    title="Atomic model validation using the CCP-EM software suite",
                    journal="Acta Cryst D",
                    year="2022",
                    volume="78",
                    issue="2",
                    pages="152-161",
                    doi="10.1107/S205979832101278X",
                ),
                Ref(
                    authors=[
                        "Sobolev OV",
                        "Afonine PV",
                        "Moriarty NW",
                        "Hekkelman ML",
                        "Joosten RP",
                        "Perrakis A",
                        "Adams PD",
                    ],
                    title=(
                        "A global Ramachandran score identifies protein structures "
                        "with unlikely stereochemistry."
                    ),
                    journal="Structure",
                    year="2020",
                    volume="28",
                    issue="11",
                    pages="1249-1258",
                    doi="10.1016/j.str.2020.08.005",
                ),
                Ref(
                    authors=["Yamashita K", "Palmer CM", "Burnley, T", "Murshudov GN"],
                    title=(
                        "Cryo-EM single-particle structure refinement and map "
                        "calculation using Servalcat"
                    ),
                    journal="Acta Cryst D",
                    year="2021",
                    volume="77",
                    issue="10",
                    pages="1282-1291",
                    doi="10.1107/S2059798321009475",
                ),
                Ref(
                    authors=["Beckers M", "Jakobi AJ", "Sachse C"],
                    title=(
                        "Thresholding of cryo-EM density maps by False Discovery "
                        "Rate control"
                    ),
                    journal="IUCrJ.",
                    year="2019",
                    volume="6",
                    issue="1",
                    pages="18-33",
                    doi="10.1107/S2052252518014434",
                ),
                Ref(
                    authors=["Olek M", "Joseph AP"],
                    title=(
                        "Cryo-EM Map-Based Model Validation Using the False "
                        "Discovery Rate Approach"
                    ),
                    journal="Front. Mol. Biosci.",
                    year="2021",
                    volume="8",
                    doi="10.3389/fmolb.2021.652530",
                ),
                Ref(
                    authors=["Chojnowski G"],
                    title=(
                        "Sequence assignment validation in cryo-EM models with"
                        " checkMySequence"
                    ),
                    journal="Acta Cryst.",
                    year="2022",
                    volume="D78",
                    pages="806-816",
                    doi="10.1107/S2059798322005009",
                ),
            ]
        )
        self.jobinfo.references.extend(TEMPyMapFit.job_references)  # tempy
        self.jobinfo.documentation = "https://doi.org/10.1107/S205979832101278X"

        self.outlier_plot_format: Dict[str, Tuple[str, int, str, Optional[str]]] = {
            "b-factor_dev": ("lines", 0, "black", None),
            "molprobity": ("markers", 1, "red", "square"),
            "smoc": ("lines", 2, "green", None),
            "smoc_outlier": ("markers", 2, "red", "circle-x"),
            "fdrscore": ("lines", 2, "blue", None),
            "fdrscore_outlier": ("markers", 2, "red", "x"),
            "tortoize_ramaz": ("lines", 1, "orange", None),
            "tortoize_torsionz": ("lines", 1, "black", None),
        }

        self.outlier_plot_group_format: Dict[int, List[str]] = {
            0: ["B-factor deviation", "B-factor dev"],
            1: ["Geometry", "Z-score"],
            2: ["Map fit", "Score"],
        }
        # TODO : fix mapmask issue and enable output plot
        self.fsc_plot_format = {
            "FSC_modelmask": {
                "mode": "lines",
                "line": {"color": "darkgreen", "width": 3},
            },
            "FSC_mapmask": {
                "mode": "lines",
                "line": {"color": "orange", "width": 3},
            },
            "halfmap_FSC_modelmask": {
                "mode": "lines",
                "line": {"color": "limegreen", "dash": "dash", "width": 3},
            },
        }

        # JOB OPTIONS
        # MAIN OPTIONS
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="Model to be evaluated (minimal required input)",
            is_required=True,
        )
        # Quick mode
        self.joboptions["quick_eval"] = BooleanJobOption(
            label="Quick evaluation?",
            default_value=False,
            help_text=(
                "Evaluate clashes with Molprobity and per-residue map-fit with "
                "TEMPy SMOC"
            ),
        )
        self.joboptions["run_molprobity"] = BooleanJobOption(
            label="Molprobity (Geometry statistics)",
            default_value=False if not shutil.which("molprobity.molprobity") else True,
            help_text="Run Molprobity geometry evaluation",
            jobop_group="Metrics",
        )
        self.joboptions["run_fsc"] = BooleanJobOption(
            label="Servalcat FSC (Model-map FSC)",
            default_value=False,
            help_text="Run Servalcat model-map fsc calculation",
            deactivate_if=self.deactivation_conditions["run_fsc"],
            jobop_group="Metrics",
        )
        self.joboptions["run_glob"] = BooleanJobOption(
            label="TEMPy global (Real space map fit)",
            default_value=False if not shutil.which("TEMPy.scores") else True,
            help_text="Run TEMPy map-fit score calculation",
            deactivate_if=self.deactivation_conditions["run_glob"],
            jobop_group="Metrics",
        )
        self.joboptions["run_smoc"] = BooleanJobOption(
            label="TEMPy SMOC (Per-residue map fit)",
            default_value=False if not shutil.which("TEMPy.smoc") else True,
            help_text="Run SMOC model-map fit evaluation",
            jobop_group="Metrics",
        )
        self.joboptions["run_fdr"] = BooleanJobOption(
            label="FDR backbone (Per-residue backbone trace)",
            default_value=False,
            help_text=(
                "Run FDR backbone trace evaluation. The confidence map "
                "calculated or supplied as input is used as mask for TEMPy global "
                "score calculation if a seperate input mask is not provided."
            ),
            deactivate_if=self.deactivation_conditions["run_fdr"],
            jobop_group="Metrics",
        )
        self.joboptions["run_checkmysequence"] = BooleanJobOption(
            label="CheckMySequence (Sequence agreement)",
            default_value=False,
            help_text="Run checkMySequence model sequence evaluation",
            deactivate_if=self.deactivation_conditions["run_checkmysequence"],
            jobop_group="Metrics",
        )
        # OTHER OPTIONS BASED ON METRIC SELECTION
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "The input map to evaluate the model against. Required"
                " for all metrics except Molprobity, Servalcat FSC."
            ),
            is_required=False,
            required_if=JobOptionCondition(
                [
                    ("run_glob", "=", True),
                    ("run_smoc", "=", True),
                    ("run_fdr", "=", True),
                    ("run_checkmysequence", "=", True),
                ],
            ),
            jobop_group="Metric specific inputs",
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in angstroms",
            required_if=JobOptionCondition(
                [("run_glob", "=", True), ("run_smoc", "=", True)]
            ),
            deactivate_if=self.deactivation_conditions["resolution"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_halfmap1"] = InputNodeJobOption(
            label="Input halfmap 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "Input halfmap1 to evaluate the model against. Required"
                " for FSC calculation."
            ),
            is_required=False,
            required_if=JobOptionCondition([("run_fsc", "=", True)]),
            deactivate_if=self.deactivation_conditions["input_halfmap1"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_halfmap2"] = InputNodeJobOption(
            label="Input halfmap 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "Input halfmap2 to evaluate the model against. "
                "Required for FSC calculation."
            ),
            is_required=False,
            required_if=JobOptionCondition([("run_fsc", "=", True)]),
            deactivate_if=self.deactivation_conditions["input_halfmap2"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input map mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The mask to apply to the map (preferred over Contour Level)"
            " (used by TEMPy global scores and optionally for FSC)",
            is_required=False,
            deactivate_if=self.deactivation_conditions["input_mask"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["use_mask_fsc"] = BooleanJobOption(
            label="Use input mask for FSC?",
            default_value=True,
            help_text=(
                "This will be used for half-map and model-map FSC calculation."
                " For a partial model (e.g. single subunit of a complex), this should"
                " usually be a mask around the modelled region. If the mask is much"
                " larger than the model, the model-map FSC curve will be lower because"
                " of the presence of unmodelled regions."
            ),
            deactivate_if=self.deactivation_conditions["use_mask_fsc"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_contour"] = BooleanJobOption(
            label="Input contour threshold?",
            default_value=False,
            help_text=(
                "For TEMPy global scores, the map will be contoured at this threshold"
                ", ignored if input mask provided."
            ),
            deactivate_if=self.deactivation_conditions["input_contour"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["contourlevel"] = FloatJobOption(
            label="Contour Level",
            default_value=-100,
            step_value=0.01,
            help_text=(
                "Contour level for masking (used by TEMPy global scores)"
                ", ignored if input mask provided. If input mask, input confidence map "
                " or a contourlevel are not provided, a contour level is estimated "
                "automatically (not recommended)."
            ),
            deactivate_if=self.deactivation_conditions["contourlevel"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_fdrmap"] = InputNodeJobOption(
            label="Input FDR confidence map",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "Input confidence map to calculate FDR-backbone score. This is "
                "used as mask for TEMPy global score calculation if a seperate input "
                "mask is not provided."
            ),
            is_required=False,
            deactivate_if=self.deactivation_conditions["input_fdrmap"],
            jobop_group="Metric specific inputs",
        )
        self.joboptions["input_sequence"] = InputNodeJobOption(
            label="Input sample sequence",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text=(
                "Reference sequence to validate model against. Required for"
                " CheckMySequence."
            ),
            is_required=False,
            deactivate_if=self.deactivation_conditions["input_sequence"],
            required_if=JobOptionCondition([("run_checkmysequence", "=", True)]),
            jobop_group="Metric specific inputs",
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # molprobity
        if self.joboptions["run_molprobity"].get_boolean() and not shutil.which(
            "molprobity.molprobity"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_molprobity"]],
                    message=(
                        "'molprobity.molprobity' is not available in the system path, "
                        "check that CCP4 is installed "
                    ),
                )
            )
        # smoc
        if self.joboptions["run_smoc"].get_boolean() and not shutil.which("TEMPy.smoc"):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_smoc"]],
                    message=(
                        "'TEMPy.smoc' is not available in the system path, check that "
                        "it is installed"
                    ),
                )
            )
        # glob scores
        if self.joboptions["run_glob"].get_boolean() and not shutil.which(
            "TEMPy.scores"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_glob"]],
                    message=(
                        "'TEMPy.scores' is not available in the system path, "
                        "check that it is installed"
                    ),
                )
            )
        if (
            self.joboptions["run_glob"].get_boolean()
            or self.joboptions["run_smoc"].get_boolean()
        ):
            if self.joboptions["resolution"].get_number() == -1:
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["resolution"]],
                        message="Map resolution in Å (required)",
                    )
                )
        # fdr map
        run_fdr_failed = False
        if self.joboptions["run_fdr"].get_boolean():
            if self.joboptions["input_fdrmap"].get_string() == "":
                if not os.path.isfile(ConfidenceMap.fdr_path) or not shutil.which(
                    "ccpem-python"
                ):
                    errors.append(
                        JobOptionValidationResult(
                            result_type="error",
                            raised_by=[self.joboptions["run_fdr"]],
                            message=(
                                "FDRcontrol script not found. "
                                "Cannot calculate FDR score. "
                                "Please provide Confidence map input. "
                            ),
                        )
                    )
                    run_fdr_failed = True
                else:
                    errors.append(
                        JobOptionValidationResult(
                            result_type="warning",
                            raised_by=[self.joboptions["run_fdr"]],
                            message=(
                                "Confidence map will be calculated if not provided. "
                                "Requires unmasked, preferrably raw-map. If the input "
                                "map has non standard axis order (not x,y,z), this "
                                "will be ignored and the output confidence map is "
                                "likely to have the axes flipped."
                            ),
                        )
                    )
                    errors.append(
                        JobOptionValidationResult(
                            result_type="warning",
                            raised_by=[self.joboptions["input_map"]],
                            message=(
                                "FDR backbone score:   If Confidence map not provided, "
                                "it will be calculated. Requires unmasked input map, "
                                "preferrably raw-map. "
                            ),
                        )
                    )
        input_map = self.joboptions["input_map"].get_string()
        if (
            self.joboptions["run_fdr"].get_boolean()
            and self.joboptions["input_fdrmap"].get_string() == ""
            and os.path.isfile(input_map)
            and not check_origin_zero(input_map)
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["input_map"]],
                    message=(
                        "Input map has non-zero origin record. An additional output "
                        "confidence map will be created with origin set to match this "
                        "(_shifted.mrc). "
                    ),
                )
            )
        # check for mask
        if self.joboptions["run_glob"].get_boolean():
            # no input mask
            if self.joboptions["input_mask"].get_string() == "":
                if self.joboptions["run_fdr"].get_boolean() and not run_fdr_failed:
                    errors.append(
                        JobOptionValidationResult(
                            result_type="warning",
                            raised_by=[self.joboptions["run_glob"]],
                            message=(
                                "Input mask not provided, the confidence map will be "
                                "used as mask for TEMPy global score calcualtion. "
                            ),
                        )
                    )
                elif not self.joboptions["input_contour"].get_boolean():
                    errors.append(
                        JobOptionValidationResult(
                            result_type="warning",
                            raised_by=[self.joboptions["run_glob"]],
                            message=(
                                "Please provide input mask or contourlevel. "
                                "Otherwise, a contour level is estimated automatically "
                                "for masking (not recommended). "
                            ),
                        )
                    )
                if (
                    self.joboptions["input_contour"].get_boolean()
                    and self.joboptions["contourlevel"].get_number() == -100
                ):
                    errors.append(
                        JobOptionValidationResult(
                            result_type="warning",
                            raised_by=[self.joboptions["contourlevel"]],
                            message=("Ensure a valid contour level is input"),
                        )
                    )
        # checkmysequence
        if self.joboptions["run_checkmysequence"].get_boolean():
            if not shutil.which("checkmysequence"):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["run_checkmysequence"]],
                        message=(
                            "'checkmysequence' is not available in the system path, "
                            "check that it is installed"
                        ),
                    )
                )

        return errors

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        input_fdrmap = self.joboptions["input_fdrmap"].get_string()
        quick_eval = self.joboptions["quick_eval"].get_boolean()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_glob = self.joboptions["run_glob"].get_boolean()
        run_fsc = self.joboptions["run_fsc"].get_boolean()
        run_fdr = self.joboptions["run_fdr"].get_boolean()
        run_checkmysequence = self.joboptions["run_checkmysequence"].get_boolean()
        if input_map != "":
            mapid = os.path.splitext(os.path.basename(input_map))[0]
        # Now make the nodes
        cif_ext = ValidateGeometry.get_cif_ext(input_model)
        if cif_ext:
            self.add_output_node(
                Path(input_model).name.replace(cif_ext, ".pdb"),
                NODE_ATOMCOORDS,
                ["gemmi", "pdbconvert"],
            )
        if not quick_eval:
            if run_molprobity:
                molprobity_out = ValidateGeometry.get_molprobity_logfile_name(modelid)
                self.add_output_node(
                    molprobity_out, NODE_LOGFILE, ["molprobity", "output"]
                )
            if run_fsc:
                self.add_output_node(
                    "modelmask_fsc.dat", NODE_EVALUATIONMETRIC, ["servalcat", "fsc"]
                )
            if run_glob:
                score_outcsv = TEMPyMapFit.get_scores_outcsv(modelid, mapid)
                self.add_output_node(
                    score_outcsv, NODE_EVALUATIONMETRIC, ["tempy", "global_scores"]
                )
            if run_fdr and not input_fdrmap and os.path.isfile(ConfidenceMap.fdr_path):
                inp_map_basename = os.path.basename(os.path.splitext(input_map)[0])
                confmap = ConfidenceMap.get_confmap_name(mapid=inp_map_basename)
                self.add_output_node(
                    confmap,
                    NODE_MASK3D,
                    ["confidencemap", "fdr_map"],
                )
                if os.path.isfile(input_map) and not check_origin_zero(input_map):
                    self.add_output_node(
                        ConfidenceMap.get_confmap_shifted_name(confmap),
                        NODE_MASK3D,
                        ["confidencemap", "nonzero_origin"],
                    )
            if run_checkmysequence:
                self.add_output_node(
                    modelid + "_" + mapid + "checkseq.json",
                    NODE_PROCESSDATA,
                    ["checkmysequence", "json_out"],
                )
        # quick run outputs
        elif run_molprobity:
            clashscore_out = ValidateGeometry.get_clashscore_jsonfile_name(modelid)
            self.add_output_node(
                clashscore_out, NODE_EVALUATIONMETRIC, ["clashscore", "output"]
            )
        # self.add_output_node(
        # "mapmask_fsc.dat", NODE_EVALUATIONMETRIC, ["servalcat", "fsc"]
        # )
        if run_smoc and input_map:
            self.add_output_node(
                TEMPyMapFit.get_smoc_outjson(modelid=modelid, mapid=mapid),
                NODE_EVALUATIONMETRIC,
                ["tempy", "smoc_scores"],
            )
        if run_molprobity or run_smoc:  # or run_tortoize:
            self.add_output_node(
                "global_report.txt", NODE_EVALUATIONMETRIC, ["modelval", "glob_output"]
            )
            self.add_output_node(
                "outlier_clusters.csv", NODE_PROCESSDATA, ["modelval", "cluster_output"]
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        if input_map:
            input_map_abs_path = input_map
            input_map = os.path.relpath(input_map, self.working_dir)
            mapid = os.path.splitext(os.path.basename(input_map))[0]
        input_mask = self.joboptions["input_mask"].get_string()
        if input_mask:
            input_mask = os.path.relpath(input_mask, self.working_dir)
        input_hmap1 = self.joboptions["input_halfmap1"].get_string()
        input_hmap2 = self.joboptions["input_halfmap2"].get_string()
        input_fdrmap = self.joboptions["input_fdrmap"].get_string()
        if input_fdrmap:
            input_fdrmap = os.path.relpath(input_fdrmap, self.working_dir)
        resolution = self.joboptions["resolution"].get_number()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        # run_tortoize = self.joboptions["run_tortoize"].get_boolean()
        run_fsc = self.joboptions["run_fsc"].get_boolean()
        run_glob = self.joboptions["run_glob"].get_boolean()
        run_fdr = self.joboptions["run_fdr"].get_boolean()
        contourlevel = None
        if self.joboptions["input_contour"].get_boolean():
            contourlevel = self.joboptions["contourlevel"].get_number()

        commands = []
        # Convert cif inputs to pdb as Molprobity does not accept cif format
        input_model_pdb = input_model
        cif_ext = ValidateGeometry.get_cif_ext(input_model)
        if cif_ext:
            input_model_pdb = Path(input_model).name.replace(cif_ext, ".pdb")
            commands.append(
                ValidateGeometry.get_cifconvert_command(input_model, input_model_pdb)
            )
        commands.append(self.get_bfact_command(input_model, modelid))
        confmap = ""
        if self.joboptions["quick_eval"].get_boolean():
            commands += self.get_quickeval_commands(
                run_molprobity,
                run_smoc,
                input_model_pdb,
                modelid,
                input_map,
                input_model,
                resolution,
                mapid,
            )
        else:
            # molprobity
            if run_molprobity:
                commands += self.get_global_molprobity_commands(
                    input_model_pdb, modelid
                )
            # FDR backbone score
            if run_fdr:
                confmap = self.get_confmap_name_from_inputs(input_fdrmap, mapid)
                # if confmap not input
                if not input_fdrmap and os.path.isfile(ConfidenceMap.fdr_path):
                    commands += self.get_confidencemap_commands(input_map=input_map)
                    # set confmap origin if non-zero
                    if not check_origin_zero(input_map_abs_path):
                        commands.append(
                            ConfidenceMap.get_confmap_origin_shift_command(
                                input_map=input_map, confmap=confmap
                            )
                        )
                        confmap = ConfidenceMap.get_confmap_shifted_name(confmap)
                if confmap:
                    commands += self.get_fdrbackbone_commands(
                        input_model=input_model,
                        confmap=confmap,
                        resolution=resolution,
                        modelid=modelid,
                        mapid=mapid,
                    )
            # servalcat fsc
            if run_fsc:
                use_mask_fsc = self.joboptions["use_mask_fsc"].get_boolean()
                commands.append(
                    self.get_servalcat_command(
                        input_model,
                        os.path.relpath(input_hmap1, self.working_dir),
                        os.path.relpath(input_hmap2, self.working_dir),
                        use_mask_fsc=use_mask_fsc,
                        input_mask=input_mask,
                    )
                )
            # tempy glob
            if run_glob:
                maskfile, contour = self.get_maskfile_contour_threshold(
                    input_map=input_map_abs_path,  # contour estimated before run
                    input_mask=input_mask,
                    confmap=confmap,
                    contourlevel=contourlevel,
                )
                commands += self.get_tempyglob_commands(
                    input_model=input_model,
                    input_map=input_map,
                    resolution=resolution,
                    maskfile=maskfile,
                    contour=contour,
                    mapid=mapid,
                    modelid=modelid,
                )
            # smoc
            if run_smoc:
                commands += self.get_smoc_commands(
                    input_map, input_model, resolution, modelid, mapid
                )
            # tortoize
            # if run_tortoize:
            #     commands += self.get_tortoize_commands(modelid, input_model)
            # checkmysequence
            if self.joboptions["run_checkmysequence"].get_boolean():
                input_seq = self.joboptions["input_sequence"].get_string()
                input_seq = os.path.relpath(input_seq, self.working_dir)
                commands += ModelSequenceValidate.get_checkmysequence_commands(
                    input_map,
                    input_model,
                    input_seq,
                    modelid,
                    mapid,
                )
        if run_molprobity or run_smoc or run_fsc or run_fdr:  # or run_tortoize
            commands.append(self.get_results_command())
        return [PipelinerCommand(x) for x in commands]

    def get_quickeval_commands(
        self,
        run_molprobity: bool,
        run_smoc: bool,
        input_model_pdb: str,
        modelid: str,
        input_map: str,
        input_model: str,
        resolution: float,
        mapid: str,
    ) -> List[List[str]]:
        commands: List[List[str]] = []
        if run_molprobity:
            commands.append(
                ValidateGeometry.get_clashscore_run_command(input_model_pdb, modelid)
            )
            commands.append(
                ValidateGeometry.get_molprobity_results_command(
                    modelid,
                    run_global=False,
                    run_clashscore=True,
                )
            )
        if run_smoc and input_map:
            commands += self.get_smoc_commands(
                input_map, input_model, resolution, modelid, mapid
            )
        return commands

    @staticmethod
    def get_results_command() -> List[str]:
        results_command: List[str] = [
            "python3",
            get_job_script("model_validation/validation_results.py"),
        ]
        return results_command

    @staticmethod
    def get_bfact_command(input_model: str, modelid: str) -> List[str]:
        # bfactor distribution
        # pdb_num = 1  # this needs to be set for multiple model input
        bfact_command = [
            "python3",
            get_job_script("model_validation/analyse_bfactors.py"),
        ]
        bfact_command += ["-p", input_model]
        bfact_command += ["-mid", modelid]
        return bfact_command

    @staticmethod
    def get_global_molprobity_commands(
        input_model: str,
        modelid: str,
    ) -> List[List[str]]:
        commands: List[List[str]] = []
        commands += ValidateGeometry.get_molprobity_run_commands(input_model, modelid)
        commands.append(
            ValidateGeometry.get_molprobity_results_command(
                modelid,
                run_global=True,
            )
        )
        return commands

    @staticmethod
    def get_servalcat_command(
        input_model: str,
        input_hmap1: str,
        input_hmap2: str,
        use_mask_fsc: bool = True,
        input_mask: Optional[str] = None,
    ) -> List[str]:
        # generate model-map and halfmap fsc with model mask
        servalcat_fsc_command: List[str] = ["servalcat", "fsc"]
        servalcat_fsc_command += ["--model", input_model]
        if use_mask_fsc and input_mask:
            servalcat_fsc_command += ["--mask", input_mask]
        else:
            servalcat_fsc_command += ["--mask_soft_edge", "6"]
        servalcat_fsc_command += ["--halfmaps", input_hmap1, input_hmap2]
        servalcat_fsc_command += ["-o", "modelmask_fsc.dat"]
        return servalcat_fsc_command

    def get_confmap_name_from_inputs(self, input_fdrmap: str, mapid: str) -> str:
        confmap = ""
        if not input_fdrmap:
            if os.path.isfile(ConfidenceMap.fdr_path):
                confmap = ConfidenceMap.get_confmap_name(mapid=mapid)
        else:
            confmap = input_fdrmap
        return confmap

    @staticmethod
    def get_confmap_shifted_name(confmap):
        return os.path.splitext(os.path.basename(confmap))[0] + "_shifted.mrc"

    def get_confidencemap_commands(self, input_map: str) -> List[List[str]]:
        commands = []
        mplbackend_command = ConfidenceMap.get_mplbackend_command()
        confmap_command = ConfidenceMap.get_minimal_confidencemap_command(
            input_map=input_map
        )
        commands.extend([mplbackend_command, confmap_command])
        return commands

    @staticmethod
    def get_maskfile_contour_threshold(
        input_map: str,
        input_mask: str = "",
        confmap: str = "",
        contourlevel: Optional[float] = None,
    ) -> Tuple[str, float]:
        maskfile = ""
        if input_mask:  # input mask
            maskfile = input_mask
        elif confmap:  # confidence map input, mask edge is ignored (applymask)
            maskfile = confmap
        # get or estimate contour
        contour = TEMPyMapFit.calc_contour_from_input_options(
            input_map=input_map, input_mask=maskfile, contourlevel=contourlevel
        )
        return maskfile, contour

    @staticmethod
    def get_tempyglob_commands(
        input_model: str,
        input_map: str,
        resolution: float,
        contour: float,
        maskfile: str = "",
        mapid: str = "",
        modelid: str = "",
    ) -> List[List]:
        commands = []
        masked_map = input_map
        if not mapid:
            mapid = os.path.splitext(os.path.basename(input_map))[0]
        if not modelid:
            modelid = os.path.splitext(os.path.basename(input_model))[0]
        # apply mask
        if maskfile:
            applymask_command = TEMPyMapFit.get_applymask_command(
                input_map=input_map, input_mask=maskfile
            )
            masked_map = (
                os.path.splitext(os.path.basename(input_map))[0] + "_masked.mrc"
            )
            commands += [applymask_command]
        # generate model-map and halfmap fsc with model mask
        tempyglob_command = TEMPyMapFit.get_tempy_score_command(
            input_model=input_model,
            input_map=masked_map,
            resolution=resolution,
            contour=contour,
            mapid=mapid,
            modelid=modelid,
        )
        commands += [tempyglob_command]
        return commands

    @staticmethod
    def get_smoc_commands(
        input_map: str,
        input_model: str,
        resolution: float,
        modelid: str,
        mapid: str,
    ) -> Sequence[List[str]]:
        # pdb_num = 1  # this needs to be set for multiple model input
        smoc_command = TEMPyMapFit.get_smoc_run_command(
            input_map, input_model, resolution, modelid, mapid
        )
        smoc_results_command = TEMPyMapFit.get_smoc_results_command(
            modelid=modelid, mapid=mapid
        )
        return [smoc_command, smoc_results_command]

    @staticmethod
    def get_fdrbackbone_commands(
        input_model: str, confmap: str, resolution: float, modelid: str, mapid: str
    ) -> List[List[str]]:
        commands: List[List[str]] = []
        fdrbackbone_command: List[str] = [
            "python3",
            get_job_script("model_validation/fdr_val.py"),
            input_model,
            confmap,
        ]
        fdrbackbone_out = modelid + "_FDR_ranked_residues"
        if resolution and resolution != -1:
            fdrbackbone_command += [
                "--resolution",
                str(resolution),
                "--coord_mode",
                "ST",
            ]
            fdrbackbone_out += "_ST"
        else:
            fdrbackbone_out += "_1A"
        fdrbackbone_out += ".csv"

        commands += [fdrbackbone_command]

        fdrbackbone_results_command: List[str] = [
            "python3",
            get_job_script("model_validation/get_fdrbackbone_results.py"),
            "-fdrscore",
            fdrbackbone_out,
            "-coord",
            modelid + "_residue_coordinates.json",
            "-id",
            modelid + "_" + mapid,
        ]
        commands += [fdrbackbone_results_command]
        return commands

    @staticmethod
    def get_tortoize_commands(
        modelid: str, input_model: str
    ) -> List[List[Union[int, float, str]]]:
        tortoize_out = modelid + "_tortoize.json"
        tortoize_command: List[Union[int, float, str]] = [
            "tortoize",
            input_model,
            tortoize_out,
        ]
        tortoize_results_command: List[Union[int, float, str]] = [
            "python3",
            get_job_script("model_validation/get_tortoize_results.py"),
            "-tortoize",
            tortoize_out,
            "-id",
            modelid,
        ]
        return [tortoize_command, tortoize_results_command]

    def gather_metadata(self) -> Dict[str, Any]:
        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        metadata_dict: dict = {"Peptide Plane": {}}

        for line in outlines:
            if "Ramachandran outliers" in line:
                metadata_dict["RamachandranOutliers"] = float(line.split()[3])
            if "favoured" in line:
                metadata_dict["Favoured"] = float(line.split()[2])
            if "Rotamer outliers" in line:
                metadata_dict["RotamerOutliers"] = float(line.split()[3])
            if "Cis-proline" in line:
                metadata_dict["PeptidePlane"]["CisProline"] = float(line.split()[2])
            if "Cis-general" in line:
                metadata_dict["PeptidePlane"]["CisGeneral"] = float(line.split()[2])
            if "Twisted Proline" in line:
                metadata_dict["PeptidePlane"]["TwistedProline"] = float(line.split()[3])
            if "Twisted General" in line:
                metadata_dict["PeptidePlane"]["TwistedGeneral"] = float(line.split()[3])
            if "C-beta deviations" in line:
                metadata_dict["CBetaDeviations"] = int(line.split()[3])
            if line.strip().startswith("Clashscore"):
                metadata_dict["ClashScore"] = {}
                metadata_dict["ClashScore"]["ClashScore"] = float(line.split()[2])
                metadata_dict["ClashScore"]["Percentile"] = float(line.split()[4])
                metadata_dict["ClashScore"]["N"] = int(
                    line.split()[7].split("=")[1].replace(")", "")
                )
            if "RMS(bonds)" in line:
                metadata_dict["RMSBonds"] = float(line.split()[2])
            if "RMS(angles)" in line:
                metadata_dict["RMSAngles"] = float(line.split()[2])
            if "MolProbity score" in line:
                metadata_dict["MolProbityScore"] = {}
                metadata_dict["MolProbityScore"]["MolProbityScore"] = float(
                    line.split()[3]
                )
                metadata_dict["MolProbityScore"]["Percentile"] = float(line.split()[5])
                metadata_dict["MolProbityScore"]["N"] = int(
                    line.split()[8].split("=")[1].replace(")", "")
                )
            if "Resolution" in line:
                metadata_dict["Resolution"] = float(line.split()[2])
            if "RefinementProgram" in line:
                metadata_dict["RefinementProgram"] = line.split()[3]

        return metadata_dict

    @staticmethod
    def get_bfactor_list(bfact_json: str):
        with open(bfact_json, "r") as oj:
            bfact_data = json.load(oj)
        list_bfactors = []
        for model in bfact_data:
            for chain in bfact_data[model]:
                for residue in bfact_data[model][chain]:
                    if residue == "max_dev":
                        continue
                    list_bfactors.append(bfact_data[model][chain][residue][0])
        return list_bfactors

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # TODO: long files names to be trimmed ?
        input_model = self.joboptions["input_model"].get_string()
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        quick_eval = self.joboptions["quick_eval"].get_boolean()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_tortoize = False  # self.joboptions["run_tortoize"].get_boolean()
        run_fsc = self.joboptions["run_fsc"].get_boolean()
        run_glob = self.joboptions["run_glob"].get_boolean()
        run_fdr = self.joboptions["run_fdr"].get_boolean()
        run_checkmysequence = self.joboptions["run_checkmysequence"].get_boolean()
        display_objects: List[ResultsDisplayObject] = []
        # b-factor distribution plot
        bfact_json = os.path.join(self.output_dir, modelid + "_residue_bfactors.json")
        list_bfactors = self.get_bfactor_list(bfact_json)
        flag, nbins = self.check_bfact_distribution(list_bfactors)
        start_collapsed = True
        if (
            not (
                run_molprobity
                or run_fsc
                or run_tortoize
                or run_glob
                or run_tortoize
                or run_checkmysequence
            )
            or flag
        ):
            start_collapsed = False
        plotlyhist_obj = create_results_display_object(
            "plotlyhistogram",
            data={"Per-residue B-factors (Å²)": list_bfactors},
            labels={"Per-residue B-factors (Å²)": "Per-residue B-factors (Å²)"},
            x="Per-residue B-factors (Å²)",
            title="B-factor distribution",
            associated_data=[bfact_json],
            flag=flag,
            nbins=nbins,
            start_collapsed=start_collapsed,
        )
        display_objects.append(plotlyhist_obj)
        # model-map fit
        fsc_plot_data = None
        fsc_plot_summary = None
        globscores_csv = None
        dict_fsc_plot = {}
        dict_fsc_summary = {}
        if not quick_eval:
            if run_fsc:
                fsc_plot_data = modelid + "_" + mapid + "_fsc_plot.json"
                fsc_plot_data = os.path.join(self.output_dir, fsc_plot_data)
                with open(fsc_plot_data, "r") as j:
                    dict_fsc_plot = json.load(j)
                fsc_plot_summary = modelid + "_" + mapid + "_fsc_summary.json"
                fsc_plot_summary = os.path.join(self.output_dir, fsc_plot_summary)
                with open(fsc_plot_summary, "r") as j:
                    dict_fsc_summary = json.load(j)
            if run_glob:
                globscores_csv = TEMPyMapFit.get_scores_outcsv(
                    modelid=modelid, mapid=mapid
                )
                globscores_csv = os.path.join(self.output_dir, globscores_csv)
            if run_fsc or run_glob:
                display_objects.extend(
                    self.create_map_fit_summary_table(
                        dict_fsc_plot=dict_fsc_summary,
                        fsc_plot_data=fsc_plot_summary,
                        glob_scores_data=globscores_csv,
                    )
                )
            if run_fsc:
                display_objects.append(
                    self.create_fsc_plot(dict_fsc_plot, fsc_plot_data)
                )

        # make global geometry stats table
        if run_molprobity:
            display_objects.append(
                self.create_molprobity_tortoize_summary_table(
                    self.output_dir,
                    modelid,
                    run_tortoize=run_tortoize,
                )
            )
        # checkmysequence table
        if not quick_eval and run_checkmysequence:
            checkseq_table = ModelSequenceValidate.create_checkseq_table(
                self.output_dir, modelid, mapid
            )
            if checkseq_table:
                display_objects.append(checkseq_table)
        # outlier cluster table
        if run_molprobity or (run_smoc and input_map) or run_tortoize:
            display_objects.append(self.create_outlier_cluster_table())
        # outlier plots
        if (run_smoc and input_map) or run_tortoize or run_fdr or run_checkmysequence:
            display_objects.extend(
                self.create_outlier_plots(modelid, run_checkmysequence, mapid)
            )
        return display_objects

    @staticmethod
    def create_molprobity_tortoize_summary_table(
        output_dir: str, modelid: str, run_tortoize: bool = False
    ) -> ResultsDisplayObject:
        labels = ["Metric", "Score", "Expected/Percentile"]
        mp_json = os.path.join(output_dir, modelid + "_molprobity_summary.json")
        list_mp_json = [mp_json]
        dict_mpdata = ValidateGeometry.merge_molprobity_summaries(
            list_mp_json, run_tortoize=run_tortoize
        )
        molprob_data = ValidateGeometry.set_molprobity_summary_data(dict_mpdata)
        if run_tortoize:
            tort_json = os.path.join(output_dir, modelid + "_tortoize_summary.json")
            with open(tort_json, "r") as j:
                tor_data = json.load(j)
            tortoize_data = []
            for model in tor_data:
                for metric in tor_data[model]:
                    molp_metric = " ".join(metric.split("_"))
                    score = round(float(tor_data[model][metric][0]), 3)
                    pct = tor_data[model][metric][1]
                    tortoize_data.append([molp_metric, score, pct])
                break
            molprob_data += tortoize_data

        mp_summary = create_results_display_object(
            "table",
            title="Global geometry scores",
            headers=labels,
            table_data=molprob_data,
            associated_data=list_mp_json,
        )
        return mp_summary

    def create_map_fit_summary_table(
        self,
        dict_fsc_plot: Optional[dict] = None,
        fsc_plot_data: Optional[str] = None,
        glob_scores_data: Optional[str] = None,
    ) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        emdb_json = get_job_script(
            "model_validation/validation_distribution_histograms.json"
        )
        add_percentile_column = os.path.isfile(emdb_json)

        labels = ["Metric", "Score"]
        if add_percentile_column:
            labels.append("Percentile (resolution range) (<25 bad; greater better)")

        map_resolution = self.joboptions["resolution"].get_number()
        resolution = None if map_resolution < 0 else map_resolution

        list_mapfit = []
        associated_data = []
        if dict_fsc_plot:
            for metric in [
                "FSCavg_modelmask_FSC0.5",
                "FSCavg_modelmask",
            ]:
                if metric in dict_fsc_plot:
                    metric_prefix = metric[:6]
                    if (
                        metric_prefix != "FSCavg"
                        or "h1_mapmask" in metric
                        or "h2_mapmask" in metric
                    ):
                        continue

                    score = round(dict_fsc_plot[metric], 3)
                    table_line = [metric, str(score)]
                    if add_percentile_column:
                        percentile = self.get_percentile_from_emdb_json(
                            metric=metric,
                            score=score,
                            resolution=resolution,
                        )
                        table_line.append(percentile)
                    list_mapfit.append(table_line)

            associated_data.append(fsc_plot_data)
        if glob_scores_data:
            with open(glob_scores_data, "r") as c:
                all_lines = c.readlines()
            for metric in TEMPyMapFit.tempyscore_names:
                for line in all_lines:
                    lsplit = line.split(",")
                    if lsplit[0].strip() == metric:
                        table_line = [
                            TEMPyMapFit.tempyscore_names[metric],
                            lsplit[1].strip(),
                        ]
                        if add_percentile_column:
                            percentile = self.get_percentile_from_emdb_json(
                                metric=metric,
                                score=round(float(lsplit[1].strip()), 3),
                                resolution=resolution,
                            )
                            table_line.append(percentile)
                        list_mapfit.append(table_line)
                        break
            associated_data.append(glob_scores_data)
        if list_mapfit:
            fit_summary = create_results_display_object(
                "table",
                title="Global map-fit scores",
                headers=labels,
                table_data=list_mapfit,
                associated_data=associated_data,
            )
            display_objects.append(fit_summary)
        return display_objects

    def create_outlier_cluster_table(self) -> ResultsDisplayObject:
        # outlier clusters table
        clusters_file = os.path.join(self.output_dir, "outlier_clusters.csv")
        clusters = []
        with open(clusters_file, "r") as cf:
            for line in cf:
                if line[0] == "#":
                    continue
                line_split = line.split(",")
                clusters.append(line_split)
        clusters_table = create_results_display_object(
            "table",
            title="Outlier clusters",
            headers=[
                "Cluster",
                "Chain_Residue",
                "Outlier Type(s)",
            ],
            table_data=clusters,
            associated_data=[clusters_file],
        )
        return clusters_table

    @staticmethod
    def create_iris_plot(html_file: str) -> ResultsDisplayObject:
        iris_html = create_results_display_object(
            "html",
            title="Iris validation display",
            html_file=html_file,
            associated_data=[html_file],
        )
        return iris_html

    def create_outlier_plots(
        self,
        modelid: str,
        run_checkmysequence: bool,
        mapid: str,
    ) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        list_plotted_cms_chains: List[str] = []
        # checkmysequence
        if run_checkmysequence:
            plot_out = os.path.join(
                self.output_dir, modelid + "_" + mapid + "_checkseq_plot.json"
            )
            glob_out = os.path.join(
                self.output_dir, modelid + "_" + mapid + "_checkseq_glob.json"
            )
            chain_out = os.path.join(
                self.output_dir, modelid + "_" + mapid + "_checkseq_chain.json"
            )
            dict_fragplot, dict_glob, dict_chainout = self.get_checkmysequence_data(
                plot_out, glob_out, chain_out
            )
        # residue outlier plot
        outplot_data = {}
        outplot_json = os.path.join(self.output_dir, modelid + "_plot_data.json")
        if os.path.isfile(outplot_json):
            with open(outplot_json, "r") as oj:
                outplot_data = json.load(oj)
        for model in outplot_data:
            model_chains = list(outplot_data[model].keys())
            model_chains.sort()  # sort chain IDs
            for chain in model_chains:
                # create residue outlier plot
                residueplot_display = self.get_per_residue_outlier_plot(
                    outplot_data,
                    model,
                    chain,
                    outplot_json,
                )
                if residueplot_display:
                    display_objects.append(residueplot_display)
                # checkmysequence
                if run_checkmysequence:
                    list_cms_chains = [
                        "protein chain " + chain,
                        "nucleic-acid chain " + chain,
                        chain,
                    ]
                    display_objects.extend(
                        self.get_checkmysequence_chain_results(
                            list_cms_chains,  # chain to plot
                            list_plotted_cms_chains,  # chains already plotted
                            dict_fragplot,
                            dict_glob,
                            dict_chainout,
                            plot_out,
                            chain_out,
                        )
                    )
        # checkmysequence other chains
        if run_checkmysequence:
            # get all chains from checkmyseq results
            list_cms_chains = list(dict_chainout.keys())
            for cms_chain in dict_fragplot:
                if cms_chain not in list_cms_chains:
                    list_cms_chains.append(cms_chain)
            list_cms_chains.sort()
            display_objects.extend(
                self.get_checkmysequence_chain_results(
                    list_cms_chains,  # chain to plot
                    list_plotted_cms_chains,  # chains already plotted
                    dict_fragplot,
                    dict_glob,
                    dict_chainout,
                    plot_out,
                    chain_out,
                )
            )
        return display_objects

    @staticmethod
    def get_checkmysequence_data(
        plot_out: str, glob_out: str, chain_out: str
    ) -> Tuple[dict, dict, dict]:
        dict_fragplot = {}
        if os.path.isfile(plot_out):
            with open(plot_out, "r") as j:
                dict_fragplot = json.load(j)
        dict_glob = {}
        if os.path.isfile(glob_out):
            with open(glob_out, "r") as j:
                dict_glob = json.load(j)
        dict_chainout = {}
        if os.path.isfile(chain_out):
            with open(chain_out, "r") as j:
                dict_chainout = json.load(j)
        return dict_fragplot, dict_glob, dict_chainout

    def get_per_residue_outlier_plot(
        self,
        outplot_data: dict,
        model: str,
        chain: str,
        outplot_json: str,
    ) -> Optional[ResultsDisplayObject]:
        subplot_args = []
        subplot_order = []
        subplot_titles = []
        list_yaxes_args = []
        data = []
        list_groups: List[int] = []
        list_outlier_names = []
        for outlier in outplot_data[model][chain]:
            outlier_name = outlier.split("/")[0]
            # skip only molprobity
            if (
                outlier_name == "molprobity"
                and "tortoize_ramaz" not in list_outlier_names
                and "tortoize_torsionz" not in list_outlier_names
            ):
                continue
            list_groups.append(self.outlier_plot_format[outlier_name][1])
            list_outlier_names.append(outlier_name)
        if not list_outlier_names:
            return None
        # number of subplots
        flag = ""
        x_range = None
        list_groups.sort()
        unique_group_num: List[int] = list(set(list_groups))
        for outlier in outplot_data[model][chain]:
            outlier_name = outlier.split("/")[0]
            if outlier_name not in list_outlier_names:
                continue
            list_x = outplot_data[model][chain][outlier][0]
            list_y = outplot_data[model][chain][outlier][1]
            if len(list_x) <= 10:
                continue  # skip small chains
            if outlier_name == "smoc":
                flag = self.check_smoc_distribution(list_y)

            data.append({"x": list_x, "y": list_y})  # add data for trace
            group_num = self.outlier_plot_format[outlier_name][1]
            row_index = unique_group_num.index(group_num) + 1
            subplot_order.append((row_index, 1))  # row,col for each trace
            plot_mode = self.outlier_plot_format[outlier_name][0]
            marker_style = {}
            if plot_mode == "markers":
                marker_style = {
                    "color": self.outlier_plot_format[outlier_name][2],
                    "symbol": self.outlier_plot_format[outlier_name][3],
                }
            subplot_args.append(
                {
                    "name": outlier,
                    "mode": plot_mode,
                    "marker": marker_style,
                    "line": {
                        "color": self.outlier_plot_format[outlier_name][2],
                        # "dash": "dashdot", "width": 2
                    },
                }
            )  # plot trace name (legend)
            if outlier_name != "molprobity":
                if x_range:
                    x_range = [
                        min(min(list_x), x_range[0]),
                        max(max(list_x), x_range[1]),
                    ]
                else:
                    x_range = [min(list_x), max(list_x)]
        if not data:
            return None
        # title and yaxis title for each subplot
        row_num = 0
        for group_num in unique_group_num:
            subplot_titles.append(self.outlier_plot_group_format[group_num][0])
            list_yaxes_args.append(
                {
                    "title_text": self.outlier_plot_group_format[group_num][1],
                    "row": row_num + 1,
                    "col": 1,
                    "gridcolor": "lightgrey",
                }
            )
            row_num += 1
        if x_range:
            xaxes_args = {
                "title_text": "Residue number",
                "gridcolor": "lightgrey",
                "range": x_range,
            }
        else:
            xaxes_args = {
                "title_text": "Residue number",
                "gridcolor": "lightgrey",
            }
        # subplots with multi_series
        disp_obj = create_results_display_object(
            "plotlyobj",
            data=data,
            plot_type="Scatter",
            subplot=True,
            multi_series=True,
            subplot_size=(len(unique_group_num), 1),
            subplot_order=subplot_order,
            subplot_args=subplot_args,
            layout_args={
                "title_text": "Residue outlier plot",
                "title_font_size": 12,
                "height": 350 * len(unique_group_num),
                "plot_bgcolor": "white",
            },
            xaxes_args=xaxes_args,
            yaxes_args=list_yaxes_args,
            make_subplot_args={
                "vertical_spacing": 0.12,
                "subplot_titles": subplot_titles,
                "shared_xaxes": True,
            },
            title=f"Outliers: chain {chain}",
            associated_data=[outplot_json],
            flag=flag,
        )
        # po.write_image(
        #     po.from_json(disp_obj.plotlyfig), chain + ".png", format="png"
        # )
        return disp_obj

    @staticmethod
    def get_checkmysequence_chain_results(
        list_cms_chains: List[str],  # chain to plot
        list_plotted_cms_chains: List[str],  # chains already plotted
        dict_fragplot: dict,
        dict_glob: dict,
        dict_chainout: dict,
        plot_out: str,
        chain_out: str,
    ) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        for cms_chain in list_cms_chains:
            if cms_chain in list_plotted_cms_chains:
                continue
            if dict_fragplot and cms_chain in dict_fragplot:
                display_objects.append(
                    ModelSequenceValidate.get_chain_plot(
                        dict_glob,
                        cms_chain,
                        dict_fragplot,
                        plot_out,
                    )
                )
                if cms_chain not in list_plotted_cms_chains:
                    list_plotted_cms_chains.append(cms_chain)
            if dict_chainout and cms_chain in dict_chainout:
                display_objects.append(
                    ModelSequenceValidate.get_chain_report(
                        chain_id=cms_chain,
                        dict_chainout=dict_chainout,
                        chain_out=chain_out,
                    )
                )
                if cms_chain not in list_plotted_cms_chains:
                    list_plotted_cms_chains.append(cms_chain)
        return display_objects

    def create_fsc_plot(
        self, dict_fsc_plot: dict, fsc_plot_data: Optional[str]
    ) -> ResultsDisplayObject:
        list_series_args = []
        data = []
        min_res = 800.0
        halfmap_input = False
        for metric in dict_fsc_plot:
            try:
                series_args = self.fsc_plot_format[metric]
                series_args["name"] = metric
            except KeyError:
                continue
            metric_suffix = metric.split("_")[-1]
            try:
                data.append(
                    {
                        "x": [
                            1 / res
                            for res in dict_fsc_plot["resolution_" + metric_suffix]
                        ],
                        "y": dict_fsc_plot[metric],
                    }
                )
            except KeyError:
                continue
            min_res = min(min(dict_fsc_plot["resolution_" + metric_suffix]), min_res)
            list_series_args.append(series_args)
            if "halfmap" in metric:
                halfmap_input = True
        # fsc 0.5
        line_05 = {
            "x": [0.0, 1.0 / min_res],
            "y": [0.5, 0.5],
        }
        data.append(line_05)
        list_series_args.append(
            {
                "name": "0.5",
                "mode": "lines",
                "line": {"color": "red", "dash": "dashdot", "width": 2},
            }
        )  # plot trace name (legend)
        if halfmap_input:
            # fsc 0.143
            line_0143 = {
                "x": [0.0, 1.0 / min_res],
                "y": [0.143, 0.143],
            }
            data.append(line_0143)
            list_series_args.append(
                {
                    "name": "0.143",
                    "mode": "lines",
                    "line": {"color": "black", "dash": "dashdot", "width": 2},
                }
            )  # plot trace name (legend)
        xaxes_args = {
            "gridcolor": "lightgrey",
            "title_text": "Spatial frequency (1/Å)",
        }
        yaxes_args = {
            "title_text": "FSC",
            "gridcolor": "lightgrey",
        }
        # multi_series plot
        disp_obj = create_results_display_object(
            "plotlyobj",
            data=data,
            plot_type="Scatter",  # all series scatter type
            subplot=False,
            multi_series=True,
            series_args=list_series_args,
            layout_args={
                "title_text": "model-map FSC plot",
                "title_font_size": 12,
                "plot_bgcolor": "white",
            },
            xaxes_args=xaxes_args,
            yaxes_args=yaxes_args,
            title="Model-map FSC",
            associated_data=[fsc_plot_data],
            flag="",
        )
        return disp_obj

    @staticmethod
    def get_percentile_from_emdb_json(
        metric: str,
        score: Union[float, int],
        resolution: Optional[float] = None,
    ) -> str:
        """Calculate the percentile rank of a score against structures from the EMDB.

        If a resolution is provided, the percentile is relative to structures in a range
        of similar resolutions, otherwise it is relative to all structures.

        Args:
            metric: The name of the metric, i.e. one of the keys in
                validation_distribution_histograms.json.
            score: The score to calculate a percentile for.
            resolution: The resolution of this structure (optional)

        Returns:
            A string with the percentile (0.0 = worst, 100.0 = best) and the relevant
            resolution range that was used (e.g. "73.4 (1.0-2.0 Å)", or an empty string
            if the percentile could not be determined.
        """
        emdb_json = get_job_script(
            "model_validation/validation_distribution_histograms.json"
        )
        if not os.path.isfile(emdb_json):
            return ""
        with open(emdb_json, "r") as j:
            dict_emdb_freq = json.load(j)
        freq = None
        bins = None
        res_range_str = ""
        freqs = dict_emdb_freq.get(metric)
        if freqs:
            if resolution:
                for res_range in freqs:
                    res_split = res_range.split("-")
                    if res_split[0] == "all":
                        continue
                    if len(res_split) == 1:
                        round_res = float(res_split[0])
                        if round(resolution, 0) == round_res:
                            freq, bins = freqs[res_range]
                            res_range_str = f" ({round_res-0.5}-{round_res+0.5} Å)"
                            break
                    else:
                        res_min = float(res_split[0])
                        res_max = float(res_split[-1])
                        if (
                            round(resolution, 0) >= res_min
                            and round(resolution, 0) <= res_max
                        ):
                            freq, bins = freqs[res_range]
                            if res_min == 1.0:  # set minimum resolution to 0
                                res_min = 0.5
                            if res_min == 7.0:
                                res_range_str = f" (>{res_min-0.5} Å)"
                            else:
                                res_range_str = f" ({res_min-0.5}-{res_max+0.5} Å)"
                            break
            else:
                freq, bins = freqs["all"]
                res_range_str = " (all structures)"
        if freq and bins:
            score_index = np.searchsorted(bins, score)
            percentile = np.sum(freq[: score_index + 1]) / np.sum(freq)
            return str(round(percentile * 100, 1)) + res_range_str
        return ""

    @staticmethod
    def check_bfact_distribution(
        list_bfactors: List[float],
    ) -> Tuple[str, Optional[int]]:
        flag = ""
        nbins = None
        if not list_bfactors:
            flag = (
                "Warning: Atomic b-factors could not be retrieved.\n"
                "Check input model data."
            )
            return flag, None
        skew = get_skewness(list_bfactors)
        if math.isnan(skew):
            flag = (
                "Warning: Atomic b-factors appear unrefined (single value).\n"
                "This will affect further validation results."
            )
            nbins = 3
        elif skew >= 3.5:  # severe right tailed
            flag = (
                "Warning: Residue b-factor distribution is severely skewed.\n"
                "A peak at low b-factors might indicate map over-sharpening or \n"
                "improper atomic b-factor refinement"
            )
        elif skew <= -2.5:  # severe left tailed
            flag = (
                "Warning: Residue b-factor distribution is severely skewed.\n"
                "This might indicate improper atomic b-factor refinement"
            )
        return flag, nbins

    @staticmethod
    def check_smoc_distribution(list_scores: List[float]) -> str:
        flag = ""
        skew = get_skewness(list_scores)
        if math.isnan(skew):
            flag = (
                "Warning: SMOC score has an unexpected distribution (identical values)."
                "\nThis is likely due to an issue with input data or score calculation."
            )
        elif skew >= 2.0:  # severe right tailed
            flag = (
                "Warning: SMOC score distribution is severely skewed.\n"
                "A peak at low scores might indicate major issues with model fit.\n"
                "The outliers detected using Z-scores may not be accurate with \n"
                "severely skewed distributions"
            )
        return flag
