#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
from typing import List, Dict, Any, Sequence

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
    MultiInputNodeJobOption,
    MultiStringJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_STRUCTUREFACTORS,
    NODE_MASK3D,
    NODE_LIGANDDESCRIPTION,
    NODE_LOGFILE,
    NODE_RESTRAINTS,
    NODE_EVALUATIONMETRIC,
)
from pipeliner.jobs.ccpem.molprobity_job import ValidateGeometry
from pipeliner.jobs.ccpem.model_validation_job import ModelValidate
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.utils import increment_file_basenames


class Refine(PipelinerJob):
    PROCESS_NAME = "refmac_servalcat.atomic_model_refine"
    OUT_DIR = "RefmacServalcat"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Refmac Servalcat"
        self.jobinfo.short_desc = "Atomic structure refinement"
        self.jobinfo.long_desc = (
            "Macromolecular refinement program. Servalcat is a wrapper for"
            " REFMAC5 that adds map sharpening, symmetry handling, difference"
            " maps and other functions. REFMAC5 is a program designed for"
            " REFinement of MACromolecular structures. It uses maximum"
            " likelihood and some elements of Bayesian statistics."
            " N.B. requires CCP4."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.programs = [
            ExternalProgram(
                command="servalcat", vers_com=["servalcat", "--version"], vers_lines=[0]
            ),
            ExternalProgram(command="gemmi", vers_com=["gemmi", "-V"], vers_lines=[0]),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://servalcat.readthedocs.io/"

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text=(
                "The input model to be refined. If your map was reconstructed with"
                " point group symmetry applied, we recommend that this model should"
                ' consist of a single asymmetric unit and you should set the "Point'
                ' group symmetry" option below.'
            ),
            is_required=True,
        )

        self.joboptions["input_ligand"] = MultiInputNodeJobOption(
            label="Input ligands",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            help_text=(
                "Ligand restraint file(s) for any ligands not included in the standard"
                " monomer library. If you have a novel ligand, you can generate"
                " suitable restraints files using the AceDRG job."
            ),
            is_required=False,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0.0,
            step_value=0.1,
            help_text=(
                "Resolution in angstroms. Servalcat will not consider resolutions"
                " beyond this limit during the refinement. Typically this should be the"
                " measured resolution from a half-map FSC curve, but in some cases it"
                " can be helpful to give a slightly higher or lower value."
            ),
            is_required=True,
        )

        self.joboptions["input_map1"] = InputNodeJobOption(
            label="Input map 1 (half map 1 or full map)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["half_map_refinement"] = BooleanJobOption(
            label="Half map refinement",
            default_value=True,
            help_text=(
                "Use half maps for refinement. If half maps are available this is"
                " strongly recommended. The alternative is to use a single full map."
            ),
        )
        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The second of a pair of half maps",
            deactivate_if=JobOptionCondition([("half_map_refinement", "=", False)]),
            required_if=JobOptionCondition([("half_map_refinement", "=", True)]),
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Mask for map normalisation",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text=(
                "A mask to be used for normalisation of the output difference maps."
                " If you have a suitable mask that defines the region of the map that"
                " contains your structure, you should provide it here. Note that this"
                ' is unrelated to the "Masked refinement" option below.'
            ),
        )

        # Refinement options

        self.joboptions["n_cycle"] = IntJobOption(
            label="Refinement cycles",
            default_value=20,
            suggested_min=1,
            hard_min=0,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of refinement cycles used. You should assess if the refinement"
                " has converged by examining the per-cycle output plots. If the"
                " refinement statistics are still changing in the final cycles, you"
                " should repeat the job and use more cycles to reach convergence."
            ),
            jobop_group="Refinement options",
            in_continue=True,
            is_required=True,
        )
        self.joboptions["point_group"] = StringJobOption(
            label="Point group",
            default_value="C1",
            help_text=(
                "Point group symmetry to apply during refinement (using RELION's point"
                " group convention). If this is set to anything other than C1,"
                " the input model must contain only a single asymmetric unit."
                " During refinement the model will be expanded by adding symmetry"
                " copies which will be constrained to be identical to each other."
                " Output models will be written for both the asymmetric unit and the"
                " full symmetry-expanded complex. If your input maps were reconstructed"
                " with point group symmetry applied, we recommend you provide a model"
                " for a single asymmetric unit and set this option to match the point"
                " group used for the map reconstruction. Alternatively you can provide"
                " a full model and leave this set to C1, but in this case the"
                " symmetry-related copies of the model will not necessarily remain"
                " identical."
            ),
            jobop_group="Refinement options",
            validation_regex="(C[1-9][0-9]*)|(D[2-9][0-9]*)|I[243]|O|T",
            regex_error_message="Symmetry must be in Cn, Dn, I[234], O or T format",
            is_required=True,
        )
        self.joboptions["nucleotide_restraints"] = BooleanJobOption(
            label="Nucleotide restraints",
            default_value=False,
            help_text=(
                "Use LIBG to generate restraints for nucleic acids. If your model"
                " contains nucleic acids you should generally set this to Yes."
            ),
            jobop_group="Refinement options",
        )
        self.joboptions["jelly_body"] = BooleanJobOption(
            label="Jelly body restraints",
            default_value=True,
            help_text=(
                "Use jelly body restraints to stabilise the refinement. This is usually"
                " a good idea when refining cryo-EM structures. See the Refmac"
                " documentation for more information."
            ),
            jobop_group="Refinement options",
        )

        # Validation options

        # Calculate clashes
        self.joboptions["calc_clash"] = BooleanJobOption(
            label="Calculate Molprobity clashscore?",
            default_value=True,
            help_text="Evaluate clashes with Molprobity",
            jobop_group="Additional validation",
            deactivate_if=JobOptionCondition([("calc_molprobity", "=", True)]),
        )
        # Geometry validation
        self.joboptions["calc_molprobity"] = BooleanJobOption(
            label="Comprehensive Molprobity run?",
            default_value=False,
            help_text="Evaluate model geometry and return Molprobity statistics",
            jobop_group="Additional validation",
        )

        # Advanced options

        self.joboptions["b_factor"] = FloatJobOption(
            label="Set model B-factors",
            default_value=40.0,
            suggested_min=0,
            help_text="Reset all atomic B-factors in input model to given value",
            jobop_group="Advanced options",
            is_required=True,
        )
        self.joboptions["use_hydrogens"] = MultipleChoiceJobOption(
            label="Include hydrogen atoms",
            choices=["All", "Yes", "No"],
            default_value_index=0,
            help_text=(
                "Control which hydrogen atoms are used in the refinement. If set to"
                ' All (the default), "riding" hydrogens will be added to the model.'
                " If set to Yes, hydrogen atoms in the input model will be used if"
                " present. If set to No, all hydrogens will be removed."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["write_hydrogens"] = BooleanJobOption(
            label="Write hydrogen atoms",
            default_value=False,
            help_text="If Yes, include hydrogen atoms in the output model",
            jobop_group="Advanced options",
            deactivate_if=JobOptionCondition([("use_hydrogens", "=", "No")]),
        )
        self.joboptions["auto_weight"] = BooleanJobOption(
            label="Auto weight",
            default_value=True,
            help_text=(
                "Use Servalcat to determine the relative weight of the experimental"
                " data vs stereochemical restraints. This usually gives good results,"
                " but if needed you can instead specify a weight below."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["weight"] = FloatJobOption(
            label="Weight",
            default_value=1.0,
            suggested_min=0.01,
            suggested_max=100,
            step_value=0.01,
            hard_min=0,
            help_text=(
                "Specific relative weight of experimental data vs stereochemical"
                " restraints. Smaller values result in stricter stereochemical"
                " restraints."
            ),
            jobop_group="Advanced options",
            deactivate_if=JobOptionCondition([("auto_weight", "=", True)]),
            required_if=JobOptionCondition([("auto_weight", "=", False)]),
        )
        self.joboptions["jelly_body_sigma"] = FloatJobOption(
            label="Jelly body sigma",
            default_value=0.02,
            hard_min=0.0,
            help_text="Sigma value for jelly body restraints",
            jobop_group="Advanced options",
            deactivate_if=JobOptionCondition([("jelly_body", "=", False)]),
            required_if=JobOptionCondition([("jelly_body", "=", True)]),
        )
        self.joboptions["jelly_body_dmax"] = FloatJobOption(
            label="Jelly body dmax",
            default_value=4.2,
            hard_min=0.0,
            help_text="Dmax value for jelly body restraints",
            jobop_group="Advanced options",
            deactivate_if=JobOptionCondition([("jelly_body", "=", False)]),
            required_if=JobOptionCondition([("jelly_body", "=", True)]),
        )
        self.joboptions["masked_refinement"] = BooleanJobOption(
            label="Masked refinement",
            default_value=True,
            help_text=(
                "Trim the map density around the input model and perform refinement"
                " with the resultant sub-volume. This makes the refinement faster with"
                " lower memory requirements and so it is almost always a good idea."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["mask_radius"] = FloatJobOption(
            label="Mask radius",
            default_value=3.0,
            hard_min=0,
            help_text="Distance around molecule the map should be cut",
            jobop_group="Advanced options",
            required_if=JobOptionCondition([("masked_refinement", "=", True)]),
            deactivate_if=JobOptionCondition([("masked_refinement", "=", False)]),
        )
        self.joboptions["auto_symmetry"] = MultipleChoiceJobOption(
            label="Auto symmetry",
            choices=["None", "Local", "Global"],
            default_value_index=1,
            help_text=(
                "Automatically detect and apply symmetry between subunits of the input"
                ' model. This option is passed to Servalcat\'s "--ncsr" option. If set'
                " to Local, Servalcat will search for similarity between local regions"
                " of the chains in the model, and if sufficient similarity is found,"
                " restraints are added to reduce any differences in the local"
                " inter-atomic distances. If there are multiple similar chains in the"
                " input model, using the Local setting is typically a good idea. It is"
                " usually harmless if the model does not contain any similar chains,"
                " so it is used as the default setting, but if you suspect it is"
                ' causing problems, turn it off by selecting "None". Since the'
                " addition of Servalcat's point group symmetry option, the Global"
                " setting is rarely needed. If necessary it can be used to add"
                " restraints that restrain the inter-atomic distances of whole chains"
                " to remain similar to each other. This would typically be helpful if"
                " the map was reconstructed with symmetry applied and you want to use a"
                " model containing multiple asymmetric units."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["keywords"] = MultiStringJobOption(
            label="Keywords",
            help_text=(
                "Refmac keywords, for control of advanced features. For details see the"
                " Refmac and Servalcat documentation."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["external_restraints"] = MultiInputNodeJobOption(
            label="External restraints files",
            node_type=NODE_RESTRAINTS,
            pattern=files_exts("Restraints", [".txt"]),
            help_text=(
                "Files containing lists of external restraints to be included in the"
                " refinement. These are typically generated by a dedicated tool such as"
                " ProSMART, though they can also be written by hand. (Advanced users"
                " should note that these files are simply lists of Refmac keywords, so"
                " this option can also be used to provide keyword files containing"
                " all kinds of keywords, not just restraints.)"
            ),
            jobop_group="Advanced options",
        )

        # Running options

        self.get_runtab_options(addtl_args=True)

        # Move the additional args option to the advanced options group
        self.joboptions["other_args"].jobop_group = "Advanced options"
        self.joboptions["other_args"].help_text = (
            "Additional arguments to be passed to the 'servalcat' command"
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        if self.joboptions["resolution"].get_number() <= 0.0:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["resolution"]],
                    message="Enter a valid map resolution",
                )
            )
        return errors

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "refined.pdb", NODE_ATOMCOORDS, ["refmac_servalcat", "refined"]
        )
        self.add_output_node(
            "refined.mmcif", NODE_ATOMCOORDS, ["refmac_servalcat", "refined"]
        )

        self.add_output_node(
            "diffmap.mtz",
            NODE_STRUCTUREFACTORS,
            ["refmac_servalcat", "sharpened_weighted_map", "difference_map"],
        )
        # refine_expanded model not produced with C1 symmetry
        if self.joboptions["point_group"].get_string() != "C1":
            self.add_output_node(
                "refined_expanded.pdb",
                NODE_ATOMCOORDS,
                ["refmac_servalcat", "refined", "expanded"],
            )
            self.add_output_node(
                "refined_expanded.mmcif",
                NODE_ATOMCOORDS,
                ["refmac_servalcat", "refined", "expanded"],
            )

        # Only present when half maps are provided
        if self.joboptions["input_half_map2"].get_string() != "":
            self.add_output_node(
                "sharpened_weighted.mrc",
                NODE_DENSITYMAP,
                ["refmac_servalcat", "sharpened_weighted_map"],
            )
        input_model = self.joboptions["input_model"].get_string()
        output_model = os.path.join(self.output_dir, "refined.pdb")
        dict_modelids = increment_file_basenames(
            [input_model, output_model], include_ext=False
        )
        calc_clash = self.joboptions["calc_clash"].get_boolean()
        calc_molprobity = self.joboptions["calc_molprobity"].get_boolean()
        if calc_molprobity:
            inp_molprobity_out = dict_modelids[input_model] + "_molprobity.out"
            self.add_output_node(
                inp_molprobity_out, NODE_LOGFILE, ["molprobity", "output"]
            )
            out_molprobity_out = dict_modelids[output_model] + "_molprobity.out"
            self.add_output_node(
                out_molprobity_out, NODE_LOGFILE, ["molprobity", "output"]
            )
        elif calc_clash:
            inp_clashscore_out = ValidateGeometry.get_clashscore_jsonfile_name(
                dict_modelids[input_model]
            )
            self.add_output_node(
                inp_clashscore_out, NODE_EVALUATIONMETRIC, ["clashscore", "output"]
            )
            out_clashscore_out = ValidateGeometry.get_clashscore_jsonfile_name(
                dict_modelids[output_model]
            )
            self.add_output_node(
                out_clashscore_out, NODE_EVALUATIONMETRIC, ["clashscore", "output"]
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        refine_cmd: List[str] = [self.jobinfo.programs[0].command]

        # Get parameters
        input_map1 = self.joboptions["input_map1"].get_string()
        input_half_map2 = self.joboptions["input_half_map2"].get_string()
        input_model = self.joboptions["input_model"].get_string()
        input_ligand = self.joboptions["input_ligand"].get_list()
        input_mask = self.joboptions["input_mask"].get_string()
        resolution = self.joboptions["resolution"].get_number()
        ncycle = self.joboptions["n_cycle"].get_number()
        do_masked_refinement = self.joboptions["masked_refinement"].get_boolean()
        mask_radius = self.joboptions["mask_radius"].get_number()
        b_factor = self.joboptions["b_factor"].get_number()
        use_hydrogens = self.joboptions["use_hydrogens"].get_string()
        write_hydrogens = self.joboptions["write_hydrogens"].get_boolean()
        ncsr = self.joboptions["auto_symmetry"].get_string()
        jelly_body = self.joboptions["jelly_body"].get_boolean()
        jelly_body_sigma = self.joboptions["jelly_body_sigma"].get_number()
        jelly_body_dmax = self.joboptions["jelly_body_dmax"].get_number()
        point_group = self.joboptions["point_group"].get_string()
        auto_weight = self.joboptions["auto_weight"].get_boolean()
        weight = self.joboptions["weight"].get_number()
        keywords = self.joboptions["keywords"].get_list()
        restraint_files = self.joboptions["external_restraints"].get_list()

        # Set command parameters
        refine_cmd += ["refine_spa", "--show_refmac_log", "--output_prefix", "refined"]

        # Half map refinement
        if input_half_map2 != "":
            refine_cmd += [
                "--halfmaps",
                os.path.relpath(input_map1, self.working_dir),
                os.path.relpath(input_half_map2, self.working_dir),
            ]
        # Full map refinement
        else:
            refine_cmd += ["--map", os.path.relpath(input_map1, self.working_dir)]

        refine_cmd += ["--model", os.path.relpath(input_model, self.working_dir)]

        if input_ligand:
            refine_cmd += ["--ligand"]
            for ligand in input_ligand:
                refine_cmd += [os.path.relpath(ligand, self.working_dir)]

        if input_mask != "":
            refine_cmd += [
                "--mask_for_fofc",
                os.path.relpath(input_mask, self.working_dir),
            ]
        refine_cmd += ["--resolution", str(resolution)]
        refine_cmd += ["--ncycle", str(ncycle)]
        refine_cmd += ["--bfactor", str(b_factor)]

        if do_masked_refinement:
            refine_cmd += ["--mask_radius", str(mask_radius)]

        # Servalcat --ncsr option supports "local" or "global" only.  Note lower case.
        if ncsr in ["Local", "Global"]:
            refine_cmd += ["--ncsr", ncsr.lower()]
        refine_cmd += ["--pg", point_group]
        if jelly_body:
            refine_cmd += [
                "--jellybody",
                "--jellybody_params",
                str(jelly_body_sigma),
                str(jelly_body_dmax),
            ]
        if not auto_weight:
            refine_cmd += ["--weight_auto_scale", str(weight)]

        refine_cmd += ["--hydrogen", use_hydrogens.lower()]
        if write_hydrogens:
            refine_cmd += ["--hout"]

        if keywords:
            refine_cmd += ["--keywords"]
            refine_cmd.extend(keywords)

        if restraint_files:
            refine_cmd += ["--keyword_file"]
            for restraint_file in restraint_files:
                refine_cmd += [os.path.relpath(restraint_file, self.working_dir)]

        # Add any additional arguments
        refine_cmd += self.parse_additional_args()

        commands: List[List[str]] = [refine_cmd]

        # Create sharpened and weight map if half maps used
        if input_half_map2 != "":
            sf2map_cmd: List[str] = [
                "gemmi",
                "sf2map",
                "diffmap.mtz",
                "sharpened_weighted.mrc",
            ]
            commands += [sf2map_cmd]

        # Create symmetry expanded PDB from mmcif
        # refine_expanded model not produced with C1 symmetry
        if point_group != "C1":
            expand_convert_cmd: List[str] = [
                "gemmi",
                "convert",
                "refined_expanded.mmcif",
                "refined_expanded.pdb",
            ]
            commands += [expand_convert_cmd]

        commands += self.get_molprobity_commands(input_model)

        return [PipelinerCommand(cmd) for cmd in commands]

    def get_molprobity_commands(self, input_model):
        # Ensure model IDs all use file paths relative to the project dir, even though
        # the relative paths from this job's working dir are different
        output_model = os.path.join(self.output_dir, "refined.pdb")
        dict_modelids = increment_file_basenames(
            [input_model, output_model], include_ext=False
        )

        calc_clash = self.joboptions["calc_clash"].get_boolean()
        calc_molprobity = self.joboptions["calc_molprobity"].get_boolean()

        mp_commands = []
        if calc_clash or calc_molprobity:
            input_model_pdb = input_model
            cif_ext = ValidateGeometry.get_cif_ext(input_model=input_model)
            if cif_ext:
                input_model_pdb = os.path.splitext(input_model)[0] + ".pdb"
                mp_commands.append(
                    ValidateGeometry.get_cifconvert_command(
                        os.path.relpath(input_model, self.working_dir),
                        os.path.relpath(input_model_pdb, self.working_dir),
                    )
                )
            if calc_molprobity:
                mp_commands += ValidateGeometry.get_molprobity_run_commands(
                    os.path.relpath(input_model_pdb, self.working_dir),
                    dict_modelids[input_model],
                )
                mp_commands += ValidateGeometry.get_molprobity_run_commands(
                    os.path.relpath(output_model, self.working_dir),
                    dict_modelids[output_model],
                )
                calc_clash = False  # manually disable as mutually exclusive
            elif calc_clash:
                mp_commands.append(
                    ValidateGeometry.get_clashscore_run_command(
                        os.path.relpath(input_model_pdb, self.working_dir),
                        dict_modelids[input_model],
                    )
                )
                mp_commands.append(
                    ValidateGeometry.get_clashscore_run_command(
                        os.path.relpath(output_model, self.working_dir),
                        dict_modelids[output_model],
                    )
                )
            # get molprobity results
            mp_commands.append(
                ValidateGeometry.get_molprobity_results_command(
                    dict_modelids[input_model],
                    run_global=calc_molprobity,
                    run_clashscore=calc_clash,
                )
            )
            mp_commands.append(
                ValidateGeometry.get_molprobity_results_command(
                    dict_modelids[output_model],
                    run_global=calc_molprobity,
                    run_clashscore=calc_clash,
                )
            )
        mp_commands.append(
            ModelValidate.get_bfact_command(
                os.path.relpath(output_model, self.working_dir),
                dict_modelids[output_model],
            )
        )
        return mp_commands

    def gather_metadata(self) -> Dict[str, Any]:
        metadata_dict: dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Rmsd from ideal" in line:
                metadata_dict["RMSDFromIdeal"] = {}
                metadata_dict["RMSDFromIdeal"]["BondLengths"] = float(
                    outlines[i + 1].split()[2]
                )
                metadata_dict["RMSDFromIdeal"]["BondAngles"] = float(
                    outlines[i + 2].split()[2]
                )
            if "Map-model FSCaverages" in line:
                metadata_dict["FSCAverage"] = float(outlines[i + 1].split()[2])
            if "ADP statistics" in line:
                metadata_dict["ADPStatistics"] = {}
                stats = []
                for j in range(i + 1, len(outlines)):
                    line_j = outlines[j]
                    if line_j.strip() == "":
                        break
                    if line_j.strip().startswith("Chain"):
                        stats.append(
                            [
                                "Chain " + line_j.split()[1],
                                int(line_j.split()[2].strip("(")),
                                float(line_j.split()[5]),
                                float(line_j.split()[7]),
                                float(line_j.split()[9]),
                            ]
                        )
                    if line_j.strip().startswith("All"):
                        stats.append(
                            [
                                "All",
                                int(line_j.split()[1].strip("(")),
                                float(line_j.split()[4]),
                                float(line_j.split()[6]),
                                float(line_j.split()[8]),
                            ]
                        )
                    metadata_dict["ADPStatistics"] = stats
            if "Weight used" in line:
                metadata_dict["Weight"] = float(line.split()[2])

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        # make the charts
        summary_file = os.path.join(self.output_dir, "shifted_refined_summary.json")
        with open(summary_file) as sf:
            sum_data = json.load(sf)
        bond = [x["rms_bond"] for x in sum_data["cycles"]]
        # rmszbond = [x["rmsz_bond"] for x in sum_data["cycles"]]
        rmsang = [x["rms_angle"] for x in sum_data["cycles"]]
        # rmszang = [x["rmsz_angle"] for x in sum_data["cycles"]]
        rmschi = [x["rms_chiral"] for x in sum_data["cycles"]]
        fscavg = [x["fsc_average"] for x in sum_data["cycles"]]
        weights = [
            x["actual_weight"] for x in sum_data["cycles"] if "actual_weight" in x
        ]

        input_model = self.joboptions["input_model"].get_string()
        output_model = os.path.join(self.output_dir, "refined.pdb")
        dict_modelids = increment_file_basenames(
            [input_model, output_model], include_ext=False
        )
        # statistics table (and molprobity)
        list_associated_data = [summary_file]
        labels = [
            "Metric",
            f"Initial ({dict_modelids[input_model]})",
            f"Final ({dict_modelids[output_model]})",
            "Expected",
        ]
        fscavg_data = ["FSC average", fscavg[0], fscavg[-1], "0.5-1.0 (>0.7 preferred)"]
        bond_data = ["RMS bond", bond[0], bond[-1], "0.005-0.01 (preferred)"]
        angles_data = ["RMS angle", rmsang[0], rmsang[-1], ""]
        chiral_data = ["RMS chiral", rmschi[0], rmschi[-1], ""]
        list_table_data = [fscavg_data, bond_data, angles_data, chiral_data]
        if weights:
            weights_data = ["Actual weight", weights[0], weights[-1], ""]
            list_table_data.append(weights_data)
        calc_clash = self.joboptions["calc_clash"].get_boolean()
        calc_molprobity = self.joboptions["calc_molprobity"].get_boolean()
        if calc_clash or calc_molprobity:
            list_mp_jsons = []
            mp_json_inp = os.path.join(
                self.output_dir,
                dict_modelids[input_model] + "_molprobity_summary.json",
            )
            if os.path.isfile(mp_json_inp):
                list_mp_jsons.append(mp_json_inp)
            mp_json_out = os.path.join(
                self.output_dir,
                dict_modelids[output_model] + "_molprobity_summary.json",
            )
            if os.path.isfile(mp_json_out):
                list_mp_jsons.append(mp_json_out)
            if list_mp_jsons:
                dict_mpdata = ValidateGeometry.merge_molprobity_summaries(list_mp_jsons)
                molprob_data = ValidateGeometry.set_molprobity_summary_data(dict_mpdata)
                list_table_data.extend(molprob_data)
                list_associated_data.extend(list_mp_jsons)
        ref_summary_table = create_results_display_object(
            "table",
            title="Refinement statistics",
            headers=labels,
            table_data=list_table_data,
            associated_data=list_associated_data,
        )
        display_objects.append(ref_summary_table)

        # Per-cycle plots
        xv = [list(range(len(bond)))]
        graph_angles = create_results_display_object(
            "graph",
            xvalues=xv,
            yvalues=[rmsang],
            title="Angles",
            associated_data=[summary_file],
            data_series_labels=["rms_angle"],
            xaxis_label="Cycle",
            yaxis_label="RMSD",
        )
        graph_bonds = create_results_display_object(
            "graph",
            xvalues=xv,
            yvalues=[bond],
            title="Bonds",
            associated_data=[summary_file],
            data_series_labels=["rms_bond"],
            xaxis_label="Cycle",
            yaxis_label="RMSD",
        )
        graph_chi = create_results_display_object(
            "graph",
            xvalues=xv,
            yvalues=[rmschi],
            title="Chiral volumes",
            associated_data=[summary_file],
            data_series_labels=["rms_chi"],
            xaxis_label="Cycle",
            yaxis_label="RMSD",
        )
        graph_fscavg = create_results_display_object(
            "graph",
            xvalues=xv,
            yvalues=[fscavg],
            title="FSC average",
            associated_data=[summary_file],
            data_series_labels=["fsc_average"],
            xaxis_label="Cycle",
            yaxis_label="FSC",
        )
        # fsc
        fsc_data = os.path.join(self.output_dir, "refined_fsc.log")
        fsc, res = [], []
        fsc_index = None
        with open(fsc_data) as fscd:
            fsc_lines = fscd.readlines()
        for line in fsc_lines:
            ls = line.split()
            # check for column name
            if ls and ls[0] == "$$" and "FSC(full,model)" in ls:
                fsc_index = ls.index("FSC(full,model)") - 1  # $$ at 0
            if line[:2] == "0." and fsc_index:
                res.append(float(ls[0]))
                fsc.append(float(ls[fsc_index]))

        fsc_graph = create_results_display_object(
            "graph",
            xvalues=[res],
            yvalues=[fsc],
            title="Map-Model Fourier Shell Correlation",
            associated_data=[fsc_data],
            xaxis_label="1/resolution<sup>2</sup> (Å<sup>−2</sup>)",
            yaxis_label="FSC",
            data_series_labels=[""],
            xrange=[min(0.0, min(res)), max(res)],
            yrange=[min(-0.05, min(fsc)), max(1.05, max(fsc))],
        )

        # map-model thumbnail and display
        if self.joboptions["input_half_map2"].get_string() != "":
            the_map = os.path.join(self.output_dir, "sharpened_weighted.mrc")
        else:
            the_map = self.joboptions["input_map1"].get_string()

        mm_dispobj = make_map_model_thumb_and_display(
            maps=[the_map],
            maps_opacity=[0.5],
            models=[output_model],
            outputdir=self.output_dir,
        )
        display_objects.extend([mm_dispobj, fsc_graph, graph_fscavg])
        # Atomic B-factor distribution
        bfact_json = os.path.join(
            self.output_dir, dict_modelids[output_model] + "_residue_bfactors.json"
        )
        if os.path.isfile(bfact_json):
            list_bfactors = ModelValidate.get_bfactor_list(bfact_json)
            flag, nbins = ModelValidate.check_bfact_distribution(
                list_bfactors=list_bfactors
            )
            plotlyhist_obj = create_results_display_object(
                "plotlyhistogram",
                data={"Per-residue B-factors (Å²)": list_bfactors},
                labels={"Per-residue B-factors (Å²)": "Per-residue B-factors (Å²)"},
                x="Per-residue B-factors (Å²)",
                title="B-factor distribution",
                associated_data=[bfact_json],
                flag=flag,
                nbins=nbins,
                start_collapsed=False,
            )
            display_objects.append(plotlyhist_obj)

        display_objects.extend([graph_angles, graph_bonds, graph_chi])
        return display_objects
