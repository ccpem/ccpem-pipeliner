#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import glob
import json
from pathlib import Path
from typing import List, Sequence, Dict, Union
import shutil
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    MultipleChoiceJobOption,
    files_exts,
    JobOptionValidationResult,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.node_factory import create_node
from pipeliner.display_tools import ResultsDisplayObject, create_results_display_object
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_PROCESSDATA,
    NODE_RIGIDBODIES,
    NODE_RESTRAINTS,
)
from pipeliner.utils import get_job_script


class RigidBodySplit(PipelinerJob):
    PROCESS_NAME = "ribfind.atomic_model_split.rigid_body"
    OUT_DIR = "RIBFIND"
    ribfind_program = ExternalProgram(
        command="ribfind", vers_com=["ribfind", "--version"], vers_lines=[0]
    )

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RIBFIND"
        self.jobinfo.short_desc = "Rigid body identification with RIBFIND2"
        self.jobinfo.long_desc = (
            "Clustering secondary structures into rigid bodies with RIBFIND2. "
            "Currently, only protein and RNA components are supported and they are "
            "clustered separately, i.e. rigid bodies with both RNA and protein are "
            "not identified. It uses DSSP and RNAView to detect protein and RNA "
            "secondary structures respectively."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            self.ribfind_program,
            ExternalProgram(command="gemmi"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Malhotra S",
                    "Mulvaney T",
                    "Cragnolini T",
                    "Sidhu H",
                    "Joseph AP",
                    "Beton J",
                    "Topf M",
                ],
                title=(
                    "RIBFIND2: Identifying rigid bodies in protein and nucleic acid "
                    "structures"
                ),
                journal="Nucleic Acid Research",
                year="2023",
                volume="51",
                issue="18",
                pages="9567-9575",
                doi="10.1093/nar/gkad721",
            ),
        ]
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="The input model to be fitted/refined",
            is_required=True,
        )
        self.joboptions["structure_content"] = MultipleChoiceJobOption(
            label="Structure content",
            choices=["protein", "RNA", "protein+RNA"],
            default_value_index=0,
            help_text=(
                "If 'RNA' or 'protein+RNA', RIBFIND calculates RNA secondary "
                "structures using RNAview. The protein components if any "
                "will be processed separately with DSSP."
            ),
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # RIBFIND2 v2.*
        ribfind_version = self.ribfind_program.get_version()
        if ribfind_version and (
            len(ribfind_version.split()) != 2 or ribfind_version.split()[1][1] in ["1"]
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_model"]],
                    message=(
                        "RIBFIND version 2.* is not available. This job requires "
                        "RIBFIND version 2 installed. "
                    ),
                )
            )
        # RNAView
        structure_content = self.joboptions["structure_content"].get_string()
        if structure_content in ["RNA", "protein+RNA"]:
            if not shutil.which("rnaview"):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["structure_content"]],
                        message=(
                            "'rnaview' is not available in the system path, "
                            "check that RNAView is installed. "
                        ),
                    )
                )
        # DSSP
        if structure_content in ["protein", "protein+RNA"] and not shutil.which(
            "mkdssp"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["structure_content"]],
                    message=(
                        "RIBFIND2 cannot be run as 'mkdssp' is not available in the "
                        "system path, check that CCP4 is installed. "
                        "RIBFIND2 uses this to identify protein secondary structures. "
                    ),
                )
            )
        return errors

    def create_output_nodes(self) -> None:
        input_model = self.joboptions["input_model"].get_string()
        model_id = Path(input_model).stem
        model_no_wat_lig = model_id + "_no_wat_lig.pdb"
        structure_content = self.joboptions["structure_content"].get_string()
        if structure_content in ["protein", "protein+RNA"]:
            self.add_output_node(
                self.get_dssp_outname(),
                NODE_PROCESSDATA,
                ["mkdssp", "secondary_structure"],
            )
        # RNA fitting
        if structure_content in ["RNA", "protein+RNA"]:
            self.add_output_node(
                self.get_rnaview_outname(model_no_wat_lig),
                NODE_PROCESSDATA,
                ["rnaview", "secondary_structure"],
            )

    def create_post_run_output_nodes(self) -> None:
        input_model = self.joboptions["input_model"].get_string()
        model_id = Path(input_model).stem
        ribfind_dir = "ribfind_" + model_id
        # Find all rigid body text files
        list_rigidbody_files = []
        output_txts = self.get_ribfind_output_list(ribfind_dir)
        for txtfile in sorted(output_txts):
            self.output_nodes.append(
                create_node(txtfile, NODE_RIGIDBODIES, ["ribfind", "rigid_body"])
            )
            list_rigidbody_files.append(txtfile)
        self.create_output_json(ribfind_dir, list_rigidbody_files)

    def create_output_json(self, ribfind_dir, list_rigidbody_files) -> None:
        dict_ribfind_outputs: Dict[str, Union[str, List[str]]] = {}
        dict_ribfind_outputs["output_dir"] = ribfind_dir
        dict_ribfind_outputs["rigid_body_files"] = list_rigidbody_files
        structure_content = self.joboptions["structure_content"].get_string()
        if structure_content == "protein+RNA":
            components = ["protein", "RNA"]
        else:
            components = [structure_content]
        dict_ribfind_outputs["components"] = components
        # create a json file with output details
        ribfind_output_json = os.path.join(
            self.output_dir, ribfind_dir + "_outputs.json"
        )
        with open(ribfind_output_json, "w") as j:
            json.dump(dict_ribfind_outputs, j)
        self.output_nodes.append(
            create_node(
                ribfind_output_json, NODE_RESTRAINTS, ["ribfind", "output_files"]
            )
        )

    def get_ribfind_output_list(self, ribfind_dir: str) -> List[str]:
        # Find all rigid body text files
        output_txts = []
        structure_content = self.joboptions["structure_content"].get_string()
        if structure_content in ["protein", "protein+RNA"]:
            output_txts += glob.glob(
                os.path.join(
                    self.output_dir, ribfind_dir, "protein", "rigid_body_*.*.txt"
                )
            )
        if structure_content in ["RNA", "protein+RNA"]:
            output_txts += glob.glob(
                os.path.join(self.output_dir, ribfind_dir, "rna", "rigid_body_*.*.txt")
            )
        return output_txts

    @staticmethod
    def get_dssp_outname() -> str:
        return "ribfind.dssp"

    @staticmethod
    def get_rnaview_outname(input_model: str) -> str:
        rnaml_out = os.path.basename(input_model) + ".xml"
        return rnaml_out

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get inputs
        input_model_orig = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model_orig, self.working_dir)
        model_id = Path(input_model).stem
        # TODO : is removing water/ligands or convert to PDB necessary?
        # Remove waters and ligands from input model with Gemmi convert
        # also ensure .pdb is generated
        # add HEADER remark at the start of pdb if not there
        model_no_wat_lig_rel = self.get_processed_model_name(model_id=model_id)
        model_process_command = self.get_process_model_command(
            input_model=input_model, model_id=model_id
        )
        commands += [model_process_command]
        # Ribfind
        ribfind_dir = "ribfind_" + model_id
        ribfind_command = [
            "ribfind",
            "--model",
            model_no_wat_lig_rel,
            "--output-dir",
            ribfind_dir,
        ]
        # protein
        structure_content = self.joboptions["structure_content"].get_string()
        if structure_content in ["protein", "protein+RNA"]:
            # DSSP
            dssp_out = RigidBodySplit.get_dssp_outname()
            dssp_command = self.get_dssp_command(model_no_wat_lig_rel)
            commands += [dssp_command]
            ribfind_command += [
                "--dssp",
                dssp_out,
            ]
        # RNA
        if structure_content in ["RNA", "protein+RNA"]:
            # RNAview
            rnaml_out = RigidBodySplit.get_rnaview_outname(model_no_wat_lig_rel)
            rnaview_command = self.get_rnaview_command(model_no_wat_lig_rel)
            commands += [rnaview_command]
            ribfind_command += [
                "--rnaml",
                rnaml_out,
            ]
        commands += [ribfind_command]
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_processed_model_name(model_id: str) -> str:
        return model_id + "_no_wat_lig.pdb"

    @staticmethod
    def get_process_model_command(input_model: str, model_id: str) -> List[str]:
        model_process_command = [
            "python3",
            get_job_script("ribfind/process_input_model.py"),
            "-p",
            input_model,
            "-mid",
            model_id,
        ]
        return model_process_command

    @staticmethod
    def get_dssp_command(input_model: str) -> List[str]:
        dssp_out = RigidBodySplit.get_dssp_outname()
        dssp = "mkdssp"
        dssp_command = [dssp, input_model, dssp_out]
        return dssp_command

    @staticmethod
    def get_rnaview_command(input_model):
        rnaview_command = ["rnaview", "-p", input_model]
        return rnaview_command

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_model = self.joboptions["input_model"].get_string()
        model_id = Path(input_model).stem
        ribfind_dir = "ribfind_" + model_id
        # Find all rigid body text files
        output_txts = self.get_ribfind_output_list(ribfind_dir)
        for txtfile in sorted(output_txts):
            display_objects.append(
                create_results_display_object("textfile", file_path=txtfile)
            )
        return display_objects
