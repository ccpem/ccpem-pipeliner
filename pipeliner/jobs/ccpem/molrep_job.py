#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
import logging
from typing import List, Dict, Any, Sequence
from ccpem_utils.map.mrcfile_utils import check_origin_zero
from collections import OrderedDict
from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    MultipleChoiceJobOption,
    InputNodeJobOption,
    IntJobOption,
    files_exts,
    JobOptionValidationResult,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject

logger = logging.getLogger(__name__)


class MolrepJob(PipelinerJob):
    """Molecular replacement with Molrep.
    Covers standard rot fn + trans fn, as well as SAPTF. These represent two methods for
    locating search models in the cryoEM map, but have the same input and output nodes
    and so are considered in the same class.
    """

    PROCESS_NAME = "molrep.atomic_model_fit"
    OUT_DIR = "Molrep"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Molrep"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Agnel Joseph"
        self.jobinfo.short_desc = "Fit model in map with Molrep"
        self.jobinfo.long_desc = (
            "Molrep fits one or more atomic models into a cryoEM map by the technique"
            " of Molecular Replacement. The spherically averaged phased translation"
            " function locates the model in the cryoEM map using a spherically averaged"
            " calculated map, then determines its optimal orientation and refines the"
            " position. The rotation translation function is the more traditional"
            " molecular replacement method. The latter is generally faster but less"
            " sensitive."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/molrep.html"
        self.jobinfo.programs = [
            ExternalProgram("molrep"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Vagin A", "Teplyakov A"],
                title="Molecular replacement with MOLREP",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2010",
                volume="66",
                issue="1",
                pages="22-25",
                doi="10.1107/S0907444909042589",
            ),
            Ref(
                authors=["Vagin AA", "Isupov MN"],
                title=(
                    "Spherically averaged phased translation function and"
                    " its application to the search for molecules and fragments"
                    " in electron-density maps"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2001",
                volume="57",
                issue="10",
                pages="1451-1456",
                doi="10.1107/s0907444901012409",
            ),
        ]

        self.score_comments = {
            "TF/sg": (
                "Z-score of Translation peaks. < 6 is unlikely at high resolutions."
                " > 100 is highly likely to be a correct fit."
            ),
            "Score": (
                "Final_CC x Packing_Coef. < 0.1 is unlikely to be correct,"
                " < 0.2 less likely to be correct"
            ),
            "Packing_Coef": (
                "Overlap between monomers. 1.00 indicates no overlap,"
                " 0 indicates full overlap"
            ),
            "Final_CC": "Final correlation coefficient between search model and map",
            "Nmon": "Monomer number",
        }
        self.joboptions["mode"] = MultipleChoiceJobOption(
            label="Mode",
            choices=["SAPTF", "RTF"],
            default_value_index=1,
            help_text=(
                "Choice of search methods to use. RTF is generally faster but"
                " less sensitive than SAPTF."
            ),
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input cryoEM map into which models will be placed",
            is_required=True,
        )

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="The input model to be fitted into map",
            is_required=True,
        )

        self.joboptions["copies"] = IntJobOption(
            label="Copies to find",
            default_value=1,
            suggested_min=1,
            hard_min=0,
            suggested_max=20,
            step_value=1,
            help_text="Number of copies of search model to find",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["subunits"] = IntJobOption(
            label="Subunits in search model",
            default_value=1,
            suggested_min=1,
            hard_min=0,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of identical monomers present in search model."
                " Specify for multimers"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["fixed_model"] = InputNodeJobOption(
            label="Fixed model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text=(
                "Fixed model file (pdb format). This model will be fixed in"
                " its position and orientation during the search."
            ),
            is_required=False,
        )

        self.joboptions["rotation_peaks"] = IntJobOption(
            label="Rotation peaks",
            default_value=1,
            suggested_min=1,
            hard_min=0,
            suggested_max=200,
            step_value=1,
            help_text=(
                "The number of peaks from the rotation function to be used."
                " Increase this while searching multiple copies or with noisy or low"
                " resolution maps"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["translation_peaks"] = IntJobOption(
            label="Translation peaks",
            default_value=1,
            suggested_min=1,
            hard_min=0,
            suggested_max=50,
            step_value=1,
            help_text=(
                "The number of peaks from the translation function to be used."
                " Increase this while searching noisy or low resolution maps"
            ),
            in_continue=True,
            is_required=True,
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        if (
            self.joboptions["copies"].get_number()
            > self.joboptions["rotation_peaks"].get_number()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["rotation_peaks"],
                        self.joboptions["copies"],
                    ],
                    message=(
                        "The number of rotation peaks should at least be equal to the"
                        " number of copies to find"
                    ),
                )
            )
        input_map = self.joboptions["input_map"].get_string()
        if os.path.isfile(input_map) and not check_origin_zero(input_map):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["input_map"]],
                    message=(
                        "Input map has non-zero origin record. Molrep currently "
                        "ignores this. The output model may be shifted with respect to "
                        "the original map origin. Use the 'Model Shift' job to shift "
                        "the fitted model back to this position."
                    ),
                )
            )
        return errors

    def create_output_nodes(self) -> None:
        self.add_output_node("molrep.pdb", NODE_ATOMCOORDS, ["molrep"])
        self.add_output_node("molrep.cif", NODE_ATOMCOORDS, ["molrep"])

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # CCP-EM gui creates:
        #    molrep -m 5me2_a.pdb -f emd_3488.map -i << eof
        #    _NMON 4
        #    _PRF S
        #    NCSM 1
        #    stick n
        #    eof
        input_model_file = self.joboptions["input_model"].get_string()
        input_map_file = self.joboptions["input_map"].get_string()

        fixed_model_file = self.joboptions["fixed_model"].get_string()

        # Prepare for Molrep, link any .mrc maps to .map ext., .ent models to .pdb and
        #  write keywords file
        tools_script = get_job_script("molrep/molrep_tools.py")
        prep_command = ["python3", tools_script]

        # Convert cif inputs to pdb as Molrep does not accept cif format
        gemmi_convert_model = None
        if input_model_file.endswith("cif"):
            input_model_corrected = Path(input_model_file).with_suffix(".pdb").name
            gemmi_convert_model = [
                "gemmi",
                "convert",
                os.path.relpath(input_model_file, self.working_dir),
                input_model_corrected,
            ]
        elif input_model_file.endswith("ent"):
            input_model_link_name = Path(input_model_file).with_suffix(".pdb").name
            prep_command.extend(
                [
                    "--symlink_ent_pdb",
                    os.path.relpath(input_model_file, self.working_dir),
                ]
            )
            input_model_corrected = input_model_link_name
        else:
            input_model_corrected = os.path.relpath(input_model_file, self.working_dir)

        gemmi_convert_fixed_model = None
        prep_command_fixed_model = None
        if fixed_model_file.endswith("cif"):
            fixed_model_corrected = Path(fixed_model_file).with_suffix(".pdb").name
            gemmi_convert_fixed_model = [
                "gemmi",
                "convert",
                os.path.relpath(fixed_model_file, self.working_dir),
                fixed_model_corrected,
            ]
        elif fixed_model_file:
            if fixed_model_file.endswith("ent"):
                prep_command_fixed_model = ["python3", tools_script]
                fixed_model_link_name = Path(fixed_model_file).with_suffix(".pdb").name
                prep_command_fixed_model.extend(
                    [
                        "--symlink_ent_pdb",
                        os.path.relpath(fixed_model_file, self.working_dir),
                    ]
                )
                fixed_model_corrected = fixed_model_link_name
            else:
                fixed_model_corrected = os.path.relpath(
                    fixed_model_file, self.working_dir
                )
        else:
            fixed_model_corrected = fixed_model_file

        molrep_command = ["molrep", "-m", input_model_corrected]

        if input_map_file.endswith(".mrc"):
            input_map_link_name = Path(input_map_file).with_suffix(".map").name
            prep_command.extend(
                ["--symlink_mrc_map", os.path.relpath(input_map_file, self.working_dir)]
            )
            molrep_command.extend(["-f", input_map_link_name])
        else:
            molrep_command.extend(
                ["-f", os.path.relpath(input_map_file, self.working_dir)]
            )

        # Add keywords
        keywords = ""
        if self.joboptions["mode"].get_string() == "SAPTF":
            keywords += " '_PRF S'"
        ncopies = self.joboptions["copies"].get_number()
        keywords += "_NMON {}\n".format(str(ncopies))
        nsubunits = self.joboptions["subunits"].get_number()
        keywords += "NCSM {}\n".format(str(nsubunits))
        keywords += "stick\n"
        rotation_peaks = self.joboptions["rotation_peaks"].get_number()
        keywords += "np {}\n".format(str(rotation_peaks))
        translation_peaks = self.joboptions["translation_peaks"].get_number()
        keywords += "npt {}\n".format(str(translation_peaks))
        #
        prep_command.extend(["--keywords", keywords])
        molrep_command.extend(["-k", "keywords.txt"])

        if fixed_model_corrected != "":
            molrep_command.extend(["-mx", fixed_model_corrected])

        # Fix Molrep PDB output
        fix_output_command = ["python3", tools_script, "--fix_pdb"]

        # Convert pdb to cif
        gemmi_convert_output = [
            "gemmi",
            "convert",
            "molrep.pdb",
            "molrep.cif",
        ]

        # Set commands list
        commands = []
        if gemmi_convert_model is not None:
            commands.append(gemmi_convert_model)
        if gemmi_convert_fixed_model is not None:
            commands.append(gemmi_convert_fixed_model)
        if prep_command_fixed_model:
            commands.append(prep_command_fixed_model)
        commands.extend(
            [
                prep_command,
                molrep_command,
                fix_output_command,
                gemmi_convert_output,
            ]
        )
        return [PipelinerCommand(x) for x in commands]

    def gather_metadata(self) -> Dict[str, Any]:
        metadata_dict = self.parse_molrep_log()
        return metadata_dict

    def parse_molrep_log(self) -> Dict[str, Any]:
        molrep_scores: Dict[str, Any] = OrderedDict()
        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()
        packing_coef = None
        final_cc = None
        summary_lines = False
        for i, line in enumerate(outlines):
            if "--- Summary (V2) ---" in line.strip():
                summary_lines = True
            elif "Checking job completion" in line.strip():
                summary_lines = False
            if summary_lines:
                lstrip = line.strip()
                lsplit = lstrip.split()
                if len(lsplit) < 3:
                    continue
                # Nmon RF  TF   theta    phi     chi   tx     ty     tz
                # TF/sg  wRfac  Score
                if lstrip.startswith("Nmon"):
                    score_names = lsplit
                    data_line = outlines[i + 1].strip().split()
                    if len(score_names) == len(data_line):
                        # monomer number
                        molrep_scores[data_line[0]] = {}
                        for sc_n in range(len(score_names)):
                            molrep_scores[data_line[0]][score_names[sc_n]] = data_line[
                                sc_n
                            ]
                        molrep_scores[data_line[0]]["Packing_Coef"] = packing_coef
                        molrep_scores[data_line[0]]["Final_CC"] = final_cc
                elif lstrip.startswith("Packing_Coef"):
                    # Packing_Coef =   1.0000
                    if len(lsplit) == 3:
                        packing_coef = lsplit[-1]
                elif lsplit[0] == "Final" and lsplit[1] == "CC":
                    # Final CC     =   0.1520
                    if len(lsplit) == 4:
                        final_cc = lsplit[-1]

        return molrep_scores

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        # create scores table
        logfile = os.path.join(self.output_dir, "run.out")
        scores_dict = self.parse_molrep_log()
        table_headers = ["Nmon", "TF/sg", "Packing_Coef", "Final_CC", "Score"]
        header_tooltips = [self.score_comments[h] for h in table_headers]
        list_all_scores = []
        for nmon in scores_dict:
            list_mon_scores = []
            for sc in table_headers:
                try:
                    list_mon_scores.append(scores_dict[nmon][sc])
                except KeyError:
                    logger.exception(f"Score not found in log file: {sc}")
                    list_mon_scores.append(None)
            if not all(x is None for x in list_mon_scores):
                list_all_scores.append(list_mon_scores)
        if list_all_scores:
            # expected values
            display_objects.append(
                create_results_display_object(
                    "table",
                    title="Fitted monomer scores",
                    headers=table_headers,
                    header_tooltips=header_tooltips,
                    table_data=list_all_scores,
                    associated_data=[logfile],
                )
            )
        # map model display
        models = [os.path.join(self.output_dir, "molrep.pdb")]
        fixed_model = self.joboptions["fixed_model"].get_string()
        if fixed_model != "":
            models.append(fixed_model)
        input_map = self.joboptions["input_map"].get_string()
        maps = [input_map]
        flag = ""
        if not check_origin_zero(input_map):
            flag = (
                "Input map has non-zero origin record. Molrep currently "
                "ignores this. The output model may be shifted with respect to "
                "the original map origin. Use the 'Model Shift' job to shift "
                "the fitted model back."
            )
        display_objects.append(
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=[0.5],
                models=models,
                title="Molrep docked model",
                outputdir=self.output_dir,
                flag=flag,
            )
        )

        return display_objects
