#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import List, Sequence
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    StringJobOption,
    JobOptionValidationResult,
    files_exts,
    JobOptionCondition,
    InputNodeJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.utils import get_job_script
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_LIGANDDESCRIPTION
from pipeliner.results_display_objects import ResultsDisplayObject


class AceDRGCreateLigand(PipelinerJob):
    PROCESS_NAME = "acedrg.create_ligand"
    OUT_DIR = "AceDRG"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "AceDRG"
        self.jobinfo.short_desc = "Create ligand dictionary"
        self.jobinfo.long_desc = "A stereo-chemical description generator for ligands."
        acedrg = ExternalProgram(
            command="acedrg", vers_com=["acedrg", "--version"], vers_lines=[8, 10, 11]
        )
        self.jobinfo.programs = [acedrg]
        self.jobinfo.version = "246"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Long F",
                    "Nicholls RA",
                    "Emsley P",
                    "GraZulis S",
                    "Merkys A",
                    "Vaitkus A",
                    "Murshudov GN",
                ],
                title="ACEDRG: A stereo-chemical description generator for ligands",
                journal="Acta Cryst. D",
                year="2017",
                volume="73",
                issue="1",
                pages="112-122",
                doi="10.1107/S2059798317000067",
            )
        ]
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/acedrg/acedrg.html"
        )

        self.joboptions["smiles_string"] = StringJobOption(
            label="SMILES string",
            default_value="",
            help_text="Simplified Molecular Input Line Entry System text",
            is_required=False,
            deactivate_if=JobOptionCondition(
                [
                    ("smiles_file", "!=", ""),
                    ("mol_file", "!=", ""),
                    ("mmcif_file", "!=", ""),
                ],
            ),
        )
        self.joboptions["smiles_file"] = InputNodeJobOption(
            label="OR: SMILES file",
            default_value="",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("SMILES", [".smi"]),
            help_text="Simplified Molecular Input Line Entry System text file",
            is_required=False,
            deactivate_if=JobOptionCondition(
                [
                    ("smiles_string", "!=", ""),
                    ("mol_file", "!=", ""),
                    ("mmcif_file", "!=", ""),
                ],
            ),
            node_kwds=["smiles"],
        )
        self.joboptions["mol_file"] = InputNodeJobOption(
            label="OR: MOL file",
            default_value="",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("MDL", [".mol"]),
            help_text="Input MDL Mol file",
            is_required=False,
            deactivate_if=JobOptionCondition(
                [
                    ("smiles_string", "!=", ""),
                    ("smiles_file", "!=", ""),
                    ("mmcif_file", "!=", ""),
                ],
            ),
        )
        self.joboptions["mmcif_file"] = InputNodeJobOption(
            label="OR: mmCIF file",
            default_value="",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("mmCIF", [".cif", ".mmcif"]),
            help_text="Input mmCIF file",
            is_required=False,
            deactivate_if=JobOptionCondition(
                [
                    ("smiles_string", "!=", ""),
                    ("smiles_file", "!=", ""),
                    ("mol_file", "!=", ""),
                ],
            ),
        )
        self.joboptions["monomer_code"] = StringJobOption(
            label="Monomer Code",
            default_value="DRG",
            help_text="Three-letter code for output monomer (e.g. GLU)",
            is_required=False,
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        """
        Check only one restraint definition is provided
        """
        smiles_string = self.joboptions["smiles_string"].get_string()
        smiles_file = self.joboptions["smiles_file"].get_string()
        mol_file = self.joboptions["mol_file"].get_string()
        mmcif_file = self.joboptions["mmcif_file"].get_string()
        to_check = [smiles_string, smiles_file, mol_file, mmcif_file]
        errs = []
        if to_check.count("") != 3 and any(to_check):
            errs.append(
                JobOptionValidationResult(
                    "error",
                    [
                        self.joboptions["smiles_string"],
                        self.joboptions["smiles_file"],
                        self.joboptions["mol_file"],
                        self.joboptions["mmcif_file"],
                    ],
                    "Please input only one ligand description",
                ),
            )
        if not any([smiles_string, smiles_file, mol_file, mmcif_file]):
            errs.append(
                JobOptionValidationResult(
                    "error",
                    [
                        self.joboptions["smiles_string"],
                        self.joboptions["smiles_file"],
                        self.joboptions["mol_file"],
                        self.joboptions["mmcif_file"],
                    ],
                    "Select at least one input",
                ),
            )

        return errs

    @staticmethod
    def get_cifconvert_command(model_cif: str, model_mmcif: str) -> List[str]:
        gemmi_convert_model: List[str] = [
            "gemmi",
            "convert",
            "--from=chemcomp",
            "--to=mmcif",
            model_cif,
            model_mmcif,
        ]
        return gemmi_convert_model

    def create_output_nodes(self) -> None:
        monomer_code = self.joboptions["monomer_code"].get_string()
        # Set AceDRG restraints output
        # <monomer_code>_acedrg.cif
        self.add_output_node(
            f"{monomer_code}_acedrg.cif",
            NODE_LIGANDDESCRIPTION,
            ["acedrg", "restraints"],
        )
        # Set AceDRG structure output
        # <monomer_code>_acedrg.mmcif
        self.add_output_node(
            f"{monomer_code}_acedrg.mmcif", NODE_ATOMCOORDS, ["acedrg", "coordinates"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        command = PipelinerCommand([self.jobinfo.programs[0].command])

        # Get parameters
        smiles_string = self.joboptions["smiles_string"].get_string()
        smiles_file = self.joboptions["smiles_file"].get_string()
        mol_file = self.joboptions["mol_file"].get_string()
        mmcif_file = self.joboptions["mmcif_file"].get_string()
        monomer_code = self.joboptions["monomer_code"].get_string()

        # Set command
        if smiles_string:
            command.com.append(f"--smi={smiles_string}")
        elif smiles_file:
            command.com.append(
                f"--smi={os.path.relpath(smiles_file, self.working_dir)}"
            )
        elif mol_file:
            command.com.append(f"--mol={os.path.relpath(mol_file, self.working_dir)}")
        elif mmcif_file:
            command.com.append(
                f"--mmcif={os.path.relpath(mmcif_file, self.working_dir)}"
            )

        command.com.append(f"--res={monomer_code}")
        command.com.append(f"--out={monomer_code}_acedrg")

        monomer_code = self.joboptions["monomer_code"].get_string()
        model_mmcif = os.path.splitext(f"{monomer_code}_acedrg.cif")[0] + ".mmcif"
        cifconvert = PipelinerCommand(
            self.get_cifconvert_command(
                model_cif=f"{monomer_code}_acedrg.cif", model_mmcif=model_mmcif
            )
        )
        file_check = PipelinerCommand(
            [
                "python3",
                get_job_script("check_for_output_files.py"),
                "--files",
                # f"{monomer_code}_acedrg.pdb",
                f"{monomer_code}_acedrg.cif",
                model_mmcif,
            ]
        )

        cleanup = PipelinerCommand(
            [
                "python3",
                get_job_script("cleanup_tmp_files.py"),
                "--dirs",
                f"{monomer_code}_acedrg_TMP",
            ],
        )
        commands = [command, cifconvert, file_check, cleanup]
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:

        ligand = self.joboptions["monomer_code"].get_string() + "_acedrg.mmcif"
        output_model = os.path.join(self.output_dir, ligand)
        return [
            make_map_model_thumb_and_display(
                maps=[],
                maps_opacity=[],
                models=[output_model],
                title=ligand,
                outputdir=self.output_dir,
            )
        ]

    def create_post_run_output_nodes(self) -> None:
        monomer_code = self.joboptions["monomer_code"].get_string()
        # Set AceDRG pdb output
        # <monomer_code>_acedrg.pdb
        if os.path.isfile(os.path.join(self.output_dir, f"{monomer_code}_acedrg.pdb")):
            self.add_output_node(
                f"{monomer_code}_acedrg.pdb",
                NODE_ATOMCOORDS,
                ["acedrg", "coordinates"],
            )
