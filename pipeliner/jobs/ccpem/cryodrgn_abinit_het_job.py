#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

from typing import List, Sequence
from pathlib import Path
import shlex

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    IntJobOption,
    FloatJobOption,
    JobOptionCondition,
)

# from pipeliner.utils import get_job_script
from pipeliner.nodes import (
    NODE_MLMODEL,
    Node,
)
from pipeliner.node_factory import create_node
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.jobs.ccpem.cryodrgn_train_vae_job import (
    setup_common_jobops,
    locate_weights_pkl,
    get_job_args,
    get_parse_ctf_cmd,
    get_downsample_cmd,
    get_analyze_cmd,
    analyze_results_display,
)


class CryodrgnAbinitHetJob(PipelinerJob):
    """
    CryoDRGN abinit_het v1 job for ccpem-pipeliner
    """

    PROCESS_NAME = "cryodrgn.abinit_het"
    OUT_DIR = "CryoDRGN"
    CATEGORY_LABEL = "Continuous Heterogeneity"

    # (required) Instantiate the object: define info about it and its input parameters
    def __init__(self) -> None:
        # super(self.__class__, self).__init__()
        super().__init__()
        self.jobinfo.display_name = "CryoDRGN ab initio heterogeneous reconstruction"
        self.jobinfo.version = "0.0.1"
        self.jobinfo.job_author = "Joel Greer"
        self.jobinfo.programs = [
            ExternalProgram(command="cryodrgn"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Zhong ED", "Lerer A", "Davis JH", "Berger B"],
                title=(
                    "CryoDRGN2: Ab initio neural reconstruction of 3D protein"
                    " structures from real cryo-EM images"
                ),
                journal=("IEEE/CVF International Conference on Computer Vision (ICCV)"),
                year="2021",
                volume="18",
                pages="4046-4055",
                doi="https://doi.org/10.1109/ICCV48922.2021.00403",
            )
        ]
        self.jobinfo.short_desc = "CryoDRGN ab-initio heterogeneous reconstruction"
        self.jobinfo.long_desc = (
            "CryoDRGN ab-initio heterogeneous reconstruction."
            " Converts star files into pkls and uses those to train cryodrgn"
            " abinit_het to produce a reconstructed density map."
        )
        self.jobinfo.documentation = "https://ez-lab.gitbook.io/cryodrgn"
        self.images = 0
        self.particles_per_image = 0

        # set up the common jobops between cryodrgn jobs
        setup_common_jobops(self.joboptions)

        # standard options
        self.joboptions["zdim"] = IntJobOption(
            label="Dimension of latent variable",
            default_value=8,
            hard_min=1,
            suggested_max=64,
            step_value=8,
            help_text="Dimension of latent variable to encode heterogeneity.",
            is_required=True,
            in_continue=False,
        )

        self.joboptions["num_epochs"] = IntJobOption(
            label="Total number of training epochs",
            default_value=30,
            suggested_min=5,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of training epochs (default: 30). "
                "Cumulative if using pretrained weights"
            ),
            is_required=True,
            in_continue=False,
        )

        # advanced options
        self.joboptions["checkpoint"] = IntJobOption(
            label="Checkpointing interval in N_EPOCHS",
            default_value=1,
            hard_min=1,
            suggested_max=25,
            step_value=1,
            help_text="Number of epochs between saving (intermediate) weights",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )
        self.joboptions["log_interval"] = IntJobOption(
            label="Logging interval in N_IMGS",
            default_value=1000,
            suggested_min=500,
            hard_min=1,
            suggested_max=10000,
            step_value=500,
            help_text="",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Checkpoints and Logs",
        )

        self.joboptions["max_threads"] = IntJobOption(
            label="Max threads for data loading",
            default_value=16,
            hard_min=1,
            suggested_max=32,
            step_value=1,
            help_text="Maximum number of CPU cores for data loading (default: 16)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Dataset Loading",
        )

        # training parameters
        self.joboptions["batch_size"] = IntJobOption(
            label="Minibatch size",
            default_value=8,
            suggested_min=8,
            hard_min=1,
            suggested_max=64,
            step_value=8,
            help_text="Minibatch size (default: 8)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["weight_decay"] = FloatJobOption(
            label="Weight decay in Adam optimizer",
            default_value=0.0,
            help_text="Weight decay in Adam optimizer (default: 0.0)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )
        self.joboptions["learning_rate"] = FloatJobOption(
            label="Learning rate in Adam optimizer",
            default_value=0.0001,
            help_text="Learning rate in Adam optimizer (default: 0.0001)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Training Parameters",
        )

        # encoder network
        self.joboptions["enc_layers"] = IntJobOption(
            label="Number of hidden layers in encoder",
            default_value=3,
            hard_min=1,
            suggested_max=6,
            step_value=1,
            help_text="Number of hidden layers in encoder network (default: 3)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Encoder Network",
        )
        self.joboptions["enc_dim"] = IntJobOption(
            label="Size of hidden layers in encoder",
            default_value=1024,
            suggested_min=128,
            hard_min=1,
            suggested_max=4096,
            step_value=256,
            help_text=(
                "Number of nodes in hidden layers in encoder network (default: 1024)"
            ),
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Encoder Network",
        )

        # decoder network
        self.joboptions["dec_layers"] = IntJobOption(
            label="Number of hidden layers in decoder",
            default_value=3,
            hard_min=1,
            suggested_max=6,
            step_value=1,
            help_text="Number of hidden layers in decoder network (default: 3)",
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Decoder Network",
        )
        self.joboptions["dec_dim"] = IntJobOption(
            label="Size of hidden layers in decoder",
            default_value=1024,
            suggested_min=128,
            hard_min=1,
            suggested_max=4096,
            step_value=256,
            help_text=(
                "Number of nodes in hidden layers in decoder network (default: 1024)"
            ),
            is_required=False,
            in_continue=False,
            deactivate_if=JobOptionCondition([("advanced_options", "=", False)]),
            jobop_group="Decoder Network",
        )

        self.get_runtab_options(mpi=False, threads=False, addtl_args=False)

        # set jobop order
        self.set_joboption_order(
            [
                "fn_img",
                "zdim",
                "downsample",
                "num_epochs",
                "rand_seed",
                "use_chunk",
                "chunk",
                "advanced_options",
                "checkpoint",
                "log_interval",
                "uninvert_data",
                "max_threads",
                "batch_size",
                "weight_decay",
                "learning_rate",
                "enc_layers",
                "enc_dim",
                "dec_layers",
                "dec_dim",
                "cryoDRGN_other_args",
            ]
        )

    # create output nodes
    def create_output_nodes(self) -> None:
        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(f"train_{downsample}")
        final_weights = train_dir / "weights.pkl"
        self.add_output_node(str(final_weights), NODE_MLMODEL, ["cryoDRGN"])

    def get_current_output_nodes(self) -> List[Node]:
        # called if user manually changes job status from failed to succeeded
        # looks for the weights file that should be present first, then if not
        # found, it looks for intermediate weights files
        # If none are found a FileNotFound error is reported. There are no
        # other output nodes, so this should be sufficient
        nodes_found = []

        downsample = self.joboptions["downsample"].get_number()
        train_dir = Path(self.output_dir) / Path(f"train_{downsample}")
        # locate_weights_pkl file if exists
        output_weights_file = locate_weights_pkl(
            train_dir,
            int(self.joboptions["num_epochs"].get_number()),
            int(self.joboptions["checkpoint"].get_number()),
        )

        if output_weights_file:
            nodes_found.append(
                create_node(output_weights_file, NODE_MLMODEL, ["cryodrgn"])
            )
        return nodes_found

    def get_commands(self) -> List[PipelinerCommand]:
        # get args common between cryodrgn jobs
        args = get_job_args(self.output_dir, self.joboptions)

        # parse_ctf
        parse_ctf_cmd = get_parse_ctf_cmd(args.ctf_pkl, args.metadata)

        # get mrcs_subdir_cmd, downsample_cmd and particles file
        mrcs_subdir_cmd, downsample_cmd, particles_str = get_downsample_cmd(
            args.downsample,
            args.metadata,
            args.mrcs,
            args.particles,
            args.use_chunk,
            args.chunk,
        )

        # abinit_het
        abinit_het_cmd = [
            "cryodrgn",
            "abinit_het",
            particles_str,
            "--ctf",
            str(args.ctf_pkl),
            "--zdim",
            str(self.joboptions["zdim"].get_number()),
            "-n",
            str(args.num_epochs + 1),  # convert base 0 to base 1
            "-o",
            str(args.train_dir),
            "--checkpoint",
            str(self.joboptions["checkpoint"].get_number()),
            "--log-interval",
            str(self.joboptions["log_interval"].get_number()),
            "--max-threads",
            str(self.joboptions["max_threads"].get_number()),
            "-b",
            str(self.joboptions["batch_size"].get_number()),
            "--wd",
            str(self.joboptions["weight_decay"].get_number()),
            "--lr",
            str(self.joboptions["learning_rate"].get_number()),
            "--enc-layers",
            str(self.joboptions["enc_layers"].get_number()),
            "--enc-dim",
            str(self.joboptions["enc_dim"].get_number()),
            "--dec-layers",
            str(self.joboptions["dec_layers"].get_number()),
            "--dec-dim",
            str(self.joboptions["dec_dim"].get_number()),
        ]
        if self.joboptions["rand_seed"].get_number() >= 0:
            abinit_het_cmd.append("--seed")
            abinit_het_cmd.append(str(self.joboptions["rand_seed"].get_number()))
        if self.joboptions["uninvert_data"].get_boolean():
            abinit_het_cmd.append("--uninvert-data")
        # add usr args
        if self.joboptions["cryoDRGN_other_args"].get_string():
            other_args = shlex.split(
                self.joboptions["cryoDRGN_other_args"].get_string()
            )
            for arg in other_args:
                abinit_het_cmd.append(arg)

        # analyze training results
        analyze_cmd = get_analyze_cmd(args.num_epochs, args.train_dir)

        final_commands_list: List[List[str]] = [
            parse_ctf_cmd,
            mrcs_subdir_cmd,
            downsample_cmd,
            abinit_het_cmd,
            analyze_cmd,
        ]

        return [PipelinerCommand(x) for x in final_commands_list]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return analyze_results_display(Path(self.output_dir), self.joboptions)
