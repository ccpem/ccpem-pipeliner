#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import re
from typing import List, Sequence, Union
from pathlib import Path
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
    JobOptionCondition,
    StringJobOption,
    JobOptionValidationResult,
    FloatJobOption,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_SEQUENCE
from pipeliner.results_display_objects import ResultsDisplayObject


class GemmiProcessModel(PipelinerJob):
    PROCESS_NAME = "gemmi.atomic_model_utilities.process_model"
    OUT_DIR = "GemmiProcessModel"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Gemmi Process Model"
        self.jobinfo.short_desc = "Process and interconvert PDB and mmCIF"
        self.jobinfo.long_desc = (
            "Uses Gemmi to process atomic models and/or inter-convert between mmCIF and"
            " PDB formats. Some options require Gemmi >=0.6.2; if your job fails check"
            " you are using a new enough version."
        )
        self.jobinfo.version = "0.2"
        self.jobinfo.job_author = "Tom Burnley, Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(
                command="gemmi", vers_com=["gemmi", "--version"], vers_lines=[0]
            ),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Wojdyr M"],
                title="GEMMI: A library for structural biology",
                journal="JOSS",
                year="2022",
                volume="7",
                issue="73",
                pages="4200",
                doi="10.21105/joss.04200",
            )
        ]
        self.jobinfo.documentation = "https://gemmi.readthedocs.io/en/latest/"

        self.joboptions["model"] = InputNodeJobOption(
            label="Model to process",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".mmcif", ".pdb", ".ent"]),
            default_value="",
            help_text="Input model to process. Choose output format below",
            is_required=True,
        )
        self.joboptions["convert_only"] = BooleanJobOption(
            label="Just convert format?",
            default_value=False,
            help_text=(
                "Simply inter-convert formats without processing or editing the model"
            ),
        )
        self.joboptions["output_format"] = MultipleChoiceJobOption(
            label="Output format",
            choices=["mmCIF", "PDB"],
            default_value_index=0,
            help_text="Choose the type of file to output",
        )
        self.joboptions["shorten_chain"] = BooleanJobOption(
            label="Shorten chain names?",
            default_value=True,
            help_text=(
                "Shorten chain names to 1 or 2 characters. (1 if there are fewer than"
                " 63 chains.) This option is recommended for mmCIF to PDB conversion."
            ),
        )
        self.joboptions["out_filename"] = StringJobOption(
            label="Output model filename",
            default_value="",
            help_text=(
                "If not given, and 'Just convert format?' is Yes, the output filename"
                " is the same as the input but with different extension. If 'Just"
                " convert format?' is No, the output filename is the input filename"
                " with '_gemmi_processed' as suffix, and selected extension."
            ),
            is_required=False,
        )
        self.joboptions["model_selection"] = StringJobOption(
            label="Atom selection",
            default_value="",
            help_text=(
                "Format: /models/chains/residues/atoms. e.g. /1 for model 1, /1/D for"
                " chain D from model 1, //D,E for chains D and E. //*/10-30 for"
                " residues with sequence IDs from 10 to 30, //*/(ALA) for all alanine"
                " residues, //*//CB for all C-beta atoms, //*//[P] for phosphorus"
                " atoms. For mmCIF file input, chain IDs should match the"
                " auth_asym_id field. Other options:"
                " https://gemmi.readthedocs.io/en/latest/analysis.html#selections"
            ),
            is_required=False,
            jobop_group="Edit model",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["reset_b"] = BooleanJobOption(
            label="Reset atomic B-factors?",
            default_value=False,
            help_text="Set isotropic B-factors to a single value?",
            jobop_group="Edit model",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["reset_b_val"] = FloatJobOption(
            label="New B-factor",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=300.0,
            step_value=0.1,
            help_text="Set isotropic B-factors to this value (in square angstroms)",
            deactivate_if=JobOptionCondition(
                [("reset_b", "=", False), ("convert_only", "=", True)]
            ),
            required_if=JobOptionCondition([("reset_b", "=", True)]),
            jobop_group="Edit model",
        )
        self.joboptions["rename_chain"] = StringJobOption(
            label="Rename a chain",
            default_value="",
            help_text=(
                "Format: OLD:NEW, e.g. A:B renames chain A as chain B. :C adds missing"
                " chain IDs as C. For mmCIF file input, chain IDs should match the"
                " auth_asym_id field. It is not possible to rename multiple chains at"
                " once."
            ),
            is_required=False,
            jobop_group="Edit model",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["assign_sequence"] = InputNodeJobOption(
            label="Sequence to assign",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Sequence file", [".fasta", ".fa", ".fas", ".pir"]),
            help_text=(
                "Use sequence(s) from this file in PIR or FASTA format. Each chain is"
                " assigned the best matching sequence, if any."
            ),
            is_required=False,
            jobop_group="Edit model",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["remove_selection"] = StringJobOption(
            label="Remove selection",
            default_value="",
            help_text=(
                "Format: /models/chains/residues/atoms. e.g. /1 for model 1, /1/D for"
                " chain D from model 1, //D,E for chains D and E. //*/10-30 for"
                " residues with sequence IDs from 10 to 30, //*/(ALA) for all alanine"
                " residues, //*//CB for all C-beta atoms, //*//[P] for phosphorus"
                " atoms. For mmCIF file input, chain IDs should match the"
                " auth_asym_id field. Other options:"
                " https://gemmi.readthedocs.io/en/latest/analysis.html#selections"
            ),
            is_required=False,
            jobop_group="Remove model components",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["remove_h"] = BooleanJobOption(
            label="Remove hydrogens",
            default_value=False,
            help_text="Remove hydrogen atoms if present",
            jobop_group="Remove model components",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["remove_ligands_waters"] = BooleanJobOption(
            label="Remove ligands and waters",
            default_value=False,
            help_text="Remove ligands and waters",
            jobop_group="Remove model components",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["remove_waters"] = BooleanJobOption(
            label="Remove waters",
            default_value=False,
            help_text="Remove waters if present",
            deactivate_if=JobOptionCondition(
                [("remove_ligands_waters", "=", True), ("convert_only", "=", True)]
            ),
            jobop_group="Remove model components",
        )
        self.joboptions["remove_anisou"] = BooleanJobOption(
            label="Remove ANISOU records",
            default_value=False,
            help_text="Remove anisotropic B-factor (ANISOU) records if present",
            jobop_group="Remove model components",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["trim_to_ala"] = BooleanJobOption(
            label="Trim residues to Ala?",
            default_value=False,
            help_text="Trim residue side chains to alanine",
            jobop_group="Remove model components",
            deactivate_if=JobOptionCondition([("convert_only", "=", True)]),
        )
        self.joboptions["all_auth"] = BooleanJobOption(
            label="Write author atom and comp IDs?",
            default_value=False,
            help_text=(
                "Write author-defined mmCIF fields _atom_site.auth_atom_id (same as"
                " label_atom_id) and auth_comp_id (same as label_comp_id). See "
                "https://mmcif.wwpdb.org/docs/tutorials/content/atomic-description.html"
                " for details."
            ),
            jobop_group="Output mmCIF options",
            deactivate_if=JobOptionCondition([("output_format", "!=", "mmCIF")]),
        )
        self.joboptions["sort_tags"] = BooleanJobOption(
            label="Sort tags?",
            default_value=False,
            help_text="Sort tags in alphabetical order",
            jobop_group="Output mmCIF options",
            deactivate_if=JobOptionCondition([("output_format", "!=", "mmCIF")]),
        )
        self.joboptions["skip_category"] = StringJobOption(
            label="Skip category",
            default_value="",
            help_text=(
                "Do not write out some mmCIF tags, e.g. if set to CAT, output tags"
                " starting with _CAT will be skipped."
            ),
            is_required=False,
            jobop_group="Output mmCIF options",
            deactivate_if=JobOptionCondition([("output_format", "!=", "mmCIF")]),
        )
        self.joboptions["cif_style"] = MultipleChoiceJobOption(
            label="Output mmCIF style",
            choices=["plain", "pdbx", "aligned"],
            default_value_index=0,
            help_text=(
                "Choose the style of mmCIF file output. One of plain, pdbx (categories"
                " separated with #) or aligned (left-aligned columns)"
            ),
            jobop_group="Output mmCIF options",
            deactivate_if=JobOptionCondition([("output_format", "!=", "mmCIF")]),
        )

        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        model = self.joboptions["model"].get_string()
        # convert only?
        convert_only = self.joboptions["convert_only"].get_boolean()
        output_format = self.joboptions["output_format"].get_string()
        if convert_only:
            if (
                os.path.splitext(model)[-1].lower() in [".cif", ".mmcif"]
                and output_format == "mmCIF"
            ) or (
                os.path.splitext(model)[-1].lower() in [".pdb", ".ent"]
                and output_format == "PDB"
            ):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["output_format"]],
                        message=("Ensure correct output format is selected"),
                    )
                )
        # chain selection
        model_selection = self.joboptions["model_selection"].get_string()
        if model_selection:
            check_string = r"^([/*[(][A-Za-z0-9/][A-Za-z0-9/,:>*.<)-]*|])$"
            if not re.search(check_string, model_selection):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["model_selection"]],
                        message=(
                            "Selection should be in the format"
                            " /models/chains/residues/atoms"
                        ),
                    )
                )
        # remove selection
        remove_selection = self.joboptions["remove_selection"].get_string()
        if remove_selection:
            check_string = r"^([/*[(][A-Za-z0-9/][A-Za-z0-9/,:>*.<)-]*|])$"
            if not re.search(check_string, remove_selection):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["remove_selection"]],
                        message=(
                            "Selection should be in the format"
                            " /models/chains/residues/atoms"
                        ),
                    )
                )
        # chain rename
        rename_chain = self.joboptions["rename_chain"].get_string()
        if rename_chain:
            check_string = r"^([A-Za-z0-9]+\:[A-Za-z0-9]+)$"
            if not re.search(check_string, rename_chain):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["rename_chain"]],
                        message=("Format should be OLD:NEW"),
                    )
                )
        return errors

    def get_output_model_name(self, input_model_name) -> str:
        out_filename = self.joboptions["out_filename"].get_string()
        out_file_path = Path(out_filename)
        out_filename_ext = out_file_path.suffix
        output_format = self.joboptions["output_format"].get_string()
        if output_format == "mmCIF":
            ext = ".cif"
            # TODO : ResultsDisplayMapModel doesnt support ".mmCIF". add .mmCIF below
            if out_filename_ext == ".mmcif":
                ext = out_filename_ext
        else:
            ext = ".pdb"
            # TODO : ResultsDisplayMapModel doesnt support .ent and .PDB now
            # if out_filename_ext in [".ent", ".PDB"]:
            #     ext = out_filename_ext
        convert_only = self.joboptions["convert_only"].get_boolean()
        input_model_path = Path(input_model_name)
        out_stem = input_model_path.stem
        if out_filename:
            out_stem = out_file_path.stem
        elif not convert_only:
            out_stem += "_gemmi_processed"
        model_out = out_stem + ext
        return str(Path(self.output_dir) / model_out)

    def create_output_nodes(self) -> None:
        model = self.joboptions["model"].get_string()
        self.output_nodes.append(
            create_node(
                self.get_output_model_name(model),
                NODE_ATOMCOORDS,
                ["gemmi", "process_model"],
            )
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        command: List[Union[int, float, str]] = [
            self.jobinfo.programs[0].command,
            "convert",
        ]

        # Get parameters
        model = self.joboptions["model"].get_string()
        convert_only = self.joboptions["convert_only"].get_boolean()
        remove_h = self.joboptions["remove_h"].get_boolean()
        remove_waters = self.joboptions["remove_waters"].get_boolean()
        remove_ligands_waters = self.joboptions["remove_ligands_waters"].get_boolean()
        remove_anisou = self.joboptions["remove_anisou"].get_boolean()
        model_selection = self.joboptions["model_selection"].get_string()
        remove_selection = self.joboptions["remove_selection"].get_string()
        rename_chain = self.joboptions["rename_chain"].get_string()
        shorten_chain = self.joboptions["shorten_chain"].get_boolean()
        assign_sequence = self.joboptions["assign_sequence"].get_string()
        trim_to_ala = self.joboptions["trim_to_ala"].get_boolean()
        reset_b = self.joboptions["reset_b"].get_boolean()
        reset_b_val = self.joboptions["reset_b_val"].get_number()
        output_format = self.joboptions["output_format"].get_string()
        all_auth = self.joboptions["all_auth"].get_boolean()
        sort_tags = self.joboptions["sort_tags"].get_boolean()
        skip_category = self.joboptions["skip_category"].get_string()
        cif_style = self.joboptions["cif_style"].get_string()
        # Set command parameters
        if not convert_only:
            if remove_h:
                command += ["--remove-h"]
            if remove_anisou:
                command += ["--anisou", "no"]
            if remove_ligands_waters:
                command += ["--remove-lig-wat"]
            elif remove_waters:
                command += ["--remove-waters"]
            if model_selection:
                command += ["--select", model_selection]
            elif remove_selection:
                command += ["--remove", remove_selection]
            if rename_chain:
                command += ["--rename-chain", rename_chain]
            if trim_to_ala:
                command += ["--trim-to-ala"]
            if reset_b:
                command += ["-B", reset_b_val]
            if assign_sequence:
                command += ["-s", os.path.relpath(assign_sequence, self.working_dir)]
        # general option
        if shorten_chain:
            command += ["--shorten"]
        # mmcif specific options
        if output_format == "mmCIF":
            if all_auth:
                command += ["--all-auth"]
            if sort_tags:
                command += ["--sort"]
            if skip_category:
                command += ["--skip-category", skip_category]
            if cif_style:
                command += ["--style", cif_style]
        output_model = os.path.basename(self.get_output_model_name(model))
        model = os.path.relpath(model, self.working_dir)
        # Set output file
        command += [
            model,
            output_model,
        ]

        return [PipelinerCommand(command)]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        os.makedirs(thumbdir, exist_ok=True)
        in_model = self.joboptions["model"].get_string()
        out_model = self.get_output_model_name(in_model)
        convert_only = self.joboptions["convert_only"].get_boolean()
        if convert_only:
            models = [out_model]
        else:
            models = [in_model, out_model]

        return [
            make_map_model_thumb_and_display(
                models=models,
                outputdir=self.output_dir,
            )
        ]
