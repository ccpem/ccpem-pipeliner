#!/usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from typing import List, Dict, Sequence

import os
import random

from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.data_structure import SELECT_DIR
from pipeliner.job_options import InputNodeJobOption, files_exts
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.utils import get_job_script
from pipeliner.starfile_handler import DataStarFile
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display


def select_3dclass_from_model(modfile: str, verbose: bool = False) -> int:
    msg = []
    class_data = DataStarFile(modfile).loop_as_list(
        "model_classes",
        ["_rlnReferenceImage", "_rlnEstimatedResolution", "_rlnClassDistribution"],
        headers=True,
    )
    msg.append(f"\nFound {len(class_data[1:])} classes:")
    msg.append(str(class_data[0]))
    for i in class_data[1:]:
        msg.append(str(i))

    max_res = min([float(x[1]) for x in class_data[1:]])
    classes_dict = dict(
        zip([x[0] for x in class_data[1:]], [y[1:] for y in class_data[1:]])
    )
    # take the highest resolution class
    res_sel = [cl for cl in classes_dict if float(classes_dict[cl][0]) == max_res]
    if len(res_sel) == 1:
        msg.append("Selection based on resolution:")
        selected_class = res_sel[0]
    else:
        # failing that, runoff by class distribution
        msg.append(f"Multiple classed have max resolution of {max_res}")
        msg.append("Choosing class based on number of particles")
        max_dist = max([float(classes_dict[x][1]) for x in res_sel])
        dist_sel = [cl for cl in res_sel if float(classes_dict[cl][1]) == max_dist]
        if len(dist_sel) != 1:
            msg.append(
                f"WARNING: {len(dist_sel)} classes have the EXACT same resolution and "
                "particle number.\nFinal class chosen from random from these classes."
            )
            selected_class = random.choice(dist_sel)
        else:
            selected_class = dist_sel[0]
    cl_file = os.path.basename(os.path.splitext(os.path.basename(selected_class))[0])
    clno = int(cl_file.split("_")[2].replace("class", ""))
    msg.append(f"Selected class number {clno}")
    if verbose:
        for m in msg:
            print(m)
    return clno


class AutoSelect3D(PipelinerJob):
    PROCESS_NAME = "pipeliner.select.autoselect3d"
    OUT_DIR = SELECT_DIR
    CATEGORY_LABEL = "Automated Class Selection"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "3D class auto selection"
        self.jobinfo.short_desc = "Automated selection classes from 3D classifications"
        self.jobinfo.long_desc = (
            "Currently just takes the class with the highest estimated resolution, "
            "later will be expanded to have more advanced selection metrics"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = []
        self.jobinfo.references = []
        self.jobinfo.documentation = ""

        self.joboptions["fn_parts"] = InputNodeJobOption(
            label="Input 3D classification particles file",
            node_type=NODE_PARTICLEGROUPMETADATA,
            pattern=files_exts("Class 3D Particles STAR file", ["*_data.star"]),
            help_text=(
                "The data star file from the 3D classification job to select from"
            ),
            node_kwds=["relion", "class3d"],
            is_required=True,
        )

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "selected_particles.star",
            NODE_PARTICLEGROUPMETADATA,
            ["relion"],
        )
        self.add_output_node(
            "selected_class.mrc",
            NODE_DENSITYMAP,
            ["3D_class", "relion"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        model_file = self.joboptions["fn_parts"].get_string()
        return [
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("autoselect_class3d/autoselect_class3d.py"),
                    "--data",
                    model_file,
                    "--outdir",
                    self.output_dir,
                ],
            )
        ]

    def gather_metadata(self) -> Dict[str, object]:
        parts_file = os.path.join(self.output_dir, "selected_particles.star")
        parts_data = DataStarFile(parts_file)
        particle_count = parts_data.count_block("particles")
        mod_file = self.joboptions["fn_parts"].get_string().replace("_data", "_model")
        clno = select_3dclass_from_model(mod_file)
        selected = mod_file.replace("_model.star", f"_class{clno:03d}.mrc")
        class_data = DataStarFile(mod_file).loop_as_list(
            "model_classes",
            [
                "_rlnReferenceImage",
                "_rlnClassDistribution",
                "_rlnEstimatedResolution",
                "_rlnOverallFourierCompleteness",
            ],
        )
        for class_line in class_data:
            if class_line[0] == selected:
                selected_class = class_line
                break
        return {
            "selected_map": selected_class[0],
            "particle_count": particle_count,
            "percent_of_total_particles": selected_class[1],
            "estimated_resolution": selected_class[2],
            "overall_fourier_completeness": selected_class[3],
        }

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        mod_file = self.joboptions["fn_parts"].get_string().replace("_data", "_model")
        clno = select_3dclass_from_model(mod_file)
        dispobjs: List[ResultsDisplayObject] = make_maps_slice_montage_and_3d_display(
            in_maps={self.output_nodes[1].name: f"Selected class {clno}"},
            output_dir=self.output_dir,
        )
        dispobjs.append(self.output_nodes[0].get_result_display_object())

        return dispobjs
