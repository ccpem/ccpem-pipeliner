#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from typing import List, Sequence
import re
from pipeliner.pipeliner_job import (
    PipelinerJob,
    ExternalProgram,
    PipelinerCommand,
    JobOptionCondition,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    StringJobOption,
    BooleanJobOption,
    JobOptionValidationResult,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_SEQUENCE,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class ModelSequenceAssign(PipelinerJob):
    PROCESS_NAME = "findmysequence.atomic_model_utilities.assign_sequence"
    OUT_DIR = "FindMySequence"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "findMySequence"
        self.jobinfo.short_desc = "Assign and build side-chains based on map features"
        self.jobinfo.long_desc = (
            "Find model sequence (side-chains), given:\n"
            "the model backbone and \n"
            "experimental map used to build the model.\n"
            "Given an input sequence, the program can also assign side-chains and \n"
            "builds these in the backbone model. \n"
        )

        self.jobinfo.programs = [
            ExternalProgram("findmysequence"),
        ]
        self.version = "0.9.2"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Chojnowski G",
                    "Simpkin AJ",
                    "Leonardo DA",
                    "Seifert-Davila W",
                    "Vivas-Ruiz DE",
                    "Keegan RM",
                    "Rigden DJ",
                ],
                title=(
                    "findMySequence: a neural-network-based approach for identification"
                    " of unknown proteins in X-ray crystallography and cryo-EM"
                ),
                journal="IUCrJ",
                year="2022",
                volume="9.1",
                pages="86-97",
                doi="10.1107/S2052252521011088",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S2052252521011088"

        # JOB OPTIONS
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="The input main-chain model (side-chains ignored)",
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The input map to extract side-chain features",
            is_required=True,
        )

        self.joboptions["chain_selection"] = StringJobOption(
            label="Chain selection",
            default_value="",
            help_text=("e.g. A.10:100"),
            is_required=False,
        )
        self.joboptions["assign_sequence"] = BooleanJobOption(
            label="Assign given sequence?",
            default_value=False,
            help_text=(
                "If Yes, input sequence will be assigned and "
                "side-chains built in the output model"
            ),
        )

        self.joboptions["input_sequence"] = InputNodeJobOption(
            label="Input sequence",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text="Sequence to assign. Only one chain should be given",
            is_required=False,
            required_if=JobOptionCondition([("assign_sequence", "=", True)]),
            deactivate_if=JobOptionCondition([("assign_sequence", "=", False)]),
        )

        self.joboptions["use_uniprot_id"] = BooleanJobOption(
            label="Download proteome from Uniprot",
            default_value=False,
            help_text=(
                "If Yes, a proteome with specified ID will be downloaded "
                "from Uniprot and used for queries"
            ),
            deactivate_if=JobOptionCondition([("assign_sequence", "=", True)]),
        )

        self.joboptions["input_sequence_db"] = InputNodeJobOption(
            label="Input search database",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text=(
                "Sequence database to search against in fasta format"
                "required if Input sequence is not provided"
            ),
            is_required=False,
            required_if=JobOptionCondition(
                [("use_uniprot_id", "=", False), ("assign_sequence", "=", False)],
                operation="ALL",
            ),
            deactivate_if=JobOptionCondition([("assign_sequence", "=", True)]),
        )
        self.joboptions["input_uniprot_id"] = StringJobOption(
            label="Uniprot ID of a target proteome",
            default_value="",
            help_text=("e.g. UP000005206"),
            is_required=False,
            required_if=JobOptionCondition(
                [("use_uniprot_id", "=", True), ("assign_sequence", "=", False)],
                operation="ALL",
            ),
            deactivate_if=JobOptionCondition([("assign_sequence", "=", True)]),
        )

        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # molprobity
        chain_selection = self.joboptions["chain_selection"].get_string()
        if chain_selection:
            check_string = r"^([A-Za-z0-9]+\.[A-Za-z0-9]+\:[A-Za-z0-9]+|[A-Za-z0-9]+)$"
            if not re.search(check_string, chain_selection):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["chain_selection"]],
                        message=("please input selection in the format e.g. A.10:100"),
                    )
                )
        return errors

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        if input_map:
            mapid = os.path.splitext(os.path.basename(input_map))[0]
        # Now make the nodes
        self.add_output_node(
            modelid + "_" + mapid + "findseq.json",
            NODE_LOGFILE,
            ["findmysequence", "json_out"],
        )
        # add output PDB node
        # ONLY if requested, datafield may !=''
        if self.joboptions["assign_sequence"].get_boolean():
            self.add_output_node(
                modelid + "_findseq.pdb",
                NODE_ATOMCOORDS,
                ["findmysequence", "model_out"],
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        commands = []
        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        input_map = os.path.relpath(input_map, self.working_dir)
        mapid = os.path.splitext(os.path.basename(input_map))[0]

        # check if a proteome should be downloaded from Uniprot by ID
        # ONLY is "assign_sequence" is False!
        # data fileds are not cleanet
        if self.joboptions["assign_sequence"].get_boolean():
            input_seq_db = ""
            input_seq = self.joboptions["input_sequence"].get_string()
            if input_seq:
                input_seq = os.path.relpath(input_seq, self.working_dir)
        else:
            input_seq = ""
            if self.joboptions["use_uniprot_id"].get_boolean():
                input_uniprot_id = self.joboptions["input_uniprot_id"].get_string()
                input_seq_db = os.path.join(
                    self.output_dir, f"{input_uniprot_id}.fasta.gz"
                )
                input_seq_db = os.path.relpath(input_seq_db, self.working_dir)

                fetch_proteome_command = [
                    "python3",
                    get_job_script("fetch_proteome_sequences.py"),
                    "-id",
                    input_uniprot_id,
                ]
                commands += [fetch_proteome_command]
            else:
                input_seq_db = self.joboptions["input_sequence_db"].get_string()
                if input_seq_db:
                    input_seq_db = os.path.relpath(input_seq_db, self.working_dir)

        chain_selection = self.joboptions["chain_selection"].get_string()
        # checkmysequence
        commands += self.get_findmysequence_commands(
            input_map,
            input_model,
            input_seq_db,
            input_seq,
            chain_selection,
            modelid,
            mapid,
        )
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_findmysequence_commands(
        input_map,
        input_model,
        input_seq_db,
        input_sequence,
        chain_selection,
        modelid,
        mapid,
    ) -> List[List[str]]:
        # pdb_num = 1  # this needs to be set for multiple model input
        findseq_command = ["findmysequence"]
        findseq_command += ["--mapin", input_map]
        findseq_command += ["--modelin", input_model]
        if input_sequence:
            findseq_command += ["--seqin", input_sequence]
            model_out = modelid + "_findseq.pdb"
            findseq_command += ["--modelout", model_out]
        elif input_seq_db:
            findseq_command += ["--db", input_seq_db]
        if chain_selection:
            chain_string = ""
            chain_sel_strip = chain_selection.strip()
            if "." in chain_sel_strip:
                chain_id = chain_sel_strip.split(".")[0]
                chain_string = "chain {}".format(chain_id)
                if ":" in chain_sel_strip:
                    res_sel = chain_sel_strip.split(".")[1]
                    resi_start = res_sel.split(":")[0].strip()
                    resi_end = res_sel.split(":")[1].strip()
                    chain_string += f" and (resi {resi_start}:{resi_end})"
            else:
                chain_id = chain_sel_strip.split()[0]
                chain_string = "chain {}".format(chain_id)
            if chain_string:
                findseq_command += ["--select", chain_string]
        output_json = modelid + "_" + mapid + "findseq.json"
        findseq_command += ["--jsonout", output_json]
        findseq_results_command = [
            "python3",
            get_job_script("findmyseq/get_findmyseq_results.py"),
            "-j",
            output_json,
            "-id",
            modelid + "_" + mapid,
        ]
        return [
            findseq_command,
            findseq_results_command,
        ]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # TODO: long files names to be trimmed ?
        input_model = self.joboptions["input_model"].get_string()
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        input_seq = self.joboptions["input_sequence"].get_string()
        display_objects: List[ResultsDisplayObject] = []
        self.create_findmysequence_report(
            self.output_dir, modelid, mapid, display_objects
        )
        if input_seq:
            model_out = modelid + "_findseq.pdb"
            model_out = os.path.join(self.output_dir, model_out)
            map_model_display = make_map_model_thumb_and_display(
                maps=[input_map],
                maps_opacity=[0.5],
                models=[model_out],
                title="Model side-chains built",
                outputdir=self.output_dir,
            )
            display_objects.append(map_model_display)
        return display_objects

    def create_findmysequence_report(
        self, output_dir, modelid, mapid, display_objects
    ) -> None:
        results_out = os.path.join(
            output_dir, modelid + "_" + mapid + "_findseqparse.json"
        )
        with open(results_out, "r") as j:
            dict_out = json.load(j)
        disp_obj = create_results_display_object(
            "text",
            title="FindMySequence Results",
            display_data=dict_out["findseq_results"],
            associated_data=[results_out],
            start_collapsed=False,
        )
        display_objects.append(disp_obj)

    def create_post_run_output_nodes(self) -> None:
        input_model = self.joboptions["input_model"].get_string()
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]

        results_out = os.path.join(
            self.output_dir, modelid + "_" + mapid + "_findseqparse.json"
        )
        with open(results_out, "r") as j:
            dict_out = json.load(j)
        if dict_out:
            # try to add output nodes with sequence hits (if any)
            for idx, hit_ev in enumerate(
                sorted(dict_out.get("findseq_hits", {}), reverse=False)
            ):
                hit_out = os.path.join(
                    self.output_dir, f"sequence_no{idx+1}" + ".fasta"
                )

                with open(hit_out, "w") as ofile:
                    ofile.write(dict_out["findseq_hits"][hit_ev])

                # Now, make the nodes
                self.add_output_node(
                    f"sequence_no{idx+1}" + ".fasta",
                    NODE_SEQUENCE,
                    ["findmysequence", "fasta_out"],
                )

        if (
            not self.joboptions["assign_sequence"].get_boolean()
            and self.joboptions["use_uniprot_id"].get_boolean()
        ):
            input_uniprot_id = self.joboptions["input_uniprot_id"].get_string()
            if input_uniprot_id:
                self.add_output_node(
                    f"{input_uniprot_id}.fasta.gz",
                    NODE_SEQUENCE,
                    ["findmysequence", "proteome"],
                )
