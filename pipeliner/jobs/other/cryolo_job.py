#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from typing import List, Dict, Sequence, Any

from pipeliner import user_settings
from pipeliner.utils import (
    truncate_number,
    get_job_script,
)
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    ExternalFileJobOption,
    StringJobOption,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionCondition,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_PARAMSDATA,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.results_display_objects import ResultsDisplayObject


class CrYOLOAutopick(PipelinerJob):
    PROCESS_NAME = "cryolo.autopick"
    OUT_DIR = "AutoPick"
    CATEGORY_LABEL = "Automated Particle Picking"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "crYOLO"
        self.jobinfo.short_desc = "Automated particle picking"
        self.jobinfo.long_desc = (
            "Fast and accurate cryo-EM particle picking based on convolutional neural"
            " networks and utilizes the popular You Only Look Once (YOLO) object"
            " detection system."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        yologui = ExternalProgram("cryolo_gui.py")
        yolopred = ExternalProgram("cryolo_predict.py")
        self.jobinfo.programs = [yologui, yolopred]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Wagner T",
                    "Merino F",
                    "Stabrin M",
                    "Moriya T",
                    "Antoni C",
                    "Apelbaum A",
                    "Hagel P",
                    "Sitsel O",
                    "Raisch T",
                    "Prumbaum D",
                    "Quentin D",
                    "Roderer D",
                    "Tacke S",
                    "Siebolds B",
                    "Schubert E",
                    "Shaikh TR",
                    "Lill P",
                    "Gatsogiannis C",
                    "Raunser S",
                ],
                title=(
                    "SPHIRE-crYOLO is a fast and accurate fully automated "
                    "particle picker for cryo-EM."
                ),
                journal="Commun. Biol.",
                year="2019",
                volume="2",
                pages="218",
                doi="10.1038/s42003-019-0437-z",
            )
        ]
        self.jobinfo.documentation = "https://cryolo.readthedocs.io/en/stable/"

        self.always_continue_in_schedule = True

        self.joboptions["input_file"] = InputNodeJobOption(
            label="Input micrographs file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern="Input Micrographs (*.star)",
            help_text=(
                "Input STAR file (preferably with CTF information) with all micrographs"
                " to pick from."
            ),
            is_required=True,
        )

        # this might be made in to an InputNodeJobOption if we adda cryolo train job
        self.joboptions["model_path"] = ExternalFileJobOption(
            label="CrYOLO model:",
            default_value="",
            pattern="Cryolo model (*.h5)",
            help_text="Download a crYOLO model or train your own",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["config_file"] = InputNodeJobOption(
            label="CrYOLO config:",
            default_value="",
            pattern="Cryolo config (*.json)",
            help_text="Optionally provide a configuration file for cryolo",
            in_continue=True,
            is_required=False,
            node_type=NODE_PARAMSDATA,
        )

        self.joboptions["box_size"] = IntJobOption(
            label="Box size:",
            default_value=128,
            hard_min=1,
            suggested_min=50,
            suggested_max=1000,
            step_value=1,
            help_text="Box size in pixels",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["confidence_threshold"] = FloatJobOption(
            label="Confidence threshold",
            default_value=0.3,
            suggested_min=0.01,
            suggested_max=1,
            step_value=0.01,
            help_text="CrYOLO confidence threshold",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["use_JANNI_denoise"] = BooleanJobOption(
            label="Use JANNI denoising?",
            default_value=False,
            help_text="Use JANNI to denoise micrographs.  A model is required",
            in_continue=True,
        )

        self.joboptions["JANNI_model_path"] = ExternalFileJobOption(
            label="Path to JANNI noise model:",
            default_value="",
            pattern="JANNI model (*.h5)",
            help_text=(
                "A noise model file for JANNI. You can download one from the"
                " JANNI website or train your own. See the JANNI documentation"
                " for details."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("use_JANNI_denoise", "=", False)]),
            required_if=JobOptionCondition([("use_JANNI_denoise", "=", True)]),
        )

        self.joboptions["gpus"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                'GPU IDs to use, separated by commas, for example "0", "0, 1" or'
                ' "0,1,2,3". Leave blank for automatic GPU selection.'
            ),
            validation_regex="^\\s*[0-9]+(\\s*,\\s*[0-9]+)*\\s*$",
            regex_error_message="Must be comma separated numbers",
            jobop_group="Running options",
        )

        # Use a custom nr_threads job option with appropriate values
        # Must still be called "nr_threads" so it is used sensibly for queue submissions
        threadmax = user_settings.get_thread_max()
        self.joboptions["nr_threads"] = IntJobOption(
            label="Number of CPUs",
            default_value=-1,
            suggested_max=threadmax,
            step_value=1,
            help_text=(
                "Number of CPUs to use during filtering / filament tracing. If set to"
                " zero or a negative number, it will use all of the available CPUs."
                " For queue submission you might need to set a positive value to make"
                " sure multiple CPUs are assigned to the job."
            ),
            in_continue=True,
            jobop_group="Running options",
        )

        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "autopick.star", NODE_MICROGRAPHCOORDSGROUP, ["cryolo", "autopick"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        self.working_dir = self.output_dir
        input_fn = self.joboptions["input_file"].get_string()
        cryolo_model = self.joboptions["model_path"].get_string()

        # add links dir to script here

        links_com = [
            "python3",
            get_job_script("cryolo/make_cryolo_links_dir.py"),
            "--input_mics_file",
            os.path.relpath(input_fn, self.working_dir),
            "--cryolo_job_dir",
            self.output_dir,
        ]
        if self.is_continue:
            links_com.append("--is_continue")

        # make the cryolo config file
        box_size = self.joboptions["box_size"].get_number()
        config_command = ["cryolo_gui.py"]
        config_command += ["config", "cryolo_config.json", truncate_number(box_size, 0)]

        # pick the filtering option
        if not self.joboptions["use_JANNI_denoise"].get_boolean():
            config_command += ["--filter", "LOWPASS", "--low_pass_cutoff", "0.1"]
        else:
            config_command += ["--filter", "JANNI", "--janni_model"]
            jannimodel = self.joboptions["JANNI_model_path"].get_string()
            config_command.append(jannimodel)

        # make the crYOLO command
        commands = [PipelinerCommand(links_com)]
        cryolo_com = ["cryolo_predict.py", "-c"]
        config_file = self.joboptions["config_file"].get_string()
        if not config_file:
            commands += [PipelinerCommand(config_command)]
            cryolo_com += ["cryolo_config.json"]
        else:
            cryolo_com += [os.path.relpath(config_file, self.working_dir)]

        cryolo_model = self.joboptions["model_path"].get_string()
        cryolo_com += ["-w", cryolo_model]

        cryolo_com += ["-i", "Micrographs/"]

        # add the GPUs entry
        # cryolo needs space-separated GPU IDs as separate arguments following "-g",
        # or leave out entirely to select automatically
        gpus_str = self.joboptions["gpus"].get_string()
        if gpus_str:
            gpus_list = [gpu.strip() for gpu in gpus_str.split(",") if gpu.strip()]
            cryolo_com.append("-g")
            cryolo_com.extend(gpus_list)

        cryolo_com += [
            "-o",
            ".",
            "-t",
            str(self.joboptions["confidence_threshold"].get_number()),
        ]

        nr_cpus = self.joboptions["nr_threads"].get_number()
        if nr_cpus > 0:
            cryolo_com += ["-nc", str(nr_cpus)]

        cryolo_com += self.parse_additional_args()

        commands.append(PipelinerCommand(cryolo_com))

        commands.append(
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("cryolo/make_cryolo_output_file.py"),
                    "-i",
                    os.path.relpath(
                        self.joboptions["input_file"].get_string(), self.working_dir
                    ),
                    "-o",
                    self.output_dir,
                ],
            )
        )
        return commands

    def gather_metadata(self) -> Dict[str, object]:
        metadata_dict: Dict[str, Any] = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "particles in total" in line:
                metadata_dict["TotalParticles"] = int(line.split()[0])
            if "fully immersed" in line:
                metadata_dict["NotFullyImmersed"] = int(line.split()[1])
            if "specified size range" in line:
                metadata_dict["OutOfSizeRange"] = int(line.split()[1])
            if "Particle diameter distribution" in line:
                metadata_dict["SizeDistribution"] = {
                    "MEAN": int(outlines[i + 1].split()[1])
                }
                metadata_dict["SizeDistribution"]["SD"] = int(
                    outlines[i + 2].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q25"] = int(
                    outlines[i + 3].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q50"] = int(
                    outlines[i + 3].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q75"] = int(
                    outlines[i + 4].split()[1]
                )

        summaryfile = os.path.join(
            self.output_dir, "DISTR/confidence_distribution_summary_*.txt"
        )
        with open(glob(summaryfile)[0], "r") as sf:
            summarylines = sf.readlines()

        metadata_dict["ConfidenceDisribution"] = {}

        for line in summarylines[1:]:
            metadata_dict["ConfidenceDisribution"][line.split(",")[0]] = float(
                line.split(",")[1]
            )

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        # get the star files
        logsdir = os.path.join(self.output_dir, "STAR")
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(DataStarFile(pf).count_block())
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "autopick.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "autopick.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [image, graph]
