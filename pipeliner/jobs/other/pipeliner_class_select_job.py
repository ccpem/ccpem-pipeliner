#
#    Copyright (C) 2024 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from pathlib import Path
from typing import List, Dict, Any, Sequence

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    BooleanJobOption,
    JobOptionValidationResult,
)
from pipeliner.nodes import (
    NODE_IMAGE2DGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
    NODE_DENSITYMAPGROUPMETADATA,
)
from pipeliner.utils import get_job_script
from pipeliner.starfile_handler import (
    DataStarFile,
    read_relion_optimiser_starfile,
    StarFile,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.display_tools import create_results_display_object


class PipelinerClassSelect(PipelinerJob):
    PROCESS_NAME = "pipeliner.select.classes"
    OUT_DIR = "Select"
    CATEGORY_LABEL = "Subset Selection"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Pipeliner class selection"
        self.jobinfo.short_desc = (
            "Select 2D or 3D classes and get the associated particles"
        )
        self.jobinfo.long_desc = (
            "This job is used internally by Doppio to do interactive class selection in"
            " the GUI. The job can also be run in an non-interactive manner by manually"
            " inputting the class numbers to select"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [
            ExternalProgram(
                "relion_image_handler", vers_com=["--version"], vers_lines=[0]
            )
        ]

        self.joboptions["fn_optimiser"] = InputNodeJobOption(
            label="Classification optimiser file",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion"],
            default_value="",
            pattern="*.star",
            help_text="Optimiser file from a RELION Class2D or Class3D job",
            is_required=True,
        )

        self.joboptions["classes"] = StringJobOption(
            label="Classes to select",
            help_text=(
                "Enter the numbers of the classes that should be selected as a comma "
                "separated list. The indices for classes start at 1 not 0"
            ),
            in_continue=True,
            is_required=True,
            validation_regex=r"^[\d,\s]*$",
            regex_error_message="Only comma separated integer values are accepted",
        )

        self.joboptions["do_recenter"] = BooleanJobOption(
            label="Recenter 2D class average images",
            help_text=(
                "This option is only used when selecting particles from 2D classes. The"
                " selected class averages will all be re-centered on their"
                " center-of-mass. This is useful when you plan to use these class"
                " averages as templates for auto-picking."
            ),
            default_value=True,
            in_continue=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        # have to do this without reading the actual file as it might not exist yet
        input_type = self.joboptions["fn_optimiser"].get_string().split("/")[0]
        if input_type == "Class3D":
            errs.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["do_recenter"]],
                    message=(
                        "Centering is only applicable to 2D classes, this option will "
                        "be ignored for 3D classes"
                    ),
                )
            )
        return errs

    def create_output_nodes(self) -> None:
        self.add_output_node("particles.star", NODE_PARTICLEGROUPMETADATA, ["relion"])

    def get_input_type(self) -> str:
        """Figure out if the input was Class2D or Class3D

        Returns:
            str: "Class2D" or "Class3D"
        """
        opt_vals = read_relion_optimiser_starfile(
            self.joboptions["fn_optimiser"].get_string()
        )

        input_type = opt_vals["_rlnOutputRootName"].split("/")[0]
        return input_type

    def create_post_run_output_nodes(self):
        intype = self.get_input_type()
        if intype == "Class2D":
            kwds = ["relion", "class_averages", "masked"]
            self.add_output_node(
                "class_averages.star",
                NODE_IMAGE2DGROUPMETADATA,
                kwds,
            )
            if self.joboptions["do_recenter"].get_boolean():
                kwds.append("centered")
                self.add_output_node(
                    "class_averages_centered.star",
                    NODE_IMAGE2DGROUPMETADATA,
                    kwds,
                )
        else:
            kwds = ["relion", "3d_classes"]
            self.add_output_node(
                "selected_classes.star",
                NODE_DENSITYMAPGROUPMETADATA,
                kwds,
            )

    def get_classes(self) -> List[str]:
        """Parse the input classes, remove spaces and duplicates"""
        classes = self.joboptions["classes"].get_string().replace(" ", "")
        classes_list = list(set([int(x) for x in classes.split(",")]))
        classes_list.sort()
        return [str(x) for x in classes_list]

    def get_commands(self) -> List[PipelinerCommand]:
        coms = []
        # for class averages

        coms.append(
            [
                "python3",
                get_job_script("pipeliner_select/clav_stack_star.py"),
                "--optimiser_file",
                self.joboptions["fn_optimiser"].get_string(),
                "--classes",
                *self.get_classes(),
                "--outdir",
                self.output_dir,
            ]
        )
        input_type = self.get_input_type()
        if self.joboptions["do_recenter"].get_boolean() and input_type == "Class2D":
            coms.append(
                [
                    "relion_image_handler",
                    "--i",
                    str(Path(self.output_dir) / "class_averages.mrcs"),
                    "--shift_com",
                    "true",
                    "--o",
                    str(Path(self.output_dir) / "class_averages_centered.mrcs"),
                ]
            )
            coms.append(
                [
                    "python3",
                    get_job_script("pipeliner_select/write_centered_clavs_starfile.py"),
                    "--optimiser_file",
                    self.joboptions["fn_optimiser"].get_string(),
                    "--classes",
                    *self.get_classes(),
                    "--outdir",
                    self.output_dir,
                ]
            )

        # for particles
        data_file = (
            self.joboptions["fn_optimiser"].get_string().replace("optimiser", "data")
        )
        coms.append(
            [
                "python3",
                get_job_script("pipeliner_select/make_particles_star.py"),
                "--data_file",
                data_file,
                "--classes",
                *self.get_classes(),
                "--outdir",
                self.output_dir,
            ]
        )
        return [PipelinerCommand(x) for x in coms]

    def gather_metadata(self) -> Dict[str, Any]:
        md_dict: Dict[str, object] = {}
        md_dict["selected_classes"] = [int(x) for x in self.get_classes()]
        data_star = DataStarFile(str(Path(self.output_dir) / "particles.star"))
        md_dict["n_selected_particles"] = data_star.count_block("particles")
        return md_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        intype = self.get_input_type()
        if intype == "Class2D":
            dispobjs = [
                x.default_results_display(self.output_dir) for x in self.output_nodes
            ]
        else:
            parts_node = [
                x for x in self.output_nodes if Path(x.name).name == "particles.star"
            ][0]
            dispobjs = [parts_node.default_results_display(self.output_dir)]
            clfile = StarFile(Path(self.output_dir) / "selected_classes.star")
            classes = clfile.loop_as_list(
                block="3d_classes", columns=["_rlnReferenceImage"]
            )
            for cl in classes:
                dispobjs.append(
                    create_results_display_object(
                        "mapmodel",
                        title=f"Selected class: {cl[0]}",
                        maps=cl,
                        associated_data=cl,
                    )
                )

        return dispobjs
