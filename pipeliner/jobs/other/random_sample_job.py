#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import List, Tuple, Sequence
from pathlib import Path

from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    IntJobOption,
    files_exts,
    JobOptionValidationResult,
    JobOptionCondition,
    JobOption,
)
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHCOORDS,
)
from pipeliner.display_tools import (
    mini_montage_from_starfile,
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class RandomSampleStarfile(PipelinerJob):
    PROCESS_NAME = "pipeliner.select.random_sample"
    OUT_DIR = "Select"
    CATEGORY_LABEL = "Subset Selection"
    BLOCKS = {
        NODE_PARTICLEGROUPMETADATA: "particles",
        NODE_MICROGRAPHMOVIEGROUPMETADATA: "movies",
        NODE_MICROGRAPHGROUPMETADATA: "micrographs",
        NODE_MICROGRAPHCOORDSGROUP: "coordinate_files",
        NODE_MICROGRAPHCOORDS: "coordinates",
    }

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Random sample from starfile"

        self.jobinfo.short_desc = (
            "Take a random sampling of a specified size from a starfile"
        )
        self.jobinfo.long_desc = (
            "Makes a new starfile with a user specified number of items from the input "
            "file.  Operates on starfiles containing images, micrographs, movies, "
            "particles, or coordinates"
        )
        self.jobinfo.programs = []
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.references = []

        self.joboptions["fn_movs"] = InputNodeJobOption(
            label="Sample from movies file",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            pattern=(files_exts("Movies STAR file", [".star"])),
            help_text="Select a starfile containing micrograph movies",
            deactivate_if=JobOptionCondition(
                [
                    ("fn_mics", "!=", ""),
                    ("fn_parts", "!=", ""),
                    ("fn_coord_files", "!=", ""),
                    ("fn_coords", "!=", ""),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["fn_mics"] = InputNodeJobOption(
            label="OR: Sample from micrographs file",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            pattern=(files_exts("Micrographs STAR file", [".star"])),
            help_text="Select a starfile containing micrographs",
            deactivate_if=JobOptionCondition(
                [
                    ("fn_movs", "!=", ""),
                    ("fn_parts", "!=", ""),
                    ("fn_coord_files", "!=", ""),
                    ("fn_coords", "!=", ""),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["fn_parts"] = InputNodeJobOption(
            label="OR: Sample from particles file",
            node_type=NODE_PARTICLEGROUPMETADATA,
            pattern=(files_exts("Particles STAR file", [".star"])),
            help_text="Select a starfile containing extracted particles",
            deactivate_if=JobOptionCondition(
                [
                    ("fn_mics", "!=", ""),
                    ("fn_movs", "!=", ""),
                    ("fn_coord_files", "!=", ""),
                    ("fn_coords", "!=", ""),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["fn_coord_files"] = InputNodeJobOption(
            label="OR: Sample coordinate files from a list",
            node_type=NODE_MICROGRAPHCOORDSGROUP,
            pattern=(files_exts("Coordinate files STAR file", [".star"])),
            help_text=(
                "Select a starfile containing micrograph coordinate files, such as "
                "an autopick.star file"
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("fn_mics", "!=", ""),
                    ("fn_parts", "!=", ""),
                    ("fn_movs", "!=", ""),
                    ("fn_coords", "!=", ""),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["fn_coords"] = InputNodeJobOption(
            label="OR: Sample points from a coordinates file",
            node_type=NODE_MICROGRAPHCOORDS,
            pattern=(files_exts("Coordinates STAR file", [".star"])),
            help_text=(
                "Select a file that contains a list of coordinates for a single "
                "micrograph"
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("fn_mics", "!=", ""),
                    ("fn_parts", "!=", ""),
                    ("fn_coord_files", "!=", ""),
                    ("fn_movs", "!=", ""),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["n_samples"] = IntJobOption(
            label="Number of samples:",
            default_value=1,
            hard_min=1,
            help_text="Number of samples to take",
            in_continue=True,
            is_required=True,
        )
        self.inputs = [
            self.joboptions["fn_movs"],
            self.joboptions["fn_mics"],
            self.joboptions["fn_parts"],
            self.joboptions["fn_coord_files"],
            self.joboptions["fn_coords"],
        ]

    def get_input_jo(self) -> InputNodeJobOption:
        """Get the input node joboption that was filled out"""
        inputs: List[JobOption] = [x for x in self.inputs if x.get_string()]
        if not len(inputs) == 1:
            raise ValueError("Select ONE input file")
        assert isinstance(inputs[0], InputNodeJobOption)
        return inputs[0]

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        if not any([x.get_string() for x in self.inputs]):
            return [
                JobOptionValidationResult(
                    "error",
                    raised_by=self.inputs,
                    message="One input file must be selected",
                )
            ]
        return []

    def get_commands(self) -> List[PipelinerCommand]:
        in_jo = self.get_input_jo()
        nsamples = str(self.joboptions["n_samples"].get_number())
        command = [
            "python3",
            get_job_script("starfile_random_sample.py"),
            "--input_file",
            in_jo.get_string(),
            "--n_samples",
            nsamples,
            "--block",
            self.BLOCKS[in_jo.node_type],
            "--output",
            str(Path(self.output_dir) / self.get_output_file_name_and_nodetype()[0]),
        ]
        return [PipelinerCommand(command)]

    def get_output_file_name_and_nodetype(self) -> Tuple[str, str]:
        in_jo = self.get_input_jo()
        file_type = self.BLOCKS[in_jo.node_type]
        nodetype = in_jo.node_type
        return f"sampled_{file_type}.star", nodetype

    def create_output_nodes(self) -> None:
        self.add_output_node(*self.get_output_file_name_and_nodetype())

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        outfile, nodetype = self.get_output_file_name_and_nodetype()
        colname = {
            NODE_PARTICLEGROUPMETADATA: "_rlnImageName",
            NODE_MICROGRAPHMOVIEGROUPMETADATA: "_rlnMicrographMovieName",
            NODE_MICROGRAPHGROUPMETADATA: "_rlnMicrographName",
        }
        if nodetype not in (NODE_MICROGRAPHCOORDSGROUP, NODE_MICROGRAPHCOORDS):
            return [
                mini_montage_from_starfile(
                    str(Path(self.output_dir) / outfile),
                    self.BLOCKS[nodetype],
                    colname[nodetype],
                    self.output_dir,
                )
            ]
        else:
            return [
                create_results_display_object(
                    "textfile",
                    file_path=str(Path(self.output_dir) / outfile),
                )
            ]
