#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Union
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayPending,
)
from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    MultiStringJobOption,
    files_exts,
    DirPathJobOption,
)
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS


class MoorhenJob(PipelinerJob):
    PROCESS_NAME = "moorhen.atomic_model_refine"
    OUT_DIR = "Moorhen"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Moorhen"

        self.jobinfo.short_desc = (
            "Use Moorhen (Coot) to view, edit and refine atomic models"
        )
        self.jobinfo.long_desc = (
            "Moorhen is a web-based molecular graphics program based on the Coot"
            " desktop program."
        )
        self.jobinfo.programs = []
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, George Coldstream"
        self.jobinfo.references = []
        self.jobinfo.documentation = ""
        self.jobinfo.alternative_unavailable_reason = (
            "This job is not available in the GUI"
        )

        self.joboptions["input_maps"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("Density map", [".mrc"]),
            help_text=(
                "The density maps that will be used for fitting and refining models"
            ),
            is_required=False,
        )

        self.joboptions["input_models"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_ATOMCOORDS,
            default_value="",
            pattern=files_exts(
                "Atomic coordinates", [".cif", ".mmcif", ".pdb", ".ent"]
            ),
            help_text="Atomic coordinates for the model(s) to be manually refined",
            is_required=False,
        )

        self.joboptions["moorhen_tmp_dir"] = DirPathJobOption(
            label="Moorhen tmp dir",
            default_value="",
            help_text="Contains all the data written by the moorhen session",
            is_required=True,
        )

        self.joboptions["saved_models_basenames"] = MultiStringJobOption(
            label="Saved models basenames",
            default_value="",
            help_text=(
                "Atomic coordinates for the edited model(s) saved by moorhen, only "
                "give the file basenames not the full path"
            ),
            is_required=True,
        )

        self.joboptions["saved_maps_basenames"] = MultiStringJobOption(
            label="Input map",
            default_value="",
            help_text=(
                "Density map(s) imported and saved by moorhen, only "
                "give the file basenames not the full path"
            ),
            is_required=False,
        )

    def create_output_nodes(self):
        for of_basename in self.joboptions["saved_models_basenames"].get_list():
            self.add_output_node(of_basename, NODE_ATOMCOORDS, ["manual_refine"])

        if self.joboptions["saved_maps_basenames"]:
            for of_basename in self.joboptions["saved_maps_basenames"].get_list():
                self.add_output_node(of_basename, NODE_DENSITYMAP, ["manual_refine"])

    def get_commands(self) -> List[PipelinerCommand]:
        tmpdir = self.joboptions["moorhen_tmp_dir"].get_string()
        com1 = PipelinerCommand(
            ["cp", "-r", str(os.path.join(tmpdir, ".")), self.output_dir]
        )
        com2 = PipelinerCommand(["rm", "-r", tmpdir])
        return [com1, com2]

    def create_results_display(
        self,
    ) -> List[Union[ResultsDisplayMapModel, ResultsDisplayPending]]:

        # get the models
        written_models = self.joboptions["saved_models_basenames"].get_list()
        original_models = self.joboptions["input_models"].get_list()
        written_maps = self.joboptions["saved_maps_basenames"].get_list()
        original_maps = self.joboptions["input_maps"].get_list()
        map_list = []

        # get the models to display, show the edited ones if they exist otherwise show
        # the original model
        model_list = []
        wmods = [x.replace("_moorhen.pdb", "") for x in written_models]
        for model in original_models:
            fn = os.path.basename(model).split(".")[0]
            if fn not in wmods:
                model_list.append(model)
        model_list.extend([os.path.join(self.output_dir, x) for x in written_models])

        # get the maps to display, show the imported ones if they exist otherwise show
        # the original model
        maps_wmods = [x.replace("_moorhen.map", "") for x in written_maps]
        for map in original_maps:
            fn = os.path.basename(map).split(".")[0]
            if fn not in maps_wmods:
                map_list.append(map)
        map_list.extend([os.path.join(self.output_dir, x) for x in written_maps])

        return [
            make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=map_list,
                maps_opacity=[0.5] * len(map_list),
                models=model_list,
            ),
        ]
