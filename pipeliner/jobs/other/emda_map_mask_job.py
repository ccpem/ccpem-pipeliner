#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import Sequence, List

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    make_mrcs_central_slices_montage,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.nodes import NODE_DENSITYMAP, NODE_MASK3D
from pipeliner.results_display_objects import ResultsDisplayObject


class EmdaMapMask(PipelinerJob):
    PROCESS_NAME = "emda.mask_creation.half_maps"
    OUT_DIR = "EmdaMapMask"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Create Mask"
        self.jobinfo.short_desc = "Create mask from half-maps"
        self.jobinfo.long_desc = (
            "Calculates mask from half-maps using EMDA library (requires version 2)"
        )
        self.jobinfo.programs = [ExternalProgram("emda2")]
        self.version = "1.1.6.post2"
        self.job_author = "Rangana Warshamanage, Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Wojdyr M"],
                title=("GEMMI: A library for structural biology"),
                journal="Journal of Open Source Software",
                year="2022",
                volume="7",
                issue="43",
                pages="4200",
                doi="10.21105/joss.04200",
            )
        ]
        self.jobinfo.documentation = "https://emda.readthedocs.io/"
        self.joboptions["half_map1"] = InputNodeJobOption(
            label="Input half map1",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            default_value="",
            help_text="Input halfmap 1, preferrably raw or not post-processed",
            is_required=True,
        )
        self.joboptions["half_map2"] = InputNodeJobOption(
            label="Input half map2",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            default_value="",
            help_text="Input halfmap 2, preferrably raw or not post-processed",
            is_required=True,
        )
        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "emda_halfmapmask_1.mrc",
            NODE_MASK3D,
            ["emda", "mapmask"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get parameters
        input_halfmap1 = self.joboptions["half_map1"].get_string()
        input_halfmap1 = os.path.relpath(input_halfmap1, self.output_dir)
        input_halfmap2 = self.joboptions["half_map2"].get_string()
        input_halfmap2 = os.path.relpath(input_halfmap2, self.output_dir)
        command = [self.jobinfo.programs[0].command, "halfmapmask"]
        command += ["--half1", input_halfmap1]
        command += ["--half2", input_halfmap2]

        return [PipelinerCommand(command)]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        input_halfmap1 = self.joboptions["half_map1"].get_string()
        output_mask = os.path.join(self.output_dir, "emda_halfmapmask_1.mrc")
        return [
            make_mrcs_central_slices_montage(
                in_files={output_mask: "Mask"},
                output_dir=self.output_dir,
            ),
            make_map_model_thumb_and_display(
                maps=[input_halfmap1, output_mask],
                maps_opacity=[1.0, 0.5],
                outputdir=self.output_dir,
            ),
        ]
