#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import logging
from typing import List, Optional, Dict, Union, Sequence
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_SEQUENCE,
)
from pipeliner.utils import get_job_script

logger = logging.getLogger(__name__)


class ModelSequenceValidate(PipelinerJob):
    PROCESS_NAME = "checkmysequence.atomic_model_validation"
    OUT_DIR = "CheckMySequence"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "checkMySequence"
        self.jobinfo.short_desc = (
            "Check modeled sequence against expected sequence and EM map"
        )
        self.jobinfo.long_desc = (
            "Check modeled sequence, given:\n"
            "the expected sample sequence and \n"
            "experimental map used to build the model.\n"
            "The program identifies issues such as: \n"
            "mismatches against sample sequence, \n"
            "Sequence register errors \n"
            "Unexpected chains in the model \n"
            "Sequence indexing errors and \n"
            "Chain tracing issues."
        )

        self.jobinfo.programs = [
            ExternalProgram("checkmysequence"),
        ]
        self.jobinfo.version = "0.14"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Chojnowski G"],
                title=(
                    "Sequence assignment validation in cryo-EM models with"
                    " checkMySequence"
                ),
                journal="Acta Cryst.",
                year="2022",
                volume="D78",
                pages="806-816",
                doi="10.1107/S2059798322005009",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S205979832101278X"

        # JOB OPTIONS
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="The input model to be evaluated",
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The input map to evaluate the model against",
            is_required=True,
        )

        self.joboptions["input_sequence"] = InputNodeJobOption(
            label="Input sample sequence",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text="Reference sequence to validate model against",
            is_required=True,
        )
        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        mapid = os.path.splitext(os.path.basename(input_map))[0]

        # Now make the nodes
        self.add_output_node(
            modelid + "_" + mapid + "checkseq.json",
            NODE_LOGFILE,
            ["checkmysequence", "json_out"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        input_map = os.path.relpath(input_map, self.working_dir)
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        input_seq = self.joboptions["input_sequence"].get_string()
        input_seq = os.path.relpath(input_seq, self.working_dir)

        # checkmysequence
        commands = self.get_checkmysequence_commands(
            input_map, input_model, input_seq, modelid, mapid
        )
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_checkmysequence_commands(
        input_map: str,
        input_model: str,
        input_sequence: str,
        modelid: str,
        mapid: str,
    ) -> List[List[str]]:
        # pdb_num = 1  # this needs to be set for multiple model input
        checkseq_command: List[str] = ["checkmysequence"]
        checkseq_command += ["--mapin", input_map]
        checkseq_command += ["--modelin", input_model]
        checkseq_command += ["--seqin", input_sequence]
        output_json = modelid + "_" + mapid + "checkseq.json"
        checkseq_command += ["--jsonout", output_json]
        checkseq_results_command: List[str] = [
            "python3",
            get_job_script("model_validation/get_checkmyseq_results.py"),
            "-j",
            output_json,
            "-id",
            modelid + "_" + mapid,
        ]
        return [
            checkseq_command,
            checkseq_results_command,
        ]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # TODO: long files names to be trimmed ?
        input_model = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model, self.output_dir)
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        display_objects: List[ResultsDisplayObject] = []
        checkseq_table = self.create_checkseq_table(self.output_dir, modelid, mapid)
        if checkseq_table:
            display_objects.append(checkseq_table)
        display_objects.extend(
            self.create_checkmysequence_reports(self.output_dir, modelid, mapid)
        )
        display_objects.extend(self.create_checkmysequence_plots(modelid, mapid))
        return display_objects

    @staticmethod
    def create_checkseq_table(
        output_dir: str, modelid: str, mapid: str, max_per_line: int = 5
    ) -> Optional[ResultsDisplayObject]:
        labels = ["Issues", "chains affected"]
        out_id = modelid + "_" + mapid
        glob_out = os.path.join(output_dir, out_id + "_checkseq_glob.json")
        with open(glob_out, "r") as j:
            dict_glob = json.load(j)
        list_checkseqdata = []
        if dict_glob:
            for metric in dict_glob:
                if metric == "register_shifts":
                    continue  # skip reg shifts
                results_set = list(set(dict_glob[metric]))
                if len(results_set) > max_per_line:
                    for i in range(0, len(set(dict_glob[metric])), max_per_line):
                        list_checkseqdata.append(
                            [metric, ", ".join(results_set[i : i + max_per_line])]
                        )
                else:
                    list_checkseqdata.append([metric, ", ".join(results_set)])
        if list_checkseqdata:
            checkseq_summary = create_results_display_object(
                "table",
                title="CheckMysequence report",
                headers=labels,
                header_tooltips=[
                    "Issues identified",
                    "Chains where issues were found",
                ],
                table_data=list_checkseqdata,
                associated_data=[glob_out],
            )
            return checkseq_summary
        return None

    @staticmethod
    def get_chain_report(
        chain_id: str, dict_chainout: dict, chain_out: str
    ) -> ResultsDisplayObject:
        disp_obj = create_results_display_object(
            "text",
            title=f"CheckmySequence Chain: {chain_id}",
            display_data=dict_chainout[chain_id],
            associated_data=[chain_out],
            start_collapsed=False,
        )
        return disp_obj

    def create_checkmysequence_reports(
        self, output_dir: str, modelid: str, mapid: str
    ) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        chain_out = os.path.join(
            output_dir, modelid + "_" + mapid + "_checkseq_chain.json"
        )
        with open(chain_out, "r") as j:
            dict_chainout = json.load(j)
        if dict_chainout:
            for chain_id in dict_chainout:
                display_objects.append(
                    self.get_chain_report(chain_id, dict_chainout, chain_out)
                )
        return display_objects

    def create_checkmysequence_plots(
        self, modelid: str, mapid: str
    ) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        plot_out = os.path.join(
            self.output_dir, modelid + "_" + mapid + "_checkseq_plot.json"
        )
        with open(plot_out, "r") as j:
            dict_fragplot = json.load(j)
        glob_out = os.path.join(
            self.output_dir, modelid + "_" + mapid + "_checkseq_glob.json"
        )
        with open(glob_out, "r") as j:
            dict_glob = json.load(j)

        if dict_fragplot:
            for chain_id in dict_fragplot:
                display_objects.append(
                    self.get_chain_plot(dict_glob, chain_id, dict_fragplot, plot_out)
                )
        return display_objects

    @staticmethod
    def get_chain_plot(
        dict_glob: dict, chain_id: str, dict_fragplot: dict, plot_out: str
    ) -> ResultsDisplayObject:
        data = []
        colors = []
        # this is a complex list of different element types
        list_series_args: List[Dict[str, Union[str, dict, List[str]]]] = []
        nfrag = 0
        if dict_glob and "register_shifts" in dict_glob:
            for frag in dict_fragplot[chain_id][0]:
                flagi = False

                frag_split = frag.strip().split("-")
                for shifts in dict_glob["register_shifts"]:
                    if chain_id in shifts:
                        start_shift = shifts[chain_id][0]
                        end_shift = shifts[chain_id][1]
                        try:
                            if (
                                int(frag_split[0])
                                <= int(start_shift)
                                < int(frag_split[1])
                            ) or (
                                int(frag_split[0])
                                <= int(end_shift)
                                < int(frag_split[1])
                            ):
                                if dict_fragplot[chain_id][1][nfrag] > 1.0:
                                    colors.append("#FF0000")
                                    flagi = True
                                break
                        except (TypeError, ValueError) as e:
                            logger.exception(e)
                if not flagi:
                    colors.append("#808080")
                nfrag += 1
        else:
            colors = ["#808080"] * len(dict_fragplot[chain_id][1])
        flag = (
            "The bars are proportional to the -log(p-value) and "
            "the higher they are, the more reliable the sequence of a fragment.\n"
            "Grey bars: Expected fragment sequence and model sequence match. "
        )
        if "#FF0000" in colors:
            flag += (
                "\n If a bar is red, a high-confidence sequence assigned to a "
                "fragment and corresponding model sequence differ. "
                "This may be an error!"
            )
        data.append(
            {
                "x": dict_fragplot[chain_id][0],
                "y": dict_fragplot[chain_id][1],
            }
        )
        list_series_args.append(
            {
                "name": "sequence confidence",
                "marker_color": colors,
            }
        )  # plot trace args
        # -log(p) 0.85 or
        if "protein" in chain_id:
            line_name = "-log(p)=0.85"
            line_pval = {
                "x": [dict_fragplot[chain_id][0][0], dict_fragplot[chain_id][0][-1]],
                "y": [0.85, 0.85],
            }
        else:
            line_name = "-log(p)=0.70"
            line_pval = {
                "x": [dict_fragplot[chain_id][0][0], dict_fragplot[chain_id][0][-1]],
                "y": [0.70, 0.70],
            }

        data.append(line_pval)
        list_series_args.append(
            {
                "name": line_name,
                "mode": "lines",
                "line": {"color": "black", "dash": "dashdot", "width": 2},
            }
        )  # plot trace name (legend)
        xaxes_args = {
            "title_text": "Residue_fragments",
        }
        yaxes_args = {
            "title_text": "-log(p-value)",
        }
        disp_obj = create_results_display_object(
            "plotlyobj",
            data=data,
            plot_type=["Bar", "Scatter"],
            subplot=False,
            multi_series=True,
            series_args=list_series_args,
            xaxes_args=xaxes_args,
            yaxes_args=yaxes_args,
            title=f"CheckMySequence sequence p-values, {chain_id}",
            associated_data=[plot_out],
            flag=flag,
        )
        return disp_obj
