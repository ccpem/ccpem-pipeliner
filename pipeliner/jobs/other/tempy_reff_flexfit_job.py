#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pathlib import Path
from typing import List, Sequence, Dict, Any, Union
import json
import shutil
import glob
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
    # StringJobOption,
    MultipleChoiceJobOption,
    JobOptionValidationResult,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_PROCESSDATA,
    NODE_RESTRAINTS,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.jobs.ccpem.ribfind_job import RigidBodySplit
from pipeliner.node_factory import create_node


class FlexfitRefine(PipelinerJob):
    PROCESS_NAME = "tempy_reff.atomic_model_refine.flexible_fit"
    OUT_DIR = "TEMPyReFF-FlexFit"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "TEMPy-ReFF FlexFit"
        self.jobinfo.short_desc = "Rigid-body based flexible fitting with TEMPy-ReFF"
        self.jobinfo.long_desc = (
            "Flexible fitting and refinement of atomic structures guided by cryoEM "
            "density. N.B. ligands and waters are not currently supported and will be "
            "automatically removed from the input model. If RIBFIND output is not "
            "provided, RIBFIND will be run internally. This requires RIBFIND2 "
            "installed."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(command="tempy-reff"),
            ExternalProgram(command="gemmi"),
            RigidBodySplit.ribfind_program,
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Mulvaney T",
                    "Cragnolini T",
                    "Beton J",
                    "Topf M",
                ],
                title=(
                    "Cryo-EM structure and B-factor refinement with ensemble"
                    " representation"
                ),
                journal="bioRXiv",
                year="2022",
                doi="10.1101/2022.06.08.495259",
            ),
        ]
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="The input model to be fitted/refined",
            is_required=True,
        )
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0.1,
            step_value=0.1,
            help_text="Resolution in angstroms",
            is_required=True,
        )
        self.joboptions["ribfind_output_files"] = InputNodeJobOption(
            label="RIBFIND2 metadata (json)",
            node_type=NODE_RESTRAINTS,
            default_value="",
            help_text=(
                "Optional, by default RIBFIND2 will be run automatically. The "
                "output metadata json file from RIBFIND2 (if run using Doppio)."
            ),
            is_required=False,
        )
        self.joboptions["rna_fitting"] = BooleanJobOption(
            label="Is it an RNA structure?",
            default_value=False,
            help_text=(
                "If Yes, the rigid body restraints from RNA are used. RIBFIND2 uses an "
                "RNA clustering approach based on RNA secondary structures identified "
                "by RNAview. If it has non-RNA components, only the RNA is used. "
                "Structures with both protein and nucleic acid components are not "
                "supported at the moment."
            ),
        )
        self.joboptions["force_density"] = FloatJobOption(
            label="Weight of map fit",
            default_value=100,
            suggested_min=0,
            suggested_max=1000,
            step_value=1,
            help_text=(
                "In refinements using the fitting density, the force applied to atoms "
                "is proportional the local gradients in the cryo EM map. These "
                "gradients are typically small, and we can speed up refinements by "
                "multiplying these gradients by this constant factor. If the refined "
                "model geometry worsens significantly with refinement, try reducing "
                "this value. On the other hand, if refinement is slow or the model "
                "doesn't seem to be moving into obvious areas of density, try "
                "increasing this value."
            ),
            is_required=False,
        )
        self.joboptions["convergence_type"] = MultipleChoiceJobOption(
            label="Convergence method",
            choices=["variance", "patience"],
            default_value_index=0,
            help_text=(
                "The 'variance' method checks whether the variance in a scoring "
                "function (normally the CCC between the model and cryo-EM map) is "
                "improving. The 'patience' approach, checks if the CCC between the map "
                "and model is improving, and terminates the refinement if it does not "
                "improve for a set number of steps. Recommend using the 'patience' "
                "method for the non RIBFIND refinement using GMMs."
            ),
        )
        self.joboptions["convergence_threshold"] = FloatJobOption(
            label="Convergence threshold",
            hard_min=0,
            default_value=1e-6,
            suggested_min=1e-8,
            suggested_max=1e-3,
            step_value=1e-7,
            help_text=(
                "When the CCC variance of the convergence runs drops below this value, "
                "stop or move to the next stage of fitting."
            ),
            deactivate_if=JobOptionCondition([("convergence_type", "!=", "variance")]),
            is_required=False,
        )
        self.joboptions["convergence_patience"] = IntJobOption(
            label="Steps for convergence",
            default_value=5,
            hard_min=0,
            suggested_min=3,
            suggested_max=20,
            step_value=1,
            help_text=(
                "The number of last conformations to test for convergence. Only "
                "valid for convergence type 'patience'"
            ),
            deactivate_if=JobOptionCondition([("convergence_type", "!=", "patience")]),
            is_required=False,
        )
        self.joboptions["convergence_timeout"] = IntJobOption(
            label="Timeout for convergence",
            default_value=100,
            hard_min=0,
            suggested_min=0,
            step_value=1,
            help_text=(
                "This sets the maximum number of steps to run before "
                "stopping refinement if convergence is not reached."
            ),
            is_required=False,
        )
        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU?",
            default_value=True,
            help_text=(
                "Use for running the MD simulation on a CUDA capable GPU. Select 'No' "
                "to do the run on the CPU."
            ),
        )
        self.joboptions["add_final_refinement"] = BooleanJobOption(
            label="Add a final refinement step?",
            default_value=False,
            help_text=(
                "If Yes, adds a final GMM based all atom refinement step after rigid "
                "body flexible fitting."
            ),
            jobop_group="GMM refinement",
        )
        self.joboptions["force_gmm"] = FloatJobOption(
            label="Weight of GMM fitting",
            default_value=5e4,
            suggested_min=10,
            suggested_max=1e7,
            step_value=10,
            help_text=(
                "If the model does not move into obvious cryo-EM density, try "
                "increasing the fitting force by, say, a factor of 2. Conversely, if "
                "the model geometry starts to get worse, try reducing the fitting "
                "force."
            ),
            is_required=False,
            deactivate_if=JobOptionCondition([("add_final_refinement", "=", False)]),
            jobop_group="GMM refinement",
        )
        self.joboptions["gmm_convergence_type"] = MultipleChoiceJobOption(
            label="Convergence method (GMM)",
            choices=["variance", "patience"],
            default_value_index=1,
            help_text=(
                "The 'variance' method checks whether the variance in a scoring "
                "function (normally the CCC between the model and cryo-EM map) is "
                "improving. The 'patience' approach, checks if the CCC between the map "
                "and model is improving, and terminates the refinement if it does not "
                "improve for a set number of steps. Recommend using the 'patience' "
                "method for the non RIBFIND refinement using GMMs."
            ),
            deactivate_if=JobOptionCondition([("add_final_refinement", "=", False)]),
            jobop_group="GMM refinement",
        )
        self.joboptions["gmm_convergence_threshold"] = FloatJobOption(
            label="Convergence threshold (GMM)",
            hard_min=0,
            default_value=1e-6,
            suggested_min=1e-8,
            suggested_max=1e-3,
            step_value=1e-7,
            help_text=(
                "When the CCC variance of the convergence runs drops below this value, "
                "stop or move to the next stage of fitting."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("gmm_convergence_type", "!=", "variance"),
                    ("add_final_refinement", "=", False),
                ]
            ),
            is_required=False,
            jobop_group="GMM refinement",
        )
        self.joboptions["gmm_convergence_patience"] = IntJobOption(
            label="Steps to check for convergence (GMM)",
            default_value=5,
            hard_min=0,
            suggested_min=3,
            suggested_max=20,
            step_value=1,
            help_text=(
                "The number of last conformations to test for convergence. Only "
                "valid for convergence type 'patience'"
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("gmm_convergence_type", "!=", "patience"),
                    ("add_final_refinement", "=", False),
                ]
            ),
            is_required=False,
            jobop_group="GMM refinement",
        )
        self.joboptions["gmm_convergence_timeout"] = IntJobOption(
            label="Timeout for convergence (GMM)",
            default_value=100,
            hard_min=0,
            suggested_min=0,
            step_value=1,
            help_text=(
                "This sets the maximum number of steps to run before just "
                "stopping refinement if convergence is not reached."
            ),
            is_required=False,
            deactivate_if=JobOptionCondition([("add_final_refinement", "=", False)]),
            jobop_group="GMM refinement",
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        # RIBFIND2
        if not self.joboptions["ribfind_output_files"].get_string():
            ribfind_version = RigidBodySplit.ribfind_program.get_version()
            if not shutil.which("ribfind"):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["ribfind_output_files"]],
                        message=(
                            "RIBFIND2 output not provided. RIBFIND2 "
                            "cannot be run internally as 'ribfind' is not available "
                            "in the system PATH, check that RIBFIND2 is installed. "
                        ),
                    )
                )
            # RIBFIND2 v2.*
            elif ribfind_version and (
                len(ribfind_version.split()) != 2
                or ribfind_version.split()[1][1] in ["1"]
            ):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["ribfind_output_files"]],
                        message=(
                            "RIBFIND2 output not provided. RIBFIND2 cannot be run "
                            "internally as RIBFIND version 2.* is not available. "
                            "This job requires RIBFIND version 2 installed. "
                        ),
                    )
                )
            if self.joboptions["rna_fitting"].get_boolean():
                if not shutil.which("rnaview"):
                    errors.append(
                        JobOptionValidationResult(
                            result_type="error",
                            raised_by=[self.joboptions["rna_fitting"]],
                            message=(
                                "'rnaview' is not available in the system PATH, "
                                "check that RNAView is installed. "
                            ),
                        )
                    )
            elif not shutil.which("mkdssp"):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["ribfind_output_files"]],
                        message=(
                            "RIBFIND2 output not provided. RIBFIND2 "
                            "cannot be run internally as 'mkdssp' is not available in "
                            "the system PATH, check that CCP4 is installed. RIBFIND2 "
                            "uses this to identify protein secondary structures. "
                        ),
                    )
                )
        return errors

    def create_output_nodes(self) -> None:
        # rigid body fitting
        self.add_output_node(
            os.path.join("flex_fit_out", "final.pdb"),
            NODE_ATOMCOORDS,
            ["tempy_reff", "ribfind_fit"],
        )
        # run refinement as second step
        if self.joboptions["add_final_refinement"].get_boolean():
            self.add_output_node(
                os.path.join("gmm_out", "final_gmm.cif"),
                NODE_ATOMCOORDS,
                ["tempy_reff", "gmm_fit"],
            )

    def create_post_run_output_nodes(self) -> None:
        input_model = self.joboptions["input_model"].get_string()
        model_id = Path(input_model).stem
        ribfind_dir = "ribfind_" + model_id
        # Find all rigid body text files
        list_rigidbody_files = []
        output_txts = self.get_ribfind_output_list(ribfind_dir)
        for txtfile in sorted(output_txts):
            self.output_nodes.append(
                create_node(txtfile, NODE_PROCESSDATA, ["ribfind", "rigid_body"])
            )
            list_rigidbody_files.append(txtfile)
        self.create_output_json(ribfind_dir, list_rigidbody_files)

    def create_output_json(self, ribfind_dir, list_rigidbody_files) -> None:
        dict_ribfind_outputs: Dict[str, Union[str, List[str]]] = {}
        dict_ribfind_outputs["output_dir"] = ribfind_dir
        dict_ribfind_outputs["rigid_body_files"] = list_rigidbody_files
        # protein or rna run?
        if self.joboptions["rna_fitting"].get_boolean():
            components = ["rna"]
        else:
            components = ["protein"]
        dict_ribfind_outputs["components"] = components
        # create a json file with output details
        ribfind_output_json = os.path.join(
            self.output_dir, ribfind_dir + "_outputs.json"
        )
        with open(ribfind_output_json, "w") as j:
            json.dump(dict_ribfind_outputs, j)
        self.output_nodes.append(
            create_node(
                ribfind_output_json, NODE_RESTRAINTS, ["ribfind", "output_files"]
            )
        )

    def get_ribfind_output_list(self, ribfind_dir: str) -> List[str]:
        # Find all rigid body text files
        output_txts = []
        if not self.joboptions["rna_fitting"].get_boolean():
            output_txts += glob.glob(
                os.path.join(
                    self.output_dir, ribfind_dir, "protein", "rigid_body_*.*.txt"
                )
            )
        else:
            output_txts += glob.glob(
                os.path.join(self.output_dir, ribfind_dir, "rna", "rigid_body_*.*.txt")
            )
        return output_txts

    def get_ribfind_dir_from_json(self, ribfind_output_json):
        # with open(ribfind_output_json, "r") as j:
        #     dict_ribfind_outputs = json.load(j)
        # ribfind_output_dir = dict_ribfind_outputs["output_dir"]
        # get dir name from filename instead of parsing the json content
        ribfind_output_dir = os.path.basename(ribfind_output_json).split("_outputs")[0]
        ribfind_output_dir = os.path.join(
            os.path.dirname(ribfind_output_json), ribfind_output_dir
        )
        if not self.joboptions["rna_fitting"].get_boolean():
            return os.path.join(ribfind_output_dir, "protein")
        else:
            return os.path.join(ribfind_output_dir, "rna")

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get inputs
        input_model_orig = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model_orig, self.working_dir)
        model_id = Path(input_model).stem
        input_map_orig = self.joboptions["input_map"].get_string()
        input_map = os.path.relpath(input_map_orig, self.working_dir)
        resolution = str(self.joboptions["resolution"].get_number())
        use_gpu = self.joboptions["use_gpu"].get_boolean()
        # Remove waters and ligands from input model with Gemmi convert
        # also ensure .pdb is generated
        model_no_wat_lig_rel = RigidBodySplit.get_processed_model_name(
            model_id=model_id
        )
        model_process_command = RigidBodySplit.get_process_model_command(
            input_model=input_model, model_id=model_id
        )
        commands += [model_process_command]

        models_to_score = [model_no_wat_lig_rel]
        tempy_reff_command = [
            "tempy-reff",
            "--model",
            model_no_wat_lig_rel,
            "--map",
            input_map,
            "--resolution",
            resolution,
        ]
        tempy_reff_command += self.get_convergence_options(
            convergence_type=self.joboptions["convergence_type"].get_string(),
            convergence_threshold=self.joboptions["convergence_threshold"].get_number(),
            convergence_patience=self.joboptions["convergence_patience"].get_number(),
            convergence_timeout=self.joboptions["convergence_timeout"].get_number(),
        )
        if use_gpu:
            tempy_reff_command += ["--platform-cuda"]
        else:
            tempy_reff_command += ["--platform-cpu"]
        # rigid body fitting
        tempy_reff_command += [
            "--fitting-density",
            "--fitting-density-k",
            str(self.joboptions["force_density"].get_number()),
        ]

        tempy_reff_command += ["--restrain-ribfind"]
        ribfind_output_files = self.joboptions["ribfind_output_files"].get_string()
        if ribfind_output_files:
            ribfind_rigid_body_dir = self.get_ribfind_dir_from_json(
                os.path.relpath(ribfind_output_files, self.working_dir)
            )
            # use existing ribfind result
            tempy_reff_command += ["--restrain-ribfind-dir", ribfind_rigid_body_dir]
        else:
            # run ribfind
            rna_fitting = self.joboptions["rna_fitting"].get_boolean()
            # protein
            if not rna_fitting:
                # DSSP
                dssp_out = RigidBodySplit.get_dssp_outname()
                dssp_command = RigidBodySplit.get_dssp_command(model_no_wat_lig_rel)
                commands += [dssp_command]

                # Ribfind
                ribfind_dir = "ribfind_" + model_id
                ribfind_command = [
                    "ribfind",
                    "--model",
                    model_no_wat_lig_rel,
                    "--dssp",
                    dssp_out,
                    "--output-dir",
                    ribfind_dir,
                ]
                commands += [ribfind_command]

                tempy_reff_command += [
                    "--restrain-ribfind-dir",
                    os.path.join(ribfind_dir, "protein"),
                ]
            # RNA
            else:
                # RNAview
                rnaml_out = RigidBodySplit.get_rnaview_outname(model_no_wat_lig_rel)
                rnaview_command = RigidBodySplit.get_rnaview_command(
                    model_no_wat_lig_rel
                )
                commands += [rnaview_command]

                # Ribfind
                ribfind_dir = "ribfind_" + model_id
                ribfind_command = [
                    "ribfind",
                    "--model",
                    model_no_wat_lig_rel,
                    "--rnaml",
                    rnaml_out,
                    "--output-dir",
                    ribfind_dir,
                ]
                commands += [ribfind_command]

                tempy_reff_command += [
                    "--restrain-ribfind-dir",
                    os.path.join(ribfind_dir, "rna"),
                ]

        tempy_reff_command += ["--output-dir", "flex_fit_out"]
        models_to_score.append(os.path.join("flex_fit_out", "final.pdb"))
        commands += [tempy_reff_command]

        # run refinement as second step
        if self.joboptions["add_final_refinement"].get_boolean():
            add_tempy_reff_command = [
                "tempy-reff",
                "--model",
                os.path.join("flex_fit_out", "final.pdb"),
                "--map",
                input_map,
                "--resolution",
                resolution,
            ]
            add_tempy_reff_command += self.get_convergence_options(
                convergence_type=self.joboptions["gmm_convergence_type"].get_string(),
                convergence_threshold=self.joboptions[
                    "gmm_convergence_threshold"
                ].get_number(),
                convergence_patience=self.joboptions[
                    "gmm_convergence_patience"
                ].get_number(),
                convergence_timeout=self.joboptions[
                    "gmm_convergence_timeout"
                ].get_number(),
            )
            if use_gpu:
                add_tempy_reff_command += ["--platform-cuda"]
            add_tempy_reff_command += [
                "--fitting-gmm",
                "--fitting-gmm-k",
                str(self.joboptions["force_gmm"].get_number()),
                "--output-dir",
                "gmm_out",
            ]
            commands += [add_tempy_reff_command]

            # Gemmi convert to make cif output file
            # Rename gmm_out/final.pdb so SMOC score displays it separately from
            # flex_fit_out/final.pdb
            gemmi_cif = self.get_gemmi_convert_to_cif_command(
                os.path.join("gmm_out", "final.pdb"),
                os.path.join("gmm_out", "final_gmm.cif"),
                remove_h=True,
            )
            commands += [gemmi_cif]
            models_to_score.append(os.path.join("gmm_out", "final_gmm.cif"))
        else:
            # Gemmi convert to make cif output file
            gemmi_cif = self.get_gemmi_convert_to_cif_command(
                os.path.join("flex_fit_out", "final.pdb"),
                os.path.join("flex_fit_out", "final.cif"),
                remove_h=True,
            )
            commands += [gemmi_cif]

        # run smoc scoring
        smoc_command = self.get_smoc_command(input_map, models_to_score, resolution)
        commands += [smoc_command]
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_convergence_options(
        convergence_type: str,
        convergence_threshold: float,
        convergence_patience: float,
        convergence_timeout: float,
    ) -> List[str]:
        conv_tempy_reff_command = []
        conv_tempy_reff_command += ["--convergence-type", convergence_type]
        if convergence_type == "variance":
            conv_tempy_reff_command += [
                "--convergence-threshold",
                str(convergence_threshold),
            ]
        else:
            conv_tempy_reff_command += [
                "--convergence-patience",
                str(convergence_patience),
            ]
        conv_tempy_reff_command += [
            "--convergence-timeout",
            str(convergence_timeout),
        ]
        return conv_tempy_reff_command

    @staticmethod
    def get_gemmi_convert_to_cif_command(
        input_pdb, output_cif, remove_h=False
    ) -> List[str]:
        gemmi_cif = [
            "gemmi",
            "convert",
        ]
        if remove_h:
            gemmi_cif += ["--remove-h"]
        gemmi_cif += [
            input_pdb,
            output_cif,
        ]
        return gemmi_cif

    @staticmethod
    def get_smoc_command(input_map, input_models, resolution) -> List[str]:
        smoc_command = ["TEMPy.smoc"]
        smoc_command += ["-m", input_map]
        smoc_command += ["--models"]
        smoc_command += input_models
        smoc_command += ["-r", resolution]
        smoc_command += ["--smoc-window", "5"]
        smoc_command += ["--output-format", "json"]
        return smoc_command

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_map = self.joboptions["input_map"].get_string()
        maps = [input_map]
        input_model = self.joboptions["input_model"].get_string()
        model_no_wat_lig_rel = Path(input_model).stem + "_no_wat_lig.pdb"
        models = [os.path.join(self.output_dir, model_no_wat_lig_rel)]
        colors = ["#000000"]
        models.append(os.path.join(self.output_dir, "flex_fit_out", "final.pdb"))
        colors.append("#00FF0F")
        add_final_refinement = self.joboptions["add_final_refinement"].get_boolean()
        if add_final_refinement:
            models.append(os.path.join(self.output_dir, "gmm_out", "final_gmm.cif"))
            colors.append("#FFC100")

        map_model_display = make_map_model_thumb_and_display(
            maps=maps,
            maps_opacity=[0.5],
            models=models,
            models_colours=colors,
            outputdir=self.output_dir,
        )
        display_objects.append(map_model_display)
        smoc_score_jsons = []
        map_id = Path(input_map).stem
        colors = ["black"]
        if len(models) == 3:
            colors.append("orange")
        colors.append("green")
        if len(colors) > 3 or len(colors) < 1:
            return display_objects
        dict_model_colors: Dict[str, Any] = {}
        dict_chain_scores: Dict[str, Any] = {}
        mod_count = 0
        # skip initial model from scoring for now
        # Atomic B-factors appear unrefined in the refined models
        for model in models:
            model_id = Path(model).stem
            smoc_json = f"SMOC_{model_id}_vs_{map_id}.json"
            smoc_score_jsons.append(smoc_json)
            if model_id not in dict_model_colors:
                dict_model_colors[model_id] = colors[mod_count]
            with open(os.path.join(self.output_dir, smoc_json), "r") as j:
                smoc_scores = json.load(j)
            for chain in smoc_scores["chains"]:
                list_resnum = []
                list_scores = []
                for resnum in smoc_scores["chains"][chain]:
                    try:
                        list_resnum.append(int(resnum))
                    except TypeError:
                        continue
                    list_scores.append(float(smoc_scores["chains"][chain][resnum]))
                try:
                    dict_chain_scores[chain].append(
                        [model_id, {"x": list_resnum, "y": list_scores}]
                    )
                except KeyError:
                    dict_chain_scores[chain] = [
                        [model_id, {"x": list_resnum, "y": list_scores}]
                    ]
            mod_count += 1
        for chain in dict_chain_scores:
            list_series_args = []
            data = []
            for name_data in dict_chain_scores[chain]:
                model_id = name_data[0]
                series_data = name_data[1]
                series_args = {
                    "name": model_id,
                    "mode": "lines",
                    "line": {"color": dict_model_colors[model_id], "width": 2},
                }
                list_series_args.append(series_args)
                data.append(series_data)

            xaxes_args = {
                "gridcolor": "lightgrey",
                "title_text": "Residue number",
            }
            yaxes_args = {
                "title_text": "SMOC score",
                "gridcolor": "lightgrey",
            }
            # multi_series plot
            disp_obj = create_results_display_object(
                "plotlyobj",
                data=data,
                plot_type="Scatter",  # all series scatter type
                subplot=False,
                multi_series=True,
                series_args=list_series_args,
                layout_args={
                    "plot_bgcolor": "white",
                },
                xaxes_args=xaxes_args,
                yaxes_args=yaxes_args,
                title="SMOC_residue_fit_chain_" + chain,
                associated_data=smoc_score_jsons,
                flag="",
                start_collapsed=True,
            )
            display_objects.append(disp_obj)

        return display_objects
