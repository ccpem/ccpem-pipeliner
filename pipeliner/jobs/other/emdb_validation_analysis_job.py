#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from typing import List, Dict, Any, Union, Sequence, Optional
import logging

from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_many_files,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    MultipleChoiceJobOption,
    files_exts,
    JobOptionCondition,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_EVALUATIONMETRIC, NODE_ATOMCOORDS
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, Ref, PipelinerCommand
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject

logger = logging.getLogger(__name__)


class MapModelValidate(PipelinerJob):
    PROCESS_NAME = "emdb_validation_analysis.map_model_validation"
    OUT_DIR = "EMDB_validation"
    emdbva_program = ExternalProgram(
        command="va", vers_com=["va", "--version"], vers_lines=[1]
    )

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "EMDB Validation Analysis"
        self.jobinfo.short_desc = "EMDB map and model validation"
        self.jobinfo.long_desc = (
            "EMDB map and atomic model validation and visual analysis. "
            "Reports multiple features, metrics and statistics.\n"
            "To install the EMDB validation analysis tool, use: \n"
            "pip install va==0.0.1.dev55 \n"
            "Source and builds are available here:\n"
            "https://pypi.org/project/va/#files"
            "\n\n To generate map surface views: "
            "install ChimeraX and add the executable to the system PATH"
        )
        self.jobinfo.programs = [
            self.emdbva_program,
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Wang Z", "Patwardhan A", "Kleywegt GJ"],
                title=("Validation analysis of EMDB entries"),
                journal="Acta Cryst D",
                year="2022",
                volume="78",
                issue="5",
                pages="542-552",
                doi="10.1107/S205979832200328X",
            ),
        ]
        self.jobinfo.documentation = "https://www.ebi.ac.uk/emdb/va/"
        self.jobinfo.alternative_unavailable_reason = self.check_right_emdbva_version()

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to be evaluated",
            is_required=True,
        )

        self.joboptions["exp_method"] = MultipleChoiceJobOption(
            label="Reconstruction method",
            choices=[
                "singleparticle",
                "tomography",
                "twodcrystal",
                "crystallography",
                "subtomogramaveraging",
                "helical",
            ],
            default_value_index=0,
            help_text="Choose the experimental type for structure determination.",
            is_required=False,
        )

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".mmcif"]),
            default_value="",
            help_text="The input model to be evaluated against the map",
            is_required=True,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in angstroms",
            required_if=JobOptionCondition([("input_model", "!=", "")]),
        )

        self.joboptions["contour"] = FloatJobOption(
            label="ContourLevel",
            default_value=-1000,
            step_value=0.05,
            help_text="Suggested contour level",
            required_if=JobOptionCondition([("input_model", "!=", "")]),
        )

        self.joboptions["half_map1"] = InputNodeJobOption(
            label="Input half map 1",
            node_type=NODE_DENSITYMAP,
            node_kwds=["halfmap"],
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map 1",
            is_required=False,
        )

        self.joboptions["half_map2"] = InputNodeJobOption(
            label="Input half map 2",
            node_type=NODE_DENSITYMAP,
            node_kwds=["halfmap"],
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map 2",
            is_required=False,
        )

        # TODO : add optional metrics to run
        # self.joboptions["run_smoc"] = BooleanJobOption(
        #     label="TEMPy SMOC",
        #     default_value=True,
        #     help_text="Run SMOC model-map fit evaluation",
        # )

        self.get_runtab_options(mpi=False, threads=False)

    def check_right_emdbva_version(self) -> str:
        emdbva_version = self.emdbva_program.get_version()
        # va: 0.0.1.dev45
        if emdbva_version:
            version_split = emdbva_version.split()
            try:
                version_number = int(version_split[1].split(".")[-1][3:])
                if version_number < 43 or version_number > 55:
                    return (
                        f"EMDB Validation version {emdbva_version} was found.\nThe"
                        f" version must be >= 0.0.1.dev43 and =< 0.0.1.dev55"
                    )

            except (IndexError, ValueError, TypeError):
                return "Could not find version of EMDB validation"

        return ""

    def create_output_nodes(self) -> None:
        input_map = self.joboptions["input_map"].get_string()
        mapname = os.path.basename(input_map)

        # outputs for map
        self.add_output_node(
            f"{mapname}_all.json", NODE_EVALUATIONMETRIC, ["emdb_va", "all_results"]
        )
        self.add_output_node(
            f"{mapname}_raps.json", NODE_EVALUATIONMETRIC, ["emdb_va", "raps"]
        )
        self.add_output_node(
            f"{mapname}_density_distribution.json",
            NODE_EVALUATIONMETRIC,
            ["emdb_va", "mapdistribution"],
        )

        # Outputs for model
        input_model = self.joboptions["input_model"].get_string()
        if input_model != "":
            self.add_output_node(
                f"{mapname}_mmfsc.json",
                NODE_EVALUATIONMETRIC,
                ["emdb_va", "mapmodelfsc"],
            )

        # Outputs for half maps
        input_hmap1 = self.joboptions["half_map1"].get_string()
        input_hmap2 = self.joboptions["half_map2"].get_string()
        if input_hmap1 != "" and input_hmap2 != "":
            self.add_output_node(
                f"{mapname}_fsc.json", NODE_EVALUATIONMETRIC, ["emdb_va", "halfmapfsc"]
            )

        # Outputs if contour level set
        contour = self.joboptions["contour"].get_number()
        if contour != -1000:
            self.add_output_node(
                f"{mapname}_atom_inclusion.json",
                NODE_EVALUATIONMETRIC,
                ["emdb_va", "atominclusion"],
            )
            self.add_output_node(
                f"{mapname}_residue_inclusion.json",
                NODE_EVALUATIONMETRIC,
                ["emdb_va", "residueinclusion"],
            )

    @staticmethod
    def get_cif_ext(input_model: str) -> Optional[str]:
        ext = os.path.splitext(input_model)[1]
        if ext.lower() in (".cif", ".mmcif"):
            return ext
        else:
            return None

    @staticmethod
    def get_cifconvert_command(
        input_model: str, input_model_corrected: str
    ) -> List[str]:
        gemmi_convert_model: List[str] = [
            "gemmi",
            "convert",
            "--shorten",
            input_model,
            input_model_corrected,
        ]
        return gemmi_convert_model

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        commands = []
        # copy inputs to output dir
        # TODO: check if big file copying can be avoided with symlinks?
        # Get parameters
        input_map = self.joboptions["input_map"].get_string()
        input_map_relpath = os.path.relpath(input_map, self.working_dir)
        input_map_basename = os.path.basename(input_map_relpath)

        commands.append(PipelinerCommand(["cp", input_map_relpath, "."]))
        # get map info
        commands.append(
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("get_map_parameters.py"),
                    "-m",
                    input_map_relpath,
                    "-odir",
                    ".",
                ]
            )
        )
        input_model = self.joboptions["input_model"].get_string()
        resolution = self.joboptions["resolution"].get_number()
        input_hmap1 = self.joboptions["half_map1"].get_string()
        input_hmap2 = self.joboptions["half_map2"].get_string()
        contour = self.joboptions["contour"].get_number()
        # run_smoc = self.joboptions["run_smoc"].get_boolean()

        # emdb validation analysis
        emdb_va_command: List[Union[str, float, int]] = [
            self.jobinfo.programs[0].command,
            "-m",
            input_map_basename,
        ]
        emdb_va_command += ["-met", self.joboptions["exp_method"].get_string()]
        emdb_va_command += ["-d", os.path.join(os.getcwd(), self.output_dir)]

        conv_com = [
            "python3",
            get_job_script("emdb_validation/convert_emdb_validation_images.py"),
            "--map",
            input_map_basename,
            "--outdir",
            ".",
        ]
        if input_model != "":
            input_model_relpath = os.path.relpath(input_model, self.working_dir)
            ext = os.path.splitext(input_model_relpath)[1]
            if ext.lower() not in (".cif", ".mmcif"):
                input_model_cif = (
                    os.path.splitext(os.path.basename(input_model_relpath))[0] + ".cif"
                )
                commands.append(
                    PipelinerCommand(
                        self.get_cifconvert_command(
                            input_model_relpath, input_model_cif
                        )
                    )
                )
                input_model_file = input_model_cif
            else:
                commands.append(PipelinerCommand(["cp", input_model_relpath, "."]))
                input_model_file = os.path.basename(input_model_relpath)
            # TODO: check if -i option needs to be used
            emdb_va_command += ["-f", input_model_file, "-i", "True"]
            conv_com += ["--model", input_model_file]

        if input_hmap1 != "" and input_hmap2 != "":
            input_hmap1_relpath = os.path.relpath(input_hmap1, self.working_dir)
            input_hmap2_relpath = os.path.relpath(input_hmap2, self.working_dir)
            commands.append(PipelinerCommand(["cp", input_hmap1_relpath, "."]))
            commands.append(PipelinerCommand(["cp", input_hmap2_relpath, "."]))
            emdb_va_command += ["-hmeven", os.path.basename(input_hmap1)]
            emdb_va_command += ["-hmodd", os.path.basename(input_hmap2)]
            conv_com.extend(
                [
                    "--hm1",
                    os.path.basename(input_hmap1),
                    "--hm2",
                    os.path.basename(input_hmap2),
                ]
            )

        if resolution != -1:
            emdb_va_command += ["-s", resolution]

        if contour != -1000:
            emdb_va_command += ["-cl", contour]

        commands.append(PipelinerCommand(emdb_va_command))
        commands.append(PipelinerCommand(conv_com))

        return commands

    def gather_metadata(self) -> Dict[str, object]:
        metadata_dict: Dict[str, Any] = {}
        input_map = self.joboptions["input_map"].get_string()
        all_output = os.path.join(
            self.output_dir, os.path.basename(input_map) + "_all.json"
        )
        with open(all_output, "r") as j:
            dict_all_out = json.load(j)
        dict_all_out = dict_all_out[os.path.basename(input_map)]
        metadata_dict["version"] = dict_all_out["version"]
        metadata_dict["volume_estimate"] = dict_all_out["volume_estimate"]["estvolume"]
        contour = self.joboptions["contour"].get_number()
        if contour != -1000:
            try:
                metadata_dict["contour_level"] = dict_all_out[
                    "recommended_contour_level"
                ]["recl"]
            except KeyError:
                logger.exception("Contour level data missing in: {}".format(all_output))
        input_model = self.joboptions["input_model"].get_string()
        if input_model != "":
            if "rmmccc" in dict_all_out:
                if (
                    os.path.basename(input_model) + "_modelmap.map"
                    in dict_all_out["rmmccc"]
                ):
                    metadata_dict[
                        "rmmccc_" + os.path.splitext(os.path.basename(input_model))[0]
                    ] = dict_all_out["rmmccc"][
                        os.path.basename(input_model) + "_modelmap.map"
                    ]
            if contour != -1000:
                try:
                    metadata_dict["atom_outside_contour"] = dict_all_out[
                        "atom_inclusion_by_level"
                    ]["0"]["atomoutside"]

                    for c in dict_all_out["atom_inclusion_by_level"]["0"][
                        "chainaiscore"
                    ]:
                        metadata_dict["chain_inclusion_score: {}".format(c)] = (
                            dict_all_out["atom_inclusion_by_level"]["0"][
                                "chainaiscore"
                            ][c]["value"]
                        )
                except KeyError:
                    logger.exception(
                        "Atom inclusion data missing in: {}".format(all_output)
                    )
        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_map = self.joboptions["input_map"].get_string()
        input_map = os.path.join(self.output_dir, os.path.basename(input_map))
        # half map inputs?
        input_hmap1 = self.joboptions["half_map1"].get_string()
        input_hmap2 = self.joboptions["half_map2"].get_string()
        contour = self.joboptions["contour"].get_number()
        resolution = self.joboptions["resolution"].get_number()
        # the output paths are relative to project dir
        map_basename = os.path.splitext(os.path.basename(input_map))[0]
        self.get_map_parameters_tableobj(map_basename, contour, display_objects)
        # map images
        self.add_map_image_objects(input_map, contour, display_objects)
        # half map inputs?
        input_hmap1_basename = None
        input_hmap2_basename = None
        if input_hmap1 != "" and input_hmap2 != "":
            input_hmap1_basename = os.path.basename(input_hmap1)
            input_hmap2_basename = os.path.basename(input_hmap2)
            self.add_halfmap_result_objects(
                input_hmap1_basename,
                input_hmap2_basename,
                contour,
                display_objects,
            )
        # voxel value distribution plot
        self.add_volume_distribution_plot(
            input_map,
            input_hmap1_basename,
            input_hmap2_basename,
            contour,
            display_objects,
        )
        # RAPS plot
        self.add_raps_plot(
            input_map,
            input_hmap1_basename,
            input_hmap2_basename,
            display_objects,
            resolution,
        )

        if input_hmap1 != "" and input_hmap2 != "":
            self.add_fsc_plot(input_map, display_objects, resolution)

        input_model = self.joboptions["input_model"].get_string()
        ext = os.path.splitext(input_model)[1]
        input_model_basename = os.path.basename(input_model)
        # if converted to cif
        if ext.lower() not in (".cif", ".mmcif"):
            input_model_basename = (
                os.path.splitext(os.path.basename(input_model))[0] + ".cif"
            )
        input_map_basename = os.path.basename(input_map)
        if input_model:
            input_model = os.path.join(self.output_dir, input_model_basename)
            # map with model views
            x_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_xsurface.tif"
            )
            y_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_ysurface.tif"
            )
            z_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_zsurface.tif"
            )
            if os.path.isfile(x_modelsurf):
                modelsurf_display = mini_montage_from_many_files(
                    [x_modelsurf, y_modelsurf, z_modelsurf],
                    self.output_dir,
                    nimg=3,
                    title="Primary map with model views",
                    ncols=3,
                    labels=["X", "Y", "Z"],
                )
                display_objects.append(modelsurf_display)
            # atom inclusion versus level
            map_data = input_map + "_atom_inclusion.json"
            if os.path.isfile(map_data):
                self.add_inclusion_plot(input_map, contour, display_objects)
            # model views
            x_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_xfitsurface.tif"
            )
            y_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_yfitsurface.tif"
            )
            z_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_zfitsurface.tif"
            )
            if os.path.isfile(x_modelfit):
                modelfit_display = mini_montage_from_many_files(
                    [x_modelfit, y_modelfit, z_modelfit],
                    self.output_dir,
                    nimg=3,
                    title="Model colored by atom inclusion per residue",
                    ncols=3,
                    labels=["X", "Y", "Z"],
                )
                display_objects.append(modelfit_display)
            res_inc = input_map + "_residue_inclusion.json"
            if contour != -1000 and os.path.isfile(res_inc):
                self.add_residue_inclusion_plot(input_map, contour, display_objects)
        return display_objects

    def add_fsc_plot(self, input_map, display_objects, resolution) -> None:
        out_halfmap_fsc = os.path.join(
            self.output_dir, os.path.basename(input_map) + "_fsc.json"
        )
        with open(out_halfmap_fsc, "r") as m:
            dict_fsc_results = json.load(m)
        dict_fsc_plot = dict_fsc_results["fsc"]["curves"]

        list_data = []
        series_args = []
        # fsc
        fsc_data = {
            "x": dict_fsc_plot["level"],
            "y": dict_fsc_plot["fsc"],
        }
        list_data.append(fsc_data)
        series_args.append(
            {
                "name": "Calculated FSC",
                "mode": "lines",
                "line": {"color": "orange"},
            }
        )  # plot trace name (legend)
        associated_data = [out_halfmap_fsc]
        # fsc 0.5
        line_05 = {
            "x": [0.0, max(fsc_data["x"])],
            "y": [0.5, 0.5],
        }
        list_data.append(line_05)
        series_args.append(
            {
                "name": "0.5",
                "mode": "lines",
                "line": {"color": "black", "dash": "dashdot"},
            }
        )  # plot trace name (legend)
        # fsc 0.143
        line_0143 = {
            "x": [0.0, max(fsc_data["x"])],
            "y": [0.143, 0.143],
        }
        list_data.append(line_0143)
        series_args.append(
            {
                "name": "0.143",
                "mode": "lines",
                "line": {"color": "black", "dash": "dash"},
            }
        )  # plot trace name (legend)
        if resolution != -1.0:
            resolution_line = {
                "x": [1.0 / resolution, 1.0 / resolution],
                "y": [
                    min(fsc_data["y"]),
                    max(fsc_data["y"]),
                ],
            }
            list_data.append(resolution_line)
            series_args.append(
                {
                    "name": "Resolution {}Å".format(round(resolution, 3)),
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        # half bit
        halfbit_data = {
            "x": dict_fsc_plot["level"],
            "y": dict_fsc_plot["halfbit"],
        }
        list_data.append(halfbit_data)
        series_args.append(
            {
                "name": "Half bit",
                "mode": "lines",
                "line": {"color": "pink", "dash": "dashdot"},
            }
        )  # plot trace name (legend)

        halfmap_fsc_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            series_args=series_args,
            title="FSC",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Spatial frequency (1/Å)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "FSC",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(halfmap_fsc_plot)

    def add_residue_inclusion_plot(self, input_map, contour, display_objects) -> None:
        # residue inclusion plot
        res_inc = input_map + "_residue_inclusion.json"
        with open(res_inc, "r") as m:
            dict_residue_inc_results = json.load(m)
        list_x: List[str] = []
        list_y: List[str] = []
        list_color: List[Union[str, float]] = []
        list_chains: List[str] = []
        list_chain_res = []
        list_chain_scores: List[Union[str, float]] = []
        dict_res_inc = dict_residue_inc_results["residue_inclusion"]["0"][str(contour)]
        for n in range(len(dict_res_inc["residue"])):
            residue = dict_res_inc["residue"][n]
            residue_split = residue.split(":")
            chain = residue_split[0]
            # append last residue and score before plot
            if n == len(dict_res_inc["residue"]) - 1:
                list_chain_res.append(residue_split[1][:-3])
                list_chain_scores.append(round(float(dict_res_inc["inclusion"][n]), 3))
                chain_last = chain
            if len(list_chains) == 0:
                chain_last = chain
                list_chains.append(chain)
            # end of each chain
            elif chain not in list_chains or n == len(dict_res_inc["residue"]) - 1:
                # TODO: chain splitting is currently disabled. This needs to reworked
                list_chain_color: List[Union[str, float]] = list_chain_scores
                list_chain_x = list_chain_res
                list_chain_y = [
                    "Chain {}".format(
                        chain_last,
                    )
                ] * len(list_chain_scores)
                list_x = list_chain_x + list_x
                list_y = list_chain_y + list_y
                list_color = list_chain_color + list_color
                if n == len(dict_res_inc["residue"]) - 1:
                    break  # break at the last residue
                list_chains.append(chain)
                list_chain_res = []
                list_chain_scores = []
            chain_last = chain
            list_chain_res.append(residue_split[1][:-3])
            list_chain_scores.append(round(float(dict_res_inc["inclusion"][n]), 3))
        plot_args = {
            "mode": "markers",
            "marker": {
                "symbol": "square",
                "size": 10,
                "color": list_color,
                "colorscale": [[0, "red"], [1, "cyan"]],  # "Viridis",
                "cmax": 1.0,
                "cmin": 0.0,
                "showscale": True,
            },
        }
        res_inc_plot = create_results_display_object(
            "plotlyobj",
            data=[{"x": list_x, "y": list_y}],
            plot_type="Scatter",
            multi_series=True,
            series_args=[plot_args],
            title="Atom inclusion by residue (contour: {})".format(contour),
            associated_data=[res_inc],
            xaxes_args={
                "title_text": "Residue",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={"gridcolor": "lightgrey", "linecolor": "black"},
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(res_inc_plot)

    def add_inclusion_plot(
        self,
        input_map,
        contour,
        display_objects,
    ) -> None:
        # Atom inclusion vs level plot
        map_data = input_map + "_atom_inclusion.json"
        with open(map_data, "r") as m:
            dict_map_data = json.load(m)
        list_data = []
        series_args = []
        data1 = {
            "x": dict_map_data["atom_inclusion_by_level"]["0"]["level"],
            "y": dict_map_data["atom_inclusion_by_level"]["0"]["all_atom"],
        }
        list_data.append(data1)
        series_args.append(
            {
                "name": "All atom inclusion",
                "mode": "lines",
                "line": {"color": "orange"},
            }
        )  # plot trace name (legend)
        data2 = {
            "x": dict_map_data["atom_inclusion_by_level"]["0"]["level"],
            "y": dict_map_data["atom_inclusion_by_level"]["0"]["backbone"],
        }
        list_data.append(data2)
        series_args.append(
            {
                "name": "Backbone atom inclusion",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_data]
        if contour != -1000:
            contour_line = {
                "x": [contour, contour],
                "y": [0.0, 1.0],
            }
            list_data.append(contour_line)
            series_args.append(
                {
                    "name": "Contour level",
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        map_plotobj = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            series_args=series_args,
            title="Atom inclusion versus contour level",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Contour level",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Fraction of model inside the map",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(map_plotobj)

    def add_raps_plot(
        self,
        input_map,
        input_hmap1_basename,
        input_hmap2_basename,
        display_objects,
        resolution,
    ) -> None:
        # RAPS plot
        map_raps = input_map + "_raps.json"
        with open(map_raps, "r") as m:
            dict_map_raps = json.load(m)
        list_data = []
        series_args = []
        ps_data = {
            "x": dict_map_raps["rotationally_averaged_power_spectrum"]["x"],
            "y": dict_map_raps["rotationally_averaged_power_spectrum"]["y"],
        }
        list_data.append(ps_data)
        series_args.append(
            {
                "name": "RAPS",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_raps]
        if resolution != -1:
            resolution_line = {
                "x": [1.0 / resolution, 1.0 / resolution],
                "y": [
                    min(dict_map_raps["rotationally_averaged_power_spectrum"]["y"]),
                    max(dict_map_raps["rotationally_averaged_power_spectrum"]["y"]),
                ],
            }
            list_data.append(resolution_line)
            series_args.append(
                {
                    "name": "Resolution {}Å".format(round(resolution, 3)),
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        if input_hmap1_basename and input_hmap2_basename:
            hmap_basename = "_".join(
                [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
            )
            hmap_path = os.path.join(self.output_dir, hmap_basename)
            rawmap_dist = hmap_path + "_raps.json"
            with open(rawmap_dist, "r") as m:
                dict_rawmap_dist = json.load(m)
            rawmap_raps_data = {
                "x": dict_rawmap_dist["rawmap_rotationally_averaged_power_spectrum"][
                    "x"
                ],
                "y": dict_rawmap_dist["rawmap_rotationally_averaged_power_spectrum"][
                    "y"
                ],
            }
            list_data.append(rawmap_raps_data)
            series_args.append(
                {
                    "name": "Raw map RAPS",
                    "mode": "lines",
                    "line": {"color": "orange"},
                }
            )  # plot trace name (legend)
            associated_data.append(rawmap_dist)
        raps_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            series_args=series_args,
            title="Rotationally averaged power spectrum",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Spatial frequency (1/Å)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Intensity (log10)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(raps_plot)

    def add_volume_distribution_plot(
        self,
        input_map,
        input_hmap1_basename,
        input_hmap2_basename,
        contour,
        display_objects,
    ) -> None:
        # voxel value distribution
        map_dist = input_map + "_density_distribution.json"
        with open(map_dist, "r") as m:
            dict_map_dist = json.load(m)
        associated_data = [map_dist]
        list_data = []
        series_args = []
        voxel_data = {
            "x": dict_map_dist["density_distribution"]["x"],
            "y": dict_map_dist["density_distribution"]["y"],
        }
        list_data.append(voxel_data)
        series_args.append(
            {
                "name": "Primary map value distribution",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_dist]
        if contour != -1000:
            contour_line = {
                "x": [contour, contour],
                "y": [0.0, max(dict_map_dist["density_distribution"]["y"])],
            }
            list_data.append(contour_line)
            series_args.append(
                {
                    "name": "Contour level",
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        if input_hmap1_basename and input_hmap2_basename:
            hmap_basename = "_".join(
                [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
            )
            hmap_path = os.path.join(self.output_dir, hmap_basename)
            rawmap_dist = hmap_path + "_density_distribution.json"
            with open(rawmap_dist, "r") as m:
                dict_rawmap_dist = json.load(m)
            rawmap_voxel_data = {
                "x": dict_rawmap_dist["rawmap_density_distribution"]["x"],
                "y": dict_rawmap_dist["rawmap_density_distribution"]["y"],
            }
            list_data.append(rawmap_voxel_data)
            series_args.append(
                {
                    "name": "Raw map value distribution",
                    "mode": "lines",
                    "line": {"color": "orange"},
                }
            )  # plot trace name (legend)
            associated_data.append(rawmap_dist)
        map_dist_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            series_args=series_args,
            title="Voxel-value distribution",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Voxel value",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Number of voxels (log10)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(map_dist_plot)

    def add_map_image_objects(self, input_map, contour, display_objects) -> None:
        # orthogonal projections
        x_proj = input_map + "_xprojection.tif"
        y_proj = input_map + "_yprojection.tif"
        z_proj = input_map + "_zprojection.tif"
        proj_display = mini_montage_from_many_files(
            [x_proj, y_proj, z_proj],
            self.output_dir,
            nimg=3,
            title="Orthogonal projections",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(proj_display)
        # orthogonal max value projections
        x_projmax = input_map + "_xmax.tif"
        y_projmax = input_map + "_ymax.tif"
        z_projmax = input_map + "_zmax.tif"
        projmax_display = mini_montage_from_many_files(
            [x_projmax, y_projmax, z_projmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projmax_display)
        # orthogonal false color max value projections
        x_projglowmax = input_map + "_glow_xmax.tif"
        y_projglowmax = input_map + "_glow_ymax.tif"
        z_projglowmax = input_map + "_glow_zmax.tif"
        projglowmax_display = mini_montage_from_many_files(
            [x_projglowmax, y_projglowmax, z_projglowmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections (False-colour)",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projglowmax_display)
        # orthogonal std dev projections
        x_projstd = input_map + "_xstd.tif"
        y_projstd = input_map + "_ystd.tif"
        z_projstd = input_map + "_zstd.tif"
        projstd_display = mini_montage_from_many_files(
            [x_projstd, y_projstd, z_projstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projstd_display)
        # orthogonal false color std dev projections
        x_projglowstd = input_map + "_glow_xstd.tif"
        y_projglowstd = input_map + "_glow_ystd.tif"
        z_projglowstd = input_map + "_glow_zstd.tif"
        projglowstd_display = mini_montage_from_many_files(
            [x_projglowstd, y_projglowstd, z_projglowstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections (False-colour)",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projglowstd_display)
        # central slices
        x_slicecentral = input_map + "_scaled_xcentral_slice.tif"
        y_slicecentral = input_map + "_scaled_ycentral_slice.tif"
        z_slicecentral = input_map + "_scaled_zcentral_slice.tif"
        slicecentral_display = mini_montage_from_many_files(
            [x_slicecentral, y_slicecentral, z_slicecentral],
            self.output_dir,
            nimg=3,
            title="Central slices",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(slicecentral_display)
        # largest variance slices
        x_slicelargevar = input_map + "_xlargestvariance_slice.tif"
        y_slicelargevar = input_map + "_ylargestvariance_slice.tif"
        z_slicelargevar = input_map + "_zlargestvariance_slice.tif"
        slicelargevar_display = mini_montage_from_many_files(
            [x_slicelargevar, y_slicelargevar, z_slicelargevar],
            self.output_dir,
            nimg=3,
            title="Largest variance slices",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(slicelargevar_display)
        # surface views
        x_surface = input_map + "_scaled_xsurface.tif"
        if os.path.isfile(x_surface) and contour != -1000:
            y_surface = input_map + "_scaled_ysurface.tif"
            z_surface = input_map + "_scaled_zsurface.tif"
            surface_display = mini_montage_from_many_files(
                [x_surface, y_surface, z_surface],
                self.output_dir,
                nimg=3,
                title="Orthogonal surface views of map (at contour {})".format(contour),
                ncols=3,
                labels=["X", "Y", "Z"],
            )
            display_objects.append(surface_display)

    def add_halfmap_result_objects(
        self,
        input_hmap1_basename,
        input_hmap2_basename,
        contour,
        display_objects,
    ) -> None:
        hmap_basename = "_".join(
            [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
        )
        hmap_path = os.path.join(self.output_dir, hmap_basename)
        x_proj = hmap_path + "_xprojection.tif"
        y_proj = hmap_path + "_yprojection.tif"
        z_proj = hmap_path + "_zprojection.tif"
        proj_display = mini_montage_from_many_files(
            [x_proj, y_proj, z_proj],
            self.output_dir,
            nimg=3,
            title="Orthogonal projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(proj_display)
        # orthogonal max-value projections
        x_projmax = hmap_path + "_xmax.tif"
        y_projmax = hmap_path + "_ymax.tif"
        z_projmax = hmap_path + "_zmax.tif"
        proj_display = mini_montage_from_many_files(
            [x_projmax, y_projmax, z_projmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(proj_display)
        # orthogonal false color max value projections
        x_projglowmax = hmap_path + "_glow_xmax.tif"
        y_projglowmax = hmap_path + "_glow_ymax.tif"
        z_projglowmax = hmap_path + "_glow_zmax.tif"
        projglowmax_display = mini_montage_from_many_files(
            [x_projglowmax, y_projglowmax, z_projglowmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections (False-colour) of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projglowmax_display)
        # orthogonal false color max value projections
        x_projstd = hmap_path + "_xstd.tif"
        y_projstd = hmap_path + "_ystd.tif"
        z_projstd = hmap_path + "_zstd.tif"
        projstd_display = mini_montage_from_many_files(
            [x_projstd, y_projstd, z_projstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projstd_display)
        # orthogonal false color std dev projections
        x_projglowstd = hmap_path + "_glow_xstd.tif"
        y_projglowstd = hmap_path + "_glow_ystd.tif"
        z_projglowstd = hmap_path + "_glow_zstd.tif"
        projglowstd_display = mini_montage_from_many_files(
            [x_projglowstd, y_projglowstd, z_projglowstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections (False-colour) \
                of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(projglowstd_display)
        # central slices
        x_slicecentral = hmap_path + "_scaled_xcentral_slice.tif"
        y_slicecentral = hmap_path + "_scaled_ycentral_slice.tif"
        z_slicecentral = hmap_path + "_scaled_zcentral_slice.tif"
        slicecentral_display = mini_montage_from_many_files(
            [x_slicecentral, y_slicecentral, z_slicecentral],
            self.output_dir,
            nimg=3,
            title="Central slices of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(slicecentral_display)
        # largest variance slices
        x_slicelargevar = hmap_path + "_xlargestvariance_slice.tif"
        y_slicelargevar = hmap_path + "_ylargestvariance_slice.tif"
        z_slicelargevar = hmap_path + "_zlargestvariance_slice.tif"
        slicelargevar_display = mini_montage_from_many_files(
            [x_slicelargevar, y_slicelargevar, z_slicelargevar],
            self.output_dir,
            nimg=3,
            title="Largest variance slices of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
        )
        display_objects.append(slicelargevar_display)
        # surface views
        x_surface = hmap_path + "_scaled_xsurface.tif"
        rawmap_contour_json = hmap_path + "_rawmapcl.json"
        rawmap_contour = contour
        if os.path.exists(rawmap_contour_json):
            with open(rawmap_contour_json, "r") as j:
                dict_contour = json.load(j)
                try:
                    rawmap_contour = dict_contour["rawmap_contour_level"]["cl"]
                except KeyError:
                    pass
        if os.path.isfile(x_surface) and contour != -1000:
            y_surface = hmap_path + "_scaled_ysurface.tif"
            z_surface = hmap_path + "_scaled_zsurface.tif"
            surface_display = mini_montage_from_many_files(
                [x_surface, y_surface, z_surface],
                self.output_dir,
                nimg=3,
                title="Orthogonal surface views of raw map (at contour {})".format(
                    rawmap_contour
                ),
                ncols=3,
                labels=["X", "Y", "Z"],
            )
            display_objects.append(surface_display)

    def get_map_parameters_tableobj(
        self, map_basename, contour, display_objects
    ) -> None:
        # map parameters
        map_parameters_json = os.path.join(
            self.output_dir, map_basename + "_map_parameters.json"
        )
        map_parameters_data = []
        if contour != -1000:
            map_parameters_data.append(["Selected contour level", contour])
        with open(map_parameters_json, "r") as j:
            dict_map_parameters = json.load(j)
            map_parameters_data.append(
                [
                    "Number of grid points",
                    "x".join(
                        [
                            str(dict_map_parameters["Box size (pixels)"][0]),
                            str(dict_map_parameters["Box size (pixels)"][1]),
                            str(dict_map_parameters["Box size (pixels)"][2]),
                        ]
                    ),
                ]
            )
            map_parameters_data.append(
                [
                    "Voxel size",
                    "x".join(
                        [
                            str(dict_map_parameters["Voxel size (angstroms)"][0]),
                            str(dict_map_parameters["Voxel size (angstroms)"][1]),
                            str(dict_map_parameters["Voxel size (angstroms)"][2]),
                        ]
                    )
                    + "Å",
                ]
            )
            map_parameters_data.append(["Minimum value", dict_map_parameters["Min"]])
            map_parameters_data.append(["Maximum value", dict_map_parameters["Max"]])
            map_parameters_data.append(["Average value", dict_map_parameters["Mean"]])
            map_parameters_data.append(
                ["Standard deviation", dict_map_parameters["Std"]]
            )
        map_parameters = create_results_display_object(
            "table",
            title="Map parameters",
            headers=["Parameters", "values"],
            table_data=map_parameters_data,
            associated_data=[map_parameters_json],
        )
        display_objects.append(map_parameters)
