#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import requests
import os
import traceback
from glob import glob
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import List, Tuple, Sequence, Union

from gemmi import cif

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, Ref, PipelinerCommand
from pipeliner.job_options import (
    StringJobOption,
    MultipleChoiceJobOption,
    BooleanJobOption,
    JobOptionCondition,
)
from pipeliner.job_options import JobOptionValidationResult
from pipeliner.display_tools import (
    make_maps_slice_montage_and_3d_display,
    make_map_model_thumb_and_display,
    create_results_display_object,
    make_mrcs_central_slices_montage,
)
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
    ResultsDisplayPending,
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)
from pipeliner.node_factory import NODE_ATOMCOORDS, NODE_DENSITYMAP, NODE_SEQUENCE
from pipeliner.utils import get_job_script

tools_script = get_job_script("download_map_model.py")
prep_command = ["python3", tools_script]


def url_has_error(address) -> str:
    """Check if a web address is live

    Args:
        address (str): The url to check

    Returns:
        str: Any errors reported
    """
    err = ""
    try:
        response = requests.head(address, timeout=3.1)
        res = response.status_code
        if res >= 400:
            err = response.reason if response.reason else "unavailable"
    except requests.Timeout:
        err = "could not check status, request timed out"
    except Exception as ex:
        err = f"error: {ex}"
    return err


class FetchPdb(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.pdb"
    OUT_DIR = "Fetch"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Fetch from PDB"
        self.jobinfo.short_desc = "Download atomic models from PDB"
        self.jobinfo.long_desc = "Download atomic models from PDB"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("gzip")]

        self.joboptions["db_id"] = StringJobOption(
            label="PDB ID number",
            help_text="Enter the model's 4-character PDB ID",
            is_required=True,
        )
        self.joboptions["format"] = MultipleChoiceJobOption(
            label="Download format:",
            choices=["PDB", "mmCIF"],
            default_value_index=1,
            help_text=(
                "Format to download model files in. mmCIF is the current standard but"
                " some programs will only use the older PDB format"
            ),
        )
        self.joboptions["get_fasta"] = BooleanJobOption(
            label="Also get FASTA sequence file?",
            default_value=False,
            help_text="Also download the sequence for this entry in FASTA format",
        )

    def pdb_id(self) -> str:
        return self.joboptions["db_id"].get_string().lower()

    def get_address(self) -> str:
        dbid = self.pdb_id()
        fmat = self.joboptions["format"].get_string()
        ext = "cif" if fmat == "mmCIF" else "pdb"
        addy = f"https://files.rcsb.org/download/{dbid.upper()}.{ext}.gz"
        return addy

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        results = []

        addy = self.get_address()
        dbid = self.pdb_id()
        err = url_has_error(addy)
        if err:
            results.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"PDB entry {dbid}: {err}",
                )
            )
        else:
            results.append(
                JobOptionValidationResult(
                    result_type="info",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"PDB entry {dbid} is valid",
                )
            )

        return results

    def create_output_nodes(self) -> None:
        address = self.get_address()
        unzipped_path = Path(address).with_suffix("")
        self.add_output_node(unzipped_path.name, NODE_ATOMCOORDS, ["from_pdb"])
        if self.joboptions["get_fasta"].get_boolean():
            fastafile = self.joboptions["db_id"].get_string().lower() + ".fasta"
            self.add_output_node(fastafile, NODE_SEQUENCE)

    def get_commands(self) -> List[PipelinerCommand]:
        address = self.get_address()
        coms = [PipelinerCommand(prep_command + [address, "-P", self.output_dir])]

        gz_path = Path(self.output_dir) / Path(address).name
        coms.append(PipelinerCommand(["gzip", "-d", str(gz_path)]))

        if self.joboptions["get_fasta"].get_boolean():
            dbid = self.pdb_id()
            seq_addy = f"https://www.ebi.ac.uk/pdbe/entry/pdb/{dbid}/fasta"
            coms.append(
                PipelinerCommand(prep_command + [seq_addy, "-P", self.output_dir])
            )
            fasta_name = self.joboptions["db_id"].get_string() + ".fasta"
            coms.append(
                PipelinerCommand(
                    [
                        "mv",
                        str(Path(self.output_dir) / "fasta"),
                        str(Path(self.output_dir) / fasta_name),
                    ]
                )
            )
        return coms

    def get_cif_file_citations(self) -> Tuple[List[Ref], str]:
        citations = []
        md = cif.read(self.output_nodes[0].name).sole_block()
        # authors
        aut_table = md.find("_citation_author.", ["citation_id", "name"])
        authors = []
        if aut_table:
            for i in aut_table:
                if i[0] == "primary":
                    authors.append(i[1].strip("'"))
            if len(authors) > 5:
                authors = authors[:5] + ["et al."]

        # single reference is a name_val pair
        cif_info = [
            {
                "title": md.find_pair("_citation.title"),
                "journal": md.find_pair("_citation.journal_abbrev"),
                "vol": md.find_pair("_citation.journal_volume"),
                "fp": md.find_pair("_citation.page_first"),
                "year": md.find_pair("_citation.year"),
                "pubmed": md.find_pair("_citation.pdbx_database_id_PubMed"),
                "doi": md.find_pair("_citation.pdbx_database_id_DOI"),
            }
        ]

        # multiple refs are a loop
        if cif_info[0]["title"] is None:
            cif_info = []
            multirefs = md.find(
                "_citation.",
                [
                    "title",
                    "journal_abbrev",
                    "journal_volume",
                    "page_first",
                    "year",
                    "pdbx_database_id_PubMed",
                    "pdbx_database_id_DOI",
                ],
            )
            for ref_row in multirefs:
                cif_info.append(
                    {
                        "title": ref_row[0],
                        "journal": ref_row[1],
                        "vol": ref_row[2],
                        "fp": ref_row[3],
                        "year": ref_row[4],
                        "pubmed": ref_row[5],
                        "doi": ref_row[6],
                    }
                )

        for ref in cif_info:
            ref_vals = {}
            # in case any None values have snuck in
            for ref_key in ref:
                if ref[ref_key] is None:
                    ref_vals[ref_key] = "?"
                elif isinstance(ref[ref_key], str):
                    ref_vals[ref_key] = ref[ref_key].strip("'")
                else:  # tuple or list, depending on the version of Gemmi
                    ref_vals[ref_key] = ref[ref_key][1].strip("'")

            citations.append(
                Ref(
                    authors=authors,
                    title=ref_vals["title"],
                    year=ref_vals["year"],
                    volume=ref_vals["vol"],
                    pages=ref_vals["fp"],
                    doi=ref_vals["doi"],
                    pubmed=ref_vals["pubmed"],
                    journal=ref_vals["journal"],
                )
            )
        res_xtal = md.find_pair("_reflns.d_resolution_high")
        res_em = md.find_pair("_em_3d_reconstruction.resolution")
        if res_xtal:
            reso = res_xtal[1].strip("'")
        elif res_em:
            reso = res_em[1].strip("'")
        else:
            reso = "Resolution not available"
        return citations, reso

    def get_pdb_file_citations(self) -> Tuple[List[Ref], str]:
        subs = {"AUT": "", "TIT": "", "REF": "", "PMI": "", "DOI": ""}
        with open(self.output_nodes[0].name, "r") as pdb:
            lines = pdb.readlines()
        reso = "not available"
        for ln in lines:
            if ln[:6] == "REMARK" and ln[11:22] == "RESOLUTION.":
                reso = ln[26:30]
            if ln[:4] == "JRNL":
                sub = ln[12:15]
                subs[sub] += ln[19:].strip() + " "
        authors = subs["AUT"].split(",")
        authors = [
            ".".join(map(lambda s: s.strip().capitalize(), x.split(".")))
            for x in authors
        ]
        if len(authors) > 5:
            authors = authors[:5] + ["et al."]
        ref_vals = {
            "journal": subs["REF"][0:27].capitalize().strip(),
            "vol": subs["REF"][33:36].strip(),
            "fp": subs["REF"][38:42].strip(),
            "year": subs["REF"][43:47],
            "title": subs["TIT"].capitalize().replace("\n", ""),
            "pubmed": subs["PMI"].strip(),
            "doi": subs["DOI"].strip(),
        }

        citations = [
            Ref(
                authors=authors,
                title=ref_vals["title"],
                year=ref_vals["year"],
                volume=ref_vals["vol"],
                pages=ref_vals["fp"],
                doi=ref_vals["doi"],
                pubmed=ref_vals["pubmed"],
                journal=ref_vals["journal"],
            )
        ]

        return citations, reso

    def get_pdb_cite(self) -> Tuple[List[Ref], str]:
        # find the primary citation
        on = self.output_nodes[0].name
        try:
            if on.endswith(".cif"):
                citations, reso = self.get_cif_file_citations()
            elif on.endswith(".pdb"):
                citations, reso = self.get_pdb_file_citations()
            else:
                citations, reso = (
                    [Ref(title=f"Error parsing file: {on}", journal="", year="")],
                    "Unavailable",
                )
        except Exception as e:
            reso = "not available"
            tb = str(traceback.format_exc())
            citations = [
                Ref(title="Error getting citation info:", journal=str(e), year=tb)
            ]

        return citations, reso

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        citations, reso = self.get_pdb_cite()

        dispobjs: List[ResultsDisplayObject] = [
            make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=[],
                models=[self.output_nodes[0].name],
                title=f"Model preview: pdb-{self.pdb_id()}; " f"{reso} Å",
            )
        ]
        for cite in citations:
            dispobjs.append(
                create_results_display_object(
                    "text",
                    title="Literature reference",
                    display_data=str(cite),
                    associated_data=[self.output_nodes[0].name],
                )
            )
        if self.joboptions["get_fasta"].get_boolean():
            fastafile = str(Path(self.output_dir) / f"{self.pdb_id()}.fasta")
            dispobjs.append(
                create_results_display_object(
                    "textfile",
                    file_path=fastafile,
                    title=f"Sequence: {os.path.basename(fastafile)}",
                    start_collapsed=True,
                )
            )

        return dispobjs

    def get_additional_reference_info(self) -> List[Ref]:
        return self.get_pdb_cite()[0]


class FetchEmdb(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.emdb"
    OUT_DIR = "Fetch"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Fetch from EMDB"
        self.jobinfo.short_desc = "Download maps from EMDB"
        self.jobinfo.long_desc = "Download maps from EMDB"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("gzip")]

        self.joboptions["db_id"] = StringJobOption(
            label="EMDB ID number",
            help_text="Enter just the number portion of the model's EMDB ID",
            is_required=True,
        )

        self.joboptions["get_associated_models"] = BooleanJobOption(
            label="Also download associated model files?",
            help_text=(
                "If selected, the pipeliner will also attempt to get any associated"
                " model files from the PDB"
            ),
            default_value=False,
        )

        self.joboptions["asoc_mod_format"] = MultipleChoiceJobOption(
            label="Format for associated models:",
            help_text="Choose what format to download the associated models in",
            choices=["mmCIF", "PDB"],
            default_value_index=0,
            deactivate_if=JobOptionCondition([("get_associated_models", "=", False)]),
        )

        self.joboptions["get_fasta"] = BooleanJobOption(
            label="Get FASTA sequence files for associated models",
            default_value=False,
            help_text=(
                "Also download the sequences for the associated models in FASTA format"
            ),
            deactivate_if=JobOptionCondition([("get_associated_models", "=", False)]),
        )

        self.joboptions["get_halfmaps"] = BooleanJobOption(
            label="Download associated halfmaps if available?",
            help_text="Get any halfmaps that were deposited with the map",
            default_value=False,
        )

    def get_addresses(self) -> Tuple[str, str]:
        dbid = self.joboptions["db_id"].get_string().lower()
        addy_map = (
            f"https://files.wwpdb.org/pub/emdb/structures/EMD-{dbid}/map/emd_{dbid}"
            ".map.gz"
        )
        addy_metadata = (
            f"https://files.wwpdb.org/pub/emdb/structures/EMD-{dbid}/header/emd-{dbid}"
            ".xml"
        )
        return addy_map, addy_metadata

    def halfmap_addresses(self) -> Tuple[str, str]:
        db_id = self.joboptions["db_id"].get_string().lower()
        hm1 = (
            f"https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-{db_id}/other/"
            f"emd_{db_id}_half_map_1.map.gz"
        )
        hm2 = (
            f"https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-{db_id}/other/"
            f"emd_{db_id}_half_map_2.map.gz"
        )
        return hm1, hm2

    def extend_com_for_halfmaps(self) -> List[PipelinerCommand]:
        hm_coms = []
        # check for the availability for halfmaps
        hm1_addy, hm2_addy = self.halfmap_addresses()
        hmerr = ""
        for hm in [hm1_addy, hm2_addy]:
            hmerr += url_has_error(hm)
        if not hmerr:
            for halfmap in [hm1_addy, hm2_addy]:
                zipped = os.path.join(self.output_dir, halfmap.split("/")[-1])
                hm_coms.extend(
                    [
                        PipelinerCommand(
                            prep_command + [halfmap, "-P", self.output_dir]
                        ),
                        PipelinerCommand(["gzip", "-d", zipped]),
                        PipelinerCommand(
                            [
                                "mv",
                                zipped.strip(".gz"),
                                zipped.replace(".map.gz", ".mrc"),
                            ]
                        ),
                    ]
                )
        return hm_coms

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        results = []

        addy = self.get_addresses()[0]
        dbid = self.joboptions["db_id"].get_string().lower()
        err = url_has_error(addy)
        if err:
            results.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"EMDB entry {dbid}: {err}",
                )
            )
        else:
            results.append(
                JobOptionValidationResult(
                    result_type="info",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"EMDB entry {dbid} is valid",
                )
            )

        if not err and self.joboptions["get_halfmaps"].get_boolean():
            if not self.extend_com_for_halfmaps():
                results.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["get_halfmaps"]],
                        message=(f"Halfmaps are not available for EMDB entry {dbid}"),
                    )
                )
            else:
                results.append(
                    JobOptionValidationResult(
                        result_type="info",
                        raised_by=[self.joboptions["get_halfmaps"]],
                        message=f"Halfmaps are available for EMDB entry {dbid}",
                    )
                )

        return results

    def create_output_nodes(self) -> None:
        map_address, _ = self.get_addresses()
        map_path = Path(map_address).with_suffix("").with_suffix(".mrc")
        self.add_output_node(map_path.name, NODE_DENSITYMAP, ["from_emdb"])

    def get_commands(self) -> List[PipelinerCommand]:
        map_address, md_address = self.get_addresses()
        coms = [
            PipelinerCommand(prep_command + [md_address, "-P", self.output_dir]),
            PipelinerCommand(prep_command + [map_address, "-P", self.output_dir]),
        ]

        map_gz_path = Path(self.output_dir) / Path(map_address).name
        coms.append(PipelinerCommand(["gzip", "-d", str(map_gz_path)]))

        map_path = map_gz_path.with_suffix("")
        coms.append(
            PipelinerCommand(["mv", str(map_path), str(map_path.with_suffix(".mrc"))])
        )

        # do the halfmaps
        if self.joboptions["get_halfmaps"].get_boolean():
            hm_coms = self.extend_com_for_halfmaps()
            if hm_coms:
                coms.extend(hm_coms)

        if self.joboptions["get_associated_models"].get_boolean():
            asoc_com = [
                "python3",
                get_job_script("fetch/emdb_get_associated_models.py"),
                "--id",
                self.joboptions["db_id"].get_string(),
                "--outdir",
                self.output_dir,
            ]
            if self.joboptions["asoc_mod_format"].get_string() == "mmCIF":
                asoc_com.append("--do_cif")

            if self.joboptions["get_fasta"].get_boolean():
                asoc_com.append("--get_seqs")
            coms.append(PipelinerCommand(asoc_com))

        return coms

    def get_xml_root(self) -> ET.Element:
        dbid = self.joboptions["db_id"].get_string()
        xml = os.path.join(self.output_dir, f"emd-{dbid}.xml")
        return ET.parse(xml).getroot()

    def get_additional_reference_info(self) -> List[Ref]:
        # citation info
        root = self.get_xml_root()
        jp = "./crossreferences/citation_list/primary_citation/journal_citation"
        jcite = root.find(jp)
        if jcite is not None:
            pub = jcite.attrib["published"]
            is_published = True if pub == "true" else False

            if is_published:
                doi, pubmed = "", ""
                authors = jcite.findall("./author")
                auth_list = [str(x.text) for x in authors]
                if len(auth_list) > 5:
                    auth_list = auth_list[:5] + ["et al."]
                title = jcite.findtext("./title", default="Title not available")
                journal = jcite.findtext("./journal")
                if not journal:
                    journal = jcite.findtext(
                        "./journal_abbreviation", default="Journal not available"
                    )
                year = jcite.findtext("./year", default="Year not available")
                ers = jcite.findall("./external_references")
                for er in ers:
                    if er.attrib["type"] == "DOI":
                        doi = f" DOI: {er.text}"
                    if er.attrib["type"] == "PUBMED":
                        pubmed = f" PubMed: {er.text}"

                return [
                    Ref(
                        authors=auth_list,
                        title=title,
                        journal=journal,
                        year=year,
                        doi=doi,
                        pubmed=pubmed,
                    )
                ]
        return []

    def parse_emdb_header(
        self,
    ) -> Tuple[str, str, str, str, str, List[List[str]], List[List[str]]]:
        asoc_mods: List[List[str]] = []
        root = self.get_xml_root()
        entryname = root.attrib["emdb_id"]

        refs = self.get_additional_reference_info()
        citation = ""
        if refs:
            citation = str(refs[0])

        # map info
        col = root.findtext("./map/dimensions/col", default="unknown")
        row = root.findtext("./map/dimensions/row", default="unknown")
        sec = root.findtext("./map/dimensions/sec", default="unknown")
        dims = f"{col} x {row} x {sec}"

        px = root.findtext("./map/pixel_spacing/x", default="unknown")

        reso = root.findtext(".//resolution", default="unknown")

        # associated maps
        allmaps = root.findall("./crossreferences/emdb_list/emdb_reference")
        map_details = {}
        for i in allmaps:
            mapid = i.findtext("emdb_id", default="unknown")
            the_map_deets = i.findtext("details", default="")
            map_details[mapid] = the_map_deets
        deets = map_details.get(entryname, "")
        title = entryname
        if deets:
            title += f": {deets}"
        asoc_maps = []
        for m in map_details:
            if m != entryname and m is not None:
                asoc_maps.append([m, map_details[m]])

        # associated models
        ap = "./crossreferences/pdb_list/pdb_reference"
        asocmod = root.findall(ap)
        for i in asocmod:
            asoc_mods.append([str(i.findtext("pdb_id"))])

        return title, citation, dims, px, reso, asoc_maps, asoc_mods

    def create_post_run_output_nodes(self):
        if self.joboptions["get_associated_models"].get_boolean():
            if self.joboptions["asoc_mod_format"].get_string() == "mmCIF":
                ext = "cif"
            else:
                ext = "pdb"
            asoc_mods = glob(self.output_dir + f"*.{ext}")
            for amod in asoc_mods:
                self.add_output_node(Path(amod).name, NODE_ATOMCOORDS, ["from_pdb"])
            seqs = glob(self.output_dir + "*.fasta")
            for seq in seqs:
                self.add_output_node(Path(seq).name, NODE_SEQUENCE)

        if self.joboptions["get_halfmaps"].get_boolean():
            dbid = self.joboptions["db_id"].get_string()
            hm_names = [f"emd_{dbid}_half_map_1.mrc", f"emd_{dbid}_half_map_2.mrc"]
            for hm in hm_names:
                if (Path(self.output_dir) / hm).is_file():
                    self.add_output_node(hm, NODE_DENSITYMAP, ["halfmap"])

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        title, citation, dims, px, reso, asoc_maps, asoc_mods = self.parse_emdb_header()
        params = create_results_display_object(
            "table",
            title=title,
            headers=[
                "Resolution (Å)",
                "Map dimensions (px)",
                "pixel size (Å)",
            ],
            table_data=[[reso, dims, px]],
            associated_data=[self.output_nodes[0].name],
        )
        mods = []
        for on in self.output_nodes:
            if on.format in ["pdb", "cif", "mmcif"]:
                mods.append(on.name)
        the_slices = make_mrcs_central_slices_montage(
            in_files={self.output_nodes[0].name: ""},
            output_dir=self.output_dir,
        )
        the_map = make_map_model_thumb_and_display(
            outputdir=self.output_dir,
            maps=[self.output_nodes[0].name],
            models=mods,
            maps_opacity=[0.5] if mods else [1.0],
            title="Map and associated models" if mods else "Downloaded map",
        )
        dispobjs: List[
            Union[
                ResultsDisplayMapModel,
                ResultsDisplayMontage,
                ResultsDisplayPending,
                ResultsDisplayObject,
            ]
        ] = [params, the_slices, the_map]

        if citation:
            litref = create_results_display_object(
                "text",
                title="Literature reference",
                display_data=citation,
                associated_data=[self.output_nodes[0].name],
            )
        else:
            litref = create_results_display_object(
                "text",
                title="No reference available",
                display_data="No reference available for this entry",
                associated_data=[self.output_nodes[0].name],
            )
        dispobjs.append(litref)

        if asoc_maps:
            a_maps = create_results_display_object(
                "table",
                title="Associated EMDB entries",
                headers=["EMDB ID", "Details"],
                table_data=asoc_maps,
                associated_data=[self.output_nodes[0].name],
            )
            dispobjs.append(a_maps)
        if asoc_mods:
            a_mods = create_results_display_object(
                "table",
                title="Associated PDB entries",
                headers=["PDB ID"],
                table_data=asoc_mods,
                associated_data=[self.output_nodes[0].name],
            )
            dispobjs.append(a_mods)

        pid = self.joboptions["db_id"].get_string()
        hm1 = os.path.join(self.output_dir, f"emd_{pid}_half_map_1.mrc")
        hm2 = os.path.join(self.output_dir, f"emd_{pid}_half_map_2.mrc")
        if self.joboptions["get_halfmaps"].get_boolean():
            dispobjs.extend(
                make_maps_slice_montage_and_3d_display(
                    in_maps={hm1: "halfmap1", hm2: "halfmap2"},
                    output_dir=self.output_dir,
                )
            )

        fastas = glob(str(Path(self.output_dir) / "*.fasta"))
        for fastafile in fastas:
            dispobjs.append(
                create_results_display_object(
                    "textfile",
                    file_path=fastafile,
                    title=f"Sequence: {os.path.basename(fastafile)}",
                    start_collapsed=True,
                )
            )

        return dispobjs


class FetchAlphaFold(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.alphafold"
    OUT_DIR = "Fetch"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Fetch from AlphaFold DB"
        self.jobinfo.short_desc = (
            "Download predicted structures from the AlphaFold Protein Structure "
            "Database"
        )
        self.jobinfo.long_desc = (
            "Download predicted structures from the AlphaFold Protein Structure "
            "Database"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("gzip")]

        self.jobinfo.references = [
            Ref(
                authors=[
                    "Varadi M",
                    "Anyango S",
                    "Deshpande M",
                    "Nair S",
                    "Natassia C",
                    "Yordanova G",
                    "Yuan D",
                    "Stroe O",
                    "Wood G",
                    "Laydon A",
                    "Žídek A",
                    "Green T",
                    "Tunyasuvunakool K",
                    "Petersen S",
                    "Jumper J",
                    "Clancy E",
                    "Green R",
                    "Vora A",
                    "Lutfi M",
                    "Figurnov M",
                    "Cowie A",
                    "Hobbs N",
                    "Kohli P",
                    "Kleywegt G",
                    "Birney E",
                    "Hassabis D",
                    "Velankar S",
                ],
                title="AlphaFold Protein Structure Database: massively expanding the "
                "structural coverage of protein-sequence space with high-accuracy"
                " models",
                journal="Nucleic Acids Research",
                year="2021",
                volume="50",
                issue="D1",
                pages="D439–D444",
                doi="10.1093/nar/gkab1061",
            ),
            Ref(
                authors=[
                    "Jumper J",
                    "Evans R",
                    "Pritzel A",
                    "Green T",
                    "Figurnov M",
                    "Ronneberger O",
                    "Tunyasuvunakool K",
                    "Bates R",
                    "Žídek A",
                    "Potapenko A",
                    "Bridgland A",
                    "Meyer C",
                    "Kohl SAA",
                    "Ballard AJ",
                    "Cowie A",
                    "Romera-Paredes B",
                    "Nikolov S",
                    "Jain R",
                    "Adler J",
                    "Back T",
                    "Petersen S",
                    "Reiman D",
                    "Clancy E",
                    "Zielinski M",
                    "Steinegger M",
                    "Pacholska M",
                    "Berghammer T",
                    "Bodenstein A",
                    "Silver D",
                    "Vinyals O",
                    "Senior AW",
                    "Kavukcuoglu K",
                    "Kohli P",
                    "Hassabis D",
                ],
                title="Highly accurate protein structure prediction with AlphaFold",
                journal="Nature",
                year="2021",
                volume="596",
                pages="583-589",
                doi="10.1038/s41586-021-03819-2",
            ),
        ]

        self.joboptions["uniprot_id"] = StringJobOption(
            label="UNIPROT accession number",
            help_text="Enter the protein's UNIPROT accession number",
            is_required=True,
        )
        self.joboptions["format"] = MultipleChoiceJobOption(
            label="Download format:",
            choices=["PDB", "mmCIF"],
            default_value_index=1,
            help_text=(
                "Format to download model files in. mmCIF is the current standard but"
                " some programs will only use the older PDB format"
            ),
        )

        self.joboptions["get_fasta"] = BooleanJobOption(
            label="Also get FASTA sequence file?",
            default_value=False,
            help_text="Also download the sequence for this entry in FASTA format",
        )

    def uniprot_id(self) -> str:
        return self.joboptions["uniprot_id"].get_string().upper()

    def get_addresses(self) -> Tuple[str, str]:
        dbid = self.uniprot_id()
        fmat = self.joboptions["format"].get_string()
        ext = "cif" if fmat == "mmCIF" else "pdb"
        model = f"https://alphafold.ebi.ac.uk/files/AF-{dbid}-F1-model_v4.{ext}"
        plot = (
            f"https://alphafold.ebi.ac.uk/files/AF-{dbid}-F1-predicted_aligned_"
            f"error_v4.json"
        )
        return model, plot

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        results = []

        addy = self.get_addresses()[0]
        dbid = self.uniprot_id()
        err = url_has_error(addy)
        if err:
            results.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["uniprot_id"]],
                    message=f"AlphaFold DB entry {dbid}: {err}",
                )
            )
        else:
            results.append(
                JobOptionValidationResult(
                    result_type="info",
                    raised_by=[self.joboptions["uniprot_id"]],
                    message=f"AlphaFold DB entry {dbid} is valid",
                )
            )

        return results

    def create_output_nodes(self) -> None:
        model, _ = self.get_addresses()
        self.add_output_node(
            os.path.basename(model), NODE_ATOMCOORDS, ["simulated", "alphafold"]
        )
        if self.joboptions["get_fasta"].get_boolean():
            fastafile = self.uniprot_id() + ".fasta"
            self.add_output_node(fastafile, NODE_SEQUENCE)

    def get_commands(self) -> List[PipelinerCommand]:
        model, plot = self.get_addresses()
        coms = [
            PipelinerCommand(prep_command + [model, "-P", self.output_dir]),
            PipelinerCommand(prep_command + [plot, "-P", self.output_dir]),
        ]
        if self.joboptions["get_fasta"].get_boolean():
            seq_addy = f"https://rest.uniprot.org/uniprotkb/{self.uniprot_id()}.fasta"
            coms.append(
                PipelinerCommand(prep_command + [seq_addy, "-P", self.output_dir])
            )
        return coms

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        dispobjs: List[ResultsDisplayObject] = [
            make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=[],
                models=[self.output_nodes[0].name],
                title=(
                    "atomic model: AlphaFold DB prediction for UNIPROT-"
                    f"{self.uniprot_id()}"
                ),
            ),
        ]
        if self.joboptions["get_fasta"].get_boolean():
            fastafile = str(Path(self.output_dir) / self.uniprot_id()) + ".fasta"
            dispobjs.append(
                create_results_display_object(
                    "textfile",
                    file_path=fastafile,
                    title=f"Sequence: {os.path.basename(fastafile)}",
                    start_collapsed=True,
                )
            )

        return dispobjs


class FetchFasta(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.sequence"
    OUT_DIR = "Fetch"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Fetch sequence"
        self.jobinfo.short_desc = "Get fasta sequence file for PDB or UNIPROT entry"
        self.jobinfo.long_desc = "Get fasta sequence file for PDB or UNIPROT entry"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"

        self.joboptions["pdb_id"] = StringJobOption(
            label="PDB ID number",
            help_text="Enter the model's 4-character PDB ID",
            deactivate_if=JobOptionCondition([("uniprot_id", "!=", "")]),
            required_if=JobOptionCondition([("uniprot_id", "=", "")]),
        )
        self.joboptions["uniprot_id"] = StringJobOption(
            label="UNIPROT accession number",
            help_text="Enter the protein's UNIPROT accession number",
            deactivate_if=JobOptionCondition([("pdb_id", "!=", "")]),
            required_if=JobOptionCondition([("pdb_id", "=", "")]),
        )

    def get_ids(self) -> Tuple[str, str]:
        uniprot_id = self.joboptions["uniprot_id"].get_string().upper()
        pdbid = self.joboptions["pdb_id"].get_string().lower()
        return uniprot_id, pdbid

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        results = []
        uniprot_id, dbid = self.get_ids()
        if uniprot_id:
            uni_addy = f"https://rest.uniprot.org/uniprotkb/{uniprot_id}.fasta"
            err = url_has_error(uni_addy)
            if err:
                # Provide a more user friendly message for invalid IDs
                if err == "Bad Request":
                    err = "not found"
                results.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["uniprot_id"]],
                        message=f"UNIPROT entry {uniprot_id}: {err}",
                    )
                )
            else:
                results.append(
                    JobOptionValidationResult(
                        result_type="info",
                        raised_by=[self.joboptions["uniprot_id"]],
                        message=f"UNIPROT entry {dbid} is valid",
                    )
                )
        # TODO: validate FASTA requests from RCSB or PDBe servers
        #  Validation has been removed for now because it didn't work properly, and it's
        #  more confusing to show incorrect validation results than to show no
        #  validation at all.
        #  As of 18/4/24, the problem is that the FASTA endpoints on both servers return
        #  a 200 Success code for GET even with an incorrect ID, and the PDBe server
        #  returns 404 Not Found for HEAD requests even though the equivalent GET
        #  request would get a 200 response.

        return results

    def get_commands(self) -> List[PipelinerCommand]:
        uniprot_id, pdbid = self.get_ids()
        if uniprot_id:
            uni_addy = f"https://rest.uniprot.org/uniprotkb/{uniprot_id}.fasta"
            return [PipelinerCommand(prep_command + [uni_addy, "-P", self.output_dir])]
        else:
            seq_addy = f"https://www.ebi.ac.uk/pdbe/entry/pdb/{pdbid}/fasta"
            coms = [PipelinerCommand(prep_command + [seq_addy, "-P", self.output_dir])]
            fasta_name = pdbid + ".fasta"
            coms.append(
                PipelinerCommand(
                    [
                        "mv",
                        str(Path(self.output_dir) / "fasta"),
                        str(Path(self.output_dir) / fasta_name),
                    ]
                )
            )
            return coms

    def create_output_nodes(self) -> None:
        uniprot_id, pdb_id = self.get_ids()
        if uniprot_id:
            self.add_output_node(uniprot_id + ".fasta", NODE_SEQUENCE)
        elif pdb_id:
            self.add_output_node(pdb_id + ".fasta", NODE_SEQUENCE)
