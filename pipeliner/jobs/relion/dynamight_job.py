#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from typing import List, Sequence
from pathlib import Path
import shlex

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.pipeliner_job import PipelinerCommand, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    files_exts,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    MultipleChoiceJobOption,
    ExternalFileJobOption,
    JobOptionValidationResult,
    JobOptionCondition,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
    NODE_PROCESSDATA,
    #  NODE_MASK3D,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.user_settings import get_dynamight_executable
from pipeliner.data_structure import DYNAMIGHT_DIR, DYNAMIGHT_NAME


class DynamightContinuousHeterogeneity(RelionJob):
    PROCESS_NAME = DYNAMIGHT_NAME
    OUT_DIR = DYNAMIGHT_DIR
    CHECKPOINT_PATH = "forward_deformations/checkpoints/checkpoint_final.pth"
    CATEGORY_LABEL = "Continuous Heterogeneity"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "DynaMight flexibility"
        self.jobinfo.short_desc = (
            "Use DynaMight to analyse datasets that contain continuous heterogeneity,"
            " calculate deformations, and generate a consensus map"
        )
        self.jobinfo.long_desc = (
            "Estimates a continuous space of conformations in a cryo-EM data set by "
            "learning 3D deformations of a Gaussian pseudo-atomic model of a consensus "
            "structure for every particle image. Inverse deformations are calculated "
            "and used for a deformed backprojection that attempts to reconstruct an "
            "improved version of the consensus map, where densities for each particle "
            "are warped into non-straight lines in the backprojection in an attempt "
            "to “un-do” their deformations"
        )
        self.executable_name = get_dynamight_executable()
        self.jobinfo.programs = [ExternalProgram(self.executable_name)]

        self.joboptions["do_est_motion"] = BooleanJobOption(
            label="Estimate heterogeneous motions?",
            default_value=False,
            help_text=(
                "Estimate the motions present in a particle set.  This is the first "
                "step for a DynaMight job"
            ),
            jobop_group="Estimate heterogeneous motion",
        )

        self.joboptions["fn_checkpoint"] = InputNodeJobOption(
            label="OR: Use motion estimation from a previous DynaMight job",
            help_text="Select a checkpoint file from a DynaMight estimate motions job",
            node_type=NODE_PROCESSDATA,
            node_kwds=["dynamight", "checkpoint"],
            is_required=False,
            required_if=JobOptionCondition([("do_est_motion", "=", False)]),
            deactivate_if=JobOptionCondition([("do_est_motion", "=", True)]),
            jobop_group="Estimate heterogeneous motion",
        )

        self.joboptions["fn_star"] = InputNodeJobOption(
            label="Input images STAR file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            pattern=files_exts("Particles STAR file", [".star"]),
            help_text="A RELION STAR file with all images (and their metadata).",
            required_if=JobOptionCondition([("do_est_motion", "=", True)]),
            jobop_group="Estimate heterogeneous motion",
            deactivate_if=JobOptionCondition(
                [("fn_checkpoint", "!=", ""), ("do_est_motion", "=", False)],
                operation="ANY",
            ),
        )

        self.joboptions["fn_map"] = InputNodeJobOption(
            label="Consensus map:",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("Density map", [".mrc"]),
            help_text=(
                "Make sure this map has the same dimensions and the same pixel size as "
                "your input images."
            ),
            required_if=JobOptionCondition([("do_est_motion", "=", True)]),
            jobop_group="Estimate heterogeneous motion",
            deactivate_if=JobOptionCondition(
                [("fn_checkpoint", "!=", ""), ("do_est_motion", "=", False)],
                operation="ANY",
            ),
        )

        self.joboptions["nr_gaussians"] = IntJobOption(
            label="Number of Gaussians:",
            default_value=10000,
            suggested_min=5000,
            suggested_max=40000,
            step_value=1000,
            hard_min=1,
            required_if=JobOptionCondition([("do_est_motion", "=", True)]),
            help_text=(
                "Number of Gaussians to describe the consensus map with. Larger "
                "structures that one wishes to describe at higher resolutions will "
                "need more Gaussians. As a rule of thumb, you could try and use 1-2 "
                "Gaussians per amino acid or nucleotide in your complex. But note "
                "that running DynaMight with more than 30,000 Gaussians may be "
                "problematic on GPUs with a memory of 24 GB."
            ),
            jobop_group="Estimate heterogeneous motion",
            deactivate_if=JobOptionCondition(
                [("fn_checkpoint", "!=", ""), ("do_est_motion", "=", False)],
                operation="ANY",
            ),
        )

        self.joboptions["initial_threshold"] = FloatJobOption(
            label="Initial map threshold (optional):",
            default_value=-1,
            help_text=(
                "If provided, this threshold will be used to position initial "
                "Gaussians in the consensus map. If left empty, an automated procedure "
                "will be used to estimate the appropriate threshold."
            ),
            jobop_group="Estimate heterogeneous motion",
            deactivate_if=JobOptionCondition(
                [("fn_checkpoint", "!=", ""), ("do_est_motion", "=", False)],
                operation="ANY",
            ),
        )

        self.joboptions["reg_factor"] = FloatJobOption(
            label="Regularization factor:",
            default_value=1,
            suggested_min=0.2,
            suggested_max=5,
            step_value=0.1,
            hard_min=0.1,
            required_if=JobOptionCondition([("do_est_motion", "=", True)]),
            help_text=(
                "This regularization factor defines the relative weights between the "
                "data over the restraints. Values higher than one will put more "
                "weights on the restraints."
            ),
            jobop_group="Estimate heterogeneous motion",
            deactivate_if=JobOptionCondition(
                [("fn_checkpoint", "!=", ""), ("do_est_motion", "=", False)],
                operation="ANY",
            ),
        )

        self.joboptions["do_inverse"] = BooleanJobOption(
            label="Do inverse-deformation estimation?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will be run to estimate "
                "inverse-deformations. These are necessary if one want to perform "
                "deformed backprojection to calculate an improved consensus model."
            ),
            jobop_group="Calculate inverse deformations",
        )
        self.joboptions["nr_epochs"] = IntJobOption(
            label="Number of epochs to perform:",
            default_value=200,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            hard_min=1,
            help_text=(
                "Number of epochs to perform inverse deformations. You can monitor "
                "the convergence of the loss function to assess how many are "
                "necessary. Often 200 are enough"
            ),
            jobop_group="Calculate inverse deformations",
            deactivate_if=JobOptionCondition([("do_inverse", "=", False)]),
        )

        self.joboptions["adarg_inverse"] = StringJobOption(
            label="Inverse deformation additional options",
            help_text="Input any additional arguments for this step",
            jobop_group="Calculate inverse deformations",
            deactivate_if=JobOptionCondition([("do_inverse", "=", False)]),
        )

        self.joboptions["do_reconstruct"] = BooleanJobOption(
            label="Do deformed backprojection?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will be run to perform a deformed "
                "backprojection, using inverse deformations calculated in this job or "
                "a previous one, to get an improved consensus reconstruction."
            ),
            jobop_group="Consensus reconstruction",
        )

        self.joboptions["backproject_batchsize"] = IntJobOption(
            label="Backprojection batchsize:",
            default_value=10,
            hard_min=1,
            suggested_max=500,
            step_value=10,
            help_text=(
                "Number of images to process in parallel. This will speed up the "
                "calculation, but will cost GPU memory. Try how high you can go on "
                "your GPU, given your box size and size of the neural network."
            ),
            jobop_group="Consensus reconstruction",
            deactivate_if=JobOptionCondition([("do_reconstruct", "=", False)]),
        )

        self.joboptions["adarg_rec"] = StringJobOption(
            label="Back projection additional options",
            help_text="Input any additional arguments for this step",
            jobop_group="Consensus reconstruction",
            deactivate_if=JobOptionCondition([("do_reconstruct", "=", False)]),
        )

        self.joboptions["do_store_deform"] = BooleanJobOption(
            label="Store deformations in RAM?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will store deformations in the GPU memory, "
                "which will speed up the calculations, but you need to have enough "
                "GPU memory to do this..."
            ),
            jobop_group="Running options",
            deactivate_if=JobOptionCondition(
                [("do_inverse", "=", False), ("do_reconstruct", "=", False)],
                operation="ALL",
            ),
        )
        self.joboptions["gpu_id"] = StringJobOption(
            label="Which (single) GPU to use:",
            default_value="0",
            validation_regex="[0-9]+",
            regex_error_message="Can only contain digits",
            help_text=(
                "Note that DynaMight can only use one GPU at a time. Data sets with "
                "many particles or large box sizes will require powerful GPUs, like "
                "an A100."
            ),
            jobop_group="Running options",
        )
        self.joboptions["do_preload"] = BooleanJobOption(
            label="Preload images in RAM?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will preload images into memory for learning "
                "the forward or inverse deformations and for deformed backprojection. "
                "This will speed up the calculations, but you need to make sure you "
                "have enough RAM to do so."
            ),
            jobop_group="Running options",
        )

        self.joboptions["gpu_id"] = StringJobOption(
            label="Which (single) GPU to use:",
            default_value="0",
            validation_regex="[0-9]+",
            regex_error_message="Can only contain digits",
            help_text=(
                "Note that DynaMight can only use one GPU at a time. Data sets with "
                "many particles or large box sizes will require powerful GPUs, like "
                "an A100."
            ),
            jobop_group="Running options",
        )
        self.joboptions["do_preload"] = BooleanJobOption(
            label="Preload images in RAM?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will preload images into memory for learning "
                "the forward or inverse deformations and for deformed backprojection. "
                "This will speed up the calculations, but you need to make sure you "
                "have enough RAM to do so."
            ),
            jobop_group="Running options",
        )
        self.get_runtab_options(threads=True)

    def add_compatibility_joboptions(self):
        # these are the joboptions that were removed
        dynexe = self.jobinfo.programs[0].exe_path
        self.joboptions["fn_dynamight_exe"] = ExternalFileJobOption(
            label="DynaMight executable:",
            default_value=dynexe if dynexe is not None else "",
        )
        self.joboptions["halfset"] = MultipleChoiceJobOption(
            label="Half-set to visualize:",
            choices=["1", "2"],
            default_value_index=0,
            help_text=(
                "Select halfset 1 or 2 to explore the latent space of that halfset. "
                "If you select halfset 0, then the validation set is being visualised, "
                "which will give you an estimate of the errors in the deformations."
            ),
        )
        self.joboptions["do_visualize"] = BooleanJobOption(
            label="Do visualization?",
            default_value=False,
            help_text=(
                "If set to Yes, DynaMight will be run to visualize the latent space "
                "and deformed models. One can also save series of maps to make movies "
                "in Chimera, or STAR files of particle subsets within this task."
            ),
        )

    def get_est_mot_com(self) -> List[str]:
        mot_com = [self.executable_name, "optimize-deformations"]
        mot_com.extend(
            ["--refinement-star-file", self.joboptions["fn_star"].get_string()]
        )
        mot_com.extend(["--output-directory", self.output_dir])
        mot_com.extend(["--initial-model", self.joboptions["fn_map"].get_string()])
        mot_com.extend(["--n-gaussians", self.joboptions["nr_gaussians"].get_string()])

        ithresh = self.joboptions["initial_threshold"].get_number()
        if ithresh > 0:
            mot_com.extend(
                [
                    "--initial-threshold",
                    str(ithresh),
                ]
            )
        mot_com.extend(
            ["--regularization-factor", self.joboptions["reg_factor"].get_string()]
        )
        mot_com.extend(["--n-threads", self.joboptions["nr_threads"].get_string()])

        if self.joboptions["do_preload"].get_boolean():
            mot_com.append("--preload-images")

        gpu_id = self.joboptions["gpu_id"].get_string()
        if gpu_id:
            mot_com.extend(["--gpu-id", gpu_id])

        return mot_com

    # check if have to add compatibility
    def get_inv_setup_com(self, do_backproject: bool = False) -> List[str]:
        fn_check = self.joboptions["fn_checkpoint"].get_string()
        checkpoint_file = fn_check if fn_check else self.CHECKPOINT_PATH
        script_com = [
            "python3",
            get_job_script("dynamight/dynamight_setup.py"),
            "--output_dir",
            self.output_dir,
            "--checkpoint_file",
            checkpoint_file,
        ]
        if do_backproject:
            script_com.append("--do_backproject")
        return script_com

    def get_inv_com(self) -> List[str]:
        inv_com = [
            self.executable_name,
            "optimize-inverse-deformations",
            self.output_dir,
            "--n-epochs",
            self.joboptions["nr_epochs"].get_string(),
            "--checkpoint-file",
            str(Path(self.output_dir) / self.CHECKPOINT_PATH),
        ]

        if self.joboptions["do_store_deform"].get_boolean():
            inv_com.append("--save-deformations")

        if self.joboptions["do_preload"].get_boolean():
            inv_com.append("--preload-images")
        gpu_id = self.joboptions["gpu_id"].get_string()
        inv_com.extend(["--gpu-id", gpu_id])
        inv_aa = self.joboptions["adarg_inverse"].get_string()
        if inv_aa:
            inv_com.extend(shlex.split(inv_aa))
        return inv_com

    def get_rec_command(self) -> List[str]:
        rec_com = [
            self.executable_name,
            "deformable-backprojection",
            self.output_dir,
            "--batch-size",
            self.joboptions["backproject_batchsize"].get_string(),
            "--checkpoint-file",
            str(Path(self.output_dir) / self.CHECKPOINT_PATH),
        ]

        if self.joboptions["do_preload"].get_boolean():
            rec_com.append("--preload-images")

        # this feature is commented out in the relion code
        # mask = self.joboptions["fn_mask"].get_string()
        # if mask:
        #     rec_com.extend(["--mask-file", mask])

        gpu_id = self.joboptions["gpu_id"].get_string()
        rec_com.extend(["--gpu-id", gpu_id])

        rec_aa = self.joboptions["adarg_rec"].get_string()
        if rec_aa:
            rec_com.extend(shlex.split(rec_aa))
        return rec_com

    def get_commands(self) -> List[PipelinerCommand]:
        coms = []
        if self.joboptions["do_est_motion"].get_boolean():
            coms.append(self.get_est_mot_com())

        if self.joboptions["do_inverse"].get_boolean():
            if not self.joboptions["do_est_motion"].get_boolean():
                coms.append(self.get_inv_setup_com())
            coms.append(self.get_inv_com())

        if self.joboptions["do_reconstruct"].get_boolean():
            if not self.joboptions["do_inverse"].get_boolean():
                coms.append(self.get_inv_setup_com(do_backproject=True))
            coms.append(self.get_rec_command())
        if not coms:
            raise ValueError("No steps were selected to perform")

        return [PipelinerCommand(x) for x in coms]

    def create_output_nodes(self) -> None:
        self.add_output_node(
            self.CHECKPOINT_PATH, NODE_PROCESSDATA, ["dynamight.checkpoint"]
        )
        if self.joboptions["do_reconstruct"].get_boolean():
            self.add_output_node(
                "backprojection/map_half1.mrc",
                NODE_DENSITYMAP,
                ["relion", "halfmap", "dynamight"],
            )
            self.add_output_node(
                "backprojection/map_half2.mrc",
                NODE_DENSITYMAP,
                ["relion", "halfmap", "dynamight"],
            )

    def get_visualize_command(self) -> str:
        ch_path = Path(self.output_dir) / self.CHECKPOINT_PATH
        command = [
            self.executable_name,
            "explore-latent-space",
            str(Path(self.output_dir).resolve()),
            "--half-set",
            "<choose a halfset to view>",
            "--checkpoint-file",
            str(ch_path.resolve()),
        ]

        gpu = self.joboptions["gpu_id"].get_string()
        if gpu:
            command.extend(["--gpu-id", gpu])

        return " ".join(command)

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        viscom = self.get_visualize_command()
        disp_objs = [
            create_results_display_object(
                "text",
                title="Visualize continuous heterogeneity",
                display_data=(
                    "Pipeliner currently cannot display the results of a DynaMight job "
                    "in the browser.\nTo visualize the results of this job and save "
                    f"movies run the following command in the terminal:\n\n{viscom}"
                    "\n\nChoose which halfset to display by altering the --half_set "
                    "argument:\n1: view halfset 1\n2: view halfset 2\n0: view error "
                    "estimates on the 3D deformations for a validation set of 10% of "
                    "your particles"
                ),
                associated_data=[str(Path(self.output_dir) / self.CHECKPOINT_PATH)],
            ),
        ]
        if self.joboptions["do_reconstruct"].get_boolean():
            maps = [
                str(Path(self.output_dir) / "backprojection/map_half1.mrc"),
                str(Path(self.output_dir) / "backprojection/map_half2.mrc"),
            ]
            for a_map in maps:
                disp_objs.append(
                    make_map_model_thumb_and_display(
                        outputdir=self.output_dir,
                        maps=[a_map],
                    )
                )

        return disp_objs

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        est_mot = self.joboptions["do_est_motion"].get_boolean()
        inverse = self.joboptions["do_inverse"].get_boolean()
        rec = self.joboptions["do_reconstruct"].get_boolean()
        if (
            not est_mot
            and (inverse or rec)
            and not self.joboptions["fn_checkpoint"].get_string()
        ):
            errs.append(
                JobOptionValidationResult(
                    "error",
                    [self.joboptions["fn_checkpoint"]],
                    "Motion estimation was not selected, so a checkpoint file "
                    "is required",
                ),
            )
        if est_mot and not inverse and rec:
            errs.append(
                JobOptionValidationResult(
                    "error",
                    [self.joboptions["do_inverse"]],
                    "Inverse deformations must be calculated if estimated motions are "
                    "used for a consensus map",
                )
            )
        return errs
