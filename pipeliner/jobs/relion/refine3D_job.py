#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from typing import List, Dict, Any, Sequence, Tuple, Union
from gemmi import cif
import numpy as np
import logging

from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.jobs.relion.relion_job import RelionJob, relion5_available
from pipeliner.data_structure import (
    REFINE3D_PARTICLE_NAME,
    REFINE3D_HELICAL_NAME,
    REFINE3D_DIR,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_OPTIMISERDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MASK3D,
    Node,
)
from pipeliner.node_factory import create_node
from pipeliner.utils import truncate_number
from pipeliner.jobs.relion.refinement_common import (
    refinement_cleanup,
    solvmask2_node,
    find_current_opt_name,
    find_current_refine_iter,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    SAMPLING,
    files_exts,
    EXT_MRC_MAP,
    EXT_STARFILE,
    EXT_RELION_OPT,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionValidationResult,
    JobOptionCondition,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.jobs.relion.relion_job import relion_program

logger = logging.getLogger(__name__)


class RelionRefine3DJob(RelionJob):
    OUT_DIR = REFINE3D_DIR
    CATEGORY_LABEL = "3D Refinement"

    def __init__(self) -> None:
        super().__init__()
        self.del_nodes_on_continue = True
        self.jobinfo.programs = [relion_program("relion_refine")]

        self.jobinfo.long_desc = (
            "This procedure employs the 'gold-standard' method to calculate Fourier"
            " Shell Correlation (FSC) from independently refined half-reconstructions"
            " in order to estimate resolution, so that self-enhancing overfitting may"
            " be avoide. The accuracy of the angular assignments is estimated it"
            " automatically determines when a refinement has converged based on these"
            " values"
        )

        self.joboptions["fn_cont"] = InputNodeJobOption(
            label="Continue from here:",
            default_value="",
            pattern=files_exts("Optimiser STAR file", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration from which you want"
                " to continue a previous run. Note that the Output rootname of the"
                " continued run and the rootname of the previous run cannot be the"
                " same. If they are the same, the program will automatically add a"
                " '_ctX' to the output rootname, with X being the iteration from which"
                " one continues the previous run."
            ),
            in_continue=True,
            only_in_continue=True,
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "refine3d"],
        )

        self.joboptions["fn_img"] = InputNodeJobOption(
            label="Input images STAR file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("STAR files", EXT_STARFILE),
            help_text=(
                "A STAR file with all images (and their metadata). \n \n Alternatively,"
                " you may give a Spider/MRC stack of 2D images, but in that case NO"
                " metadata can be included and thus NO CTF correction can be performed,"
                " nor will it be possible to perform noise spectra estimation or"
                " intensity scale corrections in image groups. Therefore, running"
                " RELION with an input stack will in general provide sub-optimal"
                " results and is therefore not recommended!! Use the Preprocessing"
                " procedure to get the input STAR file in a semi-automated manner. Read"
                " the RELION wiki for more information."
            ),
            is_required=True,
        )

        self.joboptions["fn_ref"] = InputNodeJobOption(
            label="Reference map:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("Image File", EXT_MRC_MAP),
            help_text=(
                "A 3D map in MRC/Spider format. Make sure this map has the same"
                " dimensions and the same pixel size as your input images."
            ),
            is_required=True,
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="Reference mask (optional):",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("Image File", EXT_MRC_MAP),
            help_text=(
                "If no mask is provided, a soft spherical mask based on the particle"
                " diameter will be used.\n\nOtherwise, provide a Spider/mrc map"
                " containing a (soft) mask with the same dimensions as the"
                " reference(s), and values between 0 and 1, with 1 being 100% protein"
                " and 0 being 100% solvent. The reconstructed reference map will be"
                " multiplied by this mask.\n\n In some cases, for example for non-empty"
                " icosahedral viruses, it is also useful to use a second mask. For all"
                " white (value 1) pixels in this second mask the corresponding pixels"
                " in the reconstructed map are set to the average value of these"
                " pixels. Thereby, for example, the higher density inside the virion"
                " may be set to a constant. Note that this second mask should have"
                " one-values inside the virion and zero-values in the capsid and the"
                " solvent areas. To use a second mask, use the additional option"
                " --solvent_mask2, which may given in the Additional arguments line (in"
                " the Running tab)."
            ),
            in_continue=True,
        )

        self.joboptions["ref_correct_greyscale"] = BooleanJobOption(
            label="Ref. map is on absolute greyscale?",
            default_value=False,
            help_text=(
                "Probabilities are calculated based on a Gaussian noise model, which"
                " contains a squared difference term between the reference and the"
                " experimental image. This has a consequence that the reference needs"
                " to be on the same absolute intensity grey-scale as the experimental"
                " images. RELION and XMIPP reconstruct maps at their absolute intensity"
                " grey-scale. Other packages may perform internal normalisations of the"
                " reference density, which will result in incorrect grey-scales."
                " Therefore: if the map was reconstructed in RELION or in XMIPP, set"
                " this option to Yes, otherwise set it to No. If set to No, RELION will"
                " use a (grey-scale invariant) cross-correlation criterion in the first"
                " iteration, and prior to the second iteration the map will be filtered"
                " again using the initial low-pass filter. This procedure is relatively"
                " quick and typically does not negatively affect the outcome of the"
                " subsequent MAP refinement. Therefore, if in doubt it is recommended"
                " to set this option to No."
            ),
        )

        self.joboptions["ini_high"] = FloatJobOption(
            label="Initial low-pass filter (A):",
            default_value=60,
            hard_min=0,
            suggested_max=200,
            step_value=5,
            help_text=(
                "It is recommended to strongly low-pass filter your initial reference"
                " map.If it has not yet been low-pass filtered, it may be done"
                " internally using this option. If set to 0, no low-pass filter will be"
                " applied to the initial reference(s)."
            ),
            is_required=True,
        )

        self.joboptions["sym_name"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text=(
                "If the molecule is asymmetric, set Symmetry group to C1. Note there"
                " are multiple possibilities for icosahedral symmetry: \n* I1:"
                " No-Crowther 222 (standard in Heymann, Chagoyen & Belnap, JSB, 151"
                " (2005) 196–207) \n * I2: Crowther 222 \n * I3: 52-setting (as used in"
                " SPIDER?)\n * I4: A different 52 setting \n The command 'relion_refine"
                " --sym D2 --print_symmetry_ops' prints a list of all symmetry"
                " operators for symmetry group D2. RELION uses XMIPP's libraries for"
                " symmetry operations. Therefore, look at the XMIPP Wiki for more"
                " details:"
                " http://xmipp.cnb.csic.es/twiki/bin/view/Xmipp/WebHome?topic=Symmetry"
            ),
            is_required=True,
            validation_regex="(C[1-9][0-9]*)|(D[2-9][0-9]*)|I[243]|O|T",
            regex_error_message="Symmetry must be in Cn, Dn, I[234], O or T format",
        )

        self.joboptions["do_ctf_correction"] = BooleanJobOption(
            label="Do CTF-correction?",
            default_value=True,
            help_text=(
                "If set to Yes, CTFs will be corrected inside the MAP refinement. The"
                " resulting algorithm intrinsically implements the optimal linear, or"
                " Wiener filter. Note that CTF parameters for all images need to be"
                " given in the input STAR file. The command 'relion_refine"
                " --print_metadata_labels' will print a list of all possible metadata"
                " labels for that STAR file. See the RELION Wiki for more details.\n\n"
                " Also make sure that the correct pixel size (in Angstrom) is given"
                " above!)"
            ),
        )

        self.joboptions["ctf_intact_first_peak"] = BooleanJobOption(
            label="Ignore CTFs until first peak?",
            default_value=False,
            help_text=(
                "If set to Yes, then CTF-amplitude correction will only be performed"
                " from the first peak of each CTF onward. This can be useful if the CTF"
                " model is inadequate at the lowest resolution. Still, in general using"
                " higher amplitude contrast on the CTFs (e.g. 10-20%) often yields"
                " better results. Therefore, this option is not generally recommended:"
                " try increasing amplitude contrast (in your input STAR file) first!"
            ),
            deactivate_if=JobOptionCondition([("do_ctf_correction", "=", False)]),
        )

        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Mask diameter (A):",
            default_value=200,
            hard_min=0,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "The experimental images will be masked with a soft circular mask with"
                " this diameter. Make sure this radius is not set too small because"
                " that may mask away part of the signal! If set to a value larger than"
                " the image size no masking will be performed.\n\n The same diameter"
                " will also be used for a spherical mask of the reference structures if"
                " no user-provided mask is specified."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["do_zero_mask"] = BooleanJobOption(
            label="Mask individual particles with zeros?",
            default_value=True,
            help_text=(
                "If set to Yes, then in the individual particles, the area outside a"
                " circle with the radius of the particle will be set to zeros prior to"
                " taking the Fourier transform. This will remove noise and therefore"
                " increase sensitivity in the alignment and classification. However, it"
                " will also introduce correlations between the Fourier components that"
                " are not modelled. When set to No, then the solvent area is filled"
                " with random noise, which prevents introducing correlations."
                " High-resolution refinements (e.g. ribosomes or other large complexes"
                " in 3D auto-refine) tend to work better when filling the solvent area"
                " with random noise (i.e. setting this option to No), refinements of"
                " smaller complexes and most classifications go better when using zeros"
                " (i.e. setting this option to Yes)."
            ),
        )

        self.joboptions["do_solvent_fsc"] = BooleanJobOption(
            label="Use solvent-flattened FSCs?",
            default_value=False,
            help_text=(
                "If set to Yes, then instead of using unmasked maps to calculate the"
                " gold-standard FSCs during refinement, masked half-maps are used and a"
                " post-processing-like correction of the FSC curves (with"
                " phase-randomisation) is performed every iteration. This only works"
                " when a reference mask is provided on the I/O tab. This may yield"
                " higher-resolution maps, especially when the mask contains only a"
                " relatively small volume inside the box."
            ),
            in_continue=True,
        )
        self.joboptions["do_blush"] = BooleanJobOption(
            label="Use Blush regularisation?",
            default_value=False,
            help_text=(
                "If set to Yes, relion_refine will use a neural network to perform "
                "regularisation by denoising at every iteration, instead of the "
                "standard smoothness regularisation."
            ),
        )

        self.joboptions["sampling"] = MultipleChoiceJobOption(
            label="Angular sampling interval:",
            choices=SAMPLING,
            default_value_index=2,
            help_text=(
                "There are only a few discrete angular samplings possible because we"
                " use the HealPix library to generate the sampling of the first two"
                " Euler angles on the sphere. The samplings are approximate numbers and"
                " vary slightly over the sphere.\n\n If auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
        )

        self.joboptions["offset_range"] = FloatJobOption(
            label="Offset search range (pix):",
            default_value=5,
            hard_min=0,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Probabilities will be calculated only for translations in a circle"
                " with this radius (in pixels). The center of this circle changes at"
                " every iteration and is placed at the optimal translation for each"
                " image in the previous iteration.\n\n If auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
        )

        self.joboptions["offset_step"] = FloatJobOption(
            label="Offset search step (pix):",
            default_value=1,
            suggested_min=0.1,
            suggested_max=5,
            step_value=0.1,
            hard_min=0.0,
            help_text=(
                "Translations will be sampled with this step-size (in pixels)."
                " Translational sampling is also done using the adaptive approach."
                " Therefore, if adaptive=1, the translations will first be evaluated on"
                " a 2x coarser grid.\n\n If auto-sampling is used, this will be the"
                " value for the first iteration(s) only, and the sampling rate will be"
                " increased automatically after that."
            ),
        )

        self.joboptions["auto_local_sampling"] = MultipleChoiceJobOption(
            label="Local searches from auto-sampling:",
            choices=SAMPLING,
            default_value_index=4,
            help_text=(
                "In the automated procedure to increase the angular samplings, local"
                " angular searches of -6/+6 times the sampling rate will be used from"
                " this angular sampling rate onwards. For most lower-symmetric"
                " particles a value of 1.8 degrees will be sufficient. Perhaps"
                " icosahedral symmetries may benefit from a smaller value such as 0.9"
                " degrees."
            ),
        )

        self.joboptions["relax_sym"] = StringJobOption(
            label="Relax symmetry:",
            default_value="",
            help_text=(
                " With this option, poses related to the standard local angular search"
                "range by the given point group will also be explored.For example, if "
                "you have a pseudo-symmetric dimer A-A', refinement or classification "
                "in C1 with symmetry relaxation by C2 might be able to improve "
                "distinction between A and A'.Note that the reference must be more- or"
                " -less aligned to the convention of (pseudo-) symmetry operators. For"
                "details, see Ilca et al. 2019 and Abrishami et al 2020"
            ),
            in_continue=True,
        )

        self.joboptions["auto_faster"] = BooleanJobOption(
            label="Use finer angular sampling faster?",
            default_value=False,
            help_text=(
                "If set to Yes, then let auto-refinement proceed faster with finer"
                " angular samplings. Two additional command-line options will be passed"
                " to the refine program: \n --auto_ignore_angles lets angular sampling"
                " go down despite changes still happening in the angles \n \n"
                " --auto_resol_angles lets angular sampling go down if the current"
                " resolution already requires that sampling at the edge of the"
                " particle.  \n\n This option will make the computation faster, but"
                " hasn't been tested for many cases for potential loss in"
                " reconstruction quality upon convergence."
            ),
            in_continue=True,
        )

        self.get_comp_options()
        self.get_runtab_options(
            mpi=True,
            threads=True,
            addtl_args=True,
            mpi_default_min=3,
            mpi_must_be_odd=True,
        )
        self.joboptions["nr_mpi"].default_value = 3

    def common_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        nr_mpi = self.joboptions["nr_mpi"].get_number()
        if nr_mpi < 3:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["nr_mpi"]],
                    message="Refine jobs need at least 2 mpi to run",
                )
            )
        if not nr_mpi % 2 and nr_mpi > 3:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["nr_mpi"]],
                    message="An odd number of MPIs must be used",
                )
            )
        if self.joboptions["do_blush"].get_boolean() and not relion5_available():
            errs.append(
                JobOptionValidationResult(
                    "error",
                    raised_by=[self.joboptions["do_blush"]],
                    message="Blush is only available if RELION 5 is installed",
                )
            )
        return errs

    def common_commands(self) -> None:
        self.command = self.get_relion_command("refine")

        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string()
            pos_it = int(fn_cont.partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for "
                    "continuation run!",
                )
            self.command += ["--continue", fn_cont]

        elif not self.is_continue:
            fn_img = self.joboptions["fn_img"].get_string()
            self.command += ["--i", fn_img]

        self.command += ["--o", os.path.join(self.output_dir, "run")]

        self.command += ["--auto_refine", "--split_random_halves"]

        if self.joboptions["do_blush"].get_boolean():
            if not relion5_available():
                logger.warning(
                    "Blush regularization was requested but cannot be"
                    " performed because RELION 5 was not found"
                )
            self.command.append("--blush")

        fn_ref = self.joboptions["fn_ref"].get_string()

        if fn_ref not in ["None", "none"]:
            self.command += ["--ref", fn_ref]

            ref_correct_greyscale = self.joboptions[
                "ref_correct_greyscale"
            ].get_boolean()

            if not ref_correct_greyscale:  # dont do firstiter_cc when giving None
                self.command.append("--firstiter_cc")

        ini_high = self.joboptions["ini_high"].get_number()
        if ini_high > 0:
            self.command += ["--ini_high", truncate_number(ini_high, 2)]

        # do compute options
        self.add_comp_options()
        if self.joboptions["auto_faster"].get_boolean():
            self.command += ["--auto_ignore_angles", "--auto_resol_angles"]

        # ctf stuff
        if not self.is_continue:
            if self.joboptions["do_ctf_correction"].get_boolean():
                self.command.append("--ctf")
                if self.joboptions["ctf_intact_first_peak"].get_boolean():
                    self.command.append("--ctf_intact_first_peak")
            particle_diameter = self.joboptions["particle_diameter"].get_string()
            self.command += ["--particle_diameter", particle_diameter]
            # always flatten solvent
            self.command.append("--flatten_solvent")
            if self.joboptions["do_zero_mask"].get_boolean():
                self.command.append("--zero_mask")

        # mask
        fn_mask = self.joboptions["fn_mask"].get_string()
        if fn_mask:
            self.command += ["--solvent_mask", fn_mask]
            if self.joboptions["do_solvent_fsc"].get_boolean():
                self.command.append("--solvent_correct_fsc")

        if not self.is_continue:
            iover = 1
            self.command += ["--oversampling", str(iover)]
            sampling_opts = SAMPLING
            sampling_opt = self.joboptions["sampling"].get_string()
            sampling = sampling_opts.index(sampling_opt) + 1
            # The sampling given in the GUI will be the oversampled one!
            self.command += ["--healpix_order", str(sampling - iover)]

            autolocal_opt = self.joboptions["auto_local_sampling"].get_string()
            # check this
            autolocal = sampling_opts.index(autolocal_opt)
            self.command += ["--auto_local_healpix_order", str(autolocal)]

            # Offset range
            offset_range = self.joboptions["offset_range"].get_number()
            self.command += ["--offset_range", truncate_number(offset_range, 2)]

            # The sampling given in the GUI will be the oversampled one!
            offset_step = self.joboptions["offset_step"].get_number()
            offset_step = offset_step * (2**iover)
            self.command += ["--offset_step", truncate_number(offset_step, 2)]

            sym_name = self.joboptions["sym_name"].get_string()

            # Provide symmetry, and always do norm and scale correction
            self.command += [
                "--sym",
                sym_name,
                "--low_resol_join_halves",
                "40",
                "--norm",
                "--scale",
            ]
            relax_sym = self.joboptions["relax_sym"].get_string()
            if relax_sym != "":
                self.command.extend(["--relax_sym", relax_sym])

    def running_commands(self) -> None:
        # Running stuff
        self.command += ["--j", self.joboptions["nr_threads"].get_string()]

        # GPU-stuff
        if self.joboptions["use_gpu"].get_boolean():
            gpu_ids = self.joboptions["gpu_ids"].get_string()
            self.command += ["--gpu", gpu_ids]

        # Other arguments
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            solvmask2_node(self)
            self.command += self.parse_additional_args()

    def make_output_nodes(self, is_helical=False) -> None:
        node_kwds = ["relion", "refine3d"]
        node_kwds = node_kwds + ["helical"] if is_helical else node_kwds
        self.add_output_node("run_data.star", NODE_PARTICLEGROUPMETADATA, node_kwds)
        self.add_output_node("run_optimiser.star", NODE_OPTIMISERDATA, node_kwds)
        sym_name = self.joboptions["sym_name"].get_string()
        if sym_name.lower() != "c1":
            node_kwds.append(sym_name.lower() + "sym")
        self.add_output_node(
            "run_half1_class001_unfil.mrc", NODE_DENSITYMAP, node_kwds + ["halfmap"]
        )
        self.add_output_node(
            "run_half2_class001_unfil.mrc", NODE_DENSITYMAP, node_kwds + ["halfmap"]
        )
        self.add_output_node("run_class001.mrc", NODE_DENSITYMAP, node_kwds)

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        return refinement_cleanup(self, do_harsh)

    def get_metadata(self, is_helical) -> Dict[str, object]:
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata"""

        def get_3drefine_metadata(modelfile, datafile) -> Dict[str, object]:
            """Return info about a 3D map including detailed info about the
            contributing particles and micrographs"""
            got_data: Dict[str, object] = {}
            model_data = cif.read(modelfile)

            # get the general data
            general_block = model_data.find_block("model_general")
            gen_data = [
                "_rlnCurrentImageSize",
                "_rlnPixelSize",
                "_rlnNormCorrectionAverage",
                "_rlnSigmaOffsetsAngst",
                "_rlnSigmaPriorRotAngle",
                "_rlnSigmaPriorTiltAngle",
                "_rlnSigmaPriorPsiAngle",
                "_rlnLogLikelihood",
                "_rlnAveragePmax",
            ]
            for i in gen_data:
                pair = general_block.find_pair(i)
                try:
                    val = float(pair[1])
                except ValueError:
                    val = pair[1]
                got_data[pair[0].replace("_rln", "")] = val

            # get the map specific data
            map_block = model_data.find_block("model_classes")
            cols = [
                "_rlnReferenceImage",
                "_rlnAccuracyRotations",
                "_rlnAccuracyTranslationsAngst",
                "_rlnEstimatedResolution",
                "_rlnOverallFourierCompleteness",
            ]
            map_table = map_block.find(cols)
            got_data["FileName"] = map_table[0][0]
            for n, i in enumerate(cols[1:]):
                got_data[i.replace("_rln", "")] = float(map_table[0][n + 1])

            # get the particles info
            parts_data = cif.read(datafile)
            parts_block = parts_data.find_block("particles")

            # counts
            mics = parts_block.find(["_rlnMicrographname"])
            all_parts = [x[0] for x in mics]
            parts_count = len(all_parts)
            mics_count = len(set(all_parts))
            got_data["ParticleNumber"] = parts_count
            got_data["ContributingMicrographs"] = mics_count

            # ctf info
            df_table = parts_block.find(["_rlnDefocusU", "_rlnDefocusV"])
            dfs = [(float(x[0]) + float(x[1])) / 2 for x in df_table]
            df_mean = np.mean(dfs)
            df_std = np.std(dfs)
            df_min = min(dfs)
            df_max = max(dfs)

            got_data["MeanDefocus"] = float(df_mean)
            got_data["STDDefocus"] = float(df_std)
            got_data["MinDefocus"] = df_min
            got_data["MaxDefocus"] = df_max

            # more data could be put in if desired

            return got_data

        def get_halfmap_metadata(modelfile) -> Dict[str, object]:
            """Return information about a halfmap

            Returns the particle count but there doesn;t seem to be a way to
            identify which particles went into the half map, so no detailed
            info"""

            got_data: Dict[str, object] = {}
            model_data = cif.read(modelfile)

            # get the map specific data
            map_block = model_data.find_block("model_classes")
            cols = [
                "_rlnReferenceImage",
                "_rlnAccuracyRotations",
                "_rlnAccuracyTranslationsAngst",
                "_rlnEstimatedResolution",
                "_rlnOverallFourierCompleteness",
            ]
            map_table = map_block.find(cols)
            got_data["FileName"] = map_table[0][0]
            for n, i in enumerate(cols[1:]):
                got_data[i.replace("_rln", "")] = float(map_table[0][n + 1])

            # get the particles info
            parts_block = model_data.find_block("model_groups")

            # counts
            group_nos = [int(x[0]) for x in parts_block.find(["_rlnGroupNrParticles"])]
            parts_count = sum(group_nos)
            got_data["ParticleNumber"] = parts_count

            return got_data

        metadata_dict: Dict[str, Any] = {}
        all_data_files = glob(self.output_dir + "run_it*_data.star")
        output_model = os.path.join(self.output_dir, "run_model.star")
        all_data_files.sort()
        last_output_data = all_data_files[-1]
        iter_name = os.path.basename(last_output_data)[:9]
        output_data = os.path.join(self.output_dir, "run_data.star")

        # main model
        metadata_dict["Map"] = get_3drefine_metadata(output_model, output_data)

        # halfmap1
        halfmap1 = os.path.join(self.output_dir, iter_name + "_half1_model.star")
        metadata_dict["Halfmap1"] = get_halfmap_metadata(halfmap1)

        # halfmap2
        halfmap2 = os.path.join(self.output_dir, iter_name + "_half2_model.star")
        metadata_dict["Halfmap2"] = get_halfmap_metadata(halfmap2)

        if is_helical:
            # todo: some additional helical metatdata here
            pass

        return metadata_dict

    # needs to return EMPIAR particles object and OneDep Final Rec object
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:

        if depo_type == "EMPIAR":
            mpfile = os.path.join(self.output_dir, "run_data.star")
            return prepare_empiar_parts(mpfile)

        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # use the final map if present
        cmap = os.path.join(self.output_dir, "run_class001.mrc")
        cmodel = os.path.join(self.output_dir, "run_model.star")
        mapname = "Final reconstruction"
        if not os.path.isfile(cmap):
            # otherwise do the current iter
            citer = find_current_refine_iter(self.output_dir)
            chalf = f"run_it{citer:03d}_half1_class001.mrc"
            cmap = os.path.join(self.output_dir, chalf)
            cmodel = cmap.replace("class001.mrc", "model.star")
            mapname = f"Intermediate reconstruction - iteration {citer}"

        gblock = DataStarFile(cmodel).get_block("model_general")
        reso = float(gblock.find_value("_rlnCurrentResolution"))
        output = {cmap: f"{mapname} - {round(reso, 2)} Å"}
        return make_maps_slice_montage_and_3d_display(
            in_maps=output,
            output_dir=self.output_dir,
        )


class RelionRefine3D(RelionRefine3DJob):
    PROCESS_NAME = REFINE3D_PARTICLE_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION 3D auto-refine (single particle)"

        self.jobinfo.short_desc = (
            "Automatic refinement of a 3D volume from 2D particles"
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        return self.common_joboption_validation()

    def create_output_nodes(self) -> None:
        self.make_output_nodes()

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()
        self.running_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def gather_metadata(self) -> Dict[str, Any]:
        return self.get_metadata(is_helical=False)

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "refine3d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["relion", "refine3d"]
        )
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_root += "half1_class???.mrc"
        fn_maps = glob(fn_root)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = create_node(m, NODE_DENSITYMAP, ["relion", "halfmap"])
                out_nodes.append(map_node)

        return out_nodes


class RelionRefine3DHelical(RelionRefine3DJob):
    PROCESS_NAME = REFINE3D_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION 3D auto-refine (helical)"

        self.jobinfo.short_desc = (
            "Automatic refinement of a 3D volume from 2D helical segments"
        )
        assert isinstance(self.joboptions["fn_img"], InputNodeJobOption)
        self.joboptions["fn_img"].node_type = NODE_PARTICLEGROUPMETADATA
        self.joboptions["fn_img"].node_kwds = ["relion", "helicalsegments"]

        self.joboptions["helical_tube_inner_diameter"] = FloatJobOption(
            label="Tube diameter - inner (A):",
            default_value=-1,
            help_text=(
                "Inner and outer diameter (in Angstroms) of the reconstructed helix"
                " spanning across Z axis. Set the inner diameter to negative value if"
                " the helix is not hollow in the center. The outer diameter should be"
                " slightly larger than the actual width of helical tubes because it"
                " also decides the shape of 2D particle mask for each segment. If the"
                " psi priors of the extracted segments are not accurate enough due to"
                " high noise level or flexibility of the structure, then set the outer"
                " diameter to a large value."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_tube_outer_diameter"] = FloatJobOption(
            label="Tube diameter - outer (A):",
            default_value=-1,
            hard_min=0,
            help_text=(
                "Inner and outer diameter (in Angstroms) of the reconstructed helix"
                " spanning across Z axis. Set the inner diameter to negative value if"
                " the helix is not hollow in the center. The outer diameter should be"
                " slightly larger than the actual width of helical tubes because it"
                " also decides the shape of 2D particle mask for each segment. If the"
                " psi priors of the extracted segments are not accurate enough due to"
                " high noise level or flexibility of the structure, then set the outer"
                " diameter to a large value."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["range_rot"] = FloatJobOption(
            label="Angular search range - rot (deg):",
            default_value=15,
            hard_min=0,
            help_text=(
                "Local angular searches will be performed within +/- of the given"
                " amount (in degrees) from the optimal orientation in the previous"
                " iteration. The default negative value means that no local searches"
                " will be performed. A Gaussian prior will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
        )

        self.joboptions["range_tilt"] = FloatJobOption(
            label="Angular search range - tilt (deg):",
            default_value=15,
            hard_min=0,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the optimal orientation in the previous iteration."
                " A Gaussian prior (also see previous option) will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
        )

        self.joboptions["range_psi"] = FloatJobOption(
            label="Angular search range - psi (deg):",
            default_value=10,
            hard_min=0,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the optimal orientation in the previous iteration."
                " A Gaussian prior (also see previous option) will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
        )

        self.joboptions["do_apply_helical_symmetry"] = BooleanJobOption(
            label="Apply helical symmetry?",
            default_value=True,
            help_text=(
                "If set to Yes, helical symmetry will be applied in every iteration."
                " Set to No if you have just started a project, helical symmetry is"
                " unknown or not yet estimated."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_nr_asu"] = IntJobOption(
            label="Number of unique asymmetrical units:",
            default_value=1,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of unique helical asymmetrical units in each segment box. If"
                " the inter-box distance (set in segment picking step) is 100 Angstroms"
                " and the estimated helical rise is ~20 Angstroms, then set this value"
                " to 100 / 20 = 5 (nearest integer). This integer should not be less"
                " than 1. The correct value is essential in measuring the signal to"
                " noise ratio in helical reconstruction."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_initial"] = FloatJobOption(
            label="Initial helical twist (deg):",
            default_value=0,
            help_text=(
                "Initial helical symmetry. Set helical twist (in degrees) to positive"
                " value if it is a right-handed helix. Helical rise is a positive value"
                " in Angstroms. If local searches of helical symmetry are planned,"
                " initial values of helical twist and rise should be within their"
                " respective ranges."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_initial"] = FloatJobOption(
            label="Initial helical rise (A):",
            default_value=0,
            hard_min=0,
            help_text=(
                "Initial helical symmetry. Set helical twist (in degrees) to positive"
                " value if it is a right-handed helix. Helical rise is a positive value"
                " in Angstroms. If local searches of helical symmetry are planned,"
                " initial values of helical twist and rise should be within their"
                " respective ranges."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_z_percentage"] = FloatJobOption(
            label="Central Z length (%):",
            default_value=30.0,
            suggested_min=5.0,
            suggested_max=80.0,
            step_value=1.0,
            hard_min=0,
            help_text=(
                "Reconstructed helix suffers from inaccuracies of orientation searches."
                " The central part of the box contains more reliable information"
                " compared to the top and bottom parts along Z axis, where Fourier"
                " artefacts are also present if the number of helical asymmetrical"
                " units is larger than 1. Therefore, information from the central part"
                " of the box is used for searching and imposing helical symmetry in"
                " real space. Set this value (%) to the central part length along Z"
                " axis divided by the box size. Values around 30% are commonly used."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["do_local_search_helical_symmetry"] = BooleanJobOption(
            label="Do local searches of symmetry?",
            default_value=False,
            help_text=(
                "If set to Yes, then perform local searches of helical twist and rise"
                " within given ranges."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_min"] = FloatJobOption(
            label="Helical twist search (deg) - Min:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_max"] = FloatJobOption(
            label="Helical twist search (deg) - Max:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_inistep"] = FloatJobOption(
            label="Helical twist search (deg) - Step:",
            default_value=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_min"] = FloatJobOption(
            label="Helical rise search (A) - Min:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_max"] = FloatJobOption(
            label="Helical rise search (A) - Max:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_inistep"] = FloatJobOption(
            label="Helical rise search (A) - Step:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_range_distance"] = FloatJobOption(
            label="Range factor of local averaging:",
            default_value=-1.0,
            suggested_max=5.0,
            step_value=0.1,
            help_text=(
                "Local averaging of orientations and translations will be performed"
                " within a range of +/- this value * the box size. Polarities are also"
                " set to be the same for segments coming from the same tube during"
                " local refinement. Values of ~ 2.0 are recommended for flexible"
                " structures such as MAVS-CARD filaments, ParM, MamK, etc. This option"
                " might not improve the reconstructions of helices formed from curled"
                " 2D lattices (TMV and VipA/VipB). Set to negative to disable this"
                " option."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["keep_tilt_prior_fixed"] = BooleanJobOption(
            label="Keep tilt-prior fixed:",
            default_value=True,
            help_text=(
                "If set to yes, the tilt prior will not change during the optimisation."
                " If set to No, at each iteration the tilt prior will move to the"
                " optimal tilt value for that segment from the previous iteration."
            ),
            jobop_group="Helical processing options",
        )

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "fn_mask",
                "ref_correct_greyscale",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "particle_diameter",
                "helical_tube_inner_diameter",
                "helical_tube_outer_diameter",
                "range_rot",
                "range_tilt",
                "range_psi",
                "do_apply_helical_symmetry",
                "helical_nr_asu",
                "helical_twist_initial",
                "helical_rise_initial",
                "helical_z_percentage",
                "do_local_search_helical_symmetry",
                "helical_twist_min",
                "helical_twist_max",
                "helical_twist_inistep",
                "helical_rise_min",
                "helical_rise_max",
                "helical_rise_inistep",
                "helical_range_distance",
                "keep_tilt_prior_fixed",
                "do_zero_mask",
                "do_solvent_fsc",
                "sampling",
                "offset_range",
                "offset_step",
                "auto_local_sampling",
                "relax_sym",
                "auto_faster",
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = self.common_joboption_validation()
        if self.joboptions["do_local_search_helical_symmetry"].get_boolean():
            r_step = self.joboptions["helical_rise_inistep"]
            r_max = self.joboptions["helical_rise_max"]
            r_min = self.joboptions["helical_rise_min"]
            r_minmax = r_min.get_number() > r_max.get_number()
            if r_minmax:
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[r_min, r_max],
                        message="Rise min > rise max",
                    )
                )
            if (
                r_step.get_number() > r_max.get_number() - r_min.get_number()
                and not r_minmax
            ):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[r_step],
                        message="Step is bigger than total range",
                    )
                )

            t_max = self.joboptions["helical_twist_max"]
            t_min = self.joboptions["helical_twist_min"]
            t_step = self.joboptions["helical_twist_inistep"]
            t_minmax = t_min.get_number() > t_max.get_number()
            if t_minmax:
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[t_min, t_max],
                        message="Twist min > twist max",
                    )
                )
            if (
                t_step.get_number() > t_max.get_number() - t_min.get_number()
                and not t_minmax
            ):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[t_step],
                        message="Step is bigger than total range",
                    )
                )
        return errors

    def create_output_nodes(self) -> None:
        self.make_output_nodes(is_helical=True)

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()
        # helical commands
        self.command.append("--helix")
        inner_diam = self.joboptions["helical_tube_inner_diameter"].get_number()
        if inner_diam > 0:
            self.command += ["--helical_inner_diameter", truncate_number(inner_diam, 2)]
        outer_diam = self.joboptions["helical_tube_outer_diameter"].get_string()
        self.command += ["--helical_outer_diameter", outer_diam]

        if self.joboptions["do_apply_helical_symmetry"].get_boolean():
            helical_nr_asu = self.joboptions["helical_nr_asu"].get_string()
            ini_twist = self.joboptions["helical_twist_initial"].get_string()
            ini_rise = self.joboptions["helical_rise_initial"].get_string()
            myz = self.joboptions["helical_z_percentage"].get_number() / 100.0
            self.command += [
                "--helical_nr_asu",
                helical_nr_asu,
                "--helical_twist_initial",
                ini_twist,
                "--helical_rise_initial",
                ini_rise,
                "--helical_z_percentage",
                truncate_number(myz, 2),
            ]

            if self.joboptions["do_local_search_helical_symmetry"].get_boolean():
                twist_min = self.joboptions["helical_twist_min"].get_string()
                twist_max = self.joboptions["helical_twist_max"].get_string()
                self.command += [
                    "--helical_symmetry_search",
                    "--helical_twist_min",
                    twist_min,
                    "--helical_twist_max",
                    twist_max,
                ]

                twist_inistep = self.joboptions["helical_twist_inistep"].get_number()
                if twist_inistep > 0:
                    self.command += [
                        "--helical_twist_inistep",
                        truncate_number(twist_inistep, 2),
                    ]

                rise_min = self.joboptions["helical_rise_min"].get_string()
                rise_max = self.joboptions["helical_rise_max"].get_string()
                self.command += [
                    "--helical_rise_min",
                    rise_min,
                    "--helical_rise_max",
                    rise_max,
                ]

                rise_inistep = self.joboptions["helical_rise_inistep"].get_number()
                if rise_inistep > 0:
                    self.command += [
                        "--helical_rise_inistep",
                        truncate_number(rise_inistep, 2),
                    ]

        else:
            self.command.append("--ignore_helical_symmetry")

        # range options
        rt_jo = self.joboptions["range_tilt"].get_number()
        rt_val = float(min(max(rt_jo, 0), 90))
        psi_jo = self.joboptions["range_psi"].get_number()
        psi_val = float(min(max(psi_jo, 0), 90))
        rr_jo = self.joboptions["range_rot"].get_number()
        rr_val = float(min(max(rr_jo, 0), 90))

        self.command += [
            "--sigma_tilt",
            truncate_number((rt_val / 3.0), 5),
            "--sigma_psi",
            truncate_number((psi_val / 3.0), 5),
            "--sigma_rot",
            truncate_number((rr_val / 3.0), 5),
        ]

        hd_val = self.joboptions["helical_range_distance"].get_number()
        if hd_val > 0:
            self.command += [
                "--helical_sigma_distance",
                truncate_number(hd_val / 3.0, 5),
            ]

        if self.joboptions["keep_tilt_prior_fixed"].get_boolean():
            self.command.append("--helical_keep_tilt_prior_fixed")

        self.running_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def gather_metadata(self) -> Dict[str, object]:
        return self.get_metadata(is_helical=True)

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(
            current_opt,
            NODE_OPTIMISERDATA,
            ["relion", "refine3d", "helical"],
        )

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["relion", "refine3d", "helical"]
        )
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_root += "half1_class???.mrc"
        fn_maps = glob(fn_root)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = create_node(
                    m, NODE_DENSITYMAP, ["relion", "halfmap", "helical"]
                )
                out_nodes.append(map_node)

        return out_nodes
