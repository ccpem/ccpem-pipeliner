#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import List, Dict, Any, Sequence, Union


from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.jobs.relion.relion_job import relion_program, RelionJob
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    InputNodeJobOption,
    BooleanJobOption,
    files_exts,
    EXT_STARFILE,
    JobOptionValidationResult,
)
from pipeliner.data_structure import (
    JOINSTAR_MOVIES_NAME,
    JOINSTAR_MICS_NAME,
    JOINSTAR_PARTS_NAME,
    JOINSTAR_DIR,
)
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_raw_mics,
    prepare_empiar_mics,
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
    EmpiarCorrectedMics,
    EmpiarMovieSet,
)
from pipeliner.display_tools import mini_montage_from_starfile
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionJoinStarJob(RelionJob):
    OUT_DIR = JOINSTAR_DIR
    CATEGORY_LABEL = "Join STAR Files"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_star_handler")]
        self.jobinfo.long_desc = (
            "Join together starfiles that contain the same type of data. The files must"
            " contain the same metadata columns in the same order to be joined"
            " successfully"
        )

    def make_output_nodes(self, ftype) -> None:
        node_types = {
            "particles": NODE_PARTICLEGROUPMETADATA,
            "movies": NODE_MICROGRAPHMOVIEGROUPMETADATA,
            "mics": NODE_MICROGRAPHGROUPMETADATA,
        }
        self.add_output_node(f"join_{ftype}.star", node_types[ftype], ["relion"])

    def make_join_command(self, files, ftype) -> List[Union[int, float, str]]:
        star_cols = {
            "particles": "rlnImageName",
            "movies": "rlnMicrographMovieName",
            "mics": "rlnMicrographName",
        }
        command: List[Union[int, float, str]] = [
            "relion_star_handler",
            "--combine",
            "--i",
        ]
        command += files
        command += ["--check_duplicates", star_cols[ftype]]
        command += ["--o", os.path.join(self.output_dir, f"join_{ftype}.star")]
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command += self.parse_additional_args()
        return command

    def add_common_compatibility_joboptions(self, jtype: str) -> None:
        # add the do_ jobops
        self.joboptions["do_mic"] = BooleanJobOption(
            label="Combine micrograph STAR files?",
            default_value=jtype == "mic",
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_mov"] = BooleanJobOption(
            label="Combine movie STAR files?",
            default_value=jtype == "mov",
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_part"] = BooleanJobOption(
            label="Combine particle STAR files?",
            default_value=jtype == "part",
            jobop_group="Relion compatibility options",
        )

        # add the file joboptions
        for n in range(1, 5):
            self.joboptions[f"fn_mov{n}"] = InputNodeJobOption(
                label=f"Movie STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            )
            self.joboptions[f"fn_mic{n}"] = InputNodeJobOption(
                label=f"Micrograph STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type=NODE_MICROGRAPHGROUPMETADATA,
            )
            self.joboptions[f"fn_part{n}"] = InputNodeJobOption(
                label=f"Particle STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type=NODE_PARTICLEGROUPMETADATA,
            )
        # put the values in the appropriate ones
        fjobop = self.joboptions[f"fn_{jtype}s"]

        files = fjobop.get_list()
        for n, f in enumerate(files):
            if n == 5:
                break
            self.joboptions[f"fn_{jtype}{n+1}"].value = f

        # set the do_ jobops
        self.joboptions[f"do_{jtype}"].value = "Yes"

    def handle_relionstyle_jobfiles(self, jtype: str, longname: str) -> None:
        """Input files from relion have different fields from the pipeliner version"""

        if not self.joboptions[f"fn_{jtype}s"].get_string():
            relionstyle_names = [
                f"fn_{jtype}1",
                f"fn_{jtype}2",
                f"fn_{jtype}3",
                f"fn_{jtype}4",
                f"{longname} STAR file 1:",
                f"{longname} STAR file 2:",
                f"{longname} STAR file 3:",
                f"{longname} STAR file 4:",
            ]
            the_files = []
            for i in relionstyle_names:
                val = self.raw_options.get(i)
                if val:
                    the_files.append(val)
            joinfiles = ":::".join(the_files) if the_files else ""
            self.joboptions[f"fn_{jtype}s"].value = joinfiles

    def general_metadata_gathering(self, filetype: str) -> Dict[str, object]:

        metadata_dict: Dict[str, Any] = {}

        starfile = DataStarFile(
            os.path.join(self.output_dir, "join_" + filetype + ".star")
        )
        n_opticsgroups = starfile.count_block("optics")
        metadata_dict["NumberOfOpticsGroups"] = n_opticsgroups
        if filetype == "movies":
            metadata_dict["NumberOfMovies"] = starfile.count_block("movies")
        if filetype == "mics":
            metadata_dict["NumberOfMicrographs"] = starfile.count_block("micrographs")
        if filetype == "particles":
            metadata_dict["NumberOfParticles"] = starfile.count_block("particles")

        return metadata_dict

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        mov = self.joboptions.get("fn_movs")
        mic = self.joboptions.get("fn_mics")
        part = self.joboptions.get("fn_parts")
        files = []
        for jobop in (mov, mic, part):
            if jobop is not None:
                files.extend(jobop.get_list())
        if len(files) < 2:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[x for x in (mov, mic, part) if x is not None],
                    message="Not enough files selected, select at least two to join",
                )
            ]
        return []


class RelionJoinStarMovies(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MOVIES_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION join movie STAR files"

        self.jobinfo.short_desc = "Join multiple movie STAR files into a single file"

        self.joboptions["fn_movs"] = MultiInputNodeJobOption(
            label="Movie STAR files:",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The micrograph movie STAR files to be combined.",
        )
        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_movs"])

    def create_input_nodes(self) -> None:
        """Override to handle Relion-style job files."""
        self.handle_relionstyle_jobfiles("mov", "Movie")
        super().create_input_nodes()

    def create_output_nodes(self) -> None:
        self.make_output_nodes("movies")

    def get_commands(self) -> List[PipelinerCommand]:
        command = self.make_join_command(
            self.joboptions["fn_movs"].get_list(), "movies"
        )
        return [PipelinerCommand(command, relion_control=True)]

    def prepare_deposition_data(self, depo_type: str) -> List[EmpiarMovieSet]:
        if depo_type == "EMPIAR":
            movfile = os.path.join(self.output_dir, "join_movies.star")
            return prepare_empiar_raw_mics(movfile)
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def gather_metadata(self) -> Dict[str, object]:
        metadata = self.general_metadata_gathering("movies")
        return metadata

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="movies",
                column="_rlnMicrographMovies",
                outputdir=self.output_dir,
                nimg=4,
            )
        ]

    def add_compatibility_joboptions(self) -> None:
        self.add_common_compatibility_joboptions(jtype="mov")


class RelionJoinStarMicrographs(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MICS_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION join micrograph STAR files"

        self.jobinfo.short_desc = (
            "Join multiple micrograph STAR files into a single file"
        )

        self.joboptions["fn_mics"] = MultiInputNodeJobOption(
            label="Micrograph STAR files:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The the micrograph STAR files to be combined.",
        )
        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_mics"])

    def create_input_nodes(self) -> None:
        """Override to handle Relion-style job files."""
        self.handle_relionstyle_jobfiles("mic", "Micrograph")
        super().create_input_nodes()

    def create_output_nodes(self) -> None:
        self.make_output_nodes("mics")

    def get_commands(self) -> List[PipelinerCommand]:
        command = self.make_join_command(self.joboptions["fn_mics"].get_list(), "mics")
        return [PipelinerCommand(command, relion_control=True)]

    # needs to return an EMPIAR correctedmics obj
    def prepare_deposition_data(self, depo_type: str) -> List[EmpiarCorrectedMics]:
        if depo_type == "EMPIAR":
            mpfile = os.path.join(self.output_dir, "join_mics.star")
            return prepare_empiar_mics(mpfile)
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def gather_metadata(self) -> Dict[str, object]:
        metadata = self.general_metadata_gathering("mics")
        return metadata

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="micrographs",
                column="_rlnMicrographName",
                outputdir=self.output_dir,
            )
        ]

    def add_compatibility_joboptions(self) -> None:
        self.add_common_compatibility_joboptions(jtype="mic")


class RelionJoinStarParticles(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_PARTS_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION join particle STAR files"

        self.jobinfo.short_desc = "Join multiple particle STAR files into a single file"

        self.joboptions["fn_parts"] = MultiInputNodeJobOption(
            label="Particle STAR files:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The particle STAR files to be combined.",
        )

        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_parts"])

    def create_input_nodes(self) -> None:
        """Override to handle Relion-style job files."""
        self.handle_relionstyle_jobfiles("part", "Particle")
        super().create_input_nodes()

    def create_output_nodes(self) -> None:
        self.make_output_nodes("particles")

    def get_commands(self) -> List[PipelinerCommand]:
        command = self.make_join_command(
            self.joboptions["fn_parts"].get_list(), "particles"
        )
        return [PipelinerCommand(command, relion_control=True)]

    # needs to return an EMPIAR Particles obj
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:
        if depo_type == "EMPIAR":
            mpfile = os.path.join(self.output_dir, "join_particles.star")
            return prepare_empiar_parts(mpfile)
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def gather_metadata(self) -> Dict[str, object]:
        metadata = self.general_metadata_gathering("particles")
        return metadata

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="particles",
                column="_rlnImageName",
                outputdir=self.output_dir,
                nimg=20,
            )
        ]

    def add_compatibility_joboptions(self) -> None:
        self.add_common_compatibility_joboptions(jtype="part")
