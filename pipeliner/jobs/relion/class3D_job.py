#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import logging
from math import ceil
from glob import glob
from typing import List, Dict, Any, Sequence, Tuple, Union

from pipeliner.jobs.relion.relion_job import (
    RelionJob,
    relion_program,
    relion5_available,
)
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.node_factory import create_node
from pipeliner.data_structure import (
    CLASS3D_PARTICLE_NAME,
    CLASS3D_HELICAL_NAME,
    CLASS3D_MULTIREF_PARTICLE_NAME,
    CLASS3D_MULTIREF_HELICAL_NAME,
    CLASS3D_DIR,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
    NODE_DENSITYMAPGROUPMETADATA,
    Node,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    SAMPLING,
    EXT_RELION_OPT,
    EXT_MRC_MAP,
    EXT_STARFILE,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    MultiInputNodeJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.utils import truncate_number, get_job_script
from pipeliner.jobs.relion.refinement_common import (
    get_refine_iter_from_opt_filename,
    refinement_cleanup,
    solvmask2_node,
    find_current_opt_name,
)
from pipeliner.job_options import files_exts
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_maps_slice_montage_and_3d_display,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.results_display_objects import ResultsDisplayObject

logger = logging.getLogger(__name__)


class RelionClass3DJob(RelionJob):
    OUT_DIR = CLASS3D_DIR
    CATEGORY_LABEL = "3D Classification"

    def __init__(self) -> None:
        super().__init__()
        self.del_nodes_on_continue = True
        self.jobinfo.programs = [relion_program("relion_refine")]

        self.joboptions["fn_cont"] = InputNodeJobOption(
            label="Continue from here:",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "class3d"],
            default_value="",
            pattern=files_exts("STAR Files", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration from which you want"
                " to continue a previous run. Note that the Output rootname of the"
                " continued run and the rootname of the previous run cannot be the"
                " same. If they are the same, the program will automatically add a"
                " '_ctX' to the output rootname, with X being the iteration from which"
                " one continues the previous run."
            ),
            in_continue=True,
            only_in_continue=True,
        )

        self.joboptions["fn_img"] = InputNodeJobOption(
            label="Input images STAR file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("STAR file", EXT_STARFILE),
            help_text=(
                "A STAR file with all images (and their metadata). \n \n Alternatively,"
                " you may give a Spider/MRC stack of 2D images, but in that case NO"
                " metadata can be included and thus NO CTF correction can be performed,"
                " nor will it be possible to perform noise spectra estimation or"
                " intensity scale corrections in image groups. Therefore, running"
                " RELION with an input stack will in general provide sub-optimal"
                " results and is therefore not recommended!! Use the Preprocessing"
                " procedure to get the input STAR file in a semi-automated manner. Read"
                " the RELION wiki for more information."
            ),
            is_required=True,
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="Reference mask (optional):",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("Image Files", EXT_MRC_MAP),
            help_text=(
                "If no mask is provided, a soft spherical mask based on the particle"
                " diameter will be used.\n\n Otherwise, provide a Spider/mrc map"
                " containing a (soft) mask with the same dimensions as the"
                " reference(s), and values between 0 and 1, with 1 being 100% protein"
                " and 0 being 100% solvent. The reconstructed reference map will be"
                " multiplied by this mask.\n\n In some cases, for example for non-empty"
                " icosahedral viruses, it is also useful to use a second mask. For all"
                " white (value 1) pixels in this second mask the corresponding pixels"
                " in the reconstructed map are set to the average value of these"
                " pixels. Thereby, for example, the higher density inside the virion"
                " may be set to a constant. Note that this second mask should have"
                " one-values inside the virion and zero-values in the capsid and the"
                " solvent areas. To use a second mask, use the additional option"
                " --solvent_mask2, which may given in the Additional arguments line (in"
                " the Running tab)."
            ),
            in_continue=True,
        )

        self.joboptions["ref_correct_greyscale"] = BooleanJobOption(
            label="Ref. map is on absolute greyscale?",
            default_value=False,
            help_text=(
                "Probabilities are calculated based on a Gaussian noise model, which"
                " contains a squared difference term between the reference and the"
                " experimental image. This has a consequence that the reference needs"
                " to be on the same absolute intensity grey-scale as the experimental "
                " images. RELION and XMIPP reconstruct maps at their absolute intensity"
                " grey-scale. Other packages may perform internal normalisations of the"
                " reference density, which will result in incorrect grey-scales."
                " Therefore: if the map was reconstructed in RELION or in XMIPP, set"
                " this option to Yes, otherwise set it to No. If set to No, RELION will"
                " use a (grey-scale invariant) cross-correlation criterion in the first"
                " iteration, and prior to the second iteration the map will be filtered"
                " again using the initial low-pass filter. This procedure is relatively"
                " quick and typically does not negatively affect the outcome of the"
                " subsequent MAP refinement. Therefore, if in doubt it is recommended"
                " to set this option to No."
            ),
        )

        self.joboptions["ini_high"] = FloatJobOption(
            label="Initial low-pass filter (A):",
            default_value=60,
            hard_min=0,
            suggested_max=200,
            step_value=5,
            help_text=(
                "It is recommended to strongly low-pass filter your initial reference"
                " map.If it has not yet been low-pass filtered, it may be done"
                " internally using this option. If set to 0, no low-pass filter will be"
                " applied to the initial reference(s)."
            ),
        )

        self.joboptions["sym_name"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text=(
                "If the molecule is asymmetric, set Symmetry group to C1. Note there"
                " are multiple possibilities for icosahedral symmetry: \n* I1:"
                " No-Crowther 222 (standard in Heymann, Chagoyen & Belnap, JSB, 151"
                " (2005) 196–207) \n * I2: Crowther 222 \n * I3: 52-setting (as used in"
                " SPIDER?)\n * I4: A different 52 setting \n The command 'relion_refine"
                " --sym D2 --print_symmetry_ops' prints a list of all symmetry"
                " operators for symmetry group D2. RELION uses XMIPP's libraries for"
                " symmetry operations. Therefore, look at the XMIPP Wiki for more"
                " details:"
                " http://xmipp.cnb.csic.es/twiki/bin/view/Xmipp/WebHome?topic=Symmetry"
            ),
            validation_regex="(C[1-9][0-9]*)|(D[2-9][0-9]*)|I[243]|O|T",
            regex_error_message="Symmetry must be in Cn, Dn, I[234], O or T format",
            is_required=True,
        )

        self.joboptions["do_ctf_correction"] = BooleanJobOption(
            label="Do CTF-correction?",
            default_value=True,
            help_text=(
                "If set to Yes, CTFs will be corrected inside the MAP refinement. The"
                " resulting algorithm intrinsically implements the optimal linear, or"
                " Wiener filter. Note that CTF parameters for all images need to be"
                " given in the input STAR file. The command 'relion_refine"
                " --print_metadata_labels' will print a list of all possible metadata"
                " labels for that STAR file. See the RELION Wiki for more details.\n\n"
                " Also make sure that the correct pixel size (in Angstrom) is given"
                " above!)"
            ),
        )

        self.joboptions["ctf_intact_first_peak"] = BooleanJobOption(
            label="Ignore CTFs until first peak?",
            default_value=False,
            help_text=(
                "If set to Yes, then CTF-amplitude correction will only be performed"
                " from the first peak of each CTF onward. This can be useful if the CTF"
                " model is inadequate at the lowest resolution. Still, in general using"
                " higher amplitude contrast on the CTFs (e.g. 10-20%) often yields"
                " better results. Therefore, this option is not generally recommended:"
                " try increasing amplitude contrast (in your input STAR file) first!"
            ),
            deactivate_if=JobOptionCondition([("do_ctf_correction", "=", False)]),
        )

        self.joboptions["nr_classes"] = IntJobOption(
            label="Number of classes:",
            default_value=1,
            hard_min=1,
            suggested_max=50,
            step_value=1,
            help_text=(
                "The number of classes (K) for a multi-reference refinement. These"
                " classes will be made in an unsupervised manner from a single"
                " reference by division of the data into random subsets during the"
                " first iteration."
            ),
        )

        self.joboptions["tau_fudge"] = FloatJobOption(
            label="Regularisation parameter T:",
            default_value=4,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Bayes law strictly determines the relative weight between the"
                " contribution of the experimental data and the prior. However, in"
                " practice one may need to adjust this weight to put slightly more"
                " weight on the experimental data to allow optimal results. Values"
                " greater than 1 for this regularisation parameter (T in the JMB2011"
                " paper) put more weight on the experimental data. Values around 2-4"
                " have been observed to be useful for 3D refinements, values of 1-2 for"
                " 2D refinements. Too small values yield too-low resolution structures;"
                " too high values result in over-estimated resolutions, mostly notable"
                " by the apparition of high-frequency noise in the references."
            ),
            in_continue=True,
        )

        self.joboptions["nr_iter"] = IntJobOption(
            label="Number of iterations:",
            default_value=25,
            suggested_min=1,
            hard_min=0,
            suggested_max=50,
            step_value=1,
            help_text=(
                "Number of iterations to be performed. Note that the current"
                " implementation of 2D class averaging and 3D classification does NOT"
                " comprise a convergence criterium. Therefore, the calculations will"
                " need to be stopped by the user if further iterations do not yield"
                " improvements in resolution or classes. \n\n Also note that upon"
                " restarting, the iteration number continues to be increased, starting"
                " from the final iteration in the previous run. The number given here"
                " is the TOTAL number of iterations. For example, if 10 iterations have"
                " been performed previously and one restarts to perform an additional 5"
                " iterations (for example with a finer angular sampling), then the"
                " number given here should be 10+5=15."
            ),
            in_continue=True,
        )

        self.joboptions["do_fast_subsets"] = BooleanJobOption(
            label="Use fast subsets (for large data sets)?",
            default_value=False,
            help_text=(
                "If set to Yes, the first 5 iterations will be done with random "
                "subsets of only K*1500 particles (K being the number of classes); "
                "the next 5 with K*4500 particles, the next 5 with 30% of the data "
                "set; and the final ones with all data. This was inspired by a cisTEM "
                "implementation by Niko Grigorieff et al."
            ),
        )

        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Mask diameter (A):",
            default_value=200,
            hard_min=0,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "The experimental images will be masked with a soft circular mask with"
                " this diameter. Make sure this radius is not set too small because"
                " that may mask away part of the signal! If set to a value larger than"
                " the image size no masking will be performed.\n\n The same diameter"
                " will also be used for a spherical mask of the reference structures if"
                " no user-provided mask is specified."
            ),
            in_continue=True,
        )

        self.joboptions["do_zero_mask"] = BooleanJobOption(
            label="Mask individual particles with zeros?",
            default_value=True,
            help_text=(
                "If set to Yes, then in the individual particles, the area outside a"
                " circle with the radius of the particle will be set to zeros prior to"
                " taking the Fourier transform. This will remove noise and therefore"
                " increase sensitivity in the alignment and classification. However, it"
                " will also introduce correlations between the Fourier components that"
                " are not modelled. When set to No, then the solvent area is filled"
                " with random noise, which prevents introducing correlations."
                " High-resolution refinements (e.g. ribosomes or other large complexes"
                " in 3D auto-refine) tend to work better when filling the solvent area"
                " with random noise (i.e. setting this option to No), refinements of"
                " smaller complexes and most classifications go better when using zeros"
                " (i.e. setting this option to Yes)."
            ),
        )

        self.joboptions["highres_limit"] = FloatJobOption(
            label="Limit resolution E-step to (A):",
            default_value=-1,
            suggested_max=20,
            step_value=1,
            help_text=(
                "If set to a positive number, then the expectation step (i.e. the"
                " alignment) will be done only including the Fourier components up to"
                " this resolution (in Angstroms). This is useful to prevent"
                " overfitting, as the classification runs in RELION are not to be"
                " guaranteed to be 100% overfitting-free (unlike the 3D auto-refine"
                " with its gold-standard FSC). In particular for very difficult data"
                " sets, e.g. of very small or featureless particles, this has been"
                " shown to give much better class averages. In such cases, values in"
                " the range of 7-12 Angstroms have proven useful."
            ),
        )
        self.joboptions["do_blush"] = BooleanJobOption(
            label="Use Blush regularisation?",
            default_value=False,
            help_text=(
                "If set to Yes, relion_refine will use a neural network to perform "
                "regularisation by denoising at every iteration, instead of the "
                "standard smoothness regularisation."
            ),
        )

        self.joboptions["dont_skip_align"] = BooleanJobOption(
            label="Perform image alignment?",
            default_value=True,
            help_text=(
                "If set to No, then rather than performing both alignment and"
                " classification, only classification will be performed. This allows"
                " the use of very focused masks. This requires that the optimal"
                " orientations of all particles are already stored in the input STAR"
                " file."
            ),
            in_continue=True,
        )

        self.joboptions["sampling"] = MultipleChoiceJobOption(
            label="Angular sampling interval:",
            choices=SAMPLING,
            default_value_index=2,
            help_text=(
                "There are only a few discrete angular samplings possible because we"
                " use the HealPix library to generate the sampling of the first two"
                " Euler angles on the sphere. The samplings are approximate numbers and"
                " vary slightly over the sphere.\n\n If auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["offset_range"] = FloatJobOption(
            label="Offset search range (pix):",
            default_value=5,
            hard_min=0,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Probabilities will be calculated only for translations in a circle"
                " with this radius (in pixels). The center of this circle changes at"
                " every iteration and is placed at the optimal translation for each"
                " image in the previous iteration.\n\n If auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["offset_step"] = FloatJobOption(
            label="Offset search step (pix):",
            default_value=1,
            suggested_min=0.1,
            hard_min=0,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Translations will be sampled with this step-size (in pixels)."
                " Translational sampling is also done using the adaptive approach."
                " Therefore, if adaptive=1, the translations will first be evaluated on"
                " a 2x coarser grid.\n\n If auto-sampling is used, this will be the"
                " value for the first iteration(s) only, and the sampling rate will be"
                " increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["do_local_ang_searches"] = BooleanJobOption(
            label="Perform local angular searches?",
            default_value=False,
            help_text=(
                "If set to Yes, then rather than performing exhaustive angular"
                " searches, local searches within the range given below will be"
                " performed. A prior Gaussian distribution centered at the optimal"
                " orientation in the previous iteration and with a stddev of 1/3 of the"
                " range given below will be enforced."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["sigma_angles"] = FloatJobOption(
            label="Local angular search range:",
            default_value=5.0,
            hard_min=0,
            suggested_max=15,
            step_value=0.1,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the optimal orientation in the previous iteration.A"
                " Gaussian prior (also see previous option) will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition(
                [
                    ("dont_skip_align", "=", False),
                    ("do_local_ang_searches", "=", False),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["allow_coarser"] = BooleanJobOption(
            label="Allow coarser sampling?",
            default_value=False,
            help_text=(
                "If set to Yes, the program will use coarser angular and translational"
                " samplings if the estimated accuracies of the assignments is still low"
                " in the earlier iterations. This may speed up the calculations."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.get_comp_options()
        self.get_runtab_options(mpi=True, threads=True, addtl_args=True)

        self.command = []

    def get_multiref_prep_command(self) -> List[str]:
        command = [
            "python3",
            get_job_script("relion/make_class3d_multiref_starfile.py"),
            "--output_dir",
            self.output_dir,
            "--references",
        ]
        command.extend(self.joboptions["fn_ref"].get_list())
        return command

    def get_nr_classes(self) -> int:
        nr_classes_jobop = self.joboptions.get("nr_classes")
        if nr_classes_jobop is not None:
            return int(nr_classes_jobop.get_number())
        else:
            return len(self.joboptions["fn_ref"].get_list())

    def helical_joboptions(self) -> None:

        # Change the node type for input images to ensure helical particles
        assert isinstance(self.joboptions["fn_img"], InputNodeJobOption)
        self.joboptions["fn_img"].node_type = NODE_PARTICLEGROUPMETADATA
        self.joboptions["fn_img"].node_kwds = ["relion", "helical"]

        self.joboptions["helical_tube_inner_diameter"] = FloatJobOption(
            label="Tube diameter - inner (A):",
            default_value=-1,
            help_text=(
                "Inner and outer diameter (in Angstroms) of the reconstructed helix"
                " spanning across Z axis. Set the inner diameter to -1 if"
                " the helix is not hollow in the center. The outer diameter should be"
                " slightly larger than the actual width of helical tubes because it"
                " also decides the shape of 2D particle mask for each segment. If the"
                " psi priors of the extracted segments are not accurate enough due to"
                " high noise level or flexibility of the structure, then set the outer"
                " diameter to a large value."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_tube_outer_diameter"] = FloatJobOption(
            label="Tube diameter - outer (A):",
            default_value=-1,
            hard_min=0,
            help_text=(
                "Inner and outer diameter (in Angstroms) of the reconstructed helix"
                " spanning across Z axis. Set the inner diameter to -1 if"
                " the helix is not hollow in the center. The outer diameter should be"
                " slightly larger than the actual width of helical tubes because it"
                " also decides the shape of 2D particle mask for each segment. If the"
                " psi priors of the extracted segments are not accurate enough due to"
                " high noise level or flexibility of the structure, then set the outer"
                " diameter to a large value."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["range_rot"] = FloatJobOption(
            label="Angular search range - rot (deg):",
            default_value=-1,
            help_text=(
                "Local angular searches will be performed within +/- of the given"
                " amount (in degrees) from the optimal orientation in the previous"
                " iteration. The default negative value means that no local searches"
                " will be performed. A Gaussian prior will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
            jobop_group="Helical processing options",
        )

        self.joboptions["range_tilt"] = FloatJobOption(
            label="Angular search range - tilt (deg):",
            default_value=-1,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the optimal orientation in the previous iteration."
                " A Gaussian prior (also see previous option) will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
            jobop_group="Helical processing options",
        )

        self.joboptions["range_psi"] = FloatJobOption(
            label="Angular search range - psi (deg):",
            default_value=-1,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the optimal orientation in the previous iteration."
                " A Gaussian prior (also see previous option) will be applied, so that"
                " orientations closer to the optimal orientation in the previous"
                " iteration will get higher weights than those further away.\n\nThese"
                " ranges will only be applied to the rot, tilt and psi angles in the"
                " first few iterations (global searches for orientations) in 3D helical"
                " reconstruction. Values of 9 or 15 degrees are commonly used. Higher"
                " values are recommended for more flexible structures and more memory"
                " and computation time will be used. A range of 15 degrees means sigma"
                " = 5 degrees.\n\nThese options will be invalid if you choose to"
                " perform local angular searches or not to perform image alignment on"
                " 'Sampling' tab."
            ),
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
            jobop_group="Helical processing options",
        )

        self.joboptions["do_apply_helical_symmetry"] = BooleanJobOption(
            label="Apply helical symmetry?",
            default_value=True,
            help_text=(
                "If set to Yes, helical symmetry will be applied in every iteration."
                " Set to No if you have just started a project, helical symmetry is"
                " unknown or not yet estimated."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_nr_asu"] = IntJobOption(
            label="Number of unique asymmetrical units:",
            default_value=1,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of unique helical asymmetrical units in each segment box. If"
                " the inter-box distance (set in segment picking step) is 100 Angstroms"
                " and the estimated helical rise is ~20 Angstroms, then set this value"
                " to 100 / 20 = 5 (nearest integer). This integer should not be less"
                " than 1. The correct value is essential in measuring the signal to"
                " noise ratio in helical reconstruction."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_initial"] = FloatJobOption(
            label="Initial helical twist (deg):",
            default_value=0,
            help_text=(
                "Initial helical symmetry. Set helical twist (in degrees) to positive"
                " value if it is a right-handed helix. Helical rise is a positive value"
                " in Angstroms. If local searches of helical symmetry are planned,"
                " initial values of helical twist and rise should be within their"
                " respective ranges."
            ),
            deactivate_if=JobOptionCondition(
                [("do_apply_helical_symmetry", "=", False)]
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_initial"] = FloatJobOption(
            label="Initial helical rise (A):",
            default_value=0,
            hard_min=0,
            help_text=(
                "Initial helical symmetry. Set helical twist (in degrees) to positive"
                " value if it is a right-handed helix. Helical rise is a positive value"
                " in Angstroms. If local searches of helical symmetry are planned,"
                " initial values of helical twist and rise should be within their"
                " respective ranges."
            ),
            deactivate_if=JobOptionCondition(
                [("do_apply_helical_symmetry", "=", False)]
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_z_percentage"] = FloatJobOption(
            label="Central Z length (%):",
            default_value=30.0,
            suggested_min=5.0,
            suggested_max=80.0,
            step_value=1.0,
            hard_min=0,
            help_text=(
                "Reconstructed helix suffers from inaccuracies of orientation searches."
                " The central part of the box contains more reliable information"
                " compared to the top and bottom parts along Z axis, where Fourier"
                " artefacts are also present if the number of helical asymmetrical"
                " units is larger than 1. Therefore, information from the central part"
                " of the box is used for searching and imposing helical symmetry in"
                " real space. Set this value (%) to the central part length along Z"
                " axis divided by the box size. Values around 30% are commonly used."
            ),
            deactivate_if=JobOptionCondition(
                [("do_apply_helical_symmetry", "=", False)]
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["do_local_search_helical_symmetry"] = BooleanJobOption(
            label="Do local searches of symmetry?",
            default_value=False,
            help_text=(
                "If set to Yes, then perform local searches of helical twist and rise"
                " within given ranges."
            ),
            deactivate_if=JobOptionCondition(
                [("do_apply_helical_symmetry", "=", False)]
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_min"] = FloatJobOption(
            label="Helical twist search (deg) - Min:",
            default_value=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_max"] = FloatJobOption(
            label="Helical twist search (deg) - Max:",
            default_value=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_twist_inistep"] = FloatJobOption(
            label="Helical twist search (deg) - Step:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical twist search. Set"
                " helical twist (in degrees) to positive value if it is a right-handed"
                " helix. Generally it is not necessary for the user to provide an"
                " initial step (less than 1 degree, 5~1000 samplings as default). But"
                " it needs to be set manually if the default value does not guarantee"
                " convergence. The program cannot find a reasonable symmetry if the"
                " true helical parameters fall out of the given ranges. Note that the"
                " final reconstruction can still converge if wrong helical and point"
                " group symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_min"] = FloatJobOption(
            label="Helical rise search (A) - Min:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_max"] = FloatJobOption(
            label="Helical rise search (A) - Max:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise_inistep"] = FloatJobOption(
            label="Helical rise search (A) - Step:",
            default_value=0,
            hard_min=0,
            help_text=(
                "Minimum, maximum and initial step for helical rise search. Helical"
                " rise is a positive value in Angstroms. Generally it is not necessary"
                " for the user to provide an initial step (less than 1% the initial"
                " helical rise, 5~1000 samplings as default). But it needs to be set"
                " manually if the default value does not guarantee convergence. The"
                " program cannot find a reasonable symmetry if the true helical"
                " parameters fall out of the given ranges. Note that the final"
                " reconstruction can still converge if wrong helical and point group"
                " symmetry are provided."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_range_distance"] = FloatJobOption(
            label="Range factor of local averaging:",
            default_value=-1.0,
            suggested_min=1.0,
            suggested_max=5.0,
            step_value=0.1,
            hard_min=0,
            help_text=(
                "Local averaging of orientations and translations will be performed"
                " within a range of +/- this value * the box size. Polarities are also"
                " set to be the same for segments coming from the same tube during"
                " local refinement. Values of ~ 2.0 are recommended for flexible"
                " structures such as MAVS-CARD filaments, ParM, MamK, etc. This option"
                " might not improve the reconstructions of helices formed from curled"
                " 2D lattices (TMV and VipA/VipB). Set to negative to disable this"
                " option."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("do_apply_helical_symmetry", "=", False),
                    ("do_local_search_helical_symmetry", "=", False),
                ],
                operation="ANY",
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["keep_tilt_prior_fixed"] = BooleanJobOption(
            label="Keep tilt-prior fixed:",
            default_value=True,
            help_text=(
                "If set to yes, the tilt prior will not change during the optimisation."
                " If set to No, at each iteration the tilt prior will move to the"
                " optimal tilt value for that segment from the previous iteration."
            ),
            jobop_group="Helical processing options",
        )

    def sampling_options(self) -> None:
        iover = 1
        self.command += ["--oversampling", str(iover)]
        sampling_opts = SAMPLING
        sampling_opt = self.joboptions["sampling"].get_string()
        sampling = sampling_opts.index(sampling_opt) + 1
        # The sampling given in the GUI will be the oversampled one!
        self.command += ["--healpix_order", str(sampling - iover)]

        # Manually input local angular searches
        if self.joboptions["do_local_ang_searches"].get_boolean():
            sigma_ang = self.joboptions["sigma_angles"].get_number()
            self.command += ["--sigma_ang", truncate_number((sigma_ang / 3.0), 5)]

        # Offset range
        offset_range = self.joboptions["offset_range"].get_string()
        self.command += ["--offset_range", offset_range]

        # The sampling given in the GUI will be the oversampled one!
        offset_step = self.joboptions["offset_step"].get_number()
        self.command += ["--offset_step", str(offset_step * (2**iover))]

        if self.joboptions["allow_coarser"].get_boolean():
            self.command.append("--allow_coarser_sampling")

    def common_ctf_options(self) -> None:
        if self.joboptions["do_ctf_correction"].get_boolean():
            self.command.append("--ctf")
            if self.joboptions["ctf_intact_first_peak"].get_boolean():
                self.command.append("--ctf_intact_first_peak")

    def common_commands(self, multiref=False) -> None:
        self.command = self.get_relion_command("refine")

        # setup inputs/outputs
        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string()
            pos_it = int(fn_cont.partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for "
                    "continuation run!",
                )
            self.command += ["--continue", fn_cont]

        elif not self.is_continue:
            fn_img = self.joboptions["fn_img"].get_string()
            self.command += ["--i", fn_img]

        self.command += ["--o", os.path.join(self.output_dir, "run")]

        if multiref:
            fn_ref = os.path.join(self.output_dir, "references.star")
        else:
            fn_ref = self.joboptions["fn_ref"].get_string()

        if fn_ref not in ["None", "none"]:
            self.command += ["--ref", fn_ref]
            ref_correct_gs = self.joboptions["ref_correct_greyscale"].get_boolean()

            if not ref_correct_gs:  # dont do firstiter_cc when giving None
                self.command.append("--firstiter_cc")

        ini_high = self.joboptions["ini_high"].get_number()
        if ini_high > 0:
            self.command += ["--ini_high", str(ini_high)]

        # Compute options
        self.add_comp_options()

        # ctf options
        if not self.is_continue:
            self.common_ctf_options()

        self.command += ["--iter", self.joboptions["nr_iter"].get_string()]
        self.command += ["--tau2_fudge", self.joboptions["tau_fudge"].get_string()]
        particle_diameter = self.joboptions["particle_diameter"].get_string()
        self.command += ["--particle_diameter", particle_diameter]

        if not self.is_continue:
            if self.joboptions["do_fast_subsets"].get_boolean():
                self.command.append("--fast_subsets")
            if self.joboptions["do_blush"].get_boolean():
                if not relion5_available():
                    logger.warning(
                        "Blush regularization was requested but cannot be"
                        " performed because RELION 5 was not found"
                    )
                self.command.append("--blush")
            self.command += ["--K", str(self.get_nr_classes())]
            # Always flatten the solvent
            self.command.append("--flatten_solvent")

            if self.joboptions["do_zero_mask"].get_boolean():
                self.command.append("--zero_mask")

            highres_limit = self.joboptions["highres_limit"].get_number()
            if highres_limit > 0:
                self.command += ["--strict_highres_exp", str(highres_limit)]

        fn_mask = self.joboptions["fn_mask"].get_string()
        if len(fn_mask) > 0:
            self.command += ["--solvent_mask", fn_mask]

        if not self.joboptions["dont_skip_align"].get_boolean():
            self.command.append("--skip_align")
        else:
            self.sampling_options()

        # Provide symmetry, and always do norm and scale correction
        sym_name = self.joboptions["sym_name"].get_string()
        if not self.is_continue:
            self.command += ["--sym", sym_name]
            self.command += ["--norm", "--scale"]

    def running_commands(self) -> None:
        self.command += ["--j", self.joboptions["nr_threads"].get_string()]

        # GPU-stuff
        if self.joboptions["use_gpu"].get_boolean():
            gpu_ids = self.joboptions["gpu_ids"].get_string()
            self.command += ["--gpu", gpu_ids]

        # Other arguments
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            solvmask2_node(self)
            self.command += self.parse_additional_args()

    def make_output_nodes(self, helical=False) -> None:
        sym_name = self.joboptions["sym_name"].get_string()
        kwds = ["relion", "class3d"]
        if helical:
            kwds.append("helical")
        if sym_name.lower() != "c1":
            kwds.append(sym_name.lower() + "sym")

        my_iter = int(self.joboptions["nr_iter"].get_number())
        stem = f"run_it{my_iter:03d}"
        self.add_output_node(f"{stem}_optimiser.star", NODE_OPTIMISERDATA, kwds)
        self.add_output_node(f"{stem}_data.star", NODE_PARTICLEGROUPMETADATA, kwds)

        my_classes = self.get_nr_classes()
        for iclass in range(1, my_classes + 1):
            self.add_output_node(f"{stem}_class{iclass:03d}.mrc", NODE_DENSITYMAP, kwds)

    def helical_symsearch_options(self) -> None:
        self.command.append("--helical_symmetry_search")
        twist_min = self.joboptions["helical_twist_min"].get_string()
        self.command += ["--helical_twist_min", twist_min]
        twist_max = self.joboptions["helical_twist_max"].get_string()
        self.command += ["--helical_twist_max", twist_max]
        twist_inistep = self.joboptions["helical_twist_inistep"].get_number()
        if twist_inistep > 0:
            self.command += [
                "--helical_twist_inistep",
                truncate_number(twist_inistep, 2),
            ]
        rise_min = self.joboptions["helical_rise_min"].get_string()
        self.command += ["--helical_rise_min", rise_min]
        rise_max = self.joboptions["helical_rise_max"].get_string()
        self.command += ["--helical_rise_max", rise_max]
        rise_inistep = self.joboptions["helical_rise_inistep"].get_number()
        if rise_inistep > 0:
            self.command += [
                "--helical_rise_inistep",
                truncate_number(rise_inistep, 2),
            ]

    def range_options(self) -> None:
        val = self.joboptions["range_tilt"].get_number()
        val = float(min(max(val, 0), 90))
        self.command += ["--sigma_tilt", truncate_number((val / 3.0), 5)]

        val = self.joboptions["range_psi"].get_number()
        val = float(min(max(val, 0), 90))
        self.command += ["--sigma_psi", truncate_number((val / 3.0), 5)]

        val = self.joboptions["range_rot"].get_number()
        val = float(min(max(val, 0), 90))
        self.command += ["--sigma_rot", truncate_number((val / 3.0), 5)]

        val = self.joboptions["helical_range_distance"].get_number()
        if val > 0:
            self.command += ["--helical_sigma_distance", str(val / 3.0)]

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        return refinement_cleanup(self, do_harsh)

    def gpu_validation(self) -> List[JobOptionValidationResult]:
        if (
            not self.joboptions["dont_skip_align"].get_boolean()
            and self.joboptions["use_gpu"].get_boolean()
        ):
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["dont_skip_align"]],
                    message="Cannot use GPUs when skipping image alignments",
                )
            ]
        return []

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "class3d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["relion", "class3d"]
        )
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_map = fn_root + "class???.mrc"
        fn_maps = glob(fn_map)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = create_node(m, NODE_DENSITYMAP, ["relion", "class3d"])
                out_nodes.append(map_node)

        return out_nodes

    # EMPIAR deposition
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:
        # prepare the EMPIAR object
        if depo_type == "EMPIAR":
            for f in self.output_nodes:
                if f.name.endswith("_data.star"):
                    outfile = f.name
                    return prepare_empiar_parts(outfile)
            raise ValueError(
                f"No _data.star files were found in {self.output_dir}; deposition "
                "cannot be created"
            )

        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def general_metadata_gathering(self, nr_iter) -> Dict[str, object]:

        metadata_dict: Dict[str, object] = {}

        model_file = os.path.join(self.output_dir, f"run_it{nr_iter:03d}_model.star")
        data_file = os.path.join(self.output_dir, f"run_it{nr_iter:03d}_data.star")
        mod = DataStarFile(model_file)
        dat = DataStarFile(data_file)

        nclasses = mod.count_block("model_classes")
        metadata_dict["Classes"] = nclasses

        class_dist = mod.get_block("model_classes").find(["_rlnClassDistribution"])
        metadata_dict["NonEmptyClasses"] = [float(x[0]) > 0 for x in class_dist].count(
            True
        )

        nparts = dat.count_block("particles")
        class_dists = [ceil(float(x[0]) * nparts) for x in class_dist]
        metadata_dict["ClassDistributions"] = class_dists

        resos = mod.get_block("model_classes").find(["_rlnEstimatedResolution"])
        resolist = []
        for x in resos:
            if x[0] != "inf":
                resolist.append(float(x[0]))
            else:
                resolist.append(0.0)
        metadata_dict["ClassResolutions"] = resolist

        imgs = [x[0] for x in dat.get_block("particles").find(["_rlnMicrographName"])]
        metadata_dict["ContributingMicrographs"] = len(set(imgs))

        metadata_dict["ParticleCount"] = nparts

        ctf_data = dat.get_block("particles").find(["_rlnDefocusU", "_rlnDefocusV"])
        ctfs = [(float(x[0]) + float(x[1])) / 2.0 for x in ctf_data]
        metadata_dict["CtfMin"] = min(ctfs)
        metadata_dict["CtfMax"] = max(ctfs)

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        dispobjs = []
        classinfo = {}

        # get the latest iteration's outputs
        current_output_nodes = self.get_current_output_nodes()
        optimiser_node, particles_node = (
            current_output_nodes[0],
            current_output_nodes[1],
        )
        optimiser_name, optimiser_type = optimiser_node.name, optimiser_node.type
        data_file = particles_node.name

        curr_it = get_refine_iter_from_opt_filename(optimiser_name)
        nr_iter = self.joboptions["nr_iter"].get_number()

        # prepare the class distribution graph for
        # all current complete itterations
        model_files = glob(self.output_dir + "*_model.star")
        model_files.sort()
        distros: List[List[float]] = []
        iters: List[List[int]] = []
        for mf in model_files:
            icount = int(os.path.basename(mf).split("_")[1].replace("it", ""))
            cldat = DataStarFile(mf).get_block("model_classes")
            dists = cldat.find(["_rlnClassDistribution"])
            for n, i in enumerate(dists):
                try:
                    distros[n].append(float(i[0]))
                    iters[n].append(icount)
                except IndexError:
                    distros.append([float(i[0])])
                    iters.append([icount])
        class_names = [f"Class {x:03d}" for x in range(1, len(distros) + 1)]

        dispobjs.append(
            create_results_display_object(
                "graph",
                title=f"Class distributions iteration {curr_it}/{nr_iter}",
                xvalues=iters,
                xaxis_label="Iteration",
                yvalues=distros,
                yaxis_label="Particle distribution",
                data_series_labels=class_names,
                associated_data=[
                    os.path.join(self.output_dir, f"run_it{curr_it:03d}_model.star")
                ],
            )
        )

        # prepare the map display objects
        map_files = glob(self.output_dir + f"run_it{curr_it:03d}_class*.mrc")

        # count the particle count
        pcount = DataStarFile(data_file).count_block("particles")
        model_file = data_file.replace("_data", "_model")
        classdata = DataStarFile(model_file).get_block("model_classes")
        cldt = classdata.find(
            [
                "_rlnReferenceImage",
                "_rlnClassDistribution",
                "_rlnEstimatedResolution",
            ]
        )
        for line in cldt:
            classinfo[line[0]] = (pcount * float(line[1]), float(line[2]))

        filt_mf = list(filter(lambda x: classinfo.get(x) is not None, map_files))
        filt_mf.sort(key=lambda x: classinfo[x][1])
        out_maps = {}

        for cl_map in filt_mf:
            clno = int(cl_map.split("_")[2].split(".")[0].strip("class"))
            label = (
                f"class {clno} - {round(classinfo[cl_map][1], 2)} Å - "
                f"{int(classinfo[cl_map][0])} particles"
            )
            out_maps[cl_map] = label

        optimiser_info: Dict[str, str] = {
            "name": optimiser_name,
            "type": optimiser_type,
        }

        dispobjs.extend(
            make_maps_slice_montage_and_3d_display(
                in_maps=out_maps,
                output_dir=self.output_dir,
                base64_output=True,
                optimiser_info=optimiser_info,
            )
        )

        return dispobjs


class RelionClass3D(RelionClass3DJob):
    PROCESS_NAME = CLASS3D_PARTICLE_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION 3D classification (single particle)"

        self.jobinfo.short_desc = "Classification of particles in 3D"
        self.jobinfo.long_desc = (
            "All data sets are heterogeneous! The question is how much you are willing"
            " to tolerate. Relion’s 3D multi-reference refinement procedure provides a"
            " powerful unsupervised 3D classification approach."
        )

        self.joboptions["fn_ref"] = InputNodeJobOption(
            label="Reference map:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("Image Files", EXT_MRC_MAP),
            help_text=(
                "A 3D map in MRC/Spider format. Make sure this map has the same"
                " dimensions and the same pixel size as your input images."
            ),
            is_required=True,
        )

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "nr_iter",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "dont_skip_align",
                "sampling",
                "offset_range",
                "offset_step",
                "do_local_ang_searches",
                "sigma_angles",
                "allow_coarser",
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self) -> None:
        self.make_output_nodes()

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()
        self.running_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def gather_metadata(self) -> Dict[str, object]:
        model_files = glob(os.path.join(self.output_dir, "run_it*_model.star"))
        model_files.sort()
        nr_iter = int(model_files[-1].split("_")[1].replace("it", ""))
        metadata = self.general_metadata_gathering(nr_iter)
        return metadata

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        errs.extend(self.gpu_validation())
        if self.joboptions["do_blush"].get_boolean() and not relion5_available():
            errs.append(
                JobOptionValidationResult(
                    "error",
                    raised_by=[self.joboptions["do_blush"]],
                    message="Blush is only available if RELION 5 is installed",
                )
            )
        return errs


class RelionClass3DMulti(RelionClass3DJob):
    PROCESS_NAME = CLASS3D_MULTIREF_PARTICLE_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = (
            "RELION multi-reference 3D classification (single particle)"
        )

        self.jobinfo.short_desc = (
            "Heterogeneous classification of particles in 3D, using multiple references"
        )
        self.jobinfo.long_desc = (
            "All data sets are heterogeneous! The question is how much you are willing"
            " to tolerate. Relion’s 3D multi-reference refinement procedure provides a"
            " powerful unsupervised 3D classification approach."
        )

        self.joboptions["fn_ref"] = MultiInputNodeJobOption(
            label="Reference maps:",
            default_value="",
            help_text="3D maps in MRC/Spider format.",
            is_required=True,
            node_type=NODE_DENSITYMAP,
        )

        del self.joboptions["nr_classes"]

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "tau_fudge",
                "nr_iter",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "dont_skip_align",
                "sampling",
                "offset_range",
                "offset_step",
                "do_local_ang_searches",
                "sigma_angles",
                "allow_coarser",
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_input_nodes(self) -> None:
        super().create_input_nodes()
        self.input_nodes.append(
            create_node(
                os.path.join(self.output_dir, "references.star"),
                NODE_DENSITYMAPGROUPMETADATA,
            )
        )

    def create_output_nodes(self) -> None:
        self.make_output_nodes()

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands(multiref=True)
        self.running_commands()
        commands = [
            PipelinerCommand(self.get_multiref_prep_command()),
            PipelinerCommand(self.command, relion_control=True),
        ]
        return commands

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        fn_ref = self.joboptions["fn_ref"].get_list()
        if len(fn_ref) < 2 and fn_ref:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["fn_ref"]],
                    message=(
                        "At least 2 references needed for multi ref 3D "
                        "classification"
                    ),
                )
            )
        if self.gpu_validation() is not None:
            errs.extend(self.gpu_validation())

        return errs

    def gather_metadata(self) -> Dict[str, object]:
        nr_iter = int(self.joboptions["nr_iter"].get_number())
        metadata = self.general_metadata_gathering(nr_iter)
        return metadata


class RelionClass3DHelical(RelionClass3DJob):
    PROCESS_NAME = CLASS3D_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION 3D classification (helical)"

        self.jobinfo.short_desc = (
            "Classification of particles in 3D for overlapping helical segments"
        )
        self.jobinfo.long_desc = (
            "3D classificaton taking into account psi-angle priors and limiting shifts"
            " to prevent particles from frame-shifting by moving more than one helical"
            " rise "
        )

        self.joboptions["fn_ref"] = InputNodeJobOption(
            label="Reference map:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("Image Files", EXT_MRC_MAP),
            help_text=(
                "A 3D map in MRC/Spider format. Make sure this map has the same"
                " dimensions and the same pixel size as your input images."
            ),
            is_required=True,
        )

        self.helical_joboptions()

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "nr_iter",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "dont_skip_align",
                "sampling",
                "offset_range",
                "offset_step",
                "do_local_ang_searches",
                "sigma_angles",
                "allow_coarser",
                "helical_tube_inner_diameter",
                "helical_tube_outer_diameter",
                "range_rot",
                "range_tilt",
                "range_psi",
                "do_apply_helical_symmetry",
                "helical_nr_asu",
                "helical_twist_initial",
                "helical_rise_initial",
                "helical_z_percentage",
                "do_local_search_helical_symmetry",
                "helical_twist_min",
                "helical_twist_max",
                "helical_twist_inistep",
                "helical_rise_min",
                "helical_rise_max",
                "helical_rise_inistep",
                "helical_range_distance",
                "keep_tilt_prior_fixed",
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self) -> None:
        self.make_output_nodes(helical=True)

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()

        # helix specific commands
        self.command.append("--helix")
        inner_diam = self.joboptions["helical_tube_inner_diameter"].get_number()
        if inner_diam > 0:
            self.command += [
                "--helical_inner_diameter",
                truncate_number(inner_diam, 2),
            ]
        outer_diam = self.joboptions["helical_tube_outer_diameter"].get_number()
        self.command += ["--helical_outer_diameter", truncate_number(outer_diam, 2)]

        if self.joboptions["do_apply_helical_symmetry"].get_boolean():
            helical_nr_asu = self.joboptions["helical_nr_asu"].get_string()
            self.command += ["--helical_nr_asu", helical_nr_asu]
            ini_twist = self.joboptions["helical_twist_initial"].get_number()
            self.command += ["--helical_twist_initial", truncate_number(ini_twist, 5)]
            ini_rise = self.joboptions["helical_rise_initial"].get_number()
            self.command += ["--helical_rise_initial", truncate_number(ini_rise, 5)]
            myz = self.joboptions["helical_z_percentage"].get_number() / 100.0
            self.command += ["--helical_z_percentage", truncate_number(myz, 2)]

            if self.joboptions["do_local_search_helical_symmetry"].get_boolean():
                self.helical_symsearch_options()
        else:
            self.command.append("--ignore_helical_symmetry")

        if self.joboptions["keep_tilt_prior_fixed"].get_boolean():
            self.command.append("--helical_keep_tilt_prior_fixed")
        dont_skip_align = self.joboptions["dont_skip_align"].get_boolean()
        do_loc_ang_search = self.joboptions["do_local_ang_searches"].get_boolean()
        if dont_skip_align and not do_loc_ang_search:
            self.range_options()

        self.running_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "class3d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["class3d", "helical"]
        )
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_map = fn_root + "class???.mrc"
        fn_maps = glob(fn_map)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = create_node(m, NODE_DENSITYMAP, ["relion", "class3d"])
                out_nodes.append(map_node)

        return out_nodes

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        return self.gpu_validation()

    def gather_metadata(self) -> Dict[str, object]:

        nr_iter = int(self.joboptions["nr_iter"].get_number())
        metadata = self.general_metadata_gathering(nr_iter)
        return metadata


class RelionClass3DHelicalMulti(RelionClass3DJob):
    PROCESS_NAME = CLASS3D_MULTIREF_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION multi-reference 3D classification (helical)"

        self.jobinfo.short_desc = (
            "Heterogeneous classification of overlapping helical segments in 3D,"
            " using multiple references"
        )

        self.jobinfo.long_desc = (
            "3D classification taking into account psi-angle priors and limiting shifts"
            " to prevent particles from frame-shifting by moving more than one helical"
            " rise "
        )

        self.joboptions["fn_ref"] = MultiInputNodeJobOption(
            label="Reference maps:",
            default_value="",
            help_text="3D maps in MRC/Spider format.",
            is_required=True,
            node_type=NODE_DENSITYMAP,
        )

        self.helical_joboptions()
        del self.joboptions["nr_classes"]

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "tau_fudge",
                "nr_iter",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "dont_skip_align",
                "sampling",
                "offset_range",
                "offset_step",
                "do_local_ang_searches",
                "sigma_angles",
                "allow_coarser",
                "helical_tube_inner_diameter",
                "helical_tube_outer_diameter",
                "range_rot",
                "range_tilt",
                "range_psi",
                "do_apply_helical_symmetry",
                "helical_nr_asu",
                "helical_twist_initial",
                "helical_rise_initial",
                "helical_z_percentage",
                "do_local_search_helical_symmetry",
                "helical_twist_min",
                "helical_twist_max",
                "helical_twist_inistep",
                "helical_rise_min",
                "helical_rise_max",
                "helical_rise_inistep",
                "helical_range_distance",
                "keep_tilt_prior_fixed",
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_input_nodes(self) -> None:
        super().create_input_nodes()
        self.input_nodes.append(
            create_node(
                os.path.join(self.output_dir, "references.star"),
                NODE_DENSITYMAPGROUPMETADATA,
            )
        )

    def create_output_nodes(self) -> None:
        self.make_output_nodes(helical=True)

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands(multiref=True)

        # helix specific commands
        self.command.append("--helix")
        inner_diam = self.joboptions["helical_tube_inner_diameter"].get_number()
        if inner_diam > 0:
            self.command += [
                "--helical_inner_diameter",
                truncate_number(inner_diam, 2),
            ]
        outer_diam = self.joboptions["helical_tube_outer_diameter"].get_number()
        self.command += ["--helical_outer_diameter", truncate_number(outer_diam, 2)]

        if self.joboptions["do_apply_helical_symmetry"].get_boolean():
            helical_nr_asu = self.joboptions["helical_nr_asu"].get_string()
            self.command += ["--helical_nr_asu", helical_nr_asu]
            ini_twist = self.joboptions["helical_twist_initial"].get_number()
            self.command += ["--helical_twist_initial", truncate_number(ini_twist, 5)]
            ini_rise = self.joboptions["helical_rise_initial"].get_number()
            self.command += ["--helical_rise_initial", truncate_number(ini_rise, 5)]
            myz = self.joboptions["helical_z_percentage"].get_number() / 100.0
            self.command += ["--helical_z_percentage", truncate_number(myz, 2)]

            if self.joboptions["do_local_search_helical_symmetry"].get_boolean():
                self.helical_symsearch_options()
        else:
            self.command.append("--ignore_helical_symmetry")

        if self.joboptions["keep_tilt_prior_fixed"].get_boolean():
            self.command.append("--helical_keep_tilt_prior_fixed")
        dont_skip_align = self.joboptions["dont_skip_align"].get_boolean()
        do_loc_ang_search = self.joboptions["do_local_ang_searches"].get_boolean()
        if dont_skip_align and not do_loc_ang_search:
            self.range_options()

        self.running_commands()
        commands = [
            PipelinerCommand(self.get_multiref_prep_command()),
            PipelinerCommand(self.command, relion_control=True),
        ]
        return commands

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "class3d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["relion", "class3d", "helical"]
        )
        out_nodes = [opt_node, data_node]

        fn_root = current_opt.replace("optimiser.star", "")
        fn_map = fn_root + "class???.mrc"
        fn_maps = glob(fn_map)
        if len(fn_maps) > 0:
            for m in fn_maps:
                map_node = create_node(m, NODE_DENSITYMAP, ["relion", "class3d"])
                out_nodes.append(map_node)

        return out_nodes

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        fn_ref = self.joboptions["fn_ref"].get_list()
        if len(fn_ref) < 2 and fn_ref:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["fn_ref"]],
                    message=(
                        "At least 2 references needed for multi ref 3D "
                        "classification"
                    ),
                )
            )
        if self.gpu_validation() is not None:
            errs.extend(self.gpu_validation())

        return errs

    def gather_metadata(self) -> Dict[str, Any]:
        nr_iter = int(self.joboptions["nr_iter"].get_number())
        metadata = self.general_metadata_gathering(nr_iter)
        return metadata
