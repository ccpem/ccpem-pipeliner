#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import logging
from glob import glob
from typing import List, Tuple
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.nodes import NODE_MASK3D

logger = logging.getLogger(__name__)


def solvmask2_node(job: PipelinerJob) -> None:
    """Add the input node for a secondary solvent mask if one is used

    job (Process): The process object of the job to operate on
    """
    splitargs = job.joboptions["other_args"].get_string().split()
    if "--solvent_mask2" in splitargs:
        solvmask2 = splitargs[splitargs.index("--solvent_mask2") + 1]
        job.input_nodes.append(create_node(solvmask2, NODE_MASK3D))


def refinement_cleanup(
    job: PipelinerJob, do_harsh: bool
) -> Tuple[List[str], List[str]]:
    """Return list of intermediate files/dirs to remove
    Args:
        job (Process): The process object of the job to operate on
        do_harsh (bool): Should a harsh cleaning be performed

    Returns:
        Tuple: ([Files that were deleted], [Dirs that were deleted])
    """

    del_files: List[str] = []
    del_dirs: List[str] = []

    # get all the data files
    all_datafiles = glob(job.output_dir + "run_it*_data.star")
    all_datafiles.sort()
    for df in all_datafiles[:-1]:
        del_files += glob(df.replace("_data.star", "*"))

    # Also clean up maps for PCA movies when doing harsh cleaning
    if job.PROCESS_NAME == "relion.multibody.refine" and do_harsh:
        del_files += glob(job.output_dir + "analyse_component*_bin*.mrc")

    del_files_set = set(del_files)
    del_dirs_set = set(del_dirs)

    # remove any files or dirs associated with the output nodes
    if "refine3d" in job.PROCESS_NAME:
        # special case if iterations have been manually designated as outnodes
        fns = []
        for outnode in [x.name for x in job.output_nodes]:
            if "run_it" in outnode:
                root = os.path.join(
                    job.output_dir, "_".join(os.path.basename(outnode).split("_")[:2])
                )
                fns.append(root)
    else:
        fns = ["_".join(x.name.split("_")[:-1]) for x in job.output_nodes]

    keepfiles = set()
    for fn in fns:
        for f in glob(fn + "*"):
            keepfiles.add(f)

    # remove these files from the delete list
    for f in keepfiles:
        del_files_set.discard(f)
        fildir = os.path.dirname(f)
        del_dirs_set.discard(fildir)

    return list(del_files_set), list(del_dirs_set)


def find_current_opt_name(dirname: str) -> str:
    """Get the name of the current optimiser file for a job

    Used for figuring out the last completed iteration when a job
    has been failed or aborted

    Args:
        dirname (str): The job's name

    Returns:
        str: The relative path of the optimiser file

    """
    fn = dirname + "run_it???_optimiser.star"
    fn_opts = glob(fn)

    if len(fn_opts) != 0:
        fn_opts.sort()
        fn_opt = fn_opts[-1]
        return fn_opt
    else:
        logger.error(
            f"Attempted to get current iteration from {dirname} but the job has"
            " not started yet"
        )
        return ""


def get_refine_iter_from_opt_filename(optimiser_name: str) -> int:
    """Get the refinement iteration number from an optimiser file name

     Args:
        optimiser_name (str): The path to the optimiser file

    Returns:
        int: The iteration number
    """
    if optimiser_name == "":
        return 0
    iter_name = os.path.basename(optimiser_name).split("_")[1]
    iter_no = int(iter_name.replace("it", ""))
    return iter_no


def find_current_refine_iter(dirname: str) -> int:
    """Get the current refinement iteration

     Args:
        dirname (str): The job's name

    Returns:
        int: The current iteration
    """
    opt = find_current_opt_name(dirname)
    iter_no = get_refine_iter_from_opt_filename(opt)
    return iter_no
