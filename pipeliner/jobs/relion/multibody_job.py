#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Tuple, Sequence, Union
from pathlib import Path
import logging

from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.jobs.relion.relion_job import (
    relion_program,
    RelionJob,
    relion5_available,
)
from pipeliner.data_structure import (
    MULTIBODY_DIR,
    MULTIBODY_REFINE_NAME,
    MULTIBODY_FLEXANALYSIS_NAME,
)
from pipeliner.nodes import (
    NODE_RIGIDBODIES,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
)
from pipeliner.utils import truncate_number
from pipeliner.starfile_handler import BodyFile
from pipeliner.jobs.relion.refinement_common import (
    refinement_cleanup,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.job_options import (
    InputNodeJobOption,
    SAMPLING,
    files_exts,
    EXT_STARFILE,
    EXT_RELION_OPT,
    EXT_RELION_MODEL,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
)

logger = logging.getLogger(__name__)

additional_refs = [
    Ref(
        authors=["Nakane T", "Kimanius D", "Lindahl E", "Scheres SH"],
        title=(
            "Characterisation of molecular motions in cryo-EM single-particle data by"
            " multi-body refinement in RELION"
        ),
        journal="eLife",
        year="2018",
        volume="7",
        pages="e36861",
        doi="10.7554/eLife.36861",
    ),
    Ref(
        authors=["Scheres SH"],
        title="Processing of Structurally Heterogeneous Cryo-EM Data in RELION",
        journal="Methods Enzymol.",
        year="2016",
        volume="579",
        pages="125-57",
        doi="10.1016/bs.mie.2016.04.012",
        editor="Crowther RA",
    ),
    Ref(
        authors=["Nakene T", "Scheres SH"],
        title="Multi-body Refinement of Cryo-EM Images in RELION",
        journal="Methods in Molecular Biology",
        year="2021",
        volume="2215",
        pages="145-160",
        doi="doi.org/10.1007/978-1-0716-0966-8_7",
    ),
]


class RelionMultiBodyRefine(RelionJob):
    PROCESS_NAME = MULTIBODY_REFINE_NAME
    OUT_DIR = MULTIBODY_DIR
    CATEGORY_LABEL = "Multi-body Refinement"

    def __init__(self) -> None:
        super().__init__()
        self.del_nodes_on_continue = True
        self.jobinfo.programs = [relion_program("relion_refine")]
        self.jobinfo.display_name = "RELION multi-body refinement"

        self.jobinfo.short_desc = "3D refinement with multiple independent rigid bodies"
        self.jobinfo.long_desc = (
            "Traditional image processing approaches often lead to blurred"
            " reconstructions when molecules adopt many different conformations. By"
            " considering complexes to be comprised of multiple, independently moving"
            " rigid bodies, multi-body refinement in RELION enables structure"
            " determination of highly flexible complexes, while at the same time"
            " providing a characterization of the motions in the complex. This method"
            " can be applied to any cryo-EM data set of flexible complexes that can be"
            " divided into two or more bodies, each with a minimum molecular weight of"
            " 100–150 kDa."
        )

        self.jobinfo.references.extend(additional_refs)

        self.joboptions["fn_in"] = InputNodeJobOption(
            label="Consensus refinement optimiser.star:",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "refine3d"],
            default_value="",
            pattern=files_exts("Optimiser STAR file", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration of the consensus"
                " refinement from which you want to start multi-body refinement."
            ),
            is_required=True,
        )

        self.joboptions["fn_cont"] = InputNodeJobOption(
            label="Continue from here:",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "multibody"],
            default_value="",
            pattern=files_exts("Optimiser STAR file", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration from which you want"
                " to continue this multi-body refinement. Note that the Output rootname"
                " of the continued run and the rootname of the previous run cannot be"
                " the same. If they are the same, the program will automatically add a"
                " '_ctX'to the output rootname, with X being the iteration from which"
                " one continues the previous run."
            ),
            in_continue=True,
            only_in_continue=True,
        )

        self.joboptions["fn_bodies"] = InputNodeJobOption(
            label="Body STAR file:",
            default_value="",
            pattern=files_exts("body STAR file", EXT_STARFILE),
            node_type=NODE_RIGIDBODIES,
            node_kwds=["relion", "body_definitions"],
            help_text=(
                "Provide the STAR file with all information about the bodies to be used"
                " in multi-body refinement. An example for a three-body refinement"
                " would look like this:"
                " \n\ndata_\nloop_\n_rlnBodyMaskName\n_rlnBodyRotateRelativeTo"
                " \n_rlnBodySigmaAngles\n_rlnBodySigmaOffset\nlarge_body_mask.mrc 2 10"
                " 2\nsmall_body_mask.mrc 1 10 2\nhead_body_mask.mrc 2 10 2\n\n Where"
                " each data line represents a different body, and: \n- rlnBodyMaskName"
                " contains the name of a soft-edged mask with values in [0,1] that"
                " define the body; \n - rlnBodyRotateRelativeTo defines relative to"
                " which other body this body rotates (first body is number 1); \n-"
                " rlnBodySigmaAngles and _rlnBodySigmaOffset are the standard"
                " deviations (widths) of Gaussian priors on the consensus rotations and"
                " translations; \n\n Optionally, there can be a fifth column with"
                " _rlnBodyReferenceName. Entries can be 'None' (without the ''s) or the"
                " name of a MRC map with an initial reference for that body. In case"
                " the entry is None, the reference will be taken from the density in"
                " the consensus refinement.\n\n Also note that larger bodies should be"
                " above smaller bodies in the STAR file. For more information, see the"
                " multi-body paper."
            ),
            is_required=True,
        )

        self.joboptions["do_subtracted_bodies"] = BooleanJobOption(
            label="Reconstruct subtracted bodies?",
            default_value=True,
            help_text=(
                "If set to Yes, then the reconstruction of each of the bodies willuse"
                " the subtracted images. This may give useful insights about how well"
                " the subtraction worked. If set to No, the original particles are used"
                " for reconstruction (while the subtracted ones are still used for"
                " alignment). This will result in fuzzy densities for bodies outside"
                " the one used for refinement."
            ),
        )
        self.joboptions["do_blush"] = BooleanJobOption(
            label="Use Blush regularisation?",
            default_value=False,
            help_text=(
                "If set to Yes, relion_refine will use a neural network to perform "
                "regularisation by denoising at every iteration, instead of the "
                "standard smoothness regularisation."
            ),
        )

        self.joboptions["sampling"] = MultipleChoiceJobOption(
            label="Initial angular sampling:",
            choices=SAMPLING,
            default_value_index=4,
            help_text=(
                "There are only a few discrete angular samplings possible because"
                " we use the HealPix library to generate the sampling of the first"
                " two Euler angles on the sphere. The samplings are approximate numbers"
                " and vary slightly over the sphere.\n\n Note that this will only be"
                " the value for the first few iteration(s): the sampling rate will be"
                " increased automatically after that."
            ),
            is_required=True,
        )

        self.joboptions["offset_range"] = FloatJobOption(
            label="Initial offset range (pix):",
            default_value=3,
            suggested_min=0,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Probabilities will be calculated only for translations in a circle"
                " with this radius (in pixels). The center of this circle changes at"
                " every iteration and is placed at the optimal translation for each"
                " image in the previous iteration.\n\n Note that this will only be the"
                " value for the first few iteration(s): the sampling rate will be"
                " increased automatically after that."
            ),
            is_required=True,
        )

        self.joboptions["offset_step"] = FloatJobOption(
            label="Initial offset step (pix):",
            default_value=0.75,
            suggested_min=0.1,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Translations will be sampled with this step-size (in pixels)."
                " Translational sampling is also done using the adaptive approach."
                " Therefore, if adaptive=1, the translations will first be evaluated on"
                " a 2x coarser grid.\n\n Note that this will only be the value for the"
                " first few iteration(s): the sampling rate will be increased"
                " automatically after that."
            ),
            is_required=True,
        )

        self.get_comp_options()
        self.get_runtab_options(mpi=True, threads=True, addtl_args=True)

    def create_output_nodes(self) -> None:
        fn_bodies = self.joboptions["fn_bodies"].get_string()
        bodyfile = BodyFile(fn_bodies)
        nr_bodies = bodyfile.bodycount

        if nr_bodies > 1:
            for ibody in range(1, nr_bodies + 1):
                self.add_output_node(
                    f"run_half1_body{ibody:03d}_unfil.mrc",
                    NODE_DENSITYMAP,
                    ["relion", "halfmap", "multibody"],
                )

    def create_post_run_output_nodes(self):
        optfiles = sorted(list(Path(self.output_dir).glob("*optimiser.star")))
        self.add_output_node(
            Path(optfiles[-1]).name,
            NODE_OPTIMISERDATA,
            ["relion", "multibody"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        self.command = self.get_relion_command("refine")

        fn_bodies = self.joboptions["fn_bodies"].get_string()
        fn_run = self.output_dir + "run"

        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string()
            pos_it = int(fn_cont.partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for "
                    "continuation run!",
                )
            self.command += ["--continue", fn_cont]
            self.command += ["--o", fn_run]

        else:
            fn_in = self.joboptions["fn_in"].get_string()
            self.command += [
                "--continue",
                fn_in,
                "--o",
                fn_run,
                "--solvent_correct_fsc",
                "--multibody_masks",
                fn_bodies,
            ]

            if self.joboptions["do_blush"].get_boolean():
                if not relion5_available():
                    logger.warning(
                        "Blush regularization was requested but cannot be performed"
                        " because RELION 5 was not found"
                    )
                self.command.append("--blush")

            iover = 1
            self.command += ["--oversampling", str(iover)]
            sampling_opts = SAMPLING
            sampling_opt = self.joboptions["sampling"].get_string()
            sampling = sampling_opts.index(sampling_opt) + 1
            offset_range = self.joboptions["offset_range"].get_string()
            offset_step = self.joboptions["offset_step"].get_number()
            offset_step = offset_step * (2**iover)
            self.command += [
                "--healpix_order",
                str(sampling - iover),
                "--auto_local_healpix_order",
                str(sampling - iover),
                "--offset_range",
                offset_range,
                "--offset_step",
                truncate_number(offset_step, 2),
            ]

            if self.joboptions["do_subtracted_bodies"].get_boolean():
                self.command.append("--reconstruct_subtracted_bodies")

            # Running stuff
            self.command += ["--j", self.joboptions["nr_threads"].get_string()]

            # GPU-stuff
            if self.joboptions["use_gpu"].get_boolean():
                ngpus = self.joboptions["gpu_ids"].get_string()
                self.command += ["--gpu", ngpus]

            self.add_comp_options()
            # Other arguments
            other_args = self.joboptions["other_args"].get_string()
            if len(other_args) > 0:
                self.command += self.parse_additional_args()

        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        return refinement_cleanup(self, do_harsh)

    # needs to return EMPIAR particles object and OneDep Final Rec object
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:
        if depo_type == "EMPIAR":
            mpfile = os.path.join(self.output_dir, "run_data.star")
            return prepare_empiar_parts(mpfile)
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )


class RelionMultiBodyFlexAnalysis(RelionJob):
    PROCESS_NAME = MULTIBODY_FLEXANALYSIS_NAME
    OUT_DIR = MULTIBODY_DIR
    CATEGORY_LABEL = "Multi-body Refinement"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_flex_analyse")]
        self.jobinfo.display_name = "RELION flexibility analysis"

        self.do_status_check = False  # TO DO: CHECK THAT THIS IS ACTUALLY TRUE
        # Does flex analyse write its own status files?

        self.jobinfo.short_desc = (
            "Perform flexibility analysis on RELION multi-body refinement results"
        )
        self.jobinfo.long_desc = (
            "The relion_flex_analyse program performs a principal component analysis on"
            " the relative orientations of the bodies of all particle images in the"
            " data set. This generates N combined maps for the entire complex with"
            " different relative orientations of the bodies, each corresponding to the"
            " median orientations for 1/Nth of the particle images in the data set."
            " These maps can then be used to generate a movie that visualises the"
            " motion along that eigenvector."
        )

        self.jobinfo.references.extend(additional_refs)

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model file",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "multibody"],
            default_value="",
            pattern=files_exts("Relion Multibody model file", EXT_RELION_MODEL),
            help_text=(
                "A run_model.star file generated by a Relion multibody refinement job"
            ),
            is_required=True,
        )

        self.joboptions["fn_bodies"] = InputNodeJobOption(
            label="Body STAR file:",
            default_value="",
            pattern=files_exts("body STAR file", EXT_STARFILE),
            node_type=NODE_RIGIDBODIES,
            node_kwds=["relion", "body_definitions"],
            help_text=(
                "Provide the STAR file with all information about the bodies to be used"
                " in multi-body refinement. An example for a three-body refinement"
                " would look like this:"
                " \n\ndata_\nloop_\n_rlnBodyMaskName\n_rlnBodyRotateRelativeTo"
                " \n_rlnBodySigmaAngles\n_rlnBodySigmaOffset\nlarge_body_mask.mrc 2 10"
                " 2\nsmall_body_mask.mrc 1 10 2\nhead_body_mask.mrc 2 10 2\n\n Where"
                " each data line represents a different body, and: \n- rlnBodyMaskName"
                " contains the name of a soft-edged mask with values in [0,1] that"
                " define the body; \n - rlnBodyRotateRelativeTo defines relative to"
                " which other body this body rotates (first body is number 1); \n-"
                " rlnBodySigmaAngles and _rlnBodySigmaOffset are the standard"
                " deviations (widths) of Gaussian priors on the consensus rotations and"
                " translations; \n\n Optionally, there can be a fifth column with"
                " _rlnBodyReferenceName. Entries can be 'None' (without the ''s) or the"
                " name of a MRC map with an initial reference for that body. In case"
                " the entry is None, the reference will be taken from the density in"
                " the consensus refinement.\n\n Also note that larger bodies should be"
                " above smaller bodies in the STAR file. For more information, see the"
                " multi-body paper."
            ),
            is_required=True,
        )

        self.joboptions["nr_movies"] = IntJobOption(
            label="Number of eigenvector movies:",
            default_value=3,
            hard_min=0,
            suggested_min=1,
            suggested_max=16,
            step_value=1,
            help_text=(
                "Series of ten output maps will be generated along this many"
                " eigenvectors. These maps can be opened as a 'Volume Series' in UCSF"
                " Chimera, and then displayed as a movie. They represent the principal"
                " motions in the particles."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["do_select"] = BooleanJobOption(
            label="Select particles based on eigenvalues?",
            default_value=False,
            help_text=(
                "If set to Yes, a particles.star file is written out with all particles"
                " that have the below indicated eigenvalue in the selected range."
            ),
            in_continue=True,
        )

        self.joboptions["select_eigenval"] = IntJobOption(
            label="Select on eigenvalue:",
            default_value=1,
            hard_min=1,
            suggested_max=20,
            step_value=1,
            help_text=(
                "This is the number of the eigenvalue to be used in the particle subset"
                " selection (start counting at 1)."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_select", "=", False)]),
        )

        self.joboptions["eigenval_min"] = FloatJobOption(
            label="Minimum eigenvalue:",
            default_value=-999.0,
            suggested_min=-50,
            suggested_max=50,
            hard_min=-9998,
            step_value=1,
            help_text=(
                "This is the minimum value for the selected eigenvalue; only particles"
                " with the selected eigenvalue larger than this value will be included"
                " in the output particles.star file"
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_select", "=", False)]),
        )

        self.joboptions["eigenval_max"] = FloatJobOption(
            label="Maximum eigenvalue:",
            default_value=999.0,
            suggested_min=-50,
            suggested_max=50,
            hard_max=9998,
            step_value=1,
            help_text=(
                "This is the maximum value for the selected eigenvalue; only particles"
                " with the selected eigenvalue less than this value will be included in"
                " the output particles.star file"
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_select", "=", False)]),
        )

        self.get_runtab_options(mpi=False, threads=False)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        eigenval_min = self.joboptions["eigenval_min"].get_number()
        eigenval_max = self.joboptions["eigenval_max"].get_number()
        if eigenval_min >= eigenval_max:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["eigenval_min"],
                        self.joboptions["eigenval_max"],
                    ],
                    message=(
                        "The maximum eigenvalue should be larger than the minimum"
                    ),
                )
            )

            if self.joboptions["do_blush"].get_boolean() and not relion5_available():
                errs.append(
                    JobOptionValidationResult(
                        "error",
                        raised_by=[self.joboptions["do_blush"]],
                        message="Blush is only available if RELION 5 is installed",
                    )
                )

        return errs

    def create_output_nodes(self) -> None:
        if self.joboptions["do_select"].get_boolean():
            select_eigenval = self.joboptions["select_eigenval"].get_string()
            eigenval_min = self.joboptions["eigenval_min"].get_number()
            eigenval_max = self.joboptions["eigenval_max"].get_number()

            # Add output node: selected particles star file
            fnt = "analyse_eval" + select_eigenval + "_select"
            # added a catch if min/and or max are between 1 and -1
            if 1 > eigenval_min > -1:
                evmin = str(round(eigenval_min, 2)).replace(".", "p")
            else:
                evmin = str(int(round(eigenval_min, 0)))

            if 1 > eigenval_max > -1:
                evmax = str(round(eigenval_max, 2)).replace(".", "p")
            else:
                evmax = str(int(round(eigenval_max, 0)))
            # should it throw an error if the values are outside this range?
            if evmin == evmax:
                evmin = str(round(eigenval_min, 2)).replace(".", "p")
                evmax = str(round(eigenval_max, 2)).replace(".", "p")

            fnt += f"_min{evmin}_max{evmax}.star"
            self.add_output_node(
                fnt,
                NODE_PARTICLEGROUPMETADATA,
                ["relion", "flexanalysis", "eigenselected"],
            )
        self.add_output_node(
            "analyse_logfile.pdf", NODE_LOGFILE, ["relion", "flexanalysis"]
        )

    def get_commands(self) -> List[PipelinerCommand]:

        self.command = ["relion_flex_analyse"]

        # if the refinement wasn't done before we need to find a model.star
        # file which does not have a _it specifier
        fn_run = self.joboptions["input_model"].get_string()

        self.command += [
            "--PCA_orient",
            "--model",
            fn_run,
            "--data",
            fn_run.replace("model.star", "data.star"),
        ]

        fn_bodies = self.joboptions["fn_bodies"].get_string()
        self.command += ["--bodies", fn_bodies, "--o", self.output_dir + "analyse"]

        if self.joboptions["nr_movies"].get_number() > 0:
            self.command += [
                "--do_maps",
                "--k",
                self.joboptions["nr_movies"].get_string(),
            ]

        if self.joboptions["do_select"].get_boolean():
            select_eigenval = self.joboptions["select_eigenval"].get_string()
            eigenval_min = self.joboptions["eigenval_min"].get_number()
            eigenval_max = self.joboptions["eigenval_max"].get_number()

            self.command += [
                "--select_eigenvalue",
                select_eigenval,
                "--select_eigenvalue_min",
                str(eigenval_min),
                "--select_eigenvalue_max",
                str(eigenval_max),
            ]
            # add writing of pca projections so they can be examined later
            self.command.append("--write_pca_projections")

        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """There is no cleanup for this job"""
        return [], []

    # To DO: needs to return an EMPIAR particles object if particles were
    # selected

    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:

        if depo_type == "EMPIAR":
            pfile = None
            for f in self.output_nodes:
                if f.toplevel_type == NODE_PARTICLEGROUPMETADATA:
                    pfile = f.name
                    break
            if pfile is not None:
                return prepare_empiar_parts(pfile)
            else:
                return []
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )
