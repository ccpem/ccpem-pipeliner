#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Dict, Sequence, Union
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.display_tools import mini_montage_from_stack
from pipeliner.starfile_handler import DataStarFile
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_EULERANGLES,
    NODE_IMAGE2DSTACK,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class ReprojectJob(RelionJob):
    PROCESS_NAME = "relion.reproject"
    OUT_DIR = "Reproject"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Reproject"

        self.jobinfo.short_desc = "Generate 2D projections from a 3D map"
        self.jobinfo.long_desc = (
            "Just a reminder: DO NOT use high resolution projections as autopicking"
            " references"
        )
        self.jobinfo.programs = [relion_program("relion_project")]

        self.joboptions["map_filename"] = InputNodeJobOption(
            label="Input map mrc file:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("mrc map files", [".mrc"]),
            help_text="The map that will be used for re-projections",
            is_required=True,
        )

        self.joboptions["angles_filename"] = InputNodeJobOption(
            label="Angles file:",
            node_type=NODE_EULERANGLES,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("angles star file", [".star"]),
            help_text=(
                "This should be a star file with a data_angles or data_particles block"
                " containing the following columns: rlnAngleRot, _rlnAngleTilt, and"
                " _rlnAnglePsi"
            ),
            deactivate_if=JobOptionCondition([("nr_uniform", ">", 0)]),
        )

        self.joboptions["nr_uniform"] = IntJobOption(
            label="OR: Do this many uniform projections:",
            default_value=-1,
            hard_min=0,
            suggested_max=180,
            suggested_min=1,
            step_value=1,
            help_text="Alternately do this many uniform projections",
            in_continue=True,
            deactivate_if=JobOptionCondition([("angles_filename", "!=", "")]),
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Reference pixel size (A)",
            default_value=1.07,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.01,
            help_text="The pixel size of the reference in Angstroms",
        )

        self.get_runtab_options(mpi=False, threads=False, addtl_args=True)

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "reproj.mrcs", NODE_IMAGE2DSTACK, ["relion", "projections"]
        )
        if not self.joboptions["angles_filename"].get_string():
            self.add_output_node("reproj.star", NODE_EULERANGLES, ["relion"])

    def get_commands(self) -> List[PipelinerCommand]:

        command = ["relion_project"]

        map_file = self.joboptions["map_filename"].get_string()
        command += ["--i", map_file]

        nr_uniform = self.joboptions["nr_uniform"].get_number()
        angles_file = self.joboptions["angles_filename"].get_string()
        if angles_file != "":
            command += ["--ang_simulate", angles_file]
        elif nr_uniform > 0:
            command += ["--nr_uniform", str(nr_uniform)]
        else:
            raise ValueError(
                "ERROR: An angles file or a number of uniform projections must be"
                " specified"
            )

        apix = self.joboptions["apix"].get_string()
        command += ["--angpix", apix]

        # if the job is being run from a file --o should specify the actual output file
        # name, if using -nr_uniform it should be just a root name for the output files
        output = str(Path(self.output_dir) / "reproj")
        if self.joboptions["angles_filename"].get_string():
            output += ".mrcs"
        command += ["--o", output]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        commands = [PipelinerCommand(command)]
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:

        if self.joboptions["angles_filename"].get_string():
            rp_fn = self.joboptions["angles_filename"].get_string()
        else:
            rp_fn = os.path.join(self.output_dir, "reproj.star")

        rp_file = DataStarFile(rp_fn)
        try:
            phi = rp_file.column_as_list("particles", "_rlnAngleRot")
            theta = rp_file.column_as_list("particles", "_rlnAngleTilt")
            psi = rp_file.column_as_list("particles", "_rlnAnglePsi")
        except ValueError:
            phi = rp_file.column_as_list("angles", "_rlnAngleRot")
            theta = rp_file.column_as_list("angles", "_rlnAngleTilt")
            psi = rp_file.column_as_list("angles", "_rlnAnglePsi")

        rp_labels: List[Union[int, str]] = [
            f"phi/rot: {x[0]} theta/tilt: {x[1]}, psi: {x[2]}"
            for x in zip(phi, theta, psi)
        ]
        return [
            mini_montage_from_stack(
                stack_file=str(Path(self.output_dir) / "reproj.mrcs"),
                outputdir=self.output_dir,
                title="Reprojections",
                nimg=-1,
                labels=rp_labels,
            )
        ]

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        nr_uniform = self.joboptions["nr_uniform"].get_number()
        angles_file = self.joboptions["angles_filename"].get_string()
        if angles_file != "" and nr_uniform <= 0:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["nr_uniform"],
                        self.joboptions["angles_filename"],
                    ],
                    message=(
                        "An angles file or a number of uniform projections must be "
                        "specified"
                    ),
                )
            ]
        return []

    def gather_metadata(self) -> Dict[str, object]:
        rp_file = DataStarFile(os.path.join(self.output_dir, "reproj.star"))
        n_rp = rp_file.count_block("particles")
        return {"nProj": n_rp}
