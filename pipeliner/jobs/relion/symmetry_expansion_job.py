#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from typing import List, Dict, Any, Tuple, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import relion_program, RelionJob, Ref
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    BooleanJobOption,
    JobOptionCondition,
    IntJobOption,
    JobOptionValidationResult,
)
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA
from pipeliner.starfile_handler import StarFile, DataStarFile
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionSymmetryExpansion(RelionJob):
    PROCESS_NAME = "relion.image_analysis.symmetry_expansion"
    OUT_DIR = "SymmetryExpansion"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Symmetry Expansion"

        self.jobinfo.short_desc = (
            "Generate additional particles by adding symmetry-equivalent copies"
            " of existing particle images"
        )
        self.jobinfo.long_desc = (
            "Generate additional particle by adding symmetry-equivalent copies"
            " of existing particle images"
        )
        self.jobinfo.programs = [relion_program("relion_particle_symmetry_expand")]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Ilca SL",
                    "Sun X",
                    "El Omari K",
                    "Kotecha A",
                    "de Haas F",
                    "DiMaio, F",
                    "Grimes JM",
                    "Stuart DI",
                    "Poranen MM",
                    "Huiskonen JT",
                ],
                title=(
                    "Multiple liquid crystalline geometries of highly compacted nucleic"
                    " acid in a dsRNA virus"
                ),
                journal="Nature",
                year="2019",
                volume="570",
                pages="252–256",
                doi="10.1038/s41586-019-1229-9",
            )
        ]

        self.joboptions["input_particles"] = InputNodeJobOption(
            label="Input particles file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern="Particles star file (.star)",
            help_text=(
                "Input STAR file with the projection images and their orientations"
            ),
            is_required=True,
        )

        self.joboptions["symmetry"] = StringJobOption(
            label="Symmetry:",
            default_value="C2",
            help_text=(
                "Use a C2 or higher symmetry. Note there"
                " are multiple possibilities for icosahedral symmetry: \n* I1:"
                " No-Crowther 222 (standard in Heymann, Chagoyen & Belnap, JSB, 151"
                " (2005) 196–207) \n * I2: Crowther 222 \n * I3: 52-setting (as used in"
                " SPIDER?)\n * I4: A different 52 setting \n The command 'relion_refine"
                " --sym D2 --print_symmetry_ops' prints a list of all symmetry"
                " operators for symmetry group D2. RELION uses XMIPP's libraries for"
                " symmetry operations. Therefore, look at the XMIPP Wiki for more"
                " details:"
                " http://xmipp.cnb.csic.es/twiki/bin/view/Xmipp/WebHome?topic=Symmetry"
            ),
            is_required=True,
            validation_regex="(C[1-9][0-9]*)|(D[2-9][0-9]*)|I[243]|O|T",
            regex_error_message="Symmetry must be in Cn, Dn, I[234], O or T format",
        )

        self.joboptions["do_helix"] = BooleanJobOption(
            label="Do helical symmetry expansion?",
            default_value=False,
            help_text="Symmetry expansion for helical reconsructions",
            jobop_group="Helical options",
        )

        self.joboptions["twist"] = FloatJobOption(
            label="Helical twist (degrees):",
            default_value=0,
            hard_min=0,
            hard_max=360,
            help_text="Helical twist in degrees",
            jobop_group="Helical options",
            deactivate_if=JobOptionCondition([("do_helix", "=", False)]),
        )
        self.joboptions["rise"] = FloatJobOption(
            label="Helical Rise (Å):",
            default_value=0,
            hard_min=0,
            hard_max=360,
            help_text="Helical rise in Ångstrom",
            jobop_group="Helical options",
            deactivate_if=JobOptionCondition([("do_helix", "=", False)]),
        )
        self.joboptions["asu"] = IntJobOption(
            label="Number of asymmetric units to expand:",
            default_value=1,
            hard_min=1,
            help_text="Number of asymmetric units to expand",
            jobop_group="Helical options",
            deactivate_if=JobOptionCondition([("do_helix", "=", False)]),
        )

        self.joboptions["frac_sampling"] = IntJobOption(
            label="Sampling between asymmetric units:",
            default_value=1,
            hard_min=1,
            help_text="Number of samplings in between a single asymmetrical unit",
            jobop_group="Helical options",
            deactivate_if=JobOptionCondition([("do_helix", "=", False)]),
        )
        self.joboptions["frac_range"] = FloatJobOption(
            label="Range for rise sampling:",
            default_value=0.5,
            help_text=(
                "High and low limit for sampling of the helical rise EG: 0.5 samples"
                "[-0.5, 0.5]"
            ),
            jobop_group="Helical options",
            deactivate_if=JobOptionCondition([("do_helix", "=", False)]),
        )
        self.get_runtab_options(addtl_args=True)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        if self.joboptions["symmetry"].get_string() == "C1":
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["symmetry"]],
                    message="A higher order symmetry must be used",
                )
            )

        return errs

    def create_output_nodes(self) -> None:
        self.add_output_node(
            file_name="expanded.star",
            node_type=NODE_PARTICLEGROUPMETADATA,
            keywords=["relion", "symmetry_expanded"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        input_parts = self.joboptions["input_particles"].get_string()
        command = [
            "relion_particle_symmetry_expand",
            "--i",
            input_parts,
            "--sym",
            self.joboptions["symmetry"].get_string(),
            "--o",
            str(Path(self.output_dir) / "expanded.star"),
        ]

        # try to determine the pixel size automatically
        add_ops = self.parse_additional_args()
        if "--angpix" not in add_ops:
            try:
                parts_file = StarFile(input_parts)
                px = parts_file.loop_as_list(
                    block="optics", columns=["_rlnImagePixelSize"]
                )
                command.extend(["--angpix", px[0][0]])
            except Exception:
                raise ValueError(
                    "Cannot determine pixel size of the particle images. Specify it by"
                    " adding the argument '--angpix <pixel size>' in additional"
                    " arguments"
                )

        if self.joboptions["do_helix"].get_boolean():
            command.extend(
                [
                    "--helix",
                    "--twist",
                    self.joboptions["twist"].get_string(),
                    "--rise",
                    self.joboptions["rise"].get_string(),
                    "--asu",
                    self.joboptions["asu"].get_string(),
                    "--frac_sampling",
                    self.joboptions["frac_sampling"].get_string(),
                    "--frac_range",
                    self.joboptions["frac_range"].get_string(),
                ]
            )
        command.extend(add_ops)

        return [PipelinerCommand(command)]

    def get_counts(self) -> Tuple[int, int]:
        """Count the number of original and expanded particles"""
        orig = self.joboptions["input_particles"].get_string()
        orig_count = DataStarFile(orig).count_block("particles")
        exp = str(Path(self.output_dir) / "expanded.star")
        expanded_count = DataStarFile(exp).count_block("particles")
        return orig_count, expanded_count

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        orig_count, expanded_count = self.get_counts()
        return [
            create_results_display_object(
                "table",
                title="Particle symmetry expansion results",
                headers=["", "Particle count"],
                table_data=[
                    ["Original file", str(orig_count)],
                    ["Symmetry expanded", str(expanded_count)],
                    ["Particles added", str(expanded_count - orig_count)],
                ],
                associated_data=[str(Path(self.output_dir) / "expanded.star")],
            ),
            self.output_nodes[0].default_results_display(self.output_dir),
        ]

    def gather_metadata(self) -> Dict[str, Any]:
        orig_count, expanded_count = self.get_counts()
        return {
            "original_particle_count": orig_count,
            "expanded_particle_count": expanded_count,
        }
