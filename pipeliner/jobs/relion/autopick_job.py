#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from numpy import mean, std
from typing import List, Dict, Sequence, Tuple

from pipeliner.jobs.relion.relion_job import relion_program, RelionJob
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.data_structure import (
    AUTOPICK_LOG_NAME,
    AUTOPICK_REF3D_NAME,
    AUTOPICK_REF2D_NAME,
    AUTOPICK_LOG_HELICAL_NAME,
    AUTOPICK_REF3D_HELICAL_NAME,
    AUTOPICK_REF2D_HELICAL_NAME,
    AUTOPICK_TOPAZ_NAME,
    AUTOPICK_TOPAZ_HELICAL_NAME,
    AUTOPICK_TOPAZ_TRAIN_NAME,
    AUTOPICK_DIR,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_IMAGE2DSTACK,
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MLMODEL,
    NODE_LOGFILE,
)
from pipeliner.node_factory import create_node
from pipeliner.utils import truncate_number, decompose_pipeline_filename
from pipeliner.pipeliner_job import Ref, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    ExternalFileJobOption,
    StringJobOption,
    files_exts,
    SAMPLING,
    EXT_2DREFS,
    EXT_MRC_MAP,
    EXT_STARFILE,
    EXT_COORDS,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionValidationResult,
    JobOptionCondition,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)

from pipeliner.starfile_handler import DataStarFile
from pipeliner.user_settings import get_topaz_executable
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionAutoPickJob(RelionJob):
    OUT_DIR = AUTOPICK_DIR
    CATEGORY_LABEL = "Automated Particle Picking"

    def __init__(self) -> None:
        super().__init__()
        self.always_continue_in_schedule = True
        self.jobinfo.programs = [relion_program("relion_autopick")]
        self.joboptions["fn_input_autopick"] = InputNodeJobOption(
            label="Input micrographs for autopick:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Input Micrographs", EXT_STARFILE),
            help_text=(
                "Input STAR file (preferably with CTF information) with all micrographs"
                " to pick from."
            ),
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size in micrographs (A)",
            default_value=-1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Pixel size in Angstroms. If a CTF-containing STAR file is input, then"
                " the value given here will be ignored, and the pixel size will be"
                " calculated from the values in the STAR file. A negative value can"
                " then be given here."
            ),
            is_required=True,
        )

        self.joboptions["do_write_fom_maps"] = BooleanJobOption(
            label="Write FOM maps?",
            default_value=False,
            help_text=(
                "If set to Yes, intermediate probability maps will be written out,"
                " which (upon reading them back in) will speed up tremendously the"
                " optimization of the threshold and inter-particle distance parameters."
                " However, with this option, one cannot run in parallel, as disc I/O is"
                " very heavy with this option set."
            ),
            in_continue=True,
        )

        self.joboptions["do_read_fom_maps"] = BooleanJobOption(
            label="Read FOM maps?",
            default_value=False,
            help_text=(
                "If written out previously, read the FOM maps back in and re-run the"
                " picking to quickly find the optimal threshold and inter-particle"
                " distance parameters"
            ),
            in_continue=True,
        )

        self.joboptions["shrink"] = FloatJobOption(
            label="Shrink factor:",
            default_value=0,
            step_value=0.1,
            help_text=(
                "This is useful to speed up the calculations, and to make them less"
                " memory-intensive. The micrographs will be downscaled (shrunk) to"
                " calculate the cross-correlations, and peak searching will be done in"
                " the downscaled FOM maps. When set to 0, the micrographs will be"
                " downscaled to the lowpass filter of the references, a value between 0"
                " and 1 will downscale the micrographs by that factor. Note that the"
                " results will not be exactly the same when you shrink"
                " micrographs!\n\nIn the Laplacian-of-Gaussian picker, this option is"
                " ignored and the shrink factor always becomes 0."
            ),
            hard_max=1,
            hard_min=0,
            is_required=True,
        )

        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU acceleration?",
            default_value=False,
            help_text="If set to Yes, the job will try to use GPU acceleration",
            in_continue=True,
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "This argument is not required. If left empty, the job itself will try"
                " to allocate available GPU resources. You can override the default"
                " allocation by providing a list of which GPUs (0,1,2,3, etc) to use."
                " MPI-processes are separated by ':'. For example: 0:1:0:1:0:1"
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("use_gpu", "=", False)]),
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def create_output_nodes(self) -> None:
        """Add output nodes common to most AutoPick jobs."""
        self.add_output_node(
            "autopick.star", NODE_MICROGRAPHCOORDSGROUP, ["relion", "autopick"]
        )
        self.add_output_node("logfile.pdf", NODE_LOGFILE, ["relion", "autopick"])

    def relion_back_compatibility_joboptions(
        self, do_refs=False, do_ref3d=False, do_log=False, do_topaz=False
    ) -> None:
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_refs"] = BooleanJobOption(
            label="Use reference-based template-matching?",
            default_value=do_refs,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_ref3d"] = BooleanJobOption(
            label="OR: provide a 3D reference?",
            default_value=do_ref3d,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_log"] = BooleanJobOption(
            label="OR: use Lacpaclian-of-Gaussian?",
            default_value=do_log,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_topaz"] = BooleanJobOption(
            label="OR: use Topaz?",
            default_value=do_topaz,
            jobop_group="Relion compatibility options",
        )

    def add_helical_options(self) -> None:
        self.joboptions["helical_tube_outer_diameter"] = FloatJobOption(
            label="Tube diameter (A):",
            default_value=200,
            suggested_min=100,
            suggested_max=1000,
            step_value=10,
            hard_min=0,
            help_text=(
                "Outer diameter (in Angstroms) of helical tubes. This value should be"
                " slightly larger than the actual width of the tubes."
            ),
            in_continue=True,
            is_required=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_nr_asu"] = IntJobOption(
            label="Number of unique asymmetrical units:",
            default_value=1,
            hard_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of unique helical asymmetrical units in each segment  box. This"
                " integer should not be less than 1. The inter-box  distance (pixels) ="
                " helical rise (Angstroms) * number of  asymmetrical units / pixel size"
                " (Angstroms). The optimal inter-box distance might also depend on the"
                " box size, the helical rise and the flexibility of the structure. In"
                " general, an inter-box distance of ~10% * the box size seems"
                " appropriate."
            ),
            in_continue=True,
            is_required=True,
            jobop_group="Helical processing options",
        )
        self.joboptions["helical_rise"] = FloatJobOption(
            label="Helical rise (A):",
            default_value=-1,
            suggested_max=100,
            step_value=0.01,
            help_text=(
                "Helical rise in Angstroms. (Please click '?' next to the option"
                " above for details about how the inter-box distance is calculated.)"
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
            jobop_group="Helical processing options",
        )
        # give this one a more informative error message

        self.joboptions["helical_tube_kappa_max"] = FloatJobOption(
            label="Maximum curvature (kappa):",
            default_value=0.1,
            suggested_min=0.05,
            suggested_max=0.5,
            step_value=0.01,
            help_text=(
                "Maximum curvature allowed for picking helical tubes. Kappa = 0.3 means"
                " that the curvature of the picked helical tubes should not be larger"
                " than 30% the curvature of a circle (diameter = particle mask"
                " diameter). Kappa ~ 0.05 is recommended for long and straight tubes"
                " (e.g. TMV, VipA/VipB and AChR tubes) while 0.20 ~ 0.40 seems suitable"
                " for flexible ones (e.g. ParM and MAVS-CARD filaments)."
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_tube_length_min"] = FloatJobOption(
            label="Minimum length (A):",
            default_value=-1,
            suggested_min=100,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "Minimum length (in Angstroms) of helical tubes for auto-picking."
                " Helical tubes with shorter lengths will not be picked. Note that a"
                " long helical tube seen by human eye might be treated as short broken"
                " pieces due to low FOM values or high picking threshold."
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["do_amyloid"] = BooleanJobOption(
            label="Pick amyloid segments?",
            default_value=False,
            help_text=(
                "Set to Yes if you want to use the algorithm that was developed"
                " specifically for picking amyloids."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

    def command_setup(self) -> None:
        self.command = self.get_relion_command("autopick")

        fn_input_autopick = self.joboptions["fn_input_autopick"].get_string()
        self.command += ["--i", fn_input_autopick]
        self.command += ["--odir", self.output_dir]
        self.command += ["--pickname", "autopick"]

    def additional_common_options(self) -> None:
        angpix = self.joboptions["angpix"].get_number()
        if angpix > 0:
            self.command += ["--angpix", str(angpix)]

    def read_write_options(self, gpu=True, topaz=False):
        if gpu:
            gpus = self.joboptions["gpu_ids"].get_string()
            sel_gpu = self.joboptions.get("use_gpu")
            if sel_gpu:
                if self.joboptions["use_gpu"].get_boolean():
                    self.command += ["--gpu", gpus]
            elif not sel_gpu:
                self.command += ["--gpu", gpus]

        do_read_fom_maps = False
        do_write_fom_maps = False
        if not topaz:
            do_read_fom_maps = self.joboptions["do_read_fom_maps"].get_boolean()
            do_write_fom_maps = self.joboptions["do_write_fom_maps"].get_boolean()

            if do_write_fom_maps:
                self.command.append("--write_fom_maps")

            if do_read_fom_maps:
                self.command.append("--read_fom_maps")

        if self.is_continue and not (do_read_fom_maps or do_write_fom_maps):
            self.command.append("--only_do_unfinished")

        self.command += self.parse_additional_args()

    def helical_options(self) -> None:
        helical_nr_asu = self.joboptions["helical_nr_asu"].get_number()
        helical_rise = self.joboptions["helical_rise"].get_number()

        min_distance = helical_nr_asu * helical_rise
        self.command += ["--min_distance", str(min_distance)]

        self.command.append("--helix")
        if self.joboptions["do_amyloid"].get_boolean():
            self.command.append("--amyloid")
        self.command += [
            "--helical_tube_outer_diameter",
            self.joboptions["helical_tube_outer_diameter"].get_string(),
        ]
        self.command += [
            "--helical_tube_kappa_max",
            self.joboptions["helical_tube_kappa_max"].get_string(),
        ]
        self.command += [
            "--helical_tube_length_min",
            self.joboptions["helical_tube_length_min"].get_string(),
        ]

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """Return list of intermediate files/dirs to remove"""

        # get all of the subdirs
        subdirs = self.get_job_subdirs()
        del_files = []
        del_dirs: List[str] = []

        for sd in subdirs:
            del_files += glob(sd + "/*.spi")

        return del_files, del_dirs

    def gather_metadata(self) -> Dict[str, object]:

        # read the the output file and get the picked mics
        outfile = DataStarFile(os.path.join(self.output_dir, "autopick.star"))
        coords_block = outfile.get_block("coordinate_files")
        coords_table = coords_block.find_values("_rlnMicrographCoordinates")

        # count all the picks
        ppms = []
        all_foms = []
        for pickfile in coords_table:
            coorddata = DataStarFile(pickfile)
            coordblock = coorddata.get_block(None)
            foms_table = coordblock.find_values("_rlnAutopickFigureOfMerit")
            all_foms = [float(x) for x in foms_table]
            ppms.append(len(all_foms))

        metadata_dict = {
            "ParticleCount": len(all_foms),
            "PickedMicrographs": len(coords_table),
            "MeanPartsPerMicrograph": mean(ppms),
            "MinPartsPerMicrograph": min(ppms),
            "MaxPartsPerMicrograph": max(ppms),
            "StdPartsPerMicrograph": std(ppms),
            "MeanPartFOM": mean(all_foms),
            "MinPartFOM": min(all_foms),
            "MaxPartFOM": max(all_foms),
            "StdPartFOM": std(all_foms),
        }

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        # get the star files
        instar = DataStarFile(self.joboptions["fn_input_autopick"].get_string())
        mdat = instar.get_block("micrographs").find(["_rlnMicrographName"])[0][0]
        logsdir = os.path.join(
            self.output_dir, os.path.dirname(decompose_pipeline_filename(mdat)[2])
        )
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(DataStarFile(pf).count_block())
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "autopick.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "autopick.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [image, graph]


class RelionAutoPickLoG(RelionAutoPickJob):

    PROCESS_NAME = AUTOPICK_LOG_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME

        self.jobinfo.display_name = "RELION AutoPick LoG (single particle)"
        self.jobinfo.short_desc = "Reference-free Laplacian of Gaussian autopicking"

        self.joboptions["log_diam_min"] = FloatJobOption(
            label="Min. diameter for LoG filter (A)",
            default_value=200,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text=(
                "The smallest allowed diameter for the blob-detection algorithm. This"
                " should correspond to the smallest size of your particles in"
                " Angstroms."
            ),
            hard_min=0,
            is_required=True,
        )

        self.joboptions["log_diam_max"] = FloatJobOption(
            label="Max. diameter for LoG filter (A)",
            default_value=250,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text=(
                "The largest allowed diameter for the blob-detection algorithm. This"
                " should correspond to the largest size of your particles in Angstroms."
            ),
            hard_min=0,
            is_required=True,
        )

        self.joboptions["log_invert"] = BooleanJobOption(
            label="Are the particles white?",
            default_value=False,
            help_text=(
                "Set this option to No if the particles are black, and to Yes if the"
                " particles are white."
            ),
        )

        self.joboptions["log_maxres"] = FloatJobOption(
            label="Maximum resolution to consider (A)",
            default_value=20,
            suggested_min=10,
            suggested_max=100,
            step_value=5,
            help_text=(
                "The Laplacian-of-Gaussian filter will be applied to downscaled"
                " micrographs with the corresponding size. Give a negative value to"
                " skip downscaling."
            ),
            is_required=True,
        )

        self.joboptions["log_adjust_thr"] = FloatJobOption(
            label="Adjust default threshold (stddev):",
            default_value=0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.05,
            help_text=(
                "Use this to pick more (negative number -> lower threshold) or less"
                " (positive number -> higher threshold) particles compared to the"
                " default setting. The threshold is moved this many standard deviations"
                " away from the average."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["log_upper_thr"] = FloatJobOption(
            label="Upper threshold (stddev):",
            default_value=999.99,
            hard_min=0.0,
            suggested_max=10.0,
            step_value=0.5,
            help_text=(
                "Use this to discard picks with LoG thresholds that are this many"
                " standard deviations above the average, e.g. to avoid high contrast"
                " contamination like ice and ethane droplets. Good values depend on the"
                " contrast of micrographs and need to be interactively explored; for"
                " low contrast micrographs, values of ~ 1.5 may be reasonable, but the"
                " same value will be too low for high-contrast micrographs."
            ),
            in_continue=True,
            is_required=True,
        )

        del self.joboptions["use_gpu"]
        del self.joboptions["gpu_ids"]
        del self.joboptions["shrink"]

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "log_diam_min",
                "log_diam_max",
                "log_invert",
                "log_maxres",
                "log_adjust_thr",
                "log_upper_thr",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        dmin = self.joboptions["log_diam_min"].get_number()
        dmax = self.joboptions["log_diam_max"].get_number()
        if dmin > dmax:
            return [
                JobOptionValidationResult(
                    "error",
                    [self.joboptions["log_diam_min"], self.joboptions["log_diam_max"]],
                    "Max diameter of LoG filter (log_diam_max) must be > min diameter "
                    "(log_diam_min).",
                ),
            ]
        return []

    def get_commands(self, helical=False) -> List[PipelinerCommand]:

        self.command_setup()

        self.command.append("--LoG")
        self.command += ["--LoG_diam_min", self.joboptions["log_diam_min"].get_string()]
        self.command += ["--LoG_diam_max", self.joboptions["log_diam_max"].get_string()]
        self.command += [
            "--shrink",
            "0",
            "--lowpass",
            self.joboptions["log_maxres"].get_string(),
        ]
        self.command += [
            "--LoG_adjust_threshold",
            self.joboptions["log_adjust_thr"].get_string(),
        ]
        log_upper_thr = int(self.joboptions["log_upper_thr"].get_number())

        if log_upper_thr < 999:
            self.command += ["--LoG_upper_threshold", str(log_upper_thr)]

        if self.joboptions["log_invert"].get_boolean():
            self.command.append("--Log_invert")

        # common options
        self.additional_common_options()
        if helical:
            self.helical_options()
        self.read_write_options(gpu=False)

        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_log=True)


class RelionAutoPickLogHelical(RelionAutoPickLoG):

    PROCESS_NAME = AUTOPICK_LOG_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME
        self.add_helical_options()
        self.jobinfo.display_name = "RELION AutoPick LoG (helical)"
        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "log_diam_min",
                "log_diam_max",
                "log_invert",
                "log_maxres",
                "log_adjust_thr",
                "log_upper_thr",
                "do_amyloid",
                "helical_tube_outer_diameter",
                "helical_nr_asu",
                "helical_rise",
                "helical_tube_kappa_max",
                "helical_tube_length_min",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=True) -> List[PipelinerCommand]:
        return RelionAutoPickLoG.get_commands(self, helical)


class RelionAutoPickRef3D(RelionAutoPickJob):

    PROCESS_NAME = AUTOPICK_REF3D_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME

        self.jobinfo.display_name = (
            "RELION AutoPick (3D reference-based, single particle)"
        )
        self.jobinfo.short_desc = "Autopicking with a 3-dimensional reference"

        self.joboptions["fn_ref3d_autopick"] = InputNodeJobOption(
            label="3D reference:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("Input reference", EXT_MRC_MAP),
            help_text=(
                "Input MRC file with the 3D reference maps, from which 2D references"
                " will be made by projection. Note that the absolute greyscale needs to"
                " be correct, so only use maps created by RELION itself from this data"
                " set."
            ),
            is_required=True,
        )

        self.joboptions["ref3d_symmetry"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text=(
                "Symmetry point group of the 3D reference. Only projections in the"
                " asymmetric part of the sphere will be generated."
            ),
            is_required=True,
            # TO DO: add regex validation here - need list of accepted syms
        )

        self.joboptions["ref3d_sampling"] = MultipleChoiceJobOption(
            label="3D angular sampling:",
            choices=SAMPLING,
            default_value_index=0,
            help_text=(
                "There are only a few discrete angular samplings possible because we"
                " use the HealPix library to generate the sampling of the first two"
                " Euler angles on the sphere. The samplings are approximate numbers and"
                " vary slightly over the sphere.\n\n For autopicking, 30 degrees is"
                " usually fine enough, but for highly symmetrical objects one may need"
                " to go finer to adequately sample the asymmetric part of the sphere."
            ),
        )

        # common ref options
        self.joboptions["lowpass"] = FloatJobOption(
            label="Lowpass filter references (A)",
            default_value=20,
            suggested_min=10,
            suggested_max=100,
            step_value=5,
            help_text=(
                "Lowpass filter that will be applied to the references before template"
                " matching. Do NOT use very high-resolution templates  to search your"
                " micrographs. The signal will be too weak at high resolution anyway,"
                " and you may find Einstein from noise.... Give a negative value to"
                " skip the lowpass filter."
            ),
            is_required=True,
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="Highpass filter (A)",
            default_value=-1,
            suggested_min=100,
            suggested_max=1000,
            step_value=100,
            help_text=(
                "Highpass filter that will be applied to the micrographs.This may be"
                " useful to get rid of background ramps due to uneven ice"
                " distributions. Give a negative value to skip the high pass filter. "
                " Useful values are often in the range of 200-400 Angstroms."
            ),
            is_required=True,
        )

        self.joboptions["angpix_ref"] = FloatJobOption(
            label="Pixel size in references (A)",
            default_value=-1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Pixel size in Angstroms for the provided reference images. This will"
                " be used to calculate the filters and the particle diameter in pixels."
                " If a negative value is given here, the pixel size in the references"
                " will be assumed to be the same as the one in the micrographs, i.e."
                " the particles that were used to make the references were not rescaled"
                " upon extraction."
            ),
            is_required=True,
        )

        self.joboptions["psi_sampling_autopick"] = FloatJobOption(
            label="In-plane angular sampling (deg)",
            default_value=5,
            suggested_min=1,
            suggested_max=30,
            hard_min=0,
            step_value=1,
            help_text=(
                "Angular sampling in degrees for exhaustive searches of the in-plane"
                " rotations for all references."
            ),
            is_required=True,
        )

        self.joboptions["threshold_autopick"] = FloatJobOption(
            label="Picking threshold:",
            default_value=0.05,
            hard_min=0,
            suggested_max=1.0,
            step_value=0.01,
            help_text=(
                "Use lower thresholds to pick more particles (and more junk"
                " probably).\n\nThis option is ignored in the Laplacian-of-Gaussian"
                " picker. Please use 'Adjust default threshold' in the 'Laplacian' tab"
                " instead."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["mindist_autopick"] = FloatJobOption(
            label="Minimum inter-particle distance (A):",
            default_value=100,
            hard_min=0,
            suggested_max=1000,
            step_value=20,
            help_text=(
                "Particles closer together than this distance will be consider to be a"
                " single cluster. From each cluster, only one particle will be picked."
                " \n\nThis option takes no effect for picking helical segments. The"
                " inter-box distance is calculated with the number of asymmetrical"
                " units and the helical rise on 'Helix' tab. This option is also"
                " ignored in the Laplacian-of-Gaussian picker. The inter-box distance"
                " is calculated from particle diameters."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["maxstddevnoise_autopick"] = FloatJobOption(
            label="Maximum stddev noise:",
            default_value=1.1,
            suggested_min=0.9,
            suggested_max=1.5,
            step_value=0.02,
            help_text=(
                "This is useful to prevent picking in carbon areas, or areas with big"
                " contamination features. Peaks in areas where the background standard"
                " deviation in the normalized micrographs is higher than this value"
                " will be ignored. Useful values are probably in the range 1.0 to 1.2."
                " Set to -1 to switch off the feature to eliminate peaks due to high"
                " background standard deviations.\n\nThis option is ignored in the"
                " Laplacian-of-Gaussian picker."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["minavgnoise_autopick"] = FloatJobOption(
            label="Minimum avg noise:",
            default_value=-999.0,
            suggested_min=-2,
            suggested_max=0.5,
            step_value=0.05,
            help_text=(
                "This is useful to prevent picking in carbon areas, or areas with big"
                " contamination features. Peaks in areas where the background standard"
                " deviation in the normalized micrographs is higher than this value"
                " will be ignored. Useful values are probably in the range -0.5 to 0."
                " Set to -999 to switch off the feature to eliminate peaks due to low"
                " average background densities.\n\nThis option is ignored in the"
                " Laplacian-of-Gaussian picker."
            ),
            in_continue=True,
            is_required=True,
        )

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "fn_ref3d_autopick",
                "angpix_ref",
                "ref3d_symmetry",
                "ref3d_sampling",
                "shrink",
                "lowpass",
                "highpass",
                "psi_sampling_autopick",
                "threshold_autopick",
                "mindist_autopick",
                "maxstddevnoise_autopick",
                "minavgnoise_autopick",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=False) -> List[PipelinerCommand]:
        self.command_setup()
        fn_ref3d_autopick = self.joboptions["fn_ref3d_autopick"]
        threed_ref = fn_ref3d_autopick.get_string()
        self.command += ["--ref", threed_ref]

        self.command += ["--sym", self.joboptions["ref3d_symmetry"].get_string()]

        sampling = self.joboptions["ref3d_sampling"].get_string()
        sampling_opts = SAMPLING
        ref3d_sampling = str(sampling_opts.index(sampling) + 1)
        self.command += ["--healpix_order", ref3d_sampling]

        self.command += ["--ang", self.joboptions["psi_sampling_autopick"].get_string()]
        self.command += ["--shrink", self.joboptions["shrink"].get_string()]

        lowpass = int(self.joboptions["lowpass"].get_number())
        if lowpass > 0:
            self.command += ["--lowpass", str(lowpass)]

        highpass = int(self.joboptions["highpass"].get_number())
        if highpass > 0:
            self.command += ["--highpass", str(highpass)]

        angpixref = self.joboptions["angpix_ref"].get_number()
        self.command += ["--angpix_ref", str(angpixref)]

        ap_thresh = self.joboptions["threshold_autopick"].get_string()
        self.command += ["--threshold", ap_thresh]

        # common options
        self.additional_common_options()

        # additional reference specific options

        minavgnoise = self.joboptions["minavgnoise_autopick"].get_number()
        if minavgnoise > -900:
            self.command += ["--min_avg_noise ", str(minavgnoise)]

        mindist = self.joboptions["mindist_autopick"].get_string()
        self.command += ["--min_distance", mindist]
        max_stddev_noise = self.joboptions["maxstddevnoise_autopick"].get_string()
        self.command += ["--max_stddev_noise", max_stddev_noise]

        if helical:
            self.helical_options()
        self.read_write_options()

        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_ref3d=True)


class RelionAutoPickRef3DHelical(RelionAutoPickRef3D):

    PROCESS_NAME = AUTOPICK_REF3D_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME
        self.add_helical_options()
        self.jobinfo.display_name = "RELION AutoPick (reference-based, helical)"

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "fn_ref3d_autopick",
                "angpix_ref",
                "ref3d_symmetry",
                "ref3d_sampling",
                "shrink",
                "lowpass",
                "highpass",
                "psi_sampling_autopick",
                "threshold_autopick",
                "mindist_autopick",
                "maxstddevnoise_autopick",
                "minavgnoise_autopick",
                "do_amyloid",
                "helical_tube_outer_diameter",
                "helical_nr_asu",
                "helical_rise",
                "helical_tube_kappa_max",
                "helical_tube_length_min",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=True) -> List[PipelinerCommand]:
        return RelionAutoPickRef3D.get_commands(self, helical)


class RelionAutoPickTopaz(RelionAutoPickJob):

    PROCESS_NAME = AUTOPICK_TOPAZ_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME

        # find topaz
        self.topaz = ExternalProgram(get_topaz_executable())

        self.jobinfo.display_name = "RELION AutoPick Topaz (single particle)"
        self.jobinfo.short_desc = "Reference-free autopicking using Topaz"
        self.jobinfo.programs += [self.topaz]
        self.jobinfo.references.append(
            Ref(
                authors=[
                    "Bepler T",
                    "Morin A",
                    "Rapp M",
                    "Brasch J",
                    "Shapiro L",
                    "Noble AJ",
                    "Berger B",
                ],
                title=(
                    "Positive-unlabeled convolutional neural networks for particle"
                    " picking in cryo-electron micrographs"
                ),
                journal="Nat Methods",
                year="2019",
                volume="16",
                issue="11",
                pages="1153-1160",
                doi="10.1038/s41592-019-0575-8.",
            )
        )
        del self.joboptions["do_write_fom_maps"]
        del self.joboptions["do_read_fom_maps"]
        self.joboptions["topaz_particle_diameter"] = FloatJobOption(
            label="Particle diameter (A)",
            default_value=-1,
            suggested_max=2000,
            step_value=20,
            help_text=(
                "Diameter of the particle (to be used to infer topaz downscale factor"
                " and particle radius)"
            ),
            is_required=True,
        )

        self.joboptions["topaz_nr_particles"] = IntJobOption(
            label="Nr of particles per micrograph:",
            default_value=-1,
            hard_min=0,
            suggested_max=2000,
            step_value=20,
            help_text="Expected average number of particles per micrograph",
            is_required=True,
        )

        self.joboptions["topaz_model"] = InputNodeJobOption(
            label="Trained topaz model:",
            node_type=NODE_MLMODEL,
            node_kwds=["topaz"],
            default_value="",
            pattern=files_exts("SAV Files", [".sav"]),
            help_text=(
                "Trained topaz model for topaz-based picking. Use on job for training"
                " and a next job for picking. Leave this empty to use the default"
                " (general) model."
            ),
            is_required=False,
        )

        self.joboptions["topaz_other_args"] = StringJobOption(
            label="Additional topaz arguments:",
            default_value="",
            help_text=(
                "These additional arguments will be passed onto all topaz programs."
            ),
        )

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "shrink",
                "topaz_particle_diameter",
                "topaz_nr_particles",
                "topaz_model",
                "topaz_other_args",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=False) -> List[PipelinerCommand]:
        self.command_setup()
        if self.topaz.exe_path is None:
            raise ValueError("Topaz exe not found")
        if self.topaz.name == "relion_python_topaz":
            topaz_arg = "--fn_topaz_exe"
        else:
            topaz_arg = "--topaz_exe"

        self.command += [topaz_arg, self.topaz.exe_path]

        topaz_nr_particles = self.joboptions["topaz_nr_particles"].get_number()
        if topaz_nr_particles > 0:
            self.command += [
                "--topaz_nr_particles",
                truncate_number(topaz_nr_particles, 0),
            ]

        topaz_part_diam = self.joboptions["topaz_particle_diameter"].get_number()
        if topaz_part_diam > 0:
            self.command += [
                "--particle_diameter",
                truncate_number(topaz_part_diam, 0),
            ]

        self.command.append("--topaz_extract")
        topaz_model = self.joboptions["topaz_model"].get_string()
        if topaz_model:
            self.command += ["--topaz_model", topaz_model]

        topaz_other_args = self.joboptions["topaz_other_args"].get_string()
        if topaz_other_args:
            self.command += ["--topaz_args", topaz_other_args]

        # common options
        self.additional_common_options()
        if helical:
            if self.joboptions["do_topaz_filaments"].get_boolean():
                self.command.extend(
                    [
                        "--helix",
                        "--topaz_threshold",
                        self.joboptions["topaz_filament_threshold"].get_number(),
                        "--helical_tube_length_min",
                        self.joboptions["topaz_hough_length"].get_number(),
                    ]
                )
        self.read_write_options(topaz=True)

        return [PipelinerCommand(self.command, relion_control=True)]

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_topaz=True)
        topaz = "" if not self.topaz.exe_path else self.topaz.exe_path
        self.joboptions["fn_topaz_exec"] = ExternalFileJobOption(
            label="Topaz executable",
            default_value=topaz,
        )
        self.joboptions["fn_topaz_exec"].value = topaz


class RelionAutoPickTopazHelical(RelionAutoPickTopaz):
    PROCESS_NAME = AUTOPICK_TOPAZ_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME

        self.jobinfo.display_name = "RELION AutoPick Topaz (helical)"
        self.joboptions["do_topaz_filaments"] = BooleanJobOption(
            label="Use advanced filament picking?",
            default_value=True if self.topaz == "relion_python_topaz" else False,
            help_text=(
                "If set to Yes, this option will activate a modified version of"
                " topaz that can better pick filaments, as described in Lovestam &"
                " Scheres, Faraday Discussions 2022"
            ),
            jobop_group="Helical processing options",
        )
        self.joboptions["topaz_filament_threshold"] = FloatJobOption(
            label="Threshold:",
            default_value=-5,
            help_text=(
                "This sets the filament picking threshold, as described in"
                " Lovestam & Scheres, Faraday Discussions 2022. Useful values in"
                " our work on recombinant tau for the threshold range from −4 to"
                " −7. We typically do not change the default length of the Hough"
                " transform, which is set to be equal to the particle diameter"
                " when a negative value is given here. You can provide the"
                " additional option -fp to display images of intermediate steps"
                " of the algorithm to tune difficult cases."
            ),
            deactivate_if=JobOptionCondition([("do_topaz_filaments", "=", False)]),
            jobop_group="Helical processing options",
        )
        self.joboptions["topaz_hough_length"] = FloatJobOption(
            label="Hough length (A):",
            default_value=-1,
            help_text=(
                "This sets the length of the Hough transform, as described in"
                " Lovestam & Scheres, Faraday Discussions 2022. Useful values in"
                " our work on recombinant tau for the threshold range from −4 to"
                " −7. We typically do not change the default length of the Hough"
                " transform, which is set to be equal to the particle diameter"
                " when a negative value is given here. You can provide the"
                " additional option -fp to display images of intermediate steps"
                " of the algorithm to tune difficult cases."
            ),
            deactivate_if=JobOptionCondition([("do_topaz_filaments", "=", False)]),
            jobop_group="Helical processing options",
        )

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "shrink",
                "topaz_particle_diameter",
                "topaz_nr_particles",
                "topaz_model",
                "topaz_other_args",
                "do_topaz_filaments",
                "topaz_filament_threshold",
                "topaz_hough_length",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=True) -> List[PipelinerCommand]:
        return RelionAutoPickTopaz.get_commands(self, helical)

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        if (
            self.topaz != "relion_python_topaz"
            and self.joboptions["do_topaz_filaments"].get_boolean()
        ):
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["do_topaz_filaments"]],
                    message="This option is only available when using RELION 5",
                )
            )
        return errs


class RelionAutoPickTopazTrain(RelionAutoPickJob):

    PROCESS_NAME = AUTOPICK_TOPAZ_TRAIN_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME

        self.jobinfo.display_name = "RELION Topaz training"
        self.jobinfo.short_desc = "Training for reference-free autopicking using Topaz"

        self.topaz = ExternalProgram(get_topaz_executable())
        self.jobinfo.programs = [relion_program("relion_autopick"), self.topaz]
        self.jobinfo.references.append(
            Ref(
                authors=[
                    "Bepler T",
                    "Morin A",
                    "Rapp M",
                    "Brasch J",
                    "Shapiro L",
                    "Noble AJ",
                    "Berger B",
                ],
                title=(
                    "Positive-unlabeled convolutional neural networks for particle"
                    " picking in cryo-electron micrographs"
                ),
                journal="Nat Methods",
                year="2019",
                volume="16",
                issue="11",
                pages="1153-1160",
                doi="10.1038/s41592-019-0575-8.",
            )
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "This argument is not required. If left empty, the job itself will try"
                " to allocate available GPU resources. You can override the default"
                " allocation by providing a list of which GPUs (0,1,2,3, etc) to use."
                " MPI-processes are separated by ':'. For example: 0:1:0:1:0:1"
            ),
            in_continue=True,
        )

        self.joboptions["topaz_train_picks"] = InputNodeJobOption(
            label="Input picked coordinates for training:",
            node_type=NODE_MICROGRAPHCOORDSGROUP,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Input micrographs", EXT_COORDS),
            help_text=(
                "Input STAR file (preferably with CTF information) with all micrographs"
                " to pick from."
            ),
        )

        self.joboptions["do_topaz_train_parts"] = BooleanJobOption(
            label="OR train on a set of particles?",
            default_value=False,
            help_text=(
                "If set to Yes, the input Coordinates above will be ignored. Instead,"
                " one uses a _data.star file from a previous 2D or 3D refinement or"
                " selection to use those particle positions for training."
            ),
        )

        self.joboptions["topaz_train_parts"] = InputNodeJobOption(
            label="Particles STAR file for training:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Input STAR file", EXT_STARFILE),
            help_text=(
                "Filename of the STAR file with the particle coordinates to be used for"
                " training, e.g. from a previous 2D or 3D classification or selection."
            ),
        )

        self.joboptions["topaz_particle_diameter"] = FloatJobOption(
            label="Particle diameter (A)",
            default_value=-1,
            suggested_max=2000,
            step_value=20,
            help_text=(
                "Diameter of the particle (to be used to infer topaz downscale factor"
                " and particle radius)"
            ),
            is_required=True,
        )

        self.joboptions["topaz_nr_particles"] = IntJobOption(
            label="Nr of particles per micrograph:",
            default_value=-1,
            hard_min=0,
            suggested_max=2000,
            step_value=20,
            help_text="Expected average number of particles per micrograph",
            is_required=True,
        )

        self.joboptions["topaz_model"] = InputNodeJobOption(
            label="Trained topaz model:",
            node_type=NODE_MLMODEL,
            default_value="",
            pattern=files_exts("SAV Files", [".sav"]),
            help_text=(
                "Trained topaz model for topaz-based picking. Use on job for training"
                " and a next job for picking. Leave this empty to use the default"
                " (general) model."
            ),
            is_required=False,
            node_kwds=["topaz"],
        )

        self.joboptions["topaz_other_args"] = StringJobOption(
            label="Additional topaz arguments:",
            default_value="",
            help_text=(
                "These additional arguments will be passed onto all topaz programs."
            ),
        )

        del self.joboptions["use_gpu"]
        del self.joboptions["do_write_fom_maps"]
        del self.joboptions["do_read_fom_maps"]

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "shrink",
                "topaz_train_picks",
                "do_topaz_train_parts",
                "topaz_train_parts",
                "topaz_particle_diameter",
                "topaz_nr_particles",
                "topaz_model",
                "topaz_other_args",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        if (
            self.joboptions["do_topaz_train_parts"].get_boolean()
            and self.joboptions["topaz_train_parts"].get_string() == ""
        ):
            errs.append(
                JobOptionValidationResult(
                    "error",
                    [
                        self.joboptions["do_topaz_train_parts"],
                        self.joboptions["topaz_train_parts"],
                    ],
                    "Training on particles was selected, but no particle file "
                    "was provided",
                )
            )

        return errs

    def create_output_nodes(self) -> None:
        """Override to change the output nodes produced from this particular job."""
        if self.joboptions["do_topaz_train_parts"].get_boolean():
            # Output new version: no longer save coords_suffix node_type,
            # but 2-column list of micrographs and coordinate files
            self.add_output_node(
                "input_training_coords.star", NODE_MICROGRAPHCOORDSGROUP, ["relion"]
            )
        self.add_output_node("model_epoch10.sav", NODE_MLMODEL, ["topaz"])

    def get_commands(self) -> List[PipelinerCommand]:
        self.command_setup()
        if self.topaz.exe_path is None:
            raise ValueError("Topaz exe not found")
        if not self.topaz.exe_path.endswith("relion_python_topaz"):
            self.command += ["--topaz_exe", self.topaz.exe_path]

        topaz_nr_particles = self.joboptions["topaz_nr_particles"].get_number()
        if topaz_nr_particles > 0:
            self.command += [
                "--topaz_nr_particles",
                truncate_number(topaz_nr_particles, 0),
            ]

        topaz_part_diam = self.joboptions["topaz_particle_diameter"].get_number()
        if topaz_part_diam > 0:
            self.command += [
                "--particle_diameter",
                truncate_number(topaz_part_diam, 0),
            ]

        self.command.append("--topaz_train")

        if self.joboptions["do_topaz_train_parts"].get_boolean():
            train_parts = self.joboptions["topaz_train_parts"]
            train_parts_file = train_parts.get_string()
            self.command += ["--topaz_train_parts", train_parts_file]
        else:
            train_pick = self.joboptions["topaz_train_picks"]
            train_picks = train_pick.get_string()
            self.command += ["--topaz_train_picks", train_picks]

        topaz_model = self.joboptions["topaz_model"].get_string()
        if topaz_model:
            self.command += ["--topaz_model", topaz_model]

        topaz_other_args = self.joboptions["topaz_other_args"].get_string()
        if topaz_other_args:
            self.command += ["--topaz_args", topaz_other_args]

        # common options
        self.additional_common_options()
        self.read_write_options(topaz=True)

        return [PipelinerCommand(self.command, relion_control=True)]

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_topaz=True)
        topaz = self.topaz.exe_path if self.topaz.exe_path is not None else ""
        self.joboptions["fn_topaz_exec"] = ExternalFileJobOption(
            label="Topaz executable",
            default_value=topaz,
        )
        self.joboptions["fn_topaz_exec"].value = topaz

    def _get_model_epoch_number(self, sav_file) -> int:
        return int(sav_file.split(".")[0].replace(f"{self.output_dir}model_epoch", ""))

    def create_post_run_output_nodes(self) -> None:
        # add the final trained model as an output node
        onodes = glob(self.output_dir + "model_epoch*.sav")
        onodes.sort(key=self._get_model_epoch_number)
        final_trained_model = onodes[-1]
        self.output_nodes.append(
            create_node(final_trained_model, NODE_MLMODEL, ["topaz"])
        )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        return [
            create_results_display_object(
                dobj_type="text",
                title="Topaz training complete",
                display_data="Topaz training jobs have no graphical output",
                associated_data=[os.path.join(self.output_dir, "topaz_train.txt")],
            )
        ]

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """Return list of intermediate files/dirs to remove"""
        epochs = glob(self.output_dir + "model_epoch*.sav")
        epochs.sort(key=self._get_model_epoch_number)
        return epochs[:-1], []

    def gather_metadata(self) -> Dict[str, object]:
        # ToDo: Add some metadata stats - what is important?
        return {}


class RelionAutoPickRef2D(RelionAutoPickJob):

    PROCESS_NAME = AUTOPICK_REF2D_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME
        self.jobinfo.display_name = (
            "RELION AutoPick (2D reference-based, single particle)"
        )

        self.jobinfo.short_desc = "Autopicking with a set of 2-dimensional references"

        self.joboptions["fn_refs_autopick"] = InputNodeJobOption(
            label="2D references:",
            node_type=NODE_IMAGE2DSTACK,
            default_value="",
            pattern=files_exts("Input references", EXT_2DREFS),
            help_text=(
                "Input STAR file or MRC stack with the 2D references to be used for"
                " picking. Note that the absolute greyscale needs to be correct, so"
                " only use images created by RELION itself, e.g. by 2D class averaging"
                " or projecting a RELION reconstruction."
            ),
            is_required=True,
        )
        self.joboptions["do_invert_refs"] = BooleanJobOption(
            label="References have inverted contrast?",
            default_value=True,
            help_text=(
                "Set to Yes to indicate that the reference have inverted contrast with"
                " respect to the particles in the micrographs."
            ),
        )

        self.joboptions["do_ctf_autopick"] = BooleanJobOption(
            label="Are References CTF corrected?",
            default_value=True,
            help_text=(
                "Set to Yes if the references were created with CTF-correction inside"
                " RELION. \n \n If set to Yes, the input micrographs can only be given"
                " as a STAR file, which should contain the CTF information for each"
                " micrograph."
            ),
        )
        self.joboptions["do_ignore_first_ctfpeak_autopick"] = BooleanJobOption(
            label="Ignore CTFs until first peak?",
            default_value=False,
            help_text=(
                "Set this to Yes, only if this option was also used to generate the"
                " references."
            ),
        )

        self.joboptions["lowpass"] = FloatJobOption(
            label="Lowpass filter references (A)",
            default_value=20,
            suggested_min=10,
            suggested_max=100,
            step_value=5,
            help_text=(
                "Lowpass filter that will be applied to the references before template"
                " matching. Do NOT use very high-resolution templates  to search your"
                " micrographs. The signal will be too weak at high resolution anyway,"
                " and you may find Einstein from noise.... Give a negative value to"
                " skip the lowpass filter."
            ),
            is_required=True,
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="Highpass filter (A)",
            default_value=-1,
            suggested_min=100,
            suggested_max=1000,
            step_value=100,
            help_text=(
                "Highpass filter that will be applied to the micrographs.This may be"
                " useful to get rid of background ramps due to uneven ice"
                " distributions. Give a negative value to skip the high pass filter. "
                " Useful values are often in the range of 200-400 Angstroms."
            ),
            is_required=True,
        )

        self.joboptions["angpix_ref"] = FloatJobOption(
            label="Pixel size in references (A)",
            default_value=-1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Pixel size in Angstroms for the provided reference images. This will"
                " be used to calculate the filters and the particle diameter in pixels."
                " If a negative value is given here, the pixel size in the references"
                " will be assumed to be the same as the one in the micrographs, i.e."
                " the particles that were used to make the references were not rescaled"
                " upon extraction."
            ),
            is_required=True,
        )

        self.joboptions["psi_sampling_autopick"] = FloatJobOption(
            label="In-plane angular sampling (deg)",
            default_value=5,
            suggested_min=1,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Angular sampling in degrees for exhaustive searches of the in-plane"
                " rotations for all references."
            ),
            is_required=True,
            hard_min=0,
        )

        self.joboptions["threshold_autopick"] = FloatJobOption(
            label="Picking threshold:",
            default_value=0.05,
            suggested_min=0,
            suggested_max=1.0,
            step_value=0.01,
            help_text=(
                "Use lower thresholds to pick more particles (and more junk"
                " probably).\n\nThis option is ignored in the Laplacian-of-Gaussian"
                " picker. Please use 'Adjust default threshold' in the 'Laplacian' tab"
                " instead."
            ),
            in_continue=True,
            is_required=True,
            hard_min=0,
        )

        self.joboptions["mindist_autopick"] = FloatJobOption(
            label="Minimum inter-particle distance (A):",
            default_value=100,
            suggested_min=0,
            suggested_max=1000,
            step_value=20,
            help_text=(
                "Particles closer together than this distance will be consider to be a"
                " single cluster. From each cluster, only one particle will be picked."
                " \n\nThis option takes no effect for picking helical segments. The"
                " inter-box distance is calculated with the number of asymmetrical"
                " units and the helical rise on 'Helix' tab. This option is also"
                " ignored in the Laplacian-of-Gaussian picker. The inter-box distance"
                " is calculated from particle diameters."
            ),
            in_continue=True,
            is_required=True,
            hard_min=0,
        )

        self.joboptions["maxstddevnoise_autopick"] = FloatJobOption(
            label="Maximum stddev noise:",
            default_value=1.1,
            suggested_min=0.9,
            suggested_max=1.5,
            step_value=0.02,
            help_text=(
                "This is useful to prevent picking in carbon areas, or areas with big"
                " contamination features. Peaks in areas where the background standard"
                " deviation in the normalized micrographs is higher than this value"
                " will be ignored. Useful values are probably in the range 1.0 to 1.2."
                " Set to -1 to switch off the feature to eliminate peaks due to high"
                " background standard deviations.\n\nThis option is ignored in the"
                " Laplacian-of-Gaussian picker."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["minavgnoise_autopick"] = FloatJobOption(
            label="Minimum avg noise:",
            default_value=-999.0,
            suggested_min=-2,
            suggested_max=0.5,
            step_value=0.05,
            help_text=(
                "This is useful to prevent picking in carbon areas, or areas with big"
                " contamination features. Peaks in areas where the background standard"
                " deviation in the normalized micrographs is higher than this value"
                " will be ignored. Useful values are probably in the range -0.5 to 0."
                " Set to -999 to switch off the feature to eliminate peaks due to low"
                " average background densities.\n\nThis option is ignored in the"
                " Laplacian-of-Gaussian picker."
            ),
            in_continue=True,
            is_required=True,
        )

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "fn_refs_autopick",
                "angpix_ref",
                "shrink",
                "do_invert_refs",
                "do_ctf_autopick",
                "do_ignore_first_ctfpeak_autopick",
                "lowpass",
                "highpass",
                "psi_sampling_autopick",
                "threshold_autopick",
                "mindist_autopick",
                "maxstddevnoise_autopick",
                "minavgnoise_autopick",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=False) -> List[PipelinerCommand]:
        self.command_setup()
        fn_refs_autopick = self.joboptions["fn_refs_autopick"]
        refs_2d = fn_refs_autopick.get_string()
        self.command += ["--ref", refs_2d]

        if self.joboptions["do_invert_refs"].get_boolean():
            self.command.append("--invert")

        if self.joboptions["do_ctf_autopick"].get_boolean():
            self.command.append("--ctf")
            if self.joboptions["do_ignore_first_ctfpeak_autopick"].get_boolean():
                self.command.append("--ctf_intact_first_peak")

        # common options
        self.additional_common_options()

        # additional reference specific options
        minavgnoise = self.joboptions["minavgnoise_autopick"].get_number()
        if minavgnoise > -900:
            self.command += ["--min_avg_noise", str(minavgnoise)]

        self.command += ["--ang", self.joboptions["psi_sampling_autopick"].get_string()]

        self.command += ["--shrink", self.joboptions["shrink"].get_string()]

        lowpass = int(self.joboptions["lowpass"].get_number())
        if lowpass > 0:
            self.command += ["--lowpass", str(lowpass)]

        highpass = int(self.joboptions["highpass"].get_number())
        if highpass > 0:
            self.command += ["--highpass", str(highpass)]

        self.command += [
            "--angpix_ref",
            str(self.joboptions["angpix_ref"].get_number()),
        ]

        self.command += [
            "--threshold",
            self.joboptions["threshold_autopick"].get_string(),
        ]

        if not helical:
            mindist = self.joboptions["mindist_autopick"].get_string()
            self.command += ["--min_distance", mindist]

        max_stddev_noise = self.joboptions["maxstddevnoise_autopick"].get_string()
        self.command += ["--max_stddev_noise", max_stddev_noise]

        if helical:
            self.helical_options()

        self.read_write_options()

        return [PipelinerCommand(self.command, relion_control=True)]

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_refs=True)


class RelionAutoPickRef2DHelical(RelionAutoPickRef2D):

    PROCESS_NAME = AUTOPICK_REF2D_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.type = self.PROCESS_NAME
        self.jobinfo.display_name = "RELION AutoPick (2D reference-based, helical)"
        self.add_helical_options()

        self.set_joboption_order(
            [
                "fn_input_autopick",
                "angpix",
                "fn_refs_autopick",
                "angpix_ref",
                "shrink",
                "do_invert_refs",
                "do_ctf_autopick",
                "do_ignore_first_ctfpeak_autopick",
                "lowpass",
                "highpass",
                "psi_sampling_autopick",
                "threshold_autopick",
                "mindist_autopick",
                "maxstddevnoise_autopick",
                "minavgnoise_autopick",
                "do_amyloid",
                "helical_tube_outer_diameter",
                "helical_nr_asu",
                "helical_rise",
                "helical_tube_kappa_max",
                "helical_tube_length_min",
                "do_write_fom_maps",
                "do_read_fom_maps",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self, helical=True) -> List[PipelinerCommand]:
        return RelionAutoPickRef2D.get_commands(self, helical)
