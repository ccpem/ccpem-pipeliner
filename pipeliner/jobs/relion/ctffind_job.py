#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from numpy import mean, std
from typing import List, Dict, Sequence, Tuple, Union
from random import sample
from pathlib import Path

from pipeliner.jobs.relion.relion_job import relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner import user_settings
from .relion_job import RelionJob
from pipeliner.job_options import (
    ExternalFileJobOption,
    InputNodeJobOption,
    StringJobOption,
    files_exts,
    EXT_STARFILE,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.data_structure import (
    CTFFIND_GCTF_NAME,
    CTFFIND_CTFFIND4_NAME,
    CTFFIND_DIR,
    MOTIONCORR_OWN_NAME,
    TRUES,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_LOGFILE,
)
from pipeliner.pipeliner_job import Ref, ExternalProgram
from pipeliner.starfile_handler import DataStarFile
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_mics,
    EmpiarCorrectedMics,
)
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmSoftware,
    EmCtfCorrection,
)
from pipeliner.utils import decompose_pipeline_filename, get_job_script
from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_many_files,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.starfile_handler import JobStar


def make_input_star_mics_jo() -> InputNodeJobOption:
    return InputNodeJobOption(
        label="Input micrographs STAR file:",
        node_type=NODE_MICROGRAPHGROUPMETADATA,
        node_kwds=["relion"],
        default_value="",
        pattern=files_exts("STAR files", EXT_STARFILE),
        help_text="A STAR file with all micrographs to run CTFFIND or Gctf on",
        is_required=True,
    )


def make_no_dw_jo() -> BooleanJobOption:
    return BooleanJobOption(
        label="Use micrograph without dose-weighting?",
        default_value=False,
        help_text=(
            "If set to Yes, the CTF estimation will be done using the micrograph"
            " without dose-weighting as in rlnMicrographNameNoDW (_noDW.mrc from"
            " MotionCor2). If set to No, the normal rlnMicrographName will be used."
        ),
    )


class RelionCtfFindJob(RelionJob):

    OUT_DIR = CTFFIND_DIR
    CATEGORY_LABEL = "CTF Determination"

    def __init__(self) -> None:
        super().__init__()
        self.always_continue_in_schedule = True
        self.jobinfo.programs = [relion_program("relion_run_ctffind")]
        self.jobinfo.long_desc = (
            "We now prefer ctffind 4.1, as it is the only open-source option, and "
            "because it allows reading in the movie-averaged power spectra calculation"
            " by Relion’s own implementation of the motioncor2 algorithm."
        )

        self.joboptions["do_phaseshift"] = BooleanJobOption(
            label="Estimate phase shifts?",
            default_value=False,
            help_text=(
                "If set to Yes, CTFFIND4 will estimate the phase shift, e.g. as"
                " introduced by a Volta phase-plate"
            ),
        )

        self.joboptions["phase_min"] = FloatJobOption(
            label="Phase shift (deg) - Min:",
            default_value=0,
            help_text=(
                "Minimum, maximum and step size (in degrees) for the search of the"
                " phase shift"
            ),
            deactivate_if=JobOptionCondition([("do_phaseshift", "=", False)]),
        )

        self.joboptions["phase_max"] = FloatJobOption(
            label="Phase shift (deg) - Max:",
            default_value=180,
            help_text=(
                "Minimum, maximum and step size (in degrees) for the search of the"
                " phase shift"
            ),
            deactivate_if=JobOptionCondition([("do_phaseshift", "=", False)]),
        )

        self.joboptions["phase_step"] = FloatJobOption(
            label="Phase shift (deg) - Step:",
            default_value=10,
            help_text=(
                "Minimum, maximum and step size (in degrees) for the search of the"
                " phase shift"
            ),
            deactivate_if=JobOptionCondition([("do_phaseshift", "=", False)]),
        )

        self.joboptions["dast"] = FloatJobOption(
            label="Amount of astigmatism (A):",
            default_value=100,
            suggested_min=0,
            suggested_max=2000,
            step_value=100,
            help_text="CTFFIND's dAst parameter, GCTFs -astm parameter",
        )

        self.joboptions["box"] = IntJobOption(
            label="FFT box size (pix):",
            default_value=512,
            hard_min=2,
            suggested_min=64,
            suggested_max=1024,
            step_value=8,
            help_text="CTFFIND's Box parameter",
        )
        self.joboptions["resmin"] = FloatJobOption(
            label="Minimum resolution (A):",
            default_value=30,
            suggested_min=10,
            suggested_max=200,
            step_value=10,
            help_text="CTFFIND's ResMin parameter",
        )

        self.joboptions["resmax"] = FloatJobOption(
            label="Maximum resolution (A):",
            default_value=5,
            suggested_min=1,
            suggested_max=20,
            step_value=1,
            help_text="CTFFIND's ResMax parameter",
        )

        self.joboptions["dfmin"] = FloatJobOption(
            label="Minimum defocus value (A):",
            default_value=5000,
            suggested_min=0,
            suggested_max=25000,
            step_value=1000,
            help_text="CTFFIND's dFMin parameter",
        )

        self.joboptions["dfmax"] = FloatJobOption(
            label="Maximum defocus value (A):",
            default_value=50000,
            suggested_min=20000,
            suggested_max=100000,
            step_value=1000,
            help_text="CTFFIND's dFMax parameter",
        )

        self.joboptions["dfstep"] = FloatJobOption(
            label="Defocus step size (A):",
            default_value=500,
            suggested_min=200,
            suggested_max=2000,
            step_value=100,
            help_text="CTFFIND's FStep parameter",
        )

        self.joboptions["ctf_win"] = IntJobOption(
            label="Estimate CTF on window size (pix)",
            default_value=-1,
            suggested_min=16,
            suggested_max=4096,
            step_value=16,
            help_text=(
                "If a positive value is given, a squared window of this size at the"
                " center of the micrograph will be used to estimate the CTF. This may"
                " be useful to exclude parts of the micrograph that are unsuitable for"
                " CTF estimation, e.g. the labels at the edge of photographic film. \n"
                " \n The original micrograph will be used (i.e. this option will be"
                " ignored) if a negative value is given."
            ),
        )

        self.joboptions["validate_output"] = BooleanJobOption(
            label="Validate output files?",
            default_value=True,
            help_text=(
                "If this option is selected the job will fail if the output file is"
                " empty.  This option can be turned off for on-the-fly processing"
                " but should be used in most other situations"
            ),
            jobop_group="Running options",
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def relion_back_compatibility_joboptions(
        self, use_gctf=False, use_ctffind4=False
    ) -> None:
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["use_gctf"] = BooleanJobOption(
            label="Use Gctf instead?",
            default_value=use_gctf,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["use_ctffind4"] = BooleanJobOption(
            label="Use CTFFIND-4.1?",
            default_value=use_ctffind4,
            jobop_group="Relion compatibility options",
        )

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "micrographs_ctf.star", NODE_MICROGRAPHGROUPMETADATA, ["relion", "ctf"]
        )
        self.add_output_node("logfile.pdf", NODE_LOGFILE, ["relion", "ctffind"])

    def output_validation_command(self):
        return [
            "python3",
            get_job_script("check_star_file.py"),
            "--fn_in",
            str(Path(self.output_dir) / "micrographs_ctf.star"),
            "--block",
            "micrographs",
            "--clear_relion_success_file",
        ]

    def common_commands(self) -> None:

        input_star_mics = self.joboptions["input_star_mics"].get_string()
        self.command = self.get_relion_command("run_ctffind")

        self.command += [
            "--i",
            input_star_mics,
            "--o",
            self.output_dir,
            "--Box",
            self.joboptions["box"].get_string(),
            "--ResMin",
            self.joboptions["resmin"].get_string(),
            "--ResMax",
            self.joboptions["resmax"].get_string(),
            "--dFMin",
            self.joboptions["dfmin"].get_string(),
            "--dFMax",
            self.joboptions["dfmax"].get_string(),
            "--FStep",
            self.joboptions["dfstep"].get_string(),
            "--dAst",
            self.joboptions["dast"].get_string(),
        ]

        use_nodw = self.joboptions["use_noDW"].get_boolean()
        if use_nodw:
            self.command.append("--use_noDW")

        do_phaseshift = self.joboptions["do_phaseshift"].get_boolean()
        if do_phaseshift:
            self.command.append("--do_phaseshift")
            self.command += ["--phase_min", self.joboptions["phase_min"].get_string()]
            self.command += ["--phase_max", self.joboptions["phase_max"].get_string()]
            self.command += [
                "--phase_step ",
                self.joboptions["phase_step"].get_string(),
            ]

    def final_common_commands(self) -> None:
        if self.is_continue:
            self.command.append("--only_do_unfinished")

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """Return list of intermediate files/dirs to remove"""

        # get all of the subdirs
        subdirs = self.get_job_subdirs()
        del_files, del_dirs = [], []
        exts = ["gctf*.out", "gctf*.err"]
        for ext in exts:
            del_files += glob(self.output_dir + ext)
        if do_harsh:
            del_dirs.extend(subdirs)
        return del_files, del_dirs

    def gather_metadata(self) -> Dict[str, object]:
        outfile = DataStarFile(os.path.join(self.output_dir, "micrographs_ctf.star"))
        coords_block = outfile.get_block("micrographs")
        columns = [
            "_rlnDefocusU",
            "_rlnDefocusV",
            "_rlnCtfAstigmatism",
            "_rlnCtfFigureOfMerit",
        ]
        coords_table = coords_block.find(columns)
        defoci = [(float(x[0]) + float(x[1])) / 2.0 for x in coords_table]
        astigs = [float(x[2]) for x in coords_table]
        foms = [float(x[3]) for x in coords_table]

        metadata_dict = {
            "MicrographCount": len(defoci),
            "DefocusMean": mean(defoci),
            "DefocusMin": min(defoci),
            "DefocusMax": max(defoci),
            "DefocusStd": std(defoci),
            "AstigMean": mean(astigs),
            "AstigMin": min(astigs),
            "AstigMax": max(astigs),
            "AstigStd": std(astigs),
            "FomMean": mean(foms),
            "FomMin": min(foms),
            "FomMax": max(foms),
            "FomStd": std(foms),
            "LogFiles": [os.path.join(self.output_dir, "logfile.pdf")],
        }

        return metadata_dict

    # needs to return an EMPIAR CorrectedMicsType and OneDep CtfFindType
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarCorrectedMics, EmSoftware, EmCtfCorrection]]:
        if depo_type == "EMPIAR":
            # generate the EMPIAR DepoObj
            outfile = os.path.join(self.output_dir, "micrographs_ctf.star")
            return prepare_empiar_mics(outfile)

        elif depo_type == "ONEDEP":
            mocorr = os.path.dirname(self.joboptions["input_star_mics"].get_string())
            software = EmSoftware(
                category="CTF CORRECTION",
                name=self.jobinfo.programs[-1].name,
                version=self.jobinfo.programs[-1].get_version(),
            )
            ctf = EmCtfCorrection(
                image_processing_id=f"JOBREF: {mocorr}/",
                type="PHASE FLIPPING AND AMPLITUDE CORRECTION",
            )
            return [ctf, software]
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )


class RelionCtfFindCtffind4(RelionCtfFindJob):
    PROCESS_NAME = CTFFIND_CTFFIND4_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION CTF estimation (CTFFIND4)"
        self.jobinfo.programs.append(
            ExternalProgram(
                command=user_settings.get_ctffind_executable(),
                name="ctffind4",
            )
        )
        self.jobinfo.short_desc = (
            "Estimate micrograph contrast transfer functions using CTFFIND 4.1"
        )
        self.jobinfo.documentation = "https://grigoriefflab.umassmed.edu/ctffind4"
        self.jobinfo.references.append(
            Ref(
                authors=["Rouhou A", "Grigorieff N"],
                title=(
                    "CTFFIND4: Fast and accurate defocus estimation from electron"
                    " micrographs"
                ),
                journal="J Struct Biol",
                year="2015",
                volume="192",
                issue="2",
                pages="216-21",
                doi="10.1016/j.jsb.2015.08.008",
            )
        )
        self.availability_checks = os.path.isfile(
            user_settings.get_ctffind_executable()
        )
        self.joboptions["input_star_mics"] = make_input_star_mics_jo()
        self.joboptions["use_noDW"] = make_no_dw_jo()

        self.joboptions["use_given_ps"] = BooleanJobOption(
            label="Use power spectra from MotionCorr job?",
            default_value=False,
            help_text=(
                "If set to Yes, the CTF estimation will be done using power spectra"
                " calculated during motion correction."
            ),
        )

        self.joboptions["slow_search"] = BooleanJobOption(
            label="Use exhaustive search?",
            default_value=False,
            help_text=(
                "If set to Yes, CTFFIND4 will use slower but more exhaustive search."
                " This option is recommended for CTFFIND version 4.1.8 and earlier, but"
                " probably not necessary for 4.1.10 and later. It is also worth trying"
                " this option when astigmatism and/or phase shifts are difficult to"
                " fit."
            ),
        )

        self.set_joboption_order(
            [
                "input_star_mics",
                "use_noDW",
                "do_phaseshift",
                "phase_min",
                "phase_max",
                "phase_step",
                "dast",
                "box",
                "resmin",
                "resmax",
                "dfmin",
                "dfmax",
                "dfstep",
                "ctf_win",
                "use_given_ps",
                "slow_search",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def input_is_float16(self) -> bool:
        """Attempt to determine if the input file contains float16 images

        Returns:
            bool: If the images are float16.  Also returns False if it cannot be
                determined
        """
        infile = Path(self.joboptions["input_star_mics"].get_string())
        try:
            jobstar = JobStar(str(infile.parent / "job.star"))
            jobops = jobstar.joboptions
            if jobops["do_float16"].lower() in TRUES:
                return True
            else:
                return False

        except Exception:
            return False

    def power_spectra_exist(self):
        """Attempt to determine if the power spectra were written by the motioncorr job

        Returns:
            bool: If the power spectra were (or are going to be) written. Also returns
                False if it cannot be determined
        """
        infile = Path(self.joboptions["input_star_mics"].get_string())
        try:
            jobstar = JobStar(str(infile.parent / "job.star"))
            jobops = jobstar.joboptions
            if (
                jobstar.jobtype == MOTIONCORR_OWN_NAME
                and jobops["do_save_ps"].lower() in TRUES
            ):
                return True
            else:
                return False

        except Exception:
            return False

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        valobjs = []
        if (
            self.input_is_float16()
            and not self.joboptions["use_given_ps"].get_boolean()
        ):
            valobjs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["use_given_ps"]],
                    message=(
                        "Input micrographs appear to be in float16, "
                        "pre-calculated power spectra should be used"
                    ),
                ),
            )

        if (
            self.joboptions["use_given_ps"].get_boolean()
            and not self.power_spectra_exist()
        ):
            valobjs.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["use_given_ps"]],
                    message=(
                        "It does not appear the job that created the input file "
                        "wrote the necessary power spectra files to use this option"
                    ),
                ),
            )

        return valobjs

    def get_commands(self) -> List[PipelinerCommand]:

        self.common_commands()

        self.command += [
            "--ctffind_exe",
            user_settings.get_ctffind_executable(),
        ]
        self.command += ["--ctfWin", self.joboptions["ctf_win"].get_string()]
        self.command.append("--is_ctffind4")
        slow_search = self.joboptions["slow_search"].get_boolean()
        if not slow_search:
            self.command.append("--fast_search")
        use_given_ps = self.joboptions["use_given_ps"].get_boolean()
        if use_given_ps:
            self.command.append("--use_given_ps")

        self.final_common_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        if self.joboptions["validate_output"].get_boolean():
            commands.append(PipelinerCommand(self.output_validation_command()))

        return commands

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(use_ctffind4=True)

        default_ctffind_location = user_settings.get_ctffind_executable()
        self.joboptions["fn_ctffind_exe"] = ExternalFileJobOption(
            label="CTFFIND-4.1 executable:",
            help_text=(
                "Location of the CTFFIND (release 4.1 or later) executable. You can"
                " control the default of this field by setting environment variable"
                " PIPELINER_CTFFIND_EXECUTABLE"
            ),
            default_value=default_ctffind_location,
            is_required=True,
        )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # read the log files this is done rather than the star file so it can
        # be done on the fly rather than waiting for the star to be written

        instar = DataStarFile(self.joboptions["input_star_mics"].get_string())
        mdat = instar.get_block("micrographs").find(["_rlnMicrographName"])[0][0]
        logsdir = os.path.join(
            self.output_dir, os.path.dirname(decompose_pipeline_filename(mdat)[2])
        )
        logfiles = glob(logsdir + "/*.log")
        if not len(logfiles):
            return [
                create_results_display_object("pending", reason="No log files found")
            ]
        dfs, dfs1, dfs2, astigs, resos = [], [], [], [], []
        for f in logfiles:
            with open(f, "r") as lf:
                logdat = lf.readlines()
            for ll in logdat:
                if ll.startswith("Estimated defocus"):
                    df1 = float(ll.split()[-2])
                    df2 = float(ll.split()[-4])
                    df = (df1 + df2) / 20000
                    dfs1.append(df1 / 10000)
                    dfs2.append(df2 / 10000)
                    dfs.append(df)
                elif ll.startswith("Estimated azimuth"):
                    astigs.append(float(ll.split()[-2]))
                elif ll.startswith("Thon rings"):
                    resos.append(float(ll.split()[-2]))
                    continue

        spectra_files = glob(f"{logsdir}/*.ctf")
        if len(spectra_files) == 0:
            ps_imgs = create_results_display_object(
                "text",
                title="No power spectra image preview",
                display_data=(
                    "There are no power spectra images to display, usually this is "
                    "because the job has been cleaned up and the files deleted"
                ),
                associated_data=[],
            )
        else:
            n_samp = 4 if len(spectra_files) >= 4 else len(spectra_files)
            ps_imgs = mini_montage_from_many_files(
                filelist=sample(spectra_files, n_samp),
                outputdir=self.output_dir,
                nimg=4,
                title="Example power spectra",
            )

        defocus_scatter = create_results_display_object(
            "graph",
            xvalues=[dfs1],
            yvalues=[dfs2],
            data_series_labels=["All micrographs"],
            xaxis_label="Defocus x (μm)",
            yaxis_label="Defocus y (μm)",
            title="Per-micrograph defocus",
            associated_data=[os.path.join(self.output_dir, "micrographs_ctf.star")],
            modes=["markers"],
        )

        defocus = create_results_display_object(
            "histogram",
            title="Defocus",
            xlabel="Defocus (uM)",
            ylabel="Micrographs",
            data_to_bin=dfs,
            associated_data=[os.path.join(self.output_dir, "micrographs_ctf.star")],
        )

        astigmatism = create_results_display_object(
            "histogram",
            title="Astigmatism",
            xlabel="Astigmatism (Angstrom)",
            ylabel="Micrographs",
            data_to_bin=astigs,
            associated_data=[os.path.join(self.output_dir, "micrographs_ctf.star")],
        )

        estres = create_results_display_object(
            "histogram",
            title="Resolution of good Thon ring fit",
            xlabel="Resolution (Angstrom)",
            ylabel="Micrographs",
            data_to_bin=resos,
            associated_data=[os.path.join(self.output_dir, "micrographs_ctf.star")],
        )

        return [ps_imgs, defocus_scatter, defocus, astigmatism, estres]


class RelionCtfFindGctf(RelionCtfFindJob):
    PROCESS_NAME = CTFFIND_GCTF_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION CTF estimation (Gctf)"
        self.jobinfo.programs.append(
            ExternalProgram(command=user_settings.get_gctf_executable(), name="gctf")
        )
        self.jobinfo.short_desc = (
            "Estimate micrograph contrast transfer functions using Gctf"
        )
        self.jobinfo.documentation = "https://grigoriefflab.umassmed.edu/ctffind4"
        self.jobinfo.references.append(
            Ref(
                authors=["Zhang K"],
                title="Gctf: Real-time CTF determination and correction",
                journal="J Struct Biol",
                year="2016",
                volume="193",
                issue="1",
                pages="1-12",
                doi="10.1016/j.jsb.2015.11.003",
            )
        )
        self.jobinfo.alternative_unavailable_reason = "Gctf is no longer supported"
        self.availability_checks = os.path.isfile(user_settings.get_gctf_executable())

        self.joboptions["input_star_mics"] = make_input_star_mics_jo()
        self.joboptions["use_noDW"] = make_no_dw_jo()

        self.joboptions["do_ignore_ctffind_params"] = BooleanJobOption(
            label="Ignore 'Searches' parameters?",
            default_value=True,
            help_text=(
                "If set to Yes, all parameters EXCEPT for phase shift search and its"
                " ranges on the 'Searches' tab will be ignored, and Gctf's default"
                " parameters will be used (box.size=1024; min.resol=50; max.resol=4;"
                " min.defocus=500; max.defocus=90000; step.defocus=500; astigm=1000) \n"
                " \nIf set to No, all parameters on the CTFFIND tab will be passed to"
                " Gctf."
            ),
        )

        self.joboptions["dast"] = FloatJobOption(
            label="Amount of astigmatism (A):",
            default_value=100,
            suggested_min=0,
            suggested_max=2000,
            step_value=100,
            help_text="CTFFIND's dAst parameter, GCTFs -astm parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["box"] = IntJobOption(
            label="FFT box size (pix):",
            default_value=512,
            hard_min=2,
            suggested_min=64,
            suggested_max=1024,
            step_value=8,
            help_text="CTFFIND's Box parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
        )
        self.joboptions["resmin"] = FloatJobOption(
            label="Minimum resolution (A):",
            default_value=30,
            suggested_min=10,
            suggested_max=200,
            step_value=10,
            help_text="CTFFIND's ResMin parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["resmax"] = FloatJobOption(
            label="Maximum resolution (A):",
            default_value=5,
            suggested_min=1,
            suggested_max=20,
            step_value=1,
            help_text="CTFFIND's ResMax parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["dfmin"] = FloatJobOption(
            label="Minimum defocus value (A):",
            default_value=5000,
            suggested_min=0,
            suggested_max=25000,
            step_value=1000,
            help_text="CTFFIND's dFMin parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["dfmax"] = FloatJobOption(
            label="Maximum defocus value (A):",
            default_value=50000,
            suggested_min=20000,
            suggested_max=100000,
            step_value=1000,
            help_text="CTFFIND's dFMax parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["dfstep"] = FloatJobOption(
            label="Defocus step size (A):",
            default_value=500,
            suggested_min=200,
            suggested_max=2000,
            step_value=100,
            help_text="CTFFIND's FStep parameter",
            deactivate_if=JobOptionCondition([("do_ignore_ctffind_params", "=", True)]),
            required_if=JobOptionCondition([("do_ignore_ctffind_params", "=", False)]),
        )

        self.joboptions["do_EPA"] = BooleanJobOption(
            label="Perform equi-phase averaging?",
            default_value=False,
            help_text=(
                "If set to Yes, equi-phase averaging is used in the defocus refinement,"
                " otherwise basic rotational averaging will be performed."
            ),
        )

        self.joboptions["other_gctf_args"] = StringJobOption(
            label="Other Gctf options:",
            default_value="",
            help_text="Provide additional gctf options here.",
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "This argument is not necessary. If left empty, the job itself will try"
                " to allocate available GPU resources.You can override  the default"
                " allocation by providing a list of which GPUs (0,1,2,3, etc) to use."
                " MPI-processes are separated by ':', threads by ','. "
            ),
        )

        self.set_joboption_order(
            [
                "input_star_mics",
                "use_noDW",
                "do_phaseshift",
                "phase_min",
                "phase_max",
                "phase_step",
                "do_ignore_ctffind_params",
                "dast",
                "box",
                "resmin",
                "resmax",
                "dfmin",
                "dfmax",
                "dfstep",
                "ctf_win",
                "do_EPA",
                "other_gctf_args",
                "gpu_ids",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self) -> List[PipelinerCommand]:

        self.common_commands()
        self.command += [
            "--use_gctf",
            "True",
            "--gctf_exe",
            user_settings.get_gctf_executable(),
        ]

        do_ignore_params = self.joboptions["do_ignore_ctffind_params"].get_boolean()
        if do_ignore_params:
            self.command.append("--ignore_ctffind_params")

        do_epa = self.joboptions["do_EPA"].get_boolean()
        if do_epa:
            self.command.append("--EPA")

        self.command += ["--gpu", self.joboptions["gpu_ids"].get_string()]

        other_gctf_args = self.joboptions["other_gctf_args"].get_string()

        if len(other_gctf_args) > 0:
            self.command += ["--extra_gctf_options", '"{}"'.format(other_gctf_args)]
        self.final_common_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        if self.joboptions["validate_output"].get_boolean():
            commands.append(PipelinerCommand(self.output_validation_command()))
        return commands

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        other_gctf_args = self.joboptions["other_gctf_args"].get_string()
        badcoms = ("--phase_shift_H", "--phase_shift_H", "--phase_shift_H")
        if any(badcom in other_gctf_args for badcom in badcoms):
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["other_gctf_args"]],
                    message=(
                        "Don't specify --phase_shift_L, H, or S. Use 'Estimate phase "
                        "shifts' and 'Phase shift - Min, Max, Step' instead."
                    ),
                )
            ]
        return []

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(use_gctf=True)
