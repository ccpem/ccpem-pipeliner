#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionFilterMap(RelionJob):
    OUT_DIR = "FilterMap"
    PROCESS_NAME = "relion.map_utilities.filter_map"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Filter map"
        self.jobinfo.short_desc = "Apply low-pass and/or high-pass filters to a map"

        self.jobinfo.long_desc = "Apply low-pass or high pass filters to a map"

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to filter",
            is_required=True,
            default_value="",
        )

        self.joboptions["do_lowpass"] = BooleanJobOption(
            label="Low pass filter the map?",
            default_value=False,
            help_text="Should a low-pass filter be applied to the map",
        )

        self.joboptions["lowpass"] = FloatJobOption(
            label="Low pass filter resolution (Å)",
            default_value=1,
            hard_min=0.1,
            suggested_min=0.5,
            deactivate_if=JobOptionCondition([("do_lowpass", "=", False)]),
            required_if=JobOptionCondition([("do_lowpass", "=", True)]),
            help_text=(
                "Resolution of low-pass filter.  Resolutions lower (worse) than this "
                "filter are retained"
            ),
        )

        self.joboptions["do_highpass"] = BooleanJobOption(
            label="High pass filter the map?",
            default_value=False,
            help_text="Should a high-pass filter be applied to the map",
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="High pass filter resolution (Å)",
            default_value=1,
            hard_min=0.1,
            suggested_min=0.5,
            deactivate_if=JobOptionCondition([("do_highpass", "=", False)]),
            required_if=JobOptionCondition([("do_highpass", "=", True)]),
            help_text=(
                "Resolution of high-pass filter.  Resolutions higher (better) than "
                "this value are retained"
            ),
        )

        self.joboptions["do_advanced"] = BooleanJobOption(
            label="Use advanced options?",
            default_value=False,
            help_text="Advanced filtering options",
            jobop_group="Advanced options",
        )

        self.joboptions["filter_direction"] = MultipleChoiceJobOption(
            label="Low-pass filter directionality",
            choices=["non-directional", "X", "Y", "Z"],
            default_value_index=0,
            help_text="Useful for maps with aniostropic resolution",
            deactivate_if=JobOptionCondition(
                [("do_lowpass", "=", False), ("do_advanced", "=", False)],
                operation="ANY",
            ),
            jobop_group="Advanced options",
        )

        self.joboptions["filter_edge_width"] = IntJobOption(
            label="Filter edge width (resolution shells)",
            hard_min=1,
            default_value=2,
            deactivate_if=JobOptionCondition([("do_advanced", "=", False)]),
            required_if=JobOptionCondition([("do_advanced", "=", True)]),
            help_text=(
                "Width of the raised cosine on the low/high-pass filter edge "
                "(in resolution shells)"
            ),
            jobop_group="Advanced options",
        )

    @staticmethod
    def get_outfile_name(map_name: str) -> str:
        return os.path.splitext(map_name)[0] + "_filt.mrc"

    def create_output_nodes(self) -> None:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            outfile_name = self.get_outfile_name(out_map)
            self.add_output_node(outfile_name, NODE_DENSITYMAP, ["filtered"])

    def make_command(self, inmap: str, outname: str) -> PipelinerCommand:
        do_hp = self.joboptions["do_highpass"].get_boolean()
        do_lp = self.joboptions["do_lowpass"].get_boolean()
        lp_val = self.joboptions["lowpass"].get_number() if do_lp else -1
        hp_val = self.joboptions["highpass"].get_number() if do_hp else 0

        if Path(inmap).suffix != ".mrc":
            inmap += ":mrc"

        command = ["relion_image_handler", "--i", inmap]

        if do_lp:
            command.extend(["--lowpass", str(lp_val)])

        if do_hp:
            command.extend(["--highpass", str(hp_val)])

        if self.joboptions["do_advanced"].get_boolean():
            edge_width = self.joboptions["filter_edge_width"].get_number()
            direction = self.joboptions["filter_direction"].get_string()
            adv = ["--filter_edge_width", str(edge_width)]
            direc = (
                [] if direction == "non-directional" else ["--directional", direction]
            )
            command.extend(adv + direc)

        outfile_name = self.get_outfile_name(outname)
        command.extend(["--o", os.path.join(self.output_dir, outfile_name)])

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands: List[PipelinerCommand] = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for map_pair in map_names.items():
            commands.append(self.make_command(*map_pair))
        return commands

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        do_hp = self.joboptions["do_highpass"].get_boolean()
        do_lp = self.joboptions["do_lowpass"].get_boolean()
        lp_val = self.joboptions["lowpass"].get_number() if do_lp else -1
        hp_val = self.joboptions["highpass"].get_number() if do_hp else 0

        if not do_hp and not do_lp:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["do_highpass"],
                        self.joboptions["do_lowpass"],
                    ],
                    message="Must select at least one filtering type",
                )
            )
        if lp_val == hp_val:
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["do_highpass"],
                        self.joboptions["do_lowpass"],
                    ],
                    message=(
                        "High-pass and low-pass values are identical, no filtering "
                        "will occur"
                    ),
                )
            )

        return errs

    def get_title(self, outname: str) -> str:
        do_hp = self.joboptions["do_highpass"].get_boolean()
        do_lp = self.joboptions["do_lowpass"].get_boolean()
        lp_val = self.joboptions["lowpass"].get_number()
        hp_val = self.joboptions["highpass"].get_number()
        title = outname
        title = title + f"; Low-pass filter {lp_val} Å" if do_lp else title
        title = title + f"; High-pass filter {hp_val} Å" if do_hp else title
        return title

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        outfiles = {}
        for of in map_names.values():
            outfile_name = self.get_outfile_name(of)
            outfiles[os.path.join(self.output_dir, outfile_name)] = self.get_title(
                outfile_name
            )
        return make_maps_slice_montage_and_3d_display(
            in_maps=outfiles, output_dir=self.output_dir, combine_montages=False
        )
