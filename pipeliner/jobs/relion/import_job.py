#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Tuple, Dict, Sequence, Union
from glob import glob
from pipeliner import __version__ as pipe_vers
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    StringJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    MultiStringJobOption,
    JobOptionValidationResult,
    JobOptionCondition,
    SearchStringJobOption,
    MultiExternalFileJobOption,
    InputNodeJobOption,
)
from pipeliner.data_structure import (
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_NEWNODETYPE,
    NODE_DENSITYMAP,
    IMPORT_DIR,
    IMPORT_MOVIES_NAME,
    IMPORT_OTHER_NAME,
)
from pipeliner.node_factory import all_node_toplevel_types
from pipeliner.starfile_handler import DataStarFile
from pipeliner.job_options import files_exts
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_raw_mics,
    EmpiarMovieSet,
)
from pipeliner.display_tools import (
    mini_montage_from_starfile,
    create_results_display_object,
    make_particle_coords_thumb,
    make_maps_slice_montage_and_3d_display,
)
from pipeliner.scripts.job_scripts.microscope_data_parsers import (
    check_file_is_ebic_csv,
    parse_ebic_csv,
)
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmSoftware,
    EmSpecimen,
    EmImaging,
    EmSampleSupport,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


RELION_NODE_TYPES = [
    "RELION coordinates STAR file (.star)",
    "Particles STAR file (.star)",
    "2D references STAR file (.star)",
    "Micrographs STAR file (.star)",
]


class RelionImportMovies(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_MOVIES_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_import")]
        self.jobinfo.display_name = "Import movies or raw micrographs"
        self.jobinfo.short_desc = "Import movies or single-frame micrographs"

        self.jobinfo.long_desc = (
            "Import raw micrographs and create a star file containing the collected"
            " micrographs and their metadata"
        )

        self.joboptions["fn_in_raw"] = SearchStringJobOption(
            label="Raw input files:",
            default_value="Movies/*.tif",
            help_text=(
                "Provide a Linux wildcard that selects all raw movies or micrographs to"
                " be imported."
            ),
            must_be_in_project=True,
            in_continue=True,
            is_required=True,
        )
        self.joboptions["is_multiframe"] = BooleanJobOption(
            label="Are these multi-frame movies?",
            default_value=True,
            help_text=(
                "Set to Yes for multi-frame movies, set to No for single-frame"
                " micrographs."
            ),
            in_continue=True,
        )
        self.joboptions["optics_group_name"] = StringJobOption(
            label="Optics group name:",
            default_value="opticsGroup1",
            help_text=(
                "Name of this optics group. Each group of movies/micrographs with"
                " different optics characteristics for CTF refinement should have a"
                " unique name."
            ),
            in_continue=True,
            is_required=True,
            validation_regex="^[a-zA-Z0-9_-]+$",
            regex_error_message=(
                "Can only contain alphanumeric characters underscores and hyphens"
            ),
        )
        self.joboptions["fn_mtf"] = InputNodeJobOption(
            label="MTF of the detector:",
            default_value="",
            pattern=files_exts("STAR Files", [".star"]),
            help_text=(
                "As of release-3.1, the MTF of the detector is used in the refinement"
                " stages of refinement. If you know the MTF of your detector, provide"
                " it here. Curves for some well-known detectors may be downloaded from"
                " the RELION Wiki. Also see there for the exact format \n If you do not"
                " know the MTF of your detector and do not want to measure it, then by"
                " leaving this entry empty, you include the MTF of your detector in"
                " your overall estimated B-factor upon sharpening the map. Although"
                " that is probably slightly less accurate, the overall quality of your"
                " map will probably not suffer very much. \n \n Note that when"
                " combining data from different detectors, the differences between"
                " their MTFs can no longer be absorbed in a single B-factor, and"
                " providing the MTF here is important!"
            ),
            in_continue=True,
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["mtf"],
        )
        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size (Angstrom):",
            default_value=1.4,
            suggested_min=0.5,
            suggested_max=3,
            step_value=0.1,
            help_text="Pixel size in Angstroms. ",
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["kV"] = IntJobOption(
            label="Voltage (kV):",
            default_value=300,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text="Voltage the microscope was operated on (in kV)",
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Cs"] = FloatJobOption(
            label="Spherical aberration (mm):",
            default_value=2.7,
            suggested_min=0,
            suggested_max=8,
            step_value=0.1,
            help_text=(
                "Spherical aberration of the microscope used to collect these images"
                " (in mm). Typical values are 2.7 (FEI Titan & Talos, most JEOL"
                " CRYO-ARM), 2.0 (FEI Polara), 1.4 (some JEOL CRYO-ARM) and 0.01"
                " (microscopes with a Cs corrector)."
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Q0"] = FloatJobOption(
            label="Amplitude contrast:",
            default_value=0.1,
            suggested_min=0,
            suggested_max=0.3,
            step_value=0.01,
            help_text=(
                "Fraction of amplitude contrast. Often values around 10% work better"
                " than theoretically more accurate lower values..."
            ),
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["beamtilt_x"] = FloatJobOption(
            label="Beamtilt in X (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the X-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["beamtilt_y"] = FloatJobOption(
            label="Beamtilt in Y (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the Y-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["is_synthetic"] = BooleanJobOption(
            label="Does the node contain synthetic data?",
            default_value=False,
            help_text=(
                "It is always important to differentiate synthetic data from "
                "experimental data!"
            ),
        )

        self.joboptions["microscope_data_file"] = InputNodeJobOption(
            label="Associate a collection metadata file with these movies:",
            pattern=(files_exts("session metadata file", [".json"])),
            help_text=(
                "These files contain information about the experimental setup for the "
                "data collection that generated these movies.  For information on the "
                "format of the file see the ccpem-pipeliner documentation"
            ),
            in_continue=True,
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["pipeliner", "microscope_data"],
        )

        self.joboptions["validate_output"] = BooleanJobOption(
            label="Validate output files?",
            default_value=True,
            help_text=(
                "If this option is selected the job will fail if the output file is"
                " empty.  This option can be turned off for on-the-fly processing"
                " but should be used in most other situations"
            ),
        )

    def create_output_nodes(self) -> None:
        is_synthetic = self.joboptions["is_synthetic"].get_boolean()
        kwds = ["relion", "synthetic"] if is_synthetic else ["relion"]
        if self.joboptions["is_multiframe"].get_boolean():
            self.add_output_node("movies.star", NODE_MICROGRAPHMOVIEGROUPMETADATA, kwds)
        else:
            self.add_output_node("micrographs.star", NODE_MICROGRAPHGROUPMETADATA, kwds)
        scope_file = self.joboptions["microscope_data_file"].get_string()
        if scope_file:
            self.add_output_node(
                os.path.basename(scope_file), NODE_MICROSCOPEDATA, ["ebic"]
            )

    def get_commands(self) -> List[PipelinerCommand]:
        self.command = ["relion_import"]

        if self.joboptions["is_multiframe"].get_boolean():
            fn_out = "movies.star"
            self.command.append("--do_movies")
        else:
            fn_out = "micrographs.star"
            self.command.append("--do_micrographs")

        optics_group = self.joboptions["optics_group_name"].get_string()
        self.command += ["--optics_group_name", "{}".format(optics_group)]

        fn_mtf = self.joboptions["fn_mtf"].get_string()
        if len(fn_mtf) > 0:
            self.command += ["--optics_group_mtf", fn_mtf]

        self.command += [
            "--angpix",
            self.joboptions["angpix"].get_string(),
            "--kV",
            self.joboptions["kV"].get_string(),
            "--Cs",
            self.joboptions["Cs"].get_string(),
            "--Q0",
            self.joboptions["Q0"].get_string(),
            "--beamtilt_x",
            self.joboptions["beamtilt_x"].get_string(),
            "--beamtilt_y",
            self.joboptions["beamtilt_y"].get_string(),
        ]

        fn_in = self.joboptions["fn_in_raw"].get_string()
        self.command += [
            "--i",
            "{}".format(fn_in),
            "--odir",
            self.output_dir,
            "--ofile",
            fn_out,
        ]

        if self.is_continue:
            self.command.append("--continue")

        commands = [PipelinerCommand(self.command, relion_control=True)]

        if self.joboptions["validate_output"].get_boolean():
            is_mov = self.joboptions["is_multiframe"].get_boolean()
            filename, block = (
                ("movies.star", "movies")
                if is_mov
                else ("micrographs.star", "micrographs")
            )
            commands.append(
                PipelinerCommand(
                    [
                        "python3",
                        get_job_script("check_star_file.py"),
                        "--fn_in",
                        str(Path(self.output_dir) / filename),
                        "--block",
                        block,
                        "--clear_relion_success_file",
                    ]
                )
            )

        scope_data = self.joboptions["microscope_data_file"].get_string()
        fin_scope_file = os.path.join(self.output_dir, os.path.basename(scope_data))

        # copy in the microscope data if necessary and update JO value, input_nodes
        if scope_data and not os.path.isfile(fin_scope_file):
            commands.append(PipelinerCommand(["cp", scope_data, fin_scope_file]))
            self.joboptions["microscope_data_file"].value = fin_scope_file
        return commands

    def add_compatibility_joboptions(self) -> None:
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_raw"] = BooleanJobOption(
            label="Import raw movies/micrographs?",
            default_value=True,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_other"] = BooleanJobOption(
            label="Import other node types?",
            default_value=False,
            jobop_group="Relion compatibility options",
        )

    def gather_metadata(self) -> Dict[str, object]:
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata"""
        datafile = DataStarFile(os.path.join(self.output_dir, "movies.star"))
        metadata: Dict[str, object] = {"MovieCount": datafile.count_block("movies")}
        return metadata

    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[
        Union[EmpiarMovieSet, EmImaging, EmSampleSupport, EmSpecimen, EmSoftware]
    ]:
        if depo_type == "EMPIAR":
            return prepare_empiar_raw_mics(os.path.join(self.output_dir, "movies.star"))

        elif depo_type == "ONEDEP":
            scope_datafile = self.joboptions["microscope_data_file"].get_string()
            if check_file_is_ebic_csv(scope_datafile):
                depo_outs: list = parse_ebic_csv(scope_datafile, self.output_dir)
            else:
                depo_outs = [EmImaging(), EmSampleSupport(), EmSpecimen()]

            depo_outs.append(
                EmSoftware(
                    category="OTHER",
                    name="CCPEM-pipeliner/Doppio",
                    version=pipe_vers,
                    details="Project management and data analysis",
                )
            )
            return depo_outs
        else:
            raise ValueError("Must select ONEDEP or EMPIAR")

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        is_movs = self.joboptions["is_multiframe"].get_boolean()
        outfile_name = "movies" if is_movs else "micrographs"
        col_name = "_rlnMicrographMovieName" if is_movs else "_rlnMicrographName"
        movext = self.joboptions["fn_in_raw"].get_string().split(".")[-1]
        movfile = os.path.join(self.output_dir, f"{outfile_name}.star")
        if movext == "eer":
            dispobj = create_results_display_object(
                "text",
                title="EER display not supported",
                display_data=(
                    "Thumbnail images for eer format data are not currently supported"
                ),
                associated_data=[movfile],
            )
        else:
            dispobj = mini_montage_from_starfile(
                starfile=movfile,
                block=outfile_name,
                column=col_name,
                outputdir=self.output_dir,
                nimg=2,
            )
        return [dispobj]


class RelionImportOther(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_OTHER_NAME

    def __init__(self) -> None:
        super().__init__()
        self.do_status_check = False
        self.jobinfo.display_name = "Import other files"
        self.jobinfo.short_desc = (
            "Standard import for all files other than movies, micrographs, or particle "
            "coordinates. Multiple files can be imported simultaneously as long as "
            "they are all of the same type."
        )
        self.jobinfo.programs = []
        self.jobinfo.long_desc = (
            "Import files so they are visible to the pipeline. This "
            "step is not always absolutely necessary, but will make the "
            "project more well organised. Multiple files can be imported "
            "simultaneously as long as they are all of the same type."
        )

        self.joboptions["fn_in_other"] = MultiExternalFileJobOption(
            label="Input files:",
            default_value="",
            pattern=files_exts(name="Input File"),
            help_text=(
                "Select one or more files to import. If you are importing multiple"
                " files they must all be of the same type."
            ),
            is_required=True,
            allow_upload=True,
        )

        self.joboptions["is_relion"] = BooleanJobOption(
            label="Are they RELION STAR files?",
            default_value=False,
            help_text="RELION STAR files need to be handled slightly differently",
        )

        self.joboptions["node_type"] = MultipleChoiceJobOption(
            label="RELION STAR file type:",
            choices=RELION_NODE_TYPES,
            default_value_index=0,
            help_text=(
                "Select the type of RELION STAR file they are. Note they must all be of"
                " the same type."
            ),
            is_required=True,
            deactivate_if=JobOptionCondition([("is_relion", "=", False)]),
        )

        self.joboptions["optics_group_particles"] = StringJobOption(
            label="Rename optics group for particles:",
            default_value="",
            help_text=(
                "Only for the import of a particles STAR file with a single, or no,"
                " optics groups defined: rename the optics group for the imported"
                " particles to this string."
            ),
            validation_regex="^[a-zA-Z0-9_]*$",
            regex_error_message="Can only contain alphanumeric characters",
            deactivate_if=JobOptionCondition(
                [
                    ("is_relion", "=", False),
                    ("node_type", "!=", "Particles STAR file (.star)"),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["pipeliner_node_type"] = MultipleChoiceJobOption(
            label="Node type for imported files:",
            choices=all_node_toplevel_types(add_new=True),
            default_value_index=0,
            help_text=(
                "Select the node type to assign to the files, this tells the pipeliner"
                " what kind of files they are. Note the files must all be the same"
                " type. For a list of pipeliner node types see:\n"
                "https://ccpem-pipeliner.readthedocs.io/en/latest/source/guide_to_"
                "writing_jobs.html#node-type-names"
            ),
            required_if=JobOptionCondition([("alt_nodetype", "!=", "")]),
            deactivate_if=JobOptionCondition(
                [("alt_nodetype", "!=", ""), ("is_relion", "=", True)],
                operation="ANY",
            ),
        )

        self.joboptions["alt_nodetype"] = StringJobOption(
            label="OR: Create a new node type:",
            default_value="",
            help_text=(
                "Add a new node type.  Only do this if none of the standard node"
                " types are applicable. They should almost always be. Actually don't"
                " do this!"
            ),
            required_if=JobOptionCondition(
                [("pipeliner_node_type", "=", NODE_NEWNODETYPE)],
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("pipeliner_node_type", "!=", NODE_NEWNODETYPE),
                    ("is_relion", "=", True),
                ],
                operation="ANY",
            ),
        )

        self.joboptions["kwds"] = MultiStringJobOption(
            label="Keywords",
            default_value="",
            help_text=(
                "Enter keywords to describe the node. "
                "This should be things like the program that created the file "
                "and any relevant descriptions of the file.  For example a halfmap"
                " is of the DensityMap node type and should have 'halfmap' in its"
                " keywords"
            ),
        )

        self.joboptions["is_synthetic"] = BooleanJobOption(
            label="Does the node contain synthetic data?",
            default_value=False,
            help_text=(
                "It is always important to differentiate synthetic data from "
                "experimental data!"
            ),
        )

    def get_node_type(self) -> str:
        if self.joboptions["is_relion"].get_boolean():
            node_type = self.joboptions["node_type"].get_string()
            node_translate = {
                "RELION coordinates STAR file (.star)": NODE_MICROGRAPHCOORDSGROUP,
                "Particles STAR file (.star)": NODE_PARTICLEGROUPMETADATA,
                "2D references STAR file (.star)": NODE_IMAGE2DGROUPMETADATA,
                "Micrographs STAR file (.star)": NODE_MICROGRAPHGROUPMETADATA,
            }
            try:
                mynodetype = node_translate[node_type]
                return mynodetype
            except KeyError:
                raise ValueError(
                    "It appears that this job is being run from a RELION job.star or"
                    " run.job file. This file type is not compatible with the pipeliner"
                    " relion.import job type. Try creating a new input params"
                    " file with 'pipeliner --default_jobstar relion.import'"
                )
        else:
            nodetype = self.joboptions["pipeliner_node_type"].get_string()
            if nodetype == NODE_NEWNODETYPE:
                alt_nt = self.joboptions["alt_nodetype"].get_string()
                nodetype = alt_nt.replace(" ", "")
            return nodetype

    def identify_duplicate_file_names(self) -> Dict[str, str]:
        """Make sure none of the imported files have duplicated names

        If two files have the same name they will be overwritten.
        Increment the names if necessary

        Returns:
            dict: {original filename, incremented name for Import dir}
        """
        filenames = self.joboptions["fn_in_other"].get_list()
        basename_counts = {}
        names_dict = {}
        new_names = []

        for file in filenames:
            bn = os.path.basename(file)
            name, ext = os.path.splitext(bn)
            if file not in names_dict:
                names_dict[file] = ""
            if bn not in basename_counts:
                basename_counts[bn] = 0
                names_dict[file] = bn
                new_names.append(bn)
            else:
                basename_counts[bn] += 1
                new_bn = f"{name}_{basename_counts[bn]:03d}{ext}"
                while new_bn in new_names:
                    basename_counts[name] += 1
                    new_bn = f"{name}_{basename_counts[bn]:03d}{ext}"
                names_dict[file] = new_bn
                if new_bn not in basename_counts:
                    basename_counts[new_bn] = 0
                else:
                    basename_counts[new_bn] += 1
                new_names.append(new_bn)
        return names_dict

    def create_output_nodes(self) -> None:
        raw_kwd = self.joboptions["kwds"].get_list()
        node_type = self.get_node_type()
        kwds = [x.replace(" ", "_") for x in raw_kwd]
        if self.joboptions["is_synthetic"].get_boolean() and "synthetic" not in kwds:
            kwds.append("synthetic")
        if self.joboptions["is_relion"].get_boolean():
            kwds.insert(0, "relion")
        fn_in = self.joboptions["fn_in_other"].get_list()
        file_names = self.identify_duplicate_file_names()
        for outfile in fn_in:
            self.add_output_node(file_names[outfile], node_type, kwds)

    def relion_import(self, fn_in: str) -> List[PipelinerCommand]:
        self.command = ["relion_import"]
        file_names = self.identify_duplicate_file_names()

        mynodetype = self.get_node_type()
        if mynodetype == NODE_PARTICLEGROUPMETADATA:
            self.command.append("--do_particles")
            optics_group = self.joboptions["optics_group_particles"].get_string()
            if optics_group != "":
                self.command += ["--particles_optics_group_name", optics_group]
        else:
            self.command.append("--do_other")

        self.command += ["--i", fn_in]
        self.command += ["--odir", self.output_dir]
        self.command += ["--ofile", file_names[fn_in]]
        if self.is_continue:
            self.command.append("--continue")
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def non_relion_import(self, fn_in: str) -> List[PipelinerCommand]:
        file_names = self.identify_duplicate_file_names()
        com = "mv" if fn_in.startswith(self.output_dir) else "cp"
        return [
            PipelinerCommand(
                [com, fn_in, os.path.join(self.output_dir, file_names[fn_in])]
            )
        ]

    def get_commands(self) -> List[PipelinerCommand]:
        # choose the right way to generate commands
        files_in = self.joboptions["fn_in_other"].get_list()
        is_relion = self.joboptions["is_relion"].get_boolean()
        import_funct = self.relion_import if is_relion else self.non_relion_import
        coms = []
        for infile in files_in:
            coms.extend(import_funct(fn_in=infile))
        return coms

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        if self.joboptions["pipeliner_node_type"].get_string() == NODE_DENSITYMAP:
            maps = {node.name: "" for node in self.output_nodes}
            rdos: Sequence[ResultsDisplayObject] = (
                make_maps_slice_montage_and_3d_display(
                    in_maps=maps,
                    output_dir=self.output_dir,
                    combine_montages=False,
                )
            )
        else:
            rdos = [
                x.default_results_display(output_dir=self.output_dir)
                for x in self.output_nodes
            ]
        return rdos

    def gather_metadata(self) -> Dict[str, object]:
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata.  This could be
        expanded later to collect more detailed info about specific types
        of other imports"""

        md_dict = {
            "FileNames_in": self.identify_duplicate_file_names(),
            "NodeType": self.joboptions["node_type"],
        }
        if self.joboptions.get("optics_group_particles") is not None:
            md_dict["OpticsGroup"] = self.joboptions["optics_group_particles"]

        return md_dict

    def add_compatibility_joboptions(self) -> None:
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_raw"] = BooleanJobOption(
            label="Import raw movies/micrographs?",
            default_value=False,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_other"] = BooleanJobOption(
            label="Import other node types?",
            default_value=True,
            jobop_group="Relion compatibility options",
        )


class RelionImportCoords(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = "relion.import.coordinates"

    def __init__(self) -> None:
        super().__init__()
        self.do_status_check = False
        self.jobinfo.display_name = "Import particle coordinates"
        self.jobinfo.short_desc = "Import coordinate files in .box or .star format"
        self.jobinfo.programs = []
        self.jobinfo.long_desc = (
            "Import coordinate files so they are visible to the pipeline. Each coords"
            " file should have the same name as the micrograph that it "
        )
        self.joboptions["coords_search_string"] = SearchStringJobOption(
            label="Raw coordinates files search string:",
            default_value="",
            help_text="Search string to find the raw particles file in either .box or "
            ".star format.  Each coordinate file's name must start with the name of the"
            " associated micrograph.  IE: mic1_coords.box for mic1.mrc",
            must_be_in_project=True,
        )

        self.joboptions["micrographs_starfile"] = InputNodeJobOption(
            label="Associated micrographs starfile",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Micrographs STAR file", [".star"]),
            help_text="RELION corrected micrographs starfile that contains the "
            "micrographs the coordinates refer to",
        )

    def add_compatibility_joboptions(self) -> None:
        self.joboptions["node_type"] = MultipleChoiceJobOption(
            label="Node type",
            choices=["Particle coordinates (*.box, *_pick.star)"],
            default_value_index=0,
        )
        self.joboptions["do_other"] = BooleanJobOption(
            label="Import other node types?",
            default_value=True,
        )
        self.joboptions["do_raw"] = BooleanJobOption(
            label="Import raw micrographs/movies?",
            default_value=False,
        )

    def create_output_nodes(self) -> None:
        self.add_output_node("coordinates.star", NODE_MICROGRAPHCOORDSGROUP, ["relion"])

    @staticmethod
    def get_mics_coords_search(
        mics: str, search_string: str, output_dir: str
    ) -> Tuple[List[str], str, str]:
        """Get some values needed for both get_commands and postrun actions"""
        mics_sf = DataStarFile(mics)
        mics_list = mics_sf.column_as_list("micrographs", "_rlnMicrographName")
        coordsdir = os.path.join(output_dir, "CoordinateFiles")
        return mics_list, coordsdir, search_string

    def get_commands(self) -> List[PipelinerCommand]:
        mics_list, coordsdir, search_string = self.get_mics_coords_search(
            mics=self.joboptions["micrographs_starfile"].get_string(),
            search_string=self.joboptions["coords_search_string"].get_string(),
            output_dir=self.output_dir,
        )
        coms = [PipelinerCommand(["mkdir", coordsdir])]
        coords_files = glob(search_string)
        coords_files.sort()

        # can't use shell expansion of *.box
        for cf in coords_files:
            coms.append(PipelinerCommand(["cp", cf, coordsdir]))

        coms.append(
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("import/match_coords_to_mics.py"),
                    "-m",
                    self.joboptions["micrographs_starfile"].get_string(),
                    "-c",
                    self.joboptions["coords_search_string"].get_string(),
                    "-o",
                    self.output_dir,
                ]
            )
        )

        return coms

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        sum_output = DataStarFile(os.path.join(self.output_dir, "coordinates.star"))
        partsfiles = sum_output.column_as_list(
            "coordinate_files", "_rlnMicrographCoordinates"
        )
        pcounts = []
        coordsext = self.joboptions["coords_search_string"].get_string().split(".")[-1]
        for pf in partsfiles:
            if coordsext == "box":
                with open(pf) as f:
                    pcounts.append(len(f.readlines()))
            elif coordsext == "star":
                pcounts.append(DataStarFile(pf).count_block())

        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "coordinates.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "coordinates.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [graph, image]

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        # check the micrographs all have unique names
        mics_list, coordsdir, search_string = self.get_mics_coords_search(
            mics=self.joboptions["micrographs_starfile"].get_string(),
            search_string=self.joboptions["coords_search_string"].get_string(),
            output_dir=self.output_dir,
        )
        filenames = set([os.path.basename(x) for x in mics_list])
        if len(filenames) != len(mics_list):
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["micrographs_starfile"],
                    ],
                    message=(
                        "Some micrographs do not have unique filenames. All "
                        "micrographs need to have unique names!"
                    ),
                )
            )

        coords_files = glob(search_string)
        if not len(coords_files):
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["coords_search_string"],
                    ],
                    message="No coordinate files were found",
                )
            )

        return errs
