#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import shutil
from typing import List, Union

from pipeliner import user_settings
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    StringJobOption,
    BooleanJobOption,
    IntJobOption,
    DirPathJobOption,
    JobOptionCondition,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.utils import run_subprocess


def relion5_available() -> bool:
    try:
        result = run_subprocess(
            ["relion_refine", "--version"], capture_output=True, text=True
        )
    except FileNotFoundError:
        return False

    if result.stdout.startswith("RELION version: 5"):
        return True
    else:
        return False


def relion_program(name: str, auto_mpi=None) -> ExternalProgram:
    """get an ExternalProgram object for a relion program with the common vers info

    if a job is specified returns the mpi or single version of the program depending
    on which would be run by the job's parameters

    Args:
        name (str): The name of the program
        auto_mpi (PipelinerJob): Automatically determine if the program should be the
            mpi version or the single version by the info in this job.
    """
    if not auto_mpi:
        return ExternalProgram(
            command=name, vers_com=["relion", "--version"], vers_lines=[0]
        )
    # determine if mpi should be used
    mpi_param = auto_mpi.joboptions.get("nr_mpi")
    nr_mpi = mpi_param.get_number() if mpi_param else 0

    program = name
    if nr_mpi > 1:
        program = name + "_mpi" if not name.endswith("_mpi") else name
    return ExternalProgram(
        command=program, vers_com=["relion", "--version"], vers_lines=[0]
    )


class RelionJob(PipelinerJob):
    """A subclass of the PipelinerJob superclass adds some extra RELION specific
    job options

    Each job type still has its own sub-class.

    WARNING: do not instantiate this class directly, use the factory functions in this
    module.
    """

    def __init__(self, force=False) -> None:
        super().__init__()
        if type(self) is RelionJob and not force:
            raise NotImplementedError(
                "Cannot create RelionJob objects directly - use a sub-class"
            )

        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [relion_program("relion")]
        self.jobinfo.documentation = "https://relion.readthedocs.io/"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Zivanov J",
                    "Nakane T",
                    "Forsberg BO",
                    "Kimanius D",
                    "Hagen WJ",
                    "Lindahl E",
                    "Scheres SHW",
                ],
                title=(
                    "New tools for automated high-resolution cryo-EM structure"
                    " determination in RELION-3."
                ),
                journal="eLife",
                year="2018",
                volume="7",
                pages="e42166",
                doi="10.7554/eLife.42166",
            ),
            Ref(
                authors=["Scheres SHW"],
                title=(
                    "RELION: implementation of a Bayesian approach to cryo-EM structure"
                    " determination."
                ),
                journal="J Struct Biol.",
                year="2012",
                volume="180",
                issue="3",
                pages="519-30",
                doi="10.1016/j.jsb.2012.09.006",
            ),
        ]

        self.command: List[Union[int, float, str]] = []

    def get_relion_command(self, rln_cmd_name) -> List[Union[int, float, str]]:
        """Get the Relion command for a given name (e.g. "refine" or "autopick").

        If the "nr_mpi" job option exists and is greater than 1, the full path to the
        "_mpi" version of the command is used, and the MPI command is prepended to it.

        Returns: The command, as a list of strings
        """
        cmd_name = f"relion_{rln_cmd_name}"
        cmd: List[Union[int, float, str]] = [cmd_name]
        if self.get_nr_mpi() > 1:
            mpi_cmd_name = f"{cmd_name}_mpi"
            mpi_cmd_path = shutil.which(mpi_cmd_name)
            if mpi_cmd_path is not None:
                # Use full path to executable if possible
                cmd = self.get_mpi_command() + [mpi_cmd_path]
            else:
                # Fall back to the simple command name if no full path is found
                cmd = self.get_mpi_command() + [mpi_cmd_name]
        return cmd

    def get_comp_options(self, skip_pad_grid=False) -> None:
        """Get computational options, which are common to for all jobtypes"""

        self.joboptions["do_parallel_discio"] = BooleanJobOption(
            label="Use parallel disc I/O?",
            default_value=True,
            help_text=(
                "If set to Yes, all MPI slaves will read their own images from disc."
                " Otherwise, only the master will read images and send them through the"
                " network to the slaves. Parallel file systems like luster or fhgfs"
                " are good at parallel disc I/O. NFS may break with many slaves reading"
                " in parallel. If your datasets contain particles with different box"
                " sizes, you have to say Yes."
            ),
            in_continue=True,
            jobop_group="Compute options",
        )

        self.joboptions["nr_pool"] = IntJobOption(
            label="Number of pooled particles:",
            default_value=3,
            hard_min=1,
            suggested_max=150,
            step_value=1,
            help_text=(
                "Particles are processed in individual batches by MPI slaves. During"
                " each batch, a stack of particle images is only opened and closed once"
                " to improve disk access times. All particle images of a single batch"
                " are read into memory together. The size of these batches is at least"
                " one particle per thread used. The nr_pooled_particles parameter"
                " controls how many particles are read together for each thread. If it"
                " is set to 3 and one uses 8 threads, batches of 3x8=24 particles will"
                " be read together. This may improve performance on systems where disk"
                " access, and particularly metadata handling of disk access, is a"
                " problem. It has a modest cost of increased RAM usage."
            ),
            in_continue=True,
            jobop_group="Compute options",
        )

        if not skip_pad_grid:
            self.joboptions["do_pad1"] = BooleanJobOption(
                label="Skip padding?",
                default_value=False,
                help_text=(
                    "If set to Yes, the calculations will not use padding in Fourier"
                    " space for better interpolation in the references. Otherwise,"
                    " references are padded 2x before Fourier transforms are"
                    " calculated. Skipping padding (i.e. use --pad 1) gives nearly as"
                    " good results as using --pad 2, but some artifacts may appear in"
                    " the corners from signal that is folded back."
                ),
                in_continue=True,
                jobop_group="Compute options",
            )
            self.joboptions["skip_gridding"] = BooleanJobOption(
                label="Skip gridding?",
                default_value=True,
                help_text=(
                    "If set to Yes, the calculations will skip gridding in the M step"
                    " to save time, typically with just as good results."
                ),
                in_continue=True,
                jobop_group="Compute options",
            )

        self.joboptions["do_preread_images"] = BooleanJobOption(
            label="Pre-read all particles into RAM?",
            default_value=False,
            help_text=(
                "If set to Yes, all particle images will be read into computer memory,"
                " which will greatly speed up calculations on systems with slow disk"
                " access. However, one should of course be careful with the amount of"
                " RAM available. Because particles are read in float-precision, it will"
                " take ( N * box_size * box_size * 4 / (1024 * 1024 * 1024) )"
                " Giga-bytes to read N particles into RAM. For 100 thousand 200x200"
                " images, that becomes 15Gb, or 60 Gb for the same number of 400x400"
                " particles. Remember that running a single MPI slave on each node that"
                " runs as many threads as available cores will have access to all"
                " available RAM.\n\nIf parallel disc I/O is set to No, then only the"
                " master reads all particles into RAM and sends those particles through"
                " the network to the MPI slaves during the refinement iterations."
            ),
            in_continue=True,
            jobop_group="Compute options",
        )

        default_scratch = user_settings.get_scratch_dir()
        self.joboptions["scratch_dir"] = DirPathJobOption(
            label="Copy particles to scratch directory:",
            default_value=default_scratch,
            help_text=(
                "If a directory is provided here, then the job will create a"
                " sub-directory in it called relion_volatile. If that relion_volatile"
                " directory already exists, it will be wiped. Then, the program will"
                " copy all input particles into a large stack inside the"
                " relion_volatile subdirectory. Provided this directory is on a fast"
                " local drive (e.g. an SSD drive), processing in all the iterations"
                " will be faster. If the job finishes correctly, the relion_volatile"
                " directory will be wiped. If the job crashes, you may want to remove"
                " it yourself."
            ),
            in_continue=True,
            jobop_group="Compute options",
            must_exist=False,
        )

        self.joboptions["do_combine_thru_disc"] = BooleanJobOption(
            label="Combine iterations through disc?",
            default_value=False,
            help_text=(
                "If set to Yes, at the end of every iteration all MPI slaves will write"
                " out a large file with their accumulated results. The MPI master will"
                " read in all these files, combine them all, and write out a new file"
                " with the combined results. All MPI salves will then read in the"
                " combined results. This reduces heavy load on the network, but"
                " increases load on the disc I/O. This will affect the time it takes"
                " between the progress-bar in the expectation step reaching its end"
                " (the mouse gets to the cheese) and the start of the ensuing"
                " maximisation step. It will depend on your system setup which is most"
                " efficient."
            ),
            in_continue=True,
            jobop_group="Compute options",
        )

        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU acceleration?",
            default_value=False,
            help_text="If set to Yes, the job will try to use GPU acceleration.",
            in_continue=True,
            jobop_group="Compute options",
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "This argument is not necessary. If left empty, the job itself will try"
                " to allocate available GPU resources. You can override the default"
                " allocation by providing a list of which GPUs (0,1,2,3, etc) to use."
                " MPI-processes are separated by ':', threads by ','.  For example:"
                " '0,0:1,1:0,0:1,1'"
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("use_gpu", "=", False)]),
            jobop_group="Compute options",
        )

    def add_comp_options(self) -> None:
        """Prepare the computational options section of the command;
        common to all job types"""
        if not self.joboptions["do_combine_thru_disc"].get_boolean():
            self.command.append("--dont_combine_weights_via_disc")

        if not self.joboptions["do_parallel_discio"].get_boolean():
            self.command.append("--no_parallel_disc_io")

        scratch_dir = self.joboptions["scratch_dir"].get_string()
        if self.joboptions["do_preread_images"].get_boolean():
            self.command.append("--preread_images")
        elif len(scratch_dir) > 0:
            self.command += ["--scratch_dir", scratch_dir]

        self.command += ["--pool", self.joboptions["nr_pool"].get_string()]

        if self.joboptions["do_pad1"].get_boolean():
            self.command += ["--pad", "1"]
        else:
            self.command += ["--pad", "2"]

        if not self.joboptions["skip_gridding"].get_boolean():
            self.command.append("--dont_skip_gridding")
