#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from glob import glob
import os
import ast
from typing import List, Dict, Sequence, Tuple, Union


from pipeliner.jobs.relion.relion_job import relion_program, RelionJob
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.data_structure import (
    SUBTRACT_JOB_NAME,
    SUBTRACT_REVERT_NAME,
    SUBTRACT_DIR,
)
from pipeliner.nodes import (
    NODE_MASK3D,
    NODE_PARTICLEGROUPMETADATA,
    NODE_OPTIMISERDATA,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    EXT_STARFILE,
    EXT_MRC_MAP,
    BooleanJobOption,
    IntJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.utils import truncate_number
from pipeliner.pipeliner_job import Ref
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
)
from pipeliner.display_tools import mini_montage_from_starfile
from pipeliner.results_display_objects import ResultsDisplayObject


additional_ref = Ref(
    authors=["Bai X", "Rajendra E", "Yang G", "Shi Y", "Scheres SH"],
    title=(
        "Sampling the conformational space of the catalytic subunit of human"
        " gamma-secretase"
    ),
    journal="eLife",
    year="2015",
    volume="4",
    pages="e11182",
    doi="10.7554/eLife.11182",
)


class RelionSubtract(RelionJob):
    OUT_DIR = SUBTRACT_DIR
    PROCESS_NAME = SUBTRACT_JOB_NAME
    CATEGORY_LABEL = "Particle Subtraction"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION particle subtraction"

        self.jobinfo.short_desc = (
            "Perform partial signal subtraction on particle images"
        )
        self.jobinfo.long_desc = (
            "Projections of an input map will be subtracted from the input particles"
            " (which are provided as a _data.star file from a previous 3D"
            " classification or 3D auto-refine). Prior to projecting the map, the input"
            " mask will be applied to it. Therefore, the mask needs to be white in the"
            " region one wants to subtract from the particles, and black everywhere"
            " else."
        )
        self.jobinfo.programs = [relion_program("relion_particle_subtract")]
        self.jobinfo.references.append(additional_ref)

        self.joboptions["fn_opt"] = InputNodeJobOption(
            label="Input optimiser.star:",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("STAR Files", ["_optimiser.star"]),
            help_text=(
                "Select the *_optimiser.star file for the iteration of the 3D"
                " refinement/classification which you want to use for subtraction. It"
                " will use the maps from this run for the subtraction, and of no"
                " particles input STAR file is given below, it will use all of the"
                " particles from this run."
            ),
            deactivate_if=JobOptionCondition([("do_data", "=", True)]),
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="Mask of the signal to keep:",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("Mask MRC file", EXT_MRC_MAP),
            help_text=(
                "Provide a soft mask where the protein density you wish to subtract"
                " from the experimental particles is black (0) and the density you wish"
                " to keep is white (1)."
            ),
            is_required=True,
        )

        self.joboptions["do_data"] = BooleanJobOption(
            label="Use different particles?",
            default_value=False,
            help_text=(
                "If set to Yes, subtraction will be performed on the particles in the"
                " STAR file below, instead of on all the particles of the 3D"
                " refinement/classification from the optimiser.star file."
            ),
            deactivate_if=JobOptionCondition([("fn_opt", "!=", "")]),
        )

        self.joboptions["fn_data"] = InputNodeJobOption(
            label="Input particle star file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "The particle STAR files with particles that will be used in the"
                " subtraction. Leave this field empty if all particles from the input"
                " refinement/classification run are to be used."
            ),
            deactivate_if=JobOptionCondition(
                [("fn_opt", "!=", ""), ("do_data", "=", False)],
                operation="ANY",
            ),
            required_if=JobOptionCondition([("do_data", "=", True)]),
        )

        self.joboptions["do_center_mask"] = BooleanJobOption(
            label="Do center subtracted images on mask?",
            default_value=True,
            help_text=(
                "If set to Yes, the subtracted particles will be centered on"
                " projections of the center-of-mass of the input mask."
            ),
        )

        self.joboptions["do_center_xyz"] = BooleanJobOption(
            label="Do center on my coordinates?",
            default_value=False,
            help_text=(
                "If set to Yes, the subtracted particles will be centered on"
                " projections of the x,y,z coordinates below. The unit is pixel, not"
                " angstrom. The origin is at the center of the box, not at the corner."
            ),
        )

        self.joboptions["center_x"] = IntJobOption(
            label="Center coordinate (pix) - X:",
            default_value=0,
            help_text="X-coordinate of the 3D center (in pixels).",
            deactivate_if=JobOptionCondition([("do_center_xyz", "=", False)]),
        )

        self.joboptions["center_y"] = IntJobOption(
            label="Center coordinate (pix) - Y:",
            default_value=0,
            help_text="Y-coordinate of the 3D center (in pixels).",
            deactivate_if=JobOptionCondition([("do_center_xyz", "=", False)]),
        )

        self.joboptions["center_z"] = IntJobOption(
            label="Center coordinate (pix) - Z:",
            default_value=0,
            help_text="Z-coordinate of the 3D center (in pixels).",
            deactivate_if=JobOptionCondition([("do_center_xyz", "=", False)]),
        )

        self.joboptions["new_box"] = IntJobOption(
            label="New box size:",
            default_value=-1,
            suggested_min=64,
            suggested_max=512,
            step_value=32,
            help_text=(
                "Provide a non-negative value to re-window the subtracted particles"
                " in a smaller box size."
            ),
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)
        self.joboptions["nr_mpi"].default_value = 2

        # ad hoc update to mpi JobOption because this job must always use >2 mpi
        assert isinstance(self.joboptions["nr_mpi"], IntJobOption)
        self.joboptions["nr_mpi"].default_value = 2
        self.joboptions["nr_mpi"].value = 2
        self.joboptions["nr_mpi"].hard_min = 2
        self.joboptions["nr_mpi"].help_text = (
            "This job must be run with at least 2 MPIs"
        )

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "particles_subtracted.star",
            NODE_PARTICLEGROUPMETADATA,
            ["relion", "subtracted"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        fn_opt = self.joboptions["fn_opt"].get_string()
        self.command = self.get_relion_command("particle_subtract")
        self.command += ["--i", fn_opt]

        fn_mask = self.joboptions["fn_mask"].get_string()
        if len(fn_mask) != 0:
            self.command += ["--mask", fn_mask]
        if self.joboptions["do_data"].get_boolean():
            fn_data = self.joboptions["fn_data"].get_string()
            self.command += ["--data", fn_data]

        self.command += ["--o", self.output_dir]

        if self.joboptions["do_center_mask"].get_boolean():
            self.command.append("--recenter_on_mask")

        elif self.joboptions["do_center_xyz"].get_boolean():
            self.command += [
                "--center_x",
                self.joboptions["center_x"].get_string(),
                "--center_y",
                self.joboptions["center_y"].get_string(),
                "--center_z",
                self.joboptions["center_z"].get_string(),
            ]

        new_box = self.joboptions["new_box"].get_number()
        self.command += ["--new_box", truncate_number(new_box, 0)]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.joboptions["do_revert"] = BooleanJobOption(  # TODO: verify param name
            label="OR revert to original particles?",
            default_value=False,
            jobop_group="Relion compatibility options",
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi < 2:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["nr_mpi"]],
                    message="This job must be run with at least 2 MPI",
                )
            ]
        return []

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """Return list of intermediate files/dirs to remove"""

        del_files = []

        if do_harsh:
            del_files += glob(self.output_dir + "subtracted.*")
        return del_files, []

    # needs to return an EMPIAR particles object
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:
        if depo_type == "EMPIAR":
            outfile = self.output_nodes[0].name
            return prepare_empiar_parts(outfile)
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def gather_metadata(self) -> Dict[str, object]:

        metadata_dict: Dict[str, object] = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for line in outlines:
            if "re-centred" in line:
                metadata_dict["RecentredCoordinates"] = ast.literal_eval(
                    line.split(":")[1].strip()
                )
            if "Saved STAR file" in line:
                metadata_dict["NumberOfSubtractedParticles"] = int(line.split()[5])

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        outfile = self.output_nodes[0].name
        return [
            mini_montage_from_starfile(
                starfile=outfile,
                block="particles",
                column="_rlnImageName",
                outputdir=self.output_dir,
                nimg=30,
            )
        ]


class RelionSubtractRevert(RelionJob):
    OUT_DIR = SUBTRACT_DIR
    PROCESS_NAME = SUBTRACT_REVERT_NAME
    CATEGORY_LABEL = "Particle Subtraction"

    def __init__(self) -> None:
        super().__init__()
        self.always_continue_in_schedule = False
        self.do_status_check = False
        self.jobinfo.display_name = "RELION revert subtraction"

        self.jobinfo.short_desc = (
            "Revert subtracted particle images to their original particles"
        )

        self.jobinfo.long_desc = (
            "Revert back from the subtracted particles to the original particles (by"
            " switching the columns named rlnImageName and rlnImageOriginalName in the"
            " input _data.star file). This is useful for example, when one want to"
            " re-refine the entire complex, after one has used partial signal"
            " subtraction to classify the data set into structurally homogeneous"
            " subsets."
        )

        self.jobinfo.references.append(additional_ref)

        self.joboptions["fn_fliplabel"] = InputNodeJobOption(
            label="revert this particle star file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "The particle STAR files with particles that will be used for label"
                " reversion."
            ),
            is_required=True,
        )

        self.get_runtab_options(False, False)

    def create_output_nodes(self) -> None:
        self.add_output_node("original.star", NODE_PARTICLEGROUPMETADATA, ["relion"])

    def get_commands(self) -> List[PipelinerCommand]:
        fliplabel = self.joboptions["fn_fliplabel"].get_string()
        self.command = ["relion_particle_subtract"]
        self.command += ["--revert", fliplabel, "--o", self.output_dir]
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.joboptions["do_revert"] = BooleanJobOption(  # TODO: verify param name
            label="OR revert to original particles?",
            default_value=True,
            jobop_group="Relion compatibility options",
        )

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        """Return list of intermediate files/dirs to remove"""
        del_files = []

        if do_harsh:
            del_files += glob(self.output_dir + "subtracted.*")
        return del_files, []

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        outfile = self.output_nodes[0].name
        return [
            mini_montage_from_starfile(
                starfile=outfile,
                block="particles",
                column="_rlnImageName",
                outputdir=self.output_dir,
                nimg=30,
            )
        ]
