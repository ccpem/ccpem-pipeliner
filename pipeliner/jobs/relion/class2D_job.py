#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from math import ceil
from typing import List, Dict, Sequence, Tuple, Union

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    EXT_RELION_OPT,
    EXT_STARFILE,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionCondition,
)
from pipeliner.data_structure import (
    CLASS2D_PARTICLE_NAME_EM,
    CLASS2D_DIR,
    CLASS2D_HELICAL_NAME_EM,
    CLASS2D_PARTICLE_NAME_VDAM,
    CLASS2D_HELICAL_NAME_VDAM,
)
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_OPTIMISERDATA, Node
from pipeliner.node_factory import create_node
from pipeliner.jobs.relion.refinement_common import (
    get_refine_iter_from_opt_filename,
    refinement_cleanup,
    find_current_opt_name,
)
from pipeliner.jobs.relion.relion_job import relion_program
from pipeliner.starfile_handler import DataStarFile
from pipeliner.job_options import files_exts
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_parts,
    EmpiarParticles,
    EmpiarRefinedParticles,
)
from pipeliner.display_tools import (
    get_ordered_classes_arrays,
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionClass2DJob(RelionJob):
    OUT_DIR = CLASS2D_DIR
    CATEGORY_LABEL = "2D Classification"

    def __init__(self) -> None:
        super().__init__()
        self.del_nodes_on_continue = True
        self.jobinfo.programs = [relion_program("relion_refine")]

        self.joboptions["fn_cont"] = InputNodeJobOption(
            label="Continue from here:",
            node_type=NODE_OPTIMISERDATA,
            node_kwds=["relion", "class2d"],
            default_value="",
            pattern=files_exts("STAR Files", EXT_RELION_OPT),
            help_text=(
                "Select the *_optimiser.star file for the iteration from which you want"
                " to continue a previous run. Note that the Output rootname of the"
                " continued run and the rootname of the previous run cannot be the"
                " same.If they are the same, the program will automatically add a"
                " '_ctX'to the output rootname, with X being the iteration from which"
                " one continues the previous run."
            ),
            in_continue=True,
            only_in_continue=True,
        )

        self.joboptions["fn_img"] = InputNodeJobOption(
            label="Input images STAR file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("STAR files", EXT_STARFILE),
            help_text=(
                "A STAR file with all images (and their metadata). \n \n Alternatively,"
                " you may give a Spider/MRC stack of 2D images, but in that case NO"
                " metadata can be included and thus NO CTF correction can be performed,"
                " nor will it be possible to perform noise spectra estimation or"
                " intensity scale corrections in image groups. Therefore, running"
                " RELION with an input stack will in general provide sub-optimal"
                " results and is therefore not recommended!! Use the Preprocessing"
                " procedure to get the input STAR file in a semi-automated manner. Read"
                " the RELION wiki for more information."
            ),
            is_required=True,
        )

        self.joboptions["do_ctf_correction"] = BooleanJobOption(
            label="Do CTF-correction?",
            default_value=True,
            help_text=(
                "If set to Yes, CTFs will be corrected inside the MAP refinement. The"
                " resulting algorithm intrinsically implements the optimal linear, or"
                " Wiener filter. Note that CTF parameters for all images need to be"
                " given in the input STAR file. The command 'relion_refine"
                " --print_metadata_labels' will print a list of all possible metadata"
                " labels for that STAR file. See the RELION Wiki for more details.\n\n"
                " Also make sure that the correct pixel size (in Angstrom) is given"
                " above!)"
            ),
        )

        self.joboptions["ctf_intact_first_peak"] = BooleanJobOption(
            label="Ignore CTFs until first peak?",
            default_value=False,
            help_text=(
                "If set to Yes, then CTF-amplitude correction will only be performed"
                " from the first peak of each CTF onward. This can be useful if the CTF"
                " model is inadequate at the lowest resolution. Still, in general using"
                " higher amplitude contrast on the CTFs (e.g. 10-20%) often yields"
                " better results. Therefore, this option is not generally recommended:"
                " try increasing amplitude contrast (in your input STAR file) first!"
            ),
        )

        self.joboptions["nr_classes"] = IntJobOption(
            label="Number of classes:",
            default_value=1,
            hard_min=1,
            suggested_max=150,
            step_value=1,
            help_text=(
                "The number of classes (K) for a multi-reference refinement. These"
                " classes will be made in an unsupervised manner from a single"
                " reference by division of the data into random subsets during the"
                " first iteration."
            ),
        )

        self.joboptions["tau_fudge"] = FloatJobOption(
            label="Regularisation parameter T:",
            default_value=2,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Bayes law strictly determines the relative weight between the"
                " contribution of the experimental data and the prior. However, in"
                " practice one may need to adjust this weight to put slightly more"
                " weight on the experimental data to allow optimal results. Values"
                " greater than 1 for this regularisation parameter (T in the JMB2011"
                " paper) put more weight on the experimental data. Values around 2-4"
                " have been observed to be useful for 3D refinements, values of 1-2 for"
                " 2D refinements. Too small values yield too-low resolution structures;"
                " too high values result in over-estimated resolutions, mostly notable"
                " by the apparition of high-frequency noise in the references."
            ),
            in_continue=True,
        )

        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Mask diameter (A):",
            default_value=200,
            suggested_min=0,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "The experimental images will be masked with a soft circular mask with"
                " this diameter. Make sure this radius is not set too small because"
                " that may mask away part of the signal! If set to a value larger than"
                " the image size no masking will be performed.\n\nThe same diameter"
                " will also be used for a spherical mask of the reference structures if"
                " no user-provided mask is specified."
            ),
            in_continue=True,
        )

        self.joboptions["do_zero_mask"] = BooleanJobOption(
            label="Mask individual particles with zeros?",
            default_value=True,
            help_text=(
                "If set to Yes, then in the individual particles, the area outside a"
                " circle with the radius of the particle will be set to zeros prior to"
                " taking the Fourier transform. This will remove noise and therefore"
                " increase sensitivity in the alignment and classification. However, it"
                " will also introduce correlations between the Fourier components that"
                " are not modelled. When set to No, then the solvent area is filled"
                " with random noise, which prevents introducing correlations."
                " High-resolution refinements (e.g. ribosomes or other large complexes"
                " in 3D auto-refine) tend to work better when filling the solvent area"
                " with random noise (i.e. setting this option to No), refinements of"
                " smaller complexes and most classifications go better when using zeros"
                " (i.e. setting this option to Yes)."
            ),
        )

        self.joboptions["highres_limit"] = FloatJobOption(
            label="Limit resolution E-step to (A):",
            default_value=-1,
            suggested_min=-1,
            suggested_max=20,
            step_value=1,
            help_text=(
                "If set to a positive number, then the expectation step (i.e. the"
                " alignment) will be done only including the Fourier components up to"
                " this resolution (in Angstroms). This is useful to prevent"
                " overfitting, as the classification runs in RELION are not to be"
                " guaranteed to be 100% overfitting-free (unlike the 3D auto-refine"
                " with its gold-standard FSC). In particular for very difficult data"
                " sets, e.g. of very small or featureless particles, this has been"
                " shown to give much better class averages. In such cases, values in"
                " the range of 7-12 Angstroms have proven useful."
            ),
        )

        self.joboptions["do_center"] = BooleanJobOption(
            label="Center class averages?",
            default_value=True,
            help_text=(
                "If set to Yes, every iteration the class average images will be"
                " centered on their center-of-mass. This will only work for positive"
                " signals, so the particles should be white."
            ),
            in_continue=True,
        )

        self.joboptions["dont_skip_align"] = BooleanJobOption(
            label="Perform image alignment?",
            default_value=True,
            help_text=(
                "If set to No, then rather than performing both alignment and"
                " classification, only classification will be performed. This allows"
                " the use of very focused masks. This requires that the optimal"
                " orientations of all particles are already stored in the input STAR"
                " file. "
            ),
            in_continue=True,
        )

        self.joboptions["psi_sampling"] = FloatJobOption(
            label="In-plane angular sampling:",
            default_value=6.0,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.5,
            help_text=(
                "The sampling rate for the in-plane rotation angle (psi) in degrees."
                " Using fine values will slow down the program. Recommended value for"
                " most 2D refinements: 5 degrees.\n\nIf auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["offset_range"] = FloatJobOption(
            label="Offset search range (pix):",
            default_value=5,
            suggested_min=0,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Probabilities will be calculated only for translations in a circle"
                " with this radius (in pixels). The center of this circle changes at"
                " every iteration and is placed at the optimal translation for each"
                " image in the previous iteration.\n\nIf auto-sampling is used, this"
                " will be the value for the first iteration(s) only, and the sampling"
                " rate will be increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["offset_step"] = FloatJobOption(
            label="Offset search step (pix):",
            default_value=1,
            suggested_min=0.1,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Translations will be sampled with this step-size (in pixels)."
                " Translational sampling is also done using the adaptive approach."
                " Therefore, if adaptive=1, the translations will first be evaluated on"
                " a 2x coarser grid.\n\nIf auto-sampling is used, this will be the"
                " value for the first iteration(s) only, and the sampling rate will be"
                " increased automatically after that."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.joboptions["allow_coarser"] = BooleanJobOption(
            label="Allow coarser sampling?",
            default_value=False,
            help_text=(
                "If set to Yes, the program will use coarser angular and translational"
                " samplings if the estimated accuracies of the assignments is still low"
                " in the earlier iterations. This may speed up the calculations."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("dont_skip_align", "=", False)]),
        )

        self.get_comp_options(skip_pad_grid=True)

    def relion_back_compatibility_joboptions(self, do_em=False, do_grad=False) -> None:
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation

        self.joboptions["do_em"] = BooleanJobOption(
            label="Use EM algorithm?",
            default_value=do_em,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_grad"] = BooleanJobOption(
            label="Use VDAM algorithm",
            default_value=do_grad,
            jobop_group="Relion compatibility options",
        )

    def common_commands_1(self) -> None:

        # setup inputs/outputs
        fn_run = os.path.join(self.output_dir, "run")
        if self.is_continue:
            fn_cont = self.joboptions["fn_cont"].get_string()
            pos_it = int(fn_cont.partition("it")[2].partition("_")[0])
            if pos_it < 0 or "_optimiser" not in fn_cont:
                raise ValueError(
                    "Warning: invalid optimiser.star filename provided for continuation"
                    " run!",
                )
            self.command += ["--continue", fn_cont]

        elif not self.is_continue:
            fn_img = self.joboptions["fn_img"].get_string()
            self.command += ["--i", fn_img]

        self.command += ["--o", fn_run]

        # compute options
        if not self.joboptions["do_combine_thru_disc"].get_boolean():
            self.command.append("--dont_combine_weights_via_disc")
        if not self.joboptions["do_parallel_discio"].get_boolean():
            self.command.append("--no_parallel_disc_io")

        scratch_dir = self.joboptions["scratch_dir"].get_string()
        if self.joboptions["do_preread_images"].get_boolean():
            self.command.append("--preread_images")
        elif scratch_dir != "":
            self.command += ["--scratch_dir", scratch_dir]
        self.command += ["--pool", self.joboptions["nr_pool"].get_string()]

        # Takanori observed bad 2D classifications with pad1, so use pad2 always.
        # Memory isn't a problem here anyway.
        self.command += ["--pad", "2"]

        # ctf
        if not self.is_continue and self.joboptions["do_ctf_correction"].get_boolean():
            self.command.append("--ctf")
            if self.joboptions["ctf_intact_first_peak"].get_boolean():
                self.command.append("--ctf_intact_first_peak")

    def common_commands_2(self) -> None:
        self.command += ["--tau2_fudge", self.joboptions["tau_fudge"].get_string()]
        part_diam = self.joboptions["particle_diameter"].get_string()
        self.command += ["--particle_diameter", part_diam]

        if not self.is_continue:

            self.command += ["--K", self.joboptions["nr_classes"].get_string()]
            # Always flatten the solvent
            self.command.append("--flatten_solvent")
            if self.joboptions["do_zero_mask"].get_boolean():
                self.command.append("--zero_mask")
            highres_limit = self.joboptions["highres_limit"].get_number()
            if highres_limit > 0:
                self.command += ["--strict_highres_exp", str(highres_limit)]

        if self.joboptions["do_center"].get_boolean():
            self.command.append("--center_classes")

        iover = 1
        self.command += ["--oversampling", str(iover)]

        if not self.joboptions["dont_skip_align"].get_boolean():
            self.command.append("--skip_align")

        else:
            # The sampling given in the GUI will be the oversampled one!
            psi_step = self.joboptions["psi_sampling"].get_number() * (2**iover)
            self.command += ["--psi_step", str(psi_step)]

            # Offset range
            offset_range = self.joboptions["offset_range"].get_string()
            self.command += ["--offset_range", offset_range]

            # The sampling given in the GUI will be the oversampled one!
            offset_step = self.joboptions["offset_step"].get_number() * (2**iover)
            self.command += ["--offset_step", str(offset_step)]

            if self.joboptions["allow_coarser"].get_boolean():
                self.command.append("--allow_coarser_sampling")

    def comp_commands(self) -> None:
        # always do norm and scale correction
        if not self.is_continue:
            self.command += ["--norm", "--scale"]

        self.command += ["--j", self.joboptions["nr_threads"].get_string()]
        if self.joboptions["use_gpu"].get_boolean():
            gpu_ids = self.joboptions["gpu_ids"].get_string()
            self.command += ["--gpu", gpu_ids]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    def make_output_nodes(self, fn_run, niter, outnode_type, outnode_kwds) -> None:
        fn_out = fn_run + "_it" + "{:03d}".format(niter)
        self.output_nodes.append(
            create_node(
                fn_out + "_optimiser.star",
                NODE_OPTIMISERDATA,
                ["relion", "class2d"],
            )
        )
        self.output_nodes.append(
            create_node(fn_out + "_data.star", outnode_type, outnode_kwds)
        )

    def prepare_clean_up_lists(self, do_harsh=False) -> Tuple[List[str], List[str]]:
        return refinement_cleanup(self, do_harsh)

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "class2d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data, NODE_PARTICLEGROUPMETADATA, ["relion", "class2d"]
        )

        return [opt_node, data_node]

    def general_metadata_gathering(self, nr_iter) -> Dict[str, object]:
        # files to use
        model_file = os.path.join(self.output_dir, f"run_it{nr_iter:03d}_model.star")
        data_file = os.path.join(self.output_dir, f"run_it{nr_iter:03d}_data.star")
        mod = DataStarFile(model_file)
        dat = DataStarFile(data_file)

        nclasses = mod.count_block("model_classes")
        metadata_dict: Dict[str, object] = {"Classes": nclasses}

        class_dist = mod.get_block("model_classes").find(["_rlnClassDistribution"])
        metadata_dict["NonEmptyClasses"] = [float(x[0]) > 0 for x in class_dist].count(
            True
        )

        nparts = dat.count_block("particles")
        class_dists = [ceil(float(x[0]) * nparts) for x in class_dist]
        metadata_dict["ClassDistributions"] = class_dists

        resos = mod.get_block("model_classes").find(["_rlnEstimatedResolution"])
        resolist = []
        for x in resos:
            if x[0] != "inf":
                resolist.append(float(x[0]))
            else:
                resolist.append(0.0)
        metadata_dict["ClassResolutions"] = resolist

        imgs = [x[0] for x in dat.get_block("particles").find(["_rlnMicrographName"])]
        metadata_dict["ContributingMicrographs"] = len(set(imgs))

        metadata_dict["ParticleCount"] = nparts

        ctf_data = dat.get_block("particles").find(["_rlnDefocusU", "_rlnDefocusV"])
        ctfs = [(float(x[0]) + float(x[1])) / 2.0 for x in ctf_data]
        metadata_dict["CtfMin"] = min(ctfs)
        metadata_dict["CtfMax"] = max(ctfs)
        return metadata_dict

    # needs to return an EmpiarCorrectedparticles object, and a OneDep
    # FinalClass2D object

    # TO DO: will need to be refactored to handle helical
    def prepare_deposition_data(
        self, depo_type: str
    ) -> Sequence[Union[EmpiarParticles, EmpiarRefinedParticles]]:
        if depo_type == "EMPIAR":
            for f in self.output_nodes:
                if f.name.endswith("_data.star"):
                    outfile = f.name
                    return prepare_empiar_parts(outfile)
            raise ValueError(
                f"No data.star files were found in {self.output_dir}; cannot prepare "
                "the deposition"
            )
        elif depo_type == "ONEDEP":
            return []
        else:
            raise ValueError(
                f"{depo_type} is not a valid deposition type; Use EMPIAR or ONEDEP"
            )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        optimiser_node, particles_node = self.get_current_output_nodes()
        optimiser_name, optimiser_type = optimiser_node.name, optimiser_node.type
        current_parts = particles_node.name

        iterno = get_refine_iter_from_opt_filename(optimiser_name)

        iterjo = (
            self.joboptions.get("nr_iter_em")
            if self.joboptions.get("nr_iter_em")
            else self.joboptions.get("nr_iter_grad")
        )
        if iterjo is None:
            return [
                create_results_display_object(
                    "pending",
                    message="Error generating results",
                    reason="Could not determine the current iteration",
                )
            ]
        nr_iter = iterjo.get_number()
        title = f"2D class averages - Iteration {iterno}/{nr_iter}"
        current_model = current_parts.replace("data", "model")

        optimiser_info: Dict[str, str] = {
            "name": optimiser_name,
            "type": optimiser_type,
        }

        dispobj = get_ordered_classes_arrays(
            current_model,
            10,
            64,
            output_dir=self.output_dir,
            output_filename="2dclass_montage.png",
            parts_file=current_parts,
            title=title,
            base64_output=True,
            optimiser_info=optimiser_info,
        )

        return [dispobj]


class RelionClass2DEM(RelionClass2DJob):
    PROCESS_NAME = CLASS2D_PARTICLE_NAME_EM

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Class2D (EM, single particle)"

        self.jobinfo.short_desc = (
            "Reference-free particle classification in 2D using expectation"
            " maximization"
        )
        self.jobinfo.long_desc = (
            "Reference-free 2D class averaging can be an effective way to throw away"
            " bad particles. Because bad particles do not average well together, they"
            " often go to relatively small classes that yield ugly 2D class averages."
            " Throwing those away then becomes an efficient way of cleaning up your"
            " data."
        )

        self.joboptions["nr_iter_em"] = IntJobOption(
            label="Number of iterations:",
            default_value=25,
            suggested_min=1,
            hard_min=0,
            suggested_max=50,
            step_value=1,
            help_text=(
                "Number of iterations to be performed. Note that the current"
                " implementation of 2D class averaging and 3D classification does NOT"
                " comprise a convergence criterion. Therefore, the calculations will"
                " need to be stopped by the user if further iterations do not yield"
                " improvements in resolution or classes. \n\n  Also note that upon"
                " restarting, the  iteration number continues to be increased, starting"
                " from the final iteration in the previous run. The number given here"
                " is the TOTAL number of iterations. For example, if 10 iterations have"
                " been performed previously and one restarts to perform an additional 5"
                " iterations (for example with a finer angular sampling), then the"
                " number given here should be 10+5=15."
            ),
            in_continue=True,
        )

        self.get_runtab_options(mpi=True, threads=True, addtl_args=True)

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "nr_iter_em",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "do_center",
                "dont_skip_align",
                "psi_sampling",
                "offset_range",
                "offset_step",
                "allow_coarser",
                "nr_pool",
                "do_parallel_discio",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self):
        fn_run = os.path.join(self.output_dir, "run")
        n_iter = int(self.joboptions["nr_iter_em"].get_number())
        self.make_output_nodes(
            fn_run, n_iter, NODE_PARTICLEGROUPMETADATA, ["relion", "class2d"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        self.command = self.get_relion_command("refine")

        self.common_commands_1()

        n_iter = int(self.joboptions["nr_iter_em"].get_number())

        self.command += ["--iter", str(n_iter)]

        self.common_commands_2()

        self.comp_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_em=True)

    def gather_metadata(self) -> Dict[str, object]:
        modelfiles = glob(self.output_dir + "run_it*_model.star")
        modelfiles.sort()
        n_iter = int(os.path.basename(modelfiles[-1].split("_")[1].replace("it", "")))
        metadata = self.general_metadata_gathering(n_iter)
        return metadata


class RelionClass2DVDAM(RelionClass2DJob):
    PROCESS_NAME = CLASS2D_PARTICLE_NAME_VDAM

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Class2D (VDAM, single particle)"

        self.jobinfo.short_desc = (
            "Reference-free particle classification in 2D using Variable-metric"
            " Gradient Descent with Adaptive Moments (VDAM)"
        )

        self.jobinfo.long_desc = (
            "Reference-free 2D class averaging can be an effective way to throw away"
            " bad particles. Because bad particles do not average well together, they"
            " often go to relatively small classes that yield ugly 2D class averages."
            " Throwing those away then becomes an efficient way of cleaning up your"
            " data. . The VDAM algorithm replaces the previously implemented SAGD"
            " algorithm for initial model generation, and makes 2D and 3D"
            " classification faster, especially for large data sets."
        )

        self.joboptions["nr_iter_grad"] = IntJobOption(
            label="Number of VDAM mini-batches:",
            default_value=200,
            hard_min=1,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text=(
                "Number of mini-batches to be processed using the VDAM algorithm. Using"
                " 200 has given good results for many data sets. Using 100 will run"
                " faster, at the expense of some quality in the results."
            ),
        )

        self.get_runtab_options(mpi=False, threads=True, addtl_args=True)

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "nr_iter_grad",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "do_center",
                "dont_skip_align",
                "psi_sampling",
                "offset_range",
                "offset_step",
                "allow_coarser",
                "nr_pool",
                "do_parallel_discio",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self) -> None:
        fn_run = os.path.join(self.output_dir, "run")
        n_iter = int(self.joboptions["nr_iter_grad"].get_number())
        self.make_output_nodes(
            fn_run, n_iter, NODE_PARTICLEGROUPMETADATA, ["relion", "class2d"]
        )

    def get_commands(self) -> List[PipelinerCommand]:

        self.command = ["relion_refine"]

        self.common_commands_1()

        n_iter = int(self.joboptions["nr_iter_grad"].get_number())

        self.command += [
            "--iter",
            n_iter,
            "--grad",
            "--class_inactivity_threshold",
            "0.1",
            "--grad_write_iter",
            "10",
        ]

        self.common_commands_2()

        self.comp_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_grad=True)

    def gather_metadata(self) -> Dict[str, object]:
        modelfiles = glob(self.output_dir + "run_it*_model.star")
        modelfiles.sort()
        n_iter = int(os.path.basename(modelfiles[-1].split("_")[1].replace("it", "")))
        metadata = self.general_metadata_gathering(n_iter)
        return metadata


class RelionClass2DHelical(RelionClass2DJob):
    """General class for helical class2d jobs, use RelionClass2DHelicalEM or
    RelionClass2DHelicalVDAM to create jobs to run"""

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.short_desc = (
            "Reference-free classification in 2D for overlapping helical segments"
        )
        self.jobinfo.long_desc = (
            "2D classification taking into account psi-angle priors assigned during"
            " extraction"
        )

        # Change the node type for input images to ensure helical particles
        assert isinstance(self.joboptions["fn_img"], InputNodeJobOption)
        self.joboptions["fn_img"].node_type = NODE_PARTICLEGROUPMETADATA
        self.joboptions["fn_img"].node_kwds = ["relion", "helicalsegments"]

        self.joboptions["helical_tube_outer_diameter"] = FloatJobOption(
            label="Tube diameter (A):",
            default_value=200,
            suggested_min=100,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "Outer diameter (in Angstroms) of helical tubes. This value should be"
                " slightly larger than the actual width of the tubes. You may want to"
                " copy the value from previous particle extraction job. If negative"
                " value is provided, this option is disabled and ordinary circular"
                " masks will be applied. Sometimes '--dont_check_norm' option is useful"
                " to prevent errors in normalisation of helical segments."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["do_bimodal_psi"] = BooleanJobOption(
            label="Do bimodal angular searches?",
            default_value=True,
            help_text=(
                "Do bimodal search for psi angles? Set to Yes if you want to classify"
                " 2D helical segments with priors of psi angles. The priors should be"
                " bimodal due to unknown polarities of the segments. Set to No if the"
                " 3D helix looks the same when rotated upside down. If it is set to No,"
                " ordinary angular searches will be performed.\n\nThis option will be"
                " invalid if you choose not to perform image alignment on 'Sampling'"
                " tab."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["range_psi"] = FloatJobOption(
            label="Angular search range - psi (deg):",
            default_value=6,
            suggested_min=3,
            suggested_max=30,
            step_value=1,
            help_text=(
                "Local angular searches will be performed within +/- the given amount"
                " (in degrees) from the psi priors estimated through helical segment"
                " picking. A range of 15 degrees is the same as sigma = 5 degrees. Note"
                " that the ranges of angular searches should be much larger than the"
                " sampling.\n\nThis option will be invalid if you choose not to perform"
                " image alignment on 'Sampling' tab."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["do_restrict_xoff"] = BooleanJobOption(
            label="Restrict helical offsets to rise:",
            default_value=True,
            help_text=(
                "Set to Yes if you want to restrict the translational offsets along the"
                " helices to the rise of the helix given below. Set to No to allow free"
                " (conventional) translational offsets."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_rise"] = FloatJobOption(
            label="Helical rise (A):",
            default_value=4.75,
            suggested_min=-1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "The helical rise (in Angstroms). Translational offsets along the"
                " helical axis will be limited from -rise/2 to +rise/2, with a flat"
                " prior."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

    def helical_commands(self) -> None:

        htd = self.joboptions["helical_tube_outer_diameter"].get_string()
        self.command += ["--helical_outer_diameter", htd]

        dont_skip_align = self.joboptions["dont_skip_align"].get_boolean()
        do_bimodal_psi = self.joboptions["do_bimodal_psi"].get_boolean()
        if dont_skip_align and do_bimodal_psi:
            self.command.append("--bimodal_psi")

        val = self.joboptions["range_psi"].get_number()
        val = max(min(90, val), 0)
        self.command += ["--sigma_psi", str(val / 3.0)]

        if self.joboptions["do_restrict_xoff"].get_boolean():
            helical_rise = self.joboptions["helical_rise"].get_string()
            self.command += ["--helix", "--helical_rise_initial", helical_rise]

    def get_current_output_nodes(self) -> List[Node]:
        current_opt = find_current_opt_name(self.output_dir)
        if current_opt == "":
            return []
        opt_node = create_node(current_opt, NODE_OPTIMISERDATA, ["relion", "class2d"])

        fn_data = current_opt.replace("optimiser", "data")
        data_node = create_node(
            fn_data,
            NODE_PARTICLEGROUPMETADATA,
            ["relion", "helicalsegments", "class2d"],
        )

        return [opt_node, data_node]


class RelionClass2DHelicalEM(RelionClass2DHelical):
    PROCESS_NAME = CLASS2D_HELICAL_NAME_EM

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Class2D (EM, helical)"

        self.joboptions["nr_iter_em"] = IntJobOption(
            label="Number of iterations:",
            default_value=25,
            suggested_min=1,
            suggested_max=50,
            step_value=1,
            help_text=(
                "Number of iterations to be performed. Note that the current"
                " implementation of 2D class averaging and 3D classification does NOT"
                " comprise a convergence criterion. Therefore, the calculations will"
                " need to be stopped by the user if further iterations do not yield"
                " improvements in resolution or classes. \n\n  Also note that upon"
                " restarting, the  iteration number continues to be increased, starting"
                " from the final iteration in the previous run. The number given here"
                " is the TOTAL number of iterations. For example, if 10 iterations have"
                " been performed previously and one restarts to perform an additional 5"
                " iterations (for example with a finer angular sampling), then the"
                " number given here should be 10+5=15."
            ),
            in_continue=True,
        )

        self.get_runtab_options(mpi=True, threads=True, addtl_args=True)

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "nr_iter_em",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "do_center",
                "dont_skip_align",
                "psi_sampling",
                "do_bimodal_psi",
                "range_psi",
                "helical_tube_outer_diameter",
                "do_restrict_xoff",
                "helical_rise",
                "offset_range",
                "offset_step",
                "allow_coarser",
                "nr_pool",
                "do_parallel_discio",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "nr_mpi",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self) -> None:
        fn_run = os.path.join(self.output_dir, "run")
        n_iter = int(self.joboptions["nr_iter_em"].get_number())
        self.make_output_nodes(
            fn_run,
            n_iter,
            NODE_PARTICLEGROUPMETADATA,
            ["relion", "helicalsegments", "class2d"],
        )

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_em=True)

    def get_commands(self) -> List[PipelinerCommand]:
        self.command = self.get_relion_command("refine")

        self.common_commands_1()

        n_iter = int(self.joboptions["nr_iter_em"].get_number())

        self.command += ["--iter", str(n_iter)]

        self.common_commands_2()
        self.helical_commands()
        self.comp_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands


class RelionClass2DHelicalVDAM(RelionClass2DHelical):
    PROCESS_NAME = CLASS2D_HELICAL_NAME_VDAM

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Class2D (VDAM, helical)"

        self.joboptions["nr_iter_grad"] = IntJobOption(
            label="Number of VDAM mini-batches:",
            default_value=200,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text=(
                "Number of mini-batches to be processed using the VDAM algorithm. Using"
                " 200 has given good results for many data sets. Using 100 will run"
                " faster, at the expense of some quality in the results."
            ),
        )

        self.get_runtab_options(mpi=False, threads=True, addtl_args=True)

        self.set_joboption_order(
            [
                "fn_cont",
                "fn_img",
                "nr_iter_grad",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "nr_classes",
                "tau_fudge",
                "particle_diameter",
                "do_zero_mask",
                "highres_limit",
                "do_center",
                "dont_skip_align",
                "psi_sampling",
                "do_bimodal_psi",
                "range_psi",
                "helical_tube_outer_diameter",
                "do_restrict_xoff",
                "helical_rise",
                "offset_range",
                "offset_step",
                "allow_coarser",
                "nr_pool",
                "do_parallel_discio",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def create_output_nodes(self) -> None:
        fn_run = os.path.join(self.output_dir, "run")
        n_iter = int(self.joboptions["nr_iter_grad"].get_number())
        self.make_output_nodes(
            fn_run,
            n_iter,
            NODE_PARTICLEGROUPMETADATA,
            ["relion", "helicalsegments", "class2d"],
        )

    def add_compatibility_joboptions(self) -> None:
        self.relion_back_compatibility_joboptions(do_grad=True)

    def get_commands(self) -> List[PipelinerCommand]:
        self.command = ["relion_refine"]

        self.common_commands_1()

        n_iter = int(self.joboptions["nr_iter_grad"].get_number())

        self.command += [
            "--iter",
            n_iter,
            "--grad",
            "--class_inactivity_threshold",
            "0.1",
            "--grad_write_iter",
            "10",
        ]

        self.common_commands_2()
        self.helical_commands()
        self.comp_commands()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands
