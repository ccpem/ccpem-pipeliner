#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import relion_program, RelionJob
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    BooleanJobOption,
)
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.nodes import NODE_PARTICLEGROUPMETADATA, NODE_DENSITYMAP

from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.starfile_handler import get_starfile_loop_headers


class RelionReconstruct(RelionJob):
    PROCESS_NAME = "relion.reconstruct"
    OUT_DIR = "Reconstruct"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION Reconstruct"

        self.jobinfo.short_desc = (
            "Generate a map from a set of particles using relion_reconstruct"
        )
        self.jobinfo.long_desc = (
            "For more information see the help screen by running: 'relion_reconstruct"
            " --h'"
        )
        self.jobinfo.programs = [relion_program("relion_reconstruct")]

        self.joboptions["input_particles"] = InputNodeJobOption(
            label="Input particles file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern="Particles star file (.star)",
            help_text=(
                "Input STAR file with the projection images and their orientations"
            ),
            is_required=True,
        )

        self.joboptions["do_halfmaps"] = BooleanJobOption(
            label="Reconstruct halfmaps?",
            default_value=False,
            help_text=(
                "If selected the the two halfmaps will be reconstructed along with the "
                "full map"
            ),
        )

        self.joboptions["do_ctf"] = BooleanJobOption(
            label="Apply CTF correction?",
            default_value=True,
            help_text=(
                "If 'Yes' CTF correction will be applied during reconstruction. This"
                " generally results in a higher quality reconstruction"
            ),
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Pixel size in the reconstruction (take from first optics group by"
                " default)"
            ),
            is_required=True,
        )

        self.joboptions["sym"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text="Symmetry to apply IE: C2",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["maxres"] = FloatJobOption(
            label="Maximum resolution to consider:",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Maximum resolution (in Angstrom) to consider in Fourier space (default"
                " Nyquist)"
            ),
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "reconstruction.mrc", NODE_DENSITYMAP, ["relion", "reconstruct"]
        )
        if self.joboptions["do_halfmaps"].get_boolean():
            self.add_output_node(
                "reconstruction_half1.mrc",
                NODE_DENSITYMAP,
                ["relion", "reconstruct", "halfmap"],
            )
            self.add_output_node(
                "reconstruction_half2.mrc",
                NODE_DENSITYMAP,
                ["relion", "reconstruct", "halfmap"],
            )

    def assemble_reconstruct_command(self, do_half: int = -1) -> PipelinerCommand:
        """Assemble the reconstruction command

        Args:
            do_half (int): Which half map to reconstruct, -1 for both combined
        Returns:
             PipelinerCommand: The command and its output file name
        """
        command = self.get_relion_command("reconstruct")

        input_file = self.joboptions["input_particles"].get_string()
        command += ["--i", input_file]
        assert do_half in [-1, 1, 2]
        if do_half > 0:
            outfile = f"reconstruction_half{do_half}.mrc"
        else:
            outfile = "reconstruction.mrc"

        command += ["--o", os.path.join(self.output_dir, outfile)]

        if self.joboptions["do_ctf"].get_boolean():
            command.append("--ctf")

        max_res = self.joboptions["maxres"].get_number()
        if max_res > 0:
            command += ["--maxres", str(max_res)]

        angpix = self.joboptions["angpix"].get_number()
        if angpix > 0:
            command += ["--angpix", str(angpix)]

        sym = self.joboptions["sym"].get_string()
        sym = "C1" if sym == "" else sym
        command += ["--sym", sym]

        if do_half > 0:
            command += ["--subset", do_half]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands = [self.assemble_reconstruct_command()]
        if self.joboptions["do_halfmaps"].get_boolean():
            parts_file = self.joboptions["input_particles"].get_string()
            headers = get_starfile_loop_headers(parts_file)
            if "particles" not in headers:
                raise ValueError(f"Particles block missing in input file {parts_file}")
            if "_rlnRandomSubset" not in headers["particles"]:
                raise ValueError(
                    f"Random subset assignments missing from input file {parts_file}."
                    " Cannot reconstruct halfmaps."
                )
            commands.append(self.assemble_reconstruct_command(do_half=1))
            commands.append(self.assemble_reconstruct_command(do_half=2))

        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        mainmap = {
            os.path.join(self.output_dir, "reconstruction.mrc"): "Reconstructed map"
        }
        dispobjs = make_maps_slice_montage_and_3d_display(
            in_maps=mainmap, output_dir=self.output_dir
        )
        if self.joboptions["do_halfmaps"].get_boolean():
            halfs = [
                {str(Path(self.output_dir) / "reconstruction_half1.mrc"): "Halfmap 1"},
                {str(Path(self.output_dir) / "reconstruction_half2.mrc"): "Halfmap 2"},
            ]
            for hm in halfs:
                dispobjs.extend(
                    make_maps_slice_montage_and_3d_display(
                        in_maps=hm, output_dir=self.output_dir
                    )
                )

        return dispobjs
