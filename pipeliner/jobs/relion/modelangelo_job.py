#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pathlib import Path
from typing import List, Sequence

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    ExternalFileJobOption,
    InputNodeJobOption,
    StringJobOption,
    files_exts,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_SEQUENCE,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.data_structure import MODELANGELO_DIR, MODELANGELO_NAME
from pipeliner.user_settings import get_modelangelo_executable
from ccpem_utils.model.gemmi_model_utils import GemmiModelUtils


class ModelAngelo(PipelinerJob):
    PROCESS_NAME = MODELANGELO_NAME
    OUT_DIR = MODELANGELO_DIR
    CATEGORY_LABEL = "Atomic Model Build"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "ModelAngelo"
        self.jobinfo.short_desc = "Automated atomic model building with ModelAngelo"
        self.jobinfo.long_desc = (
            "Generates an atomic model with or without additional sequence input.\n"
            "HMMer can be used to create sequence alignments from the generated "
            "model.  HMMer can also be run separately on the results of a previous "
            "ModelAngelo job."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        mang = get_modelangelo_executable()
        self.jobinfo.programs = [ExternalProgram(mang, vers_com=["--version"])]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Jamali K",
                    "Käll L",
                    "Zhang R",
                    "Brown A",
                    "Kimanius D",
                    "Scheres S",
                ],
                title="Automated Model Building in Cryo-EM Maps",
                journal="Nature",
                year="2024",
                volume="628",
                pages="450-457",
                doi="https://doi.org/10.1038/s41586-024-07215-4",
            )
        ]
        self.jobinfo.documentation = "https://github.com/3dem/model-angelo"

        self.joboptions["fn_map"] = InputNodeJobOption(
            label="B-factor sharpened map:",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("MRC map", [".mrc"]),
            help_text=(
                "Provide a (RELION-postprocessed) B-factor sharpened map for model "
                "building"
            ),
            required_if=JobOptionCondition([("hmmer_input", "=", "")]),
            deactivate_if=JobOptionCondition([("hmmer_input", "!=", "")]),
        )
        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            pattern=files_exts("MRC map", [".mrc"]),
            help_text="Optional 3D mask",
            is_required=False,
            deactivate_if=JobOptionCondition([("hmmer_input", "!=", "")]),
        )
        self.joboptions["p_seq"] = InputNodeJobOption(
            label="FASTA sequence for proteins:",
            node_type=NODE_SEQUENCE,
            pattern=files_exts("FASTA sequence file", [".fasta", ".txt"]),
            help_text=(
                "Provide a FASTA file with sequences for all protein chains to be "
                "built in the map. You can leave this empty if you don't know the "
                "proteins that are there, and then run a HMMer search to identify "
                "the unknown proteins. ModelAngelo will build much better models "
                "when provided with a FASTA sequence file!"
            ),
            deactivate_if=JobOptionCondition([("hmmer_input", "!=", "")]),
        )
        self.joboptions["d_seq"] = InputNodeJobOption(
            label="FASTA sequence for DNA:",
            node_type=NODE_SEQUENCE,
            pattern=files_exts("FASTA sequence file", [".fasta", ".txt"]),
            help_text=(
                "Provide a FASTA file with sequences for all DNA chains to be built "
                "in the map."
            ),
            deactivate_if=JobOptionCondition([("hmmer_input", "!=", "")]),
        )
        self.joboptions["r_seq"] = InputNodeJobOption(
            label="FASTA sequence for RNA:",
            node_type=NODE_SEQUENCE,
            pattern=files_exts("FASTA sequence file", [".fasta", ".txt"]),
            help_text=(
                "Provide a FASTA file with sequences for all RNA chains to be built "
                "in the map."
            ),
            deactivate_if=JobOptionCondition([("hmmer_input", "!=", "")]),
        )
        self.joboptions["do_hmmer"] = BooleanJobOption(
            label="Perform HMMer search?",
            default_value=False,
            help_text=(
                "If set to Yes, model-angelo will perform a HMM search using HMMer "
                "in the output directory of the model-angelo run (without sequence). "
                "This step can also be run independently on the atomic model from an "
                "existing ModelAngelo job. This way, you can try multiple HMMer runs."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition(
                [
                    ("p_seq", "!=", ""),
                    ("d_seq", "!=", ""),
                    ("r_seq", "!=", ""),
                ],
            ),
        )

        self.joboptions["hmmer_input"] = InputNodeJobOption(
            label="Run HMMer on an existing model:",
            node_type=NODE_ATOMCOORDS,
            help_text=(
                "Run HMMer on a .cif model file from a previous ModelAngelo job"
            ),
            deactivate_if=JobOptionCondition(
                [("fn_map", "!=", ""), ("do_hmmer", "=", False)],
                operation="ANY",
            ),
            required_if=JobOptionCondition(
                [("fn_map", "=", ""), ("do_hmmer", "=", True)],
                operation="ALL",
            ),
            jobop_group="HMM search",
        )
        self.joboptions["fn_lib"] = InputNodeJobOption(
            label="Library with sequences for HMMer search:",
            node_type=NODE_SEQUENCE,
            pattern=files_exts("FASTA sequence file", [".fasta", ".txt"]),
            help_text=(
                "FASTA file with all sequences for HMMer search. This is "
                "often an entire proteome."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
            required_if=JobOptionCondition([("do_hmmer", "=", True)]),
        )
        self.joboptions["alphabet"] = MultipleChoiceJobOption(
            label="Alphabet for the HMMer search:",
            choices=["amino", "DNA", "RNA"],
            default_value_index=0,
            help_text="Type of Alphabet for HMM searches.",
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
        )
        self.joboptions["F1"] = FloatJobOption(
            label="HMMSearch F1: ",
            default_value=0.02,
            suggested_min=0.01,
            suggested_max=10.0,
            step_value=0.1,
            hard_min=0.0,
            help_text=(
                "F1 parameter for HMMSearch, see their documentation at "
                "http://eddylab.org/software/hmmer/Userguide.pdf. "
                "The F1, F2, F3 and E-value control the statistical significance "
                "of alignment between the residue profile and any sequence from "
                "the search database. Increasing these will find more sequence hits "
                "but also bring in false positives."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
        )
        self.joboptions["F2"] = FloatJobOption(
            label="HMMSearch F2: ",
            default_value=0.001,
            suggested_min=0.0001,
            suggested_max=10.0,
            hard_min=0.0,
            step_value=0.01,
            help_text=(
                "F2 parameter for HMMSearch, see their documentation at "
                "http://eddylab.org/software/hmmer/Userguide.pdf. "
                "The F1, F2, F3 and E-value control the statistical significance "
                "of alignment between the residue profile and any sequence from "
                "the search database. Increasing these will find more sequence hits "
                "but also bring in false positives."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
        )
        self.joboptions["F3"] = FloatJobOption(
            label="HMMSearch F3: ",
            default_value=0.00001,
            hard_min=0.0,
            suggested_max=10.0,
            step_value=0.0001,
            help_text=(
                "F3 parameter for HMMSearch, see their documentation at "
                "http://eddylab.org/software/hmmer/Userguide.pdf. "
                "The F1, F2, F3 and E-value control the statistical significance "
                "of alignment between the residue profile and any sequence from "
                "the search database. Increasing these will find more sequence hits "
                "but also bring in false positives."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
        )
        self.joboptions["E"] = FloatJobOption(
            label="HMMSearch E: ",
            default_value=10,
            hard_min=0.0,
            suggested_max=100.0,
            step_value=10,
            help_text=(
                "E parameter for HMMSearch, see their documentation at "
                "http://eddylab.org/software/hmmer/Userguide.pdf. "
                "The F1, F2, F3 and E-value control the statistical significance "
                "of alignment between the residue profile and any sequence from "
                "the search database. Increasing these will find more sequence hits "
                "but also bring in false positives."
            ),
            jobop_group="HMM search",
            deactivate_if=JobOptionCondition([("do_hmmer", "=", False)]),
        )
        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU",
            default_value=True,
            help_text="ModelAnglo will run much faster on GPU",
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "This argument is not necessary. If left empty, the job itself will try"
                " to allocate available GPU resources. You can override the default"
                " allocation by providing a comma separated list of which GPUs "
                " (0,1,2,3, etc) to use."
            ),
            validation_regex="^(?:[0-9]+,)*[0-9]+$",
            regex_error_message="Must be a comma separated list with no spaces",
            jobop_group="Running options",
            deactivate_if=JobOptionCondition([("use_gpu", "=", False)]),
        )

        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        hmmer_in = self.joboptions["hmmer_input"].get_string()
        if hmmer_in and hmmer_in.split("/")[0] != "ModelAngelo":
            errs.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["hmmer_input"]],
                    message="The cif file must be from a ModelAngelo Job",
                )
            )
        return errs

    def add_compatibility_joboptions(self) -> None:
        modelangelo_exe = self.jobinfo.programs[0].exe_path
        self.joboptions["fn_modelangelo_exe"] = ExternalFileJobOption(
            label="ModelAngelo executable:",
            default_value=modelangelo_exe if modelangelo_exe is not None else "",
            help_text=(
                "The modelangelo executable. By default, the relion_python_modelangelo "
                "will be used, which was installed inside conda with a typical relion "
                "install. Only change this if that version is giving you problems."
            ),
        )

    def get_output_name(self):
        return self.output_dir.split("/")[-2] + ".cif"

    def create_output_nodes(self) -> None:
        if self.joboptions["fn_map"].get_string():
            self.add_output_node(
                self.get_output_name(), NODE_ATOMCOORDS, ["modelangelo"]
            )

    def make_build_command(self) -> List[PipelinerCommand]:
        build_command = [get_modelangelo_executable()]
        fn_protein_seq = self.joboptions["p_seq"].get_string()
        fn_dna_seq = self.joboptions["d_seq"].get_string()
        fn_rna_seq = self.joboptions["r_seq"].get_string()
        fn_map = self.joboptions["fn_map"].get_string()
        fn_mask = self.joboptions["fn_mask"].get_string()
        fn_gpu_ids = self.joboptions["gpu_ids"].get_string()

        # ModelAgnelo does not read .map files:
        # https://github.com/3dem/model-angelo/issues/52
        # Remove when bug is resolved

        coms_list = []
        if fn_map.endswith(".map"):
            tools_script = get_job_script("modelangelo/modelangelo_tools.py")
            prep_command = ["python3", tools_script]
            input_map_link_name = Path(fn_map).with_suffix(".mrc").name
            prep_command.extend(
                ["--symlink_mrc_map", os.path.relpath(fn_map, self.working_dir)]
            )
            fn_map = input_map_link_name
            coms_list.append(prep_command)

        if fn_protein_seq or fn_dna_seq or fn_rna_seq:
            build_command.extend(["build", "-v", fn_map])
            if fn_protein_seq:
                build_command.extend(["-pf", fn_protein_seq])
            if fn_dna_seq:
                build_command.extend(["-df", fn_dna_seq])
            if fn_rna_seq:
                build_command.extend(["-rf", fn_rna_seq])

        else:
            build_command.extend(["build_no_seq", "-v", fn_map])

        if fn_mask:
            build_command.extend(["--mask-path", fn_mask])

        use_gpu = self.joboptions["use_gpu"].get_boolean()
        if fn_gpu_ids and use_gpu:
            build_command.extend(["--device", fn_gpu_ids])
        elif not use_gpu:
            build_command.extend(["--device", "cpu"])

        build_command.extend(["-o", self.output_dir])
        coms_list.append(build_command)

        return [PipelinerCommand(x) for x in coms_list]

    def make_hmmer_command(self) -> PipelinerCommand:
        hmmer_command = [get_modelangelo_executable(), "hmm_search"]
        hmmer_in = os.path.dirname(self.joboptions["hmmer_input"].get_string())
        if hmmer_in:
            hmmer_command.extend(["-i", hmmer_in])
        else:
            hmmer_command.extend(["-i", self.output_dir])
        hmmer_command.extend(["-f", self.joboptions["fn_lib"].get_string()])
        hmmer_command.extend(["-o", self.output_dir])
        hmmer_command.extend(["-a", self.joboptions["alphabet"].get_string()])

        # HMMSearch parameters
        hmmer_command.extend(["--F1", self.joboptions["F1"].get_string()])
        hmmer_command.extend(["--F2", self.joboptions["F2"].get_string()])
        hmmer_command.extend(["--F3", self.joboptions["F3"].get_string()])
        hmmer_command.extend(["--E", self.joboptions["E"].get_string()])
        return PipelinerCommand(hmmer_command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands_list = []
        if not self.is_continue and self.joboptions["fn_map"].get_string():
            commands_list = self.make_build_command()
        fn_protein_seq = self.joboptions["p_seq"].get_string()
        fn_dna_seq = self.joboptions["d_seq"].get_string()
        fn_rna_seq = self.joboptions["r_seq"].get_string()
        if self.joboptions["do_hmmer"].get_boolean() and not (
            fn_protein_seq or fn_dna_seq or fn_rna_seq
        ):
            commands_list.append(self.make_hmmer_command())
        return commands_list

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        fn_map = self.joboptions["fn_map"].get_string()
        fn_mod = self.output_dir + self.get_output_name()
        fn_mod_with_score = Path(
            self.output_dir, "entropy_scores", self.get_output_name()
        )
        if not fn_mod_with_score.is_file():  # scored file doesnt exist
            fn_mod_with_score = fn_mod
        results = [
            create_results_display_object(
                "mapmodel",
                title="Generated atomic model",
                associated_data=[str(fn_mod_with_score)],
                maps=[fn_map],
                models=[str(fn_mod_with_score)],
            )
        ]
        fn_protein_seq = self.joboptions["p_seq"].get_string()
        fn_dna_seq = self.joboptions["d_seq"].get_string()
        fn_rna_seq = self.joboptions["r_seq"].get_string()
        gemmi_utils = GemmiModelUtils(fn_mod)
        list_seq_coverage = []
        list_seq_inputs = []
        for seq_inp in [fn_protein_seq, fn_dna_seq, fn_rna_seq]:
            if seq_inp:
                list_seq_inputs.append(seq_inp)
        if list_seq_inputs:
            dict_chain_match = gemmi_utils.get_chain_matches_to_fasta(
                fastafiles=list_seq_inputs
            )
            flag_coverage = ""
            for seq_id in dict_chain_match:
                for ch_match in dict_chain_match[seq_id]:  # chain, identity
                    try:
                        if ch_match[1] > 1.0:  # > 1% identity
                            list_seq_coverage.append([seq_id, ch_match[0], ch_match[1]])
                            if ch_match[1] < 3.0:
                                flag_coverage = (
                                    "Sequence match of a small fragment (e.g. < 6 "
                                    "residues) might be less reliable"
                                )
                    except (TypeError, ValueError):
                        pass
            if list_seq_coverage:
                table_header = ["Input sequence ID", "Chain ID", "Coverage"]
                chain_coverage = create_results_display_object(
                    "table",
                    title="Sequence coverage",
                    headers=table_header,
                    table_data=list_seq_coverage,
                    flag=flag_coverage,
                    associated_data=[fn_mod],
                )
                results.append(chain_coverage)

        if self.joboptions["do_hmmer"].get_boolean():
            hits_file = Path(self.output_dir) / "best_hits.csv"

            with open(hits_file) as hits:
                hits_data = hits.readlines()
            table_data = []
            for hitline in hits_data[1:]:
                table_data.append(hitline.split(",")[:-1])
            results.append(
                create_results_display_object(
                    "table",
                    title="HMMer best hits",
                    headers=hits_data[0].split(",")[:-1],
                    table_data=table_data,
                    associated_data=[str(hits_file)],
                )
            )
        return results
