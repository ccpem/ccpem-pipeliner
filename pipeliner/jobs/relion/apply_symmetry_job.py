#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
    JobOptionCondition,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionApplySymmetry(RelionJob):
    OUT_DIR = "ApplySymmetry"
    PROCESS_NAME = "relion.map_utilities.apply_symmetry"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Apply symmetry"
        self.jobinfo.short_desc = "Apply symmetry to a map"

        self.jobinfo.long_desc = "Apply the chosen symmetry to a map"

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to symmetrise",
            is_required=True,
            default_value="",
        )

        self.joboptions["sym"] = MultipleChoiceJobOption(
            label="Symmetry type",
            choices=[
                "Cyclic (C)",
                "Dihedral (D)",
                "Tetrahedral (T)",
                "Octahedral (O)",
                "Icosahedral (I)",
            ],
            default_value_index=0,
            help_text="Choose the type of symmetry to apply",
            in_continue=True,
        )

        self.joboptions["fold"] = IntJobOption(
            label="Symmetry fold",
            hard_min=1,
            default_value=1,
            required_if=JobOptionCondition(
                [("sym", "=", "Cyclic (C)"), ("sym", "=", "Dihedral (D)")],
                operation="ANY",
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("sym", "=", "Tetrahedral (T)"),
                    ("sym", "=", "Octahedral (O)"),
                    ("sym", "=", "Icosahedral (I)"),
                ],
                operation="ANY",
            ),
        )

    def get_sym(self) -> str:
        symtype = self.joboptions["sym"].get_string()[0]
        symfold = self.joboptions["fold"].get_number()
        sym = symtype + str(symfold) if symtype in ["C", "D"] else symtype
        return sym

    def get_outfile_name(self, map_name) -> str:
        sym = self.get_sym()
        return (
            os.path.splitext(os.path.basename(map_name))[0] + f"_{sym.lower()}sym.mrc"
        )

    def create_output_nodes(self) -> None:
        sym = self.get_sym()
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            outfile_name = self.get_outfile_name(map_names[out_map])
            self.add_output_node(outfile_name, NODE_DENSITYMAP, [f"{sym.lower()}sym"])

    def make_command(self, input_map: str, outfile: str) -> PipelinerCommand:
        sym = self.get_sym()
        if Path(input_map).suffix != ".mrc":
            input_map += ":mrc"
        outfile_name = self.get_outfile_name(outfile)
        command = [
            "relion_image_handler",
            "--i",
            input_map,
            "--sym",
            sym,
            "--o",
            os.path.join(self.output_dir, outfile_name),
        ]
        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for map_pair in map_names.items():
            commands.append(self.make_command(*map_pair))
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        sym = self.get_sym()
        outputs = {}
        for out in map_names.values():
            outfile_name = self.get_outfile_name(out)
            outputs[os.path.join(self.output_dir, outfile_name)] = (
                f"{outfile_name} with {sym} symmetry applied"
            )

        return make_maps_slice_montage_and_3d_display(
            in_maps=outputs, output_dir=self.output_dir, combine_montages=False
        )
