#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import os
import json
import pathlib
from typing import Union
from pipeliner.utils import get_mrc_map_summary


def parse_args():
    parser = argparse.ArgumentParser(description="CCP-EM model tools")
    parser.add_argument(
        "-m",
        "--map",
        required=True,
        help="Input map (MRC)",
    )
    parser.add_argument(
        "-use_data",
        "--use_data",
        default=False,
        action="store_true",
        help="Use data to calculate min, max, mean and std ?",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )

    return parser.parse_args()


def get_mrc_header_dataparameters(
    mapfile: str,
    use_data: bool = False,
    outdir: Union[str, pathlib.Path, None] = None,
):
    map_parameters = get_mrc_map_summary(mapfile=mapfile, use_data=use_data)
    map_basename = os.path.splitext(os.path.basename(mapfile))[0]
    if outdir:
        out_json = os.path.join(outdir, map_basename + "_map_parameters.json")
    else:
        out_json = map_basename + "_map_parameters.json"

    with open(out_json, "w") as j:
        json.dump(map_parameters, j)


def main():
    args = parse_args()
    get_mrc_header_dataparameters(args.map, use_data=args.use_data, outdir=args.odir)


if __name__ == "__main__":
    main()
