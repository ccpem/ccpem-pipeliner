#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import shutil
import os
import sys

from pipeliner.starfile_handler import JobStar
from pipeliner.api.manage_project import look_for_project
from pipeliner import __version__ as pipevers


def get_args():
    parser = argparse.ArgumentParser(
        prog="empiar_deposition_creation",
        description="make an EMPIAR deposition template",
    )

    parser.add_argument(
        "--jobstar_file",
        default=None,
        help="The jobstar for a pipeliner.deposition.empiar job",
    )

    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_args()
    args = parser.parse_args(in_args)

    outdir = os.path.dirname(args.jobstar_file)

    missing = []
    depositor = shutil.which("empiar-depositor")
    if depositor is None:
        missing.append(
            "----\n"
            "empiar-depositor not found:  This script is used to make the deposition. "
            "It must be available in the system $PATH and can be installed with "
            "'pip install empiar-depositor'"
        )
    jobops = JobStar(args.jobstar_file).all_options_as_dict()
    globus = shutil.which("globus")
    ascp = shutil.which("ascp")
    xfer_pwd = (
        jobops["empiar_transfer_password"]
        if jobops["empiar_transfer_password"]
        else None
    )
    if xfer_pwd is None:
        missing.append(
            "----\n"
            "No EMPIAR transfer password provided: The password is provided by EMPIAR "
            "when the deposition is arranged and gives access to upload data to the "
            "EMPIAR servers"
        )

    command = ["empiar-depositor"]
    if ascp is not None:
        command.extend(["-a", ascp])
    if globus is not None:
        command.extend(["-g", globus])
    thumbpath = jobops.get("thumbnail_file")
    if thumbpath:
        command.extend(["-e", os.path.join(outdir, os.path.basename(thumbpath))])
    if globus is None and ascp is None:
        missing.append(
            "----\n"
            "Globus or ascp (Aspera) not found: One of these two programs is necessary "
            "for data transfer to the EMPIAR servers.  \nDownload the ascp tool from: "
            "https://www.ibm.com/aspera/connect/\nor install globus with 'pip install "
            "globus-cli==1.7.0'"
        )
    token = jobops.get("empiar_token")
    if not token:
        missing.append(
            "----\n"
            "No EMPIAR API token was provided: This token is provided by EMPIAR when a "
            "deposition is arranged. Alternatively your EMPIAR username can be used, "
            "but either way the deposition must be arranged beforehand with EMPIAR"
        )
    else:
        command.append(token)

    command.append(os.path.join(outdir, "deposition_data_EMPIAR.json"))
    command.append(os.path.join(outdir, "deposition_data"))

    result_text = os.path.join(outdir, ".result_text.txt")
    if not missing:
        # write the script
        script_file = os.path.join(outdir, "start_empiar_deposition.py")
        proj = look_for_project()
        if proj is not None:
            proj_dir = os.path.abspath(os.path.dirname(proj["pipeline file"]))
        else:
            raise ValueError("Error getting project name")

        comment = (
            "# EMPIAR deposition script\n# Written by CCPEM-pipeliner "
            f"vers {pipevers}\n# This script should be run from the project dir:\n"
            f"#    {proj_dir}"
        )
        with open(script_file, "w") as script:
            script.write("#/usr/bin/env python3\n")
            script.write(f"{comment}\n")
            script.write("tmp_env = os.environ.copy()\n")
            script.write(f"tmp_env['EMPIAR_TRANSFER_PASS'] = '{xfer_pwd}'\n")
            script.write(f"command = {command}\n")
            script.write("subprocess.Popen(command, env=tmp_env)")

        # write the display object file - for nice formatting
        with open(result_text, "w") as res:
            res.write(
                "The deposition is ready.  Review the data below, and when satisfied "
                "begin the deposition by running:\n"
            )
            res.write(f"\t{script_file}\n")
            res.write("From the project directory: \n")
            res.write(f"\t{proj_dir}")
    else:
        with open(result_text, "w") as res:
            res.write(
                "The deposition data and file have been prepared and can be reviewed "
                "below but an automated deposition cannot be made for the following "
                "reasons:"
            )
            for missing_bit in missing:
                res.write("\n" + missing_bit)


if __name__ == "__main__":
    main()
