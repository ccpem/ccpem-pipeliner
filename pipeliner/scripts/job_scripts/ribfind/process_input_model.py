import argparse
import os
import subprocess


def parse_args():
    parser = argparse.ArgumentParser(description="remove ligand water")
    parser.add_argument(
        "-p",
        "--model",
        required=True,
        help="Input atomic model (.pdb, .cif)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-mid",
        "--mid",
        required=False,
        default=None,
        help="Model ID to prefix output file names",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    # read input
    modelfile = args.model
    if args.mid:
        modelid = args.mid
    else:
        modelid = os.path.splitext(os.path.basename(modelfile))[0]
    # remove ligand water and save pdb
    model_no_wat_lig_rel = modelid + "_no_wat_lig.pdb"
    gemmi_command = [
        "gemmi",
        "convert",
        "--remove-lig-wat",
        modelfile,
        model_no_wat_lig_rel,
    ]
    subprocess.call(gemmi_command)
    # add "HEADER" to the first line
    # model_ext = os.path.splitext(modelfile)[1]
    if os.path.isfile(model_no_wat_lig_rel):
        with open(model_no_wat_lig_rel, "r+") as m:
            header_found = False
            for line in m:
                lstrip = line.strip()
                if lstrip:  # first non empty line
                    if lstrip.startswith("HEADER"):
                        header_found = True
                    break
            if not header_found:
                m.seek(0)
                lines = m.readlines()
                m.seek(0)
                m.write("HEADER\n")
                m.writelines(lines)


if __name__ == "__main__":
    main()
