#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import sys
import argparse
import subprocess
from gemmi import cif

from pipeliner.starfile_handler import DataStarFile, interconvert_box_star_coords


def get_arguments():
    parser = argparse.ArgumentParser(description="Setup pipeliner REPIC job")

    parser.add_argument(
        "--outdir",
        help="Where to write the output",
        nargs="?",
        metavar="output dir",
        required=True,
    )

    parser.add_argument(
        "--autopick_files",
        help="Where to write the output",
        nargs="+",
        metavar="output dir",
        required=False,
    )

    parser.add_argument(
        "--write_summary",
        help="Write the summary file",
        action="store_true",
    )
    return parser


def get_parts_files(autopick_file, outdir):
    job_dir = f"from_{os.path.dirname(autopick_file).split('/')[-1]}"
    ap_data = DataStarFile(autopick_file).loop_as_list(
        "coordinate_files", ["_rlnMicrographName", "_rlnMicrographCoordinates"]
    )
    os.makedirs(os.path.join(outdir, job_dir))
    for pair in ap_data:
        mic_name = os.path.basename(pair[0].split(".")[0])
        dest_file = os.path.join(outdir, job_dir, f"{mic_name}.box")

        if pair[1].endswith(".box"):
            subprocess.call(["cp", pair[1], dest_file])
        elif pair[1].endswith(".star"):
            interconvert_box_star_coords(pair[1], dest_file)


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    if not args.write_summary:
        for autopick_file in args.autopick_files:
            get_parts_files(autopick_file, args.outdir)
    else:
        # use the autopick file to identify the micrographs and write th esummary file
        apfile = DataStarFile(args.autopick_files[0])
        mics = apfile.loop_as_list("coordinate_files", ["_rlnMicrographName"])
        summary_file = cif.Document()
        c_block = summary_file.add_new_block("coordinate_files")
        c_loop = c_block.init_loop("_rln", ["MicrographName", "MicrographCoordinates"])
        for mic in mics:
            concoords = os.path.basename(mic[0]).replace(".mrc", ".box")
            c_loop.add_row([mic[0], os.path.join(args.outdir, f"cliques/{concoords}")])
            print(concoords, "$$"),
        summary_file.write_file(os.path.join(args.outdir, "consensus_picks.star"))


if __name__ == "__main__":
    main()
