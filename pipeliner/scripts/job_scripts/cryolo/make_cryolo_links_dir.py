#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import os
import shutil
import sys
from typing import Optional, List
from gemmi import cif

from pipeliner.starfile_handler import DataStarFile


def main(in_args: Optional[List[str]] = None):
    """Makes a dir with symlinks to micrograph files for crYOLO"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_mics_file", type=str, required=True)
    parser.add_argument("--is_continue", action="store_true")
    parser.add_argument("--cryolo_job_dir", type=str, required=True)
    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    in_star = cif.read_file(args.input_mics_file)
    mics_block = in_star.find_block("micrographs")
    all_mics = list(mics_block.find_loop("_rlnMicrographName"))

    # put symlinks to the mics to pick in a tmp directory
    # if is_continue check which micrographs have already been picked
    if args.is_continue:
        ext = DataStarFile("autopick.star")
        mics_block = ext.get_block("coordinate_files")
        done_mics = set(mics_block.find_loop("_rlnMicrographName"))
        # subtract the already finished mics from the list of all mics
        all_mics = list(set(all_mics) - done_mics)

    # make the TMP dir and create the symlinks
    mics_dir = "Micrographs"
    if os.path.isdir(mics_dir):
        shutil.rmtree(mics_dir)
    os.makedirs(mics_dir)
    for mic in all_mics:
        micpath = os.path.abspath(os.path.relpath(mic, args.cryolo_job_dir))
        os.symlink(micpath, os.path.join(mics_dir, os.path.basename(mic)))


if __name__ == "__main__":
    main()
