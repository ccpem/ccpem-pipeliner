"""After a crYOLO job has been run assemble the autopick.star file for relion
this file contains name and coordinate file for each micrograph
"""

import argparse
from gemmi import cif
import sys
import os
from pipeliner.star_writer import write


def get_arguments():
    parser = argparse.ArgumentParser(description="Cryolo output file preparation")

    parser.add_argument(
        "--micrographs",
        "-i",
        help="STAR file for the picked micrographs",
        nargs="?",
        metavar="Micrographs .star file",
        required=True,
    )
    parser.add_argument(
        "--outdir",
        "-o",
        help="Output directory",
        nargs="?",
        metavar="Output directory",
        required=True,
    )
    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    infile = args.micrographs

    in_star = cif.read_file(infile)
    mics_block = in_star.find_block("micrographs")
    all_mics = mics_block.find_loop("_rlnMicrographName")

    # make sure that particle files were written
    missing_files = []
    for f in all_mics:
        fn = os.path.basename(f).replace(".mrc", ".star")
        starfile = os.path.join("STAR", fn)
        if not os.path.isfile(starfile):
            missing_files.append(f)
    if len(missing_files) > 0 and len(missing_files) != len(all_mics):
        print(
            "WARNING: No particle coordinate files were produced for the following"
            " files:"
        )
        for f in missing_files:
            print(f" - {f}")

    elif len(missing_files) == len(all_mics):
        raise ValueError("No particle coordinate files were written")

    d = cif.Document()
    block = d.add_new_block("coordinate_files")
    loop = block.init_loop("", ["_rlnMicrographName", "_rlnMicrographCoordinates"])
    for mic in all_mics:
        coord_fn = os.path.basename(mic).split(".")[0] + ".star"
        coord_file = os.path.join(args.outdir, "STAR/", coord_fn)
        loop.add_row([mic, coord_file])

    write(d, os.path.join("autopick.star"))


if __name__ == "__main__":
    main()
