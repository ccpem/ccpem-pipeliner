#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Union
from pipeliner.utils import format_string_to_type_objs
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmSpecimen,
    EmImaging,
    EmSampleSupport,
    EmSoftware,
)

ebic_csv_fields = [
    "Proposal",
    "Visit",
    "Visit type",
    "BAG",
    "Institute",
    "Anonymous",
    "Grid_type",
    "Hole (um)",
    "Space (um)",
    "Instrument",
    "Session date",
    "Directory",
    "Error",
    "Type",
    "Advisory",
    "EPU Session",
    "EPU session date",
    "Tilt",
    "Defocus max (um)",
    "Defocus min (um)",
    "Spot",
    "C2",
    "Beam_um",
    "Mag_X",
    "apix (A/px)",
    "Slit",
    "Slit width",
    "Atlas_mag",
    "Atlas_apix",
    "Atlas_probe",
    "Atlas_spot",
    "Atlas_C2",
    "Atlas_beam",
    "Atlas_DF",
    "Sq_mag",
    "Sq_apix",
    "Sq_probe",
    "Sq_spot",
    "Sq_C2",
    "Sq_beam",
    "Sq_DF",
    "Hole_mag",
    "Hole_apix",
    "Hole_probe",
    "Hole_spot",
    "Hole_C2",
    "Hole_beam",
    "Hole_DF",
    "Data_mag",
    "Data_apix",
    "Data_probe",
    "Data_spot",
    "Data_C2",
    "Data_beam",
    "Data_DF",
    "epuVersion",
]


def check_file_is_ebic_csv(input_file):
    """Check if a file is in the eBIC csv microscope data format"""
    if not os.path.isfile(input_file):
        return False
    with open(input_file) as f:
        headers = [x.strip("\n") for x in f.readlines()[0].split(",")]
    if headers == ebic_csv_fields:
        return True
    else:
        return False


def parse_ebic_csv(
    input_file: str, output_dir: str
) -> List[Union[EmSpecimen, EmImaging, EmSampleSupport, EmSoftware]]:
    with open(input_file) as f:
        raw_data = [x.split(",") for x in f.readlines()]
    if len(raw_data[0]) != len(raw_data[1]) or len(raw_data) != 2:
        raise ValueError(
            "Error in file format; number of columns doesn't match or wrong number "
            "of lines"
        )
    data = {}
    for n in range(len(raw_data[0])):
        data[raw_data[0][n].strip("\n")] = format_string_to_type_objs(raw_data[1][n])

    imaging = EmImaging(
        tilt_angle_max=data.get("Tilt"),  # type: ignore[arg-type]
        tilt_angle_min=data.get("Tilt"),  # type: ignore[arg-type]
        sample_support_id=f"JOBREF: {output_dir}",  # type: ignore[arg-type]
        nominal_defocus_min=data.get("Defocus min (um)"),  # type: ignore[arg-type]
        nominal_defocus_max=data.get("Defocus max (um)"),  # type: ignore[arg-type]
        nominal_magnification=data.get("Mag_x"),  # type: ignore[arg-type]
    )
    deets = None
    if data.get("Grid_type") == "HoleyCarbon":
        deets = (
            f"Hole size: {data.get('Hole (um)')}; Hole spacing "
            f"{data.get('Space (um)')}"
        )
    gridtype = str(data.get("Grid_type")) if data.get("Grid_type") is not None else None
    sample_support = EmSampleSupport(
        grid_type=gridtype,
        details=deets,
    )
    specimen = EmSpecimen()
    vers = str(data.get("epuVersion")) if data.get("epuVersion") is not None else None
    epu = EmSoftware(category="IMAGE ACQUISITION", name="EPU", version=vers)
    return [imaging, sample_support, specimen, epu]
