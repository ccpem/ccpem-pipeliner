#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import pandas as pd
from collections import OrderedDict
import json
import numpy as np
import warnings
from ccpem_utils.other.cluster import cluster_coord_features
from ccpem_utils.other.utils import extract_numeric_from_string
from pipeliner.starfile_handler import JobStar
from typing import Dict, List, Sequence, Optional, IO, Tuple, Union, TypedDict


class ModelValidationResultsParser(object):
    """
    Results viewer for job pipeline
    """

    def __init__(self) -> None:
        job_star = JobStar("job.star")
        job_options: Dict = job_star.all_options_as_dict()
        self.input_model: str = job_options["input_model"]
        self.modelid: str = os.path.splitext(os.path.basename(self.input_model))[0]
        input_map: str = job_options["input_map"]
        self.mapid: str = os.path.splitext(os.path.basename(input_map))[0]
        try:
            self.map_resolution: Optional[float] = float(job_options["resolution"])
        except (TypeError, ValueError):
            self.map_resolution = None
        # per-residue plots
        # self.dict_residue_plot[m][c][outlier] = [residue,score]
        self.dict_residue_plot: Dict[
            str, Dict[str, Dict[str, List[List[Union[int, float]]]]]
        ] = {}
        # iris plot data
        # self.dict_iris_data["b-factor"] = {c:{r: []}}
        dict_iris_data = TypedDict(
            "dict_iris_data",
            {
                "molprobity": Dict[str, Dict[str, List]],
                "rama_z": Dict[str, Dict[str, List]],
                "b_factor": Dict[str, Dict[str, List]],
                "map_fit": List,
            },
        )  # Dict[str, Union[List, Dict[str, Dict[str, List]]]] = {}
        self.dict_iris_data: dict_iris_data = {
            "molprobity": {},
            "rama_z": {},
            "b_factor": {},
            "map_fit": [None, {}],
        }
        # outlier clusters
        # self.dict_outlier_clusters[str(cl_num)] = list res
        self.dict_outlier_clusters: Dict[str, List[str]] = OrderedDict()
        # dictionary with residue outlier details for each method
        # self.residue_outliers[m][c][r] = [outlier_str]
        self.residue_outliers: Dict[str, Dict[str, Dict[str, List[str]]]] = (
            OrderedDict()
        )
        # dictionary with residue coordinates for each model
        self.residue_coordinates: Dict[str, Sequence[float]] = OrderedDict()
        ca_coord_json = self.modelid + "_residue_coordinates.json"
        with open(ca_coord_json, "r") as cj:
            self.dict_ca_coord = json.load(cj)
        # TODO: get b-fact results for plot
        # output report for global results
        gr = open("global_report.txt", "w")
        run_molprobity = job_options["run_molprobity"] == "Yes"
        run_fsc = job_options["run_fsc"] == "Yes"
        run_smoc = job_options["run_smoc"] == "Yes"
        run_tortoize = False  # job_options["run_tortoize"] == "Yes"
        run_fdr = job_options["run_fdr"] == "Yes"
        quick_eval = job_options["quick_eval"] == "Yes"
        if run_molprobity:
            self.set_molprobity_results(gr)
        if run_smoc:
            self.set_smoc_results(gr)
        if not quick_eval:
            if run_fsc:
                self.set_servalcat_fsc_results(gr)
            if run_tortoize:
                self.set_tortoize_results(gr)
            if run_fdr:
                self.set_fdrbackbone_results(gr)

        self.get_outlier_coordinates()
        if self.residue_coordinates:
            self.cluster_outliers()
            self.get_outlier_summary()
        self.save_plot_data()
        if len(self.dict_iris_data) > 0:
            self.save_iris_data()

    def save_plot_data(self):
        """
        Save json for outlier plot
        """
        with open(self.modelid + "_plot_data.json", "w") as j:
            json.dump(self.dict_residue_plot, j)

    def set_bfactor_results(self) -> None:
        bfact_json = self.modelid + "_residue_bfactors.json"
        with open(bfact_json, "r") as bj:
            bfact_data = json.load(bj)
        try:
            max_bfact_dev = max(round(bfact_data["0"]["0"]["max_dev"][1], 1), 10.0)
        except KeyError:
            max_bfact_dev = 10.0
        bfact_iris_data: Dict[str, Dict[str, List]] = {}
        bfactor_plotname = "b-factor_dev/" + str(max_bfact_dev)
        model1 = False
        if "1" in bfact_data:
            model1 = True
        count_models = 0
        for m in bfact_data:
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in bfact_data[m]:
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in bfact_data[m][c]:
                    if r == "max_dev":
                        continue  # skip max deviation value stored
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        bfactor_plotname,
                        r,
                        round(bfact_data[m][c][r][1] / max_bfact_dev, 3),
                    )
                    if (model1 and m == "1") or (not model1 and count_models == 0):
                        try:
                            bfact_iris_data[c][r] = bfact_data[m][c][r]
                        except KeyError:
                            bfact_iris_data[c] = {r: bfact_data[m][c][r]}
            count_models += 1

        # save iris data
        self.dict_iris_data["b_factor"] = bfact_iris_data

    def set_molprobity_results(self, gr: IO):
        mp_json = self.modelid + "_molprobity_summary.json"
        with open(mp_json, "r") as mpj:
            mp_data = json.load(mpj)
        gr.write("*Molprobity* model geometry\n")
        gr.write("===========================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
            "{:<60}".format("Expected/Percentile"),
        ]
        gr.write("\t".join(labels) + "\n")
        molprob_data = []
        for metric in mp_data:
            molp_metric = " ".join(metric.split("_"))
            score = mp_data[metric][0]
            pct = mp_data[metric][1]
            molprob_data.append(
                [
                    "{:<25}".format(molp_metric),
                    "{:<7}".format(score),
                    "{:<60}".format(pct),
                ]
            )
            gr.write("\t".join(molprob_data[-1]) + "\n")
        gr.write("\n\n")

        mp_res_json = self.modelid + "_residue_molprobity_outliers.json"
        with open(mp_res_json, "r") as mpj:
            mp_data = json.load(mpj)
        for m in mp_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in mp_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in mp_data[m][c]:
                    for outlier in mp_data[m][c][r]:
                        outlier_str = outlier[0]
                        if outlier[1]:
                            outlier_str += " (" + outlier[1] + ")"
                        try:
                            self.residue_outliers[m][c][r].append(outlier_str)
                        except KeyError:
                            self.residue_outliers[m][c][r] = [outlier_str]
                    # save for outlier plots
                    # adding 0.25 as y values for the plot
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c], "molprobity", r, 0
                    )
        iris_json = self.modelid + "_molprobity_iris.json"

        if os.path.isfile(iris_json):
            with open(iris_json, "r") as mpj:
                iris_data = json.load(mpj)
            try:
                self.dict_iris_data["molprobity"] = iris_data["molprobity"]
            except KeyError:
                warnings.warn("Molprobity Iris data not found")

    def set_servalcat_fsc_results(self, gr: IO):
        dict_fsc_out: Dict[str, float] = {}
        dict_fsc_plot_out: Dict[str, List[Union[float, int]]] = {}
        modelmask_out = "modelmask_fsc.dat"
        self.parse_servalcat_fsc(
            modelmask_out, dict_fsc_out, dict_fsc_plot_out, attribute_suffix="modelmask"
        )
        mapmask_out = "mapmask_fsc.dat"
        if os.path.isfile(mapmask_out):
            self.parse_servalcat_fsc(
                mapmask_out, dict_fsc_out, dict_fsc_plot_out, attribute_suffix="mapmask"
            )
        gr.write("*Servalcat* model-map fsc\n")
        gr.write("=========================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
        ]
        gr.write("\t".join(labels) + "\n")
        for metric in [
            "FSCavg_modelmask",
            "FSCavg_h1_modelmask",
            "FSCavg_h2_modelmask",
        ]:
            if metric in dict_fsc_out:
                gr.write(
                    "\t".join(
                        [
                            "{:<25}".format(metric),
                            "{:<7}".format(str(round(dict_fsc_out[metric], 3))),
                        ]
                    )
                    + "\n"
                )
        gr.write("\n\n")
        with open(self.modelid + "_" + self.mapid + "_fsc_plot.json", "w") as j:
            json.dump(dict_fsc_plot_out, j)
        with open(self.modelid + "_" + self.mapid + "_fsc_summary.json", "w") as j:
            json.dump(dict_fsc_out, j)

    @staticmethod
    def calc_fscavg(
        list_nref: List[float], list_fsc: List[float]
    ) -> Tuple[float, float]:
        sum_shells = 0.0
        weighted_fsc_sum = 0.0
        flag05 = 0
        sum_shells05 = 0.0
        weighted_fsc_sum05 = 0.0
        for ind in range(len(list_nref)):
            # calc in reverse
            weighted_fsc_sum += float(list_fsc[-(ind + 1)] * list_nref[-(ind + 1)])
            sum_shells += list_nref[-(ind + 1)]
            if list_fsc[-(ind + 1)] >= 0.5:
                flag05 = 1
            if flag05:
                weighted_fsc_sum05 += float(
                    list_fsc[-(ind + 1)] * list_nref[-(ind + 1)]
                )
                sum_shells05 += list_nref[-(ind + 1)]
        try:
            fsc_avg = weighted_fsc_sum / sum_shells
        except ZeroDivisionError:
            fsc_avg = 0.0
        try:
            fsc_avg05 = weighted_fsc_sum05 / sum_shells05
        except ZeroDivisionError:
            fsc_avg05 = 0.0
        return fsc_avg, fsc_avg05

    def parse_servalcat_fsc(
        self,
        fsc_outfile: str,
        dict_fsc_out: Dict[str, float],
        dict_fsc_plot_out: Dict[str, List[Union[float, int]]],
        attribute_suffix: str,
    ):
        num_columns = 0
        with open(fsc_outfile, "r") as s:
            for line in s:
                lsplit = line.split()
                if lsplit[0] == "#":
                    if lsplit[1] == "FSCaverage":
                        if lsplit[3] == "fsc_FC_full":
                            dict_fsc_out["FSCavg" + "_" + attribute_suffix] = float(
                                lsplit[-1]
                            )
                        elif lsplit[3] == "fsc_FC_half1":
                            dict_fsc_out["FSCavg_h1" + "_" + attribute_suffix] = float(
                                lsplit[-1]
                            )
                        elif lsplit[3] == "fsc_FC_half2":
                            dict_fsc_out["FSCavg_h2" + "_" + attribute_suffix] = float(
                                lsplit[-1]
                            )
                    continue
                elif lsplit[0] == "d_min":
                    num_columns = len(lsplit)
                    col_headers = lsplit
                    continue
                if num_columns > 0:  # column headers read
                    d_min = float(lsplit[col_headers.index("d_min")])
                    d_max = float(lsplit[col_headers.index("d_max")])
                    fsc_full = lsplit[col_headers.index("fsc_FC_full")]
                    try:
                        dict_fsc_plot_out["resolution" + "_" + attribute_suffix].append(
                            (d_min + d_max) / 2.0
                        )
                        dict_fsc_plot_out["FSC" + "_" + attribute_suffix].append(
                            float(fsc_full)
                        )
                    except KeyError:
                        dict_fsc_plot_out["resolution" + "_" + attribute_suffix] = [
                            (d_min + d_max) / 2.0
                        ]
                        dict_fsc_plot_out["FSC" + "_" + attribute_suffix] = [
                            float(fsc_full)
                        ]
                    if "ncoeffs" in col_headers:
                        ncoeffs = int(float(lsplit[col_headers.index("ncoeffs")]))
                        try:
                            dict_fsc_plot_out[
                                "ncoeffs" + "_" + attribute_suffix
                            ].append(ncoeffs)
                        except KeyError:
                            dict_fsc_plot_out["ncoeffs" + "_" + attribute_suffix] = [
                                ncoeffs
                            ]
                    self.parse_servalcat_halfmap_data(
                        lsplit, col_headers, dict_fsc_plot_out, attribute_suffix
                    )
        if "ncoeffs" + "_" + attribute_suffix in dict_fsc_plot_out:
            fscavg05 = self.calc_fscavg(
                dict_fsc_plot_out["ncoeffs" + "_" + attribute_suffix],
                dict_fsc_plot_out["FSC" + "_" + attribute_suffix],
            )[1]
            if fscavg05:
                dict_fsc_out["FSCavg" + "_" + attribute_suffix + "_FSC0.5"] = fscavg05
            del dict_fsc_plot_out["ncoeffs" + "_" + attribute_suffix]

    @staticmethod
    def parse_servalcat_halfmap_data(
        lsplit: List[str],
        col_headers: List[str],
        dict_fsc_plot_out: Dict[str, List[float]],
        attribute_suffix: str,
    ):
        if "fsc_FC_half1" in col_headers and "fsc_FC_half2" in col_headers:
            fsc_half1 = lsplit[col_headers.index("fsc_FC_half1")]
            fsc_half2 = lsplit[col_headers.index("fsc_FC_half2")]
            try:
                dict_fsc_plot_out["FSC_h1" + "_" + attribute_suffix].append(
                    float(fsc_half1)
                )
                dict_fsc_plot_out["FSC_h2" + "_" + attribute_suffix].append(
                    float(fsc_half2)
                )
            except KeyError:
                dict_fsc_plot_out["FSC_h1" + "_" + attribute_suffix] = [
                    float(fsc_half1)
                ]
                dict_fsc_plot_out["FSC_h2" + "_" + attribute_suffix] = [
                    float(fsc_half2)
                ]
        if "fsc_half_masked_corrected" in col_headers:
            fsc_half = lsplit[col_headers.index("fsc_half_masked_corrected")]
            try:
                dict_fsc_plot_out["halfmap_FSC" + "_" + attribute_suffix].append(
                    float(fsc_half)
                )
            except KeyError:
                dict_fsc_plot_out["halfmap_FSC" + "_" + attribute_suffix] = [
                    float(fsc_half)
                ]

    def set_tempy_glob_results(self, gr: IO):
        globscores_csv = (
            "globscores_{}_vs_{}".format(
                self.mapid,
                self.modelid,
            )
            + ".csv"
        )
        dict_glob_out = self.parse_glob_scores(globscores_csv)
        gr.write("*TEMPy* global scores\n")
        gr.write("=====================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
        ]
        gr.write("\t".join(labels) + "\n")
        for metric in dict_glob_out:
            gr.write(
                "\t".join(
                    [
                        "{:<25}".format(metric),
                        "{:<7}".format(str(round(dict_glob_out[metric], 3))),
                    ]
                )
                + "\n"
            )
        gr.write("\n\n")

    @staticmethod
    def parse_glob_scores(scores_csv: str) -> Dict[str, float]:
        dict_scores: Dict[str, float] = {}
        with open(scores_csv, "r") as c:
            for line in c:
                lsplit = line.split(",")
                dict_scores[lsplit[0].strip()] = float(lsplit[1].strip())
        return dict_scores

    @staticmethod
    def add_residue_to_plot(
        dict_residue_plot: Dict[str, List[List[Union[int, float]]]],
        outlier_name: str,
        residue_num: str,
        score: float,
    ):
        """
        Add residue outlier to the outlier plot dictionary
        """
        # TODO : insertion codes not handled
        try:
            residue = int(residue_num)
        except (TypeError, ValueError):
            try:
                residue = int(extract_numeric_from_string(residue_num)[0])
            except (TypeError, ValueError, IndexError) as e:
                print(e)
                print(
                    "Skipping {} outlier {} from the plot".format(
                        outlier_name, residue_num
                    )
                )
                return
        try:
            # overwrite same residue numbers
            if residue not in dict_residue_plot[outlier_name][0]:
                dict_residue_plot[outlier_name][0].append(int(residue))
                dict_residue_plot[outlier_name][1].append(score)
        except KeyError:
            dict_residue_plot[outlier_name] = [
                [residue],
                [score],
            ]

    def set_smoc_results(self, gr: IO):
        smoc_res_json = self.modelid + "_" + self.mapid + "_residue_smoc.json"
        with open(smoc_res_json, "r") as smocj:
            smoc_data = json.load(smocj)
        count_total_res = 0
        count_outlier_res = 0
        for m in smoc_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in smoc_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in smoc_data[m][c]:
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c], "smoc", r, smoc_data[m][c][r][0]
                    )
                    if smoc_data[m][c][r][1] < -2.0:  # z < -1.5
                        try:
                            self.residue_outliers[m][c][r].append(
                                "smoc"
                                + " (raw:"
                                + str(round(float(smoc_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(smoc_data[m][c][r][1]), 3))
                                + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "smoc"
                                + " (raw:"
                                + str(round(float(smoc_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(smoc_data[m][c][r][1]), 3))
                                + ")"
                            ]
                        count_outlier_res += 1
                        # add outlier to plot
                        self.add_residue_to_plot(
                            self.dict_residue_plot[m][c],
                            "smoc_outlier",
                            r,
                            smoc_data[m][c][r][0],
                        )
                    count_total_res += 1
        self.set_global_smoc_results(gr, count_outlier_res, count_total_res)
        self.set_smoc_iris_data()

    def set_smoc_iris_data(self) -> None:
        iris_json = self.modelid + "_" + self.mapid + "_smoc_iris.json"
        if os.path.isfile(iris_json):
            with open(iris_json, "r") as sj:
                iris_data = json.load(sj)
            if "map_fit" not in self.dict_iris_data:
                self.dict_iris_data["map_fit"] = [self.map_resolution, {}]
            try:
                for c in iris_data["res_fit"]:
                    if c not in self.dict_iris_data["map_fit"][1]:
                        self.dict_iris_data["map_fit"][1][c] = {}
                    for n in iris_data["res_fit"][c]:
                        # map fit scores are stored as triplets
                        # (residue, main and side chain scores)
                        if n not in self.dict_iris_data["map_fit"][1][c]:
                            self.dict_iris_data["map_fit"][1][c][n] = [[], []]
                        try:
                            self.dict_iris_data["map_fit"][1][c][n][0][0] = iris_data[
                                "res_fit"
                            ][c][n][0]
                        except (KeyError, IndexError):
                            self.dict_iris_data["map_fit"][1][c][n][0] = [
                                iris_data["res_fit"][c][n][0],
                                None,
                                None,
                            ]
                        try:
                            self.dict_iris_data["map_fit"][1][c][n][1][0] = iris_data[
                                "res_fit"
                            ][c][n][1]
                        except (KeyError, IndexError):
                            self.dict_iris_data["map_fit"][1][c][n][1] = [
                                iris_data["res_fit"][c][n][1],
                                None,
                                None,
                            ]
            except KeyError:
                warnings.warn("SMOC Iris data not found")

    @staticmethod
    def set_global_smoc_results(
        gr: IO, count_outlier_res: int, count_total_res: int
    ) -> None:
        gr.write("*SMOC* model fit\n")
        gr.write("================\n")
        labels = ["{:<25}".format("#Metric"), "{:<7}".format("Score")]
        gr.write("\t".join(labels) + "\n")
        gr.write(
            "\t".join(
                [
                    "{:<25}".format("SMOC-Z < -1.5"),
                    "{:<7}".format(
                        str(round(float(count_outlier_res) * 100 / count_total_res, 3))
                        + "%"
                    ),
                ]
            )
            + "\n"
        )
        gr.write("\n\n")

    def set_fdrbackbone_results(self, gr: IO):
        fdrscore_res_json = self.modelid + "_" + self.mapid + "_residue_fdrscore.json"
        with open(fdrscore_res_json, "r") as fdrscorej:
            fdrscore_data = json.load(fdrscorej)
        count_total_res = 0
        count_outlier_res = 0
        for m in fdrscore_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in fdrscore_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in fdrscore_data[m][c]:
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        "fdrscore",
                        r,
                        fdrscore_data[m][c][r][0],
                    )
                    if fdrscore_data[m][c][r][1] < -2.0:  # z < -2.0
                        try:
                            self.residue_outliers[m][c][r].append(
                                "fdrscore"
                                + " (raw:"
                                + str(round(float(fdrscore_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(fdrscore_data[m][c][r][1]), 3))
                                + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "fdrscore"
                                + " (raw:"
                                + str(round(float(fdrscore_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(fdrscore_data[m][c][r][1]), 3))
                                + ")"
                            ]
                        count_outlier_res += 1
                        # add outlier to plot
                        self.add_residue_to_plot(
                            self.dict_residue_plot[m][c],
                            "fdrscore_outlier",
                            r,
                            fdrscore_data[m][c][r][0],
                        )
                    count_total_res += 1
        if count_total_res:
            gr.write("*FDR score* backbone trace\n")
            gr.write("==========================\n")
            labels = ["{:<25}".format("#Metric"), "{:<7}".format("Score")]
            gr.write("\t".join(labels) + "\n")
            gr.write(
                "\t".join(
                    [
                        "{:<25}".format("FDRscore-Z < -2.0"),
                        "{:<7}".format(
                            str(
                                round(
                                    float(count_outlier_res) * 100 / count_total_res, 3
                                )
                            )
                            + "%"
                        ),
                    ]
                )
                + "\n"
            )
            gr.write("\n\n")
            self.set_fdrscore_iris_data()

    def set_fdrscore_iris_data(self):
        iris_json = self.modelid + "_" + self.mapid + "_fdrscore_iris.json"
        if os.path.isfile(iris_json):
            with open(iris_json, "r") as sj:
                iris_data = json.load(sj)
            if "map_fit" not in self.dict_iris_data:
                self.dict_iris_data["map_fit"] = [self.map_resolution, {}]
            try:
                for c in iris_data["main_fit"]:
                    if c not in self.dict_iris_data["map_fit"][1]:
                        self.dict_iris_data["map_fit"][1][c] = {}
                    for n in iris_data["main_fit"][c]:
                        # map fit scores are stored as triplets
                        # (residue, main and side chain scores)
                        if n not in self.dict_iris_data["map_fit"][1][c]:
                            self.dict_iris_data["map_fit"][1][c][n] = [[], []]
                        try:
                            self.dict_iris_data["map_fit"][1][c][n][0][1] = iris_data[
                                "main_fit"
                            ][c][n][0]
                        except (KeyError, IndexError):
                            self.dict_iris_data["map_fit"][1][c][n][0] = [
                                None,
                                iris_data["main_fit"][c][n][0],
                                None,
                            ]
                        try:
                            self.dict_iris_data["map_fit"][1][c][n][1][1] = iris_data[
                                "main_fit"
                            ][c][n][1]
                        except (KeyError, IndexError):
                            self.dict_iris_data["map_fit"][1][c][n][1] = [
                                None,
                                iris_data["main_fit"][c][n][1],
                                None,
                            ]
            except KeyError:
                warnings.warn("FDRscore Iris data not found")

    def set_global_tortoize_results(self, gr: IO):
        tortoize_json = self.modelid + "_tortoize_summary.json"
        with open(tortoize_json, "r") as mpj:
            t_data = json.load(mpj)
        gr.write("*Tortoize* model(1) geometry\n")
        gr.write("===========================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
            "{:<60}".format("Expected/Percentile"),
        ]
        gr.write("\t".join(labels) + "\n")
        score_data = []
        for model in t_data:
            for metric in t_data[model]:
                score = round(float(t_data[model][metric][0]), 3)
                pct = t_data[model][metric][1]  # TODO: add expected ranges
                score_data.append(
                    "\t".join(
                        [
                            "{:<25}".format(metric),
                            "{:<7}".format(score),
                            "{:<60}".format(pct),
                        ]
                    )
                )
            gr.write("\n".join(score_data) + "\n")
            break
        gr.write("\n\n")

    def set_tortoize_results(self, gr: IO):
        self.set_global_tortoize_results(gr)

        res_json = self.modelid + "_residue_tortoize.json"
        with open(res_json, "r") as j:
            res_data = json.load(j)
        for m in res_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in res_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in res_data[m][c]:
                    outlier = res_data[m][c][r]
                    if abs(float(outlier[1])) > 2:
                        try:
                            self.residue_outliers[m][c][r].append(
                                "rama_z" + " (" + str(round(float(outlier[1]), 3)) + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "rama_z" + " (" + str(round(float(outlier[1]), 3)) + ")"
                            ]
                    if abs(float(outlier[2])) > 2:
                        try:
                            self.residue_outliers[m][c][r].append(
                                "torsion_z"
                                + " ("
                                + str(round(float(outlier[2]), 3))
                                + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "torsion_z"
                                + " ("
                                + str(round(float(outlier[2]), 3))
                                + ")"
                            ]
                    # save for outlier plots
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        "tortoize_ramaz/2",
                        r,
                        round(float(outlier[1]) / 2.0, 3),
                    )
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        "tortoize_torsionz/2",
                        r,
                        round(float(outlier[2]) / 2.0, 3),
                    )
        iris_json = self.modelid + "_tortoize_iris.json"
        if os.path.isfile(iris_json):
            with open(iris_json, "r") as mpj:
                iris_data = json.load(mpj)
            try:
                self.dict_iris_data["rama_z"] = iris_data
            except KeyError:
                warnings.warn("Ramachandran-Z Iris data not found")

    def get_outlier_coordinates(self):
        for m in self.residue_outliers:
            for c in self.residue_outliers[m]:
                for r in self.residue_outliers[m][c]:
                    residue_id = "_".join([m, c, r])
                    try:
                        self.residue_coordinates[residue_id] = self.dict_ca_coord[m][c][
                            r
                        ][0]
                    except KeyError:
                        # TODO: mismatch of residue IDs?
                        pass

    def cluster_outliers(self) -> None:
        """
        Cluster outlier residues based on spatial coordinates
        Uses sklearn DBSCAN
        """
        dict_outlier_clusters: Dict[int, List] = {}
        list_res_id = []
        list_ca_coord = []
        for rid in self.residue_coordinates:
            list_res_id.append(rid)
            list_ca_coord.append(self.residue_coordinates[rid])
        cluster_labels = cluster_coord_features(
            np.array(list_ca_coord), dbscan_eps=5.5, norm=False
        )
        for c in range(len(cluster_labels)):
            try:
                dict_outlier_clusters[cluster_labels[c]].append(list_res_id[c])
            except KeyError:
                dict_outlier_clusters[cluster_labels[c]] = [list_res_id[c]]
        # sort by cluster size
        dict_outlier_clusters = OrderedDict(
            sorted(
                dict_outlier_clusters.items(),
                key=lambda x: len(x[1]),
                reverse=True,
            )
        )
        # rename
        cl_num = 1
        for lbl, lres in dict_outlier_clusters.items():
            if lbl == -1:
                self.dict_outlier_clusters["Unclustered"] = lres
            else:
                self.dict_outlier_clusters[str(cl_num)] = lres
                cl_num += 1

    def save_iris_data(self) -> None:
        iris_json = self.modelid + "_iris_data.json"
        with open(iris_json, "w") as j:
            json.dump(self.dict_iris_data, j)

    def get_outlier_summary(self) -> None:
        """
        Merge outlier types for each residue
        """
        list_cluster_outliers = []
        for cl_num in self.dict_outlier_clusters:
            # OrderedDict([(0, ['1_A_101', '1_A_102']), (1, ['1_B_105', '1_B_106'])])
            if cl_num == "Unclustered":
                continue  # skip unclustered residues
            for res_id in self.dict_outlier_clusters[cl_num]:
                m, c, r = res_id.split("_")
                try:
                    # smoc (0.521;-2.098)
                    # dihedral_angles (1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA)
                    outlier_string = " | ".join(self.residue_outliers[m][c][r])
                    list_cluster_outliers.append(
                        ",".join(
                            [
                                str(cl_num),
                                "_".join(res_id.split("_")[1:]),  # skip model number
                                outlier_string,
                            ]
                        )
                    )
                except KeyError:
                    print("Residue ID {} not found in outlier dict".format(res_id))
        cl_num = "Unclustered"
        try:
            for res_id in self.dict_outlier_clusters[cl_num]:
                m, c, r = res_id.split("_")
                try:
                    # smoc (0.521;-2.098)
                    # dihedral_angles (1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA)
                    outlier_string = " | ".join(self.residue_outliers[m][c][r])
                    list_cluster_outliers.append(
                        ",".join(
                            [
                                str(cl_num),
                                "_".join(res_id.split("_")[1:]),  # skip model number
                                outlier_string,
                            ]
                        )
                    )
                except KeyError:
                    print("Residue ID {} not found in outlier dict".format(res_id))
        except KeyError:
            pass
        with open("outlier_clusters.csv", "w") as oc:
            oc.write(
                "#Molprobity: geometry outliers reported by molprobity.\n"
                "#SMOC: local fit to map outliers reported by TEMPy SMOC.\n"
            )
            oc.write("\n".join(list_cluster_outliers))

    @staticmethod
    def set_df_csv(dict_data: Dict, data_id: str) -> None:
        df = pd.DataFrame.from_dict(dict_data, orient="index")
        # df1 = df.replace(np.nan,' ')
        df.to_csv(
            data_id + "_outliersummary.txt",
            sep=",",
        )


def main():
    ModelValidationResultsParser()


if __name__ == "__main__":
    main()
