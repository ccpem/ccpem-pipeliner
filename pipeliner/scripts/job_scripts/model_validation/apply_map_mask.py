import argparse
import os
from ccpem_utils.map.mrcfile_utils import mask_map


def parse_args():
    parser = argparse.ArgumentParser(description="lowpass filter mrc map")
    parser.add_argument(
        "-m",
        "--map",
        required=True,
        help="Input map (MRC)",
    )
    parser.add_argument(
        "-ma",
        "--mask",
        required=True,
        help="Input mask (MRC)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-ignore_edge",
        "--ignore_edge",
        default=False,
        help="Ignore non zero mask values (edge) ?",
        action="store_true",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    if args.odir:
        map_output = os.path.join(
            args.odir,
            os.path.splitext(os.path.basename(args.map))[0] + "_masked.mrc",
        )
    else:
        map_output = os.path.splitext(os.path.basename(args.map))[0] + "_masked.mrc"
    mask_map(
        args.map, args.mask, map_output=map_output, ignore_maskedge=args.ignore_edge
    )


if __name__ == "__main__":
    main()
