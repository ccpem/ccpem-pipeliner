#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from collections import OrderedDict
import re
import argparse
import json
import warnings
import sys
from typing import List, Tuple, Dict, Optional
from ccpem_utils.other.utils import extract_numeric_from_string

dict_referencerange: Dict[str, str] = OrderedDict(
    [
        ("Ramachandran_outliers", "< 0.05%"),
        ("Ramachandran_favored", "> 98%"),
        (
            "RamachandranZ_whole",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_helix",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_sheet",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_loop",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        ("Rotamer_outliers", "< 0.3%"),
        ("CBeta_deviations", "0"),
        ("Clashscore", " "),
        ("Molprobity_score", " "),
        ("Cis_proline", "0%"),
        ("Cis_general", "0%"),
        ("Rms_bonds", "< 0.02"),
        ("Rms_angles", "< 2.0"),
    ]
)


class MolprobityLogParser(object):
    """
    Parser for Molprobity stdout
    """

    def __init__(
        self,
        molprobity_out: str = "",
        model_id: str = "",
        rama_out: str = "",
        rota_out: str = "",
        clash_out_json: str = "",
        omega_out: str = "",
        cablam_out: str = "",
        resname_file: str = "",
        cootscript: str = "",
    ) -> None:
        self.stdout = molprobity_out
        self.dict_summary_raw: Dict[str, List[str]] = {}
        self.dict_summary: Dict[str, List[str]] = OrderedDict()
        self.dict_outliers: Dict[str, List[List[str]]] = OrderedDict()
        self.dict_res_outliers: Dict[str, dict] = OrderedDict()
        self.dict_outliers_headers: Dict[str, List[str]] = {}
        self.summary_start: bool = False
        self.outlier_start: bool = False
        self.dict_stat_len: Dict[str, Optional[int]] = {
            "dihedral_angles": 4,
            "bond_lengths": 2,
            "bond_angles": 3,
            "chiral_volumes": None,
            "planar_groups": None,
            "sugar_pucker": 3,
            "backbone_torsion_suites": 3,
        }
        self.model_id = model_id
        self.generate_iris_data = True
        self.rama_out = rama_out
        self.rota_out = rota_out
        self.clash_out = clash_out_json
        self.omega_out = omega_out
        self.cablam_out = cablam_out
        self.cootscript = cootscript
        self.ramaz_start: bool = False
        self.dict_rotalyze: Dict[str, dict] = OrderedDict()
        self.dict_ramalyze: Dict[str, dict] = OrderedDict()
        self.dict_omega: Dict[str, dict] = OrderedDict()
        self.dict_cbeta: Dict[str, dict] = OrderedDict()
        self.iris_data: Dict[str, dict] = {}

        if not molprobity_out or not (rama_out and rota_out):
            warnings.warn(
                "Molprobity run or both Ramalyze and Rotalyze results not found, "
                "skipping Molprobity from Iris data"
            )
            self.generate_iris_data = False

        if self.rama_out:
            self.get_ramalyze_outliers_from_json()
        if self.rota_out:
            self.get_rotalyze_outliers_from_json()
        if self.clash_out:
            self.get_clashscore_outliers_from_json()
        if self.cootscript:
            self.get_outliers_from_coot_script()
        if resname_file:
            with open(resname_file, "r") as j:
                self.dict_residue_names = json.load(j)

        stat = None
        if self.stdout:
            stdout_fh = open(self.stdout, "r")
            # parse the lines
            for line in stdout_fh:
                self.is_summary(line)
                self.is_rama_z(line)
                self.is_outlier(line)
                if self.summary_start:
                    self.get_summary(line[:-1])
                #                        ----------Bond lengths----------
                if "------" in line and line.strip()[-2] == "-":
                    stat = line.strip()
                    stat = stat.strip("-")
                    stat = "_".join([s.strip().lower() for s in stat.split()])
                if self.outlier_start and stat is not None:
                    if len(line.strip()) > 0:
                        self.get_outliers(line, stat)
            stdout_fh.close()
        if self.generate_iris_data:
            self.generate_iris_data_summary()
            self.generate_iris_data_residue()

    def is_rama_z(self, line: str):
        line_split = line.split()
        if not self.summary_start or len(line_split) == 0:
            self.ramaz_start = False
        else:
            if line_split[0].lower() == "rama-z":
                self.ramaz_start = True
            if (
                line[0] == "="
                and line_split[1].lower() == "summary"
                and line[-2] == "="
            ):
                self.ramaz_start = False

    @staticmethod
    def get_summary_value(line: str) -> str:
        if "=" in line:
            # Rotamer outliers      =   0.00 %
            return line.split("=")[1].strip()
        elif ":" in line:
            # whole: -5.68 (0.22), residues: 558
            return ":".join(line.split(":")[1:]).strip()
        return ""

    def get_ramaz_values(self, line: str) -> List[str]:
        ramaz_str = self.get_summary_value(line)
        list_ramaz = self.extract_numeric_from_string(ramaz_str)
        return list_ramaz

    @staticmethod
    def extract_numeric_from_string(string) -> List[str]:
        return re.findall(r"[-+]?(?:\d*\.*\d+)", string)

    @staticmethod
    def get_percentile(line: str) -> str:
        try:
            percentile_value = line.split("(percentile:")[1]
        except IndexError:
            return "NA"
        percentile_value = percentile_value.split()[0].strip()
        if percentile_value[-1] == ")":
            percentile_value = percentile_value[:-1]
        return percentile_value

    def get_summary(self, line: str):
        """
        Get global molprobity statistics from log
        NOTE that the pattern search is based on the log output
        """
        if self.ramaz_start:
            if "whole:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_whole"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_whole"],
                ]
            elif "helix:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_helix"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_helix"],
                ]
            elif "sheet:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_sheet"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_sheet"],
                ]
            elif "loop :" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_loop"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_loop"],
                ]
        elif "Ramachandran outliers" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Ramachandran_outliers"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Ramachandran_outliers"],
            ]
        elif "favored" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Ramachandran_favored"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Ramachandran_favored"],
            ]
        elif "Rotamer outliers" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rotamer_outliers"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Rotamer_outliers"],
            ]
        elif "Clashscore" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Clashscore"] = [
                self.extract_numeric_from_string(stat_str)[0],
                str(self.get_percentile(stat_str)) + " (percentile)",
            ]
        elif "C-beta deviations" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["CBeta_deviations"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["CBeta_deviations"],
            ]
        elif "RMS(bonds)" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rms_bonds"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Rms_bonds"],
            ]
        elif "RMS(angles)" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rms_angles"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Rms_angles"],
            ]
        elif "MolProbity score" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Molprobity_score"] = [
                self.extract_numeric_from_string(stat_str)[0],
                self.get_percentile(stat_str) + " (percentile)",
            ]
        elif "Cis-proline" in line and ":" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Cis_proline"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Cis_proline"],
            ]
        elif "Cis-general" in line and ":" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Cis_general"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Cis_general"],
            ]

    def is_summary(self, line: str):
        line_split = line.strip().split()
        # Molprobity Statistics.
        try:
            if (
                len(line_split) == 2
                and line_split[0].lower() == "molprobity"
                and line_split[1].lower() == "statistics."
            ):
                self.summary_start = True
                self.outlier_start = False
        except IndexError:
            pass
        # =============== Molprobity validation ================
        # This start includes bond lengths and angles deviation
        # if (
        #     line[0] == "="
        #     and line.split()[1].lower() == "molprobity"
        #     and line.split()[2].lower() == "validation"
        #     and line[-2] == "="
        # ):
        #     self.summary_start = True
        #     self.outlier_start = False
        # ================== Summary ==================
        if (
            line[0] == "="
            and line_split[1].lower() == "summary"
            and line.strip()[-2] == "="
        ):
            self.summary_start = True
            self.outlier_start = False

        try:
            if line_split[0] == "Results" or line_split[0] == "Refinement":
                self.summary_start = False
        except IndexError:
            pass

    def is_outlier(self, line: str):
        line_split = line.strip().split()
        if line[0] == "=" and line_split[1] == "Geometry":
            self.outlier_start = True
            self.summary_start = False
        if "------" in line and line.strip()[-2] == "-":
            self.summary_start = False
            self.outlier_start = True
            self.ramaz_start = False
        if (
            line[0] == "="
            and line_split[1].lower() == "molprobity"
            and line.strip()[-2] == "="
        ):
            self.outlier_start = False

    @staticmethod
    def get_outlier_id(line: str) -> List[str]:
        id_split = line[:19].strip().split()
        return id_split

    @staticmethod
    def get_outlier_atoms(line: str) -> Tuple[str, str, str]:
        chain_id = line[2:4].strip()
        resnum = line[4:9].strip()
        # resname = line[10:13].strip()
        atom_name = line[15:18].strip()
        return chain_id, resnum, atom_name

    @staticmethod
    def get_outlier_residue(line: str) -> Tuple[str, str]:
        # resname = line[11:13].strip()
        resnum = line[4:8].strip()
        chain_id = line[2:4].strip()
        return chain_id, resnum

    @staticmethod
    def get_outlier_suite(line: str) -> Tuple[str, str]:
        chain_id = line[6:8].strip()
        resnum = line[9:13].strip()
        # resname = line[3:6].strip()
        return chain_id, resnum

    def skip_outlier_lines(self, line: str, measure: str) -> bool:
        list_id_types = ["atoms", "residue", "Suite"]
        line_split = line.strip().split()
        if line_split[0] in list_id_types:
            self.dict_outliers_headers[measure] = line.strip().split()
            return True
        if line[2] != " " or len(line_split) < 3 or "delta:" in line:
            return True
        if (
            measure not in self.dict_outliers_headers
            or len(self.dict_outliers_headers[measure]) == 0
        ):  # asn/gln/his_flips
            return True
        return False

    def get_outlier_id_from_line(
        self, line: str, measure: str
    ) -> Optional[Tuple[str, str, str]]:
        # parse ids and values
        if self.dict_outliers_headers[measure][0] == "atoms":
            chain_id, resnum, atom_name = self.get_outlier_atoms(line)
            outlier_id = "_".join(["1", chain_id, resnum, atom_name])
            return chain_id, resnum, outlier_id
        elif self.dict_outliers_headers[measure][0] == "residue":
            if not line[:3] == "   ":
                return None
            chain_id, resnum = self.get_outlier_residue(line)
            outlier_id = "_".join(["1", chain_id, resnum])
            return chain_id, resnum, outlier_id
        elif self.dict_outliers_headers[measure][0] == "Suite":
            if not line[:3] == "   ":
                return None
            chain_id, resnum = self.get_outlier_suite(line)
            outlier_id = "_".join(["1", chain_id, resnum])
            return chain_id, resnum, outlier_id
        else:
            # TODO: any other outlier types?
            return None

    def get_outliers(self, line: str, measure: str):
        if self.skip_outlier_lines(line, measure):
            return
        line_split = line.strip().split()
        # parse ids and values
        outlier_details = self.get_outlier_id_from_line(line, measure)
        if not outlier_details:
            return
        chain_id, resnum, outlier_id = outlier_details

        try:
            if chain_id not in self.dict_res_outliers["1"]:
                self.dict_res_outliers["1"][chain_id] = {}
        except KeyError:
            self.dict_res_outliers["1"] = {chain_id: {}}
        try:
            self.dict_outliers[measure][-1].append(outlier_id)
        except KeyError:
            self.dict_outliers[measure] = [[outlier_id]]
        if len(line_split) > 4:
            sigma = ""
            if "sigma" in line_split[-1]:
                sigma = line_split[-1]
            if self.dict_outliers_headers[measure][0] == "atoms":
                atom_list = "-".join(self.dict_outliers[measure][-1])
                list_outlier_details = [measure, atom_list, sigma]
            else:
                list_outlier_details = [measure, sigma]
            # self.dict_res_outliers["1"][chain_id][resnum] =
            # [[measure1,sigma1],[measure2,""]]
            try:
                self.dict_res_outliers["1"][chain_id][resnum].append(
                    list_outlier_details
                )
            except KeyError:
                self.dict_res_outliers["1"][chain_id][resnum] = [list_outlier_details]

            # self.dict_outliers[measure] = [[atom1,atom2,..],[atom7,atom8,..]]
            try:
                # new atom set
                self.dict_outliers[measure].append([])
            except KeyError:
                self.dict_outliers[measure] = [[]]

    def get_outliers_from_coot_script(self):
        with open(self.cootscript, "r") as c:
            for line in c:
                if "=" not in line:
                    continue
                if line[:5] == "data[" and line.split()[1] == "=":
                    linesplit = line.strip().split(" = ")
                    measure = linesplit[0][6:-2]
                    # [('A', ' 141 ', 'ARG', 0.01136474356399822,
                    # (65.10899999999998, 45.318, 72.317)), ('C', ' 141 ', 'ARG', ...
                    outlier_str = linesplit[1][1:-1]
                    if not outlier_str:
                        continue
                    outlier_list = outlier_str.split("), (")
                    outlier_list[0] = outlier_list[0][1:]  # ('A',
                    outlier_list[-1] = outlier_list[-1][:-1]  # , 72.316)
                    if measure == "rota":
                        self.get_cootscript_rota_outliers(outlier_list)
                    elif measure == "rama":
                        self.get_cootscript_rama_outliers(outlier_list)
                    elif measure == "probe":
                        self.get_cootscript_clash_outliers(outlier_list)
                    elif measure == "omega":
                        self.get_cootscript_omega_outliers(outlier_list)
                    elif measure == "cbeta":
                        self.get_cootscript_cbeta_outliers(outlier_list)

    def get_chain_res_cootscript_clash_outlier(self, outlier: str) -> Tuple:
        # ' A 655  HIS  O  '
        split = outlier[1:-1].split()
        if len(split) == 4:
            chain, res, resname, atom = split
        elif len(split) == 3:
            res, resname, atom = split
            # ' A1310  NAG  H81'
            if len(res) > 4:
                chain = ""
                index_num = self.find_index_first_number(res)
                if index_num > 0:
                    chain = res[:index_num]
                res = extract_numeric_from_string(res)[0]
        return chain, res, resname, atom

    @staticmethod
    def find_index_first_number(input_str: str) -> int:
        ind_c = 0
        for c in input_str:
            try:
                int(c)
                return ind_c
            except (ValueError, TypeError):
                pass
            ind_c += 1
        return ind_c

    def get_cootscript_clash_outliers(self, outlier_list) -> None:
        # [(' A 655  HIS  O  ', ' A1310  NAG  H81', -0.508,
        # (171.433, 227.409, 185.822)), (' A 520  ALA  HB1', ' A 521  PRO  HD2', -0.503,
        for outlier in outlier_list:
            out_split = outlier.split(", ")
            chain1, res1, resname1, atom1 = self.get_chain_res_cootscript_clash_outlier(
                out_split[0]
            )
            chain2, res2, resname2, atom2 = self.get_chain_res_cootscript_clash_outlier(
                out_split[1]
            )
            try:
                if chain1 not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain1] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain1, OrderedDict())])
            list_outlier_details: List[str] = [
                "bad_clash",
                "atom:" + atom1 + " - " + "_".join([chain2, res2, atom2]),
                "",
            ]
            try:
                self.dict_res_outliers["1"][chain1][res1].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain1][res1] = [list_outlier_details]

            try:
                if chain2 not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain2] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain2, OrderedDict())])
            list_outlier_details = [
                "bad_clash",
                "atom:" + atom2 + " - " + "_".join([chain1, res1, atom1]),
                "",
            ]
            try:
                self.dict_res_outliers["1"][chain2][res2].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain2][res2] = [list_outlier_details]

    def get_cootscript_rota_outliers(self, outlier_list: List[str]):
        for outlier in outlier_list:
            out_split = outlier.split(", ")
            try:
                chain = out_split[0][1:-1]
            except IndexError:
                chain = ""
            resnum = out_split[1][1:-1].strip()
            if chain not in self.dict_rotalyze:
                self.dict_rotalyze[chain] = OrderedDict()
            self.dict_rotalyze[chain][resnum] = "outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            list_outlier_details = [
                "rotamer_outlier",
                "",
                res_id,
            ]
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]

    def get_cootscript_rama_outliers(self, outlier_list: List[str]):
        for outlier in outlier_list:
            out_split = outlier.split(", ")
            try:
                chain = out_split[0][1:-1]
            except IndexError:
                chain = ""
            resnum = out_split[1][1:-1].strip()
            if chain not in self.dict_ramalyze:
                self.dict_ramalyze[chain] = OrderedDict()
            self.dict_ramalyze[chain][resnum] = "outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            list_outlier_details = [
                "ramachandran_outlier",
                "",
                res_id,
            ]
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]

    def get_cootscript_cbeta_outliers(self, outlier_list: List[str]):
        for outlier in outlier_list:
            out_split = outlier.split(", ")
            try:
                chain = out_split[0][1:-1].strip()
            except IndexError:
                chain = ""
            resnum = out_split[1][1:-1].strip()
            if chain not in self.dict_cbeta:
                self.dict_cbeta[chain] = OrderedDict()
            self.dict_cbeta[chain][resnum] = "outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            list_outlier_details = [
                "cbeta_outlier",
                "",
                res_id,
            ]
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]

    def get_cootscript_omega_outliers(self, outlier_list: List[str]):
        for outlier in outlier_list:
            out_split = outlier.split(", ")
            try:
                chain = out_split[0][1:-1]
            except IndexError:
                chain = ""
            resnum = out_split[1][1:-1].strip()
            if chain not in self.dict_omega:
                self.dict_omega[chain] = OrderedDict()
            self.dict_omega[chain][resnum] = "outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            list_outlier_details = [
                "omega_outlier",
                "",
                res_id,
            ]
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]

    def remove_empty_outliers(self):
        for stat in self.dict_outliers:
            if not self.dict_outliers[stat][-1]:
                try:
                    self.dict_outliers[stat] = self.dict_outliers[stat][:-1]
                except IndexError:
                    del self.dict_outliers[stat]

    def get_ramalyze_outliers_from_json(self):
        self.dict_ramalyze = OrderedDict()
        with open(self.rama_out, "r") as j:
            dict_ramalyze_out = json.load(j)
        list_residue_results = dict_ramalyze_out["flat_results"]
        for res_result in list_residue_results:
            if not res_result["outlier"]:
                continue
            chain = res_result["chain_id"].strip()
            resnum = res_result["resseq"].strip()  # no ins code
            # ins_code = res_clash["icode"]
            if chain not in self.dict_ramalyze:
                self.dict_ramalyze[chain] = OrderedDict()
            self.dict_ramalyze[chain][resnum] = "Outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            try:
                list_outlier_details = [
                    "ramachandran_outlier",
                    "phi:"
                    + str(round(res_result["phi"], 2))
                    + "_"
                    + "psi:"
                    + str(round(res_result["psi"], 2)),
                    res_id,
                ]
            except KeyError:
                continue
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]
        summary_results = dict_ramalyze_out["summary_results"]
        outlier_percent = None
        for m in summary_results:
            if not m:  # "" for single model
                outlier_percent = (
                    str(round(float(summary_results[""]["outlier_percentage"]), 2))
                    + "%"
                )
                reference_range = summary_results[""]["outlier_goal"]
                favored_percent = (
                    str(round(float(summary_results[""]["favored_percentage"]), 2))
                    + "%"
                )
                favored_reference_range = summary_results[""]["favored_goal"]
            elif m == "1":
                outlier_percent = (
                    str(round(float(summary_results["1"]["outlier_percentage"]), 2))
                    + "%"
                )
                reference_range = summary_results["1"]["outlier_goal"]
                favored_percent = (
                    str(round(float(summary_results["1"]["favored_percentage"]), 2))
                    + "%"
                )
                favored_reference_range = summary_results["1"]["favored_goal"]
        if not outlier_percent:
            warnings.warn(
                "Missing Ramachandran outlier percentage field in "
                f"json file {self.rama_out}"
            )
        else:
            self.dict_summary_raw["Ramachandran_outliers"] = [
                outlier_percent,
                reference_range,
            ]
            self.dict_summary_raw["Ramachandran_favored"] = [
                favored_percent,
                favored_reference_range,
            ]

    def get_rotalyze_outliers_from_json(self):
        self.dict_rotalyze = OrderedDict()
        with open(self.rota_out, "r") as j:
            dict_rotalyze = json.load(j)
        list_residue_results = dict_rotalyze["flat_results"]
        for res_result in list_residue_results:
            if not res_result["outlier"]:
                continue
            chain = res_result["chain_id"].strip()
            resnum = res_result["resseq"].strip()  # no ins code
            # ins_code = res_clash["icode"]
            if chain not in self.dict_rotalyze:
                self.dict_rotalyze[chain] = OrderedDict()
            self.dict_rotalyze[chain][resnum] = "Outlier"
            try:
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain, OrderedDict())])
            res_id = "_".join(["1", chain, str(resnum)])
            chi_angles = [
                str(round(chi, 3)) if isinstance(chi, float) else ""
                for chi in res_result["chi_angles"]
            ]
            try:
                list_outlier_details = [
                    "rotamer_outlier",
                    "chi1:"
                    + chi_angles[0]
                    + "-"
                    + "chi2:"
                    + chi_angles[1]
                    + "-"
                    + "chi3:"
                    + chi_angles[2]
                    + "-"
                    + "chi4:"
                    + chi_angles[3],
                    res_id,
                ]
            except IndexError:
                continue
            try:
                self.dict_res_outliers["1"][chain][resnum].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain][resnum] = [list_outlier_details]
        summary_results = dict_rotalyze["summary_results"]
        outlier_percent = None
        for m in summary_results:
            if not m:  # "" for single model
                outlier_percent = (
                    str(round(float(summary_results[""]["outlier_percentage"]), 2))
                    + "%"
                )
                reference_range = summary_results[""]["outlier_goal"]
            elif m == "1":
                outlier_percent = (
                    str(round(float(summary_results["1"]["outlier_percentage"]), 2))
                    + "%"
                )
                reference_range = summary_results["1"]["outlier_goal"]
        if not outlier_percent:
            warnings.warn(
                f"Missing Rotamer outlier percentage field in json file {self.rota_out}"
            )
        else:
            self.dict_summary_raw["Rotamer_outliers"] = [
                outlier_percent,
                reference_range,
            ]

    def get_clashscore_outliers_from_json(self):
        with open(self.clash_out, "r") as j:
            dict_clashscore = json.load(j)
        list_residue_results = dict_clashscore["flat_results"]
        for res_clash in list_residue_results:
            if not res_clash["outlier"]:
                continue
            chain1 = res_clash["chain_id"].strip()
            res1 = res_clash["resseq"].strip()  # no ins code
            # ins_code = res_clash["icode"]
            atom1 = res_clash["name"].strip()
            chain2 = res_clash["target_chain_id"].strip()
            res2 = res_clash["target_resseq"].strip()
            atom2 = res_clash["target_name"].strip()
            try:
                if chain1 not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain1] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain1, OrderedDict())])
            list_outlier_details = [
                "bad_clash",
                "atom:" + atom1 + " - " + "_".join([chain2, res2, atom2]),
                "",
            ]
            try:
                self.dict_res_outliers["1"][chain1][res1].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain1][res1] = [list_outlier_details]

            try:
                if chain2 not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain2] = OrderedDict()
            except KeyError:
                self.dict_res_outliers["1"] = OrderedDict([(chain2, OrderedDict())])
            list_outlier_details = [
                "bad_clash",
                "atom:" + atom2 + " - " + "_".join([chain1, res1, atom1]),
                "",
            ]
            try:
                self.dict_res_outliers["1"][chain2][res2].append(list_outlier_details)
            except KeyError:
                self.dict_res_outliers["1"][chain2][res2] = [list_outlier_details]
        summary_results = dict_clashscore["summary_results"]
        self.get_clashscore_summary_from_json(summary_results)

    def get_clashscore_summary_from_json(self, summary_results):
        clashscore = None
        for m in summary_results:
            if not m:  # "" for single model
                clashscore = str(round(float(summary_results[""]["clashscore"]), 2))
            elif m == "1":
                clashscore = str(round(float(summary_results["1"]["clashscore"]), 2))
        if not clashscore:
            warnings.warn(f"Missing clashscore field in json file {self.clash_out}")
        # TODO: whether to include number of clashes
        try:
            self.dict_summary_raw["Clashscore"][0] = clashscore
        except (KeyError, IndexError):
            self.dict_summary_raw["Clashscore"] = [
                clashscore,
                "< 10; < 5 preferred",
            ]

    def generate_iris_data_summary(self):
        """
        Maps results onto a dictionary format that Iris display expects
        """
        self.iris_data["model_wide"] = {}
        self.iris_data["model_wide"]["summary"] = OrderedDict()
        try:
            self.iris_data["model_wide"]["summary"]["cbeta_deviations"] = float(
                self.dict_summary_raw["CBeta_deviations"][0]
            )
        except KeyError:
            warnings.warn("CBeta_deviations missing in Molprobity summary")
        try:
            self.iris_data["model_wide"]["summary"]["clashscore"] = float(
                self.dict_summary_raw["Clashscore"][0]
            )
        except KeyError:
            warnings.warn("Clashscore missing in Molprobity summary")
        try:
            self.iris_data["model_wide"]["summary"]["ramachandran_outliers"] = float(
                self.dict_summary_raw["Ramachandran_outliers"][0][:-1]
            )  # remove%
            self.iris_data["model_wide"]["summary"]["ramachandran_favoured"] = float(
                self.dict_summary_raw["Ramachandran_favored"][0][:-1]
            )  # remove%
        except KeyError:
            warnings.warn("Ramachandran outlier data missing in Molprobity summary")
        try:
            self.iris_data["model_wide"]["summary"]["rms_bonds"] = float(
                self.dict_summary_raw["Rms_bonds"][0]
            )
            self.iris_data["model_wide"]["summary"]["rms_angles"] = float(
                self.dict_summary_raw["Rms_angles"][0]
            )
        except KeyError:
            warnings.warn("Bond lengths/angles data missing in Molprobity summary")
        try:
            self.iris_data["model_wide"]["summary"]["rotamer_outliers"] = float(
                self.dict_summary_raw["Rotamer_outliers"][0][:-1]
            )  # remove%
        except KeyError:
            warnings.warn("Rotamer outlier data missing in Molprobity summary")
        try:
            self.iris_data["model_wide"]["summary"]["molprobity_score"] = float(
                self.dict_summary_raw["Molprobity_score"][0]
            )
        except KeyError:
            warnings.warn("Molprobity score data missing in Molprobity summary")

    @staticmethod
    def get_int(n: str, prefix_msg: str = "", suffix_msg: str = "") -> int:
        try:
            resnum = int(n)
        except (TypeError, ValueError) as e:
            if hasattr(e, "message"):
                warnings.warn(
                    prefix_msg
                    + "{} not an int".format(n)
                    + suffix_msg
                    + ":{}".format(e.message)
                )
            else:
                warnings.warn(
                    prefix_msg
                    + "{} not an int".format(n)
                    + suffix_msg
                    + ":{}".format(e)
                )
            raise ValueError
        return resnum

    def generate_iris_data_residue(self):
        """
        Maps per-residue results onto a dictionary format that Iris display expects
        """
        for c in self.dict_ramalyze:
            self.iris_data[c] = OrderedDict()
            for n in self.dict_ramalyze[c]:
                try:
                    resnum = self.get_int(n)
                except ValueError:
                    continue
                self.iris_data[c][resnum] = {
                    "clash": 2,
                    "c-beta": None,
                    "nqh_flips": None,
                    "omega": None,
                    "ramachandran": 2,
                    "rotamer": 2,
                    "cmo": None,
                }
                try:
                    # {"1": {"A": {"201": [["dihedral_angles",
                    # "1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA", "4.3*sigma"]]}
                    for outliers in self.dict_res_outliers["1"][c][str(resnum)]:
                        outlier_type = outliers[0]
                        if "clash" in outlier_type.lower():
                            self.iris_data[c][resnum]["clash"] = 0
                        # TODO : add other outliers here?
                except KeyError:
                    pass
                self.set_iris_rama_outlierflag(c, n, str(resnum))
                self.set_iris_rota_outlierflag(c, n, str(resnum))
            # default value to fill if resnum dont match with Iris numbering
            self.iris_data[c]["fill"] = {
                "clash": 2,
                "c-beta": None,
                "nqh_flips": None,
                "omega": None,
                "ramachandran": 2,
                "rotamer": 2,
                "cmo": None,
            }

    def set_iris_rama_outlierflag(self, c: str, n: str, resnum: str) -> None:
        try:
            rama_category = self.dict_ramalyze[c][n].lower()
            if rama_category == "outlier":
                self.iris_data[c][resnum]["ramachandran"] = 0
            elif rama_category == "allowed":
                self.iris_data[c][resnum]["ramachandran"] = 1
        except ValueError:
            pass

    def set_iris_rota_outlierflag(self, c: str, n: str, resnum: str):
        try:
            rota_category = self.dict_rotalyze[c][n].lower()
            if rota_category == "outlier":
                self.iris_data[c][resnum]["rotamer"] = 0
            elif rota_category == "allowed":
                self.iris_data[c][resnum]["rotamer"] = 1
        except (KeyError, ValueError) as e:
            print("Rotalyze output data not found for {}, {}".format(n, e))

    def save_outputs(self):
        self.remove_empty_outliers()
        if self.model_id:
            out_json = self.model_id + "_molprobity_summary.json"
        else:
            out_json = "molprobity_summary.json"
        # reorder the summary table
        for stat in dict_referencerange:
            try:
                self.dict_summary[stat] = self.dict_summary_raw[stat]
            except KeyError:
                pass
        if self.dict_summary:
            with open(out_json, "w") as j:
                json.dump(self.dict_summary, j)
        if self.model_id:
            out_json = self.model_id + "_residue_molprobity_outliers.json"
        else:
            out_json = "residue_molprobity_outliers.json"
        with open(out_json, "w") as j:
            json.dump(self.dict_res_outliers, j)
        if self.model_id:
            out_json = self.model_id + "_molprobity_outliers.json"
        else:
            out_json = "molprobity_outliers.json"
        with open(out_json, "w") as j:
            json.dump(self.dict_outliers, j)
        if self.generate_iris_data:
            dict_iris_data = {}
            if self.model_id:
                out_json = self.model_id + "_molprobity_iris.json"
                dict_iris_data["molprobity"] = self.iris_data
            else:
                out_json = "molprobity_iris.json"
                dict_iris_data["molprobity"] = self.iris_data
            with open(out_json, "w") as j:
                json.dump(dict_iris_data, j)


def parse_args(in_args):
    parser = argparse.ArgumentParser(description="get SMOC score results")
    parser.add_argument(
        "-molp",
        "--molp",
        required=False,
        help="Input molprobity output file (.out, .txt)",
    )
    parser.add_argument(
        "-rama",
        "--rama",
        required=False,
        help="Input ramalyze output file (.out, .txt)",
    )
    parser.add_argument(
        "-rota",
        "--rota",
        required=False,
        help="Input rotalyze output file (.out, .txt)",
    )
    parser.add_argument(
        "-clash",
        "--clash",
        required=False,
        help="Input clashscore output file (.out, .txt)",
    )
    parser.add_argument(
        "-omega",
        "--omega",
        required=False,
        help="Input omegalyze output file (.out, .txt)",
    )
    parser.add_argument(
        "-cablam",
        "--cablam",
        required=False,
        help="Input cablam output file (.out, .txt)",
    )
    parser.add_argument(
        "-cootscript",
        "--cootscript",
        required=False,
        help="Input molprobity output coot script (.py)",
    )
    parser.add_argument(
        "-resname",
        "--resname",
        required=False,
        help="Input c-alpha coords (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="pdbid",
        help="ID used to save results json file",
    )

    return parser.parse_args(in_args)


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    args = parse_args(in_args)
    # read input
    if (
        not args.molp
        and not args.rama
        and not args.rota
        and not args.omega
        and not args.clash
    ):
        raise ValueError(
            "Atleast one of molprobity program outputs is required as input"
        )
    model_id = args.id
    mp = MolprobityLogParser(
        molprobity_out=args.molp,
        model_id=model_id,
        rama_out=args.rama,
        rota_out=args.rota,
        clash_out_json=args.clash,
        omega_out=args.omega,
        cablam_out=args.cablam,
        cootscript=args.cootscript,
    )
    mp.save_outputs()


if __name__ == "__main__":
    main()
