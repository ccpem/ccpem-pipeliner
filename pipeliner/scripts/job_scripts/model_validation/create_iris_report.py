import argparse
import warnings
import json
from typing import Optional, List


def parse_args():
    parser = argparse.ArgumentParser(description="Generate Iris report")
    parser.add_argument(
        "-p1",
        "--model1",
        required=True,
        help="Input atomic model (.pdb, .cif)",
    )
    parser.add_argument(
        "-p2",
        "--model2",
        required=False,
        default=None,
        help="Input model2 (use model1 when not provided)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-data1",
        "--data1",
        required=False,
        default=None,
        help="Input json data for model1 (.json)",
    )
    parser.add_argument(
        "-data2",
        "--data2",
        required=False,
        default=None,
        help="Input json data for model2 (.json). Use data1 when not provided",
    )
    parser.add_argument(
        "-smoc",
        "--smoc",
        default=False,
        help="Input data has smoc scores?",
        action="store_true",
    )
    parser.add_argument(
        "-tortoize",
        "--tortoize",
        default=False,
        help="Input data has tortoize scores?",
        action="store_true",
    )

    return parser.parse_args()


def generate_iris_report(
    input_model1,
    input_model2=None,
    data1_json=None,
    data2_json=None,
    run_smoc=True,
    run_tortoize=True,
    odir=None,
) -> None:
    try:
        from iris_validation import generate_report
    except ImportError:
        warnings.warn("iris_validation not found - skipping html report")
        return

    model1_json = None
    if data1_json:
        model1_json = data1_json
    model2_json = model1_json
    if data2_json:
        model2_json = data2_json
    continuous_metrics_to_display = ["Avg. B", "Std. B"]
    data_with_percentiles = None
    residue_bars_to_display = None
    percentile_bar_label = None
    percentile_bar_range: Optional[List[float]] = None
    if run_smoc:
        data_with_percentiles = ["map_fit"]
        continuous_metrics_to_display.append("Res. Fit")
        residue_bars_to_display = ["Res. Fit"]
        percentile_bar_label = "Z-score"
        percentile_bar_range = [-5.0, 5.0]
    if run_tortoize:
        continuous_metrics_to_display.extend(["Rama Z", "Rota Z"])
    if data_with_percentiles and percentile_bar_label == "Z-score":
        with open(data1_json, "r") as j:
            dict_data1 = json.load(j)
        min_z = 0.0
        max_z = 0.0
        for metric in data_with_percentiles:
            if metric == "map_fit" and metric in dict_data1:
                for c in dict_data1[metric][1]:
                    for res in dict_data1[metric][1][c]:
                        min_z = min(dict_data1[metric][1][c][res][1][0], min_z)
                        max_z = max(dict_data1[metric][1][c][res][1][0], max_z)

        if min_z < 0.0:
            if max_z < 0.0:
                common_max = -1 * min(min_z, max_z)
            else:
                common_max = max(-min_z, max_z)
            percentile_bar_range = [-common_max, common_max]
        else:
            percentile_bar_range = [min_z, max_z]

    output_dir = "."
    if odir:
        output_dir = odir
    model2_path = input_model1
    if input_model2:
        model2_path = input_model2
    generate_report(
        input_model1,
        previous_model_path=model2_path,
        latest_model_metrics_json=model1_json,
        previous_model_metrics_json=model2_json,
        data_with_percentiles=data_with_percentiles,
        discrete_metrics_to_display=[],
        continuous_metrics_to_display=continuous_metrics_to_display,
        residue_bars_to_display=residue_bars_to_display,
        percentile_bar_label=percentile_bar_label,
        percentile_bar_range=percentile_bar_range,
        output_dir=output_dir,
    )


def main():
    args = parse_args()
    # read input
    model1 = args.model1
    run_smoc = args.smoc
    run_tortoize = args.tortoize
    model2 = args.model2
    if not model2:
        warnings.warn("Using input model1 as the second model (model2) as well")
        model2 = model1
    input_json1 = args.data1
    input_json2 = args.data2
    if input_json1 and not input_json2:
        warnings.warn("Using input data1 as the second model data (data2) as well")
        input_json2 = input_json1
    odir = args.odir
    if not odir:
        warnings.warn("Using current directory to generate output")
        odir = "."
    generate_iris_report(
        model1,
        input_model2=model2,
        data1_json=input_json1,
        data2_json=input_json2,
        run_smoc=run_smoc,
        run_tortoize=run_tortoize,
        odir=odir,
    )


if __name__ == "__main__":
    main()
