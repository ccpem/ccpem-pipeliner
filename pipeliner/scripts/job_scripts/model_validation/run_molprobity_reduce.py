#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import subprocess
import sys


def parse_args(in_args):
    parser = argparse.ArgumentParser(
        description="run Molprobity reduce and save output to file"
    )
    parser.add_argument(
        "-i",
        "--input",
        required=True,
        help="Input model (.pdb)",
    )
    parser.add_argument(
        "-o",
        "--output",
        required=False,
        default=None,
        help="Output file (.pdb)",
    )
    return parser.parse_args(in_args)


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    args = parse_args(in_args)
    input_model = args.input
    output_file = "reduce_out.pdb"
    if args.output:
        output_file = args.output
    with open(output_file, "w") as o:
        subprocess.run(["molprobity.reduce", "-FLIP", "-Quiet", input_model], stdout=o)


if __name__ == "__main__":
    main()
