#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import argparse
import json
from ccpem_utils.other import cluster, calc
from ccpem_utils.model import gemmi_model_utils
from typing import List, Sequence


def parse_args():
    parser = argparse.ArgumentParser(description="get SMOC score results")
    parser.add_argument(
        "-fdrscore",
        "--fdrscore",
        required=True,
        help="Input FDR backbone scores (.csv)",
    )
    parser.add_argument(
        "-coord",
        "--coord",
        required=True,
        help="Input c-alpha coords (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="pdbid",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def parse_fdrscore_csv(
    fdrscore_csv: str,
    ca_coord_json: str,
    model_id: str,
):
    with open(ca_coord_json, "r") as j:
        dict_pdb_ca_coord = json.load(j)
    list_ids, list_coords = gemmi_model_utils.convert_dict_attributes_to_list(
        dict_pdb_ca_coord
    )
    model_kdtree = cluster.generate_kdtree(list_coords)

    # Get data
    dict_resnum: dict = {}
    dict_coord: dict = {}
    dict_fdrscore: dict = {}
    list_coord_chain: List[Sequence[float]] = []
    list_resnum: List[str] = []
    dict_iris_data: dict = {}
    dict_iris_data["main_fit"] = {}
    dict_fdrscore["1"] = {}
    dict_resnum["1"] = {}
    dict_coord["1"] = {}
    chain_prev = ""
    with open(fdrscore_csv, "r") as c:
        for line in c:
            lsplit = line.strip().split(",")
            # Chain_name,residue_id,residue_name,conf_score
            if lsplit[0] == "Chain_name":
                continue
            chain = lsplit[0]
            resnum = lsplit[1]
            score = float(lsplit[-1])
            if score == -1:  # failure
                continue
            if chain not in dict_fdrscore["1"]:
                if list_coord_chain:
                    dict_resnum["1"][chain_prev] = list_resnum[:]
                    dict_coord["1"][chain_prev] = list_coord_chain[:]
                    # default fill values for Iris
                    dict_iris_data["main_fit"][chain_prev]["fill"] = [0.0, 0.0]
                dict_fdrscore["1"][chain] = {}
                dict_iris_data["main_fit"][chain] = {}
                list_coord_chain = []
                list_resnum = []
            if score:
                dict_fdrscore["1"][chain][resnum] = [score]
            # residues with insertion codes fail this!
            try:
                ca_coord = dict_pdb_ca_coord["1"][chain][str(resnum)][0]
            except KeyError:
                continue
            list_resnum.append(resnum)
            list_coord_chain.append(ca_coord)
            chain_prev = chain
    if list_coord_chain:
        dict_resnum["1"][chain] = list_resnum
        dict_coord["1"][chain] = list_coord_chain
        # default fill values for Iris
        dict_iris_data["main_fit"][chain]["fill"] = [0.0, 0.0]
        # calculate fdr Z scores
        fdrscore_z(dict_fdrscore, dict_resnum, dict_coord, model_kdtree, list_ids)
    for m in dict_fdrscore:
        for c in dict_fdrscore[m]:
            for n in dict_fdrscore[m][c]:
                # check score failures
                if len(dict_fdrscore[m][c][n]) == 1:  # no z-score
                    dict_fdrscore[m][c][n].append(0.0)
                elif len(dict_fdrscore[m][c][n]) == 0:
                    del dict_fdrscore[m][c][n]
                    continue
                try:
                    dict_iris_data["main_fit"][c][str(n)] = [
                        round(dict_fdrscore[m][c][n][0], 3),  # raw
                        round(dict_fdrscore[m][c][n][1], 3),  # z-score
                    ]
                except TypeError:
                    print("Iris FDR score skip {},{},{}".format(m, c, n))
        break
    out_json = model_id + "_residue_fdrscore.json"
    with open(out_json, "w") as j:
        json.dump(dict_fdrscore, j)
    if model_id:
        out_json = model_id + "_fdrscore_iris.json"
    else:
        out_json = "fdrscore_iris.json"
    with open(out_json, "w") as j:
        json.dump(dict_iris_data, j)


def fdrscore_z(
    dict_fdrscore,
    dict_resnum,
    dict_coord,
    model_kdtree,
    list_ids,
):
    for m in dict_resnum:
        for c in dict_resnum[m]:
            list_resnum = dict_resnum[m][c]
            try:
                list_coord_chain = dict_coord[m][c]
            except KeyError:
                print("Coordinate data not found for chain {}".format(c))
                continue
            list_neighbors = cluster.get_neighbors_kdtree(
                coords=list_coord_chain, kdtree=model_kdtree, distance=12.0
            )
            assert len(list_resnum) == len(list_neighbors)
            count_res = 0
            for neighbors in list_neighbors:  # neighbors of each input coordinate
                list_fdrscore_neigh = []
                for n in neighbors:
                    residue_id = list_ids[n]  # find residue id of neighbor
                    mn, cn, rn = residue_id.split("_")
                    try:
                        list_fdrscore_neigh.append(dict_fdrscore[mn][cn][rn][0])
                    except KeyError:
                        pass
                if len(list_fdrscore_neigh) > 2:
                    try:
                        fdrscore_z = calc.calc_z(
                            list_fdrscore_neigh,
                            dict_fdrscore[m][c][list_resnum[count_res]][0],
                        )
                    except KeyError:
                        continue
                    dict_fdrscore[m][c][list_resnum[count_res]].append(
                        round(float(fdrscore_z), 3)
                    )
                else:
                    dict_fdrscore[m][c][list_resnum[count_res]].append(0.0)
                count_res += 1


def main():
    args = parse_args()
    # read input
    fdrscore_outputfile = args.fdrscore
    coord_json = args.coord
    model_id = args.id
    parse_fdrscore_csv(
        fdrscore_csv=fdrscore_outputfile, ca_coord_json=coord_json, model_id=model_id
    )


if __name__ == "__main__":
    main()
