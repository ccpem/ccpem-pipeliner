#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import os.path
import sys
import argparse
from urllib.parse import urlparse
from urllib.request import urlopen, urlretrieve
import shutil
from pathlib import Path

from pipeliner.utils import run_subprocess


def get_arguments() -> argparse.ArgumentParser:
    description = "Python tool to replace wget using urllib"
    parser = argparse.ArgumentParser(
        prog="download_map_model", description="".join(description)
    )
    parser.add_argument("url")

    parser.add_argument(
        "-P",
        "--directory_prefix",
        default="",
        help="Save files to this directory",
    )
    return parser


def dl_progress(count, blockSize, totalSize) -> None:
    """Callable used by urlretrieve to report download progress.

    From the urlretrieve docs: "a callable that will be called once on establishment of
    the network connection and once after each block read thereafter. The callable will
    be passed three arguments; a count of blocks transferred so far, a block size in
    bytes, and the total size of the file. The third argument may be -1 on older FTP
    servers which do not return a file size in response to a retrieval request."
    """
    n_bytes = count * blockSize
    msg = f"{n_bytes >> 10} KB"
    if totalSize > 0:
        fraction = n_bytes / totalSize
        percent = min(int(fraction * 100), 100)
        msg += f", {percent}%"
    print(msg, flush=True)


def get_file(url, dir_prefix="") -> None:
    response = urlopen(url)
    filename = os.path.basename(response.url)
    dest = os.path.join(dir_prefix, filename)
    print(f"Downloading {url} to {dest} ...")
    urlretrieve(url, dest, reporthook=dl_progress)
    print(f"Finished downloading {dest}")


def wget_dl(url: str, directory_prefix: str) -> int:
    dl = run_subprocess(["wget", "-P", directory_prefix, url])
    return dl.returncode


def curl_dl(url: str, directory_prefix: str) -> int:
    fn = Path(urlparse(url).path).name
    outfile = str(Path(directory_prefix) / fn)
    dl = run_subprocess(["curl", "-o", outfile, url])
    return dl.returncode


def main(in_args=None) -> None:
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    has_curl = shutil.which("curl")
    has_wget = shutil.which("wget")

    dl_rt = 1
    if has_wget:
        print("-- Downloading with wget --")
        dl_rt = wget_dl(url=args.url, directory_prefix=args.directory_prefix)

    if has_curl and dl_rt != 0:
        print("-- Downloading with curl --")
        dl_rt = curl_dl(url=args.url, directory_prefix=args.directory_prefix)

    if dl_rt != 0:
        print("-- Downloading with CCP-EM Pipeliner download script --")
        get_file(url=args.url, dir_prefix=args.directory_prefix)


if __name__ == "__main__":
    main()
