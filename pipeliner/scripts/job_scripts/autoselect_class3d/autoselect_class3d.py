#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import sys
import os
from typing import Optional, List
import argparse
from gemmi import cif
import shutil

from pipeliner.starfile_handler import DataStarFile
from pipeliner.jobs.other.autoselect_class3d_job import select_3dclass_from_model


def main(in_args: Optional[List[str]] = None):
    """Finds the class with highest resolution and makes a starfile of its particles"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", type=str, required=True)
    parser.add_argument("--outdir", type=str, required=True)

    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    modfile = args.data.replace("_data", "_model")
    clno = select_3dclass_from_model(modfile)

    # select the particles and write the starfile
    parts_data = DataStarFile(args.data)
    parts = parts_data.loop_as_list("particles", headers=True)
    pindex = -1
    for n, i in enumerate(parts[0]):
        if i == "_rlnClassNumber":
            pindex = n
            break
    if pindex == -1:
        raise ValueError("index '_rlnClassNumber not found in data star file'")
    selected_parts = []
    for line in parts[1:]:
        if int(line[pindex]) == clno:
            selected_parts.append(line)

    sel_part_star = cif.Document()
    og_block = sel_part_star.add_new_block("optics")
    optics_groups = parts_data.loop_as_list(block="optics", headers=True)
    og_loop = og_block.init_loop("", optics_groups[0])
    for line in optics_groups[1:]:
        og_loop.add_row(line)

    parts_block = sel_part_star.add_new_block("particles")
    parts_loop = parts_block.init_loop("", parts[0])
    for line in selected_parts:
        parts_loop.add_row(line)

    sel_part_star.write_file(os.path.join(args.outdir, "selected_particles.star"))
    mapfile = args.data.replace("_data.star", f"_class{clno:03d}.mrc")
    shutil.copy(mapfile, os.path.join(args.outdir, "selected_class.mrc"))


if __name__ == "__main__":
    main()
