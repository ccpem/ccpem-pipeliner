#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
import argparse
import os
from typing import Optional, List


def main(in_args: Optional[List[str]] = None):
    """Make symbolic links to the two halfmaps in the output dir"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--hm1", type=str, required=True)
    parser.add_argument("--hm2", type=str, required=True)
    parser.add_argument("--output_dir", type=str, required=True)
    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    # make symbolic links to the halfmaps in the output directory
    os.symlink(
        os.path.relpath(args.hm1, args.output_dir),
        os.path.join(args.output_dir, "half1.mrc"),
    )
    os.symlink(
        os.path.relpath(args.hm2, args.output_dir),
        os.path.join(args.output_dir, "half2.mrc"),
    )


if __name__ == "__main__":
    main()
