#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys

"""
This is required as passing phil values via the command results in a bug where
the molecule attributes (molecule_name, map_or_model_file, starting_model_vrms)
result in three molecule groups with one attribute as opposed to one molecule
group with three attributes.  If more phil parsing is required consider using:

https://pypi.org/project/freephil/
"""


def main():
    (
        half_map_1,
        half_map_2,
        best_resolution,
        point_group_symmetry,
        sequence_composition,
        molecule_name,
        map_or_model_file,
        starting_model_vrs,
        path,
    ) = sys.argv[1:]

    phil_text = f"""
    voyager
        {{
        remove_phasertng_folder = True
        map_model
        {{
            half_map = {half_map_1}
            half_map = {half_map_2}
            best_resolution = {best_resolution}
            point_group_symmetry = {point_group_symmetry}
            sequence_composition = {sequence_composition}
        }}

        biological_unit {{
        molecule
        {{
            molecule_name = {molecule_name}
            map_or_model_file = {map_or_model_file}
            starting_model_vrms = {starting_model_vrs}
        }}
        }}
    }}
    """
    with open(path, "w") as phil:
        phil.write(phil_text)


if __name__ == "__main__":
    main()
