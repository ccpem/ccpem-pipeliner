#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

from glob import glob
import os
import sys
import argparse
from pathlib import Path
import shutil

"""
Makes a dir appear like previous Dynamight steps have been run in it for continuing
previous Dynamight jobs in a new job for pipeliner and reilion compatibility
"""


def get_arguments():
    parser = argparse.ArgumentParser(description="Dynamight job setup")

    parser.add_argument(
        "--output_dir",
        help="The job's output directory",
        nargs="?",
        metavar="Output dir",
        required=True,
    )
    parser.add_argument(
        "--checkpoint_file",
        help="The checkpoint file from the previous job",
        nargs="?",
        metavar="Original job's checkpoint file",
        required=True,
    )

    parser.add_argument(
        "--do_backproject",
        action="store_true",
        help="Setup for consensus backprojection calculation",
    )

    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    odir = Path(args.output_dir)
    orig = Path(args.checkpoint_file).parent
    orig_root = Path(str(orig).replace("forward_deformations/checkpoints", ""))
    orig_vol = Path(str(orig).replace("checkpoints", "volumes"))
    inv_dirs = [
        odir / "forward_deformations/checkpoints",
        odir / "forward_deformations/volumes",
    ]
    for d in inv_dirs:
        os.makedirs(d, exist_ok=True)
    shutil.copy(
        args.checkpoint_file,
        odir / "forward_deformations/checkpoints/checkpoint_final.pth",
    )
    vols = glob(str(orig_vol / "*.mrc"))
    vols.sort()
    vol_n = int(Path(vols[-1]).stem.split("_")[-1])
    vol_files = glob(str(orig_vol / f"*_{vol_n:03d}.mrc"))
    for f in vol_files:
        shutil.copy(f, odir / "forward_deformations/volumes")

    if args.do_backproject:
        # do the additional file copying for back projection
        id_dir = odir / "inverse_deformations"
        os.makedirs(id_dir, exist_ok=True)
        shutil.copy(
            orig_root / "inverse_deformations/inv_chkpt.pth",
            odir / "inverse_deformations/inv_chkpt.pth",
        )


if __name__ == "__main__":
    main()
