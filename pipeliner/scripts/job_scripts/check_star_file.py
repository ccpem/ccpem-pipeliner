#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
from pipeliner.starfile_handler import DataStarFile
import argparse
from typing import Optional, List
from pipeliner.data_structure import RELION_SUCCESS_FILE
from pathlib import Path


def get_argument_parser():
    description = "Check a starfile contains data in the stated block"
    parser = argparse.ArgumentParser(
        prog="starfile_check", description="".join(description)
    )
    parser.add_argument(
        "--fn_in",
        help="Input file name",
        nargs="?",
        metavar="Input file",
        required=True,
    )
    parser.add_argument(
        "--block",
        help="Block name to check",
        nargs="?",
        metavar="block",
        default=None,
    )
    parser.add_argument(
        "--clear_relion_success_file",
        help=(
            'If the star file is empty, remove a "RELION_JOB_SUCCESS" file from the'
            " job if there is one"
        ),
        action="store_true",
    )
    return parser


def check_star_has_contents(file_name: str, block_name: Optional[str]) -> bool:
    try:
        if block_name is None:
            print(f'Checking contents of the sole block in file "{file_name}"...')
        else:
            print(f'Checking contents of block "{block_name}" in file "{file_name}"...')
        infile = DataStarFile(file_name)
        n_block = infile.count_block(block_name)
        if n_block > 0:
            print(f"Found {n_block} records")
            return True
        else:
            print("No data found")
            return False
    except Exception as ex:
        print(f"An error occurred trying to read the file: {ex}")
        return False


def main(in_args: Optional[List[str]] = None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_argument_parser()
    args = parser.parse_args(in_args)

    star_has_contents = check_star_has_contents(args.fn_in, args.block)
    if star_has_contents:
        return 0
    else:
        if args.clear_relion_success_file:
            success_file = Path(args.fn_in).parent / RELION_SUCCESS_FILE
            print(f'Removing file "{success_file}"')
            success_file.unlink(missing_ok=True)
        return 1


if __name__ == "__main__":
    sys.exit(main())
