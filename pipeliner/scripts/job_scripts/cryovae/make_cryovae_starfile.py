import argparse
import sys
from gemmi import cif
from pathlib import Path
from pipeliner.mrc_image_tools import get_mrc_dims


def get_args():
    parser = argparse.ArgumentParser(description="make starfile for cryovae job")
    parser.add_argument(
        "--starfile",
        required=False,
        default=None,
        help="Original star file input to cryoVAE",
    )
    parser.add_argument(
        "--stackfile",
        required=True,
        default=None,
        help="Stack output by cryoVAE",
    )
    parser.add_argument(
        "--outdir",
        required=True,
        help="Output directory",
    )

    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_args()
    args = parser.parse_args(in_args)
    outname = str(Path(args.outdir) / "denoised_particles.star")

    if args.starfile:
        star = cif.read_file(args.starfile)
        parts = star.find_block("particles")
        names_loop = parts.find_loop("_rlnImageName")
        for n, mic in enumerate(names_loop):
            new_name = mic.split("@")[0] + f"@{args.stackfile}"
            names_loop[n] = new_name
        psi_loop = parts.find_loop("_rlnAnglePsi")
        for n in range(len(psi_loop)):
            psi_loop[n] = "0.0"
        star.write_file(filename=outname)

    else:
        stack_z = get_mrc_dims(args.stackfile)[2]
        outfile = cif.Document()
        parts_block = outfile.add_new_block("particles")
        parts_loop = parts_block.init_loop("", ["_rlnImageName"])
        for n in range(1, stack_z + 1):
            parts_loop.add_row([f"{n:06d}@{args.stackfile}"])
        outfile.write_file(filename=outname)


if __name__ == "__main__":
    main()
