#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import sys
from typing import List, Optional
import logging

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.project_graph import ProjectGraph

logger = logging.getLogger(__name__)


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description="Generate results display files for jobs and nodes"
    )
    parser.add_argument(
        "--jobs",
        help="Jobs to create results for",
        nargs="*",
        metavar="Job names",
    )
    parser.add_argument(
        "--nodes",
        help="Nodes to create results for",
        nargs="*",
        metavar="Node names",
    )
    parser.add_argument(
        "--node_summary",
        help=(
            "Generate summary objects for the nodes instead of normal results display"
            " objects"
        ),
        action="store_true",
    )
    parser.add_argument(
        "--ignore_errors",
        help=(
            "If there are errors generating results for a specific job or node, ignore"
            " them and carry on processing others"
        ),
        action="store_true",
    )
    return parser


def main(in_args: Optional[List[str]] = None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_argument_parser()
    args = parser.parse_args(in_args)

    logger.info("Regenerating pipeliner results files")

    proj = PipelinerProject()

    # If run with no arguments, generate results for all jobs and nodes
    if args.jobs is None and args.nodes is None:
        with ProjectGraph(name=proj.pipeline_name) as pipeline:
            job_names = [proc.name for proc in pipeline.process_list]
            node_names = [node.name for node in pipeline.node_list]
    else:
        # Otherwise, just generate results for the specified jobs and nodes
        job_names = args.jobs
        node_names = args.nodes

    if job_names is not None:
        for job_name in job_names:
            try:
                job = proj.get_job(job_name)
                logger.info(f"Generating results for job {job_name}")
                job.save_results_display_files()
            except Exception:
                logger.exception(f"Error generating results for job {job_name}")
                if not args.ignore_errors:
                    print(
                        "Run again with '--ignore_errors' to ignore this error and"
                        " continue processing other jobs"
                    )
                    raise

    if node_names is not None:
        with ProjectGraph(name=proj.pipeline_name) as pipeline:
            for node_name in node_names:
                try:
                    node = pipeline.find_node(node_name)
                    if node is not None:
                        if args.node_summary:
                            logger.info(f"Generating summary for node {node_name}")
                            node.write_summary_file()
                        else:
                            logger.info(f"Generating results for node {node_name}")
                            node.write_default_result_file()
                except Exception:
                    logger.exception(f"Error generating results for node {node_name}")
                    if not args.ignore_errors:
                        print(
                            "Run again with '--ignore_errors' to ignore this error and"
                            " continue processing other nodes"
                        )
                        raise


if __name__ == "__main__":
    main()
