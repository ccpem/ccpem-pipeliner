#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

# Clean up the summary reports directories.  Remove any reports that are missing files.

import argparse
import os
import sys
import logging
from glob import glob
from typing import Optional, List
from gemmi import cif
from pipeliner.starfile_handler import DataStarFile

logger = logging.getLogger(__name__)


class SummaryFile(object):
    def __init__(self, filename):
        self.data = DataStarFile(filename)
        self.updated = cif.Document()
        self.updated.add_new_block("project_archives")
        self.pa_loop = self.updated.find_block("project_archives").init_loop(
            "_pipeliner", ["ArchiveName", "InitiatedFrom", "ArchiveType"]
        )
        self.updated.add_new_block("metadata_reports")
        self.md_loop = self.updated.find_block("metadata_reports").init_loop(
            "_pipeliner", ["MetadataReportName", "InitiatedFrom", "NumberOfJobs"]
        )
        self.updated.add_new_block("reference_reports")
        self.rf_loop = self.updated.find_block("reference_reports").init_loop(
            "_pipeliner", ["ReferenceReportName", "InitiatedFrom", "NumberOfJobs"]
        )

    def cleanup_block(self, block: str, do_update: bool = True):
        logger.info(f"Cleaning up {block}")
        block_loops = {
            "project_archives": self.pa_loop,
            "metadata_reports": self.md_loop,
            "reference_reports": self.rf_loop,
        }
        try:
            data = self.data.loop_as_list(block)
            n_removed = 0
            for line in data:
                found = os.path.isfile(line[0])
                if found or not do_update:
                    block_loops[block].add_row(line)
                if not found and do_update:
                    logger.info(f"REMOVED: {line[0]}")
                    n_removed += 1
            if do_update:
                logger.info(f"{n_removed}/{len(data)} entries removed")
            else:
                logger.info(
                    f"Did not cleanup this section {len(data)}/{len(data)} "
                    "entries retained"
                )
        except ValueError:
            logger.info("Block is empty.")


def main(in_args: Optional[List[str]] = None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--archives",
        action="store_true",
        help="clean up archives summary data",
    )
    parser.add_argument(
        "-m",
        "--metadata",
        action="store_true",
        help="clean up metadata summary data",
    )
    parser.add_argument(
        "-r",
        "--references",
        action="store_true",
        help="clean up references summary data",
    )

    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    summary_file = "pipeliner_summary_data.star"
    logger.info("Cleaning up summary files")
    fn_sum = glob(summary_file)
    if not fn_sum:
        logger.error(f"The summary data file '{summary_file}' was not found, exiting")
        return
    sumfile = SummaryFile(fn_sum[0])
    if not any([args.archives, args.metadata, args.references]):
        args.archives, args.metadata, args.references = True, True, True
    sumfile.cleanup_block("project_archives", args.archives)
    sumfile.cleanup_block("metadata_reports", args.metadata)
    sumfile.cleanup_block("reference_reports", args.references)
    sumfile.updated.write_file(fn_sum[0])


if __name__ == "__main__":
    main()
