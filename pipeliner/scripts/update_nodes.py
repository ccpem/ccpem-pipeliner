#
#     Copyright (C) 2024 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

"""
This script makes sure all of the expected nodes for the jobs are included in the
pipeline file.  It is generally used when converting from a project started in RELION
and moved to pipeliner because some nodes pipeliner expects are not produced by RELION
"""


from pipeliner.api.manage_project import PipelinerProject
from pipeliner.utils import make_pretty_header


def main():
    print(make_pretty_header("Updating pipeline node list"))

    proj = PipelinerProject()
    added_nodes = proj.add_missing_nodes_from_all_jobs()

    if added_nodes:
        print("Added the following nodes")
        for node in added_nodes:
            print(f"- {node}")
    else:
        print("Pipeline is up to date")


if __name__ == "__main__":
    main()
