#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

# Recover a broken pipeliner project where the default_pipeline.star file has become
# lost or corrupted and/or the lock directory has been left in place
#
# To use, run this script from your project directory and follow the instructions

import argparse
import os
import shutil
from glob import glob
import logging

logger = logging.getLogger(__name__)


def byebye(why):
    logger.error(f"  {why}")
    logger.error("  Unable to recover project")
    exit(1)


def recover(confirm=True):
    logger.info("Attempting to recover CCPEM-Pipeliner project")

    # Check if we are in a project or not
    if not os.path.isfile("default_pipeline.star"):
        byebye(
            "No pipeline file found. Are you sure you are in a pipeliner project"
            " directory?"
        )

    # check for .relion_lock and remove if present
    is_locked = "locked" if os.path.isdir(".relion_lock") else "unlocked"
    logger.info("Checking for lockfile")
    logger.info(f"Pipeline is currently {is_locked}")

    logger.info("Finding pipeline backups")

    # find the last job
    backup = None
    alldirs = glob("*/job*")
    if len(alldirs):
        logger.info(f"{len(alldirs)} jobs were found")
        alldirs.sort(key=lambda x: int(x[-3:]), reverse=True)
        skipped_any = False
        for jobdir in alldirs:
            pipe_file = os.path.join(jobdir, "default_pipeline.star")
            if os.path.isfile(pipe_file):
                backup = pipe_file
                logger.info(f"Pipeline will be restored from {jobdir}")
                break
            else:
                skipped_any = True
                logger.warning(f"No pipeline backup found in {jobdir}")
        if backup is None:
            logger.error("Pipeline will not be restored")
        elif skipped_any:
            logger.warning(
                "Warning: subsequent jobs will be lost from the restored project"
            )
    else:
        logger.error("No pipeline backups found")

    if confirm:
        ok = input("Perform recovery (Y/N)? ")
        if ok.lower() != "y":
            print("\n goodbye\n")
            exit()

    try:
        if is_locked == "locked":
            shutil.rmtree(".relion_lock")
            logger.info("Lockfile removed")
        if backup is not None:
            shutil.copy(backup, "default_pipeline.star")
            logger.info("Pipeline restored from backup")

    except Exception as e:
        byebye(f"An error occurred during recovery {str(e)}")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-y",
        "--assume-yes",
        action="store_true",
        help="Do not prompt for confirmation before recovering the project",
    )
    args = parser.parse_args()
    recover(confirm=not args.assume_yes)


if __name__ == "__main__":
    main()
