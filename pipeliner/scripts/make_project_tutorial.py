"""
This script takes a completed doppio project and writes a tutorial to run it.
You should be able to run the project on the host system, make sure it worked properly
and then generate a custom tutorial document for that specific system.
"""

from pathlib import Path
from typing import Dict, Tuple
from collections import OrderedDict
import argparse

from pipeliner.utils import (
    decompose_pipeline_filename,
    make_pretty_header,
    date_time_tag,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.job_options import FloatJobOption
from pipeliner.project_graph import ProjectGraph
from pipeliner.api.manage_project import PipelinerProject


def parse_args():
    parser = argparse.ArgumentParser(description="crop or pad mrc map")
    parser.add_argument(
        "-j",
        "--job",
        required=True,
        help="Terminal job for the project",
    )

    return parser.parse_args()


def run_params(jdir) -> Dict[str, str]:
    """Get the parameters used to run a job from the job's run.job file

    Args:
        jdir (str): The job directory in format JobType/jobnnn/

    Returns:
        Dict[str, str]: {parameter: value}
    """
    param_dict = {}
    runjob = Path(jdir) / "run.job"
    with open(runjob) as rj:
        params = [x.strip() for x in rj.readlines()]
    for pair in params:
        psplit = [x.strip() for x in pair.split("==")]
        param_dict[psplit[0]] = psplit[1]
    return param_dict


def default_param_dict(job_type) -> Tuple[Dict[str, str], str, str]:
    """
    Get default information about a job tupe

    Args:
        job_type (str): The pipeliner job type

    Returns:
        Tuple[Dict[str, str], str, str]: Parameters dict, top level GUI grouping, Job's
        display name
    """
    defaults_dict = OrderedDict()
    job = new_job_of_type(job_type)
    groupings = job.get_joboption_groups()
    final_order = []
    for group in groupings:
        final_order += groupings[group]
    for jo in final_order:
        label = job.joboptions[jo].label.strip()
        value = str(job.joboptions[jo].default_value)
        if isinstance(job.joboptions[jo], FloatJobOption):
            value = str(float(value))
        defaults_dict[label] = value
    if "Continue from here:" in defaults_dict:
        del defaults_dict["Continue from here:"]
    job_cat = job.PROCESS_NAME.split(".")[1].replace("_", " ")
    job_desc = job.jobinfo.display_name
    return defaults_dict, job_cat, job_desc


def main():
    """Make a tutorial for the project in the current directory

    Raises:
        ValueError: If no job directories are found
    """
    args = parse_args()
    proj = PipelinerProject()
    job = proj.parse_procname(args.job)
    print(make_pretty_header(f"Making tutorial for {job}"))

    with ProjectGraph() as pipeline:
        proc = pipeline.find_process(proj.parse_procname(job))
        jobdirs = [
            x.name for x in pipeline.get_project_procs_list(proc.name, reverse=False)
        ]

    all_dat = ["Starred parameters need to be updated, otherwise use default values\n"]
    print(f"Found {len(jobdirs)} jobs")

    for jdir in jobdirs:
        actual_params = run_params(jdir)
        defaults, job_cat, job_desc = default_param_dict(actual_params["job_type"])
        jtype, jnum = decompose_pipeline_filename(jdir)[:2]
        name = f"{jnum} - {jtype}"

        all_dat.append(f"{name}\n")
        all_dat.append(f"Under **{job_cat}** select a new **{job_desc.title()}** job\n")
        all_dat.append("**Parameters:**\n")
        all_dat.append("::\n")
        for param in defaults:
            pretty_param = (
                param + ":"
                if (not param.endswith(":") and not param.endswith("?"))
                else param
            )
            if str(defaults[param]) == actual_params[param]:
                all_dat.append(f"    {pretty_param}  {defaults[param]}")
            else:
                all_dat.append(f"  * {pretty_param}  {actual_params[param]}")
        all_dat.append("\n|\n")

    fn = date_time_tag(compact=True)[:8] + "_doppio_tutorial.rst"
    with open(fn, "w") as out:
        for i in all_dat:
            out.write(f"{i}\n")
    print(f"Wrote {fn}\nFinished!")


if __name__ == "__main__":
    main()
