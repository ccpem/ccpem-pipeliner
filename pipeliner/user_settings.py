#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
"""
user_settings.py
----------------

Settings for the CCP-EM pipeliner.

Settings can be provided by the user in a JSON-formatted settings file or in environment
variables. When the pipeliner is first run, a new settings file is created in a
platform-specific directory (typically ``~/.config/ccpem/pipeliner/`` on Linux or
``~/Library/Application Support/ccpem/pipeliner/`` on Mac). This contains default
settings values. These settings can be edited in that file, or overridden by setting
environment variables before running the pipeliner. Both RELION- and Pipeliner-type
environment variables are supported. For example, the setting for the default qsub
script template can be set by editing the value for ``"qsub_template"`` in the settings
file or by setting either the ``PIPELINER_QSUB_TEMPLATE`` or ``RELION_QSUB_TEMPLATE``
environment variable. If more than one of these is set, ``PIPELINER_QSUB_TEMPLATE`` will
be used by preference, followed by ``RELION_QSUB_TEMPLATE``, followed by the value from
the settings file.

For programmatic access to settings values, use the helper functions which are provided
at the module level (e.g. ``user_settings.get_qsub_template()``. These allow easy access
to the correctly-typed value of each individual setting.

This module also contains a Settings class and some internal functions, but there should
be no need to access any of these directly from outside this module. To add a new
setting, create a new ``get_<setting name>()`` function at the top of this module and
add a new setting definition in ``Settings.add_all_settings()``, making sure to use the
correct types for the function calls and default value.
"""
import json
import os
import platform
import shutil
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Optional
import logging

from pipeliner.data_structure import TRUES

logger = logging.getLogger(__name__)


def get_path_to_source_files() -> List[str]:
    """Script files to set up the environment variables for the pipeliner. These files
    will be sourced when the pipeliner starts up and their environment read in and used
    to update the pipeliner's environment variables. This is a clunky mechanism intended
    mainly for setting up CCP4 and the old version of CCP-EM. Avoid using it for new
    packages.

    In JSON format, this setting is a list of strings. It can also be set using the
    ``PIPELINER_PATH_TO_SOURCE_FILES`` environment variable, which should be a
    PATH-style string containing individual paths separated by path separators
    (typically ':' on Linux and Mac)."""
    return _get_settings().get_list("path_to_source_files")


def get_additional_program_paths() -> List[str]:
    """Paths to prepend to the PATH environment variable before searching for program
    executables. Use this setting to make third-party software available to the
    pipeliner. Note that the paths should be to the directory containing the executable,
    not to the executable itself.

    In JSON format, this setting is a list of strings. It can also be set using the
    ``PIPELINER_ADDITIONAL_PROGRAM_PATHS`` environment variable, which should be a
    PATH-style string containing individual paths separated by path separators
    (typically ':' on Linux and Mac)."""
    return _get_settings().get_list("additional_program_paths")


def get_ctffind_executable() -> str:
    """The default CTFFIND-4.1+ executable."""
    return _get_settings().get_string("ctffind_executable")


def get_default_nrmpi() -> int:
    """The default for 'Number of MPI procs'."""
    return _get_settings().get_int("default_nrmpi")


def get_default_nrthreads() -> int:
    """The default for 'Number of threads'."""
    return _get_settings().get_int("default_nrthreads")


def get_gctf_executable() -> str:
    """The default Gctf executable."""
    return _get_settings().get_string("gctf_executable")


def get_motioncor2_executable() -> str:
    """The default MotionCor2 executable."""
    return _get_settings().get_string("motioncor2_executable")


def get_minimum_dedicated() -> int:
    """The default for 'Minimum dedicated cores per node'."""
    return _get_settings().get_int("minimum_dedicated")


def get_mpi_max() -> Optional[int]:
    """The maximum number of MPI processes available from the GUI."""
    return _get_settings().get_optional_int("mpi_max")


def get_mpirun_command() -> str:
    """The default command prepended to MPI jobs, including 'XXXmpinodesXXX' which will
    be substituted with the number of MPI nodes to use."""
    return _get_settings().get_string("mpirun_command")


def get_qsub_command() -> str:
    """The default for 'Queue submit command'."""
    return _get_settings().get_string("qsub_command")


def get_qsub_extra_count() -> int:
    """The number of extra qsub template substitution variables to use."""
    return _get_settings().get_int("qsub_extra_count")


def get_qsub_extras(number: int) -> Dict[str, str]:
    """Return a dictionary of values for a single numbered set of extra qsub settings.

    For example::

        get_qsub_extras(1) -> {
            "name": "Label for qsub_extra1",
            "default": "Default value for qsub_extra1",
            "help": "Help text for qsub_extra1",
        }
    """
    return _get_settings().get_qsub_extras(number)


def get_qsub_template() -> str:
    """The default queue submission script template."""
    return _get_settings().get_string("qsub_template")


def get_queue_name() -> str:
    """The default for 'Queue Name"."""
    return _get_settings().get_string("queue_name")


def get_queue_use() -> bool:
    """The default for 'Submit to queue?'."""
    return _get_settings().get_bool("queue_use")


def get_resmap_executable() -> str:
    """The default ResMap executable."""
    return _get_settings().get_string("resmap_executable")


def get_scratch_dir() -> str:
    """The default scratch directory."""
    return _get_settings().get_string("scratch_dir")


def get_thread_max() -> Optional[int]:
    """The maximum number of threads per MPI process available from the GUI."""
    return _get_settings().get_optional_int("thread_max")


def get_topaz_executable() -> str:
    """The default Topaz executable."""
    return _get_settings().get_string("topaz_executable")


def get_modelangelo_executable() -> str:
    """The default ModelAngelo executable."""
    return _get_settings().get_string("modelangelo_executable")


def get_dynamight_executable() -> str:
    """The default DynaMight executable."""
    return _get_settings().get_string("dynamight_executable")


def get_warning_local_mpi() -> Optional[int]:
    """Warn if users try to submit local jobs with more than this many MPI nodes."""
    return _get_settings().get_optional_int("warning_local_mpi")


def get_ccpem_share_dir() -> str:
    """Path to the share dir in an installation of ccpem for rvapi to use"""
    share_dir = _get_settings().get_optional_string("ccpem_share_dir")
    ccp_python_path = shutil.which("ccpem-python")
    if not share_dir and ccp_python_path:
        bin_dir = os.path.dirname(ccp_python_path)
        if bin_dir:
            share_dir = os.path.join(os.path.dirname(bin_dir), "share")
    return share_dir


def get_modelcraft_executable() -> str:
    """The default Modelcraft executable."""
    return _get_settings().get_string("modelcraft_executable")


_settings = None


def _get_settings():
    """Function to initialise the settings once the first time they are needed."""
    global _settings
    if _settings is None:
        _settings = Settings()
    return _settings


@dataclass
class SettingDefinition:
    """Base class for setting definitions. Not intended to be used directly."""

    name: str
    env_vars: List[str]
    default: Any

    @staticmethod
    def get_value_from_string(value_str: str) -> Any:
        raise NotImplementedError


@dataclass
class StringSettingDefinition(SettingDefinition):
    """Class for defining a setting with a string value."""

    default: str

    @staticmethod
    def get_value_from_string(value_str: str) -> str:
        return value_str


@dataclass
class OptionalStringSettingDefinition(SettingDefinition):
    """Class for defining a setting with a string value."""

    default: Optional[str]

    @staticmethod
    def get_value_from_string(value_str: Optional[str]) -> str:
        if value_str:
            return value_str
        else:
            return ""


@dataclass
class BoolSettingDefinition(SettingDefinition):
    """Class for defining a setting with a bool value."""

    default: bool

    @staticmethod
    def get_value_from_string(value_str: str) -> bool:
        return True if value_str.lower() in TRUES else False


@dataclass
class IntSettingDefinition(SettingDefinition):
    """Class for defining a setting with an int value."""

    default: int

    @staticmethod
    def get_value_from_string(value_str: str) -> int:
        return int(value_str)


@dataclass
class OptionalIntSettingDefinition(SettingDefinition):
    """Class for defining a setting with an optional int value (i.e. int or None)."""

    default: Optional[int]

    @staticmethod
    def get_value_from_string(value_str: str) -> Optional[int]:
        if value_str:
            return int(value_str)
        else:
            return None


@dataclass
class PathListSettingDefinition(SettingDefinition):
    """Class for defining a setting with a value which is a list of file paths.

    In JSON format, the value should appear as a list of strings. When stored in an
    environment variable, the value should take the form of a single string with the
    individual paths separated by ``os.pathsep`` (typically ``:``).
    """

    default: List[str]

    @staticmethod
    def get_value_from_string(value_str: str) -> List[str]:
        return value_str.split(os.pathsep)


class Settings:
    """Container class to hold settings definitions and the ``get_<type>()`` functions
    that fetch their values."""

    def __init__(self, settings_file_override=None) -> None:
        # Create all setting definitions
        self.definitions: Dict[str, SettingDefinition] = {}
        self.add_all_settings()

        if settings_file_override is not None:
            self.settings_file = settings_file_override
        else:
            self.settings_file = _get_settings_path()

        # Get the settings from the settings file on disk.
        # First, as a convenience to the user, if the settings file does not exist we
        # try to create one and write the default values to it.
        if not self.settings_file.is_file():
            _create_settings_json(
                self.settings_file,
                {
                    setting.name: setting.default
                    for setting in self.definitions.values()
                },
            )

        # Then, we read the settings in from the file. If they were just created, the
        # values will be the same as the defaults, but it's still useful to read them in
        # since the user will see a warning if there is a problem reading the file.
        # If there was a problem creating the settings file, it might still not exist,
        # so check again before trying to read it.
        if self.settings_file.is_file():
            self.settings_from_file = _load_settings_from_file(self.settings_file)
            self.add_qsub_extra_settings()
            self.check_for_extra_keys()
        else:
            self.settings_from_file = {}

        # Check for any mismatches in the qsub extra settings
        self.check_qsub_extras()

    def add_qsub_extra_settings(self):
        nqsub_extra = self.get_int("qsub_extra_count")
        for n in range(1, nqsub_extra + 1):
            self.get_qsub_extras(n)

    def check_qsub_extras(self):
        # check all qsub_extra settings are accounted for - only checking the names
        # as the help and default are fine with the default empty string.
        qsub_extra_count = self.get_int("qsub_extra_count")
        if qsub_extra_count > 0:
            missing_qsubextra = []
            for n in range(1, qsub_extra_count + 1):
                extra_name = f"qsub_extra{n}"
                if self.get_string(extra_name) == "":
                    missing_qsubextra.append(extra_name)
            if missing_qsubextra:
                logger.warning(
                    f"CCP-EM Pipeliner found {qsub_extra_count} qsub_extra"
                    " variables have been specified in the 'qsub_extra_count' setting,"
                    " but the expected settings for the following names are missing:"
                    f" {', '.join(missing_qsubextra)}"
                )

    def add_all_settings(self):
        self.add_path_list_setting(
            name="path_to_source_files",
            env_vars=["PIPELINER_PATH_TO_SOURCE_FILES"],
            default=[],
        )
        self.add_path_list_setting(
            name="additional_program_paths",
            env_vars=["PIPELINER_ADDITIONAL_PROGRAM_PATHS"],
            default=[],
        )
        self.add_string_setting(
            name="ctffind_executable",
            env_vars=["PIPELINER_CTFFIND_EXECUTABLE", "RELION_CTFFIND_EXECUTABLE"],
            default="ctffind",
        )
        self.add_int_setting(
            name="default_nrmpi",
            env_vars=["PIPELINER_DEFAULT_NRMPI", "RELION_QSUB_NRMPI"],
            default=1,
        )
        self.add_int_setting(
            name="default_nrthreads",
            env_vars=["PIPELINER_DEFAULT_NRTHREADS", "RELION_QSUB_NRTHREADS"],
            default=1,
        )
        self.add_string_setting(
            name="gctf_executable",
            env_vars=["PIPELINER_GCTF_EXECUTABLE", "RELION_GCTF_EXECUTABLE"],
            default="Gctf",
        )
        self.add_string_setting(
            name="motioncor2_executable",
            env_vars=[
                "PIPELINER_MOTIONCOR2_EXECUTABLE",
                "RELION_MOTIONCOR2_EXECUTABLE",
            ],
            default="MotionCor2",
        )
        self.add_int_setting(
            name="minimum_dedicated",
            env_vars=["PIPELINER_MINIMUM_DEDICATED", "RELION_MINIMUM_DEDICATED"],
            default=1,
        )
        self.add_optional_int_setting(
            name="mpi_max",
            env_vars=["PIPELINER_MPI_MAX", "RELION_MPI_MAX"],
            default=None,
        )
        self.add_string_setting(
            name="mpirun_command",
            env_vars=["PIPELINER_MPIRUN_COMMAND"],
            default="mpirun -n XXXmpinodesXXX",
        )
        self.add_string_setting(
            name="qsub_command",
            env_vars=["PIPELINER_QSUB_COMMAND", "RELION_QSUB_COMMAND"],
            default="qsub",
        )
        self.add_int_setting(
            name="qsub_extra_count",
            env_vars=["PIPELINER_QSUB_EXTRA_COUNT", "RELION_QSUB_EXTRA_COUNT"],
            default=0,
        )
        self.add_string_setting(
            name="qsub_template",
            env_vars=["PIPELINER_QSUB_TEMPLATE", "RELION_QSUB_TEMPLATE"],
            default="",
        )
        self.add_string_setting(
            name="queue_name",
            env_vars=["PIPELINER_QUEUE_NAME", "RELION_QUEUE_NAME"],
            default="openmpi",
        )
        self.add_bool_setting(
            name="queue_use",
            env_vars=["PIPELINER_QUEUE_USE", "RELION_QUEUE_USE"],
            default=False,
        )
        system = "mac64" if platform.system() == "Darwin" else "linux64"
        self.add_string_setting(
            name="resmap_executable",
            env_vars=["PIPELINER_RESMAP_EXECUTABLE", "RELION_RESMAP_EXECUTABLE"],
            default=f"ResMap-1.1.4-{system}",
        )
        self.add_string_setting(
            name="scratch_dir",
            env_vars=["RELION_SCRATCH_DIR"],
            default="",
        )
        self.add_optional_int_setting(
            name="thread_max",
            env_vars=["PIPELINER_THREAD_MAX", "RELION_THREAD_MAX"],
            default=None,
        )
        self.add_string_setting(
            name="topaz_executable",
            env_vars=["PIPELINER_TOPAZ_EXECUTABLE", "RELION_TOPAZ_EXECUTABLE"],
            default=(
                "relion_python_topaz"
                if shutil.which("relion_python_topaz")
                else "topaz"
            ),
        )
        self.add_string_setting(
            name="modelangelo_executable",
            env_vars=[
                "PIPELINER_MODELANGELO_EXECUTABLE",
                "RELION_MODELANGELO_EXECUTABLE",
            ],
            default=(
                "relion_python_modelangelo"
                if shutil.which("relion_python_modelangelo")
                else "modelangelo"
            ),
        )
        self.add_string_setting(
            name="dynamight_executable",
            env_vars=[
                "PIPELINER_DYNAMIGHT_EXECUTABLE",
                "RELION_DYNAMIGHT_EXECUTABLE",
            ],
            default=(
                "relion_python_dynamight"
                if shutil.which("relion_python_dynamight")
                else "dynamight"
            ),
        )
        self.add_optional_int_setting(
            name="warning_local_mpi",
            env_vars=["PIPELINER_WARNING_LOCAL_MPI", "RELION_WARNING_LOCAL_MPI"],
            default=None,
        )
        self.add_optional_string_setting(
            name="ccpem_share_dir",
            env_vars=["PIPELINER_CCPEM_SHARE_DIR"],
            default=None,
        )
        self.add_string_setting(
            name="modelcraft_executable",
            env_vars=["PIPELINER_MODELCRAFT_EXECUTABLE"],
            default="modelcraft",
        )

    def check_for_extra_keys(self):
        diff = set(self.settings_from_file.keys()).difference(self.definitions.keys())
        if diff:
            logger.warning(
                f"CCP-EM Pipeliner found unrecognised settings in the settings"
                f" file at {self.settings_file}. The following settings were not"
                f" recognised and will be ignored:"
                f" {' '.join(str(name) for name in diff)}. Perhaps the settings were"
                f" written by an older version of the pipeliner or a mistake was made"
                f" while editing the file?"
            )

    def add_string_setting(self, *, name: str, env_vars: List[str], default: str):
        self.definitions[name] = StringSettingDefinition(name, env_vars, default)

    def add_optional_string_setting(
        self, *, name: str, env_vars: List[str], default: Optional[str]
    ):
        self.definitions[name] = OptionalStringSettingDefinition(
            name, env_vars, default
        )

    def add_bool_setting(self, *, name: str, env_vars: List[str], default: bool):
        self.definitions[name] = BoolSettingDefinition(name, env_vars, default)

    def add_int_setting(self, *, name: str, env_vars: List[str], default: int):
        self.definitions[name] = IntSettingDefinition(name, env_vars, default)

    def add_optional_int_setting(
        self, *, name: str, env_vars: List[str], default: Optional[int]
    ):
        self.definitions[name] = OptionalIntSettingDefinition(name, env_vars, default)

    def add_path_list_setting(
        self, *, name: str, env_vars: List[str], default: List[str]
    ):
        self.definitions[name] = PathListSettingDefinition(name, env_vars, default)

    def get_setting_value(self, name: str):
        setting_def = self.definitions[name]

        # Environment variables take precedence. Search in order to see if one is set.
        value = None
        for env_var_name in setting_def.env_vars:
            value_str = os.getenv(env_var_name)
            if value_str is not None:
                value = setting_def.get_value_from_string(value_str)
                break

        # Next try looking for the setting in the settings file
        if value is None:
            value = self.settings_from_file.get(name)

        # If we have not found a value yet, use the default
        if value is None:
            value = setting_def.default

        return value

    def get_string(self, name: str) -> str:
        value = self.get_setting_value(name)
        assert isinstance(value, str)
        return value

    def get_optional_string(self, name: str) -> str:
        value = self.get_setting_value(name)
        assert isinstance(value, str) or not value
        return value

    def get_bool(self, name: str) -> bool:
        value = self.get_setting_value(name)
        assert isinstance(value, bool)
        return value

    def get_int(self, name: str) -> int:
        value = self.get_setting_value(name)
        assert isinstance(value, int)
        return value

    def get_optional_int(self, name: str) -> Optional[int]:
        value = self.get_setting_value(name)
        assert value is None or isinstance(value, int)
        return value

    def get_list(self, name: str) -> List[str]:
        value = self.get_setting_value(name)
        assert isinstance(value, list)
        return value

    def get_qsub_extras(self, number: int) -> Dict[str, str]:
        """Add definitions for the requested qsub extra settings and get their values"""
        extra_name = f"qsub_extra{number}"
        default_name = f"{extra_name}_default"
        help_name = f"{extra_name}_help"
        if extra_name not in self.definitions:
            self.add_string_setting(
                name=extra_name,
                env_vars=[
                    f"PIPELINER_QSUB_EXTRA{number}",
                    f"RELION_QSUB_EXTRA{number}",
                ],
                default="",
            )
            self.add_string_setting(
                name=default_name,
                env_vars=[
                    f"PIPELINER_QSUB_EXTRA{number}_DEFAULT",
                    f"RELION_QSUB_EXTRA{number}_DEFAULT",
                ],
                default="",
            )
            self.add_string_setting(
                name=help_name,
                env_vars=[
                    f"PIPELINER_QSUB_EXTRA{number}_HELP",
                    f"RELION_QSUB_EXTRA{number}_HELP",
                ],
                default="",
            )
        return {
            "name": self.get_string(extra_name),
            "default": self.get_string(default_name),
            "help": self.get_string(help_name),
        }


def _get_settings_path() -> Path:
    """Get ccpem-pipeliner settings file from platform-specific standard location"""
    user_platform = platform.system()
    # Linux
    if user_platform == "Linux":
        config_path = Path(os.getenv("XDG_CONFIG_HOME", Path("~/.config").expanduser()))
    # Mac
    elif user_platform == "Darwin":
        config_path = Path("~/Library/Application Support/").expanduser()
    else:
        raise RuntimeError(f'Platform "{user_platform}" is not supported.')

    return config_path / "ccpem" / "pipeliner" / "ccpem-pipeliner-settings.json"


def _load_settings_from_file(path) -> Dict[str, str]:
    """Load settings from a json file and return them in a dictionary.

    If there is a problem, print a warning and return an empty dictionary.
    """
    try:
        with path.open() as jf:
            settings = json.loads(jf.read())
        return settings
    except Exception:
        logger.warning(
            f"CCP-EM Pipeliner could not load a settings file at {path}."
            " Settings can still be set using environment variables.",
            exc_info=True,
        )
        return {}


def _create_settings_json(path, settings: dict):
    """Create settings json. If there is a problem, print a warning and continue."""
    try:
        # Create settings directory and file
        path.parent.mkdir(parents=True, exist_ok=True)
        with path.open("w", encoding="utf-8") as f:
            json.dump(settings, f, ensure_ascii=False, indent=4)
    except Exception:
        logger.warning(
            f"CCP-EM Pipeliner could not create a settings file at {path}."
            " Settings can still be set using environment variables.",
            exc_info=True,
        )
