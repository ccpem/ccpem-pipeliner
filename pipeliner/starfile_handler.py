#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
from pathlib import Path  # noqa: F401 # import needed for docs build but not in code
from typing import Optional, List, Dict, Union
from copy import deepcopy
import shlex
from gemmi import cif
import logging

from pipeliner.data_structure import TRUES
from pipeliner.star_writer import write_jobstar, star_quote
from pipeliner.relion_compatibility import (
    relion31_jobtype_conversion,
    convert_relion3_1_pipeline,
    convert_relion_4_pipeline,
    get_pipeline_version,
    additional_job_conversions,
)


logger = logging.getLogger(__name__)

datalabels = (
    "data_",
    "data_pipeline_general",
    "data_pipeline_processes",
    "data_pipeline_nodes",
    "data_pipeline_input_edges",
    "data_pipeline_output_edges",
    "data_job",
    "data_joboptions_values",
    "data_joboptions_full_definition",
    "data_guinier",
    "data_fsc",
    "data_general",
    "data_optics",
    "data_micrographs",
    "data_particles",
    "data_movies",
    "data_schedule_general",
    "data_schedule_bools",
    "data_schedule_floats",
    "data_schedule_strings",
    "data_schedule_operators",
    "data_schedule_jobs",
    "data_schedule_edges",
    "data_output_nodes",
    "data_filament_splines",
    "data_coordinates",
    "data_model_general",
    "data_model_class",
    "data_class_averages",
)


def quotate_starfile_line(in_line: str) -> Optional[str]:
    """Adds the necessary quotation to a line to make it compatible with gemmi

    Args:
        in_line (str): The line to operate on

    Returns:
        str: The line, properly quotated, or ``None`` if no changes to the line are
        needed
    """
    # Use posix=False here to avoid stripping quotes from values
    line_split = shlex.split(in_line, posix=False)
    stripped_quotes = [cif.as_string(item) for item in line_split]
    requoted = [star_quote(item) for item in stripped_quotes]
    if requoted == line_split:
        return None
    else:
        return " ".join(requoted)


def fix_reserved_words(fn_in):
    """Make sure the starfile doesn't contain any illegally used reserved words

    Overwrites the original file if it is corrected.  The old file is saved as
    ``filename.orig``.

    Returns:
        ``True`` if the file was corrected, or ``False`` if it contained no reserved
        words and did not need correcting.
    """

    with open(fn_in) as thefile:
        lines = thefile.readlines()

    # go line by line and look for reserved words used badly
    been_corrected = False
    for nline, line in enumerate(lines):
        if not any(
            [
                line.rstrip() == "loop_",
                any(label in line.rstrip() for label in datalabels),
                line[0] == "#",
                line.isspace(),
                line.startswith("_rln"),
            ]
        ):
            fixed_line = quotate_starfile_line(line)
            if fixed_line:
                lines[nline] = fixed_line + "\n"
                been_corrected = True

    # if the file has been corrected write a file with the corrections
    if been_corrected:
        oldname = fn_in + ".orig"
        shutil.move(fn_in, oldname)

        with open(fn_in, "w") as out_file:
            for ln in lines:
                out_file.write(ln)
            out_file.flush()
            os.fsync(out_file.fileno())

        logger.warning(
            f"Reserved words were found in STAR file file '{fn_in}' and have"
            " been corrected. The file has been overwritten and original saved as"
            f" {oldname}"
        )
    return been_corrected


def convert_old_relion_pipeline(fn_in: str):
    """Convert a pipeline STAR file from RELION 3 or 4 to ccpem-pipeliner format.

    The file is also checked and corrected if it contains any reserved words which would
    cause Gemmi's CIF parser to fail when the file is read.

    Note that this function reads the whole file several times, but pipeline STAR files
    are never expected to be so large that this would take a significant amount of time.

    Returns:
        ``True`` if the file was converted, or ``False`` otherwise.

    Raises:
        ValueError: if the pipeline file is invalid or its version cannot be determined
    """
    been_converted = fix_reserved_words(fn_in)
    data = cif.read_file(fn_in)
    pipeline_version = get_pipeline_version(data)
    if pipeline_version == "relion3":
        convert_relion3_1_pipeline(fn_in)
        been_converted = True
    elif pipeline_version == "relion4":
        convert_relion_4_pipeline(fn_in)
        been_converted = True
    return been_converted


class StarFile(object):
    """A superclass for all types of starfiles used by the pipeliner

    Attributes:
        file_name (str or :class:`~pathlib.Path`): The star file
    """

    def __init__(self, fn_in: Union[str, "os.PathLike[str]"]):
        if not fn_in:
            raise ValueError("File name cannot be empty")
        self.file_name = fn_in
        try:
            # Try to read the file first so if it is valid (the normal case) it is read
            # only once
            self.data = cif.read_file(str(self.file_name))
        except Exception as orig_ex:
            # If the file is not valid, try to fix any reserved words and try again
            fix_reserved_words(fn_in)
            try:
                self.data = cif.read_file(str(self.file_name))
            except Exception as new_ex:
                raise ValueError(
                    f"File {self.file_name} is not a valid STAR file.\nThe following"
                    f" exception was raised when trying to read it: {orig_ex}\nAfter"
                    f" an attempt to fix the file automatically, the following"
                    f" exception was raised when trying to read the file again:"
                    f" {new_ex}"
                ) from new_ex

    def pairs_as_dict(self, block: str = "") -> Dict[str, str]:
        """Returns paired values from a starfile as a dict

        Args:
            block (str): The name of the block to get the data from

        Returns:
            dict: `{parameter: value}`

        Raises:
            ValueError: If the specified block is not found
            ValueError: If the specified block is a loop and not a pair-value
        """
        if block:
            fblock = self.data.find_block(block)
        else:
            fblock = self.data.sole_block()
        if fblock is None:
            raise ValueError(f"Block '{block}' not found")
        out_dict = {}
        if fblock[0].pair is None:
            raise ValueError(
                f"Block '{block}' does not contain item-value pairs. "
                "Try using star_loop_as_list() instead"
            )
        for item in fblock:
            out_dict[item.pair[0]] = item.pair[1]
        return out_dict

    def loop_as_list(
        self,
        block: str = "",
        columns: Optional[List[str]] = None,
        headers: bool = False,
    ) -> List[List[str]]:
        """Returns a set of columns from a starfile loop as a list

        Args:
            block (str): The name of the block to get the data from
            columns (list): Names of the columns to get, if None then all
                columns are returned
            headers (bool): Should the 1st row of the returned list be the header names

        Returns:
            List[List[str]]: The column data one row per sublist.  If headers=True
                the first sublist contains the headers

        Raises:
            ValueError: If the specified block is not found
            ValueError: If the specified block does not contain a loop
            ValueError: If any of the specified columns are not found
        """
        if block:
            fblock = self.data.find_block(block)
        else:
            fblock = self.data.sole_block()
        if fblock is None:
            raise ValueError(f"ERROR: The requested block {block} was not found")
        try:
            loop = fblock[0].loop
        except IndexError:
            raise ValueError(
                f"Block '{block}' does not contain a loop; Use star_pairs_as_dict()"
                " instead"
            )
        if loop is None:
            raise ValueError(
                f"Block '{block}' does not contain a loop; try star_pairs_as_dict()"
                " instead"
            )

        cols = loop.tags[0:] if columns is None else columns
        for col in cols:
            if col not in loop.tags[0:]:
                raise ValueError(f"Column '{col}' not found in block '{block}'")
        data = [cols] if headers else []
        for row in fblock.find(cols):
            data.append([cif.as_string(x) for x in row])
        return data


class JobStar(StarFile):
    """A class for star files that define a pipeliner job parameters

    Attributes:
        jobtype (str): the job type, converted from the relion nomenclature if
            necessary
        joboptions(dict): The joboptions {name: value} all values are strings regardless
            of joboption type
        is_continue (bool): is the job a continuation
        is_tomo (bool): is the job tomography (might be removed when relon compatobility
            is deprecated)
    """

    def __init__(self, fn_in: str):
        super().__init__(fn_in)
        self.is_continue = False
        self.is_tomo = False
        self.joboptions: Dict[str, str] = {}
        self.jobtype = ""

        job_block = self.data.find_block("job")
        options_block = self.data.find_block("joboptions_values")
        if options_block is None or job_block is None:
            raise ValueError("File does not appear to be a valid job.star file")
        options_table = options_block.find(
            ["_rlnJobOptionVariable", "_rlnJobOptionValue"]
        )

        for item in options_table:
            key = cif.as_string(item[0])
            val = cif.as_string(item[1])
            self.joboptions[key] = val

        for item in job_block:
            key = cif.as_string(item.pair[0])
            val = cif.as_string(item.pair[1])
            if key == "_rlnJobTypeLabel":
                self.jobtype = additional_job_conversions(val)

            elif key == "_rlnJobType":
                self.jobtype = relion31_jobtype_conversion(int(val), self.joboptions)
            elif key == "_rlnJobIsContinue":
                self.is_continue = val in TRUES
            elif key == "_rlnJobIsTomo":
                self.is_tomo = val in TRUES

    def all_options_as_dict(self) -> Dict[str, str]:
        """Returns a dict of all the parameters of a jobstar file

        The dict contains both the job options and the running options.
        All values in the dict are strings.
        """
        is_continue = "1" if self.is_continue else "0"
        is_tomo = "1" if self.is_tomo else "0"
        run_dict = {
            "_rlnJobTypeLabel": self.jobtype,
            "_rlnJobIsContinue": is_continue,
            "_rlnJobIsTomo": is_tomo,
        }
        return {**run_dict, **self.joboptions}

    def make_substitutions(self, conversions: Dict[str, str]):
        """Substitute one job name or file name for another wherever it appears

        Args:
            conversions (Dict[str, str]): {"old job name": "new job name"}
        """
        updated = deepcopy(self.joboptions)

        for jobop in self.joboptions:
            for conv in conversions:
                if conv in self.joboptions[jobop]:
                    updated[jobop] = self.joboptions[jobop].replace(
                        conv, conversions[conv]
                    )
        self.joboptions = updated

    def write(self, outname: Union[str, "os.PathLike[str]"] = ""):
        """Write a star file from the JobStar

        Args:
            outname (str or :class:`~pathlib.Path`): name of the output file; will
                overwrite the original file if none provided
        """
        outfile = self.file_name if not outname else outname
        logger.info(f"Writing jobstar file {Path(outfile).absolute()}")
        write_jobstar(self.all_options_as_dict(), outfile)

    def modify(self, params_to_change: dict, allow_add=False):
        """Change multiple values in a jobstar from a dict

        Args:
            params_to_change (dict): The Parameters to change in the template in the
                format {param_name: new_value}
            allow_add (bool): Can new joboptions be added?
        Raises:
            ValueError: If an attempt is made to add new fields with allow_add=False
        """

        # change any necessary parameters

        new_continue = params_to_change.get("_rlnJobIsContinue")
        new_tomo = params_to_change.get("_rlnJobIsTomo")
        new_type = params_to_change.get("_rlnJobTypeLabel")

        self.is_continue = new_continue in TRUES if new_continue else self.is_continue
        self.is_tomo = new_tomo if new_tomo in TRUES else self.is_tomo
        self.jobtype = new_type if new_type else self.jobtype

        for changed_opt in params_to_change:
            if changed_opt not in [
                "_rlnJobIsContinue",
                "_rlnJobIsTomo",
                "_rlnJobTypeLabel",
            ]:
                if not allow_add and changed_opt not in self.joboptions:
                    raise ValueError(f"JobOption {changed_opt} not in the starfile")
                self.joboptions[changed_opt] = params_to_change[changed_opt]


class BodyFile(StarFile):
    """A star file that lists the bodies in a multibody refinement

    Attributes:
       bodycount (int): Number bodies in the file
    """

    def __init__(self, fn_in: str):
        super().__init__(fn_in)
        bodies_block = self.data.sole_block()
        self.bodycount = len(bodies_block.find_loop("_rlnBodyMaskName"))


class DataStarFile(StarFile):
    """A general class for starfiles that contain data, such as particles files

    Attributes:
        data (:class:`gemmi.cif.Document`): A gemmi cif object containing the data from
            the star file
    """

    def get_block(self, blockname: Optional[str] = None) -> cif.Block:
        """Get a block from the star file

        Args:
            blockname (str): The name of the block to get.  Use ``None``
                if the file has a single unnamed block

        Returns:
            (:class:`gemmi.cif.Block`): The desired block or None if not found
        """
        block = None
        if blockname is not None:
            block = self.data.find_block(blockname)
        elif blockname is None:
            block = self.data.sole_block()
        if block is None:
            if blockname is not None:
                raise ValueError(f"Block {blockname} not found")
            else:
                raise ValueError("No block found in file")
        return block

    def count_block(self, blockname: Optional[str] = None) -> int:
        """Count the number of items in a block that only contains a single loop

        This is the format in most relion data star files

        Args:
            blockname (str): The name of the block to count

        Returns:
            int: The count
        """
        block = self.get_block(blockname)
        item_count = int(shlex.split(str(block[0].loop))[1])
        return item_count

    def column_as_list(self, blockname: Optional[str], colname: str) -> List[str]:
        """Return a single column from a block as a list

        Args:
            blockname (str): The name of the block to use
            colname (str): The name of the column to get

        Returns:
            list: The values from that column
        """

        coldata = self.get_block(blockname).find_loop(colname)
        return list(coldata)


def read_relion_optimiser_starfile(fn_in) -> Dict[str, str]:
    """Special function for reading optimiser.star files written by relion

    These files have issues with duplicate entries because of a Relion5 bug, which
    precludes use of gemmi because it raises an error if it encounters duplicate entires
    If a duplicate entry exists it uses the value from the last instance.

    Args:
        fn_in (str): Name of the optimiser file

    Returns:
        Dict[str, str]: The param-value pairs from the file

    Raises:
        ValueError: If the file isn't an optimiser file
    """
    with open(fn_in) as f:
        data = [x.strip() for x in f.readlines()]
    if "data_optimiser_general" not in data:
        raise ValueError("File does not appear to be in Relion optimiser format")
    pairs = {}
    for line in data:
        line_spit = shlex.split(line)
        if len(line_spit) == 2 and "_rln" in line_spit[0]:
            pairs[line_spit[0]] = line_spit[1]
    return pairs


def interconvert_box_star_coords(fn_in: str, out_name: Optional[str] = None) -> str:
    """Interconvert .box and .star coordinate files

    Args:
        fn_in (str): The file to convert
        out_name (str): Dir to write the output file into, if None writes it the same
            place as the input, with the same name just switching the extension

    Returns:
        str: Name of the file written

    Raises:
        ValueError: If the file is not .box or .star
    """

    def get_outfile() -> str:
        """Subfunction to get the output file name"""
        if ".star" in fn_in:
            outfile = fn_in.replace(".star", ".box") if out_name is None else out_name
        else:
            outfile = fn_in.replace(".box", ".star") if out_name is None else out_name
        odir = os.path.dirname(outfile)
        if odir:
            os.makedirs(odir, exist_ok=True)
        return outfile

    coords, dim = [], ["_rlnCoordinateX", "_rlnCoordinateY", "_rlnCoordinateZ"]
    # box -> star
    if fn_in.split(".")[-1] == "box":
        with open(fn_in) as in_f:
            raw_coords = [x.split() for x in in_f.readlines()]
            for line in raw_coords:
                if line[0] != "#":
                    coords.append(line)

        outcif = cif.Document()
        coords_block = outcif.add_new_block("coordinates")
        coord_loop = coords_block.init_loop("", dim[: len(raw_coords[0])])
        for line in raw_coords:
            coord_loop.add_row(line)
        outcif.write_file(get_outfile())

    # star -> box
    elif fn_in.split(".")[-1] == "star":
        data = DataStarFile(fn_in)
        try:
            raw_coords = data.loop_as_list("coordinates", dim)
        except ValueError:
            raw_coords = data.loop_as_list("coordinates", dim[:2])

        with open(get_outfile(), "w") as f_out:
            for line in raw_coords:
                f_out.write(" ".join(line) + "\n")

    # wrong file type
    else:
        raise ValueError("Wrong file format: Must be .star or .box")

    return get_outfile()


def get_starfile_loop_headers(in_file: str) -> Dict[str, List[str]]:
    """Get the headers for each loop in each block in a starfile

    Args:
        in_file (str): The star file to operate on

    Returns:
        Dict[str, List[str]]: {block_name: [header1, header2, ... headerN]} Headers
            appear in the column order of the star file.
    """
    cif_document = cif.read_file(in_file)

    result = {}

    for block in cif_document:
        block_name = block.name
        columns = []
        for item in block:
            if isinstance(item.loop, cif.Loop):
                for tag in item.loop.tags:
                    columns.append(tag)

        result[block_name] = columns
    return result
