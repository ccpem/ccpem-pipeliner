#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from pipeliner.job_factory import read_job
from pipeliner.star_writer import write_jobstar
from pipeliner.relion_compatibility import additional_job_conversions


class RunJobFile(object):
    """A class for the old style run.job file in RELION. These
    are actually easier to work with than the current job.star files
    because they have more verbose descriptions for the parameters

    """

    def __init__(self, file_name: str):
        """Init the class

        Args:
            file_name (str): The name of the run.job file
        """
        self.file_name = file_name
        self.dummy_job = read_job(file_name)

    def get_all_options(self):
        """Get a dictionary of the parameters specified by the file

        This is in the format appropriate for running a job from a
        dict.  The keys are the parameter names like in a job.star
        file.

        Returns:
            dict: {parameter: value}
        """
        params_dict = {}
        for jo in self.dummy_job.joboptions:
            params_dict[jo] = str(self.dummy_job.joboptions[jo].value)
        params_dict["_rlnJobTypeLabel"] = additional_job_conversions(
            self.dummy_job.PROCESS_NAME
        )
        params_dict["_rlnJobIsContinue"] = "1" if self.dummy_job.is_continue else "0"
        params_dict["_rlnJobIsTomo"] = "1" if self.dummy_job.is_tomo else "0"
        return params_dict

    def convert_to_jobstar(self, output_name: str = ""):
        """Convert the run.job file to a job.star file

        Args:
            output_name (str): Name for the output file if `None`
                it gives it the same name as the input file but
                with _job.star added.  If using a custom name
                make sure it ends with job.star

        Raises:
            ValueError: If the specified name does not end with 'job.star'
        """

        options = self.get_all_options()
        out_fn = output_name
        if not output_name:
            out_fn = self.file_name.replace(".job", "_job.star")
        if not out_fn.endswith(".star"):
            raise ValueError("JobStar output file must have .star extension")
        write_jobstar(options, out_fn)
