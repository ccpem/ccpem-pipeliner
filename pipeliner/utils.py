#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""These utilities are used by the pipeliner for basic tasks such as nice looking
on-screen display, checking file names, and getting directory and file names"""

import uuid
from datetime import datetime
from pathlib import Path
from typing import List, Tuple, Union, Optional, Dict
import os
import subprocess
import sys
import re
import time
import json
import shlex
import threading
from itertools import chain
import mrcfile
import numpy as np
from collections import OrderedDict
from io import StringIO
from pipeliner.data_structure import JobInfoData, JOBINFO_FILE


def format_string_to_type_objs(in_str: str) -> Optional[Union[str, int, float, bool]]:
    """Returns Int, Float, Bool, and None Objects from strings

    Any number with a decimal point, in scientific notation, or 'NaN' will return a
    float
    Any other number will retun an int
    'False' or 'false' returns False
    'True' or  'true' returns True
    'None' returns a NoneType object

    Args:
        in_str (str): The input string

    Returns:
        Optional[Union[str, int, float, bool]]: The appropriate object
    """
    try:
        float(in_str)
    except ValueError:
        in_str = in_str.strip()
        if in_str.lower() == "true":
            return True
        elif in_str.lower() == "false":
            return False
        elif in_str == "None":
            return None
        else:
            return in_str

    if "." in in_str or "e" in in_str or in_str.lower() == "nan":
        return float(in_str)
    else:
        return int(in_str)


def touch(filename: str):
    """Create an empty file

    Args:
        filename (str): The name for the file to create
    """
    open(filename, "w").close()


def get_pipeliner_root() -> Path:
    """Get the directory of the main pipeliner module

    Returns:
        :class:`~pathlib.Path`: The path of the pipeliner
    """
    return Path(__file__).parent


def get_job_script(name: str) -> str:
    """Get the full path to a job script file.

    Returns:
        The job script file, if it exists.

    Raises:
        FileNotFoundError: if the named job script cannot be found.
    """
    script = get_pipeliner_root() / "scripts" / "job_scripts" / name
    if not script.is_file():
        raise FileNotFoundError(f"Could not find job script with name {name}")
    return str(script)


def get_job_runner_command() -> List[str]:
    """Get the full command to run the job_runner.py script."""
    command = get_python_command()
    job_runner_script = str(get_pipeliner_root() / "scripts" / "job_runner.py")
    command.append(job_runner_script)
    return command


def get_regenerate_results_command() -> List[str]:
    """Get the full command to run the regenerate_results.py script."""
    command = get_python_command()
    results_script = str(get_pipeliner_root() / "scripts" / "regenerate_results.py")
    command.append(results_script)
    return command


def get_python_command() -> List[str]:
    """Get the command to launch the current Python interpreter.

    Note that the command is returned as a list and might include some arguments as well
    as the command itself.
    """
    # Workaround for running in Doppio packaged version: doppio-web can be
    # used to run Python scripts but requires an extra argument.
    if os.path.basename(sys.executable) == "doppio-web":
        return [sys.executable, "run-script"]
    else:
        return [sys.executable]


def truncate_number(number: float, maxlength: int) -> str:
    """Return a number with no more than x decimal places but no trailing 0s

    This is used to format numbers in the exact same way that RELION does it.
    IE: with maxlength 3; 1.2000 = 1.2, 1.0 = 1, 1.23 = 1.23. RELION commands are happy
    to accept numbers with any number of decimal places or trailing 0s. This
    function is just to maintain continuity between RELION and pipeliner
    commands

    Args:
        number (float): The number to be truncated
        maxlength (int): The maximum number of decimal places
    """

    rounded = round(number, maxlength)
    if int(rounded) == rounded:
        return str(int(rounded))
    return str(rounded)


def decompose_pipeline_filename(fn_in: str) -> Tuple[str, int, str]:
    """Breaks a job name into usable pieces

    Returns everything before the job number, the job number as an int
    and everything after the job number setup for up to 20 dirs deep.
    The 20 directory limit is from the relion code but no really necessary anymore

    Args:
        fn_in (str): The job or file name to be broken down in the format:
            <jobtype>/jobxxx/<filename>

    Returns:
        tuple: The decomposed file name: (:class:`str`, :class:`int`, :class:`str`)
            `[0]` Everything before 'job' in the file name

            `[1]` The job number

            `[2]` Everything after the job number

    Raises:
        ValueError: If the input file name is more than 20 directories deep
    """

    fn_split = fn_in.split("/")
    # > 20 dirs doesn't really need to be an error anymore with the way it
    # is being done now.
    if len(fn_split) > 20:
        raise ValueError(
            "decomposePipelineFileName: BUG or found more than 20"
            " directories deep structure for pipeline filename: " + fn_in
        )
    # return everything before job number, job number, and everything after
    i = 0
    for chunk in fn_split:
        if len(chunk) >= 6 and chunk[0:3] == "job":
            try:
                fn_jobnr = int(chunk[3:])
            except ValueError:
                fn_jobnr = 0
            if fn_jobnr:
                fn_pre = "/".join(fn_split[:i])
                fn_post = "/".join(fn_split[i + 1 :])
                return fn_pre, fn_jobnr, fn_post
        i += 1
    # return just the file name with jobnumber 0 if not a pipeliner file
    return "", 0, fn_in


def date_time_tag(compact: bool = False) -> str:
    """Get a current date and time tag

    It can return a compact version or one that is easier to read

    Args:
        compact (bool): Should the returned tag be in the compact form

    Returns:
        str: The datetime tag

        compact format is: `YYYYMMDDHHMMSS`

        verbose form is: `YYYY-MM-DD HH:MM:SS.MS`
    """
    now = datetime.now()
    if compact:
        date_time = now.strftime("%Y%m%d%H%M%S")
    else:
        date_time = now.isoformat(sep=" ", timespec="milliseconds")

    return date_time


def check_for_illegal_symbols(
    check_string: str, string_name: str = "input", exclude: str = ""
) -> Optional[str]:
    """Check a text string doesn't have any of the disallowed symbols.

    Illegal symbols are !*?()^/\\#<>&%{}$."' and @.

    Args:
        check_string (str): The string to be checked
        string_name (str): The name of the string being checked; for more informative
            error messages
        exclude (str): Any symbols that are normally in the illegal symbols list but
            should be allowed.
    Returns:
        str: An error message if any illegal symbols are present
    """
    badsym = ""
    illegal = [
        "!",
        "*",
        "?",
        "(",
        ")",
        "^",
        "/",
        "\\",
        "|",
        "#",
        "<",
        ">",
        "&",
        "%",
        "{",
        "}",
        "$",
        ",",
        '"',
        "'",
        "@",
        ":",
    ]
    for symbol in exclude:
        illegal.remove(symbol)
    for symbol in illegal:
        if symbol in check_string:
            badsym += symbol
    if len(badsym) > 0:
        return (
            f"ERROR: Symbol(s) '{''.join(badsym)}' in {string_name}: {check_string}."
            f" {string_name} cannot contain any of the following symbols:"
            f" {' '.join(illegal)}"
        )
    return None


def clean_job_dirname(dirname: str) -> str:
    """Makes sure a pipeline job_dir name is valid and in the right format

    Args:
        dirname (str): The dirname to check

    Returns:
        str: The correctly formatted dirname

    Raises:
        ValueError: If the dir name connot be formatted correctly
    """
    dirname = dirname + "/" if not dirname.endswith("/") else dirname
    exp = "^\\w*/job\\d{3}\\/$"
    reg = re.compile(exp)
    if not reg.match(dirname):
        raise ValueError(
            "Invalid job dir name; must be in format 'JobType/jobXXX/' relative to the "
            "project directory"
        )
    return dirname


def clean_jobname(jobname: str) -> str:
    """Makes sure job names are in the correct format

    Job names must have a trailing slash, cannot begin with a slash,
    and have no illegal characters

    Args:
        jobname (str): The job name to be checked

    Returns:
        str: The job name, with corrections in necessary

    """
    # fix missing trailing slashes
    if jobname[-1] != "/":
        jobname = jobname + "/"

    # fix leading slashes
    if jobname[0] == "/":
        jobname = jobname[1:]

    # fix double slashes
    if "//" in jobname:
        jobname = "".join(
            jobname[i]
            for i in range(len(jobname))
            if i == 0 or not (jobname[i - 1] == jobname[i] and jobname[i] == "/")
        )

    # return error if illegal characters present
    error_message = check_for_illegal_symbols(jobname, "job name", "/")
    if error_message:
        raise ValueError(error_message)
    return jobname


def get_job_number(job_name):
    """Get the job number from  a pipeliner job as an int

    Args:
        job_name (str): The job name in the pipeliner format
    returns:
        int: The job number
    """
    jn = clean_jobname(job_name)
    pattern = r"/job(\d+)/"
    jobnos = set(re.findall(pattern, jn))
    if len(jobnos) != 1:
        raise ValueError(f"Error getting job number from {jn}")
    return int(jobnos.pop())


def print_nice_columns(
    list_in: List[str], err_msg: str = "ERROR: No items in input list"
):
    """Takes a list of items and makes three columns for nicer on-screen display

    Args:
        list_in (str): The list to display in columns
        err_msg (str): The message to display if the list is empty
    """

    if len(list_in) == 0:
        print(f"\n{err_msg}")
        return

    list_in.sort()
    if len(list_in) <= 10:
        for i in list_in:
            print(i)
        print("\n")
        return

    third = int(len(list_in) / 3)
    chunk1 = list_in[0:third]
    c1 = max([len(x) for x in chunk1]) + 2
    chunk2 = list_in[third : 2 * third]
    c2 = max([len(x) for x in chunk2]) + 2
    chunk3 = list_in[2 * third :]
    c3 = max([len(x) for x in chunk3]) + 2
    comb_data = zip(chunk1, chunk2, chunk3)

    spacer = ["", ""]
    if len(chunk1) < len(chunk3):
        chunk1 += spacer
        chunk2 += spacer
    elif len(chunk3) < len(chunk1):
        chunk3 += spacer

    for row in comb_data:
        print(f"{row[0].ljust(c1)} {row[1].ljust(c2)} {row[2].ljust(c3)}")
    print("\n")


def make_pretty_header(
    text: str, char: str = "-=", top: bool = True, bottom: bool = True
):
    """Make nice looking headers for on-screen display

    Args:
        text (str): The text to put in the header
        char (str): What characters to use for the header
        top (bool): Put a border on the top?
        bottom (bool): Put a border on the bottom

    Returns:
        str: A nice looking header
    """
    length = max([len(x) for x in text.split("\n")])
    if len(char) > 1:
        tb = bb = char * int(length / len(char))
        mod = length % len(char)
        tb += char[0:mod]
        bb += char[0:mod]

    else:
        tb = bb = "-" * length
    tb += "\n"
    bb = f"\n{bb}"

    tb = tb if top else ""
    bb = bb if bottom else ""

    return f"{tb}{text}{bb}"


def wrap_text(text_string: str):
    """Produces <= 55 character wide wrapped text for on-screen display

    Args:
        text_string (str): The text to be displayed
    """
    n = 0
    printed = 0
    text_split = text_string.split(" ")
    while printed <= len(text_split) - 1:
        line_string = ""
        length = 0
        while length < 55 and printed <= len(text_split) - 1:
            line_string += text_split[n] + " "
            length += len(text_split[n])
            if "\n" in text_split[n]:
                length = 0
            printed += 1
            n += 1
        print(line_string)


def find_common_string(input_strings: List[str]) -> str:
    """Find the common part of a list of strings starting from the beginning

    Args:
        input_strings (list): List of strings to compare

    Returns:
        str: The common portion of the strings

    Raises:
        ValueError: If input_list is shorter than 2
    """

    common = ""
    for n, character in enumerate(list(input_strings[0])):
        for the_str in input_strings:
            if list(the_str)[n] != character:
                return common
        common += character
    return common


def launch_detached_process(command: List[str], timeout: float) -> Optional[int]:
    """Run the given command as a detached process.

    The process is started in a new session and with all file handles set to null, to
    ensure it keeps running in the background after the parent Python process exits.

    Args:
        command (List[str]): The commands to execute
        timeout (int): How many seconds to wait to check if the command failed early

    Returns:
        int | None: The process's return code, if available, or None if it is still
            running after `timeout` seconds
    """
    process = subprocess_popen(
        command,
        stdin=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        start_new_session=True,
    )
    try:
        rc = process.wait(timeout=timeout)
        return rc
    except subprocess.TimeoutExpired:
        return None


def subprocess_popen(*args, **kwargs) -> subprocess.Popen:
    # Check if we are in a Pyinstaller bundle, and if so, reverse its changes to the
    # LD_LIBRARY_PATH
    # See https://pyinstaller.org/en/stable/runtime-information.html
    env = None
    if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
        env = dict(os.environ)  # make a copy of the environment
        lp_key = "LD_LIBRARY_PATH"  # for GNU/Linux and *BSD.
        lp_orig = env.get(lp_key + "_ORIG")
        if lp_orig is not None:
            env[lp_key] = lp_orig  # restore the original, unmodified value
        else:
            # This happens when LD_LIBRARY_PATH was not set.
            # Remove the env var as a last resort:
            env.pop(lp_key, None)
    arg: List[str] = list(*args)
    return subprocess.Popen(arg, env=env, **kwargs)


def run_subprocess(*args, **kwargs) -> subprocess.CompletedProcess:
    # Check if we are in a Pyinstaller bundle, and if so, reverse its changes to the
    # LD_LIBRARY_PATH
    # See https://pyinstaller.org/en/stable/runtime-information.html
    env = None
    if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
        env = dict(os.environ)  # make a copy of the environment
        lp_key = "LD_LIBRARY_PATH"  # for GNU/Linux and *BSD.
        lp_orig = env.get(lp_key + "_ORIG")
        if lp_orig is not None:
            env[lp_key] = lp_orig  # restore the original, unmodified value
        else:
            # This happens when LD_LIBRARY_PATH was not set.
            # Remove the env var as a last resort:
            env.pop(lp_key, None)
    arg: List[str] = list(*args)
    return subprocess.run(arg, env=env, **kwargs)


def str_is_hex_colour(in_string, allow_0x: bool = False) -> bool:
    """Test that a string is a hexadecimal colour code

    Valid codes consist of a # symbol or '0x' followed by exactly six hexadecimal digits
    (0-9 or a-f, lower or upper case).

    Args:
        in_string (str): The string to test
        allow_0x (bool): Also allow '0x' style codes

    Returns:
        bool: is it a valid colour code?

    """
    exp = "^(#|0x)([A-Fa-f0-9]{6})$" if allow_0x else "^#([A-Fa-f0-9]{6})$"
    reg = re.compile(exp)
    if reg.match(in_string):
        return True
    return False


def is_uuid4(in_str: str) -> bool:
    """Check that a string is a UID4

    Args:
        in_str (str): The string to test
    Returns:
        bool: Is the string a valid uid4
    """
    try:
        vers = uuid.UUID(in_str).version
        return vers == 4
    except ValueError:
        return False


class DirectoryBasedLock:
    """A lock based on the creation and existence of a directory on the file system.

    The interface is almost the same as Python's standard :class:`multiprocessing.Lock`,
    except for some changes related to timeout behaviour:

    * There is a default timeout of 60 seconds when acquiring the lock (rather than
      the default ``None`` value, with corresponding infinite timeout, that is used by
      :class:`multiprocessing.Lock`). This is for compatibility with previous RELION
      locking timeout behaviour.
    * A timeout for use when entering a context manager can be set when the lock object
      is created. Note that this value is ignored if the ``acquire()`` method is called
      directly. If there is a timeout waiting to acquire the lock when entering a
      context manager, a :class:`TimeoutError` is raised.

    The principle of this lock is that directory creation is an atomic operation
    provided by the file system, even in (most, modern) networked file systems. If
    several processes try to create the same directory at the same time, only one will
    succeed and the rest will get an error. Therefore, we can use this as a locking
    primitive, acquiring the lock if we successfully create the directory and releasing
    it by deleting the directory afterwards.

    The lock directory name can be set if required. For compatibility with RELION, the
    default directory name is ".relion_lock".
    """

    def __init__(
        self, dirname: Union[str, "os.PathLike[str]"] = ".relion_lock", timeout=60.0
    ):
        super().__init__()
        self.dirname = str(dirname)
        self.timeout_for_context_manager = timeout

    def __enter__(self):
        """Acquire the lock as a context manager, typically in a ``with`` statement.

        Raises:
            TimeoutError: if the timeout expires while waiting to acquire the lock.
        """
        result = self.acquire(block=True, timeout=self.timeout_for_context_manager)
        if not result:
            raise TimeoutError(
                f"Timed out waiting for lock directory {self.dirname} to disappear"
            )
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Release the lock when the context manager exits."""
        self.release()
        return False

    def acquire(self, block=True, timeout=60.0):
        """Acquire a lock, blocking or non-blocking.

        With the ``block`` argument set to ``True`` (the default), the method call will
        block until the lock is in an unlocked state, then set it to locked and return
        ``True``.

        With the ``block`` argument set to ``False``, the method call does not block. If
        the lock is currently in a locked state, return ``False``; otherwise set the
        lock to a locked state and return ``True``.

        When invoked with a positive, floating-point value for timeout, block for at
        most the number of seconds specified by timeout as long as the lock can not be
        acquired. The default is 60.0 seconds; note that this is different from the
        default timeout in :meth:`multiprocessing.Lock.acquire`.

        Invocations with a negative value for timeout are equivalent to a timeout of
        zero. Invocations with a timeout value of ``None`` set the timeout period to
        infinite. The timeout argument has no practical implications if the block
        argument is set to ``False`` and is thus ignored.

        Returns:
            ``True`` if the lock has been acquired or ``False`` if the timeout period
            has elapsed.

        Raises:
            Various possible errors from :func:`os.mkdir` including
             :class:`FileNotFoundError` or :class:`PermissionError`.
        """
        waited = 0.0
        while True:
            try:
                os.mkdir(self.dirname)
                return True
            except FileExistsError:
                if not block or (timeout is not None and waited > timeout):
                    return False
                time.sleep(0.05)
                waited += 0.05

    def release(self):
        """Release the lock.

        This can be called from any thread, not only the thread which has acquired the
        lock.

        When the lock is locked, reset it to unlocked, and return. If any other threads
        are blocked waiting for the lock to become unlocked, allow exactly one of them
        to proceed.

        When invoked on an unlocked lock, a RuntimeError is raised.

        There is no return value.
        """
        try:
            os.rmdir(self.dirname)
        except FileNotFoundError as ex:
            raise RuntimeError(
                f"Trying to release but lock directory {self.dirname} is not present"
            ) from ex


def file_in_project(filename: str) -> bool:
    """Check that a file is part of the project

    Not done with os.path.abspath(file).startswith(project_dir) because this causes
    errors during testing
    """

    if any([filename.startswith(x) for x in ["/", "../", "~/"]]):
        return False
    return True


def convert_relative_filename(filename: str) -> str:
    """
    Convert a filename that is relative to the project to just its name

    IE:
    ../../my_dir/my_file.txt -> my_dir/my_file
    /my_dir/my_file.txt -> my_dir/my_file
    ~/my_dir/my_file.txt -> my_dir/my_file

    Args:
        filename:

    Returns:
        str: The part of the file path that is not relative to the project

    """

    if filename.startswith("/"):
        return filename.lstrip("/")

    if filename.startswith("~/"):
        filename = filename.lstrip("~/")
    while filename.startswith("../"):
        filename = filename.lstrip("../")
    return filename


def compare_nested_lists(
    a_list: list,
    e_list: list,
    tolerance: Optional[float] = None,
):
    """Compare two nested lists, with an optional tolerance for float values.

    If the items in the lists are all dicts, the two lists will simply be compared with
    ``==``.

    If a tolerance is given, list items will be converted to floats and then compared to
    check if their absolute difference is less than ``tolerance`` multiplied by the
    value from ``a_list``.

    Otherwise the individual items in each list will be compared with ``==``.

    Args:
        a_list (list): the actual list
        e_list (list): the expected list
        tolerance (float): The relative tolerance for float values, i.e. to compare as
            equivalent, the values must differ by less than this value multipled by the
            value from ``a_list``. Use ``None`` or 0 if the values must match exactly.

    Returns:
        bool: do they match (within tolerance if specified)
    """
    # Check if the list is contains only dictionaries and compare
    if all(isinstance(i, dict) for i in a_list):
        return bool(a_list == e_list)
    # If the list contains all nested lists flatten them
    if all(isinstance(i, list) for i in a_list) and all(
        isinstance(i, list) for i in e_list
    ):
        a_list = list(chain.from_iterable(a_list))
        e_list = list(chain.from_iterable(e_list))
    if len(a_list) != len(e_list):
        return False
    for actual_item, expected_item in zip(a_list, e_list):
        # If a tolerance is specified check if the values fall within
        if tolerance:
            try:
                act_f = float(actual_item)
                exp_f = float(expected_item)
                diff = abs(act_f - exp_f)
                tol = abs(act_f) * tolerance
                if diff > tol:
                    return False
            except ValueError:
                # Failed float conversion, fall back to simple equality comparison
                if actual_item != expected_item:
                    return False
        # Else direct compare the items
        elif actual_item != expected_item:
            return False
    return True


def get_file_size_mb(file: Union[str, Path]) -> float:
    """Get the size of a file in MB, rounded to 2 decimal places

    Args:
        file (str): The file to check
    """
    size_bytes = os.path.getsize(file)
    size_mb = size_bytes / 1048576
    return round(size_mb, 2)


def get_directory_size(directory_path: Union[str, Path]) -> int:
    """Get the size of a directory

    Args:
        directory_path (str): The dir to check

    Returns:
        str: The size in bytes
    """

    total_size = 0
    for path in Path(directory_path).rglob("*"):
        if path.is_file():
            total_size += path.stat().st_size
    return total_size


def make_pretty_size(size_b: int) -> str:
    """Get a file/dir size in human-readable form

    Args:
        size_b (int): The size in bytes
    Returns:
        str: The size in B, KB, MB, GB, or TB as appropriate
    """

    sizes = {1: "B", 2: "KB", 3: "MB", 4: "GB", 5: "TB", 6: "PB"}
    for n in sizes:
        test_size = 1024**n
        div = 1024 ** (n - 1)
        fin_size = f"{size_b / div:.2f}" if n > 2 else f"{int(size_b / div)}"
        if size_b <= test_size:
            return f"{fin_size} {sizes[n]}"
    return f"{fin_size} {sizes[n]}"


def get_directory_info(dir_path: Union[str, Path]) -> List[Tuple[str, str]]:
    rel_paths = []
    for dirpath, dirnames, filenames in os.walk(dir_path):
        rel_paths.extend([Path(dirpath) / filename for filename in filenames])
        rel_files = [x.relative_to(dir_path) for x in rel_paths]
    rel_files = sorted(rel_files, key=lambda x: (str(x).count("/"), x))
    output = []
    for f in rel_files:
        size = os.path.getsize(Path(dir_path) / f)
        output.append((str(f), make_pretty_size(size)))
    return output


def count_file_lines(filename: str) -> int:
    """Fast and efficient count of number of lines in a file

    Args:
        filename (str): Name of the file to count the lines in

    Returns:
        int: Number of lines
    """

    def _count_generator(reader):
        bytes_to_read = 1024 * 1024
        b = reader(bytes_to_read)
        while b:
            yield b
            b = reader(bytes_to_read)

    with open(filename, "rb") as fp:
        c_generator = _count_generator(fp.read)
        count = sum(buffer.count(b"\n") for buffer in c_generator)
        return count + 1


def increment_file_basenames(files: List[str], include_ext=True) -> Dict[str, str]:
    """Increment the base names of files if there are duplicates

    e.g. for Import/job001/myfile, Import/job002/myfile
        job001_myfile and job002_myfile are set as unique names in the output dict
    If the files have different extensions:
        e.g. Import/job001/myfile.ext1, Import/job002/myfile.ext2
            myfile.ext1 and myfile.ext2 are returned in the output dict
            If include_ext is False, job001_myfile and job002_myfile are set.

    Args:
        files (List[str]): The files to operate on
        include_ext (bool): Include file extension in unique basename check?
            True by default

    Returns:
        Dict[str, str]: The file name and its incremented basename

    """
    if include_ext:
        check = [Path(x).name for x in files]
    else:
        check = [Path(x).stem for x in files]
    renamed: Dict[str, str] = {}
    increment = 1

    for f in files:
        if include_ext:
            basename_check = Path(f).name
        else:
            basename_check = Path(f).stem
        if check.count(basename_check) > 1:
            job_n = decompose_pipeline_filename(f)[1]
            if job_n > 0:
                renamed[f] = f"job{job_n:03d}_" + str(basename_check)
            else:
                renamed[f] = f"{increment:03d}_" + str(basename_check)
                increment += 1
        else:
            renamed[f] = basename_check

    all_bns = list(renamed.values())
    if len(set(all_bns)) != len(all_bns):
        errs = []
        for name in renamed:
            if all_bns.count(renamed[name]) > 1:
                errs.append(f"{name} -> {renamed[name]}")
        raise ValueError(
            f"Renaming files failed because of duplicate file names: {errs}"
        )
    return renamed


def get_mrc_map_summary(
    mapfile: str, use_data: bool = False
) -> Dict[str, Union[Tuple, int, float]]:
    map_parameters: Dict[str, Union[Tuple, int, float]] = OrderedDict()
    if not use_data:
        mrc = mrcfile.open(mapfile, mode="r", header_only=True)
    else:
        mrc = mrcfile.mmap(mapfile, mode="r")
    map_parameters["Box size (pixels)"] = (
        mrc.header.nx.item(),
        mrc.header.ny.item(),
        mrc.header.nz.item(),
    )
    map_parameters["Mode"] = mrc.header.mode.item()
    map_parameters["Start offsets (pixels)"] = mrc.nstart.item()
    map_parameters["Box size (angstroms)"] = tuple(
        round(c, 3) for c in mrc.header.cella.item()
    )
    map_parameters["Cell angles"] = tuple(round(c, 3) for c in mrc.header.cellb.item())
    map_parameters["Axis order"] = (
        mrc.header.mapc.item(),
        mrc.header.mapr.item(),
        mrc.header.maps.item(),
    )
    map_parameters["Origin"] = tuple(round(o, 3) for o in mrc.header.origin.item())
    map_parameters["Voxel size (angstroms)"] = tuple(
        round(s, 5) for s in mrc.voxel_size.item()
    )
    # in a few cases, min,max,std info in the header doesnt match with data
    if use_data:
        map_parameters["Min"] = round(np.amin(mrc.data), 5)
        map_parameters["Max"] = round(np.amax(mrc.data), 5)
        map_parameters["Mean"] = round(np.mean(mrc.data), 5)
        map_parameters["Std"] = round(np.std(mrc.data), 5)
    else:
        map_parameters["Min"] = round(mrc.header.dmin.item(), 5)
        map_parameters["Max"] = round(mrc.header.dmax.item(), 5)
        map_parameters["Mean"] = round(mrc.header.dmean.item(), 5)
        map_parameters["Std"] = round(mrc.header.rms.item(), 5)
    mrc.close()
    return map_parameters


def get_mrc_map_header_string(mapfile: str) -> str:
    with StringIO(f"MRC header info for {mapfile}:\n") as stringf:
        with mrcfile.open(mapfile, mode="r", header_only=True) as mrc:
            mrc.print_header(print_file=stringf)
        return stringf.getvalue()


def make_atomic_model_summary(modelfile: str, summary_file: str):
    with open(summary_file, "w") as s:
        subprocess.call(["gemmi", "contents", modelfile], stdout=s)


def update_jobinfo_file(
    jobdir: str,
    action: Optional[str] = None,
    comment: Optional[str] = None,
    command_list: Optional[List[str]] = None,
) -> None:
    """Update the file in the jobdir that stores info about the job

    Args:
        jobdir: The job that contains the file to be updated
        action (str): what action was performed on the job, e.g. Run, Scheduled,
            Cleaned up
        comment (str): Comment to append to the job's comments list
        command_list (list): Commands that were run. Generally `None` if action
            was any other than Run or Scheduled
    """
    if all([command_list is None, comment is None, action is None]):
        return
    jinfo = os.path.join(jobdir, JOBINFO_FILE)
    jobinfo: JobInfoData = {
        "job_directory": jobdir,
        "comments": [comment] if comment is not None else [],
        "created": date_time_tag(),
        "history": [],
        "command_history": [],
    }
    if os.path.isfile(jinfo):
        try:
            with open(jinfo) as ji:
                jobinfo = json.load(ji)
            if comment is not None:
                jobinfo["comments"].append(comment)
        except json.decoder.JSONDecodeError:
            pass

    dtt = date_time_tag()

    if action is not None:
        jobinfo["history"].append(f"{dtt}: {action}")

    if command_list is not None:
        jobinfo["command_history"].append(f"{dtt}: {shlex.join(command_list)}")

    atomic_write_json(jobinfo, jinfo, indent=2)


def atomic_write_json(obj: object, filename: str, **json_dump_kwargs) -> None:
    """
    Save an object to JSON as an atomic operation.

    This ensures any processes reading the JSON file will always see a valid version
    (old or new) and not a half-written new file.

    This function writes the JSON to a temporary file first, then renames it after
    writing is complete and flushed to disk. The temporary file will always be removed
    even if the write-and-rename is unsuccessful.

    Pass additional keyword arguments for the json.dump() command as keyword arguments
    to this function.

    Note: using this function avoids race conditions caused by processes reading the
    file when it is partly-written by another process. It does not avoid possible
    problems caused by two processes writing to the same file at the same time. In that
    case, one version will be kept and the other will not, but there is no guarantee
    about which one will be kept. Avoiding that problem would require some kind of file
    locking, versioning or time stamping to identify conflicting or simultaneous edits.

    Args:
        obj: The object to save to JSON
        filename: The file name or path to save to
        json_dump_kwargs: Keyword arguments to be passed through to ``json.dump()``
    """
    # Ensure the directory for the target file exists
    target = Path(filename)
    target_dir = target.parent
    target_dir.mkdir(parents=True, exist_ok=True)
    # Create a unique name for the temporary file using the PID and current thread ID
    pid = os.getpid()
    thread_id = threading.current_thread().ident
    temp_file = target_dir / f"{target.name}.tmp{pid}-{thread_id}"
    try:
        # Create a temporary file, write the JSON to it and flush to disk
        with open(temp_file, "w") as temp_fileobj:
            # Write the JSON to the temporary file, flush it to disk and close it
            json.dump(obj, temp_fileobj, **json_dump_kwargs)
            temp_fileobj.flush()
            os.fsync(temp_fileobj.fileno())
        # Rename the temporary file to the target filename
        temp_file.replace(target)
    finally:
        # Ensure the temporary file is cleaned up even if the write-and-rename failed
        temp_file.unlink(missing_ok=True)
