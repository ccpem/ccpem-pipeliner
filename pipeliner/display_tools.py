#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import math
import traceback
from typing import List, Optional, Union, Dict, Tuple
from pathlib import Path

import mrcfile
import numpy as np
from PIL import Image
from gemmi import cif
from ccpem_utils.map.mrcfile_utils import bin_mrc_map

from pipeliner.mrc_image_tools import (
    threed_array_to_montage,
    threed_array_to_images,
    mrc_thumbnail,
    tiff_thumbnail,
    read_tiff,
)
from pipeliner.mrc_image_tools import (
    bin_array_to_size,
    mrcs_slices_montage,
    mrcs_slices_montage_base64,
)
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
    ResultsDisplayPending,
    ResultsDisplayGraph,
    ResultsDisplayMontage,
    ResultsDisplayHistogram,
    ResultsDisplayMapModel,
    RESULTS_DISPLAY_OBJECTS,
    get_next_resultsfile_name,
    ResultsDisplayImage,
)
from pipeliner.doppio_tools import get_doppio_filesize_limit, set_doppio_map_bin


def create_results_display_object(dobj_type: str, **kwargs) -> ResultsDisplayObject:
    """Safely create a results display object

    Returns a ResultsDisplayPending if there are any problems.  Give it the type of
    display object as the first argument followed by the kwargs for that specific
    type of ResultsDisplayObject

    Args:
        dobj_type (str): The type of DisplayObject to create
    """
    dt = RESULTS_DISPLAY_OBJECTS.get(dobj_type)
    if dt is None:
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Unknown DisplayObject type: {dobj_type}",
            start_collapsed=False,
        )

    try:
        return dt(**kwargs)
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating {dt.__name__}: {str(e)} \nTraceback: {tb}",
        )


def graph_from_starfile_cols(
    title: str,
    starfile: str,
    block: str,
    ycols: list,
    xcols: Optional[list] = None,
    xrange: Optional[list] = None,
    yrange: Optional[list] = None,
    data_series_labels: Optional[List[str]] = None,
    xlabel: str = "",
    ylabel: str = "",
    assoc_data: Optional[List[str]] = None,
    modes: Optional[List[str]] = None,
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayGraph, ResultsDisplayPending]:
    """Automatically generate a ResultsDisplayGraph object from a starfile

    Can use one or two columns and third column for labels if desired

    Args:
        title (str): The title of the final graph
        starfile (str): Path to the star file ot use
        block (str): The block to use in the starfile, use `None` for a starfile
            with only a single block
        ycols (list): Column label(s) from the star file to use for the y data series
        xcols (list): Column label(s) from the star file to use for the y data series
            if `None` a simple count from 1 will be used
        xlabel (str): Label for the x axis, if no x data are specified the label will
            'Count', if x data are specified and the xlabel is `None` the x axis label
            will be the name of the starfile column used
        xrange (list): Range for x vlaues to be displayed, full range if `None`
        yrange (list): Range for y vlaues to be displayed, full range if `None`
        data_series_labels (list): Names for the data series
        ylabel (str): Label for the y axis, if  `None` the y axis label will be the
            name of the starfile column used
        assoc_data (list): List of data file(s) associated with this graph
        modes (list): Controls the appearance of each data series, choose from 'lines',
            'markers' 'or lines+markers'
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayGraph`: A
        ResultsDisplayGraph object for the created graph
    """
    data_labels = [""] * len(ycols) if not data_series_labels else data_series_labels
    modes = modes if modes else ["lines"] * len(data_labels)
    if len(modes) != len(data_labels):
        raise ValueError("Number of modes does not match number of data series")
    try:
        star = cif.read_file(starfile).find_block(block)
        x_data_series = []
        y_data_series = []
        for ycol in ycols:
            try:
                ydata = [float(x[0]) for x in star.find([ycol])]
            except ValueError:
                ydata = [x[0] for x in star.find([ycol])]
            y_data_series.append(ydata)

        if xcols is None:
            for yds in y_data_series:
                x_data_series.append([float(x) for x in range(1, len(yds) + 1)])
        else:
            for xcol in xcols:
                try:
                    xdata = [float(x[0]) for x in star.find([xcol])]
                except ValueError:
                    xdata = [x[0] for x in star.find([xcol])]
                x_data_series.append(xdata)

        ylabel = ycols[0] if not ylabel else ylabel
        if not xlabel:
            xlabel = "Count" if xcols is None else xcols[0]
        assoc_data = [starfile] if assoc_data is None else assoc_data

        return create_results_display_object(
            "graph",
            title=title,
            xvalues=x_data_series,
            xaxis_label=xlabel,
            xrange=xrange,
            yrange=yrange,
            yvalues=y_data_series,
            yaxis_label=ylabel,
            data_series_labels=data_labels,
            associated_data=assoc_data,
            modes=modes,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating graph display object: {str(e)} \nTraceback: {tb}",
        )


def histogram_from_starfile_col(
    title: str,
    starfile: str,
    block: str,
    data_col: str,
    xlabel: str = "",
    ylabel: str = "Count",
    assoc_data: Optional[List[str]] = None,
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayHistogram, ResultsDisplayPending]:
    """Automatically generate a ResultsDisplayHistogram object from a starfile

    Args:
        title (str): The title of the final graph
        starfile (str): Path to the star file ot use
        block (str): The block to use in the starfile, use `None` for a starfile
            with only a single block
        data_col (str): Column label from the star file to use for the data series
        xlabel (str): Label for the x axis, if no x data are specified the label will
            'Count', if x data are specified and the xlabel is `None` the x axis label
            will be the name of the starfile column used
        ylabel (str): Label for the y axis, if  `None` the y axis label will be the
            name of the starfile column used
        assoc_data (list): List of data file(s) associated with this graph
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    """
    try:
        star = cif.read_file(starfile).find_block(block)

        try:
            alldata = [float(x[0]) for x in star.find([data_col])]
        except ValueError:
            alldata = [x[0] for x in star.find([data_col])]

        xlabel = data_col if not xlabel else xlabel

        assoc_data = [starfile] if not assoc_data else assoc_data

        return create_results_display_object(
            "histogram",
            title=title,
            data_to_bin=alldata,
            xlabel=xlabel,
            ylabel=ylabel,
            associated_data=assoc_data,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options

    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating histogram display object: {str(e)} \nTraceback:"
            f"{tb}",
        )


def get_ordered_classes_arrays(
    model_file: str,
    ncols: int,
    boxsize: int,
    output_dir: str,
    output_filename: str,
    parts_file: Optional[str] = None,
    title: str = "2D class averages",
    start_collapsed: bool = False,
    flag: str = "",
    base64_output: bool = False,
    optimiser_info: Optional[dict] = None,
) -> ResultsDisplayObject:
    """Return a 3D array of class averages from a Relion Class2D model file

    Args:
        model_file (str): Name of the model file
        ncols (int): number of columns desired in the file montage
        boxsize (int): Size of the class averages in the final montage
        output_dir (str): The output dir of the pipeliner job creating this object
        output_filename (str): The name for the output montage file
        parts_file (str): Path of the file containing the particles, for counting
        title (str): A title for the DisplayObject
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
        base64_output (bool): flag for a JSON file output
            with a list of objects holding base64 images and class IDs
        optimiser_info (dict): {name: str, type: str} Specify an optimiser data node
            to be included in the associated nodes of a gallery results display object.
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayMontage`: An object
        for the GUI to use to render the graph
    """
    try:
        star = cif.read_file(model_file)
        class_info = star.find_block("model_classes").find(
            ["_rlnReferenceImage", "_rlnClassDistribution"]
        )

        # count the particles if possible
        asoc_data_files = [model_file]
        if parts_file is not None:
            pblock = cif.read_file(parts_file).find_block("particles")
            nparts = int(str(pblock[0].loop).split()[1])
            asoc_data_files.append(parts_file)
        else:
            nparts = 0

        # get the info on each class
        classdict: Dict[int, Tuple[float, str]] = {}
        x, y, xs, ys, labels = 0, 0, [], [], []
        for i in class_info:
            clno = int(i[0].split("@")[0])
            cldist = float(i[1])
            xs.append(x)
            ys.append(y)
            if x != ncols - 1:
                x += 1
            else:
                x = 0
                y += 1
            pcount = f"; {int(nparts * cldist)} particles" if nparts > 0 else ""
            classdict[clno - 1] = (
                cldist,
                f"Class {clno}; {cldist * 100:.02f}%{pcount}",
            )

        # pad the last row if there were unused slots
        square = (ys[-1] + 1) * ncols
        dif = square - len(class_info)
        extralabels = []
        if dif != 0:
            xs.extend([xs[-1] + n for n in range(1, dif + 1)])
            ys.extend([ys[-1]] * dif)
            extralabels = [""] * dif
        # reverse the ys (Assuming 0,0 == bottom left)
        ys.reverse()

        # sort the classes by class distribution
        classlist = list(classdict)
        classlist.sort(key=lambda x: classdict[x][0], reverse=True)

        # write the labels in order
        for class_no in classlist:
            labels.append(classdict[class_no][1])
        labels.extend(extralabels)

        stackfile = class_info[0][0].split("@")[1]

        with mrcfile.mmap(stackfile) as stack:
            reordered = np.zeros(stack.data.shape)
            for n, img in enumerate(classlist):
                reordered[n, :, :] = stack.data[img, :, :]

        # create the montage image
        thumbs_dir = os.path.join(output_dir, "Thumbnails")
        os.makedirs(thumbs_dir, exist_ok=True)
        montage_name = os.path.join(thumbs_dir, output_filename)
        threed_array_to_montage(reordered, ncols, boxsize, montage_name)

        if base64_output:
            asoc_nodes: List[Dict[str, str]] = []

            if optimiser_info:
                asoc_nodes.append(optimiser_info)

            images = threed_array_to_images(
                raw=reordered,
                ncols=ncols,
                boxsize=boxsize,
                labels=[str(class_id) for class_id in classlist],
                outname=thumbs_dir,
            )
            return create_results_display_object(
                "gallery",
                title=title,
                labels=labels,
                associated_nodes=asoc_nodes,
                associated_data=asoc_data_files,
                start_collapsed=start_collapsed,
                flag=flag,
                images=images,
            )

        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            associated_data=asoc_data_files,
            img=montage_name,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def make_map_model_thumb_and_display(
    outputdir: str,
    maps: Optional[List[str]] = None,
    maps_opacity: Optional[List[float]] = None,
    maps_colours: Optional[List[str]] = None,
    models: Optional[List[str]] = None,
    models_colours: Optional[List[str]] = None,
    title: Optional[str] = None,
    maps_data: str = "",
    models_data: str = "",
    assoc_data: Optional[List] = None,
    flag: str = "",
) -> Union[ResultsDisplayMapModel, ResultsDisplayPending]:
    """Make a display object for an atomic model overlaid over a map

    Makes a binned map and a ResultsDisplayMapModel display object

    Args:
        outputdir (str): Name of the job's output directory
        maps (list): List of map files to use
        models (list): List of model files to use
        maps_opacity (list): List of opacity for the maps, from 0-1
            if `None` 0.5 is used for all
        maps_colours (list): Colors for the maps of specific ones are desired,
            otherwise mol* will assign them
        title (str): The title for the ResultsDisplayMapModel object, if `None`
            the name of the map and model will be used
        maps_data (str): Any additional data to be included about the map
        models_data (str): Any additional data to be included about the map
        models_colours (list): Colors for the models of specific ones are desired,
            otherwise mol* will assign them
        assoc_data (list): List of associated data, if left as `None`
             then just uses the file itself
        flag (str): If the results are considered scientifically dubious explain in this
            string

    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayMapModel`: The
        DisplayObject for the map and model
    """
    # make the thumbnails dir if necessary
    try:
        maps = maps if maps is not None else []
        models = models if models is not None else []
        maps_opacity = maps_opacity if maps_opacity is not None else []
        maps_colours = maps_colours if maps_colours is not None else []
        models_colours = models_colours if models_colours is not None else []
        outdir = os.path.join(outputdir, "Thumbnails")
        if maps:
            os.makedirs(outdir, exist_ok=True)

        # check the file sizes
        mapbins = {1.0: 0.0}
        file_size_limit = get_doppio_filesize_limit()
        for mapfile in maps:
            bintest, new_px = set_doppio_map_bin(mapfile, file_size_limit)
            mapbins[bintest] = new_px
        final_bin, newpx = min(mapbins), mapbins[min(mapbins)]

        # prepare the binned maps if necessary
        min_dim = 0
        outmaps = []
        for mapfile in maps:
            if final_bin != 1.0:
                with mrcfile.open(mapfile, header_only=True) as mf:
                    new_box = (
                        int(mf.header.nx * final_bin),
                        int(mf.header.ny * final_bin),
                        int(mf.header.nz * final_bin),
                    )
                    min_dim = max(
                        min_dim, min(mf.header.nx, mf.header.ny, mf.header.nz)
                    )
                outmap = os.path.join(outdir, os.path.basename(mapfile))
                bin_mrc_map(mapfile, new_dim=new_box, map_output=outmap)
                outmaps.append(outmap)
            else:
                outmaps.append(mapfile)
        if final_bin != 1.0:
            flag = flag + (
                f" Maps binned to {int(final_bin * 100)}% size for display. "
                f"Maximum resolution of display is {2 * newpx} Å."
            )
            if min_dim > 500:
                flag += (
                    " For large maps with all dimensions > 500, the downsampling "
                    "uses strides (less accurate), the step size depends on the "
                    "requested map size."
                )

        # create the results display object
        ad = maps + models if assoc_data is None else assoc_data
        # figure out the title
        if len(maps) == 0 and len(models) == 1:
            obj_title = f"Model: {models[0]}"
        elif len(maps) == 1 and len(models) == 0:
            obj_title = f"Map: {maps[0]}"
        elif len(maps) == 1 and len(models) == 1:
            obj_title = f"Overlaid map: {maps[0]} model: {models[0]}"
        else:
            tmap = "map" if len(maps) >= 1 else ""
            tmap = tmap + "s" if len(maps) > 1 else tmap
            sp = "/" if len(maps) > 0 and len(models) > 0 else ""
            tmod = "model" if len(models) >= 1 else ""
            tmod = tmod + "s" if len(models) > 1 else tmod
            obj_title = f"Overlaid {tmap}{sp}{tmod}"
        title = title if title is not None else obj_title

        dispobj = create_results_display_object(
            "mapmodel",
            title="3D viewer: " + title if not title.startswith("3D viewer") else title,
            maps=outmaps,
            models=models,
            maps_opacity=[] if maps_opacity is None else maps_opacity,
            maps_colours=maps_colours,
            maps_data=", ".join(maps) if maps_data == "" else maps_data,
            models_data=", ".join(models) if models_data == "" else models_data,
            models_colours=models_colours,
            associated_data=ad,
            flag=flag,
        )
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating map-model display object: {str(e)} "
            f"\nTraceback: {tb}",
        )
    return dispobj  # type: ignore[return-value]  # limited return options


def mini_montage_from_many_files(
    filelist: List[str],
    outputdir: str,
    nimg: int = 5,
    montagesize: int = 640,
    title: str = "",
    ncols: int = 5,
    associated_data: Optional[List[str]] = None,
    labels: Optional[List[str]] = None,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:
    """Make a mini montage from a list of images

    Merge and flatten image stacks

    Args:
        filelist (list): A list of the files to use
        outputdir (str): The output dir of the pipeliner job
        nimg (int): Number of images to use in the montage
        montagesize (int): Desired size of the final montage image
        title (str): Title for the ResultsDisplay object that will be output
        ncols (int): Number of columns to make in the montage
        associated_data (list): Data files associated with these images, if `None`
            then all of the selected images
        labels (list): The labels for the items in the montage
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message

    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is used
    """
    # get the images
    try:
        if nimg < 1 or nimg > len(filelist):
            images = filelist
            nimg = len(images)
        else:
            images = filelist[:nimg]
        nimg = len(images) if len(images) < nimg else nimg
        ncols = nimg if ncols > nimg else ncols
        imgsize = montagesize // ncols
        nrows = math.ceil(nimg / ncols)

        img_arrays = []
        for img in images:
            ext = img.split(".")[-1]
            if ext in ["mrc", "mrcs", "ctf"]:
                img_arrays.append(mrc_thumbnail(img, imgsize))
            elif ext in ["tif", "tiff", "eer"]:
                img_arrays.append(tiff_thumbnail(img, imgsize))
            else:
                raise ValueError("Invalid image type")
        zstack = np.stack(img_arrays, axis=0)
        thumbs_dir = os.path.join(outputdir, "Thumbnails")
        os.makedirs(thumbs_dir, exist_ok=True)
        outfile = get_next_resultsfile_name(thumbs_dir, "montage_f*.png")
        threed_array_to_montage(zstack, ncols, imgsize, outfile, cmap, inpnormed=True)

        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)

        labels = images if not labels else labels
        associated_data = images if not associated_data else associated_data
        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            img=outfile,
            associated_data=associated_data,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def mini_montage_from_stack(
    stack_file: str,
    outputdir: str,
    nimg: int = 40,
    ncols: int = 10,
    montagesize: int = 640,
    title: str = "",
    labels: Optional[List[Union[int, str]]] = None,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:
    """Make a montage from a mrcs or tiff file

    Args:
        stack_file (str): The path to the stack_file
        outputdir (str): The output dir of the pipeliner job
        nimg (int): Number of images to use in the montage, if < 1 uses all of them
        ncols (int): Number of columns to use
        montagesize (int): Desired size of the final montage image
        title (str): Title for the ResultsDisplay object that will be output
        labels (list): Labels for the images
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is used
    """
    try:
        # get the images
        imgsize = montagesize // ncols

        ext = os.path.basename(stack_file).split(".")[-1]
        if ext in ["mrcs", "mrc"]:
            with mrcfile.open(stack_file) as mrcs:
                imgstack = mrcs.data
        elif ext in ["tiff", "tif"]:
            imgstack = read_tiff(stack_file)
        else:
            return create_results_display_object(
                "text",
                title="Error creating results display",
                display_data=f"Illegal file type for mini_montage_from_stack(): .{ext}",
                associated_data=[stack_file],
                start_collapsed=start_collapsed,
                flag=flag,
            )  # type: ignore[return-value]  # limited return options
        img_count = imgstack.shape[0]

        # check there are enough images and labels and update accordingly
        nimg = img_count if nimg < 1 else nimg
        nimg = img_count if nimg > img_count else nimg
        ncols = nimg if ncols > nimg else ncols
        labels = (
            [str(x) for x in range(nimg)] if not labels else [str(x) for x in labels]
        )
        stackslice = imgstack[:nimg, :, :]
        # make the montage image
        thumbdir = os.path.join(outputdir, "Thumbnails")
        os.makedirs(thumbdir, exist_ok=True)
        outfile = get_next_resultsfile_name(thumbdir, "montage_m*.png")
        threed_array_to_montage(stackslice, ncols, imgsize, outfile, cmap)

        nrows = math.ceil(nimg / ncols)
        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)
        if len(labels) != len(ys):
            labels.extend([""] * (len(ys) - len(labels)))

        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            img=outfile,
            associated_data=[stack_file],
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def mini_montage_from_starfile(
    starfile: str,
    block: str,
    column: str,
    outputdir: str,
    title: str = "",
    nimg: int = 20,
    montagesize: int = 640,
    ncols: int = 10,
    labels: Optional[List[str]] = None,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:
    """Make a montage from a list of images in a starfile column

    Merge and flatten image stacks if they are encountered.

    Args:
        starfile (str): The starfile to use
        block (str): The name of the block with the images
        column (str): The name of the column that has the images
        outputdir (str): The output dir of the pipeliner job
        title (str): The title for the object, automatically generated if ""
        nimg (int): Number of images to use in the montage, uses all if < 1
        montagesize (int): Desired size of the final montage image
        ncols (int): number of columns to use
        labels (list): Labels for the images in the montage, in order
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is encountered
    """
    try:
        if block in ("", None):
            stardat = cif.read_file(starfile).sole_block()
        else:
            stardat = cif.read_file(starfile).find_block(block)
        imgdata = stardat.find_values(column)
        total_images = len(imgdata)
        # check the counts of images and columns
        nimg = total_images if nimg < 1 else min(nimg, total_images)

        if nimg == 0:
            message = (
                f"No particles were found in file: {starfile},"
                f" block: {block}, column: {column}"
            )
            return ResultsDisplayPending(
                message="Error creating montage results display",
                reason=message,
            )

        # Gemmi columns don't support slicing, so we have to access images one by one
        images = [imgdata[index] for index in range(nimg)]

        ncols = min(nimg, ncols)

        # if all the image list is straight images just make the object
        if all([os.path.isfile(x) for x in images]):
            return mini_montage_from_many_files(
                filelist=images,
                outputdir=outputdir,
                nimg=nimg,
                ncols=ncols,
                montagesize=montagesize,
                title=f"{starfile}; {nimg}/{total_images} images",
                associated_data=[starfile],
                labels=labels,
                start_collapsed=start_collapsed,
                flag=flag,
            )

        # if images were not found and they aren't slices the montage can't be made
        if not any(["@" in x for x in images]):
            missing = ", ".join([x for x in images if not Path(x).is_file()])
            return ResultsDisplayPending(
                message="Error creating montage results display",
                reason=f"The following image files were not found: {missing}",
            )

        # if the images are slices from stacks, they need to be handled separately
        # remember they count from 1 not 0
        if not all(["@" in x for x in images]):
            message = (
                "Unable to parse images in star file. Possibly a mix of stack slices"
                " and single images, or files are missing"
            )
            return ResultsDisplayPending(
                message="Error creating montage results display",
                reason=message,
            )

        arrays = []
        for img in images:
            imsplit = img.split("@")
            n = int(imsplit[0])
            image = imsplit[1]
            with mrcfile.mmap(image) as mrc:
                arrays.append(mrc.data[n - 1, :, :])

        zstack = np.stack(arrays, axis=0)
        thumbsdir = os.path.join(outputdir, "Thumbnails")
        os.makedirs(thumbsdir, exist_ok=True)
        output_image = get_next_resultsfile_name(thumbsdir, "montage_s*.png")
        boxsize = montagesize // ncols
        montage = threed_array_to_montage(
            zstack, ncols=ncols, boxsize=boxsize, outname=output_image, cmap=cmap
        )

        outimg = Image.fromarray(montage)
        outimg = outimg.convert("RGB")
        outimg.save(output_image)

        nrows = math.ceil(nimg / ncols)
        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)
        if len(images) != len(ys):
            images.extend([""] * (len(ys) - len(images)))
        title = title if title != "" else f"{starfile}; {nimg}/{total_images} images"
        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=images if not labels else labels,
            img=output_image,
            associated_data=[starfile],
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore[return-value]  # limited return options

    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating montage results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def make_particle_coords_thumb(
    in_mrc,
    in_coords,
    out_dir,
    thumb_size=640,
    pad=5,
    start_collapsed=False,
    title: str = "Example picked particles",
    flag: str = "",
    markers: bool = False,
) -> Union[ResultsDisplayImage, ResultsDisplayPending]:
    """Create a thumbnail of picked particle coords on their micrograph

    Because the extraction box size is not known boxes will be  a % of the
    total image size.

    Args:
        in_mrc (str): Path to the merged micrograph mrc file
        in_coords (str): Path to the .star coordinates file
        out_dir (str): Name of the output directory
        thumb_size (int): Size of the x dimension of the final thumbnail image
        pad (int): Thickness of the particle box borders before binning in px
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        title (str): What title to use for the displayobj created
        flag (str): If this display object contains scientificlly dubious results
            display this message
        markers (bool): Instead of making boxes make markers
    """

    # make the micrographs thumbnail
    with mrcfile.open(in_mrc, header_only=True) as mrcin:
        thumb_size = min(thumb_size, int(mrcin.header.nx))
    mic_thumb = mrc_thumbnail(in_mrc, thumb_size)

    # read the particles file
    coordsext = in_coords.split(".")[-1]
    # assumes relion style coordinates files
    if coordsext == "star":
        cd_block = cif.read_file(in_coords).sole_block()
        cd = list(cd_block.find("_rln", ["CoordinateX", "CoordinateY"]))
    elif coordsext == "box":
        with open(in_coords) as ic:
            cd = [x.split() for x in ic.readlines()]
    else:
        raise ValueError("Invalid coordinate file format use .box or .star")

    coords = []
    for i in cd:
        coords.append([float(i[0]), float(i[1])])

    with mrcfile.open(in_mrc, header_only=True) as mrc:
        assert mrc.header.nz == 1, "MRC image must be 2D"
        mic_shape = int(mrc.header.ny), int(mrc.header.nx)
    coords_array = np.zeros(mic_shape)

    def square_corners(
        c_coords, size, pct, ppad, do_markers: bool = markers
    ) -> Tuple[int, int, int, int]:
        """Get the corners for the particle boxes

        Args:
            c_coords (list): x and y coordinates
            size (int): desired size of the thumbnail image
            pct (float): percentage of the thumbnail width to make the
                box (because particle boxsize is not known)
            ppad (int): Thickness of the box borders in pixels
            do_markers (bool): Add markers to the graph?

        """
        boxshift = 0.5 * pct * size if not do_markers else 1
        x, y = c_coords
        y_top = max(int(math.ceil(y - boxshift)), ppad)
        y_bot = min(int(math.ceil(y + boxshift)), mic_shape[0] - ppad)
        x_left = max(int(math.ceil(x - boxshift)), ppad)
        x_right = min(int(math.ceil(x + boxshift)), mic_shape[1] - ppad)

        return y_top, y_bot, x_left, x_right

    for cpair in coords:
        # get the box coordinates
        ytop, ybot, xleft, xright = square_corners(cpair, thumb_size, 0.2, pad)
        xlength = xright - xleft if xright - xleft > 0 else 1
        ylength = ybot - ytop if ybot - ytop > 0 else 1
        # draw the boxes array
        coords_array[ytop:ybot, xleft - pad : xleft] = np.full([ylength, pad], 100)
        coords_array[ytop:ybot, xright : xright + pad] = np.full([ylength, pad], 100)
        coords_array[ytop - pad : ytop, xleft:xright] = np.full([pad, xlength], 100)
        coords_array[ybot : ybot + pad, xleft:xright] = np.full([pad, xlength], 100)

    # make an image of the coords
    binned_coords = bin_array_to_size(coords_array, thumb_size)
    binned_coords[binned_coords > 0] = 100
    box_img = Image.fromarray(binned_coords).convert("RGBA")
    boxes_data = box_img.getdata()
    transboxes = []
    for pix in boxes_data:
        if pix[0] == 0 and pix[1] == 0 and pix[2] == 0:
            transboxes.append((255, 255, 255, 0))
        else:
            transboxes.append((255, 0, 0, 255))
    # Ignoring the type error on the next line because the Pillow type stubs require
    # Sequence[int] but we have Sequence[Tuple[int, int, int, int]] (which is fine for
    # an RGBA image)
    box_img.putdata(transboxes)  # type: ignore[arg-type]

    # make image of the micrograph and merge the two
    mic_img = Image.fromarray(mic_thumb).convert("RGB")
    mic_img.paste(box_img, (0, 0), box_img)
    thumbdir = os.path.join(out_dir, "Thumbnails")
    os.makedirs(thumbdir, exist_ok=True)
    out_img = get_next_resultsfile_name(thumbdir, "picked_coords*.png")

    mic_img.save(out_img)
    description = f"{in_mrc}: {len(coords)} particles"
    # make the ResultsDisplayObject
    dispobj = create_results_display_object(
        "image",
        title=title,
        image_path=out_img,
        image_desc=description,
        associated_data=[in_mrc, in_coords],
        start_collapsed=start_collapsed,
        flag=flag,
    )
    return dispobj  # type: ignore[return-value]  # limited return options


def make_mrcs_central_slices_montage(
    in_files: Dict[str, str],
    output_dir: str,
    cmap: str = "",
    base64_output: bool = False,
    optimiser_info: Optional[dict] = None,
) -> ResultsDisplayObject:
    """Make a montage of x,y,z central slices of maps

    Args:
        in_files (Dict[str, str]): {file name: label if different from file name}
        output_dir (str): Where to make the Thumbnails dir (if necessary) and put the
            montage image
        cmap (str): What colormap to use, if any
        optimiser_info (dict): {name: str, type: str} Specify an optimiser data node
            to be included in the associated nodes of a gallery results display object.
        Returns:
        ResultsDisplayMontage: The montage ResultsDisplayObject
    """
    thumbsdir = Path(output_dir) / "Thumbnails"
    thumbsdir.mkdir(exist_ok=True)
    outfile = get_next_resultsfile_name(str(thumbsdir), "slices_montage_*.png")
    try:
        if base64_output:
            montage_labels, base64_file = mrcs_slices_montage_base64(
                in_files, outname=outfile, cmap=cmap
            )
        else:
            montage_labels = mrcs_slices_montage(in_files, outname=outfile, cmap=cmap)

        nrows = int(len(montage_labels) / 3)
        xvals = [0, 1, 2] * nrows
        yvals = []
        for x in range(nrows):
            yvals.extend([x, x, x])

        lab = list(in_files.items())
        title_add = ""
        if len(lab) == 1:
            lval = lab[0][0] if lab[0][1] == "" else lab[0][1]
            title_add = f": {lval}"

        asoc_data = list(in_files.keys())
        asoc_data.sort()

        if base64_output:
            asoc_nodes: List[Dict[str, str]] = []
            if optimiser_info:
                asoc_nodes.append(optimiser_info)

            rdo = create_results_display_object(
                "gallery",
                title=f"Map slices{title_add}",
                associated_nodes=asoc_nodes,
                associated_data=asoc_data,
                labels=montage_labels,
                images=base64_file,
            )
        else:
            rdo = create_results_display_object(
                "montage",
                xvalues=xvals,
                yvalues=yvals,
                img=outfile,
                title=f"Map slices{title_add}",
                associated_data=asoc_data,
                labels=montage_labels,
            )
    except Exception as e:
        rdo = create_results_display_object(
            "pending",
            message=str(e),
            reason=str(traceback.format_exc()),
        )

    assert isinstance(rdo, (ResultsDisplayObject))
    return rdo


def make_maps_slice_montage_and_3d_display(
    in_maps: Dict[str, str],
    output_dir: str,
    combine_montages: bool = True,
    cmap: str = "",
    base64_output: bool = False,
    optimiser_info: Optional[dict] = None,
) -> List[ResultsDisplayObject]:
    """Make a set of display objects for 3D maps

    Returns separate 3D viewer display objects for each map and either a combined
    slices montage or a slices montage for each.

    Args:
        in_maps (dict): {input file: label}. If the label is "", the filename
            will be used
        output_dir (str): The job's output dir where the thumbnails dir will be
            created if necessary
        combine_montages (bool): Should a single montage be made with slices for
            all maps, otherwise a separate montage is made for each
        cmap (str): what color map to use for the montage, if any.
        base64_output (bool): Whether to create base64 thumbnails and gallery
            display object.
        optimiser_info (dict): {name: str, type: str} Specify an optimiser data node
            to be included in the associated nodes of a gallery results display object.
    Returns:
        List: The display objects montage and then the 3D viewers if combine_montages
            is ``False``, otherwise the montage followed by the 3D viewer for each map
            in the order they were given.
    """

    dispobjs: List[ResultsDisplayObject] = []
    if combine_montages:
        dispobjs.append(
            make_mrcs_central_slices_montage(
                in_files=in_maps,
                output_dir=output_dir,
                cmap=cmap,
                base64_output=base64_output,
                optimiser_info=optimiser_info,
            )
        )
    for map_name, map_label in in_maps.items():
        # If not combining montages, add a separate montage and 3D viewer for each map
        # in turn
        if not combine_montages:
            dispobjs.append(
                make_mrcs_central_slices_montage(
                    in_files={map_name: map_label},
                    output_dir=output_dir,
                    cmap=cmap,
                )
            )
        dispobjs.append(
            make_map_model_thumb_and_display(
                outputdir=output_dir,
                maps=[map_name],
                title=map_label if map_label else map_name,
            ),
        )

    return dispobjs
