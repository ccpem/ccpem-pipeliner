#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import mrcfile
import os
import numpy as np
import base64
import json
from io import BytesIO
from PIL import Image
from matplotlib.pyplot import get_cmap
from typing import Any, Iterable, List, Optional, Tuple, Dict, Union, Sequence
import logging

logger = logging.getLogger(__name__)


def read_tiff(intiff: str, new_size: Optional[int] = None) -> np.ndarray:
    """Read multi-image tiff file, e.g. tiff stacks
    Args:
        intiff (str): The name of the file
        new_size (int): Resize the image to this size (assumes image is square)

    Returns:
        array (array): Numpy array
    """
    img = Image.open(intiff)
    images = []
    for i in range(img.n_frames):
        img.seek(i)
        if new_size:
            # resize width to new and keep aspect ratio
            img_resized = img.resize(
                (new_size, int(round((new_size / img.width) * img.height)))
            )
            images.append(np.array(img_resized))
        else:
            images.append(np.array(img))
    img.close()
    return np.array(images)


def get_mrc_dims(inmrc: str) -> Tuple[int, int, int]:
    """Get the shape of an mrc file

    Args:
        inmrc (str): The name of the file
    Returns:
        tuple: (int,int,int) x,y,z size in pixels

    """
    with mrcfile.open(inmrc, header_only=True) as mrc:
        return int(mrc.header.nx), int(mrc.header.ny), int(mrc.header.nz)


def get_tiff_dims(intiff: str) -> Tuple[int, int, int]:
    img = read_tiff(intiff)  # read_tiff always returns stack array?
    n = 1  # color channels
    if len(img.shape) == 4 and img.shape[-1] in [3, 4]:  # stack of RGB/RGBA images
        z, y, x, n = img.shape
    elif len(img.shape) == 3:
        if img.shape[-1] in [3, 4]:  # single RGB image
            y, x, n = img.shape
        else:
            z, y, x = img.shape  # stack of single channel/ grey images
    else:
        z = 1
        y, x = img.shape
    if n > 1:
        logger.info("Input tiff image {} is multi-channel (RGB/RGBA)".format(intiff))
    return x, y, z


def get_tiff_stats(intiff: str) -> list:
    final_stats: list = []
    img = read_tiff(intiff)  # read_tiff always returns stack array?
    n = 1  # color channels
    stack = True
    if len(img.shape) == 3:
        if img.shape[-1] in [3, 4]:  # single RGB image
            y, x, n = img.shape
        else:
            z, y, x = img.shape
    elif len(img.shape) == 4 and img.shape[-1] in [3, 4]:  # stack of RGB/RGBA images
        z, y, x, n = img.shape
    else:
        z = 1
        y, x = img.shape
        stack = False
    if n > 1:
        logger.info("Input tiff image {} is multi-channel (RGB/RGBA)".format(intiff))

    dtype = img.dtype
    modes = {
        "int8": "8-bit signed integer (range -128 to 127)",
        "uint8": "8-bit unsigned integer (range 0 to 255)",
        "int16": "16-bit signed integer",
        "int32": "32-bit signed real",
        "uint16": "16-bit unsigned integer",
        "float16": "16-bit float (IEEE754)",
    }
    typedesc = modes.get(str(dtype), "Unknown data type")
    dtype_desc = f"{typedesc}: {dtype}"

    if not stack:
        final_stats.append((x, y, z))
        final_stats.append(np.min(img))
        final_stats.append(np.max(img))
        final_stats.append(np.mean(img))
        final_stats.append(np.std(img))
        final_stats.append(None)
        final_stats.append(dtype_desc)

    elif stack:
        for frame in range(z):
            sub_stats: list = []
            sub = img[frame, :, :]
            sub_stats.append((sub.shape[1], sub.shape[0]))
            sub_stats.append(np.min(sub))
            sub_stats.append(np.max(sub))
            sub_stats.append(np.mean(sub))
            sub_stats.append(np.std(sub))
            sub_stats.append(None)
            sub_stats.append(dtype_desc)
            final_stats.append(sub_stats)

    return final_stats


def get_mrc_stats(inmrc: str, stack: bool = False) -> list:
    """Get statistics on a mrc file


    Args:
        inmrc (str): The name of the file
        stack (bool): Calculate stats for each individual
            frame rather than the image as a whole
    Returns:
        tuple: ((imgsize x, y, z), min, max, mean, std,
            (voxelsize x, y, z), 'mode: mode description)') or a
            tuple of with a tuple for each image in stack mode
    """
    dims = get_mrc_dims(inmrc)
    final_stats: list = []

    modes = {
        0: ("8-bit signed integer (range -128 to 127)", np.int8),
        1: ("16-bit signed integer", np.int16),
        2: ("32-bit signed real", np.float32),
        3: ("transform : complex 16-bit integers", np.int16),
        4: ("transform : complex 32-bit reals", np.float32),
        6: ("16-bit unsigned integer", np.int16),
        12: ("16-bit float (IEEE754)", np.float16),
    }

    with mrcfile.mmap(inmrc) as mrc:
        data = mrc.data
        mode = int(mrc.header["mode"])
        modes_list = modes.get(mode)
        if modes_list is None:
            raise ValueError(f"Unknown mode (data type): {mode}")
        mode_desc = modes_list[0]
        modedisp = f"{mode}: {mode_desc}"
        conv = modes[mode][1]
        apix = (
            conv(mrc.voxel_size["x"]),
            conv(mrc.voxel_size["y"]),
            conv(mrc.voxel_size["z"]),
        )

    if not stack:
        final_stats.append(dims)
        final_stats.append(conv(np.min(data)))
        final_stats.append(conv(np.max(data)))
        final_stats.append(conv(np.mean(data)))
        final_stats.append(conv(np.std(data)))
        final_stats.append(apix)
        final_stats.append(modedisp)

    elif stack:
        for frame in range(dims[2]):
            sub_stats: list = []
            sub = data[frame, :, :]
            sub_stats.append((sub.shape[1], sub.shape[0]))
            sub_stats.append(conv(np.min(sub)))
            sub_stats.append(conv(np.max(sub)))
            sub_stats.append(conv(np.mean(sub)))
            sub_stats.append(conv(np.std(sub)))
            sub_stats.append((apix[0], apix[1]))
            sub_stats.append(modedisp)
            final_stats.append(sub_stats)

    return final_stats


def stack_mrcs(input_files: List[str], out: str = "stacked.mrcs") -> Tuple[str, int]:
    """Create a stack from mrc files

    Args:
        input_files (list): The files to be stacked, in order
        out (str): The name to be given to the output file

    Returns:
        tuple: ('output file name', number of files stacked)
    """
    # make sure file name ends with .mrcs and not .mrc
    if not out.endswith("s"):
        out += "s"
    if not out.endswith(".mrcs"):
        out += ".mrcs"

    # make the output directory if necessary
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "":
        os.makedirs(out_dir, exist_ok=True)
    imgs, shapes, apixs, errors = [], [], [], []
    for in_file in input_files:
        with mrcfile.open(in_file) as mrc:
            imgs.append(mrc.data)
            shapes.append(mrc.data.shape)
            apixs.append(mrc.voxel_size)
    if not all(x == shapes[0] for x in shapes):
        errors.append("All images must have the same dimensions.")
    if not all(x == apixs[0] for x in apixs):
        errors.append("All images must have the same voxel size.")
    if len(errors) > 0:
        raise ValueError("".join(errors))

    with mrcfile.new(out, np.stack(imgs), overwrite=True) as newmrc:
        newmrc.voxel_size = apixs[0]

    return out, len(input_files)


def unstack_mrcs(
    input_file: str, imgs: Optional[Iterable[int]] = None, out: str = "unstack"
) -> Tuple[str, int]:
    """Create individual images from an mrc stack

    Args:
        input_file (str): Name of the input file
        imgs (list): indexes of the images to unstack, will do all if None
            use proper numbering starting with 0
        out (str): base name of the output, if a directory is included it will be
            created if necessary

    Returns:
        tuple: ('format of the output', number of files created)
    """
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "":
        os.makedirs(out_dir, exist_ok=True)

    with mrcfile.mmap(input_file) as stack:
        if imgs is None:
            imgs = range(stack.data.shape[0])
        if max(imgs) >= stack.data.shape[0]:
            raise ValueError("Requested frames not in the stack")
        zeros = len(str(len(list(imgs))))
        apix = stack.voxel_size
        for n, img in enumerate(imgs):
            the_slice = stack.data[img, :, :]
            sp = "" if out[-1] == "/" else "_"
            name = f"{out}{sp}{n:0{zeros}d}.mrc"
            with mrcfile.new(name, overwrite=True) as nf:
                nf.set_data(the_slice)
                nf.voxel_size = apix

    return f"{out}{sp}{'%' * zeros}.mrc", len(list(imgs))


def substack_mrcs(
    input_stack: str, imgs: Optional[Iterable[Any]], out: str = "substack.mrcs"
) -> Tuple[str, int]:
    """Create a substack from a mrc stack

    Args:
        input_stack (str): Name of the input file
        imgs (list): Which images to unstack, will do all if None
        out (str): Name of the output file to be written

    Returns:
        tuple: ('name of output stack', number of frames new stack)
    """
    # make sure file name ends with .mrcs and not .mrc
    if not out.endswith("s"):
        out += "s"
    if not out.endswith(".mrcs"):
        out += ".mrcs"

    # make the output directory if necessary
    out_dir = os.path.dirname(out)
    out = out[:-4] if out[-4:] == ".mrc" else out
    if out_dir != "":
        os.makedirs(out_dir, exist_ok=True)
    frames = []
    with mrcfile.mmap(input_stack) as instack:
        if imgs is None:
            imgs = range(instack.data.shape[0])
        if max(imgs) >= instack.data.shape[0]:
            raise ValueError("Requested frames not in the stack")

        apix = instack.voxel_size
        for fr in imgs:
            frames.append(instack.data[fr, :, :])

    with mrcfile.new(out, np.stack(frames), overwrite=True) as newmrc:
        newmrc.voxel_size = apix
    return out, len(list(imgs))


def remove_outliers(in_array: np.ndarray, fill_mean: bool = False) -> np.ndarray:
    """Filter out outier pixels from an array

    Args:
        in_array (array): The numpy array to be filtered
        fill_mean (bool): Fill outliers with the mean value rather than min/max

    Returns:
        array: filtered array
    """
    # remove outliers
    array_mean = np.mean(in_array)
    # have to lower precision of std calculation to prevent overflow errors
    std_cutoff = 3.0 * np.std(in_array, dtype=np.float32)
    outlier_h = array_mean + std_cutoff
    outlier_l = array_mean - std_cutoff
    filt_a = in_array.copy()
    if fill_mean:
        filt_a[filt_a > outlier_h] = array_mean
        filt_a[filt_a < outlier_l] = array_mean
    else:
        filt_a[filt_a > outlier_h] = np.amax(filt_a[filt_a <= outlier_h])
        filt_a[filt_a < outlier_l] = np.amin(filt_a[filt_a >= outlier_l])
    return filt_a


def norm_array(in_array: np.ndarray) -> np.ndarray:
    """Normalize an array

    Args:
        in_array (array): The numpy array to be normalized

    Returns:
        array: normalized array
    """
    # normalize  from 0-255
    normed = np.nan_to_num((in_array - np.min(in_array)) / np.ptp(in_array))
    intarray = np.full(normed.shape, 255)
    normed = normed * intarray
    return normed


def bin_and_norm_array(in_array: np.ndarray, newsize: int) -> np.ndarray:
    """Bin and normalize an array

    If the array is 3D it is first summed along the z axis

    Args:
        in_array (array): The numpy array to be binned
        newsize (int): The new size, must be square

    Returns:
        array: The binned and normalized array

    Raises:
        ValueError: If the array is not 2D or 3D

    """
    # If 3D array merge along zaxis
    if len(in_array.shape) == 3:
        raw = np.sum(in_array, axis=0)
    elif len(in_array.shape) == 2:
        raw = in_array
    else:
        raise ValueError(f"Array is {len(in_array.shape)}D; must be 2D or 3D")

    # bin array
    binned = bin_array_to_size(raw, newsize)
    # remove outliers
    filtered_binned = remove_outliers(binned)
    # normalize and return
    return norm_array(filtered_binned)


def mrc_thumbnail(
    in_mrc: str, new_size: int, outname: Optional[str] = None
) -> np.ndarray:
    """Write a thumbnail image from an mrc file

    Args:
        in_mrc (str): Name of input file
        new_size (int): The size to make the thumbnail axis 0 of the image will be
            shrunk to this size and axis 1 shrunk proportionally
        outname (str): Name of the output file

    Returns:
        array: The binned and normalized (and z-summed if applicable) array
    """
    with mrcfile.open(in_mrc) as mrc:
        raw = mrc.data
    normed = bin_and_norm_array(raw, new_size)
    if outname:
        outdir = os.path.dirname(outname)
        if outdir:
            os.makedirs(outdir, exist_ok=True)
        outimg = Image.fromarray(normed)
        outimg = outimg.convert("RGB")
        outimg.save(outname)

    return normed


def tiff_thumbnail(
    in_tiff: str, new_size: int, outname: Optional[str] = None
) -> np.ndarray:
    """Write a thumbnail image from a tif file

    Args:
        in_tiff (str): Name of input file
        new_size (int): The size to make the thumbnail axis. 0 of the image will be
            shrunk to this size and axix 1 shrunk proportionally
        outname (str): Name of the output file, if any

    Returns:
        array: The binned and normalized (and z-summed if applicable) array
    """
    raw = read_tiff(in_tiff, new_size=new_size)
    if len(raw.shape) == 2:  # 2D array
        filt_array = remove_outliers(raw)
        normed = norm_array(filt_array)
        if outname is not None:
            outimg = Image.fromarray(normed)
            outimg = outimg.convert("RGB")
            outimg.save(outname)
    # If 3D array merge along zaxis
    elif len(raw.shape) == 3:  # stack of images
        img_array = np.sum(raw, axis=0)
        filt_array = remove_outliers(img_array)
        normed = norm_array(filt_array)
        if outname is not None:
            outimg = Image.fromarray(normed)
            outimg = outimg.convert("RGB")
            outimg.save(outname)
    elif len(raw.shape) == 4 and raw.shape[-1] in [3, 4]:  # stack of RGB/RGBA images
        img_array = np.sum(raw, axis=0, dtype=np.uint8)
        normed = img_array
        # TODO : add an option to filter/denoise RGB images
        if outname is not None:
            outimg = Image.fromarray(img_array, "RGB")
            outimg.save(outname)
    else:
        raise ValueError(
            f"Array is {len(raw.shape)}D; \
                        must be 2d or 3D (stack) or 4D (RGB/RGBA)"
        )
    return normed


def bin_axis(data: np.ndarray, axis: int, bstep: float, binsize: float) -> np.ndarray:
    """Bin a numpy array along one axis

    Args:
        data (array): The array to bin
        axis (int): The axis to bin along
        bstep (float): Number of points between each bin
        binsize (float): Size of each bin

    Returns:
        array: The array binned along the selected axis
    """
    data = np.array(data)
    dims = np.array(data.shape)
    argdims = np.arange(data.ndim)
    argdims[0], argdims[axis] = argdims[axis], argdims[0]
    data = data.transpose(argdims)
    data = np.array(
        [
            np.mean(
                np.take(data, np.arange(int(i * bstep), int(i * bstep + binsize)), 0), 0
            )
            for i in np.arange(dims[axis] // bstep)
        ]
    )
    data = data.transpose(argdims)
    return data


def bin_array_to_size(in_array: np.ndarray, dim: int) -> np.ndarray:
    """Bin an array to a specific size

    Bins an array so axis 0 is the specified size

    Args:
        in_array: The array to bin
        dim (int): The size to bin axis 0 to.  Axis 1 will be binned
            proportionally.
    Returns:
        array: The binned array
    """
    binx = float(in_array.shape[0]) / dim
    if binx <= 1:
        return in_array

    if len(in_array.shape) == 2:
        return bin_axis(bin_axis(in_array, 0, binx, binx), 1, binx, binx)

    elif len(in_array.shape) == 3:
        ox, oy, oz = [float(x) for x in in_array.shape]
        if not ox == oy == oz:
            raise ValueError("Only works on 3D arrays where all dimensions are equal")
        in_cube = ox * oy * oz
        new_cube = dim**3
        binx = (in_cube / new_cube) ** (1.0 / 3)

        return bin_axis(
            bin_axis(bin_axis(in_array, 0, binx, binx), 1, binx, binx), 2, binx, binx
        )
    else:
        raise ValueError("Only functions on arrays with 1, 2, or 3 axes")


def normalise_images(
    raw: np.ndarray,
    boxsize: int,
    cmap: str = "",
    inpnormed: bool = False,
) -> np.ndarray:
    """Conditionally resample, normalise and recolour images

    RGB or RGBA images should be resampled before calling this function.

    Args:
        raw (array): 3D input array (or 4D for RGB / RGBA)
        boxsize (int): Desired size of each image in px
        cmap (str): colormap to apply if any
        inpnormed (bool): input already normalised?

    Returns:
        List[array]: Normalised image tiles
    """

    boxsize = boxsize if boxsize < raw.shape[1] else raw.shape[1]

    # get a list of the individual images and normalize them
    # RGB images are expected to be resampled prior to this (e.g. read_tiff())
    if boxsize < raw.shape[1] and not (
        len(raw.shape) == 4 and raw.shape[-1] in [3, 4]
    ):  # stack of RGB/RGBA images
        individual_imgs = [
            bin_array_to_size(raw[n, :, :], boxsize) for n in range(raw.shape[0])
        ]
    else:
        individual_imgs = [raw[n, :, :] for n in range(raw.shape[0])]

    normed_imgs = []
    if not cmap:
        if not inpnormed:
            for x in individual_imgs:
                normed = np.nan_to_num((x - np.min(x)) / np.ptp(x))
                intarray = np.full(normed.shape, 255)
                normed = normed * intarray
                normed_imgs.append(normed)
        else:
            normed_imgs = individual_imgs
    else:
        for x in individual_imgs:
            tocolour = (x - np.min(x)) / np.ptp(x)
            cm = get_cmap(cmap)
            coloured = cm(tocolour)
            img = (coloured[:, :, :3] * 255).astype(np.uint8)
            normed_imgs.append(img)

    return np.array(normed_imgs)


def threed_array_to_montage(
    raw: np.ndarray,
    ncols: int,
    boxsize: int,
    outname: str = "",
    cmap: str = "",
    inpnormed: bool = False,
    border: float = 0.01,
) -> np.ndarray:
    """Make a single array with tiled images

    Args:
        raw (array): 3D input array
        ncols (int): Number of columns desired in the montage
        boxsize (int): Desired size of each panel in px
        outname (str): Output file name
        cmap (str): colormap to apply if any
        inpnormed (bool): input already normalised?
        border (float): Border (in percent) around each image

    Returns:
        array: The images tiles in an array of shape [x, ncols]
    """

    normed_imgs = normalise_images(
        raw=raw, boxsize=boxsize, cmap=cmap, inpnormed=inpnormed
    )

    # calculate the number of rows and create target array
    n_rows = raw.shape[0] // ncols
    final_row = raw.shape[0] % ncols != 0

    bindim = normed_imgs[0].shape
    totrows = n_rows + int(final_row)

    border_px = int(bindim[0] * border)
    border_px = 2 if border_px < 2 else border_px

    img_mean = 1.25 * np.mean([np.mean(x) for x in normed_imgs])
    if not cmap and not (len(bindim) == 3 and bindim[-1] in [3, 4]):
        target_array: np.ndarray = np.full(
            [
                (totrows * bindim[0]) + (border_px * (totrows + 1)),
                (ncols * bindim[1]) + (border_px * (ncols + 1)),
            ],
            img_mean,
            dtype=np.int8,
        )
    else:
        target_array = np.full(
            [
                (totrows * bindim[0]) + (border_px * (totrows + 1)),
                (ncols * bindim[1]) + (border_px * (ncols + 1)),
                3,
            ],
            img_mean,
            dtype=np.uint8,
        )
    xpos, ypos = 0, 0
    for img in normed_imgs:
        y_border_px_add = border_px * (ypos + 1)
        x_border_px_add = border_px * (xpos + 1)
        ystart = (ypos * bindim[0]) + y_border_px_add
        yend = ((ypos + 1) * bindim[0]) + y_border_px_add
        xstart = (xpos * bindim[1]) + x_border_px_add
        xend = ((xpos + 1) * bindim[1]) + x_border_px_add
        xpos += 1
        if xpos % ncols == 0:
            xpos = 0
            ypos += 1
        target_array[ystart:yend, xstart:xend] = img

    if outname:
        outimg = Image.fromarray(target_array)
        outimg = outimg.convert("RGB")
        outimg.save(outname)

    return target_array


def norm_image_to_base64_string(image: np.ndarray) -> str:
    """Encodes a normalised image to a base64 string
    Args:
        rgb_image (np-ndarray): Normalised image

    Returns:
        str: Base64 string image for use on the web.
    """
    input_image = Image.fromarray(image)
    rgb_image = input_image.convert("RGB")
    # store image as bytes in memory, and convert to base64
    buffer = BytesIO()
    rgb_image.save(buffer, format="PNG")
    buffer.seek(0)
    base64_bytes_string = base64.b64encode(buffer.getvalue())
    base64_string = base64_bytes_string.decode()

    return base64_string


def threed_array_to_images(
    raw: np.ndarray,
    ncols: int,
    boxsize: int,
    labels: Sequence[Union[str, int]] = [],
    outname: str = "",
    cmap: str = "",
    inpnormed: bool = False,
    border: float = 0.01,
) -> str:
    """Make an JSON file containing an array of base64 image objects

    Args:
        raw (array): 3D input array
        ncols (int): Number of columns desired in the montage
        boxsize (int): Desired size of each panel in px
        outname (str): Output file name
        cmap (str): colormap to apply if any
        inpnormed (bool): input already normalised?
        border (float): Border (in percent) around each image
        labels (array): labels

    Returns:
        str: Filepath to the .json file containing base64
            image objects.
    """
    normed_imgs = normalise_images(
        raw=raw, boxsize=boxsize, cmap=cmap, inpnormed=inpnormed
    )

    image_list_file_path = os.path.join(os.path.dirname(outname), "imagelist.json")
    outdata = []

    if labels:
        try:
            labels = [int(x) for x in labels]
            numeric_labels = True
        except ValueError:
            numeric_labels = False

    for index, img in enumerate(normed_imgs):
        base64_string = norm_image_to_base64_string(img)

        if labels and not numeric_labels:
            data = {"classID": str(labels[index]), "img": base64_string}

        elif labels and numeric_labels:
            data = {"classID": str(int(labels[index]) + 1), "img": base64_string}

        else:
            data = {"classID": str(index), "img": base64_string}

        outdata.append(data)

    with open(image_list_file_path, "w") as image_list:
        json.dump(outdata, image_list)

    return image_list_file_path


def pad_and_center_slice(slice_data, max_dim, const: int = 0):
    pad_width = (
        (max_dim - slice_data.shape[0]) // 2,
        (max_dim - slice_data.shape[1]) // 2,
    )
    padded_slice = np.pad(
        slice_data,
        (
            (pad_width[0], max_dim - pad_width[0] - slice_data.shape[0]),
            (pad_width[1], max_dim - pad_width[1] - slice_data.shape[1]),
        ),
        mode="constant",
        constant_values=const,
    )

    return padded_slice


def mrc_central_slices(in_mrc: str) -> np.ndarray:
    with mrcfile.mmap(in_mrc) as mrc_vol:
        shape = mrc_vol.data.shape
        if len(shape) != 3:
            raise ValueError(
                f"Can only make central slices for a 3D volume. File {in_mrc} has"
                f" {len(shape)} dimensions."
            )
        central_indices = [dim // 2 for dim in shape]

        xy_slice = mrc_vol.data[central_indices[0], :, :]
        xz_slice = mrc_vol.data[:, central_indices[1], :]
        yz_slice = mrc_vol.data[:, :, central_indices[2]]

        max_dim = max(shape)

        xy_slice = pad_and_center_slice(xy_slice, max_dim)
        xz_slice = pad_and_center_slice(xz_slice, max_dim)
        yz_slice = pad_and_center_slice(yz_slice, max_dim)

        central_slices = np.stack([xy_slice, xz_slice, yz_slice], axis=0)

    return central_slices


def mrcs_create_slices_for_montage(
    in_files: Dict[str, str]
) -> Tuple[np.ndarray, tuple]:
    arrays: List[np.ndarray] = []
    for f in in_files.keys():
        arrays.append(mrc_central_slices(f))

    # The central slices should be in stacks of 3 square images, so we expect this to be
    # (3, max_x, max_y) where max_x and max_y will be the same
    max_shape = tuple(np.max([arr.shape for arr in arrays], axis=0))

    if not all([arr.shape == max_shape for arr in arrays]):
        max_dim = max(max_shape[1:])
        padded_arrays = []
        for array in arrays:
            slice_store = []
            for slice2d in array:
                slice_store.append(
                    pad_and_center_slice(slice2d, max_dim, const=np.max(slice2d))
                )
            padded_arrays.append(np.array(slice_store))
        arrays = padded_arrays

    # Stack the central slices along the y-axis
    if len(arrays) > 1:
        central_slices = np.concatenate(arrays, axis=0)
    else:
        central_slices = arrays[0]

    return central_slices, max_shape


def mrcs_slices_montage(
    in_files: Dict[str, str], outname: str, cmap: str = ""
) -> List[str]:
    central_slices, max_shape = mrcs_create_slices_for_montage(in_files)

    threed_array_to_montage(
        raw=central_slices,
        ncols=3,
        boxsize=max_shape[1],
        outname=outname,
        cmap=cmap,
    )
    slicenames: List[str] = []

    # if there are more than one file in the montage use the labels for tooltips
    # otherwise use the filename
    for f in in_files:
        if len(in_files) > 1:
            lab = f if not in_files[f] else in_files[f]
        else:
            lab = f

        slicenames[:0] = [f"{lab}: xy", f"{lab}: xz", f"{lab}: yz"]

    return slicenames


def mrcs_slices_montage_base64(
    in_files: Dict[str, str], outname: str, cmap: str = ""
) -> Tuple[List[str], str]:
    central_slices, max_shape = mrcs_create_slices_for_montage(in_files)

    threed_array_to_montage(
        raw=central_slices,
        ncols=3,
        boxsize=max_shape[1],
        outname=outname,
        cmap=cmap,
    )
    slicenames: List[str] = []

    # if there are more than one file in the montage use the labels for tooltips
    # otherwise use the filename
    for f in in_files:
        if len(in_files) > 1:
            lab = f if not in_files[f] else in_files[f]
        else:
            lab = f
        # keep the labels in order of the base64 images
        slicenames += [f"{lab}: xy", f"{lab}: xz", f"{lab}: yz"]

    base64_file = threed_array_to_images(
        raw=central_slices,
        ncols=3,
        labels=slicenames,
        boxsize=max_shape[1],
        outname=outname,
        cmap=cmap,
    )

    return slicenames, base64_file
